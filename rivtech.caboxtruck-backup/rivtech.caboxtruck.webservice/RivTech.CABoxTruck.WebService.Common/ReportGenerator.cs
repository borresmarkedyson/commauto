﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Common.ReportWorksheetStyles;
using System;
using System.Data;
using System.IO;

namespace RivTech.CABoxTruck.WebService.Common
{
    public class ReportGenerator : IReportGenerator
    {
        public void GenerateReportToExcel(string reportName, string reportContent, MemoryStream memoryStream)
        {
            DataTable dataTable = JsonConvert.DeserializeObject<DataTable>(reportContent);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                dataTable.TableName = reportName;

                using (XLWorkbook workbook = new XLWorkbook())
                {
                    IXLWorksheet sheet = workbook.Worksheets.Add(dataTable);

                    sheet.Table(reportName).ShowAutoFilter = false;
                    sheet.Table(reportName).Theme = XLTableTheme.None;

                    switch (reportName)
                    {
                        case "DMVReport":
                            DMVReportWorksheetStyle.ReportWorksheetStyle(sheet);
                            break;
                    }

                    workbook.SaveAs(memoryStream);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                }
            }
        }
    }
}
