﻿using System.IO;

namespace RivTech.CABoxTruck.WebService.Common
{
    public interface IReportGenerator
    {
        void GenerateReportToExcel(string reportName, string reportContent, MemoryStream memoryStream);
    }
}
