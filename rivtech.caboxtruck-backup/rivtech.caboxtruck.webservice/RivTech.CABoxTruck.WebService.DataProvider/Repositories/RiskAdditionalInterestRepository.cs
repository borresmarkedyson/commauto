﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories
{
    public class RiskAdditionalInterestRepository : BaseRepository<RiskAdditionalInterest, Guid>, IRiskAdditionalInterestRepository
    {
        public RiskAdditionalInterestRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task<IEnumerable<RiskAdditionalInterest>> GetAllIncludeAsync(Guid riskDetailId)
        {
            return await _context.RiskAdditionalInterest
                                .Include(config => config.Entity)
                                    .ThenInclude(config => config.EntityAddresses)
                                        .ThenInclude(config => config.Address)
                                .Where(x => x.RiskDetailId == riskDetailId)
                                .ToListAsync();
        }
    }
}
