﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories
{
    public class RiskManuscriptsRepository : BaseRepository<RiskManuscripts, Guid>, IRiskManuscriptsRepository
    {
        public RiskManuscriptsRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task CancelManuscript(Guid riskDetailId, long manuscriptIdentifier, DateTime effectiveDate, decimal proratedAmount)
        {
            var riskManuscripts = await _context.Set<RiskManuscripts>().Where(manuscript => manuscript.RiskDetailId == riskDetailId).ToListAsync();
            var manuscript = riskManuscripts.FirstOrDefault(x => x.CreatedDate.Ticks == manuscriptIdentifier);

            if (manuscript != null)
            {
                manuscript.ExpirationDate = effectiveDate;
                manuscript.DeletedDate = effectiveDate;
                manuscript.ProratedPremium = proratedAmount;
                _context.Update(manuscript);
                await _context.SaveChangesAsync();
            }
        }


        public DateTime GetEndorsementEffectiveDate(RiskManuscripts manuscript)
        {
            var riskDetail = _context.RiskDetail.First(x => x.Id == manuscript.RiskDetailId);
            var query = (from manus in _context.Set<RiskManuscripts>().AsNoTracking()
                        join rd in _context.RiskDetail.AsNoTracking() on manus.RiskDetailId equals rd.Id
                        join ph in _context.PolicyHistory.AsNoTracking() on rd.Id.ToString() equals ph.PreviousRiskDetailId 
                        join broker in _context.BrokerInfo.AsNoTracking() on rd.RiskId equals broker.RiskId
                        where manus.Title == manuscript.Title && ph.PolicyStatus == RiskStatus.Active && rd.RiskId == riskDetail.RiskId
                        orderby rd.CreatedDate
                        select new { Id = manus.Id, RiskDetailId = rd.Id, EffectiveDate = rd.EndorsementEffectiveDate ?? broker.EffectiveDate.Value }
                    ).Distinct();

            var first = query.First();

            return first.EffectiveDate;
        }
    }
}


