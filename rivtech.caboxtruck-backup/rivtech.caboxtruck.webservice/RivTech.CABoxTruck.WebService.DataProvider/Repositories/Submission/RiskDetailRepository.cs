﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories
{
    public class RiskDetailRepository : BaseRepository<RiskDetail, Guid>, IRiskDetailRepository
    {
        public RiskDetailRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task<IEnumerable<RiskDetail>> GetAllIncludeAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<RiskDetail> GetOneIncludeByIdAsync(Guid id)
        {
            var risk = await _context.RiskDetail
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.Driver).ThenInclude(config => config.Entity)
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.Driver).ThenInclude(config => config.DriverIncidents)
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.RiskDetail)
                                .Include(risk => risk.RadiusOfOperations)
                                .Include(risk => risk.Insured).ThenInclude(config => config.Entity)
                                    .ThenInclude(config => config.EntityAddresses).ThenInclude(config => config.Address)
                                .Include(risk => risk.RiskFilings)
                                .Include(risk => risk.DriverInfo).ThenInclude(driverInfo => driverInfo.MvrPullingFrequency)
                                .Include(risk => risk.DriverHeader)
                                .Include(risk => risk.DriverHiringCriteria)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.ChameleonIssues)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.CurrentSaferRating)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.IssCabRating)
                                .Include(risk => risk.UnderwritingQuestions)
                                .AsSplitQuery()
                                //.Include(risk => risk.Vehicles)
                                .Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.Vehicle)
                                .AsSplitQuery()
                                //.Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.Vehicle.VehicleProratedPremium)
                                .Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.RiskDetail)
                                .AsSplitQuery()
                                .Include(risk => risk.RiskCoverages).ThenInclude(riskCoverage => riskCoverage.RiskCoveragePremium)
                                .AsSplitQuery()
                                .Include(risk => risk.DriverHiringCriteria).ThenInclude(x => x.BackGroundCheckIncludes)
                                .AsSplitQuery()
                                .Include(risk => risk.DriverHiringCriteria).ThenInclude(x => x.DriverTrainingIncludes)
                                .AsSplitQuery()
                                .Include(risk => risk.CommoditiesHauled)
                                //.AsSplitQuery()
                                //.Include(risk => risk.RiskResponses).ThenInclude(x => x.Question)
                                .FirstOrDefaultAsync(x => x.Id == id);

            return risk;
        }

        public async Task<RiskDetail> GetInsuredAsync(Guid riskId)
        {
            return await _context.Set<RiskDetail>()
                                .Include(i => i.Insured).ThenInclude(ti => ti.Entity)
                                .Where(x => x.Id == riskId).FirstOrDefaultAsync();
        }

        public async Task<RiskDetail> GetInsuredAddressAsync(Guid riskDetailId)
        {
            return await _context.Set<RiskDetail>()
                                .Include(i => i.Insured)
                                    .ThenInclude(ti => ti.Entity)
                                        .ThenInclude(config => config.EntityAddresses)
                                            .ThenInclude(config => config.Address)
                                .Where(x => x.Id == riskDetailId).FirstOrDefaultAsync();
        }

        public async Task<RiskDetail> GetInsuredAddressByRiskIdAsync(Guid riskId)
        {
            return await _context.Set<RiskDetail>()
                                .Include(i => i.Insured)
                                    .ThenInclude(ti => ti.Entity)
                                        .ThenInclude(config => config.EntityAddresses)
                                            .ThenInclude(config => config.Address)
                                .Where(x => x.RiskId == riskId).OrderByDescending(x => x.UpdatedDate).FirstOrDefaultAsync();
        }

        public async Task<List<VehiclePremiumRatingFactor>> GetVehicleRatingFactorsByRiskDetailId(Guid riskDetailId)
        {
            return await _context.Set<VehiclePremiumRatingFactor>().Where(vprf => vprf.RiskDetailId == riskDetailId).ToListAsync();
        }

        public async Task<RiskDetail> GetSubmissionRiskDetailAsync(Guid riskId)
        {
            IQueryable<PolicyHistory> policyHistories = _context.PolicyHistory.Where(x => x.RiskId == riskId.ToString());
            if (!policyHistories.Any())
                return await _context.RiskDetail.FirstOrDefaultAsync(x => x.RiskId == riskId);

            string riskDetailId = policyHistories.FirstOrDefault(x => x.EndorsementNumber == "0")?.PreviousRiskDetailId;
            if (riskDetailId is null)
                return null;
            return await GetOneIncludeByIdAsync(Guid.Parse(riskDetailId));
        }

        public async Task<RiskDetail> GetLatestRiskDetailByPolicyNumberAsync(Guid id)
        {
            var risk = await _context.RiskDetail
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.Driver).ThenInclude(config => config.Entity)
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.Driver).ThenInclude(config => config.DriverIncidents)
                                .Include(risk => risk.RiskDetailDrivers).ThenInclude(rdv => rdv.RiskDetail)
                                .Include(risk => risk.RadiusOfOperations)
                                .Include(risk => risk.Insured).ThenInclude(config => config.Entity)
                                    .ThenInclude(config => config.EntityAddresses).ThenInclude(config => config.Address)
                                .Include(risk => risk.RiskFilings)
                                .Include(risk => risk.DriverInfo).ThenInclude(driverInfo => driverInfo.MvrPullingFrequency)
                                .Include(risk => risk.DriverHeader)
                                .Include(risk => risk.DriverHiringCriteria)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.ChameleonIssues)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.CurrentSaferRating)
                                .Include(risk => risk.DotInfo).ThenInclude(dotInfo => dotInfo.IssCabRating)
                                .Include(risk => risk.UnderwritingQuestions)
                                .AsSplitQuery()
                                //.Include(risk => risk.Vehicles)
                                .Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.Vehicle)
                                .AsSplitQuery()
                                //.Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.Vehicle.VehicleProratedPremium)
                                .Include(risk => risk.RiskDetailVehicles).ThenInclude(rdv => rdv.RiskDetail)
                                .AsSplitQuery()
                                .Include(risk => risk.RiskCoverages).ThenInclude(riskCoverage => riskCoverage.RiskCoveragePremium)
                                .AsSplitQuery()
                                .Include(risk => risk.DriverHiringCriteria).ThenInclude(x => x.BackGroundCheckIncludes)
                                .AsSplitQuery()
                                .Include(risk => risk.DriverHiringCriteria).ThenInclude(x => x.DriverTrainingIncludes)
                                .AsSplitQuery()
                                .Include(risk => risk.CommoditiesHauled)
                                //.AsSplitQuery()
                                //.Include(risk => risk.RiskResponses).ThenInclude(x => x.Question)
                                .FirstOrDefaultAsync(x => x.Id == id);

            return risk;
        }
        public async Task<RiskDetail> GetLatestRiskDetailByRiskIdAsync(Guid? riskId)
        {
            var riskDetail = await _context.RiskDetail
                                .Include(risk => risk.Insured).ThenInclude(config => config.Entity).ThenInclude(config => config.EntityAddresses).ThenInclude(config => config.Address)
                                .OrderByDescending(o => o.UpdatedDate).FirstOrDefaultAsync(x => x.RiskId == riskId);

            return riskDetail;
        }

        public async Task<RiskDetail> GetFirstRiskDetailByRiskIdAsync(Guid? riskId)
        {
            var riskDetail = await _context.RiskDetail
                                .OrderBy(o => o.UpdatedDate).FirstOrDefaultAsync(x => x.RiskId == riskId);

            return riskDetail;
        }
    }
}
