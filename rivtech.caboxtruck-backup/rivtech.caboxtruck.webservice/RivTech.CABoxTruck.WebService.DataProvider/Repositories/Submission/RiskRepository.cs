﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Common;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.DTO.Submission;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories.Submission
{
    public class RiskRepository : BaseRepository<Risk, Guid>, IRiskRepository
    {
        public RiskRepository(AppDbContext context) : base(context)
        {

        }

        public IEnumerable<RiskSummaryDto> GetFilteredSubmissionSummaryListAsync(SubmissionFilterDto filter)
        {
            //var result = from risk in _context.Set<Risk>()
            //            .Include(risk => risk.RiskDetails).ThenInclude(riskDetail => riskDetail.Insured).ThenInclude(insured => insured.Entity)
            //             select new RiskSummaryDto
            //             {
            //                 Id = Guid.NewGuid(),
            //                 CurrentRiskId = risk.RiskDetails[0].Id,
            //                 SubmissionNumber = risk.SubmissionNumber,
            //                 PolicyNumber = risk.PolicyNumber,
            //                 Status = risk.RiskDetails[0].Status,
            //                 InsuredName = risk.RiskDetails[0].Insured.Entity.CompanyName,
            //                 InsuredEntityId = risk.RiskDetails[0].Insured.EntityId,
            //                 NewVenture = risk.RiskDetails[0].IsNewVenture.ToYesNo(),
            //                 UseClass = risk.RiskDetails[0].MainUseId,
            //                 DateSubmitted = risk.RiskDetails[0].CreatedDate,
            //                 RiskId = risk.RiskDetails[0].RiskId,
            //                 Owner = risk.RiskDetails[0].Insured.Entity.BusinessPrincipal
            //             };

            var risks = _context.Set<Risk>().Include(risk => risk.RiskDetails).ThenInclude(riskDetail => riskDetail.Insured).ThenInclude(insured => insured.Entity);
            var result = new List<RiskSummaryDto>();
            foreach(var item in risks)
            {
                var riskSummary = new RiskSummaryDto();
                // Policy Submission
                if (item.RiskDetails.Count() > 1 && item.Status != RiskStatus.Pending)
                {
                    //var issued = item.RiskDetails.OrderByDescending(x => x.UpdatedDate).Where(x => x.Status == RiskDetailStatus.Issued).FirstOrDefault();
                    var latestEndorsement = item.RiskDetails.OrderByDescending(x => x.UpdatedDate).FirstOrDefault();
                    if (latestEndorsement != null)
                    { 
                        riskSummary.Id = Guid.NewGuid();
                        riskSummary.CurrentRiskId = latestEndorsement.Id;
                        riskSummary.SubmissionNumber = item.SubmissionNumber;
                        riskSummary.PolicyNumber = item.PolicyNumber;
                        riskSummary.Status = latestEndorsement.Status == RiskDetailStatus.PendingEndorsement ? RiskDetailStatus.Active: latestEndorsement.Status;
                        riskSummary.InsuredName = latestEndorsement.Insured.Entity.CompanyName;
                        riskSummary.InsuredEntityId = latestEndorsement.Insured.EntityId;
                        riskSummary.NewVenture = latestEndorsement.IsNewVenture.ToYesNo();
                        riskSummary.UseClass = latestEndorsement.MainUseId;
                        riskSummary.DateSubmitted = latestEndorsement.CreatedDate;
                        riskSummary.RiskId = latestEndorsement.RiskId;
                        riskSummary.Owner = latestEndorsement.Insured.Entity.BusinessPrincipal;
                        riskSummary.PolicyStatus = item.Status;
                        riskSummary.IsPolicy = true;
                        riskSummary.CreatedDate =
                            item.RiskDetails.OrderBy(x => x.CreatedDate).FirstOrDefault().CreatedDate;
                        riskSummary.AssignedToId = latestEndorsement.AssignedToId.HasValue ? latestEndorsement.AssignedToId.Value.ToString() : null;
                        result.Add(riskSummary);
                        continue;
                    }
                }

                // Submission
                riskSummary = new RiskSummaryDto
                {
                    Id = Guid.NewGuid(),
                    CurrentRiskId = item.RiskDetails[0].Id,
                    SubmissionNumber = item.SubmissionNumber,
                    PolicyNumber = item.PolicyNumber,
                    Status = item.RiskDetails[0].Status,
                    PolicyStatus = item.Status,
                    InsuredName = item.RiskDetails[0].Insured.Entity.CompanyName,
                    InsuredEntityId = item.RiskDetails[0].Insured.EntityId,
                    NewVenture = item.RiskDetails[0].IsNewVenture.ToYesNo(),
                    UseClass = item.RiskDetails[0].MainUseId,
                    DateSubmitted = item.RiskDetails[0].CreatedDate,
                    RiskId = item.RiskDetails[0].RiskId,
                    Owner = item.RiskDetails[0].Insured.Entity.BusinessPrincipal,
                    CreatedDate = item.RiskDetails.OrderBy(x => x.CreatedDate).FirstOrDefault().CreatedDate,
                    AssignedToId = item.RiskDetails[0].AssignedToId.HasValue ? item.RiskDetails[0].AssignedToId.Value.ToString() : null
                };
                result.Add(riskSummary);
            }

            return result.AsEnumerable();
        }

        public async Task<Risk> GetRiskByPolicyNumberAsync(string policyNumber)
        {
            return _context.Set<Risk>()
                        .Where(x => x.PolicyNumber == policyNumber && (x.Status == RiskStatus.Active || x.Status == RiskStatus.Canceled))
                        .Include(risk => risk.RiskDetails)
                        .ThenInclude(riskDetail => riskDetail.Insured)
                        .ThenInclude(insured => insured.Entity)
                        .ThenInclude(config => config.EntityAddresses).FirstOrDefault();
        }

        public async Task<List<Guid>> GetAllRiskIdForCancellationAsync(DateTime pendingCancellationDate)
        {
            return await _context.Set<Risk>()
                        .Where(x => x.PendingCancellationDate.Value.Date == pendingCancellationDate.Date && x.Status == RiskStatus.Active)
                        .Select(x => x.Id).ToListAsync();
        }

        public async Task<List<Risk>> GetAllRiskIdForPendingCancellationAsync()
        {
            return await _context.Set<Risk>()
                        .Where(x => x.PendingCancellationDate != null && x.Status == RiskStatus.Active).ToListAsync();
        }
        
        public async Task<Risk> GetRiskByIdAsync(Guid id)
        {
            return await _context.Set<Risk>()
                        .Where(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
