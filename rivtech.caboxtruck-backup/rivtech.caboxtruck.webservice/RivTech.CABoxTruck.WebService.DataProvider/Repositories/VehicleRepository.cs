﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories
{
    public class VehicleRepository : BaseRepository<Vehicle, Guid>, IVehicleRepository
    {
        public VehicleRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task<IEnumerable<Vehicle>> GetAllIncludeAsync()
        {
            return await _context.Set<Vehicle>()
                                //.Include(config => config.driverSummary)
                                //.Include(config=> config.driverPeriod)
                                .ToListAsync();
        }

        public async Task<IEnumerable<Vehicle>> GetAllIncludeAsync(Expression<Func<Vehicle, bool>> expression)
        {
            return await _context.Set<Vehicle>()
                                .Where(expression)
                                //.Include(config => config.driverSummary)
                                //.Include(config => config.driverPeriod)
                                .ToListAsync();
        }

        public Vehicle GetById(Guid vehicleId)
        {
            return _context.Vehicle
                    //.Include(v => v.VehicleProratedPremium)
                    .Include(v => v.RiskDetailVehicles)
                        .ThenInclude(v => v.RiskDetail)
                    .SingleOrDefault(v => v.Id == vehicleId);
        }

        public async Task<IEnumerable<Vehicle>> GetByRiskDetailAsync(Guid riskDetailId, bool? fromPolicy = null)
        {
            var vehicles = await _context.Vehicle
                //.Include(v => v.VehicleProratedPremium)
                .Include(v => v.PreviousVehicleVersion)//.ThenInclude(v => v.VehicleProratedPremium)
                .Include(v => v.RiskDetailVehicles)
                    .ThenInclude(rdv => rdv.RiskDetail)
                .Where(v => v.RiskDetailVehicles.Any(rdv => rdv.RiskDetailId == riskDetailId))
                ?.ToListAsync() ?? new List<Vehicle>();

            var filteredVehicles = vehicles
                .OrderBy(x => x.VIN)
                .Where(v =>
                    !v.IsDeleted
                    || (fromPolicy == true && v.IsDeleted)
                );

            foreach (var veh in filteredVehicles)
            {
                AssignPremiums(veh, riskDetailId, fromPolicy ?? false);
            }

            return filteredVehicles;
        }

        private void AssignPremiums(Vehicle vehicle, Guid riskDetailId, bool fromPolicy)
        {
            int option = 0;
            if (!fromPolicy)
            {
                var query = from rd in _context.RiskDetail
                            join bind in _context.Set<Binding>() on rd.RiskId equals bind.RiskId
                            where rd.Id == riskDetailId
                            select bind;
                option = query.FirstOrDefault()?.BindOptionId ?? 1;
            }
            vehicle.VehiclePremiums = _context.VehiclePremium.Where(x => x.VehicleId == vehicle.Id)?.ToList() ?? new List<VehiclePremium>();
            vehicle.VehiclePremiumRatingFactor = _context.VehiclePremiumRatingFactor
                .FirstOrDefault(v => v.VehicleId == vehicle.Id.ToString() && v.RiskDetailId == riskDetailId && v.OptionId == option);
        }

        public async Task<IEnumerable<Vehicle>> GetPreviousVehiclesByRiskDetail(RiskDetail riskDetail, bool? fromPolicy = null)
        {
            var riskDetails = from rd in _context.RiskDetail
                        join ph in _context.PolicyHistory on rd.Id.ToString() equals ph.PreviousRiskDetailId
                        where rd.RiskId == riskDetail.RiskId && ph.PolicyStatus == RiskStatus.Active
                        orderby rd.CreatedDate descending
                        select rd;
            var previousRiskDetail = riskDetails?.FirstOrDefault();
            if (previousRiskDetail is null)
                return null;
            return await GetByRiskDetailAsync(previousRiskDetail.Id, fromPolicy);
        }


        public async Task<bool> IsFromSubmission(Guid vehicleId)
        {
            Vehicle vehicle = GetById(vehicleId);
            IQueryable<PolicyHistory> policyHistory = _context.PolicyHistory.Where(x => x.RiskId == vehicle.RiskDetail.RiskId.ToString());
            string submissionRiskDetailId = policyHistory.FirstOrDefault(x => x.EndorsementNumber == "0")?.PreviousRiskDetailId ?? null;
            var vehicles = await GetByRiskDetailAsync(Guid.Parse(submissionRiskDetailId));
            return vehicles.Any(v => v.Id == vehicleId);
        }

        public bool IsOldVersion(Guid? id)
        {
            var query = from rdv in _context.RiskDetailVehicle
                        join rd in _context.RiskDetail on rdv.RiskDetailId equals rd.Id
                        join v in _context.Vehicle on rdv.VehicleId equals v.Id
                        join ph in _context.PolicyHistory on rd.Id.ToString() equals ph.PreviousRiskDetailId
                        where v.PreviousVehicleVersionId == id && ph.PolicyStatus == RiskStatus.Active
                        select v;
            return query.Any(x => x.PreviousVehicleVersionId == id);
        }

        public VehiclePremiumRatingFactor GetRatingFactors(Guid vehicleId, Guid riskDetailId)
        {
            return _context.VehiclePremiumRatingFactor.OrderByDescending(x => x.Id).FirstOrDefault(v => v.VehicleId == vehicleId.ToString() && v.RiskDetailId == riskDetailId);
        }

        public Task<bool> UpdateRatingFactors(VehiclePremiumRatingFactor ratingFactors)
        {
            _context.Update(ratingFactors);
            return Task.FromResult(true);
        }
    }
}


