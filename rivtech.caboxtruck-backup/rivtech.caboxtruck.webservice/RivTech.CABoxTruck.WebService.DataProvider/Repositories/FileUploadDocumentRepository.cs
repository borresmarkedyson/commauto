﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.Repositories
{
    public class FileUploadDocumentRepository : BaseRepository<FileUploadDocument, Guid>, IFileUploadDocumentRepository
    {
        public FileUploadDocumentRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task<IEnumerable<FileUploadDocument>> GetAllIncludeAsync()
        {
            return await _context.Set<FileUploadDocument>()
                                .Include(config => config.FileCategory)
                                .ToListAsync();
        }

        public async Task<IEnumerable<FileUploadDocument>> GetAllIncludeAsync(Expression<Func<FileUploadDocument, bool>> expression)
        {
            return await _context.Set<FileUploadDocument>()
                                .Where(expression)
                                .Include(config => config.FileCategory)
                                .Include(config => config.RiskDetail)
                                .ToListAsync();
        }
    }
}


