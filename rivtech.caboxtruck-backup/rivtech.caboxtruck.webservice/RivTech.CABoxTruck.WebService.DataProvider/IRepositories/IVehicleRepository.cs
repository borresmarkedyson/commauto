﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IVehicleRepository : IRepository<Vehicle, Guid>
    {
        Task<IEnumerable<Vehicle>> GetAllIncludeAsync();
        Task<IEnumerable<Vehicle>> GetAllIncludeAsync(Expression<Func<Vehicle, bool>> expression);
        Task<IEnumerable<Vehicle>> GetByRiskDetailAsync(Guid riskDetailId, bool? fromPolicy = false);
        Vehicle GetById(Guid vehicleId);
        Task<IEnumerable<Vehicle>> GetPreviousVehiclesByRiskDetail(RiskDetail riskDetail, bool? fromPolicy = false);
        Task<bool> IsFromSubmission(Guid vehicleId);
        bool IsOldVersion(Guid? id);
        VehiclePremiumRatingFactor GetRatingFactors(Guid vehicleId, Guid riskDetailId);
        Task<bool> UpdateRatingFactors(VehiclePremiumRatingFactor ratingFactors);
    }
}
