﻿using System;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskManuscriptsRepository : IRepository<RiskManuscripts, Guid>
    {
        Task CancelManuscript(Guid riskDetailId, long manuscriptIdentifier, DateTime effectiveDate, decimal proratedAmount);
        DateTime GetEndorsementEffectiveDate(RiskManuscripts manuscript);
    }
}