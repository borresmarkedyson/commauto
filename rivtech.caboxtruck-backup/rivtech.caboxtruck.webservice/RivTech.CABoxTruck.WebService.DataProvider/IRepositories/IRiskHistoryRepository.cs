﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskHistoryRepository : IRepository<RiskHistory, Guid>
    {
    }
}
