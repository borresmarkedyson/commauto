﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskNotesRepository : IRepository<RiskNotes, Guid>
    {
    }
}
