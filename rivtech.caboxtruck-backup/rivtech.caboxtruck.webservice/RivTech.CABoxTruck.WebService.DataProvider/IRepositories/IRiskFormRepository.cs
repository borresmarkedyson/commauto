﻿using System;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskFormRepository : IRepository<RiskForm, Guid>
    {
        Task UpdateRiskForm(RiskForm form);
    }
}