﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface ICoveredSymbolRepository : IRepository<CoveredSymbol, Int16>
    {
    }
}
