﻿using System;
using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRetailerAgentRepository : IRepository<RetailerAgent, Guid>
    {
    }
}