﻿using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskStatusTypeRepository : IRepository<LvRiskStatusType, byte>
    {
    }
}
