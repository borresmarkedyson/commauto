﻿using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRiskTypeRepository : IRepository<LvRiskType, byte>
    {
    }
}
