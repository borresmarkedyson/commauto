﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IFileUploadDocumentRepository : IRepository<FileUploadDocument, Guid>
    {
        Task<IEnumerable<FileUploadDocument>> GetAllIncludeAsync();
        Task<IEnumerable<FileUploadDocument>> GetAllIncludeAsync(Expression<Func<FileUploadDocument, bool>> expression);
    }

   
}
