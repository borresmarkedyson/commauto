﻿using RivTech.CABoxTruck.WebService.DTO.Billing.Taxes;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Billing
{
    public interface IStateTaxRepository
    {
        StateTax Get(string stateCode, DateTime effectiveDate);
    }
}
