﻿using RivTech.CABoxTruck.WebService.Data.Entity.Policy;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface ICancellationBreakdownRepository : IRepository<CancellationBreakdown, long>
    {
        IEnumerable<CancellationBreakdown> GetByRiskDetailId(Guid riskDetailId);
    }
}
