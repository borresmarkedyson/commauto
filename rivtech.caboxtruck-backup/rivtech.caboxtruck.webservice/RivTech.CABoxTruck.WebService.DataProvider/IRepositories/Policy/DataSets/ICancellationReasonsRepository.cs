﻿using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Policy.DataSets
{
    public interface ICancellationReasonsRepository : IRepository<CancellationReasons, short>
    {
    }
}
