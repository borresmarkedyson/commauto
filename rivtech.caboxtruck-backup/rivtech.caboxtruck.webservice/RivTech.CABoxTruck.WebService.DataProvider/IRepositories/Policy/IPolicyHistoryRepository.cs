﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IPolicyHistoryRepository : IRepository<PolicyHistory, long>
    {
        Task<PolicyHistory> GetLatestByRisk(Guid riskId);
        Task<PolicyHistory> GetPreviousHistory(long id);
        Task<PolicyHistory> GetLastActive(Guid riskId);
        Task<IEnumerable<PolicyHistory>> GetByRiskDetailAsync(Guid riskDetailId);
    }
}
