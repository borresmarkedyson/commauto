﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IRelatedEntityTypeRepository : IRepository<LvRelatedEntityType, byte>
    {
    }
}
