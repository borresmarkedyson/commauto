﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IAppLogRepository : IRepository<Data.Entity.AppLog, long>
    {
    }
}
