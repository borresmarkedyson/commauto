﻿
using RivTech.CABoxTruck.WebService.ExternalData.Entity;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IEmailQueueArchiveRepository : IRepository<EmailQueueArchive, Guid>
    {
    }
}
