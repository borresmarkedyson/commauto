﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface ILvLimitsRepository : IRepository<LvLimits, int>
    {
    }
}

