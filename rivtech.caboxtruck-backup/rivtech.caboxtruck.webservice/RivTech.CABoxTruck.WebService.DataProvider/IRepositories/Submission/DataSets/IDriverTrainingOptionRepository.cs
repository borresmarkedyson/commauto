﻿using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.DataSets
{
    public interface IDriverTrainingOptionRepository : IRepository<DriverTrainingOption, short>
    {
    }
}
