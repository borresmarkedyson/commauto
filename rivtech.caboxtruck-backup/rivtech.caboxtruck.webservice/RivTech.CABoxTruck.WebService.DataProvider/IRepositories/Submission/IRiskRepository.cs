﻿using RivTech.CABoxTruck.WebService.Data.Entity.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Submission;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission
{
    public interface IRiskRepository : IRepository<Risk, Guid>
    {
        IEnumerable<RiskSummaryDto> GetFilteredSubmissionSummaryListAsync(SubmissionFilterDto filter);
        Task<Risk> GetRiskByPolicyNumberAsync(string policyNumber);
        Task<List<Guid>> GetAllRiskIdForCancellationAsync(DateTime pendingCancellationDate);
        Task<List<Risk>> GetAllRiskIdForPendingCancellationAsync();
        Task<Risk> GetRiskByIdAsync(Guid id);
    }
}
