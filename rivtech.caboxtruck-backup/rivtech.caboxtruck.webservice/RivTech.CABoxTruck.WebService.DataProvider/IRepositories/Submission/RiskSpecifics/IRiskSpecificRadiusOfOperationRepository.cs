﻿using RivTech.CABoxTruck.WebService.Data.Entity.Submission.Risk_Specifics;
using System;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.RiskSpecifics
{
    public interface IRiskSpecificRadiusOfOperationRepository : IRepository<RiskSpecificRadiusOfOperation, Guid>
    {
    }
}
