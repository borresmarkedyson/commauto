﻿using RivTech.CABoxTruck.WebService.Data.Entity.Submission.Risk_Specifics;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.RiskSpecifics
{
    public interface IRiskSpecificSafetyDeviceRepository : IRepository<RiskSpecificSafetyDevice, Int16>
    {
    }
}
