﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DataProvider.IRepositories
{
    public interface IBrokerInfoRepository : IRepository<BrokerInfo, Guid>
    {
        Task<BrokerInfo> GetByRiskIdAllAsync(Guid id);
        Task<BrokerInfo> GetByRiskIdAllIncludeAsync(Guid id);
    }
}
