﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class ApplicantPersonnelDTO
    {
        public List<SaveEntityContactDTO> Contacts { get; set; }
    }
}

