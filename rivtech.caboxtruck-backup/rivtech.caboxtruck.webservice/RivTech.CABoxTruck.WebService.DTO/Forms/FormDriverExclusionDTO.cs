﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.WebService.DTO.Forms
{
    public class FormDriverExclusionDTO
    {
        public List<FormDriverDTO> Drivers { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}