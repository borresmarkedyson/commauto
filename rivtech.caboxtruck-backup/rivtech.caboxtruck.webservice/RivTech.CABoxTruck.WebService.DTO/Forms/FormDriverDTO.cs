﻿namespace RivTech.CABoxTruck.WebService.DTO.Forms
{
    public class FormDriverDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LicenseNumber { get; set; }
    }
}