﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Forms
{
    public class FormBaseDTO : IDialogUpdate
    {
        public DateTime? UpdatedDate { get; set; }
    }
}