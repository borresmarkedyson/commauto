﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public interface IDialogUpdate
    {
        DateTime? UpdatedDate { get; set; }
    }
}