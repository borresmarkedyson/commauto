﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Forms
{
    public class FormUIIADTO
    {
        public short? AutoBILimitId { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}