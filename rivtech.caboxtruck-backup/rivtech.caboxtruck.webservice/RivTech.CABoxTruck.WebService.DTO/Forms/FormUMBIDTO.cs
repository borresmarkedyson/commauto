﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Forms
{
    public class FormUMBIDTO
    {
        public short? UMBILimitId { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}