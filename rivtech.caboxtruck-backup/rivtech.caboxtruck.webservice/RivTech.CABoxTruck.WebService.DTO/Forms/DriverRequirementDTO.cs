﻿namespace RivTech.CABoxTruck.WebService.DTO
{
    public class DriverRequirementDTO
    {
        public string Requirement { get; set; }
    }
}