﻿namespace RivTech.CABoxTruck.WebService.DTO
{
    public class ExclusionRadiusDTO
    {
        public int Radius { get; set; }
    }
}