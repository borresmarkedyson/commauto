﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.WebService.DTO.Report.Filters
{
    public class ReportManagementFilter : FilterBase
    {
        public DateTime? DateMin { get; set; }
        public DateTime? DateMax { get; set; }
        public string ReportName { get; set; }
    }
}