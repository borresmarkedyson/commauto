﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Common;
using RivTech.CABoxTruck.WebService.Data.DALDapperModels;
using RivTech.CABoxTruck.WebService.DTO.Report;
using System.Globalization;

namespace RivTech.CABoxTruck.WebService.DTO.DTOMapping
{
    public class ReportMapperProfile : Profile
    {
        public ReportMapperProfile()
        {
            CreateMap<DMVReportModel, DMVReportTemplate>()
                .ForMember(d => d.ErrorCorrection, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.Agent, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.IDState, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.InsuredDateOfBirth, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.InsuredFirstName, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.InsuredMiddleName, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.InsuredGender, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.ZipCode4N, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.GarageAddress, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.GarageCity, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.GarageState, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.GarageZip5N, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.GarageZip4N, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.LicensePlate, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.HireIndicator, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.HireCancellationReasons, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.DeductibleCollision, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.DeductibleComprehensive, opt => opt.MapFrom(src => string.Empty))
                .ForMember(d => d.PriorPolicyNumber, opt => opt.MapFrom(src => string.Empty))

                .ForMember(d => d.IndividualLimit2, opt => opt.MapFrom(src => src.IndividualLimit2.Replace("$", string.Empty)))
                .ForMember(d => d.OccurrenceLimit2, opt => opt.MapFrom(src => src.OccurrenceLimit2.Replace("$", string.Empty)))
                .ForMember(d => d.CSLLimit1, opt => opt.MapFrom(src =>src.CSLLimit1.ToString("#,#", CultureInfo.InvariantCulture)))
                .ForMember(d => d.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.ToString("MM/dd/yyyy")))
                .ForMember(d => d.ExpirationDate, opt => opt.MapFrom(src => src.ExpirationDate.ToString("MM/dd/yyyy")))
                .ForMember(d => d.TransactionEffectiveDate, opt => opt.MapFrom(src => src.TransactionEffectiveDate.ToString("MM/dd/yyyy")))
                .ForMember(d => d.VehicleEffectiveDate, opt => opt.MapFrom(src => src.VehicleEffectiveDate.ToString("MM/dd/yyyy")))
                .ForMember(d => d.VehicleExpirationDate, opt => opt.MapFrom(src => src.VehicleExpirationDate.ToString("MM/dd/yyyy")));
        }
    }
}
