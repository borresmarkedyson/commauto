﻿namespace RivTech.CABoxTruck.WebService.DTO.Submission.RiskSpecifics
{
    public class RiskSpecificDriverInfoPageDto
    {
        public RiskSpecificDriverInfoDto DriverInfo { get; set; }
        public RiskSpecificDriverHiringCriteriaDto DriverHiringCriteria { get; set; }
    }
}
