﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Submission
{
    public class SubmissionFilterDto
    {
        public string SubmissionNumber { get; set; }
        public string InsuredName { get; set; }
        public string Broker { get; set; }
        public DateTime? InceptionDateFrom { get; set; }
        public DateTime? InceptionDateTo { get; set; }
        public string Status { get; set; }
        public string PolicyStatus { get; set; }
        public string Underwriter { get; set; }
        public string Au_rep { get; set; }
        public string Au_psr { get; set; }
        public string Owner { get; set; }
        public string State { get; set; }
        public string SearchText { get; set; }
    }
}
