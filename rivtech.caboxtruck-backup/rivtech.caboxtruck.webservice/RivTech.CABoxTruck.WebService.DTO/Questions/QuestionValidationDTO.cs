﻿using RivTech.CABoxTruck.WebService.Data.Entity;

namespace RivTech.CABoxTruck.WebService.DTO.Questions
{
    public class QuestionValidationDTO
    {
        //public QuestionDTO Question { get; set; }
        public ValidationDTO Validation { get; set; }
    }
}
