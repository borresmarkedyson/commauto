﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.WebService.DTO.Questions
{
    public class ValidationDTO
    {
        public Int16 ValidationTypeId { get; set; }
        public string Value { get; set; }
    }
}
