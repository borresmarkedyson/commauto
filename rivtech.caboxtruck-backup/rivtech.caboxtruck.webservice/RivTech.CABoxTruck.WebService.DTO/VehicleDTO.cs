﻿using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DTO.Vehicles;
using System;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class VehicleDTO
    {
        public Guid? Id { get; set; }
        public Guid? RiskDetailId { get; set; }

        public bool? isEndorsement { get; set; } = null;
        public short? Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VIN { get; set; }
        public string Style { get; set; }
        public string VehicleType { get; set; }
        public short? GrossVehicleWeightId { get; set; }
        public short? VehicleDescriptionId { get; set; }
        public string Description { get; set; }
        public short? VehicleBusinessClassId { get; set; }
        public string BusinessClass { get; set; }

        public bool? IsAutoLiability { get; set; }
        public bool? IsCoverageFireTheft { get; set; }
        public short? CoverageFireTheftDeductibleId { get; set; }

        public bool? IsCoverageCollision { get; set; }
        public short? CoverageCollisionDeductibleId { get; set; }

        public decimal StatedAmount { get; set; }
        public bool? HasCoverageCargo { get; set; }
        public bool? HasCoverageRefrigeration { get; set; }
        public short? UseClassId { get; set; }
        public Guid? GaragingAddressId { get; set; }
        public string PrimaryOperatingZipCode { get; set; }
        public string State { get; set; }
        public string RegisteredState { get; set; }
        public string StateFull { get; set; }
        public string Options { get; set; }

        public decimal? Radius { get; set; }
        public decimal? RadiusTerritory { get; set; }

        public decimal? AutoLiabilityPremium { get; set; }
        public decimal? CargoPremium { get; set; }
        public decimal? RefrigerationPremium { get; set; }
        public decimal? PhysicalDamagePremium { get; set; }
        public decimal? ComprehensivePremium { get; set; }
        public decimal? CollisionPremium { get; set; }
        public decimal? TotalPremium { get; set; }
        public decimal? ProratedPremium { get; set; }

        public string PolicyNumber { get; set; }

        //Excel file upload mapper
        public string GrossVehicleWeight { get; set; }
        public string CoverageCollisionDeductible { get; set; }

        public Guid? PreviousVehicleVersionId { get; set; }

        public Int16? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public bool? IsActive { get; set; }
        public DateTime? DeletedDate { get; set; }

        public bool? IsValidated { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool? CanReinstate { get; set; }
        public VehiclePremium LatestVehiclePremium { get; set; }
    }
}
