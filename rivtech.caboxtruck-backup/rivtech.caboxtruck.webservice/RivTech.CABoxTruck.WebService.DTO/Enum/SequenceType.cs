﻿namespace RivTech.CABoxTruck.WebService.DTO.Enum
{
    public enum SequenceType
    {
        NextPolicyNumber,
        NextSubmissionNumber,
        NextQuoteNumber
    }
}
