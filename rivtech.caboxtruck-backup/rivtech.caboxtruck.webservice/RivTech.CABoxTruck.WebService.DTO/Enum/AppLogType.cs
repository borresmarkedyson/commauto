﻿namespace RivTech.CABoxTruck.WebService.DTO.Enum
{
    public enum AppLogType
    {
        None = 0,
        Create = 1,
        Update = 2,
        Delete = 3,
        DocGen = 4
    }
}
