﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class InsuredDTO
    {
        public Guid Id { get; set; }
        public Guid EntityId { get; set; }
    }
}
