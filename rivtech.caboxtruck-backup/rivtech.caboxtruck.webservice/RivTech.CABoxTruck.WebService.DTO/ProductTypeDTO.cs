﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class ProductTypeDTO
    {
        public string Description { get; set; }
    }
}
