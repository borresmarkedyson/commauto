﻿namespace RivTech.CABoxTruck.WebService.DTO.Dashboard
{
    public class DashboardCountDto
    {
        public int SubmissionCount { get; set; }
        public int CompleteCount { get; set; }
        public int InResearchCount { get; set; }
        public int InCompleteCount { get; set; }
        public int QuotedCount { get; set; }
        public int ReceivedCount { get; set; }
        public int WithdrawnCount { get; set; }
        public int ActivePoliciesCount { get; set; }
        public int CancelledPoliciesCount { get; set; }
        public int PendingCancellationCount { get; set; }
        public int DeclinedCount { get; set; }
        
    }
}
