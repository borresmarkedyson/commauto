﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Dashboard
{
    public class DashboardPolicySearchDTO
    {
        public Guid RiskId { get; set; }
        public Guid LatestRiskDetailId { get; set; }
    }
}
