﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class RiskTypeDTO
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
