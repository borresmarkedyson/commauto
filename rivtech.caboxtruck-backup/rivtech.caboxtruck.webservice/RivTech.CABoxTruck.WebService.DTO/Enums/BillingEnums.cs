﻿namespace RivTech.CABoxTruck.WebService.DTO.Enums
{
    public enum InvoiceType
    {
        Deposit,
        Installment,
        Endorsement,
        Cancellation
    }
}