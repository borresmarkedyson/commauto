﻿namespace RivTech.CABoxTruck.WebService.DTO.User
{
    public class LoginDto
    {
        public string Username { get; set; }
    }
}
