﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class RetroDateTypeDTO
    {
        public byte Id { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
