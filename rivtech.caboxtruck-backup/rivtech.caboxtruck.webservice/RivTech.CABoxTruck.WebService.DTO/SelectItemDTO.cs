﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class SelectItemDTO
    {
        public Int16? Value { get; set; }
        public string Label { get; set; }
    }
}
