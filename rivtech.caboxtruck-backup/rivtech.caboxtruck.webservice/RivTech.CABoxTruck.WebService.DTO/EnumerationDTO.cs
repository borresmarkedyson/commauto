﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class EnumerationDTO
    {
        public short Id { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
