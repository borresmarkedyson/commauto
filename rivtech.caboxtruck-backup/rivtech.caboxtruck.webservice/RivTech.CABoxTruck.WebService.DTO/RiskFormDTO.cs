﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO
{
    public class RiskFormDTO
    {
        public Guid? Id { get; set; }
        public Guid RiskDetailId { get; set; }
        public string FormId { get; set; }
        public bool? IsSelected { get; set; }
        public string Other { get; set; }

        public FormDTO Form { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool? IsUserUploaded { get; set; }
    }

    public class FormDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string FormNumber { get; set; }
        public bool? IsMandatory { get; set; }
        public bool? IsDefault { get; set; }
        public string FileName { get; set; }
        public bool IsSupplementalDocument { get; set; }
    }
}