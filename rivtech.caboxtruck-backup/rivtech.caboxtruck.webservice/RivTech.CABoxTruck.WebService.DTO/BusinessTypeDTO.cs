﻿namespace RivTech.CABoxTruck.WebService.DTO
{
    public class BusinessTypeDTO
    {
        public byte Id { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
