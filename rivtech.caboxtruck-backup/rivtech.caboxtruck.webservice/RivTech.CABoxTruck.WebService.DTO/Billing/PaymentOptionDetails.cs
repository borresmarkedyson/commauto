﻿using System;

namespace RivTech.CABoxTruck.WebService.DTO.Billing
{
    public class PaymentOptionDetails
    {
        public Guid Id { get; }

        public PaymentOptionDetails(PolicyTotals policyTotals, decimal depositPremium, decimal taxRate, int installmentCount = 10)
        {
            PolicyTotals = policyTotals;
            DepositPremium = depositPremium;

            if(depositPremium > 0 && installmentCount > 0)
                InstallmentDetails = new InstallmentDetails(policyTotals.Premium, DepositPremium, installmentCount, taxRate);
        }

        public decimal DepositAmount => DepositPremium + PolicyTotals.Fee + PolicyTotals.Tax;

        public decimal DepositPremium { get; }
        public InstallmentDetails InstallmentDetails { get; }
        public PolicyTotals PolicyTotals { get; }
        public decimal TotalWithInstallments => DepositAmount + InstallmentDetails?.TotalDue ?? 0;
    }
}
