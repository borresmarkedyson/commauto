﻿namespace RivTech.CABoxTruck.WebService.DTO.Billing
{
    public interface IAmount
    {
        decimal Value { get; }
    }
}
