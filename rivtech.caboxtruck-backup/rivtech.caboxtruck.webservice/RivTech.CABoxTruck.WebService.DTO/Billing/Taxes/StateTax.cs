﻿using RivTech.CABoxTruck.WebService.DTO.Billing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.WebService.DTO.Billing.Taxes
{
    public class StateTax
    {
        public StateTax(string stateCode, IEnumerable<SurplusLineTax> slTaxes, StampingFeeTax stampingFee = null)
        {
            StateCode = stateCode;
            SLTaxes = slTaxes;
            StampingFeeTax = stampingFee;
        }

        public string StateCode { get; }
        public IEnumerable<SurplusLineTax> SLTaxes { get; }
        public StampingFeeTax StampingFeeTax { get; }
        public decimal TotalRate => SLTaxes.Sum(x => x.Rate) + (StampingFeeTax?.Rate ?? 0);

    }
}
