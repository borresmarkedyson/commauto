﻿using RivTech.CABoxTruck.WebService.DTO.Billing.Taxes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.DTO.Billing
{
    public class BillingComputeRiskTaxResponseDTO
    {
        public TaxDetailsDTO TaxDetails { get; set; }
    }
}
