import {Component, OnInit, ViewChild, AfterViewInit, AfterContentInit, Input, ViewEncapsulation} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {NavigationService} from '../../../core/services/navigation/navigation.service';
import {LayoutService} from '../../../core/services/layout/layout.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../core/services/auth.service';
import {BlacklistedDriverModalComponent} from '@app/modules/blacklisted-driver/components/blacklisted-driver-modal/blacklisted-driver-modal.component';
import {AddDriverVerificationModalComponent} from '@app/modules/blacklisted-driver/components/blacklisted-driver-modal/add-blacklisted-driver-modal/add-driver-verification-modal.component';
import { AccountsReceivableComponent } from '@app/modules/dashboard/components/reports/accounts-receivable/accounts-receivable.component';
import { DMVModalComponent } from '@app/modules/dashboard/components/reports/dmv/dmv-modal.component';

@Component({
  selector: 'app-header-admin',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None // rolan use to take effect on css and override the unmodified css.
})
export class HeaderComponent implements OnInit {

  @Input() leftSidebarAvailable: boolean = true;

  uname: string;
  currentUserType: string = 'exernal';

  modalRef: BsModalRef | null;

  constructor(public layoutService: LayoutService, public navService: NavigationService, private authService: AuthService, private router: Router,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.uname = localStorage.getItem('uname');
  }

  logout() {
    this.authService.logUserLogout(this.uname).subscribe(() => {
      return;
    });
    this.authService.logout();
    this.router.navigate(['login']);
  }

  onToolClick(obj) {
    obj.preventDefault();

    switch (obj.target.innerText) {
      case 'Driver Verification':
        this.onDriverVerificationClick();
        break;
      case 'Accounts Receivable':
        this.showAccountsReceivableDialog();
        break;
        case 'Generate DMV Report':
          this.showDMVDialog();
          break;
    }
  }

  onDriverVerificationClick(): void {
    const initialState = {
      title: 'Search Blacklisted Drivers',
    };
    this.modalRef = this.modalService.show(BlacklistedDriverModalComponent, {
      initialState,
      class: 'modal-xl modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
    });

    this.modalRef.content.showAnotherModal.subscribe((data) => {
      this.showAddDriverModal(data);
    });
  }

  showAccountsReceivableDialog(): void {
    const initialState = {
      title: 'Accounts Receivable Report'
    };

    this.modalRef = this.modalService.show(AccountsReceivableComponent, {
      initialState,
      class: 'modal-md modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true
    });
  }

  showDMVDialog(): void {
    const initialState = {
      title: 'DMV Report'
    };

    this.modalRef = this.modalService.show(DMVModalComponent, {
      initialState,
      class: 'modal-md modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true
    });
  }

  showAddDriverModal(initialState: any) {
    this.modalRef = this.modalService.show(AddDriverVerificationModalComponent, {
      initialState,
      class: 'modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true
    });

    this.modalRef.content.onClose.subscribe((data) => {
      this.onDriverVerificationClick();
    });
  }
}
