
export enum PreloadingStatus {
	InProgress = 1,
	Complete = 2, 
	Failed = 3
}