import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LayoutService } from '@app/core/services/layout/layout.service';
import { RaterApiData } from '@app/modules/submission/data/rater-api.data';
import { SubmissionData } from '@app/modules/submission/data/submission.data';
import { RiskSpecificsData } from '@app/modules/submission/data/risk-specifics.data';
import { BindIssueService } from '@app/modules/submission/services/bind-issue.service';
import { EndorsementService } from '@app/modules/submission/services/endorsement.service';
import { BindAndIssueLabelConstants } from '@app/shared/constants/bind-and-issue.labels.constants';
import { ErrorMessageConstant } from '@app/shared/constants/error-message.constants';
import { PathConstants } from '@app/shared/constants/path.constants';
import FormUtils from '@app/shared/utilities/form.utils';
import NotifUtils from '@app/shared/utilities/notif-utils';
import Utils from '@app/shared/utilities/utils';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, finalize, switchMap, take, tap } from 'rxjs/operators';
import { createPolicyDetailsMenuItems } from '../components/policy-details/policy-details-navitems';
import { PolicySummaryData } from './policy-summary.data';

@Injectable(
    { providedIn: 'root' }
  )
export class PolicyIssuanceData {

    issuanceForm: FormGroup;
    datepickerOptions: IAngularMyDpOptions;
    policyChanges: string[] = [];
    enableIssueAndResetButtons: boolean = false;
    premiumChangeList: any;
    isNewlyIssued: boolean = false;
    isLoadingChanges: boolean = false;

    constructor(private formBuilder: FormBuilder,
                private submissionData: SubmissionData,
                private bindIssueService: BindIssueService,
                private router: Router,
                private toastr: ToastrService,
                private raterApiData: RaterApiData,
                private endorsementPremiumService: EndorsementService,
                private policySummaryData: PolicySummaryData,
                private riskSpecificsData: RiskSpecificsData,
                private layoutService: LayoutService) {}

    initiateFormFields() {
      this.isNewlyIssued = false;
      this.isLoadingChanges = false;
      this.policyChanges = [];
      const effectiveDate = this.submissionData.riskDetail?.prevEndorsementEffectiveDate ?? this.submissionData.riskDetail?.brokerInfo?.effectiveDate;
      const policyEffectiveDate = moment(effectiveDate).subtract(1, 'day').toDate();
      const policyExpirationDate = moment(this.submissionData.riskDetail?.brokerInfo?.expirationDate).add(1, 'day').toDate();
      this.datepickerOptions = {
          dateRange: false,
          dateFormat: 'mm/dd/yyyy',
          disableUntil: { year: policyEffectiveDate.getFullYear() , month: policyEffectiveDate.getMonth() + 1, day: policyEffectiveDate.getDate() },
          disableSince: { year: policyExpirationDate.getFullYear() , month: policyExpirationDate.getMonth() + 1, day: policyExpirationDate.getDate() },
      };

      this.issuanceForm = this.formBuilder.group({
          effectiveDate: [null, Validators.required],
          policyChanges: [null, Validators.required],
          endorsementText: [null, Validators.required]
      });

      return this.issuanceForm;
    }

    populateFormFields() {
        console.info("Populating issuance...", {
          effectiveDte: this.submissionData?.riskDetail?.endorsementEffectiveDate
        });
        this.issuanceForm.controls['endorsementText'].patchValue(this.policyChanges.join('\n'));
        this.issuanceForm.get('effectiveDate').setValue({ isRange: false, singleDate: { jsDate: new Date(this.submissionData?.riskDetail?.endorsementEffectiveDate ?? new Date(this.submissionData.currentServerDateTime)) } });
        this.issuanceForm.get('policyChanges').setValue(JSON.stringify(this.policyChanges));
        this.enableIssueAndResetButtons = this.policyChanges.length > 0 && this.validateRequiredToRateModules();
    }

    resetFormFields() {
        this.issuanceForm.controls['endorsementText'].patchValue(null);
        this.issuanceForm.get('effectiveDate').setValue({ isRange: false, singleDate: { jsDate: new Date(this.submissionData.currentServerDateTime) } });
        this.issuanceForm.get('policyChanges').setValue(null);
        this.enableIssueAndResetButtons = false;
        this.policyChanges = [];
    }

    getPolicyChanges(): Observable<any> {
        return this.bindIssueService.GetPolicyChanges(this.submissionData.riskDetail.id).pipe(
          tap((data) => {
            this.policyChanges = data;
          }),
          catchError((error) => {
            console.error(error);
            NotifUtils.showError(ErrorMessageConstant.getPolicyChangesErrorMsg);
            return throwError(error);
          })
        );
    }

    IssueEndorsement() {
      Utils.blockUI();
      const policyChanges = {
          riskDetailId: this.submissionData.riskDetail.id,
          effectiveDate: this.issuanceForm.get('effectiveDate').value?.singleDate?.jsDate?.toLocaleDateString(),
          endorsementText: this.issuanceForm.value.endorsementText,
          policyChanges: this.issuanceForm.value.policyChanges
      };
      this.bindIssueService.IssueEndorsement(policyChanges).pipe(take(1)).subscribe(data => {
        NotifUtils.showSuccessWithConfirmationFromUser(BindAndIssueLabelConstants.issuedSuccessfully, () => {
          this.policySummaryData.canEditPolicy = false;
          this.isNewlyIssued = true;
          this.policySummaryData.updateEstimatedPremium(true);
          this.resetFormFields();
          this.router.navigateByUrl(`/policies/${this.submissionData.riskDetail.riskId}/${data.id}/${PathConstants.Policy.Summary}`);
        });
      }, (error) => {
        console.error(error);
        NotifUtils.showError(ErrorMessageConstant.issuePolicyErrorMsg);
      });
    }

    ResetPolicy() {
      NotifUtils.showConfirmMessage(BindAndIssueLabelConstants.areYouSureYouWantToReset, () => {
        Utils.blockUI();
        this.bindIssueService.Reset(this.submissionData.riskDetail.id).pipe(take(1)).subscribe(data => {
          NotifUtils.showSuccessWithConfirmationFromUser(BindAndIssueLabelConstants.resetSuccessfully, () => {
            this.submissionData.getRiskByIdAsync(this.submissionData.riskDetail.id).pipe(
              take(1),
              finalize(() => this.loadPolicyAndPremiumChanges(Number(this.submissionData.riskDetail?.binding?.bindOptionId), false).subscribe()) // to recalculate reverted values
            ).subscribe((submissionData) => {
              // console.log('RiskDetail', submissionData);
              this.submissionData.riskDetail = submissionData;
              this.policySummaryData.name = submissionData?.nameAndAddress?.businessName;
              this.policySummaryData.canEditPolicy = false;
              this.policySummaryData.updateEstimatedPremium(true);
              this.policySummaryData.updatePolicyStatus();
              this.resetFormFields();
              this.layoutService.updateMenu(createPolicyDetailsMenuItems(this.submissionData.riskDetail.riskId, this.submissionData.riskDetail.id, this.submissionData.validationList, this.riskSpecificsData.hiddenNavItems));
            });
          });
        }, (error) => {
          Utils.unblockUI();
          console.error(error);
          NotifUtils.showError(ErrorMessageConstant.resetPolicyErrorMsg);
        });
      });
    }

    validateRequiredToRateModules(): boolean {
      const excludeBindreq = ['applicantNameAndAddressBinding', 'businessDetailsBinding', 'bind', 'brokerBinding'];
      const riskValidationList = Object.keys(this.submissionData.validationList).filter(x => !excludeBindreq.find(r => r === x));
      const invalidModule = riskValidationList
      .find(e => e !== 'uwAnswersConfirmed' && this.submissionData.validationList[e] == false);
      const lstToValidate: Array<any> = [undefined, null];
      return (lstToValidate.indexOf(invalidModule) !== -1);
    }

    getEndorsementPremiumChanges(): Observable<any> {
      return this.endorsementPremiumService.GetEndorsementPremiumChanges(this.submissionData.riskDetail.id).pipe(take(1))
        .pipe(
          tap((data) => this.premiumChangeList = data),
          catchError((error) => {
            console.error(error);
            NotifUtils.showError(ErrorMessageConstant.getPolicyChangesErrorMsg);
            return throwError(error);
          })
        );
    }

    loadPolicyAndPremiumChanges(bindOption?: number, forIssuance?: boolean): Observable<any> {
      if(forIssuance == null)
        forIssuance = true;

      this.premiumChangeList = [];
      this.isLoadingChanges = true;
      if (bindOption === null) { bindOption = Number(this.submissionData?.riskDetail?.binding?.bindOptionId); }

      let changeRequest = forkJoin([
        this.getPolicyChanges(),
        this.getEndorsementPremiumChanges()
      ]);

      const effectiveDate = this.submissionData?.riskDetail?.endorsementEffectiveDate ?? new Date(this.submissionData.currentServerDateTime);

      Utils.blockUIWithMessage("Loading changes...");

      return this.raterApiData.preloadRater(true).pipe(
        switchMap((result) => {
          const riskCov = this.submissionData.riskDetail.riskCoverages.find(coverage => coverage?.optionNumber === bindOption);
          return this.raterApiData.calculate({ covOption: riskCov, effectiveDate: effectiveDate, forIssuance: forIssuance }).pipe(
            catchError((err) => {
              Utils.unblockUI();
              console.error(err);
              return throwError(err);
            })
          );
        }),
        switchMap(() => changeRequest),
        tap(() => Utils.unblockUI()),
        catchError(() => {
          Utils.unblockUI();
          // this.toastr.error("An error occured.");
          return of();
        }),
        finalize(() => {
          this.populateFormFields();
          this.raterApiData.preloadRater(true).subscribe();
        })
      );
    }

    get isFormValid() {
      return FormUtils.validateForm(this.issuanceForm);
    }

    updateEndorsementEffectiveDate(riskDetailId: string, effectiveDate: Date) : Observable<any> {
      return this.endorsementPremiumService.updateEffectiveDate(riskDetailId, effectiveDate)
        .pipe(
          tap((data) => this.submissionData.riskDetail.endorsementEffectiveDate = data)
        );
    }

}
