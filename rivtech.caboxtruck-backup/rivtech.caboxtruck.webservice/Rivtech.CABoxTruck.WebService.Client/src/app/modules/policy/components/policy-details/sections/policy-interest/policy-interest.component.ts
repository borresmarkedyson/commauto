import { Component, OnInit } from '@angular/core';
import { ApplicantData } from '../../../../../../modules/submission/data/applicant/applicant.data';

@Component({
  selector: 'app-policy-interest',
  templateUrl: './policy-interest.component.html',
  styleUrls: ['./policy-interest.component.scss']
})
export class PolicyInterestComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
