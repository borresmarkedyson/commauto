import { Component, Input, OnInit } from '@angular/core';
import { PolicyCancellationData } from '@app/modules/policy/data/policy-cancellation.data';
import { BindAndIssueLabelConstants } from '@app/shared/constants/bind-and-issue.labels.constants';
import NotifUtils from '@app/shared/utilities/notif-utils';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PolicySummaryData } from 'app/modules/policy/data/policy-summary.data';

@Component({
  selector: 'app-policy-cancellation',
  templateUrl: './policy-cancellation.component.html',
  styleUrls: ['./policy-cancellation.component.scss']
})
export class PolicyCancellationComponent implements OnInit {

  @Input() title: string;
  @Input() lastIssuedEffectiveDate: Date;

  constructor(
    public summaryData: PolicySummaryData,
    public policyCancellationData: PolicyCancellationData,
    public modalRef: BsModalRef
  ) {}

  ngOnInit() {
    this.policyCancellationData.lastIssuedEffectiveDate = this.lastIssuedEffectiveDate;
    this.policyCancellationData.initiateFormFields();
  }

  get fg() { return this.policyCancellationData.cancellationForm; }
  get fc() { return this.fg.controls; }
  get fn() { return 'cancellationForm'; }

  onSubmit() {
    this.policyCancellationData.CancelPolicy();
    this.modalRef.hide();
    setTimeout(() => {
      this.summaryData.updatePolicyStatus();
    }, 1000);
  }

  onClose() {
    this.modalRef.hide();
  }
}
