import {Component, OnDestroy, OnInit} from '@angular/core';
import { ApplicantData } from '../../../../../../modules/submission/data/applicant/applicant.data';

import { SubmissionData } from '@app/modules/submission/data/submission.data';
import { NavigationEnd, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { createPolicyDetailsMenuItems } from '../../policy-details-navitems';
import { RiskSpecificsData } from '@app/modules/submission/data/risk-specifics.data';
import { LayoutService } from '@app/core/services/layout/layout.service';
import * as _ from 'lodash';
import { PolicySummaryData } from '@app/modules/policy/data/policy-summary.data';
import { PathConstants } from '@app/shared/constants/path.constants';
import FormUtils from '@app/shared/utilities/form.utils';
import { PolicySummaryHeaderConstants } from '@app/shared/constants/policy-header.constants';

@Component({
  selector: 'app-policy-applicant',
  templateUrl: './policy-applicant.component.html',
  styleUrls: ['./policy-applicant.component.scss']
})
export class PolicyApplicantComponent implements OnInit {

  private reset$ = new Subject();
  private newCategoryRoute: string;
  private currentCategoryRoute: string;

  constructor(private applicantData: ApplicantData,
    private router: Router,
    private layoutService: LayoutService,
    private submissionData: SubmissionData,
    private riskSpecificsData: RiskSpecificsData,
    private policySummaryData: PolicySummaryData) { }

  ngOnInit() {
    this.applicantData.initiateFormFields();
    this.currentCategoryRoute = this.getCurrentCategoryRoute();
    this.savingUponNavigate();
    this.applicantData.populateNameAndAddressFields();
    this.applicantData.populateBusinessDetailsFields();
    this.applicantData.populateFilingsInformationFields();
    this.applicantData.applicantForms.nameAndAddressForm.markAllAsTouched();
  }

  savingUponNavigate() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .pipe(takeUntil(this.reset$)).subscribe((event: NavigationEnd) => {
        this.newCategoryRoute = this.getCurrentCategoryRoute();
        if (['dashboard', 'login'].includes(this.newCategoryRoute, 0)) {
          return;
        }
        // console.log(this.newCategoryRoute);
        if (this.currentCategoryRoute === PathConstants.Submission.Applicant.NameAndAddress) {
          if (this.policySummaryData.canEditPolicy) {
            this.applicantData.updateNameAndAddressMidterm();
            this.policySummaryData.updatePendingEndorsement();
          }
          this.submissionData.validationList.applicantNameAndAddress = this.checkNameAndAddressSection;
          this.layoutService.updateMenu(createPolicyDetailsMenuItems(this.submissionData.riskDetail.riskId, this.submissionData.riskDetail.id, this.submissionData.validationList, this.riskSpecificsData.hiddenNavItems));
        }
        this.currentCategoryRoute = this.newCategoryRoute;
      });
  }

  private getCurrentCategoryRoute(): string {
    const splitUrl = _.split(this.router.url, '/');
    const currentCategoryIndex = splitUrl.length - 1;
    return splitUrl[currentCategoryIndex];
  }

  get checkNameAndAddressSection() {
    this.applicantData.applicantForms.nameAndAddressForm.markAllAsTouched();
    return FormUtils.validateForm(this.applicantData.applicantForms.nameAndAddressForm);
  }

}
