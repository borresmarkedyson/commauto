import { Component, OnInit } from '@angular/core';
import { ApplicantData } from '../../../../../../../modules/submission/data/applicant/applicant.data';
import { ClickTypes } from '../../../../../../../shared/constants/click-type.constants';
import { NavigationService } from '../../../../../../../core/services/navigation/navigation.service';
import { PathConstants } from '@app/shared/constants/path.constants';

@Component({
  selector: 'app-name-and-address',
  templateUrl: './name-and-address.component.html',
  styleUrls: ['./name-and-address.component.scss']
})
export class NameAndAddressComponent implements OnInit {
  tempriskHeaderId: any;

  constructor(private navigationService: NavigationService) { }

  ngOnInit() {
  }

  public onClick(clickType?) {
    switch (clickType) {
      case ClickTypes.Back:
        this.navigationService.navigatePolicyRoute(PathConstants.Policy.Billing);
        break;
      case ClickTypes.Next:
        this.navigationService.navigatePolicyRoute(`${PathConstants.Submission.Applicant.Index}/${PathConstants.Submission.Applicant.BusinessDetails}`);
        break;
    }
  }
}
