import { Component, HostListener, OnInit } from '@angular/core';
import { SubmissionData } from '../../../../../modules/submission/data/submission.data';
import { PolicySummaryData } from '../../../../../modules/policy/data/policy-summary.data';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PolicyCancellationComponent } from './policy-header-modals/policy-cancellation/policy-cancellation.component';
import { PolicyHistoryData } from '@app/modules/policy/data/policy-history.data';
import { ToastrService } from 'ngx-toastr';
import { ErrorMessageConstant } from '../../../../../shared/constants/error-message.constants';
import { PolicyHistoryDTO } from '@app/shared/models/policy/PolicyHistoryDto';
import { PolicySummaryHeaderConstants } from '@app/shared/constants/policy-header.constants';

@Component({
  selector: 'app-policy-summary',
  templateUrl: './policy-summary.component.html',
  styleUrls: ['./policy-summary.component.scss']
})
export class PolicySummaryComponent implements OnInit {

  isOpen: boolean = true;
  isShow: boolean = false;
  isCalcPremium: boolean = false;
  modalRef: BsModalRef | null;
  public pageHeaderConstants = PolicySummaryHeaderConstants;
  constructor(
    public summaryData: PolicySummaryData,
    public submissionData: SubmissionData,
    public modalService: BsModalService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    const risk = this.submissionData.riskDetail;
    if (risk?.id) {
      // this.summaryData.form.patchValue({status: risk.status, assignedToId: risk.assignedToId});
      this.summaryData.updateEstimatedPremium(true);
    }
  }

  collapse() {
    this.isOpen = !this.isOpen;
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event: any) {
    const scrollPosition = window.scrollY;
    if (scrollPosition >= 94) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  onStatusChanged() {
    const formValue = this.summaryData.form.value;
    this.summaryData.quoteStatus = formValue.status;
    this.summaryData.updateStatus(formValue.status, formValue.assignedToId);
  }

  showManualCancellationModal(): void {
    const initialState = {
      title: 'Policy Cancellation',
      lastIssuedEffectiveDate: this.summaryData.lastIssuedEffectiveDate
    };

    this.modalRef = this.modalService.show(PolicyCancellationComponent, {
      initialState,
      class: 'modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
    });
  }

  reinstatePolicy() {
    this.submissionData.ReinstatePolicy();
    setTimeout(() => {
      this.summaryData.updatePolicyStatus();
    }, 1000);
  }

  rewritePolicy() {
    if (this.summaryData.disableRewriteAction) { return; }
    this.submissionData.RewritePolicy();
    // let endorsementCount = 0;
    // this.policyHistoryData.getEndorsementHistory().subscribe((data) => {
    //   endorsementCount = data.length;
    //   if (endorsementCount === 1) {
    //     this.submissionData.RewritePolicy();
    //   } else {
    //     this.toastr.error(ErrorMessageConstant.rewritePolicyNotAllowed, 'Failed!');
    //   }
    // });
  }
}
