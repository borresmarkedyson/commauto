import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PolicyIssuanceRoutingModule } from './policy-issuance-routing.module';
import { PolicyIssuanceComponent } from './policy-issuance.component';
import { PolicyChangesSectionComponent } from './policy-changes-section/policy-changes-section.component';
import { SharedModule } from '@app/shared/shared.module';
import { PremiumChangesComponent } from './premium-changes/premium-changes.component';


@NgModule({
  declarations: [PolicyIssuanceComponent, PolicyChangesSectionComponent, PremiumChangesComponent],
  imports: [
    CommonModule,
    PolicyIssuanceRoutingModule,
    SharedModule
  ]
})
export class PolicyIssuanceModule { }
