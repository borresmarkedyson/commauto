import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PolicyBindRequestComponent } from './policy-bind-request.component';


const routes: Routes = [
  {
    path: '',
    component: PolicyBindRequestComponent,
    // canDeactivate: [CanDeactivateBrokerComponentGuard]
  },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PolicyBindRequestRoutingModule { }
