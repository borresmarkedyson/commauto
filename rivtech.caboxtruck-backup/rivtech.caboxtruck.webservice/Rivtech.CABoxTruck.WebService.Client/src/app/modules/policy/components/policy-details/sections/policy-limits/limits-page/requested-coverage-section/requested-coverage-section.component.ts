import { Component, OnInit } from '@angular/core';
import { LimitsService } from '../../../../../../../../modules/submission/services/coverage-limits.service';
import { LimitsData } from '../../../../../../../../modules/submission/data/limits/limits.data';
import { SubmissionData } from '../../../../../../../../modules/submission/data/submission.data';
import { ApplicantData } from '../../../../../../../../modules/submission/data/applicant/applicant.data';
import { PolicySummaryData } from '../../../../../../../../modules/policy/data/policy-summary.data';

@Component({
  selector: 'app-requested-coverage-section',
  templateUrl: './requested-coverage-section.component.html',
  styleUrls: ['./requested-coverage-section.component.scss']
})
export class RequestedCoverageSectionComponent implements OnInit {
  hideMe: boolean = false;
  constructor(
    public applicantData: ApplicantData,
    public limitsData: LimitsData,
    public submissionData: SubmissionData,
    public coverageLimitService: LimitsService,
    public policySummaryData: PolicySummaryData
  ) { }

  get f() { return this.limitsData.limitsForm.limitsPageForm.controls; }

  ngOnInit() {
    this.limitsData.initiateFormFields();
    this.getCoverageLimitAll();
    this.limitsData.retrieveDropDownValues(true);
    this.limitsData.limitsForm.limitsPageForm.markAllAsTouched();
  }

  getCoverageLimitAll() {
    this.limitsData.populatePolicyLimitsFields();
    // Set defaults
    if (this.submissionData.riskDetail.riskCoverage == null) {
      this.limitsData.setDefaults();
      this.limitsData.hasRiskCoverage = false;
      return;
    }
  }

  public ToggleHiding(): void {
    this.hideMe = !this.hideMe;
  }
}
