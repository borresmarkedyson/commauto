import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { LayoutService } from '@app/core/services/layout/layout.service';
import { NavigationService } from '@app/core/services/navigation/navigation.service';
import { PolicySummaryData } from '@app/modules/policy/data/policy-summary.data';
import { RiskSpecificsData } from '@app/modules/submission/data/risk-specifics.data';
import { SubmissionData } from '@app/modules/submission/data/submission.data';
import { DriverDto, DriverHeaderDto } from '@app/shared/models/submission/Driver/DriverDto';
import { DriverIncidentDto } from '@app/shared/models/submission/Driver/DriverIncidentDto';
import { DriverData } from '../../../../../../modules/submission/data/driver/driver.data';
import { ClickTypes } from '../../../../../../shared/constants/click-type.constants';
import { PathConstants } from '../../../../../../shared/constants/path.constants';
import * as _ from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { createPolicyDetailsMenuItems } from '../../policy-details-navitems';
import { PolicyNavValidateService } from '@app/core/services/navigation/policy-nav-validate.service';

@Component({
  selector: 'app-policy-drivers',
  templateUrl: './policy-drivers.component.html',
  styleUrls: ['./policy-drivers.component.scss']
})
export class PolicyDriversComponent implements OnInit {

  private reset$ = new Subject();
  private newCategoryRoute: string;
  private currentCategoryRoute: string;

  constructor(private navigationService: NavigationService,
              private riskSpecificsData: RiskSpecificsData,
              public driverData: DriverData,
              private router: Router,
              private layoutService: LayoutService,
              private submissionData: SubmissionData,
              private policyNavValidateService: PolicyNavValidateService) { }

  ngOnInit() {
    this.driverData.initiateFormGroup(new DriverDto());
    this.driverData.initiateHeaderFormGroup(new DriverHeaderDto());
    this.driverData.initiateFormIncident(new DriverIncidentDto());
    this.currentCategoryRoute = this.getCurrentCategoryRoute();
    this.savingUponNavigate();
  }

  public onClick(clickType?) {
    switch (clickType) {
      case ClickTypes.Back:
        this.navigationService.navigatePolicyRoute(PathConstants.Submission.Vehicle.Index);
        break;
      case ClickTypes.Next:
        if (this.riskSpecificsData.hiddenNavItems.includes('Historical Coverage') && this.riskSpecificsData.hiddenNavItems.includes('Claims History')) {
          this.navigationService.navigatePolicyRoute(`${PathConstants.Submission.RiskSpecifics.Index}/${PathConstants.Submission.RiskSpecifics.DestinationInfo}`);
        } else {
          this.navigationService.navigatePolicyRoute(PathConstants.Submission.Coverages.Index);
        }
        break;
    }
  }

  savingUponNavigate() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .pipe(takeUntil(this.reset$)).subscribe((event: NavigationEnd) => {
        this.newCategoryRoute = this.getCurrentCategoryRoute();
        if (['dashboard', 'login'].includes(this.newCategoryRoute, 0)) {
          return;
        }
        if (this.currentCategoryRoute === PathConstants.Submission.Driver.Index) {
          this.policyNavValidateService.validateCurrentCategory(PathConstants.Submission.Driver.Index);
          this.layoutService.updateMenu(createPolicyDetailsMenuItems(this.submissionData.riskDetail.riskId, this.submissionData.riskDetail.id, this.submissionData.validationList, this.riskSpecificsData.hiddenNavItems));
        }
        this.currentCategoryRoute = this.newCategoryRoute;
      });
  }

  private getCurrentCategoryRoute(): string {
    const splitUrl = _.split(this.router.url, '/');
    const currentCategoryIndex = splitUrl.length - 1;
    return splitUrl[currentCategoryIndex];
  }

}
