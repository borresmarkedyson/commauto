import { Component, OnInit } from '@angular/core';
import { PolicyBillingLabelsConstants } from '../../../../../../../shared/constants/policy-billing.labels.constants';
import { BaseClass } from '../../../../../../../shared/base-class';
import { PolicyBillingData } from '../../../../../data/policy-billing.data';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PaymentDetailsModalComponent } from './payment-details-modal/payment-details-modal.component';
import { PostPaymentModalComponent } from './post-payments/post-payment-modal/post-payment-modal.component';
import { WriteOffModalComponent } from './write-off-modal/write-off-modal.component';
// import { AdjustmentModalComponent } from './adjustment-modal/adjustment-modal.component';
import { BillingService } from '../../../../../../../../app/core/services/billing/billing.service';
import { PaymentDetailsViewDTO } from '../../../../../../../shared/models/policy/billing/payment-details-view.dto';
import { takeUntil } from 'rxjs/operators';
import NotifUtils from '../../../../../../../shared/utilities/notif-utils';
import { PolicyBillingConstants } from '../../../../../../../shared/constants/policy-billing.constants';
import { PaymentType } from '../../../../../../../shared/enum/payment-type.enum';
import Utils from '../../../../../../../shared/utilities/utils';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent extends BaseClass {
  isOpen: boolean = true;

  public paymentLabelConstants = PolicyBillingLabelsConstants.payments;

  paymentDetailsModalRef: BsModalRef | null;

  public paymentsHeaders: string[] = [
    this.paymentLabelConstants.postDate,
    this.paymentLabelConstants.amount,
    this.paymentLabelConstants.type,
    this.paymentLabelConstants.method,
    this.paymentLabelConstants.referenceNumber,
    this.paymentLabelConstants.comments,
    this.paymentLabelConstants.reversalDate,
    this.paymentLabelConstants.clearDate,
    ''
  ];

  constructor(public billingData: PolicyBillingData,
    private modalService: BsModalService,
    private billingService: BillingService
  ) {
    super();
  }

  viewPaymentDetails(paymentId: string, paymentType: string): void {

    const riskId = this.billingData.data.riskId;

    Utils.blockUI();
    this.billingService.getPaymentDetails(riskId, paymentId).pipe(takeUntil(this.stop$)).subscribe(res => {
      Utils.unblockUI();

      const paymentDetails: PaymentDetailsViewDTO = res;

      let modalTitle: string = PolicyBillingLabelsConstants.paymentDetails.paymentModalTitle;
      if (paymentDetails.paymentMethod === PolicyBillingConstants.paymentMethod.adjustment) {
        modalTitle = PolicyBillingLabelsConstants.paymentDetails.adjustmentModalTitle;
      } else if (paymentDetails.paymentMethod === PolicyBillingConstants.paymentMethod.writeOff) {
        modalTitle = PolicyBillingLabelsConstants.paymentDetails.writeOffModalTitle;
      }

      let isRefund: boolean = false;
      if (paymentType.toLowerCase() === PaymentType.Refund.toLowerCase()) {
        modalTitle = 'Refund Details';
        isRefund = true;
      }

      let isWriteOff: boolean = false;
      if (paymentType.toLowerCase() === PaymentType.WriteOff.toLowerCase()) {
        isWriteOff = true;
      }

      const initialState = {
        title: modalTitle,
        paymentData: paymentDetails,
        isPostPayment: false,
        isRefund: isRefund,
        isWriteOff: isWriteOff
      };

      this.paymentDetailsModalRef = this.modalService.show(PaymentDetailsModalComponent, {
        initialState,
        backdrop: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
      });
    },
      err => {
        NotifUtils.showError(err.error);
      }
    );
  }

  collapse(): void {
    this.isOpen = !this.isOpen;
  }

  onAdjustment(): void {
    // this.paymentDetailsModalRef = this.modalService.show(AdjustmentModalComponent, {
    //   backdrop: true,
    //   ignoreBackdropClick: true,
    //   class: 'modal-md',
    // });
  }

  onWriteOff(): void {
    this.paymentDetailsModalRef = this.modalService.show(WriteOffModalComponent, {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-md',
    });
  }

  onRefund(): void {
    const initialState = {
      isPostPayment: true,
      isRefund: true
    };

    this.paymentDetailsModalRef = this.modalService.show(PostPaymentModalComponent, {
      initialState,
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-md',
    });
  }
}
