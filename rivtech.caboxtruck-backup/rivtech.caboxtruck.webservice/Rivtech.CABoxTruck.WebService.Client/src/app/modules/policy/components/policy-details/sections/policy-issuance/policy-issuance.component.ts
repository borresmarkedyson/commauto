import { PolicyIssuanceData } from '../../../../../../modules/policy/data/policy-issuance.data';
import { Component, OnInit } from '@angular/core';
import { RaterApiData } from '@app/modules/submission/data/rater-api.data';
import { SubmissionData } from '@app/modules/submission/data/submission.data';
import Utils from '@app/shared/utilities/utils';
import NotifUtils from '@app/shared/utilities/notif-utils';
import { RiskSpecificsData } from '@app/modules/submission/data/risk-specifics.data';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs-compat/operator/switchMap';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-policy-issuance',
  templateUrl: './policy-issuance.component.html',
  styleUrls: ['./policy-issuance.component.scss']
})
export class PolicyIssuanceComponent implements OnInit {


  constructor(
    private raterApiData: RaterApiData,
    private submissionData: SubmissionData,
    private policyIssuanceData: PolicyIssuanceData,
    private riskSpecificsData: RiskSpecificsData
  ) { }


  ngOnInit() {
    this.policyIssuanceData.initiateFormFields();
    if (this.submissionData.isCancelledPolicy) {
      // this.policyIssuanceData.getEndorsementPremiumChanges().subscribe(); // should have a version that shows prorated summary
      this.policyIssuanceData.premiumChangeList = []; // remove table values
      return;
    }
    Utils.blockUI();
    if (!this.raterApiData.validateRequiredToRatePremiumModules()) {
      Utils.unblockUI();
      // NotifUtils.showError(this.raterApiData.showRequiredtoRateToolTip);
      return;
    }
    // Rate premium first before getting changes.
    const bindOption = Number(this.submissionData.riskDetail?.binding?.bindOptionId ?? 1);
    this.riskSpecificsData.resetGeneralLiabilityAndCargoIfDisabledInBindOption(bindOption);

    const riskDetail = this.submissionData.riskDetail;
    const effectiveDate = riskDetail.endorsementEffectiveDate;

    // If no initial endorsement effective date yet, 
    // set current date as initial. [changed]
    // Set broker effective date.
    let effectiveDateObservable: Observable<any> = of(effectiveDate);
    if (effectiveDate == null) {
      effectiveDateObservable = this.policyIssuanceData.updateEndorsementEffectiveDate(riskDetail.id, new Date(riskDetail.brokerInfo?.effectiveDate));
    }
    effectiveDateObservable.subscribe(() => {
      this.policyIssuanceData.loadPolicyAndPremiumChanges(bindOption).subscribe();
    });
  }
}
