import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuOverrideService } from '../../../../../../core/services/layout/menu-override.service';
import { ClickTypes } from '../../../../../../shared/constants/click-type.constants';
import { NavigationService } from '../../../../../../core/services/navigation/navigation.service';
import { PathConstants } from '../../../../../../shared/constants/path.constants';
import { ToggleCollapseComponent } from '../../../../../../shared/components/toggle-collapse/toggle-collapse.component';
import { DocumentData } from '../../../../../../modules/documents/document.data';

@Component({
  selector: 'app-policy-documents',
  templateUrl: './policy-documents.component.html',
  styleUrls: ['./policy-documents.component.css']
})
export class PolicyDocumentsComponent implements OnInit {
  @ViewChild(ToggleCollapseComponent) toggleCollapse: ToggleCollapseComponent;
  hidePolicyDocuments: boolean = false;
  hideBillingDocuments: boolean = false;
  disableItems = {
    updateIcon: true,
    deleteIcon: true,
  };

  constructor(private navigationService: NavigationService,
    private documentsData: DocumentData
    ) { }

  ngOnInit(): void {
    this.documentsData.sourcePage = 'PolicyDocuments';
  }

  public ToggleHidePolicyDocuments(): void {
    this.hidePolicyDocuments = !this.hidePolicyDocuments;
  }

  public ToggleHideBillingDocuments(): void {
    this.hideBillingDocuments = !this.hideBillingDocuments;
  }

  public onClick(clickType?) {
    switch (clickType) {
      case ClickTypes.Back:
        this.navigationService.navigatePolicyRoute(PathConstants.Policy.Issuance);
        break;
      case ClickTypes.Next:
        this.navigationService.navigatePolicyRoute(PathConstants.Policy.Notes);
        break;
    }
  }
}

