import { Component, OnDestroy, OnInit } from '@angular/core';
import { PolicyIssuanceData } from '@app/modules/policy/data/policy-issuance.data';
import { PreloadingStatus } from '@app/modules/rating/models/preloading-status';
import { RaterApiData } from '@app/modules/submission/data/rater-api.data';
import { SubmissionData } from '@app/modules/submission/data/submission.data';
import Utils from '@app/shared/utilities/utils';
import { of, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-policy-changes-section',
  templateUrl: './policy-changes-section.component.html',
  styleUrls: ['./policy-changes-section.component.scss']
})
export class PolicyChangesSectionComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  hideEndorsementEffectiveDate: boolean = false;
  hidePolicyChanges: boolean = false;
  hideEndorsementText: boolean = false;
  preloadStatus: PreloadingStatus;

  lastSelectedDate: Date = new Date();

  get endorsementEffectiveDateTitle() {
    if (this.preloadStatus === PreloadingStatus.InProgress) {
      return "Preloading rater...";
    } else if (this.preloadStatus === PreloadingStatus.Failed) {
      return "Preloading rater failed.";
    }
  }

  get canRate() {
    return this.preloadStatus === PreloadingStatus.Complete;
  }

  constructor(
    public policyIssuanceData: PolicyIssuanceData,
    private submissionData: SubmissionData,
    private raterApiData: RaterApiData
  ) {
    this.preloadStatus = PreloadingStatus.InProgress;
  }

  ngOnInit() {
    if (this.submissionData.isCancelledPolicy) { return; } // if policy is cancelled, should not continue here
    this.raterApiData.preloadingStatus.subscribe((status) => {
      if (status != null) { this.preloadStatus = status; }
    });

    this.subscriptions.push(this.form.get('effectiveDate').valueChanges.subscribe((value) => {
      if (this.policyIssuanceData.isNewlyIssued) { return; } // date changes upon issuance, should not continue here
      if (this.policyIssuanceData.isLoadingChanges) { return; } // if loading onInit already, should not continue here

      let localTime = value.singleDate.formatted;
      const riskDetail = this.submissionData.riskDetail;
      if (this.lastSelectedDate.toLocaleString() == riskDetail.endorsementEffectiveDate.toLocaleString()) {
        return of();
      }
      Utils.blockUI();
      const bindOption = Number(this.submissionData.riskDetail?.binding?.bindOptionId ?? 1);
      this.policyIssuanceData.updateEndorsementEffectiveDate(riskDetail.id, new Date(localTime)).pipe(
        tap((result) => {
          riskDetail.endorsementEffectiveDate = new Date(result);
        }),
        switchMap((result) => {
          this.lastSelectedDate = result;
          return this.policyIssuanceData.loadPolicyAndPremiumChanges(bindOption);
        }),
      ).subscribe();
    }));
  }

  ngOnDestroy() {
    if (this.submissionData.isCancelledPolicy) { return; } // if policy is cancelled, should not continue here
    this.subscriptions.forEach(subs => subs.unsubscribe());
    console.info('Removed subscriptions...');
  }

  ToggleEndorsementEffectiveDate() {
    this.hideEndorsementEffectiveDate = !this.hideEndorsementEffectiveDate;
  }

  TogglePolicyChanges() {
    this.hidePolicyChanges = !this.hidePolicyChanges;
  }

  ToggleEndorsementText() {
    this.hideEndorsementText = !this.hideEndorsementText;
  }

  endorsementEffectiveDateChanged() {
    this.policyIssuanceData.isLoadingChanges = false;
  }

  get form() { return this.policyIssuanceData.issuanceForm; }

}
