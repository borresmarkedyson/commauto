import { Component, OnInit } from '@angular/core';
import { ErrorMessageConstant } from '../../../../../../shared/constants/error-message.constants';
import { UsersLabelConstants } from '../../../../../../shared/constants/user.label.constants';
import { ChangeDetectorRef } from '@angular/core';
import { UserData } from '../../../../../../modules/management/data/user.data';
import { AuthService } from 'app/core/services/auth.service';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  styleUrls: ['./user-information.component.scss']
})
export class UserInformationComponent implements OnInit {
  UsersLabelConstants = UsersLabelConstants;
  ErrorMessageConstant = ErrorMessageConstant;
  isOpen: boolean = false;
  constructor(public userData: UserData, private detect: ChangeDetectorRef, private authService: AuthService) { }

  ngOnInit() {
  }

  collapse() {
    this.isOpen = !this.isOpen;
  }

  calculateAge(date) {
    const dateOfBirth = date?.jsDate;

    if (!!!dateOfBirth) {
      this.userData.UserInfoFormGroup.get('age').setValue('0');
      return 0;
    }

    const today = this.authService.getCustomDate();
    const birthDate = new Date(dateOfBirth);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    this.userData.UserInfoFormGroup.get('age').setValue(age);
    this.detect.detectChanges();
  }
}
