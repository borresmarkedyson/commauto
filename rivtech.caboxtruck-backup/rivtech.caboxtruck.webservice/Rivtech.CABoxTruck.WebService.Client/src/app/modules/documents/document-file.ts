export class DocumentFile {
    id?: string;
    tableRefId: string;
    riskDetailId: string;
    fileCategoryId: number;
    category: string;
    description: string;
    fileName: string;
    filePath: string;
    createdDate: any;
    dateAdded: any;
    checked?: boolean = false;
    isSystemGenerated?: boolean = false;

    public static CreateNew(riskDetailId?: string): DocumentFile {
        return  {
            id: null,
            tableRefId: riskDetailId,
            riskDetailId: riskDetailId,
            fileCategoryId: null,
            category: null,
            description: null,
            fileName: null,
            filePath: null,
            createdDate: null,
            dateAdded: null,
            checked: false
        };
    }

    private static formatDateForPicker(dateValue?: any) {
        const dateSet = dateValue ? new Date(dateValue) : new Date();
        return { isRange: false, singleDate: { jsDate: dateSet } }
    }
}
