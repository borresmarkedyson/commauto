import { HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Utils from '../../shared/utilities/utils';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { BehaviorSubject, Observable } from 'rxjs';
import { SubmissionData } from '../submission/data/submission.data';
import { DocumentApiService } from './document-api.service';
import { DocumentFile } from './document-file';
import NotifUtils from '@app/shared/utilities/notif-utils';
import { TableData } from '../submission/data/tables.data';
import { PagerService } from '../../core/services/pager.service';

@Injectable({
    providedIn: 'root'
})
export class DocumentData {
    loading: boolean;
    pager: any = {};
    currentPage: number = 0;
    pagedItems: any[];

    loadingBillingDocs: boolean;
    pagerBillingDocs: any = {};
    currentPageBillingDocs: number = 0;
    pagedItemsBillingDocs: any[];

    sourcePage: string = '';

    datepickerOptions: IAngularMyDpOptions;

    categories: any[];

    policy: boolean = false;

    documentFormData: DocumentFile;
    /* Observable list of documents.  */
    private documentListData: BehaviorSubject<DocumentFile[]> = new BehaviorSubject<DocumentFile[]>([]);
    public documentsListData: any = {};
    public billingDocumentsListData: any = {};

    get documentForm(): DocumentFile {
        return this.documentFormData ?? DocumentFile.CreateNew();
    }

    get documentList(): Observable<DocumentFile[]> {
        return this.documentListData.asObservable();
    }

    get isFormDocumentNew(): boolean {
        return this.documentForm?.id == null ?? true;
    }

    get disableActionButton(): boolean {
        return this.submissionData?.isIssued && this.sourcePage === 'BindDocuments';
    }

    constructor(
        private submissionData: SubmissionData,
        private documentApiService: DocumentApiService,
        private tableData: TableData,
        public pagerService: PagerService
    ) {

        this.datepickerOptions = {
            dateRange: false,
            dateFormat: 'mm/dd/yyyy',
            disableSince: { year: 0, month: 0, day: 0 },
            disableUntil: { year: 0, month: 0, day: 0 }
        };

        this.documentApiService.getCategories().subscribe(data => {
            this.categories = data;
        });
    }

    loadSubmissionDocuments() {
        this.documentApiService.getDocuments(this.submissionData.riskDetail?.id).subscribe(data => {
            data = data.filter(d => d.fileCategoryId);
            data.forEach(d => {
                d.category = this.categories.find(c => c.id === d.fileCategoryId)?.description;
            });
            data = data.sort((a, b) => a.createdDate > b.createdDate ? -1 : 1);
            // this.documentListData.next(data);
            this.documentsListData = data;
            this.setPage(1, this.documentsListData);
        });
    }

    loadPolicyDocuments() {
        this.documentApiService.getRiskDocuments(this.submissionData.riskDetail?.riskId).subscribe(data => {
            data = data.filter(d => d.fileCategoryId && d.fileCategoryId !== 3);
            data.forEach(d => {
                d.category = this.categories.find(c => c.id === d.fileCategoryId)?.description;
            });
            data = data.sort((a, b) => a.createdDate > b.createdDate ? -1 : 1);
            // this.documentListData.next(data);
            this.documentsListData = data;
            this.setPage(1, this.documentsListData);
        });
    }

    loadBillingDocuments() {
        this.documentApiService.getRiskDocuments(this.submissionData.riskDetail?.riskId).subscribe(data => {
            data = data.filter(d => d.fileCategoryId && d.fileCategoryId === 3);
            console.log(JSON.stringify(data.filter(d => d.fileCategoryId)));
            data.forEach(d => {
                d.category = this.categories.find(c => c.id === d.fileCategoryId)?.description;
            });
            data = data.sort((a, b) => a.createdDate > b.createdDate ? -1 : 1);
            // this.documentListData.next(data);
            this.billingDocumentsListData = data;
            this.setPageBillingDocuments(1, this.billingDocumentsListData);
        });
    }

    setPage(page: number, data: any[], isNew: boolean = false) {
        if (page < 1) {
          return;
        }

        this.pager = this.pagerService.getPager(data.length, page);

        if (isNew) {
          this.currentPage = this.pager.currentPage = this.pager.totalPages;
          this.pager.endIndex = this.pager.totalItems;
          this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);
        } else {
          while (this.pager.totalItems <= ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize)) {
            this.pager.currentPage--;
            this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);
          }

          this.currentPage = page;
        }

        this.pagedItems = data.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    setPageBillingDocuments(page: number, data: any[], isNew: boolean = false) {
        if (page < 1) {
          return;
        }

        this.pagerBillingDocs = this.pagerService.getPager(data.length, page);

        if (isNew) {
          this.currentPageBillingDocs = this.pagerBillingDocs.currentPage = this.pagerBillingDocs.totalPages;
          this.pagerBillingDocs.endIndex = this.pagerBillingDocs.totalItems;
          this.pagerBillingDocs.startIndex = ((this.pagerBillingDocs.currentPage * this.pagerBillingDocs.pageSize) - this.pagerBillingDocs.pageSize);
        } else {
          while (this.pagerBillingDocs.totalItems <= ((this.pagerBillingDocs.currentPage * this.pagerBillingDocs.pageSize) - this.pagerBillingDocs.pageSize)) {
            this.pagerBillingDocs.currentPage--;
            this.pagerBillingDocs.startIndex = ((this.pagerBillingDocs.currentPage * this.pagerBillingDocs.pageSize) - this.pagerBillingDocs.pageSize);
          }

          this.currentPageBillingDocs = page;
        }

        this.pagedItemsBillingDocs = data.slice(this.pagerBillingDocs.startIndex, this.pagerBillingDocs.endIndex + 1);
    }

    checkDocumentIsUploaded(id: string) {
        return this.documentApiService.getDocumentIsUploaded(id);
    }

    checkDocumentIsAllowedToEditDelete(id: string) {
        return this.documentApiService.getDocumentIsAllowedToEditDelete(id);
    }


    changeEditDocumentForm(doc?: DocumentFile) {
        doc.dateAdded = this.formatDateForPicker(doc.createdDate);
        this.documentFormData = doc;
    }

    clearDocumentForm() {
        this.documentFormData = null;
    }

    get defaultPolicyState(): string { return this.submissionData.riskDetail?.nameAndAddress?.state; }

    loadDocuments() {
        if (this.policy) {
          this.loadPolicyDocuments();
          this.loadBillingDocuments();
        } else {
          this.loadSubmissionDocuments();
        }
      }

    saveDocument(riskDocument: DocumentFile, fileInfo: any, modal: any) {
        Utils.blockUI();
        const formData = new FormData();
        if (fileInfo?.length > 0) {
            // const file = <File>fileInfo[0];
            // formData.append('file', file, file.name);
            Array.from(fileInfo).forEach(file => {
                if (file instanceof File) {
                  formData.append('file', (<File>file), (<File>file).name);
                }
              });
        }

        riskDocument.createdDate = riskDocument.dateAdded?.singleDate?.jsDate?.toLocaleDateString();
        formData.append('riskDocument', JSON.stringify(riskDocument));
        formData.append('states', this.defaultPolicyState);

        this.documentApiService.addDocument(formData).subscribe(data => {
            if (data.body?.message) {
                NotifUtils.showError(data.body.message);
                return;
            }

            if (data.type === HttpEventType.Response) {
                this.loadDocuments();
                Utils.unblockUI();
                modal.hide();
            }
        });
    }

    deleteDoc(doc: DocumentFile) {
        this.documentApiService.delete(doc.id).subscribe(() => {
            this.loadDocuments();
        });
    }

    toggleAllCheck(value: boolean) {
        const newData = this.documentListData.getValue();
        newData.forEach(x => x.checked = value);
        this.documentListData.next(newData);
    }

    downloadDocs() {        
        const selectedItemIds = this.documentListData.getValue().filter(x => x.checked).map(x => x.id);
        if (selectedItemIds.length === 0) { return; }
        Utils.blockUI();
        this.documentApiService.downloadDocs(selectedItemIds).subscribe((data) => {
            if (data.type === HttpEventType.Response) {
                Utils.unblockUI();
                const blob = new Blob([data.body], { type: 'application/zip' });
                const url = window.URL.createObjectURL(blob);
                const anchor = document.createElement('a');
                anchor.download = `BT Documents.zip`;
                anchor.href = url;
                anchor.click();
            }
        });
    }

    emailDocs() {
        const selectedItemIds = this.documentListData.getValue().filter(x => x.checked).map(x => x.id);
        this.documentApiService.emailDocs(selectedItemIds);
    }

    formatDateForPicker(dateValue?: any) {
        const dateSet = dateValue ? new Date(dateValue) : new Date();
        return { isRange: false, singleDate: { jsDate: dateSet } }
    }

}
