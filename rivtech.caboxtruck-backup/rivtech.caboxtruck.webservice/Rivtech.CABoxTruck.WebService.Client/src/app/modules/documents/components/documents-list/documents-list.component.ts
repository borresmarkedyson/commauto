import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DocumentFile } from '../../document-file';
import { DocumentData } from '../../document.data';
import NotifUtils from '../../../../shared/utilities/notif-utils';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DocumentsAddDialogComponent } from '../documents-add-dialog/documents-add-dialog.component';
import { SubmissionData } from '../../../../modules/submission/data/submission.data';
import { TableData } from '../../../../modules/submission/data/tables.data';
import { TableConstants } from '../../../../shared/constants/table.constants';
import { NotificationMessageConstant } from '../../../../shared/constants/notification-message.constants';
import { BindingData } from '../../../../modules/submission/data/binding/binding.data';

@Component({
  selector: 'app-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.scss'],
  providers: [DatePipe]
})
export class DocumentsListComponent implements OnInit {

  @Input()
  items: DocumentFile[];
  allChecked: boolean = false;
  modalRef: BsModalRef;
  TableConstants = TableConstants;
  @Input() disableItems = {
    updateIcon: false,
    deleteIcon: false,
  };
  @Input() policy: boolean = false;
  @Input() isBillingCategory: boolean = false;
  @Input() isPolicyCategory: boolean = true;

  constructor(
    private modalService: BsModalService,
    public documentData: DocumentData,
    private submissionData: SubmissionData,
    private tableData: TableData,
    public datePipe: DatePipe,
    private bindingData: BindingData
  ) { }

  ngOnInit(): void {
    this.documentData.documentList.subscribe(data => this.items = data);
    this.documentData.policy = this.policy;
    this.documentData.loadDocuments();
  }

  onEditItem(doc?: DocumentFile) {
    if (!doc) {
      doc = DocumentFile.CreateNew(this.submissionData.riskDetail.id);

      this.openDocuModal(doc);
    } else {
      this.documentData.checkDocumentIsAllowedToEditDelete(doc.id).subscribe(data => {
        if (data === true) {
          this.openDocuModal(doc);
        } else {
          NotifUtils.showInfo(NotificationMessageConstant.fileNotAllowedToEditDeleteErrorMessage);
        }
      });
    }
  }

  openDocuModal(doc: DocumentFile) {
    this.documentData.changeEditDocumentForm(doc);
    this.modalRef = this.modalService.show(DocumentsAddDialogComponent, {
      initialState: {
        category: (this.isBillingCategory) ? 'Billing' : 'Policy'
      },
      backdrop: 'static',
      keyboard: false,
      class: 'modal-md'
    });
  }

  onDeleteItem(doc: DocumentFile) {
    this.documentData.checkDocumentIsAllowedToEditDelete(doc.id).subscribe(data => {
      if (data == true) {
        NotifUtils.showConfirmMessage('Are you sure you want to delete this document?', () => {
          this.documentData.deleteDoc(doc);
          this.bindingData.DeleteFileRequirements(doc.tableRefId);
          this.bindingData.DeleteFileQuoteCondition(doc.tableRefId);
        });
      } else {
        NotifUtils.showInfo(NotificationMessageConstant.fileNotAllowedToEditDeleteErrorMessage);
        return;
      }
    });
  }

  toggleAllCheck() {
    this.documentData.toggleAllCheck(this.allChecked);
  }

  viewDocument(doc: DocumentFile) {
    console.log('document', doc);
    this.documentData.checkDocumentIsUploaded(doc.id).subscribe(data => {
      if (data == true) {
        const docUrl = decodeURIComponent(doc.filePath);
        const win = window.open(docUrl, '_blank');
        win.focus();
      } else {
        NotifUtils.showInfo(NotificationMessageConstant.fileNotYetReadyErrorMessage);
      }
    });
  }

  downloadDocuments() {
    this.documentData.downloadDocs();
  }

  sort(targetElement) {
    this.tableData.sort(targetElement, this.items, 1);
  }
}
