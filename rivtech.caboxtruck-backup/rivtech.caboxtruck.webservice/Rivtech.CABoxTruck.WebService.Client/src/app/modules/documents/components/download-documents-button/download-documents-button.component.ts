import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-download-documents-button',
  templateUrl: './download-documents-button.component.html',
  styleUrls: ['./download-documents-button.component.scss']
})
export class DownloadDocumentsButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
