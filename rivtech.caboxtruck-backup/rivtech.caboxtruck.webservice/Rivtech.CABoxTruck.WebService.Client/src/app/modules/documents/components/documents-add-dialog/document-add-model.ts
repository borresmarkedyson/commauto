export class DocumentAddModel {
    public categoryId: number;
    public description: string;
    public file: any;
    public isConfidential: boolean;
}