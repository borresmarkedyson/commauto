import { LinkUrl } from '../../../../shared/models/linkUrl';

export class StatusCounter {
    public description: string;
    public count: number;
    public icon: string;
    public linkUrl: LinkUrl;
}
