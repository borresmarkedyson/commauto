import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from '../../../core/services/common.service';
import { DashboardDTO } from '../../../shared/models/dashboard/dashboard';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DashboardPolicySearchDTO } from '../../../shared/models/dashboard/dashboard-policy-search';
import { AccountsReceivableDTO, AccountsReceivableInputDTO } from '@app/shared/models/dashboard/arDto';
import { ReportManagementFilterDTO } from 'app/shared/models/reports/report-management-filter.dto';

@Injectable({
  providedIn: 'root'
})

export class ReportsService {
  private readonly baseUrl: string;

  constructor(private http: HttpClient, private commonService: CommonService) {
    this.http = http;
    this.baseUrl = environment.ApiUrl.replace('/api', '');
  }

  searchAccountsReceivable(data: AccountsReceivableInputDTO): Observable<AccountsReceivableDTO[]> {
    const url_ = this.baseUrl + `/api/Reports/SearchAccountsReceivable`;
    return this.http.post(url_, data)
      .pipe(
        map(data => {
          return data as AccountsReceivableDTO[];
        }),
        catchError(this.commonService.handleObservableHttpError)
      );
  }

  getReport(paymentRequest: ReportManagementFilterDTO): Observable<any> {
    const url_ = this.baseUrl + `/api/Reports/GetReportStream`;
      return this.http.post(url_, paymentRequest, {
        observe: 'response',
        responseType: 'blob'
    })
    .catch(this.commonService.handleObservableHttpError);
  }
}
