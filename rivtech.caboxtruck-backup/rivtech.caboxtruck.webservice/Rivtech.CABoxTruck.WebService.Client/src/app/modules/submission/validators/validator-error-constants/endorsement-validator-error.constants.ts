export const EndorsementValidatorErrorConstants = {
  personalPropertyAtOtherResidencesLimitDivisibleErrorMessage: {
    key: 'personalPropertyAtOtherResidencesLimitDivisible',
    value: 'Limit must be in increments of 1000.'
  },
  personalPropertyAtOtherResidencesLimitNotZeroErrorMessage: {
    key: 'personalPropertyAtOtherResidencesLimitZero',
    value: 'This cannot be zero.'
  }  
};
