export const PropertyValidatorErrorConstants = {
  yearRoofShouldNotBeOlderThanYearBuiltErrorMessage: {
    key: 'yearRoofShouldNotBeOlderThanYearBuilt',
    value: 'Year Roof Installed cannot be prior to year built.'
  },
  yearRoofInstalledCannotBeAFutureYearErrorMessage: {
    key: 'yearRoofInstalledCannotBeAFutureYear',
    value: 'Year Roof Installed cannot be more than 1 year in the future.'
  },
  yearShouldNotBeOlderThanYearBuiltErrorMessage: {
    key: 'yearShouldNotBeOlderThanYearBuilt',
    value: '{0} cannot be prior to year built.'
  },
  yearInstalledCannotBeAFutureYearErrorMessage: {
    key: 'yearInstalledCannotBeAFutureYear',
    value: '{0} cannot be more than 1 year in the future.'
  }
};
