export const BillingErrorConstants = {
  amountCannotBeZeroErrorMessage: {
    key: 'amountCannotBeZero',
    value: 'Amount cannot be zero.'
  },
};