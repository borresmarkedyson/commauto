export const BindAndIssueValidatorErrorConstants = {
  creditCardExpired: {
    key: 'creditCardExpired',
    value: 'Credit Card is expired'
  }
};