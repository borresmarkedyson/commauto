import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { CommonService } from '../../../core/services/common.service';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class RaterApiService {
  private raterUrl: string;
  private baseUrl: string;

  constructor(private http: HttpClient, private commonService: CommonService, private httpbackend: HttpBackend) {
    this.raterUrl = environment.ApiUrl.replace('/api', '');
    this.baseUrl = environment.ApiUrl.replace('/api', '');
  }

  test(): Observable<any> {
    return this.http.get(new URL(`/api/ExperienceRater/testconnection`, this.baseUrl).href, { responseType: 'text' })
      .catch(this.commonService.handleObservableHttpError);
  }

  upload(obj: any, id: string): Observable<any> {

    this.http = new HttpClient(this.httpbackend);
    return this.http.post(new URL(`/api/ExperienceRater/upload/${id}`, this.baseUrl).href, obj, { reportProgress: true, observe: 'events', responseType: 'text' })
      .catch(this.commonService.handleObservableHttpError);
  }

  upload2(obj: any, id: string, callbackUploadStart?: Function, callbackUploadProgress?: Function, callbackUploadComplete?: Function): Promise<unknown> {

    const promise = new Promise((resolve, reject) => {
      try {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', new URL(`/api/ExperienceRater/upload/${id}`, this.baseUrl).href, true);
        xhr.responseType = 'text';

        if (callbackUploadStart) {
          xhr.addEventListener('loadstart', callbackUploadStart.bind(this), false);
        }

        if (callbackUploadProgress) {
          xhr.upload.addEventListener('progress', callbackUploadProgress.bind(this), false);
        }

        if (callbackUploadComplete) {
          xhr.addEventListener('load', callbackUploadComplete.bind(this), false);
        }

        xhr.onreadystatechange = function (response) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              resolve(xhr.response);
            } else {
              reject(xhr.response);
            }
          }
        };

        xhr.send(obj);
      } catch {
        reject(null);
      }
    });
    return promise;
  }

  // download(id: string, inputValues:any): Observable<any> {
  //   return this.http.post(new URL(`/api/ExperienceRater/download/${id}`, this.raterUrl).href, inputValues, { responseType: 'blob' })
  //     .catch(this.commonService.handleObservableHttpError);
  // }

  download(id: string, inputValues: any): Observable<any> {
    return this.http.post(new URL(`/api/ExperienceRater/download/${id}`, this.baseUrl).href, inputValues, { reportProgress: true, observe: 'events', responseType: 'blob' })
      .catch(this.commonService.handleObservableHttpError);
  }

  download2(id: string, effectiveDate: string, inputValues: any, callbackStart?: Function, callbackDownload?: Function): Promise<unknown> {
    // native way of calling api but modified as asyncronous call.
    const promise = new Promise((resolve, reject) => {
      try {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', new URL(`/api/ExperienceRater/download/${id}?effectiveDate=${effectiveDate}`, this.baseUrl).href, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.responseType = 'blob';

        if (callbackStart) {
          xhr.addEventListener('loadstart', callbackStart.bind(this), false);
        }

        if (callbackDownload) {
          xhr.addEventListener('progress', callbackDownload.bind(this), false);
        }

        xhr.onreadystatechange = function (response) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              resolve(xhr.response);
            } else {
              reject(xhr.response);
            }
          }
        };
        xhr.send(JSON.stringify(inputValues));
      } catch {
        reject(null);
      }
    });
    return promise;
  }

  testPremium(): Observable<any> {
    return this.http.get(new URL(`/api/PremiumRater/testconnection`, this.baseUrl).href, { responseType: 'text' })
      .catch(this.commonService.handleObservableHttpError);
  }

  create(id: string, effectiveDate: string): Observable<any> {
    return this.http.post(new URL(`/api/ExperienceRater/create/${id}?effectiveDate=${effectiveDate}`, this.baseUrl).href, true)
      .catch(this.commonService.handleObservableHttpError);
  }

  preLoadPremium(id: string, riskId: string, effectiveDate: any, fromPolicy?: boolean): Observable<any> {
    let params = new HttpParams()
      .set('riskDetailId', riskId);

    if (effectiveDate) {
      params = params.set('inceptionDate', effectiveDate);
    }

    if(!!fromPolicy)
      params = params.set('fromPolicy', "true");

    return this.http.post(new URL(`/api/PremiumRater/preload/${id}?${params.toString()}`, this.baseUrl).href, true)
      .catch(this.commonService.handleObservableHttpError);
  }


  unloadPremium(id: string, riskDetailId: string, ispageunload: boolean = false, fromPolicy: boolean): Observable<any> {
    return this.http.post(new URL(`/api/PremiumRater/unload/${id}?riskDetailId=${riskDetailId}&ispageunload=${ispageunload}&fromPolicy=${fromPolicy}`, this.baseUrl).href, true)
      .catch(this.commonService.handleObservableHttpError);
  }

  calculatePremium(params: { inputData: any, fromPolicy?: boolean, effectiveDate?: Date, forIssuance?:boolean}): Observable<any> {
    var url = "/api/PremiumRater/calculatePremium?";
    if(!!params.fromPolicy)
      url += `&fromPolicy=${params.fromPolicy}`;
    if(!!params.effectiveDate)
      url += `&effectiveDate=${formatDate(params.effectiveDate ?? Date.now(), "yyyy-MM-dd", "en-us")}`;
    if(params.forIssuance != null)
      url += `&forIssuance=${params.forIssuance}`;

    console.log(url);
    return this.http.post(new URL(url, this.baseUrl).href, params.inputData, { responseType: 'text' })
      .catch(this.commonService.handleObservableHttpError);
  }
}
