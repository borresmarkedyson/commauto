import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../../core/services/common.service';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class EndorsementService {
  private localServiceUrl: string = environment.ApiUrl.replace('/api', '');

  constructor(private http: HttpClient, private commonService: CommonService) {
  }

  GetEndorsementPremiumChanges(riskDetailId: string): Observable<any> {
    return this.http.get(`${this.localServiceUrl}/api/Endorsement/PremiumChanges/${riskDetailId}`)
      .catch(this.commonService.handleObservableHttpError);
  }

  updateEffectiveDate(riskDetailId: string, effectiveDate: Date): Observable<any> {
    const effDate = formatDate(effectiveDate, 'yyyy-MM-dd', 'en');
    return this.http.put<string>(`${this.localServiceUrl}/api/Endorsement/UpdateEffectiveDate/${riskDetailId}?effectiveDate=${effDate}`, null)
      .catch(this.commonService.handleObservableHttpError);
  }

  GeneratePolicyNumber(riskDetailId: string): Observable<any> {
    return this.http.post(`${this.localServiceUrl}/api/Endorsement/GeneratePolicyNumber/${riskDetailId}`, null, { responseType: 'text' })
      .catch(this.commonService.handleObservableHttpError);
  }
}
