import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LimitsService } from '../../services/coverage-limits.service';
import { take } from 'rxjs/operators';
import Utils from '../../../../shared/utilities/utils';
import { RiskCoverageDTO, SaveRiskCoverageDTO } from '../../../../shared/models/submission/limits/riskCoverageDto';
import { SubmissionData } from '../submission.data';
import { ToastrService } from 'ngx-toastr';
import { SummaryData } from '../summary.data';
import { LimitsDefaults, LimitsDefaultValues } from '../../../../shared/constants/limits-defaults.constants';
import FormUtils from '../../../../shared/utilities/form.utils';
import { LayoutService } from '../../../../core/services/layout/layout.service';
import { RiskSpecificsData } from '../risk-specifics.data';
import { createSubmissionDetailsMenuItems } from '../../components/submission-details/submission-details-navitems';
import { SelectItem } from '../../../../shared/models/dynamic/select-item';
import { ErrorMessageConstant } from '../../../../shared/constants/error-message.constants';

export interface LimitsForm {
  limitsPageForm?: FormGroup;
}

@Injectable(
  { providedIn: 'root' }
)
export class LimitsData {
  // General
  isGeneralLiability: boolean = false;
  isCargo: boolean = false;
  isRefrigeration: boolean = false;
  isCollision: boolean = false;
  isComprehensive: boolean = false;

  isUmBiAvailable: boolean;
  isUmPdAvailable: boolean;
  isUimBiAvailable: boolean;
  isUimPdAvailable: boolean;
  isPipAvailable: boolean;

  // Hide UM/UIM PD
  // isDisablePD: boolean = true;
  showPD: boolean = false;

  // Limits dropdown lookup
  biLimitList: any[];
  pdLimitList: any[];
  liabDeductibleList: any[];
  umBiLimitList: any[];
  umPdLimitList: any[];
  uimBiLimitList: any[];
  uimPdLimitList: any[];
  medpayList: any[];
  pipList: any[];
  CauseOfLossList: any[];
  ComprehensiveList: any[];
  CollisionList: any[];
  glList: any[];
  cargoList: any[];
  refBreakList: any[];

  bComp: any;
  bColl: any;
  bCargo: any;
  bRef: any;

  symbolList: any[] = [
    { label: '7, 8 & 9', value: '1' },
    { label: '7 & 9', value: '2' },
    { label: '2, 8 & 9', value: '3' },
    { label: '1) Any Auto', value: '4' },
    { label: '2) All Owned Autos', value: '5' },
    { label: '3) Owned Private Passenger Autos', value: '6' },
    { label: '4) Owned Autos Other Than Private Passenger', value: '7' },
    { label: '5) All Owned Autos Which Require No-Fault Coverage', value: '8' },
    { label: '6) Owned Autos Subject to Compulsory UM Law', value: '9' },
    { label: '7) Autos Specified on  Schedule', value: '10' },
    { label: '8) Hired Autos', value: '11' },
    { label: '9) Non-Owned Autos', value: '12' },
  ];

  hasRiskCoverage: boolean = true;

  // All forms
  limitsForm: LimitsForm;

  constructor(
    private limitService: LimitsService,
    private formBuilder: FormBuilder,
    public summaryData: SummaryData,
    public submissionData: SubmissionData,
    private toastr: ToastrService,
    private layoutService: LayoutService,
    private riskSpecificsData: RiskSpecificsData,
  ) { }

  get coverage() {
    return this.submissionData.getCoverage();
  }

  get hasAutoPhysicalDamage() {
    return this.coverage?.compDeductibleId > LimitsDefaultValues.compId
      || this.coverage?.fireDeductibleId > LimitsDefaultValues.compId
      || this.coverage?.collDeductibleId > LimitsDefaultValues.colId;
  }

  get hasGeneralLiability() {
    return this.coverage?.glbiLimitId > LimitsDefaultValues.glId;
  }

  get hasComprehensive() {
    return this.coverage?.compDeductibleId > LimitsDefaultValues.compId || this.coverage?.fireDeductibleId > LimitsDefaultValues.compId;
  }

  get hasCollision() {
    return this.coverage?.collDeductibleId > LimitsDefaultValues.colId;
  }

  get hasCargo() {
    return this.coverage?.cargoLimitId > LimitsDefaultValues.cargoId;
  }

  get hasRefrigeration() {
    return this.coverage?.refCargoLimitId && this.coverage.refCargoLimitId !== LimitsDefaultValues.refId;
  }

  get hasUninsuredMotoristBI() {
    return this.hasUninsuredMotoristSingle || this.hasUninsuredMotoristSplit;
  }

  get hasUninsuredMotoristSingle() {
    return this.coverage?.umbiLimitId && LimitsDefaultValues.umBISingleLimitIds.some(limit => limit == this.coverage?.umbiLimitId) && 
        this.coverage?.umbiLimitId !== LimitsDefaultValues.umBILimitId;
  }

  get hasUninsuredMotoristSplit() {
    return this.coverage?.umbiLimitId && LimitsDefaultValues.umBISplitLimitIds.some(limit => limit == this.coverage?.umbiLimitId) && 
        this.coverage?.umbiLimitId !== LimitsDefaultValues.umBILimitId;
  }

  initiateFormFields() {
    // this.isDisablePD = true;
    this.Reset();
    this.limitsForm = {};
    this.limitsForm.limitsPageForm = this.formBuilder.group({
      riskDetailId: [null],
      autoBILimitId: [null, [Validators.required]],
      autoPDLimitId: [null],
      umbiLimitId: [null],
      umpdLimitId: [null],
      uimbiLimitId: [null],
      uimpdLimitId: [null],
      medPayLimitId: [null, [Validators.required]],
      pipLimitId: [null],
      liabilityDeductibleId: [null, [Validators.required]],
      compDeductibleId: [null, [Validators.required]],
      collDeductibleId: [null, [Validators.required]],
      glbiLimitId: [null, [Validators.required]],
      cargoLimitId: [null, [Validators.required]],
      refCargoLimitId: [null, [Validators.required]],

      comprehensiveFire: [true],

      alSymbolId: [null, [Validators.required]],
      pdSymbolId: [null], // Validators.required
      umbiLimitSymbolId: [null], // Validators.required
      umpdLimitSymbolId: [null], // Validators.required
      uimbiLimitSymbolId: [null], // Validators.required
      uimpdLimitSymbolId: [null], // Validators.required
      medPayLimitSymbolId: [null], // Validators.required
      pipLimitSymbolId: [null], // Validators.required
      liabilityDeductibleSymbolId: [null], // Validators.required
      compDeductibleSymbolId: [null], // Validators.required
      collDeductibleSymbolId: [null], // Validators.required
      glbiLimitSymbolId: [null],
      cargoLimitSymbolId: [null], // Validators.required
      refCargoLimitSymbolId: [null], // Validators.required

      alPremiumInputSymbol: [null],
      pdPremiumInputSymbol: [null],
      glPremiumInputSymbol: [null],
      cargoPremiumInputSymbol: [null],
    });
  }

  populatePolicyLimitsFields() {
    this.Reset();
    // Initial submission load patch
    if (this.submissionData?.riskDetail?.riskCoverage == null && this.submissionData?.riskDetail?.riskCoverages != null && this.submissionData?.riskDetail?.riskCoverages?.length > 0) {
        const bindOptionNumber = Number(this.submissionData?.riskDetail.binding?.bindOptionId ?? 1);
        const option1 = this.submissionData.riskDetail.riskCoverages.find(x => x.optionNumber === bindOptionNumber);
        this.submissionData.riskDetail.riskCoverage = option1;
    }
    if (this.submissionData?.riskDetail?.riskCoverage) {
      this.limitsForm.limitsPageForm.patchValue(this.submissionData?.riskDetail?.riskCoverage);
      if (this.submissionData?.riskDetail?.riskCoverage?.compDeductibleId == null && this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId) {
        this.limitsForm.limitsPageForm.controls['comprehensiveFire'].patchValue(false);
        this.limitsForm.limitsPageForm.controls['compDeductibleId'].patchValue(this.submissionData.riskDetail.riskCoverage.fireDeductibleId);
        this.limitsForm.limitsPageForm.controls['compDeductibleSymbolId'].patchValue(this.submissionData.riskDetail.riskCoverages?.find(x => x.optionNumber === 1)?.fireDeductibleSymbolId); // always use option 1
      }

      if (this.submissionData?.riskDetail?.riskCoverage?.compDeductibleId && this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId == null && this.submissionData.riskDetail.riskCoverage.cargoLimitId !== 1) { this.isComprehensive = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.cargoLimitId && this.submissionData?.riskDetail?.riskCoverage?.cargoLimitId !== 1) { this.isCargo = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.glbiLimitId && this.submissionData?.riskDetail?.riskCoverage?.glbiLimitId !== 1) { this.isGeneralLiability = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.refCargoLimitId && this.submissionData?.riskDetail?.riskCoverage?.refCargoLimitId !== 1) { this.isRefrigeration = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.collDeductibleId && this.submissionData?.riskDetail?.riskCoverage?.collDeductibleId !== 1) { this.isCollision = true; }

      this.bComp = this.submissionData?.riskDetail?.riskCoverage.compDeductibleId ?? this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId;
      this.bColl = this.submissionData?.riskDetail?.riskCoverage.collDeductibleId;
      this.bCargo = this.submissionData?.riskDetail?.riskCoverage.cargoLimitId;
      this.bRef = this.submissionData?.riskDetail?.riskCoverage.refCargoLimitId;
      // console.log('backup', this.bComp, this.bColl, this.bCargo, this.bRef );
    }
  }

  populateLimitsFields() {
    this.Reset();
    // Initial submission load patch
    if (this.submissionData?.riskDetail?.riskCoverage == null && this.submissionData?.riskDetail?.riskCoverages != null && this.submissionData?.riskDetail?.riskCoverages?.length > 0) {
      const option1 = this.submissionData.riskDetail.riskCoverages.find(x => x.optionNumber === 1);
      this.submissionData.riskDetail.riskCoverage = option1;
    }
    if (this.submissionData?.riskDetail?.riskCoverage) {
      this.limitsForm.limitsPageForm.patchValue(this.submissionData?.riskDetail?.riskCoverage);
      if (this.submissionData?.riskDetail?.riskCoverage?.compDeductibleId == null && this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId) {
        this.limitsForm.limitsPageForm.controls['comprehensiveFire'].patchValue(false);
        this.limitsForm.limitsPageForm.controls['compDeductibleId'].patchValue(this.submissionData.riskDetail.riskCoverage.fireDeductibleId);
        this.limitsForm.limitsPageForm.controls['compDeductibleSymbolId'].patchValue(this.submissionData.riskDetail.riskCoverage.fireDeductibleSymbolId);
      }

      if (this.submissionData?.riskDetail?.riskCoverage?.compDeductibleId && this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId == null && this.submissionData.riskDetail.riskCoverage.cargoLimitId !== 1) { this.isComprehensive = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.cargoLimitId && this.submissionData?.riskDetail?.riskCoverage?.cargoLimitId !== 1) { this.isCargo = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.glbiLimitId && this.submissionData?.riskDetail?.riskCoverage?.glbiLimitId !== 1) { this.isGeneralLiability = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.refCargoLimitId && this.submissionData?.riskDetail?.riskCoverage?.refCargoLimitId !== 1) { this.isRefrigeration = true; }
      if (this.submissionData?.riskDetail?.riskCoverage?.collDeductibleId && this.submissionData?.riskDetail?.riskCoverage?.collDeductibleId !== 1) { this.isCollision = true; }

      this.bComp = this.submissionData?.riskDetail?.riskCoverage.compDeductibleId ?? this.submissionData?.riskDetail?.riskCoverage?.fireDeductibleId;
      this.bColl = this.submissionData?.riskDetail?.riskCoverage.collDeductibleId;
      this.bCargo = this.submissionData?.riskDetail?.riskCoverage.cargoLimitId;
      this.bRef = this.submissionData?.riskDetail?.riskCoverage.refCargoLimitId;
      // console.log('backup', this.bComp, this.bColl, this.bCargo, this.bRef );
    }
  }

  setDefaults() {
    const state = this.submissionData?.riskDetail?.nameAndAddress?.state;
    const limitDefault = LimitsDefaults.find(x => x.state === state);
    if (limitDefault != null) {
      this.limitsForm.limitsPageForm.controls['umbiLimitId'].patchValue(limitDefault.umbiLimitId);
      this.limitsForm.limitsPageForm.controls['umpdLimitId'].patchValue(limitDefault.umpdLimitId);
      this.limitsForm.limitsPageForm.controls['uimbiLimitId'].patchValue(limitDefault.uimbiLimitId);
      this.limitsForm.limitsPageForm.controls['uimpdLimitId'].patchValue(limitDefault.uimpdLimitId);
      this.limitsForm.limitsPageForm.controls['pipLimitId'].patchValue(limitDefault.pipLimitId);

      if (this.submissionData.riskDetail?.riskCoverage != null) {
        this.submissionData.riskDetail.riskCoverage.umbiLimitId = limitDefault.umbiLimitId;
        this.submissionData.riskDetail.riskCoverage.umpdLimitId = limitDefault.umpdLimitId;
        this.submissionData.riskDetail.riskCoverage.uimbiLimitId = limitDefault.uimbiLimitId;
        this.submissionData.riskDetail.riskCoverage.uimpdLimitId = limitDefault.uimpdLimitId;
        this.submissionData.riskDetail.riskCoverage.pipLimitId = limitDefault.pipLimitId;
      }

      if (this.submissionData.riskDetail.riskCoverages !== null && this.submissionData.riskDetail.riskCoverages.length > 0) {
        this.submissionData.riskDetail.riskCoverages.forEach(item => {
          item.umbiLimitId = limitDefault.umbiLimitId;
          item.umpdLimitId = limitDefault.umpdLimitId;
          item.uimbiLimitId = limitDefault.uimbiLimitId;
          item.uimpdLimitId = limitDefault.uimpdLimitId;
          item.pipLimitId = limitDefault.pipLimitId;
        });
      }
    }

    // Set autobi and medpay ALL to 23 and 1
    this.limitsForm.limitsPageForm.controls['autoBILimitId'].patchValue(LimitsDefaultValues.autoBILimitId);
    this.limitsForm.limitsPageForm.controls['autoPDLimitId'].patchValue(LimitsDefaultValues.autoPDSingleLimitId);
    this.limitsForm.limitsPageForm.controls['medPayLimitId'].patchValue(LimitsDefaultValues.medPayLimitId);
    this.limitsForm.limitsPageForm.controls['liabilityDeductibleId'].patchValue(LimitsDefaultValues.liabilityLimitId);

    if (this.submissionData.riskDetail?.riskCoverage != null) {
      this.submissionData.riskDetail.riskCoverage.autoBILimitId = LimitsDefaultValues.autoBILimitId;
      this.submissionData.riskDetail.riskCoverage.autoPDLimitId = LimitsDefaultValues.autoPDSingleLimitId;
      this.submissionData.riskDetail.riskCoverage.medPayLimitId = LimitsDefaultValues.medPayLimitId;
    }

    if (this.submissionData.riskDetail.riskCoverages !== null && this.submissionData.riskDetail.riskCoverages.length > 0) {
      this.submissionData.riskDetail.riskCoverages.forEach(item => {
        item.autoBILimitId = LimitsDefaultValues.autoBILimitId;
        item.autoPDLimitId = LimitsDefaultValues.autoPDSingleLimitId;
        item.medPayLimitId = LimitsDefaultValues.medPayLimitId;
      });
    }

    this.limitsForm.limitsPageForm.controls['compDeductibleId'].patchValue(LimitsDefaultValues.compId);
    this.limitsForm.limitsPageForm.controls['collDeductibleId'].patchValue(LimitsDefaultValues.colId);

    this.limitsForm.limitsPageForm.controls['glbiLimitId'].patchValue(LimitsDefaultValues.glId);
    this.limitsForm.limitsPageForm.controls['cargoLimitId'].patchValue(LimitsDefaultValues.cargoId);
    this.limitsForm.limitsPageForm.controls['refCargoLimitId'].patchValue(LimitsDefaultValues.refId);

    this.limitsForm.limitsPageForm.controls['alSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['pdSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['umbiLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['umpdLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['uimbiLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['uimpdLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['medPayLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['pipLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);

    this.limitsForm.limitsPageForm.controls['compDeductibleSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['collDeductibleSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['cargoLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
    this.limitsForm.limitsPageForm.controls['refCargoLimitSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
  }

  retrieveDropDownValues(isInitial: boolean = false) {
    this.umBiLimitList = [];
    this.umPdLimitList = [];
    this.uimBiLimitList = [];
    this.uimPdLimitList = [];
    this.pipList = [];

    this.isUmBiAvailable = false;
    this.isUmPdAvailable = false;
    this.isUimBiAvailable = false;
    this.isUimPdAvailable = false;
    this.isPipAvailable = false;
    const state = this.submissionData?.riskDetail?.nameAndAddress?.state;
    // Autoliability/Property
    //Utils.blockUI();
    this.limitService.getLimits(state).pipe(take(1)).subscribe(data => {
      if (data.umbiLimits && data.umbiLimits.length > 0) {
        this.umBiLimitList = this.customConvertEnumerableListToSelectItemList(data.umbiLimits);
        this.isUmBiAvailable = true;
      }

      if (data.umpdLimits && data.umpdLimits.length > 0) {
        this.umPdLimitList = this.customConvertEnumerableListToSelectItemList(data.umpdLimits);
        this.isUmPdAvailable = true;
      }

      if (data.uimbiLimits && data.uimbiLimits.length > 0) {
        this.uimBiLimitList = this.customConvertEnumerableListToSelectItemList(data.uimbiLimits);
        this.isUimBiAvailable = true;
      }

      if (data.uimpdLimits && data.uimpdLimits.length > 0) {
        this.uimPdLimitList = this.customConvertEnumerableListToSelectItemList(data.uimpdLimits);
        this.isUimPdAvailable = true;
      }

      if (data.pipLimits && data.pipLimits.length > 0) {
        this.pipList = this.customConvertEnumerableListToSelectItemList(data.pipLimits);
        this.isPipAvailable = true;
      }

      this.biLimitList = this.customConvertEnumerableListToSelectItemList(data.autoBILimits);
      this.pdLimitList = this.customConvertEnumerableListToSelectItemList(data.autoPDLimits);
      this.liabDeductibleList = this.customConvertEnumerableListToSelectItemList(data.liabilityDeductibleLimits);
      this.medpayList = (['NJ', 'CA'].includes(state)) ? [] : this.customConvertEnumerableListToSelectItemList(data.medPayLimits);
      this.ComprehensiveList = this.customConvertEnumerableListToSelectItemList(data.comprehensiveLimits);
      this.CollisionList = this.customConvertEnumerableListToSelectItemList(data.collisionLimits);

      this.glList = this.sortGlList(this.customConvertEnumerableListToSelectItemList(data.glLimits));
      this.cargoList = this.customConvertEnumerableListToSelectItemList(data.cargoLimits);
      this.refBreakList = this.customConvertEnumerableListToSelectItemList(data.refLimits);

      this.setRequiredFields();

      // Manual Update of menu badge because of this async http method\
      if (!isInitial) {
        this.submissionData.validationList.limits = FormUtils.validateForm(this.limitsForm.limitsPageForm);
        this.layoutService.updateMenu(createSubmissionDetailsMenuItems(this.submissionData.riskDetail.id, this.submissionData.validationList,
          this.riskSpecificsData.hiddenNavItems));
      }

      this.checkAndSetPdDisplayAndValue();
      //Utils.unblockUI();
    });
  }

  checkAndSetPdDisplayAndValue(isChange: boolean = false) {
    const alValue = this.limitsForm.limitsPageForm.controls['autoBILimitId'].value;
    const alSymbolValue = this.limitsForm.limitsPageForm.controls['alSymbolId'].value;
    if (alValue == null || alValue === '') {
      this.showPD = false;
      this.resetField('autoPDLimitId');
      this.resetField('pdSymbolId');
      this.limitsForm.limitsPageForm.controls['autoPDLimitId'].patchValue(null);
      this.limitsForm.limitsPageForm.controls['pdSymbolId'].patchValue(null);
      return;
    }

    const alLabel = this.biLimitList.find(x => x.value == alValue)?.label;

    if (alLabel) {
      if (alLabel.toString().indexOf('/') !== -1) {
        this.showPD = true;
        this.setAsRequired('autoPDLimitId');
        this.setAsRequired('pdSymbolId');
        if (isChange) {
          this.limitsForm.limitsPageForm.controls['autoPDLimitId'].patchValue(LimitsDefaultValues.autoPDSplitLimitId);
          this.limitsForm.limitsPageForm.controls['pdSymbolId'].patchValue(LimitsDefaultValues.defaultSymbol);
        }
      } else {
        this.showPD = false;
        this.resetField('autoPDLimitId');
        this.resetField('pdSymbolId');
        const pdValue = this.pdLimitList.find(x => x.label == alLabel)?.value;
        this.limitsForm.limitsPageForm.controls['autoPDLimitId'].patchValue(pdValue);
        this.limitsForm.limitsPageForm.controls['pdSymbolId'].patchValue(alSymbolValue);
      }
    }
  }

  // Remove coma in value
  customConvertEnumerableListToSelectItemList(list: any[]): SelectItem[] {
    const convertedList: SelectItem[] = [];
    if (list) {
        for (const item of list) {
            convertedList.push({
                label: item.limitDisplay,
                value: item.id
            });
        }
    }

    return convertedList;
  }

  setRequiredFields() {
    if (this.isUmBiAvailable) { this.setAsRequired('umbiLimitId'); } else { this.resetField('umbiLimitId'); }
    if (this.isUmPdAvailable) { this.setAsRequired('umpdLimitId'); } else { this.resetField('umpdLimitId'); }
    if (this.isUimBiAvailable) { this.setAsRequired('uimbiLimitId'); } else { this.resetField('uimbiLimitId'); }
    if (this.isUimPdAvailable) { this.setAsRequired('uimpdLimitId'); } else { this.resetField('uimpdLimitId'); }
    if (this.isPipAvailable) { this.setAsRequired('pipLimitId'); } else { this.resetField('pipLimitId'); }
  }

  Reset() {
    this.isGeneralLiability = false;
    this.isCargo = false;
    this.isRefrigeration = false;
    this.isCollision = false;
    this.isComprehensive = false;
    this.showPD = false;
  }

  get f() { return this.limitsForm.limitsPageForm.controls; }

  private resetField(fieldName: string) {
    this.f[fieldName].reset();
    this.f[fieldName].clearValidators();
    this.f[fieldName].updateValueAndValidity();
  }

  private setAsRequired(fieldName: string) {
    this.f[fieldName].setValidators(Validators.required);
    this.f[fieldName].updateValueAndValidity();
  }

  // Services
  saveRiskCoverageLimit(callbacAfterSave?: Function) {
    const option = (this.submissionData.isIssued) ? this.submissionData.riskDetail?.binding?.bindOptionId ?? 1 : 1;
    const coverageLimit = new SaveRiskCoverageDTO({
      riskDetailId: this.submissionData.riskDetail.id,
      autoBILimitId: this.limitsForm.limitsPageForm.value.autoBILimitId ? parseInt(this.limitsForm.limitsPageForm.value.autoBILimitId) : null,
      autoPDLimitId: this.limitsForm.limitsPageForm.value.autoPDLimitId ? parseInt(this.limitsForm.limitsPageForm.value.autoPDLimitId) : null,
      umbiLimitId: this.limitsForm.limitsPageForm.value.umbiLimitId ? parseInt(this.limitsForm.limitsPageForm.value.umbiLimitId) : null,
      umpdLimitId: this.limitsForm.limitsPageForm.value.umpdLimitId ? parseInt(this.limitsForm.limitsPageForm.value.umpdLimitId) : null,
      uimbiLimitId: this.limitsForm.limitsPageForm.value.uimbiLimitId ? parseInt(this.limitsForm.limitsPageForm.value.uimbiLimitId) : null,
      uimpdLimitId: this.limitsForm.limitsPageForm.value.uimpdLimitId ? parseInt(this.limitsForm.limitsPageForm.value.uimpdLimitId) : null,
      medPayLimitId: this.limitsForm.limitsPageForm.value.medPayLimitId ? parseInt(this.limitsForm.limitsPageForm.value.medPayLimitId) : null,
      pipLimitId: this.limitsForm.limitsPageForm.value.pipLimitId ? parseInt(this.limitsForm.limitsPageForm.value.pipLimitId) : null,
      liabilityDeductibleId: this.limitsForm.limitsPageForm.value.liabilityDeductibleId ? parseInt(this.limitsForm.limitsPageForm.value.liabilityDeductibleId) : null,
      collDeductibleId: this.limitsForm.limitsPageForm.value.collDeductibleId ? parseInt(this.limitsForm.limitsPageForm.value.collDeductibleId) : null,
      glbiLimitId: this.limitsForm.limitsPageForm.value.glbiLimitId ? parseInt(this.limitsForm.limitsPageForm.value.glbiLimitId) : null,
      cargoLimitId: this.limitsForm.limitsPageForm.value.cargoLimitId ? parseInt(this.limitsForm.limitsPageForm.value.cargoLimitId) : null,
      refCargoLimitId: this.limitsForm.limitsPageForm.value.refCargoLimitId ? parseInt(this.limitsForm.limitsPageForm.value.refCargoLimitId) : null,
      compDeductibleId: this.limitsForm.limitsPageForm.value.compDeductibleId && this.limitsForm.limitsPageForm.value.comprehensiveFire ? parseInt(this.limitsForm.limitsPageForm.value.compDeductibleId) : null,
      fireDeductibleId: this.limitsForm.limitsPageForm.value.compDeductibleId && !this.limitsForm.limitsPageForm.value.comprehensiveFire ? parseInt(this.limitsForm.limitsPageForm.value.compDeductibleId) : null,

      alSymbolId: this.limitsForm.limitsPageForm.value.alSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.alSymbolId) : null,
      pdSymbolId: this.limitsForm.limitsPageForm.value.pdSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.pdSymbolId) : null,
      liabilityDeductibleSymbolId: this.limitsForm.limitsPageForm.value.liabilityDeductibleSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.liabilityDeductibleSymbolId) : null,
      medPayLimitSymbolId: this.limitsForm.limitsPageForm.value.medPayLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.medPayLimitSymbolId) : null,
      pipLimitSymbolId: this.limitsForm.limitsPageForm.value.pipLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.pipLimitSymbolId) : null,
      umbiLimitSymbolId: this.limitsForm.limitsPageForm.value.umbiLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.umbiLimitSymbolId) : null,
      umpdLimitSymbolId: this.limitsForm.limitsPageForm.value.umpdLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.umpdLimitSymbolId) : null,
      uimbiLimitSymbolId: this.limitsForm.limitsPageForm.value.uimbiLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.uimbiLimitSymbolId) : null,
      uimpdLimitSymbolId: this.limitsForm.limitsPageForm.value.uimpdLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.uimpdLimitSymbolId) : null,
      collDeductibleSymbolId: this.limitsForm.limitsPageForm.value.collDeductibleSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.collDeductibleSymbolId) : null,
      glbiLimitSymbolId: this.limitsForm.limitsPageForm.value.glbiLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.glbiLimitSymbolId) : null,
      cargoLimitSymbolId: this.limitsForm.limitsPageForm.value.cargoLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.cargoLimitSymbolId) : null,
      refCargoLimitSymbolId: this.limitsForm.limitsPageForm.value.refCargoLimitSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.refCargoLimitSymbolId) : null,

      compDeductibleSymbolId: this.limitsForm.limitsPageForm.value.compDeductibleSymbolId && this.limitsForm.limitsPageForm.value.comprehensiveFire ? parseInt(this.limitsForm.limitsPageForm.value.compDeductibleSymbolId) : null,
      fireDeductibleSymbolId: this.limitsForm.limitsPageForm.value.compDeductibleSymbolId && !this.limitsForm.limitsPageForm.value.comprehensiveFire ? parseInt(this.limitsForm.limitsPageForm.value.compDeductibleSymbolId) : null,
      optionNumber: Number(option)
    });
    this.submissionData.riskDetail.riskCoverage = coverageLimit;

    // Set Quote Options 1
    if (this.submissionData.riskDetail.riskCoverages != null && this.submissionData.riskDetail.riskCoverages?.length > 0) {
      const option1 = this.submissionData.riskDetail.riskCoverages.find(x => x.optionNumber === Number(option));
      const index = this.submissionData.riskDetail.riskCoverages.indexOf(option1);
      this.submissionData.riskDetail.riskCoverages[index].autoBILimitId = coverageLimit.autoBILimitId;
      this.submissionData.riskDetail.riskCoverages[index].autoPDLimitId = coverageLimit.autoPDLimitId;
      this.submissionData.riskDetail.riskCoverages[index].umbiLimitId = coverageLimit.umbiLimitId;
      this.submissionData.riskDetail.riskCoverages[index].umpdLimitId = coverageLimit.umpdLimitId;
      this.submissionData.riskDetail.riskCoverages[index].uimbiLimitId = coverageLimit.uimbiLimitId;
      this.submissionData.riskDetail.riskCoverages[index].uimpdLimitId = coverageLimit.uimpdLimitId;
      this.submissionData.riskDetail.riskCoverages[index].medPayLimitId = coverageLimit.medPayLimitId;
      this.submissionData.riskDetail.riskCoverages[index].pipLimitId = coverageLimit.pipLimitId;
      this.submissionData.riskDetail.riskCoverages[index].liabilityDeductibleId = coverageLimit.liabilityDeductibleId;
      this.submissionData.riskDetail.riskCoverages[index].collDeductibleId = coverageLimit.collDeductibleId;
      this.submissionData.riskDetail.riskCoverages[index].glbiLimitId = coverageLimit.glbiLimitId;
      this.submissionData.riskDetail.riskCoverages[index].cargoLimitId = coverageLimit.cargoLimitId;
      this.submissionData.riskDetail.riskCoverages[index].refCargoLimitId = coverageLimit.refCargoLimitId;
      this.submissionData.riskDetail.riskCoverages[index].compDeductibleId = coverageLimit.compDeductibleId;
      this.submissionData.riskDetail.riskCoverages[index].fireDeductibleId = coverageLimit.fireDeductibleId;
      this.submissionData.riskDetail.riskCoverages[index].alSymbolId = coverageLimit.alSymbolId;
      this.submissionData.riskDetail.riskCoverages[index].pdSymbolId = coverageLimit.pdSymbolId;
    }

    this.updateUninsuredMotoristBIForms();
    this.updateRefCargoForms();

    Utils.blockUI();
    this.limitService.saveRiskCoverageLimit(coverageLimit).pipe(take(1)).subscribe(data => {
      Utils.unblockUI();
      this.submissionData.riskDetail.riskCoverage = data;
      if (callbacAfterSave) { callbacAfterSave(); }
      console.log('Limits Saved!');
    }, (error) => {
      if (callbacAfterSave) { callbacAfterSave(); }
      Utils.unblockUI();
      this.toastr.error(ErrorMessageConstant.savingErrorMsg);
    });
  }

  updateUninsuredMotoristBIForms() {
    if (this.submissionData.riskDetail.riskForms?.length > 0) {
      const form = this.submissionData.riskDetail.riskForms.find(x => x.form?.id === 'UninsuredMotoristCoverageBodilyInjury');
      if (form != null) {
        const index = this.submissionData.riskDetail.riskForms.indexOf(form);
        this.submissionData.riskDetail.riskForms[index].isSelected = false;
        if (this.hasUninsuredMotoristBI) {
          this.submissionData.riskDetail.riskForms[index].isSelected = true;
        }
      }
      const splitform = this.submissionData.riskDetail.riskForms.find(x => x.form?.id === 'SplitBodilyInjuryUninsuredMotoristCoverage');
      if (splitform != null) {
        const index = this.submissionData.riskDetail.riskForms.indexOf(splitform);
        this.submissionData.riskDetail.riskForms[index].isSelected = false;
        if (this.hasUninsuredMotoristBI) {
          this.submissionData.riskDetail.riskForms[index].isSelected = true;
        }
      }
    }
  }

  updateRefCargoForms() {
    if (this.submissionData.riskDetail.riskForms?.length > 0) {
      const form = this.submissionData.riskDetail.riskForms.find(x => x.form?.id === 'RefrigerationCargoEndorsement');
      if (form != null) {
        const index = this.submissionData.riskDetail.riskForms.indexOf(form);
        this.submissionData.riskDetail.riskForms[index].isSelected = false;
        if (this.hasRefrigeration) {
          this.submissionData.riskDetail.riskForms[index].isSelected = true;
        }
      }
    }
  }

  setDefaultRiskCoverageLimit() {
    const coverageLimit = new SaveRiskCoverageDTO({
      riskDetailId: this.submissionData.riskDetail.id,
      autoBILimitId: this.limitsForm.limitsPageForm.value.autoBILimitId ? parseInt(this.limitsForm.limitsPageForm.value.autoBILimitId) : null,
      autoPDLimitId: this.limitsForm.limitsPageForm.value.autoPDLimitId ? parseInt(this.limitsForm.limitsPageForm.value.autoPDLimitId) : null,
      pdSymbolId: this.limitsForm.limitsPageForm.value.pdSymbolId ? parseInt(this.limitsForm.limitsPageForm.value.pdSymbolId) : null,
      umbiLimitId: this.limitsForm.limitsPageForm.value.umbiLimitId ? parseInt(this.limitsForm.limitsPageForm.value.umbiLimitId) : null,
      umpdLimitId: this.limitsForm.limitsPageForm.value.umpdLimitId ? parseInt(this.limitsForm.limitsPageForm.value.umpdLimitId) : null,
      uimbiLimitId: this.limitsForm.limitsPageForm.value.uimbiLimitId ? parseInt(this.limitsForm.limitsPageForm.value.uimbiLimitId) : null,
      uimpdLimitId: this.limitsForm.limitsPageForm.value.uimpdLimitId ? parseInt(this.limitsForm.limitsPageForm.value.uimpdLimitId) : null,
      pipLimitId: this.limitsForm.limitsPageForm.value.pipLimitId ? parseInt(this.limitsForm.limitsPageForm.value.pipLimitId) : null,
      medPayLimitId: this.limitsForm.limitsPageForm.value.medPayLimitId ? parseInt(this.limitsForm.limitsPageForm.value.medPayLimitId) : null,
    });
    Utils.blockUI();
    this.limitService.setDefaultRiskCoverageLimit(coverageLimit).pipe(take(1)).subscribe(data => {
      Utils.unblockUI();
      console.log('Limits default Saved!');
    }, (error) => {
      Utils.unblockUI();
      this.toastr.error(ErrorMessageConstant.savingErrorMsg);
    });
  }

  setCoveragesDisplay() {
    const coverage = this.submissionData?.riskDetail?.riskCoverage;
    if (coverage) {
      this.isCollision = coverage.collDeductibleId && coverage.collDeductibleId !== LimitsDefaultValues.colId;
      this.isComprehensive = (coverage.compDeductibleId && coverage.compDeductibleId !== LimitsDefaultValues.compId) ||
      (coverage.fireDeductibleId && coverage.fireDeductibleId !== LimitsDefaultValues.compId);
      this.isCargo = coverage.cargoLimitId && coverage.cargoLimitId !== LimitsDefaultValues.cargoId;
      this.isRefrigeration = coverage.refCargoLimitId && coverage.refCargoLimitId !== LimitsDefaultValues.refId;
    }
  }

  private sortGlList(glList: any[]) {
    glList = glList.sort((a, b) => b.value === 87 || a.value < b.value ? 1 : -1);
    return glList;
  }

}

