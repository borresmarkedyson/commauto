import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from '@app/shared/models/dynamic/select-item';
import { take } from 'rxjs/operators';
import { DestinationDto } from '../../../../shared/models/submission/riskspecifics/destinationDto';
import { FormsService } from '../../services/forms.service';

@Injectable()
export class DestinationData {
  destinationForm: FormGroup;
  stateList: any = [];

  constructor(private formBuilder: FormBuilder, private formsService: FormsService) { }

  initiateFormGroup(obj: DestinationDto): FormGroup {
    this.destinationForm = this.formBuilder.group({
      id: [this.setInitialValue(obj.id, null)],
      city: [this.setInitialValue(obj.city, ''), Validators.required],
      state: [this.setInitialValue(obj.state, null), Validators.required],
      zipCode: [this.setInitialValue(obj.zipCode, ''), [Validators.required, Validators.min(1)]],
      percentageOfTravel: [this.setInitialValue(obj.percentageOfTravel, ''), [Validators.required, Validators.min(0.1)]],
      totalPercentageOfTravel: [obj.totalPercentageOfTravel],
      currentDestinations: [obj.currentDestinations]
    }, { validators: [this.validatePercentageOfTravel, this.validateDestination] });

    this.getStates();
    return this.destinationForm;
  }

  validatePercentageOfTravel(form: FormGroup) {
    let totalPercentageOfTravel: number;
    const percentageOfTravel = form.controls['percentageOfTravel'].value;
    const currentPercentageOfTravel = form.controls['totalPercentageOfTravel'].value;

    totalPercentageOfTravel = +(Number(currentPercentageOfTravel) + Number(percentageOfTravel)).toFixed(1);
    return totalPercentageOfTravel > 100.0 ? { 'invalidPercentageOfTravel': true } : null;
  }

  validateDestination(form: FormGroup) {
    const currentDestinations = form.controls['currentDestinations']?.value as Array<string>;
    const city = form.controls['city']?.value;
    const state = form.controls['state']?.value;
    const zipCode = form.controls['zipCode']?.value;

    const selectedDestination = city?.toLowerCase()?.trim() + state + zipCode?.trim();
    return currentDestinations?.includes(selectedDestination) ? { 'duplicateDestination': true } : null;
  }

  initiateFormValues(fb: FormGroup) {
  }

  public setInitialValue(obj: any, val: any) {
    return (obj == null || obj.length === 0) ? val : obj;
  }

  getStates() {
    this.formsService.getStates().pipe(take(1)).subscribe(data => {
      this.stateList = data.map(s => ({
        value: s.stateCode?.toString(),
        label: s.fullStateName?.toString()?.replace('[E]', '')
      })) as SelectItem[];
    });
  }
}
