import { Injectable } from '@angular/core';
import { take, map, mergeMap, tap, finalize } from 'rxjs/operators';
import { forkJoin, Observable, Subject } from 'rxjs';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from '../../../../../app/shared/models/dynamic/select-item';
import { VehicleDto } from '../../../../shared/models/submission/Vehicle/VehicleDto';
import { LvAccountCategory, LvMainUse } from '../../../../shared/constants/business-details.options.constants';
import { Options } from '../../../../shared/constants/vehicle-driver.options.constants';
import { VehicleService } from '../../services/vehicle.service';
import { SubmissionData } from '../submission.data';
import { ApplicantNameAddressService } from '../../services/applicant-name-address.service';
import { VehicleModel } from '../../../../shared/models/submission/Vehicle/vehicle.model';
import { LimitsService } from '../../services/coverage-limits.service';
import { PagerService } from '../../../../core/services/pager.service';
import { NameAndAddressDTO } from '../../../../shared/models/submission/applicant/applicant-name-address';
import { LimitsDefaultValues } from '../../../../shared/constants/limits-defaults.constants';
import { data } from 'jquery';
import { LimitsData } from '../limits/limits.data';
import { TableData } from '../tables.data';
import Utils from '@app/shared/utilities/utils';
import { RiskCoverageDTO } from '@app/shared/models/submission/limits/riskCoverageDto';
import { PolicyHistoryData } from '@app/modules/policy/data/policy-history.data';
import { PolicyHistoryDTO } from '@app/shared/models/policy/PolicyHistoryDto';

@Injectable(
  { providedIn: 'root' }
)
export class VehicleData {

  riskDetailId: string = '';
  form: FormGroup;
  vehicleDropdownsList: VehicleInformationDropDowns = new VehicleInformationDropDowns();
  vehicleInfoList: Array<VehicleModel> = new Array<VehicleModel>();
  pagedItems: VehicleModel[];
  applicant: any = {};

  searchFilter: string;
  pager: any = {};
  loading: boolean;
  currentPage: number = 1;

  policyHistories: PolicyHistoryDTO[] = [];

  isVehicleUpdated: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private vehicleService: VehicleService,
    private applicantNameAddressService: ApplicantNameAddressService,
    private limitsData: LimitsData,
    private limitService: LimitsService,
    private submissionData: SubmissionData,
    private tableData: TableData,
    private policyHistoryData: PolicyHistoryData,
    private pagerService: PagerService) { }

  initiateFormGroup(obj: VehicleDto): FormGroup {

    this.form = this.formBuilder.group({
      id: [''],
      options: [''],
      includes: [''],
      riskDetailId: [''],
      year: ['', Validators.required],
      age: [''],
      make: ['', Validators.required],
      model: ['', Validators.required],
      vin: ['', [Validators.required, this.validateVinChars, this.validateVinLength]],
      style: [''],
      vehicleType: ['', Validators.required],
      isAutoLiability: new FormControl({ value: true, disabled: true }),
      grossVehicleWeightId: ['', Validators.required],
      grossVehicleWeightCategoryId: [0],
      vehicleDescriptionId: ['', Validators.required],
      vehicleBusinessClassId: ['', Validators.required],
      isCoverageFireTheft: [true],
      coverageFireTheftDeductibleId: [0],
      isCoverageCollision: [true],
      coverageCollisionDeductibleId: [0],
      statedAmount: [0, [Validators.required, Validators.min(1)]],
      hasCoverageCargo: [true],
      hasCoverageRefrigeration: [true],
      useClassId: ['', Validators.required],
      garagingAddressId: ['', Validators.required],
      primaryOperatingZipCode: ['', Validators.required],
      state: [''],
      registeredState: ['', Validators.required],
      radius: [null],
      radiusTerritory: [null],
      autoLiabilityPremium: [null],
      cargoPremium: [null],
      physicalDamagePremium: [null],
      totalPremium: [null],
      createdDate: [''],
      createdBy: [null],
      savedVins: [[]],
      isValidated: [null],
      effectiveDate: [null],
      expirationDate: [null],
    }, { validators: this.validateDuplicateVin });
    return this.form;
  }

  private validateVinChars(control: AbstractControl): { [key: string]: boolean } | null {
    const pattern = new RegExp(/^[A-HJ-NPR-Za-hj-npr-z0-9]*$/);
    return !pattern.test(String(control.value)) ? { 'vinInvalidChar': true } : null;
  }

  private validateVinLength(control: AbstractControl) {
    let value = control.value;
    if (value && isNaN(+value) && value.length < 17) {
      return { 'vinInvalidLength': true };
    }
    return null;
  }

  private validateDuplicateVin(form: FormGroup) {
    const savedVins = form?.controls['savedVins'].value as Array<string>;
    const vin = String(form.controls['vin'].value);

    const savedVin = savedVins.find(sv => sv?.toLowerCase() === vin?.toLowerCase());
    return savedVin ? { 'vinDuplicate': true } : null;
  }

  initiateFormValues(vehicle: VehicleDto, savedVins?: string[], isViewOnly: boolean = false) {
    const model = new VehicleModel(vehicle);
    this.riskDetailId = this.submissionData?.riskDetail?.id;
    this.form.patchValue(model);
    this.form.patchValue({ savedVins: savedVins });

    if (isViewOnly) {
      this.form.get('hasCoverageCargo').disable();
      this.form.get('hasCoverageRefrigeration').disable();
      this.form.get('isCoverageFireTheft').disable();
      this.form.get('isCoverageCollision').disable();
      this.form.get('year').disable();
      this.form.get('make').disable();
      this.form.get('model').disable();
      this.form.get('vin').disable();
      this.form.get('registeredState').disable();
      this.form.get('style').disable();
      this.form.get('vehicleType').disable();
      this.form.get('grossVehicleWeightId').disable();
      this.form.get('vehicleDescriptionId').disable();
      this.form.get('vehicleBusinessClassId').disable();
      this.form.get('coverageFireTheftDeductibleId').disable();
      this.form.get('coverageCollisionDeductibleId').disable();
      this.form.get('statedAmount').disable();
      this.form.get('useClassId').disable();
      this.form.get('garagingAddressId').disable();
      this.form.get('primaryOperatingZipCode').disable();
      this.form.markAsUntouched();
      this.form.markAsPristine();
      return;
    } else {
      this.form.get('hasCoverageCargo').enable();
      this.form.get('hasCoverageRefrigeration').enable();
      this.form.get('isCoverageFireTheft').enable();
      this.form.get('isCoverageCollision').enable();
      this.form.get('year').enable();
      this.form.get('make').enable();
      this.form.get('model').enable();
      // this.form.get('vin').enable();
      this.form.get('registeredState').enable();
      this.form.get('style').enable();
      this.form.get('vehicleType').enable();
      this.form.get('grossVehicleWeightId').enable();
      this.form.get('vehicleDescriptionId').enable();
      this.form.get('vehicleBusinessClassId').enable();
      this.form.get('coverageFireTheftDeductibleId').enable();
      this.form.get('coverageCollisionDeductibleId').enable();
      this.form.get('statedAmount').enable();
      this.form.get('useClassId').enable();
      this.form.get('garagingAddressId').enable();
      this.form.get('primaryOperatingZipCode').enable();
    }

    // enable disable coverages per vehicle based on limits
    if (!this.limitsData.hasCargo) {
      this.form.get('hasCoverageCargo').setValue(false);
      this.form.get('hasCoverageCargo').disable();
    }
    if (!this.form.get('hasCoverageCargo').value || !this.limitsData.hasRefrigeration) {
      this.form.get('hasCoverageRefrigeration').setValue(false);
      this.form.get('hasCoverageRefrigeration').disable();
    } else {
      this.form.get('hasCoverageRefrigeration').enable();
      this.form.get('hasCoverageCargo').enable();
    }

    if (!this.limitsData.hasComprehensive) {
      this.form.get('isCoverageFireTheft').setValue(false);
      this.form.get('isCoverageFireTheft').disable();
    } else {
      this.form.get('isCoverageFireTheft').enable();
    }

    if (!this.limitsData.hasCollision) {
      this.form.get('isCoverageCollision').setValue(false);
      this.form.get('isCoverageCollision').disable();
    } else {
      this.form.get('isCoverageCollision').enable();
    }

    if (model.id) {
      this.form.markAllAsTouched();
    } else {
      this.form.markAsUntouched();
      this.form.markAsPristine();
    }
  }

  populateVehicleField() {
    this.retrieveDropDownValues();
    this.renderVehicleList();
  }

  setPage(page: number, hasAdded: boolean = false) {
    if (page < 1) {
      return;
    }

    let vehicles = Object.assign([], this.vehicleInfoList);
    if (this.searchFilter) {
      vehicles = this.submissionData.riskDetail.vehicles.filter(v => v.vin?.trim().toLocaleLowerCase().includes(this.searchFilter)) as any[];
    }

    this.pager = this.pagerService.getPager(vehicles.length, page);

    if (hasAdded) {
      this.currentPage = this.pager.currentPage = this.pager.totalPages;
      this.pager.endIndex = this.pager.totalItems;
      this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);

      this.pagedItems = vehicles.slice(this.pager.startIndex, this.pager.endIndex + 1);
    } else {
      while (this.pager.totalItems <= ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize)) {
        this.pager.currentPage--;
        this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);
      }

      this.pagedItems = vehicles.slice(this.pager.startIndex, this.pager.endIndex + 1);
      this.currentPage = page;
    }
  }

  uploadExcelFile(inputData?: any): Observable<any> {
    return this.vehicleService.uploadExcelFile(inputData);
  }

  downloadTemplate() {
    return this.vehicleService.downloadTemplate();
  }

  retrieveDropDownValues() {
    return this.vehicleDropdownsList = {

      yearList: this.generateYearList(1960),

      vehicleTypeList: [
        { value: 'Power Unit', label: 'Power Unit' },
        { value: 'Trailer', label: 'Trailer' },
        { value: 'Other', label: 'Other' },
      ],

      grossVehicleWeightList: [
        { value: 1, label: '0 - 10,000' },
        { value: 2, label: '10,001 - 20,000' },
        { value: 3, label: '20,001 - 45,000' },
        { value: 4, label: '45,001 +' },
      ],

      grossVehicleWeightCategoryList: [
        { value: 1, label: 'Light' },
        { value: 2, label: 'Medium' },
        { value: 3, label: 'Heavy' },
        { value: 4, label: 'Extra Heavy (including tractors)' },
      ],

      vehicleDescriptionList: [
        // manually sorted
        { value: 38, label: 'Delivery Van>10k GVW' },
        { value: 16, label: 'Dry Freight Trailer' },
        { value: 21, label: 'Dump Body Trailer' },
        { value: 2, label: 'Dump Truck 0-16k GVW' },
        { value: 66, label: 'Dump Truck 33-45k GVW' },
        { value: 65, label: 'Dump Truck >45k GVW' },
        { value: 8, label: 'Full size Van' },
        { value: 60, label: 'Garbage Truck 0-45 GVW' },
        { value: 68, label: 'Garbage Truck >45 GVW' },
        { value: 5, label: 'Pickup<= 1/2 Ton 4x2' },
        { value: 6, label: 'Pickup<= 1/2 Ton 4x4' },
        { value: 7, label: 'Pickup >  1/2 Ton 4x4' },
        { value: 34, label: 'Private Passenger Auto' },
        { value: 22, label: 'Refrigerated Dry Freight Trailer' },
        { value: 44, label: 'Refrigerated Truck 0-16k GVW' },
        { value: 45, label: 'Refrigerated Truck 16-26k GVW' },
        { value: 46, label: 'Refrigerated Truck >26k GVW' },
        { value: 37, label: 'Step Van 0-10k GVW' },
        { value: 41, label: ' Straight Truck 0-16k GVW' },
        { value: 42, label: ' Straight Truck 16-26k GVW' },
        { value: 43, label: ' Straight Truck >26K GVW' },
        { value: 17, label: 'Tank Trailer' },
        { value: 1, label: 'Tractor' }
        // old dataset
        // { value: 1, label: 'Tractor' },
        // { value: 2, label: 'Dump Truck 0-16k GVW' },
        // { value: 3, label: 'Flatbed Truck 0-16 GVW' },
        // { value: 4, label: 'Tow Truck - one axle' },
        // { value: 5, label: 'Pickup<= 1/2 Ton 4x2' },
        // { value: 6, label: 'Pickup<= 1/2 Ton 4x4' },
        // { value: 7, label: 'Pickup >  1/2 Ton 4x4' },
        // { value: 8, label: 'Full size Van' },
        // { value: 9, label: 'Wheelchair Van' },
        // { value: 10, label: 'Wheelchair Bus' },
        // { value: 13, label: 'Stake Truck 0-16k GVW' },
        // { value: 14, label: 'Tank Truck <= 1400 gals' },
        // { value: 15, label: 'Mini Van' },
        // { value: 16, label: 'Dry Freight Trailer' },
        // { value: 17, label: 'Tank Trailer' },
        // { value: 18, label: 'Flatbed Trailer' },
        // { value: 19, label: 'Pole Trailer' },
        // { value: 20, label: 'Livestock Trailer' },
        // { value: 21, label: 'Dump Body Trailer' },
        // { value: 22, label: 'Refrigerated Dry Freight Trailer' },
        // { value: 23, label: 'Horse Trailer(1-2 stalls)' },
        // { value: 24, label: 'Lowboy Trailer' },
        // { value: 25, label: 'Unidentified Trailer' },
        // { value: 26, label: 'Bulk Commodity Trailer' },
        // { value: 27, label: 'Tilt Trailer' },
        // { value: 28, label: 'Rag Top Trailer' },
        // { value: 29, label: 'Utility Trailer <= 1 ft.' },
        // { value: 30, label: 'Auto Hauling Trailer' },
        // { value: 31, label: 'Sport Auto' },
        // { value: 32, label: 'Utility Trailer > 12 ft' },
        // { value: 33, label: 'Luxury Auto' },
        // { value: 34, label: 'Private Passenger Auto' },
        // { value: 35, label: 'Motor Home' },
        // { value: 36, label: 'Utility Vehicle' },
        // { value: 37, label: 'Step Van 0-10k GVW' },
        // { value: 38, label: 'Delivery Van>10k GVW' },
        // { value: 39, label: 'Limousine' },
        // { value: 40, label: 'Hearse' },
        // { value: 41, label: ' Straight Truck 0-16k GVW' },
        // { value: 42, label: ' Straight Truck 16-26k GVW' },
        // { value: 43, label: ' Straight Truck >26K GVW' },
        // { value: 44, label: 'Refrigerated Truck 0-16k GVW' },
        // { value: 45, label: 'Refrigerated Truck 16-26k GVW' },
        // { value: 46, label: 'Refrigerated Truck >26k GVW' },
        // { value: 47, label: 'Ice Cream Truck' },
        // { value: 48, label: 'Concession Trailer' },
        // { value: 49, label: 'Travel Trailer' },
        // { value: 50, label: 'Cement Mixer' },
        // { value: 51, label: 'Flatbed Truck >26k GVW' },
        // { value: 52, label: 'TW Truck-two axle' },
        // { value: 53, label: 'Pickup >  1/2 Ton 4x2' },
        // { value: 54, label: 'Catering Truck' },
        // { value: 55, label: 'Stake Truck >26k GVW' },
        // { value: 56, label: 'Pickup w/ 5th Wheel 0-16k GVW' },
        // { value: 57, label: 'Dump Truck 16-33k GVW' },
        // { value: 58, label: 'Flatbed Truck 16-26k GVW' },
        // { value: 59, label: 'Front Loader 0-45k' },
        // { value: 60, label: 'Garbage Truck 0-45 GVW' },
        // { value: 61, label: 'Roll On Vehicle 0-45 GVW' },
        // { value: 62, label: 'Stake Truck 16-26k GVW' },
        // { value: 63, label: 'Tank Truck > 1400 gals' },
        // { value: 64, label: 'Logging Trailer' },
        // { value: 65, label: 'Dump Truck >45k GVW' },
        // { value: 66, label: 'Dump Truck 33-45k GVW' },
        // { value: 67, label: 'Front Loader >45k GVW' },
        // { value: 68, label: 'Garbage Truck >45 GVW' },
        // { value: 69, label: 'Roll On Vehicle >45k GVW' },
        // { value: 70, label: 'Bucket Truck/Cherry Picker 0-16k GVW' },
        // { value: 71, label: 'Bucket Truck/Cherry Picker 16-26k GVW' },
        // { value: 72, label: 'Bucket Truck/Cherry Picker >26K GVW' },
        // { value: 73, label: 'Gooseneck Trailer' },
        // { value: 74, label: 'Car Carrier 0-16 GVW' },
        // { value: 75, label: 'Pump Truck(Cement) 0-16k GVW' },
        // { value: 76, label: 'Pump Truck(Cement) 16-26k GVW' },
        // { value: 77, label: 'Pump Truck(Cement) >26k GVW' },
        // { value: 78, label: 'Bus 9-15 passengers' },
        // { value: 79, label: 'Bus 16-60 passengers' },
        // { value: 80, label: 'Bus 61+ passengers' },
        // { value: 81, label: 'Car Carrier >16k GVW' },
        // { value: 82, label: 'Agricultural Hopper Truck' }
      ],

      vehicleBusinessClassList: LvAccountCategory,

      comprehensiveDeductibleList: [],

      collisionDeductibleList: [],

      useClassList: LvMainUse,

      garagingAddressList: [],
      oldGaragingAddressList: [],

      zipCodeList: [],

      optionList: Options

    };

  }

  renderVehicleList(hasAdded?: boolean) {
    if ((this.submissionData.riskDetail.vehicles?.length ?? 0) === 0) { this.pagedItems = []; } // Temp fix for recurring vehicle cache
    if (this.submissionData.isNew) { return; }
    const riskDetailId = this.submissionData.riskDetail?.id;
    let vehicles: any[] = [];
    Utils.blockUI();
    this.vehicleService.getByRiskDetailId(riskDetailId, true).pipe(
      take(1),
      map(data => {
        vehicles = data.filter(v => v.deletedDate == null);
      }),
      mergeMap(() => {
        const garagingAddresses = this.getGaragingAddressList();
        const deductibles = this.getDeductibles();
        return forkJoin([garagingAddresses, deductibles]);
      }),
      finalize(() => Utils.unblockUI())
    )
      .subscribe(data => {
        Utils.unblockUI();
        this.applicant.zipCode = String(data[0].applicantZipCode);
        this.applicant.mainGarageAddressId = data[0].mainGarageAddressId;
        this.vehicleDropdownsList.garagingAddressList = data[0].garagingAddress;
        this.vehicleDropdownsList.zipCodeList = data[0].zipCodes.filter(this.uniqueOnly);
        this.vehicleDropdownsList.comprehensiveDeductibleList = data[1].comprehensive;
        this.vehicleDropdownsList.collisionDeductibleList = data[1].collision;
        vehicles.forEach(v => {
          v = this.formatIdsToText(v);
        });
        this.vehicleInfoList = vehicles.sort(this.sortByVIN);
        this.submissionData.riskDetail.vehicles = this.vehicleInfoList;
        this.setPage(this.currentPage, hasAdded);
      });

    this.getStateList();
  }

  renderPolicyVehicleListObservable(hasAdded?: boolean): Observable<any> {
    if ((this.submissionData.riskDetail.vehicles?.length ?? 0) === 0) { this.pagedItems = []; } // Temp fix for recurring vehicle cache
    if (this.submissionData.isNew) { return; }
    const riskDetail = this.submissionData.riskDetail;
    const riskDetailId = riskDetail?.id;
    let vehicles: any[] = [];

    return this.vehicleService.getByRiskDetailId(riskDetailId, true).pipe(
      take(1),
      map(data => {
        vehicles = data as VehicleDto[];
        vehicles.forEach((v: VehicleDto) => v.isMainRow = true);
      }),
      mergeMap(() => {
        const garagingAddresses = this.getGaragingAddressList();
        const deductibles = this.getDeductibles();
        return forkJoin([garagingAddresses, deductibles]);
      }),
      tap((data) => {
        this.applicant.zipCode = String(data[0].applicantZipCode);
        this.applicant.mainGarageAddressId = data[0].mainGarageAddressId;
        this.vehicleDropdownsList.garagingAddressList = data[0].garagingAddress;
        this.vehicleDropdownsList.zipCodeList = data[0].zipCodes.filter(this.uniqueOnly);
        this.vehicleDropdownsList.comprehensiveDeductibleList = data[1].comprehensive;
        this.vehicleDropdownsList.collisionDeductibleList = data[1].collision;
        vehicles.forEach(v => {
          v = this.formatIdsToText(v);
        });
        this.vehicleInfoList = vehicles.sort(this.sortByVIN);
        this.submissionData.riskDetail.vehicles = this.vehicleInfoList;
        this.setPage(this.currentPage, hasAdded);

        this.getStateList();
      })
    )
  }

  renderPolicyVehicleList(hasAdded?: boolean, isSortDisabled?: boolean) {
    if ((this.submissionData.riskDetail.vehicles?.length ?? 0) === 0) { this.pagedItems = []; } // Temp fix for recurring vehicle cache
    if (this.submissionData.isNew) { return; }
    const riskDetail = this.submissionData.riskDetail;
    const riskDetailId = riskDetail?.id;
    let vehicles: any[] = [];
    Utils.blockUI();
    this.vehicleService.getByRiskDetailId(riskDetailId, true).pipe(
      take(1),
      map(data => {
        vehicles = data as VehicleDto[];
        vehicles.forEach((v: VehicleDto) => v.isMainRow = true);
      }),
      mergeMap(() => {
        const garagingAddresses = this.getGaragingAddressList();
        const deductibles = this.getDeductibles();
        return forkJoin([garagingAddresses, deductibles]);
      })
    )
      .subscribe(data => {
        this.applicant.zipCode = String(data[0].applicantZipCode);
        this.applicant.mainGarageAddressId = data[0].mainGarageAddressId;
        this.vehicleDropdownsList.garagingAddressList = data[0].garagingAddress;
        this.vehicleDropdownsList.zipCodeList = data[0].zipCodes.filter(this.uniqueOnly);
        this.vehicleDropdownsList.comprehensiveDeductibleList = data[1].comprehensive;
        this.vehicleDropdownsList.collisionDeductibleList = data[1].collision;

        if(!isSortDisabled)
        {
          this.vehicleInfoList = vehicles.sort(this.sortByVIN);
        }
        this.vehicleInfoList.forEach(v => {
          v = this.formatIdsToText(v);
        });
        this.submissionData.riskDetail.vehicles = this.vehicleInfoList;
        this.setPage(this.currentPage, hasAdded);

        Utils.unblockUI();
      });

    this.getStateList();
  }

  update(vehicle: VehicleDto) {
    return this.vehicleService.put(vehicle);
  }

  insert(newVehicle: VehicleDto): Observable<VehicleDto> {
    return this.vehicleService.post(newVehicle);
  }

  insertVehicles(vehicles: VehicleDto[]): Observable<VehicleDto[]> {
    return this.vehicleService.postVehicles(vehicles);
  }

  delete(id: string, fromEndorsement?: boolean): Observable<any> {
    const observable = this.vehicleService.delete(id, fromEndorsement);
    return observable.pipe(
      tap(() => {
        this.submissionData.riskDetail.vehicles = this.submissionData.riskDetail?.vehicles.filter(t => t.id !== id && t.deletedDate == null);
      }));
  }

  public deleteUnboundVehicles() {
    const risk = this.submissionData?.riskDetail;
    const bindOption = Number(risk.binding?.bindOptionId) ?? 0;
    const vehicles = risk.vehicles.filter(vehicle => !vehicle.options?.split(',').includes(`${bindOption}`));
    if (vehicles?.length < 1) { return; }
    vehicles.forEach((vehicle) => {
      this.delete(vehicle.id).pipe(take(1)).subscribe(data => {
        this.vehicleInfoList = this.vehicleInfoList.filter(t => t.id !== vehicle.id);
        this.submissionData.riskDetail.vehicles = this.vehicleInfoList;
      }, (error) => {
        console.error('There was an error trying to remove a record. Please try again.');
      });
    });
  }

  reinstate(id: string, arg1: boolean): Observable<any> {
    return this.vehicleService.reinstate(id).pipe(
      tap(() => {
        this.submissionData.riskDetail.vehicles = this.submissionData.riskDetail?.vehicles.filter(t => t.id !== id && t.deletedDate == null);
      })
    );
  }

  public formatIdsToText(vehicle: any) {
    const lists = this.vehicleDropdownsList;
    vehicle.vehicleDescription = lists.vehicleDescriptionList?.find(vd => vd.value === vehicle.vehicleDescriptionId)?.label;
    vehicle.coverageFireTheftDeductible = lists.comprehensiveDeductibleList?.find(g => g.value == vehicle.coverageFireTheftDeductibleId)?.label;
    vehicle.coverageCollisionDeductible = lists.collisionDeductibleList?.find(g => g.value == vehicle.coverageCollisionDeductibleId)?.label;
    vehicle.stateCode = lists.garagingAddressList?.find(g => g.value === vehicle.garagingAddressId)?.state;
    return vehicle;
  }

  updateAddresses(applicant: NameAndAddressDTO) {
    const garagingAddressList = applicant.addresses?.filter(a => a.addressTypeId === 3);
    const vehiclesAddressInvalid: VehicleDto[] = [];
    this.submissionData?.riskDetail?.vehicles?.forEach(v => {
      const garagingAddressId = garagingAddressList.find(g => g.addressId === v.garagingAddressId)?.addressId;
      if (!garagingAddressId) {
        v.garagingAddressId = this.applicant.mainGarageAddressId;
      }
      const zipCode = applicant.addresses?.find(z => String(z.address.zipCode) === v.primaryOperatingZipCode);
      if (!zipCode) {
        v.primaryOperatingZipCode = String(applicant.zipCode);
        v.state = applicant.state;
      }

      if (!garagingAddressId || !zipCode) {
        vehiclesAddressInvalid.push(v);
      }
    });

    if (vehiclesAddressInvalid.length) {
      this.vehicleService.putAddresses(vehiclesAddressInvalid).pipe(take(1)).subscribe(() => {
      }, (error) => {
        console.error('Error on saving', error);
      });
    }
  }

  updateOtherCoverages(fromEndorsement: boolean = false) {
    // if (this.limitsData.hasCargo && this.limitsData.hasRefrigeration) { return; }
    if (this.submissionData?.riskDetail?.vehicles === null || this.submissionData?.riskDetail?.vehicles?.length < 1) { return; }
    const vehicles: VehicleDto[] = [];
    const riskCoverage = this.submissionData?.riskDetail?.riskCoverage;
    this.submissionData?.riskDetail?.vehicles.forEach(v => {
      v.isEndorsement = fromEndorsement;

      const compAndFire = riskCoverage?.compDeductibleId ?? riskCoverage?.fireDeductibleId;

      if (this.limitsData.bComp !== compAndFire) {
        v.isCoverageFireTheft = this.limitsData.hasComprehensive;
      }

      if (this.limitsData.bColl !== riskCoverage?.collDeductibleId) {
        v.isCoverageCollision = this.limitsData.hasCollision;
      }

      if (this.limitsData.bCargo !== riskCoverage?.cargoLimitId) {
        v.hasCoverageCargo = this.limitsData.hasCargo;
        if (!v.hasCoverageCargo) { v.hasCoverageRefrigeration = v.hasCoverageCargo; }
      }

      if (this.limitsData.bRef !== riskCoverage?.refCargoLimitId) {
        v.hasCoverageRefrigeration = this.limitsData.hasRefrigeration;
      }

      vehicles.push(v);
    });
    Utils.blockUI();
    this.vehicleService.putOtherCoverages(vehicles).pipe(take(1)).subscribe(() => {
      console.log('Limits vehicle loaded');
      if (fromEndorsement) {
        this.renderPolicyVehicleList();
      } else {
        this.renderVehicleList();
      }
    }, (error) => {
      Utils.unblockUI();
      console.error('Error on saving', error);
    });
  }

  private get coverage(): RiskCoverageDTO {
    let coverage = this.submissionData.riskDetail?.riskCoverage;
    if (!coverage) {
      const riskCoverages = this.submissionData.riskDetail?.riskCoverages;
      if (riskCoverages) {
        coverage = riskCoverages[0];
      }
    }
    return coverage;
  }

  private generateYearList(startYear: number) {
    const currentYear = new Date().getFullYear()
    let years: SelectItem[] = [];
    startYear = startYear || 1960;
    for (var y = startYear; y <= currentYear; y++) {
      years.push({ value: y, label: String(y) });
    }
    return years.reverse();
  }

  public getStateList() {
    this.vehicleService.getStateList().subscribe(data => {
      this.vehicleDropdownsList.stateList = (data as any[]).map(d => ({ label: d.fullStateName?.replace('[E]', ''), value: d.stateCode }));
    });
  }

  private getGaragingAddressList() {
    return this.applicantNameAddressService.getNameAndAddress(this.submissionData.riskDetail.id).pipe(
      take(1),
      map(
        data => {
          return {
            applicantZipCode: data.zipCode,
            mainGarageAddressId: data.addresses.find(a => a.address.isMainGarage)?.addressId,
            garagingAddress: data.addresses
              .filter(a => a.addressTypeId === 3)
              .map(a => {
                return {
                  label: `${a.address.streetAddress1}, ${a.address.city}, ${a.address.state}, ${a.address.zipCode}`,
                  value: a.addressId,
                  state: a.address.state
                }
              }),
            zipCodes: data.addresses
              .map(a => { return { label: a.address.zipCode, value: a.address.zipCode, state: a.address.state } })
          }
        }
      )
    );
  }

  getGaragingAddress(id, callbackFn?: Function) {
    this.applicantNameAddressService.getAddress(id).pipe(take(1)).subscribe((a) => {
      this.vehicleDropdownsList.oldGaragingAddressList = [{
        label: `${a.streetAddress1}, ${a.city}, ${a.state}, ${a.zipCode}`,
        value: a.id,
        state: a.state
      }];
      if (callbackFn) { callbackFn(); }
    }, (error) => {
      Utils.unblockUI();
      console.error('Error on saving', error);
    });
  }

  private getDeductibles() {
    const state = this.submissionData.riskDetail.nameAndAddress.state;
    return this.limitService.getLimits(state).pipe(
      take(1),
      map(
        data => {
          return {
            comprehensive: data.comprehensiveLimits.map(c => { return { label: c.limitDisplay, value: c.id } }),
            collision: data.collisionLimits.map(c => { return { label: c.limitDisplay, value: c.id } })
          }
        }
      )
    );
  }

  private uniqueOnly(item: any, i: number, array: any[]) {
    return array.findIndex(t => t.value === item.value) === i;
  }

  private withCurreny(value: string) {
    return value === 'No Coverage' ? value : value;
  }

  private sortByVIN(cv1, cv2) {
    if (cv1.vin > cv2.vin) return 1;
    if (cv1.vin < cv2.vin) return -1;
    return 0;
  }

  public setDefaults(vehicle?: any) {
    const coverage = this.submissionData.getCoverage();
    vehicle = new VehicleDto(vehicle);

    const compFireDeductibleId = coverage?.compDeductibleId ?? coverage?.fireDeductibleId;
    if (compFireDeductibleId && compFireDeductibleId > LimitsDefaultValues.compId) {
      vehicle.coverageFireTheftDeductibleId = compFireDeductibleId;
      vehicle.isCoverageFireTheft = true;
    }

    if (coverage?.collDeductibleId && coverage?.collDeductibleId > LimitsDefaultValues.colId) {
      vehicle.coverageCollisionDeductibleId = coverage.collDeductibleId;
      vehicle.isCoverageCollision = true;
    }

    vehicle.hasCoverageCargo = coverage?.cargoLimitId > LimitsDefaultValues.cargoId;
    vehicle.hasCoverageRefrigeration = coverage?.refCargoLimitId > LimitsDefaultValues.refId;
    vehicle.vehicleBusinessClassId = this.submissionData.riskDetail?.businessDetails?.accountCategoryUWId;
    vehicle.useClassId = this.submissionData.riskDetail?.businessDetails?.mainUseId;
    vehicle.garagingAddressId = this.applicant?.mainGarageAddressId;
    vehicle.primaryOperatingZipCode = this.applicant.zipCode;
    vehicle.state = this.vehicleDropdownsList.zipCodeList.find(z => z.value == vehicle.primaryOperatingZipCode)?.state;
    vehicle.registeredState = this.submissionData.riskDetail?.nameAndAddress?.state;

    return vehicle;
  }

  get getOptionQuote(): string {
    const opt = this.submissionData?.riskDetail?.riskCoverages?.map(x => x.optionNumber)?.toString();
    return !((opt ?? '').length) ? '1' : opt;
  }

  public onExitVehicleScreen(callbackFn: Function) {
    if (callbackFn) { callbackFn(); }
  }

  sort(targetElement) {
    if (targetElement instanceof HTMLElement) {
      const property = targetElement.getAttribute('data-name');
      if (property === 'coverageFireTheftDeductible' || property === 'coverageCollisionDeductible') {
        targetElement.setAttribute('data-type', 'text');
      }
    }
    let vehicles = Object.assign([], this.vehicleInfoList);
    let sortData = this.tableData.sortPagedData(targetElement, vehicles, this.currentPage);
    this.pagedItems = sortData.pagedItems;
    this.vehicleInfoList = sortData.sortedData;
  }

  loadPreviousVehicle(vehicle: VehicleDto): Observable<any> {
    return this.vehicleService.getPreviousVehicles(vehicle).pipe(
      tap((data: VehicleDto[]) => {
        data.forEach(v => {
          this.formatIdsToText(v);
          // remove amounts
          v.autoLiabilityPremium = null;
          v.physicalDamagePremium = null;
          v.cargoPremium = null;
          v.totalPremium = null;
          v.proratedPremium = null;
        });
        vehicle.previousVehicles = data;
      })
    );
  }

  GetPolicyHistories() {
    this.policyHistories = [];
    this.policyHistoryData.getEndorsementHistory().subscribe((data: PolicyHistoryDTO[]) => {
      this.policyHistories = data;
    });
  }
}

export class VehicleInformationDropDowns {
  yearList: SelectItem[];
  vehicleTypeList: SelectItem[];
  grossVehicleWeightList: SelectItem[];
  grossVehicleWeightCategoryList: SelectItem[];
  vehicleDescriptionList: SelectItem[];
  vehicleBusinessClassList: SelectItem[];
  comprehensiveDeductibleList: SelectItem[];
  collisionDeductibleList: SelectItem[];
  useClassList: SelectItem[];
  zipCodeList: any[];
  garagingAddressList: any[];
  oldGaragingAddressList: any[];
  optionList: SelectItem[];
  stateList?: SelectItem[];
}
function withDeleted(riskDetailId: string, withDeleted: any, arg2: boolean) {
  throw new Error('Function not implemented.');
}

