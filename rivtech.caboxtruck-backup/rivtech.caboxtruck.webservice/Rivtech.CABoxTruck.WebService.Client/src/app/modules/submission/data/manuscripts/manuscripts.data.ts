import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PagerService } from '../../../../core/services/pager.service';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { SubmissionData } from '../submission.data';
import { TableData } from '../tables.data';
import { ManuscriptDTO } from '@app/shared/models/submission/manuscript/ManuscriptDto';
import { ManuscriptService } from '../../services/manuscript.service';
import { BrokerInfoDto } from '@app/shared/models/submission/brokerinfo/brokerInfoDto';
import { QuoteLimitsData } from '../quote/quote-limits.data';
import { SummaryData } from '../summary.data';
import { PolicySummaryData } from '@app/modules/policy/data/policy-summary.data';
import Utils from '@app/shared/utilities/utils';

@Injectable(
  { providedIn: 'root'}
)
export class ManuscriptsData {
  manuscripts: ManuscriptDTO[];
  manuscriptForm: FormGroup;
  dateOptions: IAngularMyDpOptions;
  pager: any = {};
  currentPage: number = 0;
  pagedItems: any[];
  loading: boolean;
  currentServerDateTime: Date;

  constructor(
    private fb: FormBuilder,
    private service: ManuscriptService,
    private submissionData: SubmissionData,
    private tableData: TableData,
    private pagerService: PagerService,
    private quoteLimitsData: QuoteLimitsData,
    private summaryData: SummaryData,
    private policySummaryData: PolicySummaryData
  ) { }

  initiateFormFields() {
    this.submissionData.getCurrentServerDateTime2().subscribe((date) => {
      this.currentServerDateTime = date;
    });
    this.manuscripts = [];
    this.pagedItems = [];
    this.manuscriptForm = this.fb.group({
      id: [],
      effectiveDate: [],
      expirationDate: [],
      isProrate: [],
      isSelected: [],
      title: [null, Validators.required],
      description: [null, Validators.required],
      premium: [null, [Validators.required]],
      premiumType: [],
    });
    this.initDateOptions();
  }

  populateFields() {
    if (this.submissionData.riskDetail?.riskManuscripts) {
      this.manuscripts = this.submissionData.riskDetail.riskManuscripts;
      this.setPage(1, this.manuscripts);
    }
  }

  updateById(id: string, value: boolean, fromEndorsement: boolean = false) {
    const manuscript = this.manuscripts.find(m => m.id === id);
    manuscript.isSelected = value;
    if (manuscript) {
      this.save(manuscript, fromEndorsement);
    }
  }

  save(manuscript: ManuscriptDTO, fromEndorsement: boolean = false) {
    manuscript.riskDetailId = this.submissionData.riskDetail?.id;
    Utils.blockUI();
    if (!manuscript.id) {
      this.service.post(manuscript).subscribe(data => {
        Utils.unblockUI();
        this.manuscripts.push(data);
        this.setPage(1, this.manuscripts, true);
        this.refreshQuoteOptionsAndSummaryHeader();
        if (fromEndorsement) { this.policySummaryData.updatePendingEndorsement(); }
      });
    } else {
      this.service.put(manuscript).subscribe(data => {
        Utils.unblockUI();
        const updatingManuscript = this.manuscripts.find(m => m.id === data.id);
        Object.assign(updatingManuscript, data);
        this.refreshQuoteOptionsAndSummaryHeader();
        if (fromEndorsement) { this.policySummaryData.updatePendingEndorsement(); }
      });
    }
    this.submissionData.riskDetail.riskManuscripts = this.manuscripts;
  }

  getAllManuscript() {
    Utils.blockUI();
    this.service.get(this.submissionData.riskDetail.id).subscribe((data) => {
      Utils.unblockUI();
      this.submissionData.riskDetail.riskManuscripts = data;
      this.populateFields();
    });
  }

  updateDates(broker: BrokerInfoDto) {
    if (this.manuscripts?.length > 0) {
      const manuscript = this.manuscripts[0];
      manuscript.effectiveDate = broker?.effectiveDate;
      manuscript.expirationDate = broker?.expirationDate;
      this.service.putDates(manuscript).subscribe();
    }
  }

  delete(id: string) {
    this.service.delete(id).subscribe(() => {
      this.manuscripts = this.manuscripts.filter(ai => ai.id !== id);
      this.setPage(this.currentPage, this.manuscripts);
      this.submissionData.riskDetail.riskManuscripts = this.manuscripts;
      this.refreshQuoteOptionsAndSummaryHeader();
    });
  }

  deleteEndrosement(id: string, expirationDate: Date) {
    this.service.delete(id, true, expirationDate).subscribe((data) => {
      if (data) {
        this.manuscripts = this.manuscripts.filter(ai => ai.id !== id);
      } else {
        const manu = this.manuscripts.find(ai => ai.id === id);
        if (manu.isProrate) { manu.expirationDate = expirationDate; }
        manu.isSelected = false;
        manu.isActive = false;
      }
      this.setPage(this.currentPage, this.manuscripts);
      this.submissionData.riskDetail.riskManuscripts = this.manuscripts;
      this.refreshQuoteOptionsAndSummaryHeader();
      this.policySummaryData.updatePendingEndorsement();
    });
  }

  initDateOptions() {
    this.dateOptions = {
      dateRange: false,
      dateFormat: 'mm/dd/yyyy',
    };
  }

  sort(targetElement) {
    const sortData = this.tableData.sortPagedData(targetElement, this.manuscripts, this.currentPage);
    this.manuscripts = sortData.sortedData;
    this.pagedItems = sortData.pagedItems;
  }

  setPage(page: number, data: any[], isNew: boolean = false) {
    if (page < 1) {
      return;
    }

    this.pager = this.pagerService.getPager(this.manuscripts.length, page);

    if (isNew) {
      this.currentPage = this.pager.currentPage = this.pager.totalPages;
      this.pager.endIndex = this.pager.totalItems;
      this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);
    } else {
      while (this.pager.totalItems <= ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize)) {
        this.pager.currentPage--;
        this.pager.startIndex = ((this.pager.currentPage * this.pager.pageSize) - this.pager.pageSize);
      }

      this.currentPage = page;
    }

    this.pagedItems = data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  moveToLastPage() {
    this.pager = this.pagerService.getPager(this.manuscripts.length);
    this.setPage(this.pager.endPage, this.manuscripts);
  }

  public refreshQuoteOptionsAndSummaryHeader(): void {
    this.quoteLimitsData.loadQuoteOptions().subscribe(data => {
      if (this.submissionData.isIssued) {
        const bindOptionId = this.submissionData?.riskDetail?.binding?.bindOptionId ?? 1;
        const riskCoverage = data?.riskCoverages.find(coverage => coverage.optionNumber == bindOptionId);
        this.policySummaryData.estimatedPremium = ((riskCoverage?.riskCoveragePremium?.proratedPremium ?? 0) == 0) ? riskCoverage?.riskCoveragePremium?.premium : this.policySummaryData.getBillingSummary();
      } else {
        const bindOptionId = 1;
        const riskCoverage = data?.riskCoverages.find(coverage => coverage.optionNumber == bindOptionId);
        this.summaryData.estimatedPremium = riskCoverage?.riskCoveragePremium?.premium;
      }
    });
  }
}
