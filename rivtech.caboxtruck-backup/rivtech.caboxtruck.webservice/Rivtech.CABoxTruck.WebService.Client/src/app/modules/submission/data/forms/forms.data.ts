import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormDTO, FormInfoDTO } from '../../../../shared/models/submission/forms/FormDto';
import { finalize, take } from 'rxjs/operators';
import { SubmissionData } from '../submission.data';
import { FormsService } from '../../services/forms.service';
import { LimitsData } from '../limits/limits.data';
import { BindingService } from '../../services/binding.service';
import { ApplicantAdditionalInterestService } from '../../services/applicant-additional-interest.service';
import { RiskAdditionalInterestDTO } from '../../../../shared/models/submission/RiskAdditionalInterestDto';
import { TableData } from '../tables.data';
import { fileSizeValidatorMultiple, customFileTypeValidatorMultiple, filenameLengthValidator } from '../../helpers/file.validator';
import Utils from '../../../../shared/utilities/utils';
import { SelectItem } from '@app/shared/models/dynamic/select-item';
import NotifUtils from '@app/shared/utilities/notif-utils';
import { BindingData } from '../binding/binding.data';
import { ErrorMessageConstant } from '../../../../shared/constants/error-message.constants';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { SummaryData } from '../summary.data';
import { QuoteLimitsData } from '../quote/quote-limits.data';
import { mcs90Validator } from '../../helpers/forms.validator';
import { BrokerGenInfoData } from '../broker/general-information.data';
import { PolicySummaryData } from '@app/modules/policy/data/policy-summary.data';

@Injectable()
export class FormsData {
  forms: FormDTO[];
  uploadForm: FormGroup;
  exclusionRadiusForm: FormGroup;
  hiredPhysdamForm: FormGroup;
  driverRequirementForm: FormGroup;
  territoryExclusionForm: FormGroup;
  manuscriptForm: FormGroup;
  trailerInterchangeForm: FormGroup;
  generalLiabilityForm: FormGroup;
  mcs90Form: FormGroup;
  lossPayeeForm: FormGroup;
  bindOption: string;
  additionalInterests: RiskAdditionalInterestDTO[] = [];
  stateList = [];
  monthsList = [];
  surplusLineNumber: string;
  isPolicyUI = false;
  isEndorsement = false;

  datepickerOptions: IAngularMyDpOptions = {
    dateRange: false,
    dateFormat: 'mm/dd/yyyy',
    disableSince: { year: 0, month: 0, day: 0 },
    disableUntil: { year: 0, month: 0, day: 0 }
  };

  currencySettings: any = {
    align: 'left',
    allowNegative: false,
    precision: 0,
    nullable: true
  };

  constructor(
    private fb: FormBuilder,
    private bindingService: BindingService,
    private bindingData: BindingData,
    private interestService: ApplicantAdditionalInterestService,
    private formsService: FormsService,
    private limitsData: LimitsData,
    private submissionData: SubmissionData,
    private tableData: TableData,
    private summaryData: SummaryData,
    private quoteLimitsData: QuoteLimitsData,
    private brokerData: BrokerGenInfoData,
    private policySummaryData: PolicySummaryData
  ) { }

  initiateFormFields() {
    this.forms = [];

    this.uploadForm = this.fb.group({
      description: [null, Validators.required],
      fileInfo: [null, [customFileTypeValidatorMultiple, fileSizeValidatorMultiple, filenameLengthValidator]],
      fileName: [null, Validators.required],
    });

    this.exclusionRadiusForm = this.fb.group({
      radius: [null, [Validators.required, Validators.min(1)]]
    });

    this.hiredPhysdamForm = this.fb.group({
      estimatedAdditionalPremium: [null, [Validators.required, Validators.min(1)]],
      vehicleDays: [null, [Validators.required, Validators.min(1)]],
      ratePerDay: [null, [Validators.required, Validators.min(1)]],
      grossCombinedWeight: [null, [Validators.required, Validators.min(1)]],
      accidentLimitPerVehicle: [null, [Validators.required, Validators.min(1)]],
      deductible: [null, [Validators.required, Validators.min(1)]],
      physdamLimit: [null, [Validators.required, Validators.min(1)]]
    });

    this.driverRequirementForm = this.fb.group({
      requirement: [null, Validators.required]
    });

    this.territoryExclusionForm = this.fb.group({
      states: [null, Validators.required]
    });

    this.manuscriptForm = this.fb.group({
      premium: [null, [Validators.required, Validators.min(1)]],
      description: [null, Validators.required]
    });

    this.trailerInterchangeForm = this.fb.group({
      compreLimitOfInsurance: [null, [Validators.required, Validators.min(1)]],
      compreDeductible: [null, [Validators.required, Validators.min(1)]],
      comprePremium: [null, [Validators.required, Validators.min(1)]],
      lossLimitOfInsurance: [null, [Validators.required, Validators.min(1)]],
      lossDeductible: [null, [Validators.required, Validators.min(1)]],
      lossPremium: [null, [Validators.required, Validators.min(1)]],
      collisionLimitOfInsurance: [null, [Validators.required, Validators.min(1)]],
      collisionDeductible: [null, [Validators.required, Validators.min(1)]],
      collisionPremium: [null, [Validators.required, Validators.min(1)]],
      fireLimitOfInsurance: [null, [Validators.required, Validators.min(1)]],
      fireDeductible: [null, [Validators.required, Validators.min(1)]],
      firePremium: [null, [Validators.required, Validators.min(1)]],
      fireTheftLimitOfInsurance: [null, [Validators.required, Validators.min(1)]],
      fireTheftDeductible: [null, [Validators.required, Validators.min(1)]],
      fireTheftPremium: [null, [Validators.required, Validators.min(1)]],
    });

    this.generalLiabilityForm = this.fb.group({
      injuryLimit: [null, [Validators.required, Validators.min(1)]],
      medicalExpenses: [null, [Validators.required, Validators.min(1)]],
      damagesRentedToYou: [null, [Validators.required, Validators.min(1)]],
      aggregateLimit: [null, [Validators.required, Validators.min(1)]],
    });

    this.mcs90Form = this.fb.group({
      usDotNumber: [null, Validators.required],
      dateReceived: [null, Validators.required],
      issuedTo: [null, Validators.required],
      state: [null, Validators.required],
      datedAt: [null, Validators.required],
      policyDateDay: [null, [Validators.required, Validators.min(1), Validators.max(31)]],
      policyDateMonth: [null, Validators.required],
      policyDateYear: [null, Validators.required],
      policyNumber: [null, Validators.required],
      effectiveDate: [null, Validators.required],
      insuranceCompany: [null, Validators.required],
      counterSignedBy: [null, Validators.required],
      accidentLimit: [null, [Validators.required, Validators.min(1)]]
    },
    {
      validators: [mcs90Validator(this.brokerData)]
    });

    this.lossPayeeForm = this.fb.group({
      descriptionOfAutos: [null, [Validators.required]]
    });
  }

  retrieveDropdownValues() {
    this.bindingService.getBindOption().pipe(take(1)).subscribe(data => {
      this.bindOption = data.find(d => d.id === this.submissionData.riskDetail?.binding?.bindOptionId)?.description ?? 'Option 1';
    });
    this.getStates();
    this.monthsList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  }

  populateFields(isEndorsement: boolean = false) {
    const risk = this.submissionData.riskDetail;
    this.isEndorsement = isEndorsement;
    this.getSurplusLineNumber();
    if (risk?.riskForms && risk.riskForms.length > 0) {
      this.forms = risk.riskForms;
    } else {
      this.createDefaultForms(risk.id);
    }

    // TODO: Refactor Additional Interests to be preloaded to SubmissionData
    this.interestService.getAdditionalInterests(this.submissionData.riskDetail?.id).subscribe(data => {
      this.additionalInterests = data;
      this.applyRules();
    });

  }

  ensureMandatorySelected(preForms: any) {
    this.forms.forEach(f => {
      if (f.form.isMandatory) {
        f.isSelected = true;
      }
    });
    this.formsService.postPreForms(preForms).subscribe();
  }

  save() {
    this.formsService.post(this.forms).pipe(take(1)).subscribe(() => { });
  }

  createDefaultForms(riskId: string) {
    this.formsService.postDefaults(riskId).subscribe((data) => {
      this.forms = this.submissionData.riskDetail.riskForms = data;
      this.applyRules();
    });
  }

  mergeManuscripts() {
    this.forms = this.forms.filter(f => f.form.formNumber !== 'RPCAMANU'); // Remove first merged manuscripts
    const ofacSorOrder = this.forms.find(f => f.form.id === 'OFACNotice')?.form?.sortOrder;

    this.submissionData.riskDetail?.riskManuscripts?.forEach(m => {
      const formInfoData = {
        id: 'ManuscriptEndorsement',
        name: m.title,
        formNumber: 'RPCAMANU',
        sortOrder: ofacSorOrder - 1
      };
      const formData = {
        id: m.id,
        riskDetailId: this.submissionData.riskDetail?.id,
        isSelected: m.isSelected,
        form: new FormInfoDTO(formInfoData)
      };
      this.forms.push(new FormDTO(formData));
    });
  }

  applyRules() {
    this.mergeManuscripts();

    const formRules = [
      {
        formId: 'SurplusLinesNotice',
        number: this.surplusLineNumber,
        include: this.hasSurplusLinesNoticeForm,
      },
      {
        formId: 'PrimaryAndNoncontributory',
        include: this.hasPrimaryNonContributory,
        isMandatory: this.hasPrimaryNonContributory,
      },
      {
        formId: 'WaiverOfTransferOfRights',
        include: this.hasWaiverOfSubrogation,
        isMandatory: this.hasWaiverOfSubrogation,
      },
      {
        formId: 'PhysicalDamageLimitOfInsurance',
        include: this.limitsData.hasAutoPhysicalDamage,
        isMandatory: this.limitsData.hasAutoPhysicalDamage,
      },
      {
        formId: 'InstallmentSchedule',
        include: this.isDirectBillInstallments,
        isMandatory: this.isDirectBillInstallments
      },
      {
        formId: 'AdditionalInsuredBlanket',
        include: this.hasAdditionaInsuredBlanket,
        isMandatory: this.hasAdditionaInsuredBlanket
      },
      {
        formId: 'DesignatedInsured',
        include: this.hasDesignatedInsured,
        isMandatory: this.hasDesignatedInsured
      },
      {
        formId: 'DriverExclusion',
        include: this.hasDriverExclusions || this.submissionData.riskDetail.hasInitialDriverExclusion
      },
      {
        formId: 'GeneralLiabilityEndorsement',
        include: this.limitsData.hasGeneralLiability
      },
      {
        formId: 'MotorTruckCargoEndorsement',
        include: this.limitsData.hasCargo,
        isMandatory: this.limitsData.hasCargo,
      },
      {
        formId: 'LossPayeeEndorsement',
        include: this.hasLossPayee,
        // isMandatory: this.hasLossPayee,
      },
      {
        formId: 'CourierCoverageEndorsement',
        include: this.isMainUseCourier
      },
      {
        formId: 'RefrigerationCargoEndorsement',
        include: this.limitsData.hasRefrigeration
      },
      {
        formId: 'UninsuredMotoristCoverageBodilyInjury',
        include: this.limitsData.hasUninsuredMotoristBI
      },
      {
        formId: 'SplitBodilyInjuryUninsuredMotoristCoverage',
        include: this.limitsData.hasUninsuredMotoristBI
      }
    ];

    const preForms = [];
    this.forms.forEach(f => {
      f.include = true;
      const formRule = formRules.find(fr => fr.formId === f.form?.id);

      if (formRule) {
        if (formRule.number !== undefined) {
          f.form.formNumber = formRule.number;
        }
        if (formRule.isMandatory !== undefined) {
          f.form.isMandatory = formRule.isMandatory;
        }
        if (formRule.include !== undefined) {
          f.include = formRule.include;
          f.isSelected = formRule.include ? f.isSelected : false;
          preForms.push(f);
        }
      }
    });

    console.log('hasInitialDriverExclusion', this.submissionData.riskDetail.hasInitialDriverExclusion);

    this.ensureMandatorySelected(preForms);
    this.forms = this.forms.sort(this.sorter);

    if (!this.hasDriverExclusions && this.submissionData.riskDetail.hasInitialDriverExclusion !== true) {
      this.forms = this.forms.filter(x => x.form?.id !== 'DriverExclusion');
    }

    // Remove forms on display
    if (!this.limitsData.hasUninsuredMotoristBI) {
      this.forms = this.forms.filter(x => x.form?.id !== 'UninsuredMotoristCoverageBodilyInjury' && x.form?.id !== 'SplitBodilyInjuryUninsuredMotoristCoverage');
    }
  }

  private get hasPrimaryNonContributory(): boolean {
    return this.submissionData.riskDetail?.blanketCoverage?.isPrimaryNonContributory ||
      this.additionalInterests.some(ai => ai.isPrimaryNonContributory && ai.removeProcessDate === null);
  }

  private get isMainUseCourier(): boolean {
    return +this.submissionData.riskDetail?.businessDetails?.mainUseId === 1;
  }

  private get hasWaiverOfSubrogation(): boolean {
    return this.submissionData.riskDetail?.blanketCoverage?.isWaiverOfSubrogation ||
      this.additionalInterests.some(ai => ai.isWaiverOfSubrogation && ai.removeProcessDate === null);
  }

  private get isDirectBillInstallments() {
    return +this.submissionData.riskDetail?.binding?.paymentTypesId === 2;
  }

  private get hasAdditionaInsuredBlanket() {
    return this.submissionData.riskDetail?.blanketCoverage?.isAdditionalInsured
      || this.additionalInterests.some(ai => ai.additionalInterestTypeId === 1 && ai.removeProcessDate === null);
  }

  private get hasDesignatedInsured() {
    return this.additionalInterests.some(ai => ai.additionalInterestTypeId === 12 && ai.removeProcessDate === null);
  }

  private get hasLossPayee() {
    return this.additionalInterests.some(ai => ai.additionalInterestTypeId === 2 && ai.removeProcessDate === null);
  }

  private get hasDriverExclusions() {
    const optionId = this.submissionData.riskDetail?.binding?.bindOptionId ?? '1';
    const drivers = this.submissionData.riskDetail?.drivers;
    console.log('hasDriverExclusions', drivers?.length > 0 && drivers.some(d => d.options == null || !d.options?.split(',')?.includes(optionId.toString())));
    return drivers?.length > 0 && drivers.some(d => d.options == null || !d.options?.split(',')?.includes(optionId.toString()));
  }

  private get hasSurplusLinesNoticeForm() {
    const slForms = ['AL', 'AZ', 'CA', 'CO', 'DE', 'GA', 'IL', 'KY', 'MD', 'NC',
    'NJ', 'NV', 'OH', 'OR', 'PA', 'SC', 'TN', 'TX', 'VA', 'WA'];
    return slForms.includes(this.submissionData.riskDetail?.nameAndAddress?.state);
  }

  private getSurplusLineNumber() {
    const bindSurplusNum = this.submissionData.riskDetail?.binding?.surplusLIneNum;
    if (!bindSurplusNum) {
      this.setDefaultSurplusLineNumber();
    }
    this.surplusLineNumber = bindSurplusNum;
  }

  sort(targetElement) {
    this.tableData.sort(targetElement, this.forms, 1);
  }

  private sorter(a: any, b: any) {
    return a.form.sortOrder < b.form.sortOrder ? -1 : 1;
  }

  viewForm(riskForm?: FormDTO) {
    Utils.blockUI();
    if (!riskForm) {
      riskForm = new FormDTO();
      riskForm.riskDetailId = this.submissionData.riskDetail.id;
    }
    const riskForms = [];
    riskForms.push(riskForm);

    this.generateForms(riskForms);
  }

  viewForms(riskForms?: FormDTO[]) {
    const excludeFromPacket = ['RPCA030']; // Contains pop-up and mapping info only;
      riskForms = riskForms.filter(rf => rf.isSelected
          && !excludeFromPacket.includes(rf.form.formNumber));
      this.generateForms(riskForms);
  }

  viewInvoice(invoice: any) {
    this.formsService.generateInvoice(this.submissionData.riskDetail?.riskId, invoice).subscribe(result => {
      Utils.unblockUI();
      const quoteFileUrl = decodeURIComponent(result);
      const win = window.open(quoteFileUrl, '_blank');
      win.focus();
    });
  }

  generateForms(riskForms: FormDTO[]) {
    this.formsService.generate(riskForms).subscribe(result => {
      if (result !== null) {
        Utils.unblockUI();
        const quoteFileUrl = decodeURIComponent(result);
        const win = window.open(quoteFileUrl, '_blank');
        win.focus();
      } else {
        NotifUtils.showError(ErrorMessageConstant.packetGenerationErrorMsg);
      }
    }, (error) => {
      console.error(error);
      NotifUtils.showError(ErrorMessageConstant.packetGenerationErrorMsg);
    });
  }

  updateRiskForm(riskForm?: FormDTO, fromEndorsement: boolean = false) {
    if (!riskForm) { return; }
    if (this.submissionData?.isIssued && !this.isPolicyUI) { return; }

    // if (!riskForm.isSelected && this.isSubPhysicalDamageForm(riskForm?.form?.id)) {
    //   riskForm.other = null; // reset forms if unselected
    // }

    Utils.blockUI();
    this.formsService.updateForm(riskForm)
    .pipe(
      finalize(() => {
        if (this.isSubPhysicalDamageForm(riskForm?.form?.id)) {
          this.refreshQuoteOptionsAndSummaryHeader();
        }
        if (fromEndorsement) { this.policySummaryData.updatePendingEndorsement(); }
        Utils.unblockUI();
      }), take(1))
    .subscribe(result => {
      // console.log(`${riskForm.form?.name} selected successfully`);
    }, (error) => {
      console.log(error);
    });
  }

  getStates() {
    this.formsService.getStates().pipe(take(1)).subscribe(data => {
      this.stateList = data.map(s => ({
        value: s.stateCode?.toString(),
        label: s.fullStateName?.toString()?.replace('[E]', '')
      })) as SelectItem[];
    });
  }

  deleteRiskForm(riskFormId: string) {
    if (!riskFormId) { return; }

    Utils.blockUI();
    this.formsService.removeForm(riskFormId)
      .pipe(finalize(() => Utils.unblockUI()), take(1))
      .subscribe(result => {
        const riskForms = this.forms.filter(form => form.id !== riskFormId);
        this.submissionData.riskDetail.riskForms = riskForms.map(x => Object.assign({}, x));
        this.forms = this.submissionData.riskDetail.riskForms;
      }, (error) => {
        console.log(error);
        NotifUtils.showError('There was an error trying to delete the record. Please try again');
      });
  }

  updateDefaultStates() {
    const territoryExcForm = this.forms?.find(f => f.form.id === 'TerritoryExclusion');
    if (!territoryExcForm) { return; }

    const statesDefault = JSON.parse(territoryExcForm?.other)?.statesDefault;
    if (statesDefault === undefined || statesDefault) {
      this.formsService.updateDefaultStates(this.submissionData.riskDetail?.id).subscribe((data) => {
        territoryExcForm.other = data?.other;
      });
    }
  }

  // TODO: Refactor, this is dirty and redundant
  setDefaultSurplusLineNumber() {
    const riskDetail = this.submissionData.riskDetail;
    const policyState = riskDetail?.nameAndAddress?.state;
    const creditedOfficeId = riskDetail?.binding?.creditedOfficeId ?? '3';
    this.bindingService.getSurplusLineDefault(creditedOfficeId, policyState).subscribe(data => {
      this.surplusLineNumber = data?.licenseNumber;
      this.bindingData.formBinding?.patchValue({ surplusLIneNum: this.surplusLineNumber });
      if (riskDetail.binding) {
        riskDetail.binding.surplusLIneNum = this.surplusLineNumber;
      }
    });
  }

  public refreshQuoteOptionsAndSummaryHeader(): void {
    this.quoteLimitsData.loadQuoteOptions().subscribe(data => {
      if (this.submissionData.isIssued && this.isPolicyUI) {
        const bindOptionId = this.submissionData?.riskDetail?.binding?.bindOptionId ?? 1;
        const riskCoverage = data?.riskCoverages.find(coverage => coverage.optionNumber == bindOptionId);
        this.policySummaryData.estimatedPremium = ((riskCoverage?.riskCoveragePremium?.proratedPremium ?? 0) == 0) ? riskCoverage?.riskCoveragePremium?.premium : this.policySummaryData.getBillingSummary();
      } else {
        const bindOptionId = 1;
        const riskCoverage = data?.riskCoverages.find(coverage => coverage.optionNumber == bindOptionId);
        this.summaryData.estimatedPremium = riskCoverage?.riskCoveragePremium?.premium;
      }
    });
  }

  isSubPhysicalDamageForm(formId: string): boolean {
    return formId === 'HiredPhysicalDamage' || formId === 'TrailerInterchangeCoverage';
  }
}
