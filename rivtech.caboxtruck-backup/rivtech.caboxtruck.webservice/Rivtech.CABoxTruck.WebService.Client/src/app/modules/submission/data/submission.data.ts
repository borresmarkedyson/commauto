import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RiskDTO } from '../../../shared/models/risk/riskDto';
import { Observable, throwError } from 'rxjs';
import { SubmissionService } from '../services/submission.service';
import { ValidationListModel } from '../../../shared/models/validation-list.model';
import { BindIssueService } from '../services/bind-issue.service';
import NotifUtils from '../../../shared/utilities/notif-utils';
import { catchError, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PathConstants } from '../../../shared/constants/path.constants';
import { BindAndIssueLabelConstants } from '../../../shared/constants/bind-and-issue.labels.constants';
import { ErrorMessageConstant } from '../../../shared/constants/error-message.constants';
import { LvRiskStatus } from '@app/shared/constants/risk-status';
import Utils from '@app/shared/utilities/utils';
import { EndorsementService } from '../services/endorsement.service';

@Injectable(
  { providedIn: 'root' }
)
export class SubmissionData {
  formInfo: FormGroup;
  riskDetail: RiskDTO;
  isNew: boolean = false;
  isQuoteIssued: boolean = false;
  isNewlyIssued: boolean = false;
  public validationList = new ValidationListModel();
  currentServerDateTime: Date;

  constructor(private formBuilder: FormBuilder,
    private submissionService: SubmissionService,
    private bindIssueService: BindIssueService,
    private endorsementService: EndorsementService,
    private router: Router
    ) { }

  initiateFormGroup(obj: RiskDTO): FormGroup {
    this.formInfo = this.formBuilder.group({
      id: [''],
      riskDetailId: [''],
      submissionNumber: [''],
      effectiveDate: [''],
      expirationDate: [''],
      insuredName: [''],
      broker: [''],
    });
    this.isQuoteIssued = false;
    return this.formInfo;
  }

  reset() {
    this.initiateFormGroup(new RiskDTO());
  }

  initiateFormValues(obj: RiskDTO) {
    this.formInfo.patchValue(obj);
  }

  getRiskByIdAsync(id: string) {
    return this.submissionService.getRiskByIdAsync(id);
  }

  getRiskDetailStatusByIdAsync(id: string) {
    return this.submissionService.getRiskDetailStatusByIdAsync(id);
  }

  getCurrentServerDateTime() {
    let currentServerDateTime = new Date();
    this.submissionService.getServerDateTime()
      .pipe(
        tap((data) => currentServerDateTime = data),
        catchError((error) => {
          console.error(error);
          return throwError(error);
        })
      );

    return currentServerDateTime;
  }

  getCurrentServerDateTime2() {
    return this.submissionService.getServerDateTime().pipe(take(1));
  }

  public update(obj: RiskDTO): Observable<RiskDTO> {
    return this.submissionService.put(obj);
  }

  public post(obj: RiskDTO): Observable<RiskDTO> {
    return this.submissionService.post(obj);
  }

  getCoverage() {
    let coverage = this.riskDetail?.riskCoverage;
    if (!coverage) {
      const riskCoverages = this.riskDetail?.riskCoverages;
      if (riskCoverages) {
        coverage = riskCoverages[0];
      }
    }
    return coverage;
  }

  IssuePolicy() {
    NotifUtils.showConfirmMessage(BindAndIssueLabelConstants.areYouSureYouWantToIssue, () => {
      Utils.blockUI();
      this.bindIssueService.Issue(this.riskDetail.id).pipe(take(1)).subscribe(data => {
        NotifUtils.showSuccessWithConfirmationFromUser(BindAndIssueLabelConstants.issuedSuccessfully, () => {
          this.isNewlyIssued = true;
          this.router.navigateByUrl(`/policies/${this.riskDetail.riskId}/${data.id}/${PathConstants.Policy.Summary}`);
        });
      }, (error) => {
        console.error(error);
        NotifUtils.showError(ErrorMessageConstant.issuePolicyErrorMsg);
      });
    });
  }

  ReinstatePolicy() {
    NotifUtils.showConfirmMessage(BindAndIssueLabelConstants.areYouSureYouWantToReinstate, () => {
      Utils.blockUI();
      this.bindIssueService.ReinstatePolicy(this.riskDetail.id).pipe(take(1)).subscribe(data => {
        NotifUtils.showSuccessWithConfirmationFromUser(BindAndIssueLabelConstants.reinstatedSuccessfully, () => {
          this.router.navigateByUrl(`/policies/${this.riskDetail.riskId}/${data.id}/${PathConstants.Policy.Summary}`);
        });
      }, (error) => {
        console.error(error);
        NotifUtils.showError(ErrorMessageConstant.issuePolicyErrorMsg);
      });
    });
  }

  RewritePolicy() {
    NotifUtils.showConfirmMessage(BindAndIssueLabelConstants.areYouSureYouWantToRewrite, () => {
      Utils.blockUI();
      this.bindIssueService.RewritePolicy(this.riskDetail.id).pipe(take(1)).subscribe(data => {
        NotifUtils.showSuccessWithConfirmationFromUser(BindAndIssueLabelConstants.rewriteSuccessfully, () => {
          this.getRiskByIdAsync(this.riskDetail.id).pipe(take(1)).subscribe((submissionData) => {
            console.log('RiskDetail', submissionData);
            this.riskDetail = submissionData;
            this.router.navigateByUrl(`/submissions/${data.id}/${PathConstants.Submission.Applicant.Index}/${PathConstants.Submission.Applicant.NameAndAddress}`);
          });
        });
      }, (error) => {
        console.error(error);
        NotifUtils.showError(ErrorMessageConstant.issuePolicyErrorMsg);
      });
    });
  }

  GeneratePolicyNumber(callbackFn?: Function) {
    Utils.blockUI();
    this.endorsementService.GeneratePolicyNumber(this.riskDetail.id).pipe(take(1)).subscribe(data => {
      Utils.unblockUI();
      this.riskDetail.policyNumber = data;
      if (callbackFn) { callbackFn(); }
    }, (error) => {
      Utils.unblockUI();
      console.error(error);
      NotifUtils.showError(ErrorMessageConstant.issuePolicyErrorMsg);
    });

    return this.riskDetail.policyNumber
  }

  get isIssued(): boolean {
    return this.riskDetail?.isPolicy;
  }

  get isCancelledPolicy() {
    return LvRiskStatus.find(x => x.description === this.riskDetail.status)?.description === 'Canceled';
  }
}
