import { Component, HostListener, OnInit } from '@angular/core';
import { PathConstants } from '../../../../../../shared/constants/path.constants';
import { SummaryData } from '../../../../../../modules/submission/data/summary.data';
import { SubmissionData } from '../../../../../../modules/submission/data/submission.data';

@Component({
  selector: 'app-submission-summary',
  templateUrl: './submission-summary.component.html',
  styleUrls: ['./submission-summary.component.scss']
})
export class SubmissionSummaryComponent implements OnInit {
  isOpen: boolean = true;
  isShow: boolean = false;
  isCalcPremium: boolean = false;
  public pathConstants = PathConstants;
  constructor(
    public summaryData: SummaryData,
    public submissionData: SubmissionData,
  ) {
  }

  ngOnInit() {
    const risk = this.submissionData.riskDetail;
    if (risk?.id) {
      this.summaryData.form.patchValue({status: risk.subStatus, assignedToId: risk.assignedToId});
      this.summaryData.quoteStatus = risk.subStatus;
      this.summaryData.updateEstimatedPremium();
    }
  }

  collapse() {
    this.isOpen = !this.isOpen;
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event: any) {
    const scrollPosition = window.scrollY;
    if (scrollPosition >= 94) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  onStatusChanged() {
    if (this.submissionData.isIssued) { return; }
    const formValue = this.summaryData.form.value;
    this.summaryData.quoteStatus = formValue.status;
    this.summaryData.updateStatus(formValue.status, formValue.assignedToId);
  }
}
