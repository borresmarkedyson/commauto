import { Component, OnInit, ViewChild } from '@angular/core';
import { NextBackButtonComponent } from '../../submission-details-shared/components/next-back-button/next-back-button.component';
import { Location } from '@angular/common';
import { ClickTypes } from '../../../../../../shared/constants/click-type.constants';
import { MenuOverrideService } from '../../../../../../core/services/layout/menu-override.service';
import { Router } from '@angular/router';
import { BindingData } from '../../../../../../modules/submission/data/binding/binding.data';
import { BindingDto } from '../../../../../../shared/models/submission/binding/BindingDto';
import { SubmissionData } from '../../../../../../modules/submission/data/submission.data';
import { FileUploadDocumentDto } from '../../../../../../shared/models/submission/binding/FileUploadDocumentDto';
import { BindingRequirementsDto } from '../../../../../../shared/models/submission/binding/BindingRequirementsDto';
import { QuoteConditionsDto } from '../../../../../../shared/models/submission/binding/QuoteConditionsDto';
import { NotesData } from '../../../../../../modules/submission/data/note/note.data';
import { DocumentData } from '../../../../../../modules/documents/document.data';

@Component({
  selector: 'app-bind',
  templateUrl: './bind.component.html',
  styleUrls: ['./bind.component.scss']
})
export class BindComponent implements OnInit {

  @ViewChild(NextBackButtonComponent) nextBack: NextBackButtonComponent;

  constructor(private location: Location,
    private menuOverrideService: MenuOverrideService,
    private router: Router,
    public Data: BindingData,
    private submissionData: SubmissionData
  ) { }

  ngOnInit() {
    //this.Data.renderTable();
    this.Data.retrieveDropdownValues(() => {
      this.Data.initializeForms();
      setTimeout(() => {
        this.Data.loadBindReqDefaults();
        this.Data.loadQuoteConditionDefaults();
        this.initializeTables();
        // this.Data.saveBinding();
      }, 1500);
    });
  }

  private initializeTables() {
    this.Data.bindingRequirementsList?.forEach((bindingReq) => {
      bindingReq.bindRequirementsOption = this.Data.dropdownList.bindRequirementList?.find(x => x.value == bindingReq?.bindRequirementsOptionId)?.label;
      bindingReq.bindStatus = this.Data.dropdownList.bindStatusList?.find(x => x.value == bindingReq?.bindStatusId)?.label;
    });

    this.Data.quoteConditionsList?.forEach((quoteCondition) => {
      quoteCondition.quoteConditionsOption = this.Data.dropdownList.quoteConditionList?.find(x => x.value == quoteCondition?.quoteConditionsOptionId)?.label;
    });

    // sort onload
    const bindRequirementsOptionEl: HTMLElement = document.querySelector('[id=bindRequirementsOption]');
    bindRequirementsOptionEl?.setAttribute('data-order', 'asc');
    this.Data.sortBindRequirements(bindRequirementsOptionEl);

    const quoteConditionsOptionEl: HTMLElement = document.querySelector('[id=quoteConditionsOption]');
    quoteConditionsOptionEl?.setAttribute('data-order', 'asc');
    this.Data.sortQuoteConditions(quoteConditionsOptionEl);

    this.Data.setRequirementsPage(1, this.Data.bindingRequirementsList);
    this.Data.setConditionsPage(1, this.Data.quoteConditionsList);

    const riskDetail = this.submissionData?.riskDetail;
    if (riskDetail?.binding && riskDetail?.riskCoverages.length > 0) {
      if (riskDetail?.riskCoverages[+riskDetail?.binding?.bindOptionId - 1]?.depositPct < 100) { // If BindOption default value is Option 1
        this.Data.formBinding.controls.paymentTypesId.setValue(2); // Direct Bill Installment
      }
    }
  }

  public onClick(clickType?) {
    this.nextBack.location = this.location;
    let url = '';
    switch (clickType) {
      case ClickTypes.Back:
        this.menuOverrideService.overrideSideBarBehavior();
        url = this.nextBack.getPrev();
        this.router.navigate([url]);
        break;
      case ClickTypes.Next:
        this.menuOverrideService.overrideSideBarBehavior();
        url = this.nextBack.getNext();
        this.router.navigate([url]);
        break;
    }
  }

  get isScroll() {
    return false;
  }

  get hasQuoteIssued() {
    const preBindQuote = this.submissionData.riskDetail?.riskDocuments?.some(f => f.fileName === 'Pre-Bind Quote' && f.isConfidential);
    if (preBindQuote === true) {
      this.submissionData.isQuoteIssued = preBindQuote;
    }
    if (this.submissionData.isQuoteIssued === true) {
      return true;
    }
    return false;
  }
}
