import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NextBackButtonComponent } from './components/next-back-button/next-back-button.component';
import { SubmissionActionBarComponent } from './components/submission-action-bar/submission-action-bar.component';
import { LaddaModule } from 'angular2-ladda';
import { RatingModule } from '@app/modules/rating/rating.module';



@NgModule({
  declarations: [
    NextBackButtonComponent,
    SubmissionActionBarComponent,
  ],
  exports: [
    NextBackButtonComponent,
    SubmissionActionBarComponent,
  ],
  imports: [
    CommonModule,
    RatingModule,
  ],
  entryComponents: [
  ]
})
export class SubmissionDetailsSharedModule { }
