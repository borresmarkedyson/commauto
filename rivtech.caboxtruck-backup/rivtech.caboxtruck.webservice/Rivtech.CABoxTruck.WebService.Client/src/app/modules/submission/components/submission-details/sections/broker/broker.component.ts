import { Component, OnInit, ViewChild } from '@angular/core';
import { BrokerGenInfoData } from '../../../../data/broker/general-information.data';
import { ClickTypes } from '../../../../../../shared/constants/click-type.constants';
import { MenuOverrideService } from '../../../../../../core/services/layout/menu-override.service';
import { Router } from '@angular/router';
import { NextBackButtonComponent } from '../../submission-details-shared/components/next-back-button/next-back-button.component';
import { Location } from '@angular/common';
import { BrokerInfoDto } from '../../../../../../shared/models/submission/brokerinfo/brokerInfoDto';
import { BrokerValidationService } from '../../../../../../core/services/validations/broker-validation.service';
import { SubmissionNavValidateService } from '../../../../../../core/services/navigation/submission-nav-validate.service';
import { SubmissionData } from '../../../../../../modules/submission/data/submission.data';

@Component({
  selector: 'app-broker',
  templateUrl: './broker.component.html',
  styleUrls: ['./broker.component.scss']
})
export class BrokerComponent implements OnInit {
  constructor(public Data: BrokerGenInfoData,
    private router: Router,
    private menuOverrideService: MenuOverrideService,
    private location: Location,
    public brokerValidation: BrokerValidationService,
    public submissionData: SubmissionData,
    public formValidation: SubmissionNavValidateService
  ) { }

  @ViewChild(NextBackButtonComponent) nextBack: NextBackButtonComponent;

  ngOnInit() {
    this.Data.initiateFormGroup(new BrokerInfoDto());
    this.Data.dropdownList.agencyList = this.Data.dropdownList.agentList =
      this.Data.dropdownList.retailerList = this.Data.dropdownList.retailAgentList =
      this.Data.dropdownList.reasonForMove = [];
    this.Data.dateFormatOptions();
    this.Data.initiatePolicyContacts();
  }

  get isScroll() {
    return this.Data.formInfo.controls['isBrokeredAccount'].value || this.Data.formInfo.controls['isIncumbentAgency'].value || this.Data.formInfo.controls['isMidtermMove'].value;
  }

  get fc() { return this.fg.controls; }
  get fg() { return this.Data.formInfo; }

  public onClick(clickType?) {
    this.nextBack.location = this.location;
    let url = '';
    switch (clickType) {
      case ClickTypes.Back:
        this.menuOverrideService.overrideSideBarBehavior();
        url = this.nextBack.getPrev();
        this.router.navigate([url]);
        break;
      case ClickTypes.Next:
        this.menuOverrideService.overrideSideBarBehavior();
        url = this.nextBack.getNext();
        this.router.navigate([url]);
        break;
    }
  }
}
