import { Component, ElementRef, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { LimitsService } from '../../../../../../../../modules/submission/services/coverage-limits.service';
import { LimitsData } from '../../../../../../../../modules/submission/data/limits/limits.data';
import { SubmissionData } from '../../../../../../../../modules/submission/data/submission.data';
import { LimitsDefaults } from '../../../../../../../../shared/constants/limits-defaults.constants';
import { ApplicantData } from '../../../../../../../../modules/submission/data/applicant/applicant.data';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-requested-coverage-section',
  templateUrl: './requested-coverage-section.component.html',
  styleUrls: ['./requested-coverage-section.component.scss']
})
export class RequestedCoverageSectionComponent implements OnInit {
  hideMe: boolean = false;
  constructor(
    public applicantData: ApplicantData,
    public limitsData: LimitsData,
    public submissionData: SubmissionData,
    public coverageLimitService: LimitsService
  ) { }

  get f() { return this.limitsData.limitsForm.limitsPageForm.controls; }

  ngOnInit() {
    this.getCoverageLimitAll();
    this.limitsData.retrieveDropDownValues(true);
  }

  getCoverageLimitAll() {
    this.limitsData.populateLimitsFields();
    // Set defaults
    if (this.submissionData.riskDetail.riskCoverage == null) {
      this.limitsData.setDefaults();
      this.limitsData.hasRiskCoverage = false;
      return;
    }
  }

  public ToggleHiding(): void {
    this.hideMe = !this.hideMe;
  }
}
