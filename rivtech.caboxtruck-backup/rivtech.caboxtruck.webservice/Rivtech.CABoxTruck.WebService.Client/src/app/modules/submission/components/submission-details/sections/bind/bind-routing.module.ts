import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BindComponent } from './bind.component';
// import { CanDeactivateBrokerComponentGuard } from './broker-navigation-guard.service';

const routes: Routes = [

  {
    path: '',
    component: BindComponent,
    // canDeactivate: [CanDeactivateBrokerComponentGuard]
  },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers: [CanDeactivateDriverComponentGuard]
})
export class BindRoutingModule { }
