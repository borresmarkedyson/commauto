import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ApplicantData } from '../../../../../../../modules/submission/data/applicant/applicant.data';
import { ClickTypes } from '../../../../../../../shared/constants/click-type.constants';
import { MenuOverrideService } from '../../../../../../../core/services/layout/menu-override.service';
import { LayoutService } from '../../../../../../../core/services/layout/layout.service';
import { SubmissionData } from '../../../../../../../modules/submission/data/submission.data';
import { NextBackButtonComponent } from '../../../submission-details-shared/components/next-back-button/next-back-button.component';
import { ApplicantNameaddressValidationService } from '../../../../../../../core/services/validations/applicant-nameaddress-validation.service';
import { SubmissionNavValidateService } from '../../../../../../../core/services/navigation/submission-nav-validate.service';
import { NavigationService } from '../../../../../../../core/services/navigation/navigation.service';
import { Guid } from 'guid-typescript';
import NotifUtils from '../../../../../../../shared/utilities/notif-utils';
import { PageSectionsValidations } from '../../../../../../../shared/constants/page-sections.contants';

@Component({
  selector: 'app-name-and-address',
  templateUrl: './name-and-address.component.html',
  styleUrls: ['./name-and-address.component.scss']
})
export class NameAndAddressComponent implements OnInit {
  tempriskHeaderId: any;

  constructor(private router: Router,
    private menuOverrideService: MenuOverrideService,
    public location: Location,
    public submissionData: SubmissionData,
    public applicantNameAndAddressValidation: ApplicantNameaddressValidationService,
    public formValidation: SubmissionNavValidateService,
    private navigationService: NavigationService,
    public applicantData: ApplicantData) { }

  @ViewChild(NextBackButtonComponent) nextBack: NextBackButtonComponent;
  ngOnInit() {
  }

  public onClick(clickType?) {
    this.nextBack.location = this.location;
    const nextUrl = this.nextBack.getNext();
    switch (clickType) {
      case ClickTypes.Next:
        this.applicantData.applicantForms.nameAndAddressForm.markAllAsTouched();
        // stop here if form is invalid
        if (!this.applicantData.validateRequired()) {
          NotifUtils.showError(PageSectionsValidations.NameAndAddressError);
          return;
        }

        this.menuOverrideService.overrideSideBarBehavior();
        if (this.submissionData.isNew) {
          this.applicantData.saveNameAndAddress(this.navigationService.navigateRoute(nextUrl, this.submissionData.riskDetail.id));
        } else {
          this.router.navigate([nextUrl]);
        }
        break;
    }
  }
}
