import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coverage-questions-chart',
  templateUrl: './coverage-questions-chart.component.html',
  styles: [`
    .col-l { width: 80%}
    .col-m { width: 20%}`
  ]
})
export class CoverageQuestionsChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
