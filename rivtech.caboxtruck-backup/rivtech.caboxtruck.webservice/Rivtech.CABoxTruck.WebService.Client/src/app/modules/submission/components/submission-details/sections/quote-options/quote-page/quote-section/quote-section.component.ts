import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { SubmissionNavSavingService } from '../../../../../../../../core/services/navigation/submission-nav-saving.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { finalize, take, takeUntil, tap } from 'rxjs/operators';
import { QuoteLimitsData } from '../../../../../../../../modules/submission/data/quote/quote-limits.data';
import { RaterApiData } from '../../../../../../../../modules/submission/data/rater-api.data';
import { SubmissionData } from '../../../../../../../../modules/submission/data/submission.data';
import Utils from '@app/shared/utilities/utils';
import { RiskSpecificsData } from '../../../../../../../../modules/submission/data/risk-specifics.data';
import { DriverHiringCriteriaData } from '../../../../../../../../modules/submission/data/riskspecifics/driver-hiring-criteria.data';
import { SubmissionNavValidateService } from '../../../../../../../../core/services/navigation/submission-nav-validate.service';
import { PathConstants } from '../../../../../../../../shared/constants/path.constants';
import { ReportData } from '../../../../../../../../modules/submission/data/report/report.data';

@Component({
  selector: 'app-quote-section',
  templateUrl: './quote-section.component.html',
  styleUrls: ['./quote-section.component.scss']
})
export class QuoteSectionComponent implements OnInit {
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  @ViewChild('btnShowLoadingUploader') btnShowLoadingUploader;
  @ViewChild('fileSelection') fileSelection: ElementRef;
  @Output() public onUploadFinished = new EventEmitter();
  dropdownSettings: IDropdownSettings = {};
  public message: string;
  public progress: number;
  public styleLoading: string;
  hideMe: boolean = false;


  constructor(
    public quoteLimitsData: QuoteLimitsData,
    public raterApiData: RaterApiData,
    public submissionData: SubmissionData,
    public submissionNavSavingService: SubmissionNavSavingService,
    private riskSpecificsData: RiskSpecificsData,
    private driverHiringCriteriaData: DriverHiringCriteriaData,
    private submissionValidationService: SubmissionNavValidateService,
    private reportData: ReportData
  ) { }

  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'value',
      textField: 'label',
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    Utils.blockUI();
    // if these screens are skipped. Should include in validation.
    this.riskSpecificsData.isInitialMaintenanceAndSatefy = false;
    this.driverHiringCriteriaData.isInitial = false;
    this.riskSpecificsData.isInitialGeneralLiabAndCargo = false;
    this.submissionValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.MaintenanceSafety);
    this.submissionValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.DriverInfo);
    this.submissionValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.GeneralLiabilityCargo);
    this.submissionValidationService.validateCurrentCategory(PathConstants.Submission.Claims.Index);
    // ---------------------------------------------------------------
    this.quoteLimitsData.loadQuoteOptions().pipe(
      //tap(() => this.raterApiData.validateRequiredToRateModules())
    ).subscribe(() => Utils.unblockUI());

    if(this.newBusiness && this.requiredModulesValid) {
      this.raterApiData.preloadRater().subscribe();
    } 
  }

  get subjectivitiesList() { return this.quoteLimitsData.subjectivities; }

  get xpRaterButtonDisabled() {
    return ((!this.requiredModulesValid && this.hasLiabilityRF));
  }

  get xpRaterTooltip() {
    if(this.xpRaterButtonDisabled && !this.requiredModulesValid) {
        return this.raterApiData.showRequiredtoRateToolTip;
    }
    return null;
  }

  get requiredModulesValid() {
    return this.raterApiData.validateRequiredToRateModules();
  }

  get newBusiness() {
    return this.submissionData.riskDetail.businessDetails.isNewVenture;
  }
  
  get hasLiabilityRF() {
    var riskCov = this.submissionData?.riskDetail?.riskCoverages[0];
    var hasRF =  riskCov.liabilityExperienceRatingFactor != null 
      && riskCov.liabilityScheduleRatingFactor != null;
    return hasRF;
  }

  public ToggleHiding(): void {
    this.hideMe = !this.hideMe;
  }

  clickUpload(f) {
    this.fileSelection.nativeElement.value = null;
    this.fileSelection.nativeElement.click();
  }

  public uploadFile = (f) => {
    if (!this.raterApiData.validateRequiredToRateModules()) { return; }
    this.raterApiData.btnShowLoadingUploader = this.btnShowLoadingUploader;
    this.raterApiData.onUploadFinished = this.onUploadFinished;
    this.raterApiData.upload(f);
  }

  clickDownload(obj) {
    obj.preventDefault();
    obj.stopPropagation();

    obj.target.disabled = true;

    if (!this.raterApiData.validateRequiredToRateModules()) { obj.preventDefault(); return; }
    this.raterApiData.btnShowLoadingUploader = this.btnShowLoadingUploader;
    this.raterApiData.onUploadFinished = this.onUploadFinished;
    if (this.submissionNavSavingService.isSaving) {
      this.submissionNavSavingService.saveCategoryEnded.first().pipe(take(1)).subscribe(() => {
        this.triggerExpRaterDownload(obj);
      });
    } else {
      this.triggerExpRaterDownload(obj);
    }
  }


  private triggerExpRaterDownload(obj): void {
    this.quoteLimitsData.saveOptions()
      .pipe(tap(() => this.raterApiData.Download(obj)))
      .subscribe();
  }
}
