import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { LimitsData } from '../../../../../../../../../modules/submission/data/limits/limits.data';
import { OptionForm, QuoteLimitsData, QuoteLimitsForm } from '../../../../../../../../../modules/submission/data/quote/quote-limits.data';
import { SubmissionData } from '../../../../../../../../../modules/submission/data/submission.data'; import { RaterApiData } from '../../../../../../../../../modules/submission/data/rater-api.data';
import { LayoutService } from '../../../../../../../../../core/services/layout/layout.service';
import { GeneralLiabilityCargoValidationService } from '../../../../../../../../../core/services/validations/general-liability-cargo-validation.service';
import { SubmissionNavValidateService } from '../../../../../../../../../core/services/navigation/submission-nav-validate.service';
import { PathConstants } from '../../../../../../../../../shared/constants/path.constants';
import { createSubmissionDetailsMenuItems } from '../../../../../../../../../modules/submission/components/submission-details/submission-details-navitems';
import { RiskSpecificsData } from '../../../../../../../../../modules/submission/data/risk-specifics.data';
import { ToastrService } from 'ngx-toastr';
import { SubmissionNavSavingService } from '../../../../../../../../../core/services/navigation/submission-nav-saving.service';
import { finalize, first, map, switchMap, take, tap } from 'rxjs/operators';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { RiskCoverageDTO, RiskCoverageListDTO } from '@app/shared/models/submission/limits/riskCoverageDto';
import Utils from '../../../../../../../../../shared/utilities/utils';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { RiskCoveragePremiumDTO } from '@app/shared/models/submission/limits/riskCoveragePremiumDto';
import { Subscription } from 'rxjs';
import { ReportData } from '@app/modules/submission/data/report/report.data';


@Component({
  selector: 'app-quote-auto-liability-chart',
  templateUrl: './auto-liability-chart.component.html',
  styleUrls: ['./auto-liability-chart.component.scss']
})
export class QuoteAutoLiabilityChartComponent implements OnInit {
  maxArray: number = 3;

  pctSettings: any = {
    align: 'left',
    allowNegative: false,
    max: 100,
    prefix: '',
    suffix: '%',
    precision: 1,
    nullable: true,
  };
  currencySettings: any = {
    align: 'left',
    allowNegative: false,
    precision: 2,
    nullable: true
  };

  showPropertyDamageField: boolean = false;

  /**
   * Holds references to all subscription that will be unsubscribed upon component's onDestroy event.
   */
  subscriptions: Subscription[] = [];

  /**
   * Indicates whether the component is ready to do some tasks upon init or waits if not yet ready. 
   */
  isReady = new BehaviorSubject<boolean>(false);
  
  get quoteOptionsForm() {
    return this.quoteLimitsData.quoteOptionsForm;
  }

  get optionsArray() {
    return this.quoteLimitsData.optionsArray;
  }

  constructor(
    public quoteLimitsData: QuoteLimitsData,
    public limitsData: LimitsData,
    private submissionData: SubmissionData,
    private raterApiData: RaterApiData,
    private submissionNavValidationService: SubmissionNavValidateService,
    private layoutService: LayoutService,
    private riskSpecificsData: RiskSpecificsData,
    private toastr: ToastrService,
    private submissionNavSavingService: SubmissionNavSavingService,
    private reportData: ReportData
  ) {
    this.quoteLimitsData.initiateFormFields();
  }

  ngOnInit() {

    let subscription = this.isReady.asObservable().subscribe(isReady => {
      if (!isReady) { return; }
      this.onInitValidations();
      //this.quoteLimitsData.loadQuoteOptions().subscribe();

      // Whenever rater data changes,
      // reload quotes data.
      this.raterApiData.ratingData.pipe(
        switchMap(() =>  this.quoteLimitsData.loadQuoteOptions()),
        switchMap(() => {
          //this.quoteLimitsData.setQuoteDefaults();
          console.log(this.submissionData.riskDetail.riskCoverages[0].liabilityScheduleRatingFactor);
          return this.raterApiData.preloadRater();
        } )
      ).subscribe();
    });
    this.subscriptions.push(subscription);

    // to wait on other data saving processes whenever user navigate quickly
    if (this.submissionNavSavingService.isSaving) {
      this.submissionNavSavingService.saveCategoryEnded.first().pipe(take(1)).subscribe(() => this.isReady.next(true));
    } else {
      this.isReady.next(true);
    }

  }

  // tslint:disable-next-line: use-lifecycle-interface
  async ngOnDestroy() {
    // unloading premium when changing url or redirecting
    //this.raterApiData.UnloadPremium(true); //Moved to submission-details.component for all pages use
    //this.subscriptions.forEach(subs => subs.unsubscribe());

  }



  onInitValidations() {
    this.quoteLimitsData.yearsInBusiness = this.submissionData?.riskDetail?.businessDetails?.yearsInBusiness ?? 0;
    // execute preload only if all is valid for rating.

    // triggers the bug when changing value from Limit - > Quote option page directly
    // this.generalLiabilityCargoValidation.checkGeneralLiabilityCargoPage();
    // this.submissionData.validationList.generalLiabilityCargo = this.submissionNavValidateService.generalLiabilityCargoValidStatus;
    // this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.GeneralLiabilityCargo);

    // we use this method to trigger all required field that is not filled out for rating.
    this.validateCategories();
    if (this.raterApiData.validateRequiredToRateModules()) {
      //this.raterApiData.PreLoadPremium();
    }
  }

  setTooltip(field, dataList) {
    if (field && dataList?.length > 0) {
      const data = dataList.find(x => x.value == field);
      if (data) {
        return data.label;
      }
    }
  }

  setValue(name) {
    this.quoteLimitsData[name] = true;
  }

  /**
   * Copies the selected option. Binded to 'Copy' button click event.
   * @param optionForm The OptionForm object to copy.
   */
  copyOption(fgOption: FormGroup) {
    if (this.optionsArray.controls.length > this.maxArray) { return; }
    Utils.blockUI();
    this.quoteLimitsData.saveOptions()
      .pipe(
        switchMap(() => this.quoteLimitsData.copyOption(fgOption)),
        tap(() => {
          this.raterApiData.preloadRater().subscribe();
          this.quoteLimitsData.updateDriver();
          this.quoteLimitsData.updateVehicle();
          this.quoteLimitsData.updateAllColumnPdDisplayValue();
          this.quoteLimitsData.autoComputeAdditionalInterest(fgOption);
        }),
        finalize(() => Utils.unblockUI())
      ).subscribe();
  }

  /**
   * Deletes the selected option. Binded to 'Delete' button click event. 
   * @param optionForm 
   */
  deleteOption(fgOption: FormGroup) {
    Utils.blockUI();
    this.quoteLimitsData.saveOptions()
      .pipe(
        switchMap(() => this.quoteLimitsData.deleteOption(fgOption)),
        tap(() => {
          this.quoteLimitsData.updateDriver(false);
          this.quoteLimitsData.updateVehicle(false);
          this.quoteLimitsData.updateBinding();
        }),
        finalize(() => Utils.unblockUI())
      ).subscribe();
  }

  computePrice(fgOption: FormGroup) {
    this.reportData.calculatePremium(fgOption.value.optionNumber);
  }

  get f() { return this.quoteLimitsData.quoteLimitsForm.limitsForm.controls; }

  validateCategories(): void {
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Applicant.NameAndAddress);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Applicant.BusinessDetails);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Applicant.FilingsInformation);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Applicant.AdditionalInterest);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Broker.Index);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Limits.LimitsPage);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Vehicle.Index);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Driver.Index);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Coverages.HistoricalCoverage);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.DestinationInfo);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.DriverInfo);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.DotInfo);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.MaintenanceSafety);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.GeneralLiabilityCargo);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.RiskSpecifics.UWQuestions);
    this.submissionNavValidationService.validateCurrentCategory(PathConstants.Submission.Quote.Index);
    this.layoutService.updateMenu(createSubmissionDetailsMenuItems(this.submissionData.riskDetail.id, this.submissionData.validationList,
      this.riskSpecificsData.hiddenNavItems));
  }

  changeAL(isChange: boolean = false, index: any) {
    this.quoteLimitsData.checkAndSetPdDisplayAndValue(isChange, index);
  }

  public onDepositPercentageKeyup(fgOption: FormGroup) {
    if (this.submissionData?.isIssued) { return; }
    let optionForm: OptionForm = OptionForm.create(fgOption.value);
    if (optionForm?.premium > 0) {
      optionForm.recalculateDepositPremium();
      fgOption.controls["depositPremium"].patchValue(optionForm.depositPremium);
    }
  }

  public onDepositPremiumKeyup(fgOption: FormGroup) {
    if (this.submissionData?.isIssued) { return; }
    let optionForm = OptionForm.create(fgOption.value);
    if (!(optionForm.premium > 0) || !(optionForm.depositPremium > 0)) { return; }
    if (optionForm.depositPremium >= optionForm.premium) {
      optionForm.equalizePremiumAndDepositPremium();
    } else {
      optionForm.recalculateDepositPercentage();
    }

    fgOption.patchValue(optionForm);
  }

  public depositPercentageChanged(fgOption: FormGroup) {
    this.udpatePaymentOption(fgOption);
  }

  public depositPremiumChanged(fgOption: FormGroup) {
    this.udpatePaymentOption(fgOption);
  }

  public numberOfInstallmentChanged(fgOption: FormGroup) {
    this.udpatePaymentOption(fgOption);
  }

  public cargoChanged(fgOption: FormGroup) {
    if (fgOption.value.cargoLimitId === '53') {
      fgOption.controls['refCargoLimitId'].setValue('157');
    }
  }

  private udpatePaymentOption(fgOption: FormGroup) {
    if (this.submissionData?.isIssued) { return; }
    if (this.disableFieldsByPaymentPlan(fgOption.value.optionNumber - 1)) { return; }
    this.showInstallmentDropdown(fgOption);
    if (fgOption.value.depositPremium > 0) {
      Utils.blockUI();
      this.quoteLimitsData.updatePaymentOption(fgOption)
        .pipe(finalize(() => Utils.unblockUI())
      ).subscribe();
    }
  }

  disableFieldsByPaymentPlan(index: number): boolean {
    if (!this.submissionData?.riskDetail?.binding) { return false; }
    const bindOptionId = Number(this.submissionData?.riskDetail?.binding?.bindOptionId);
    const paymentPlanId = Number(this.submissionData?.riskDetail?.binding?.paymentTypesId);
    const optionNumber = index + 1;
    if (optionNumber === bindOptionId && paymentPlanId !== 2) {
      return true;
    } else {
      return false;
    }
  }

  showInstallmentDropdown(fgOption: FormGroup): boolean {
    return fgOption.value.depositPct < 100;
  }

}

