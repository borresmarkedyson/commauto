import { EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { SubmissionListData } from '../../../data/submission-list.data';
@Component({
  selector: 'app-submission-search-filter',
  templateUrl: './submission-search-filter.component.html',
  styleUrls: ['./submission-search-filter.component.scss']
})
export class SubmissionSearchFilterComponent implements OnInit {
  searchFilterForm: FormGroup;
  statusData: any = [
    {value: 1, label: 'Received'},
    {value: 2, label: 'In Research'},
    {value: 3, label: 'Incomplete'},
    {value: 4, label: 'Complete'},
    {value: 5, label: 'Withdrawn'},
    {value: 7, label: 'Quoted'},
    {value: 8, label: 'Canceled'},
    {value: 9, label: 'Declined'},
    {value: 10, label: 'Active'},
  ];
  hasMarginRight: boolean = true;

  @Output() submitSearch: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(
    public submissionListData: SubmissionListData
  ) { }

  ngOnInit(): void {
    this.submissionListData.initForms();
    this.searchFilterForm = this.submissionListData.constructFormGroup();
  }

  onSubmit() {
    this.submitSearch.emit(this.searchFilterForm);
  }
}
