import { Component, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SummaryData } from '@app/modules/submission/data/summary.data';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SubmissionListData } from '../../../data/submission-list.data';

@Component({
  selector: 'app-submission-table',
  templateUrl: './submission-table.component.html',
  styleUrls: ['./submission-table.component.scss']
})
export class SubmissionTableComponent implements OnInit {

  constructor(
    public submissionListData: SubmissionListData,
    public summaryData: SummaryData
  ) { }

  ngOnInit(): void {
  }

  setPage(page: number) {
    this.submissionListData.setPage(page);
  }

  getName(assignedToId) {
    if (assignedToId == null || assignedToId == '') { return; }
    this.summaryData.initialize();
    const assigned = this.summaryData.assignedToList.find(x => x.value == assignedToId);
    if (assigned != null) { return assigned.label; }
    return;
  }
}
