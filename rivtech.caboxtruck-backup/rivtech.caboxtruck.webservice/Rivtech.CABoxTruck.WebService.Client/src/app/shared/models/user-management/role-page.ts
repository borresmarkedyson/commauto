
export class RolePage {
    
    public rolePageId: number;

    public applicationId: number;

    public menuId: number;

    public roleId: number;

    public isActive: boolean;

    // public createdDate: Date;

    // public isActive: Boolean;

}
