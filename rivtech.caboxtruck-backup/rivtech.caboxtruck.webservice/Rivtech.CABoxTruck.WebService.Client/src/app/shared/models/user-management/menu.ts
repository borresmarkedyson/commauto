export class Menu {
    menuId: number;
    parentId: number;
    applicationId: number;
    menuLabel: string;
    controllerName: string;
    createdDate: Date;
    isActive: boolean;
}