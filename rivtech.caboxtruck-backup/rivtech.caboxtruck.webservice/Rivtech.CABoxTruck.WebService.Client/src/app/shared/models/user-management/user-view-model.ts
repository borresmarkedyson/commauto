export class UserViewModel {
    public userID: number;
    public userName: string;
    public applicationID: number;
    public carrierCode: string;
    public agencyID: number;
    public subAgencyID: number;
    public emailAddress: string;
    public createdDate: Date;
    public isActive: boolean;
    public companyName: string;
    public firstName: string;
    public lastName: string;
}
