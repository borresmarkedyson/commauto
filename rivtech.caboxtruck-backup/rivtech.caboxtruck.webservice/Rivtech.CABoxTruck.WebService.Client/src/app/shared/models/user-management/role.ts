

export class Role {
    
    public roleId?: number;

    public applicationId?: number;

    public roleGroupId?: number;

    public roleName?: string;

    public createdDate?: Date;

    public isActive?: Boolean;

}
