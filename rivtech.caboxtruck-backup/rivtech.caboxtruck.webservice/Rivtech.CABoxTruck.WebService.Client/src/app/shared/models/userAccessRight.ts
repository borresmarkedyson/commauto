export class UserAccessRight {
    userAccessRightId: number;
    userName: string;
    roleId: number;
    menuId: number;
    isView: boolean;
    isAdd: boolean;
    isEdit: boolean;
    isDelete: boolean;
    isActive: boolean;
}