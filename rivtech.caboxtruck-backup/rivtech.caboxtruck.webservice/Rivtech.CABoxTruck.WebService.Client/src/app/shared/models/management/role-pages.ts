export class RolePages {
    rolePageId: number;
    applicationId: number;
    menuId: number;
    roleId: number;
    createdDate: string;
    isActive: boolean;
}
