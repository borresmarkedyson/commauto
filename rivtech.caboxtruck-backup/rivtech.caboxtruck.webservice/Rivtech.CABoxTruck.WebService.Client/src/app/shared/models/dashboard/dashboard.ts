export class DashboardDTO {
  submissionCount: number;
  completeCount: number;
  declinedCount: number;
  inResearchCount: number;
  inCompleteCount: number;
  quotedCount: number;
  receivedCount: number;
  withdrawnCount: number;
  activePoliciesCount: number;
  cancelledPoliciesCount: number;
  pendingCancellationCount: number;

  public constructor(init?: Partial<DashboardDTO>) {
    Object.assign(this, init);
  }
}
