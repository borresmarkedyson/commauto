
export class SubmissionDetails {
    public dateReceived: Date;
    public effectiveDate: Date;
    public quoteNeededBy: Date;
    public programId: number;
    public jurisdictionState: number;
    public productType: number;
    public underwriterId: number;   
}