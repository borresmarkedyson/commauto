export class FilterSubmissionListDTO implements IFilterSubmissionListDTO {
  submissionNumber?: string | undefined;
  insuredName?: string | undefined;
  broker?: string | undefined;
  underwriter?: string | undefined;
  au_rep?: string | undefined;
  au_psr?: string | undefined;
  owner?: string | undefined;
  inceptionDateFrom?: Date | undefined;
  inceptionDateTo?: Date | undefined;
  status?: string | undefined;
  state?: string | undefined;
  searchText?: string | undefined;
}

export interface IFilterSubmissionListDTO {
  submissionNumber?: string | undefined;
  insuredName?: string | undefined;
  broker?: string | undefined;
  underwriter?: string | undefined;
  au_rep?: string | undefined;
  au_psr?: string | undefined;
  owner?: string | undefined;
  inceptionDateFrom?: Date | undefined;
  inceptionDateTo?: Date | undefined;
  status?: string | undefined;
  state?: string | undefined;
  searchText?: string | undefined;
}
