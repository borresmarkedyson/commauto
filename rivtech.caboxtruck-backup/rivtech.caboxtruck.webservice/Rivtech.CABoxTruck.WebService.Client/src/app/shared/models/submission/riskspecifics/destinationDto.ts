export class DestinationDto implements IDestinationDto {
  id?: string = null;
  destination?: string;
  city?: string = null;
  state?: string = null;
  zipCode?: string = null;
  percentageOfTravel?: number = null;
  totalPercentageOfTravel?: number = 0;
  currentDestinations?: string[] = [];
  riskDetailId?: string = null;

  constructor(init?: Partial<IDestinationDto>) {
    if (init) {
      for (const property in init) {
        if (init.hasOwnProperty(property)) {
          (<any>this)[property] = (<any>init)[property];
        }
      }
    }
  }
}

export interface IDestinationDto {
  id?: string;
  destination?: string;
  city?: string;
  state?: string;
  zipCode?: string;
  percentageOfTravel?: number;
  totalPercentageOfTravel?: number;
  currentDestinations?: string[];
  riskDetailId?: string;
}
