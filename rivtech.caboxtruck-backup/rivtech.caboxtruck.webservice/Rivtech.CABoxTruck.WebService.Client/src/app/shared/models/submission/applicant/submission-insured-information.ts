
export class SubmissionInsuredInformation {
    public insuredName: string;
    public insuredStreetAddress: string;
    public insuredStreetAddress2: string;
    public insuredZipCode: string;
    public insuredCity: string;
    public insuredState: string;
    public insuredPhone: string;
}