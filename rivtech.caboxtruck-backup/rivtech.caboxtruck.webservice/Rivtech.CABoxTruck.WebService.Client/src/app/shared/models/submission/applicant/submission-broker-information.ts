export class SubmissionBrokerInformation {
    public brokerId: number;
    public agentId: number;
    public agentEmail: string;
    public brokerCommission: number;
}

