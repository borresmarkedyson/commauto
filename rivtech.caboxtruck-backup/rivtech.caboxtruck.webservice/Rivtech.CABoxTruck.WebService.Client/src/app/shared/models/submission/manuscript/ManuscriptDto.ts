
export class ManuscriptDTO implements IManuscriptDTO {
    id?: string;
    riskDetailId: string | undefined;
    title: string | undefined;
    description: string | undefined;
    premium: number | undefined;
    proratedPremium: number | undefined;
    isProrate: boolean | undefined;
    isSelected?: boolean | undefined;
    isActive?: boolean | undefined;
    effectiveDate?: Date | undefined;
    expirationDate?: Date | undefined;
    deletedDate?: Date | undefined;
    createdBy?: number | undefined;
    createdDate?: Date | undefined;
    premiumType: string | undefined;
    fromPreviousEndorsement: boolean | undefined;

    constructor(init?: Partial<ManuscriptDTO>) {
        Object.assign(this, init);
      }
}

export interface IManuscriptDTO {
    id?: string;
    riskDetailId: string | undefined;
    title: string | undefined;
    description: string | undefined;
    premium: number | undefined;
    proratedPremium: number | undefined;
    isProrate: boolean | undefined;
    isSelected?: boolean | undefined;
    isActive?: boolean | undefined;
    effectiveDate?: Date | undefined;
    expirationDate?: Date | undefined;
    deletedDate?: Date | undefined;
    createdBy?: number | undefined;
    createdDate?: Date | undefined;
    premiumType: string | undefined;
    fromPreviousEndorsement: boolean | undefined;
}




