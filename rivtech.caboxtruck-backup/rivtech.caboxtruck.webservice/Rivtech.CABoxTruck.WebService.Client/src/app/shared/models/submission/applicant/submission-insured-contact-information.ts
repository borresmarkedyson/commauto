
export class SubmissionInsuredContactInformation {
    public insuredContactFirstName: string;
    public insuredContactLastName: string;
    public insuredContactPhone: string;
    public insuredContactEmail: string;
}