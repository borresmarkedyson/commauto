export interface SelectItem {
  label?: string;
  value?: any;
  isActive?: boolean;
  sortOrder?: number;
}
