export const BillingLabelConstants = {
  billingTitle: 'Billing',
  paymentPlan: 'Pay Plan Option',
  downpayment: 'Downpayment Amount',
  paymentMethod: 'Payment Method',
  cardHolderDetails: 'Card Holder Details',
  firstName: 'First Name',
  lastName: 'Last Name',
  address: 'Address',
  city: 'City',
  state: 'State',
  zipCode: 'Zip Code',
  emailAddress: 'Email Address',
  creditCardDetails: 'Credit Card Details',
  cardType: 'Credit Card Type',
  creditCardNumber: 'Credit Card Number',
  creditCardExpirationMonth: 'Expiration Month',
  creditCardExpirationYear: 'Expiration Year',
  cvv: 'CVV',
  cid: 'CID',
  totalChargeAmount: 'Total Charge Amount',
  comments: 'Comments',
  bankAccountInformation: 'Bank Account Information',
  accountType: 'Account Type',
  routingNumber: 'Routing Number',
  accountNumber: 'Account Number',
  nameOnAccount: 'Name on Account',
  nextTermPaymentPlan: 'Next Term Payment Plan',
  payLater: 'Pay Later',
  enrollAutoPayTermsAndCondition: 'I understand that this authorization will remain in effect throughout all policy renewal terms, until I cancel it in writing or by phone and /or I terminate my policy.  I agree to notify Commercial Auto, via my agent, in writing or by phone at least 10 days prior to the next billing date of any changes in my account information or of any request to terminate this authorization.  In the case of a payment being rejected by my financial institution, I understand that Commercial Auto may assess a Non-sufficient Funds (NSF) charge to my policy.  By clicking the button below, I certify that I am an authorized user of this credit card/bank account and will not dispute these scheduled transactions with my financial institution, so long as the transactions correspond to the terms indicated in this authorization form. A copy of this authorization will be emailed to the insured outlining the dates their financial institution will have the funds drawn based on the pay plan selected. This authorization includes payments for either Commercial Auto Specialty Insurance Company or Commercial Auto National Insurance Company.',
  oneTimePaymentLanguage: 'By clicking the button below, I certify that I am an authorized user of this credit card/bank account and will not dispute this one-time payment with my financial institution. This authorization includes payments for either Commercial Auto Specialty Insurance Company or Commercial Auto National Insurance Company.',
  enrollAutoPayAgree: 'I agree',
  oneTimePaymentAgree: 'I agree'
};