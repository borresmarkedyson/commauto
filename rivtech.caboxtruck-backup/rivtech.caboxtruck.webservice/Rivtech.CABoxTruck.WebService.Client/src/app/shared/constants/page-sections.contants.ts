export enum PageSectionsValidations {
    ConfirmMessage = 'There are still missing Required to Quote fields.<br>Would you like to proceed?',
    ConfirmRateMessage = 'There are still missing Required to Rate fields.<br>Would you like to proceed?',
    ConfirmMessageForSubmission = 'Any changes will not be saved.<br>Would you like to proceed?',
    NameAndAddressError = 'Business Name, Business Address, and Zip Code fields are required to proceed',
    CurrentPage = 'Current',
    // Dashboard = '/dashboard',
    SubmissionList = '/submissions/list',
    ManagementPage = '/management/agency',
  }