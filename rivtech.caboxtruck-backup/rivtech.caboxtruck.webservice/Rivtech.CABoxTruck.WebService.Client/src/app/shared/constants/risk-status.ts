export const LvRiskStatus = [
  {
    id: 1,
    code: 'PEN',
    description: 'Pending'
  },
  {
    id: 2,
    code: 'IF',
    description: 'In-Force'
  },
  {
    id: 3,
    code: 'RUW',
    description: 'Referred to UW'
  },
  {
    id: 4,
    code: 'AUW',
    description: 'Approved by UW'
  },
  {
    id: 5,
    code: 'DUW',
    description: 'Declined by UW'
  },
  {
    id: 6,
    code: 'QEXP',
    description: 'Quote Expired'
  },
  {
    id: 7,
    code: 'ACT',
    description: 'Active'
  },
  {
    id: 8,
    code: 'PEC',
    description: 'Pending Cancellation'
  },
  {
    id: 9,
    code: 'CAN',
    description: 'Canceled'
  },
  {
    id: 10,
    code: 'PER',
    description: 'Pending Renewal'
  },
  {
    id: 11,
    code: 'PRPYMNT',
    description: 'Pending Renewal Payment'
  },
  {
    id: 12,
    code: 'QUO',
    description: 'Quoted'
  },
  {
    id: 13,
    code: 'APP',
    description: 'Application'
  },
  {
    id: 14,
    code: 'PEXP',
    description: 'Policy Expired'
  },
  {
    id: 15,
    code: 'AIR',
    description: 'Additional Information Required'
  },
  {
    id: 16,
    code: 'ISS',
    description: 'Issued'
  },
  {
    id: 17,
    code: 'PNR',
    description: 'Pending Non-Renewal'
  },
];

export const StatusConstants = {
  approvedByUW: 'AUW',
  pending: 'PEN',
  cancelled: 'CAN'
};