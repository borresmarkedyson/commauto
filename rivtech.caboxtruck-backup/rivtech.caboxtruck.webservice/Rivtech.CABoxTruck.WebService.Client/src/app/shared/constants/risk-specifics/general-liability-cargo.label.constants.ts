export const GeneralLiabilityConstants = {
    generalLiabilityTitle: 'General Liability',
    contractuallyRequiredToCarryGeneralLiabilityInsurance: 'Are you contractually required to carry General Liability insurance?',
    operationsAtStorageLotOrImpoundYard: 'Are any operations at a storage lot or impound yard?',
    anyStorageOfGoods: 'Any storage of goods?',
    anyWarehousing: 'Any warehousing?',
    anyStorageOfVehiclesForOthers: 'Any storage of vehicles for others?',
    anyLeasingSpaceToOthers: 'Any leasing space to others?',
    anyFreightForwarding: 'Any freight forwarding?',
    anyStorageOfFuelsAndOrChemicals: 'Any storage of fuels and/or chemicals?',
    glAnnualPayroll: 'GL Annual Payroll:',
    haveOperationsOtherThanTrucking: 'Does the applicant have any operations other than trucking?',
    hasApplicantHadGeneralLiabilityLoss: 'Has applicant ever had a general liability loss?'
};

export const CargoConstants = {
    cargoTitle: 'Cargo',
    areRequiredToCarryCargoInsurance: 'Are you contractually required to carry cargo insurance?',
    requireRefrigerationBreakdownCoverage: 'Do you require refrigeration breakdown coverage?',
    haulHazardousMaterials: 'Do you haul any hazardous materials?',
    haveRefrigeratedUnits: 'Do you have any refrigerated units?',
    areCommoditiesStoredInTruckOvernight: 'Are commodities ever stored in truck overnight?',
    hadCargoLoss: 'Has applicant ever had a cargo loss?'
};
