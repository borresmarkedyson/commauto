export const MaintenanceQuestionConstants = {
    maintenanceQuestionTitle: 'Maintenance Questions',
    safetyMeetings: 'How often do you hold safety meetings?',
    personInCharge: 'Who is in charge of claims?',
    garagingType: 'Garaging Type:',
    vehicleMaintenance: 'Your vehicle maintenance program includes:',
    isMaintenanceProgramManagedByCompany: 'Is your maintenance program managed by your company?',
    provideCompleteMaintenanceOnVehicles: 'Do you provide complete maintenance on all vehicles?',
    areDriverFilesAvailableForReview: 'Are Driver Files available for review?',
    areAccidentFilesAvailableForReview: 'Are Accident Files available for review?',
    companyPractice: 'Company practices include:'
};

export const SafetyDeviceConstants = {
    safetyDeviceTitle: 'Safety Devices',
    safetyDeviceCategory: 'Safety device category:',
    safetyDeviceCategoryHeader: 'Safety Device Category',
    isInPlace: 'Currently in Place?',
    unitDescription: 'Unit Description<br>(i.e. Manufacturer/Device Name)',
    yearsInPlace: 'If YES, number of years in place?',
    percentageOfFleet: 'Percentage of Fleet',
};

