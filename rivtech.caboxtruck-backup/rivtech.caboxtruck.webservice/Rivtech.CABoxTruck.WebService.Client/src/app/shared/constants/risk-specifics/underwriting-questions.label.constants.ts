export const UnderwritingQuestionConstants = {
    underwritingQuestionTitle: 'Underwriting Questions',
    areWorkersCompensationProvided: 'Do you provide worker\'s compensation for all employees?',
    workersCompensationCarrier: 'Please provide carrier',
    areAllEquipmentOperatedUnderApplicantsAuthority: 'Is all equipment operated under the applicant\'s authority scheduled on the applicant\'s driver and vehicle schedule?',
    hasInsuranceBeenObtainedThruAssignedRiskPlan: 'During the past 4 years, has your insurance ever been obtained through an Assigned Risk Plan?',
    hasAnyCompanyProvidedNoticeOfCancellation: 'Has any company provided notice of cancellation/non-renewal or otherwise canceled/refused to renew your insurance, including during the current term?',
    hasFiledForBankruptcy: 'Have you ever filed for or contemplated filing for bankruptcy or had bankruptcy proceedings initiated against you by another party?',
    hasOperatingAuthoritySuspended: 'Has your operating authority ever been suspended or revoked or have you received notice of intent to suspend?',
    hasVehicleCountBeenAffectedByCovid19: 'Has your vehicle count been affected by Covid-19?',
    numberOfVehiclesRunningDuringCovid19: 'How many vehicles were running during Covid-19?',
    confirm: 'I confirm that the answers to the above are correct to my knowledge.',
    custominvalid: 'Fill in required fields.'
};

