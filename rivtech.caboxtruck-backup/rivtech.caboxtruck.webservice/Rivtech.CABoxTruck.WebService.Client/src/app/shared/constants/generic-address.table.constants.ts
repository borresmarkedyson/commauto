export const GenericAddressTableConstants = {
  addressType: 'Type',
  address: 'Address',
  city: 'City',
  zipCode: 'Zip Code',
  state: 'State',
};