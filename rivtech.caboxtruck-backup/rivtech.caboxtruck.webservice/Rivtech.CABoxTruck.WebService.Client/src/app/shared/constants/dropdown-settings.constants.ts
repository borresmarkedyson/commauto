import { IDropdownSettings } from 'ng-multiselect-dropdown';

export const DropdownSettingsConstants: IDropdownSettings = {
    singleSelection: false,
    idField: 'value',
    textField: 'label',
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
};