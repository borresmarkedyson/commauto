export const Options = [
  { value: 1, label: 'Option 1' },
  { value: 2, label: 'Option 2' },
  { value: 3, label: 'Option 3' },
  { value: 4, label: 'Option 4' },
];

export const ApplyFilter = [
  { value: 1, label: 'KO Under 25' },
  { value: 2, label: 'KO AF/Moving Violations' },
];
