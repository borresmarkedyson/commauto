export const AccountAchUsageTypeConstants = {
  Unknown: {
    id: '0',
    name: 'Unknown'
  },
  Business: {
    id: '1',
    name: 'Business'
  },
  Personal: {
    id: '2',
    name: 'Personal'
  }
};
