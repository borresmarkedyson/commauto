export const PathConstants = {
  PaymentManagement: {
    Index: 'payment-management',
    ClientPortal: 'client-portal',
    DepositPayment: 'deposit-payment'
  }
};
