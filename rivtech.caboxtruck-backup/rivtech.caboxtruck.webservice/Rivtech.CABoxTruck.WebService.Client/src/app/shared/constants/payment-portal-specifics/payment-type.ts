export const PaymentType = {
    Payment: {
        id: "P",
        name: "Payment"
    },
    Void: { 
        id: "V",
        name: "Void"
    },
    WriteOff: { 
        id: "W",
        name: "Write Off"
    },
};