export const AccountAchTypeConstants = {
  Unknown: {
    id: '0',
    name: 'Unknown'
  },
  Checking: {
    id: '1',
    name: 'Checking'
  },
  Savings: {
    id: '2',
    name: 'Savings'
  }
};
