export const InstrumentConstants = {
  CreditCard:
    {
      id: 'CC',
      name: 'Credit Card'
    },
  EFT:
    {
      id: 'EFT',
      name: 'EFT'
    },
};
