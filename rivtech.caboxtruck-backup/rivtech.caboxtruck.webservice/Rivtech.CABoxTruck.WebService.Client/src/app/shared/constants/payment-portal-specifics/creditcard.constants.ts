export const CreditCardConstants = [
  {
    id: '4',
    name: 'American Express'
  },
  {
    id: '3',
    name: 'Discover'
  },
  {
    id: '2',
    name: 'Mastercard'
  },
  {
    id: '1',
    name: 'Visa'
  }
];
