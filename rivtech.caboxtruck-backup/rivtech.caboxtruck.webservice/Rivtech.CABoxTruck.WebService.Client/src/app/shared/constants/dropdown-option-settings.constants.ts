import { IDropdownSettings } from 'ng-multiselect-dropdown';

export const DropdownOptionSettingsConstants: IDropdownSettings = {
  singleSelection: false,
  idField: 'id',
  textField: 'description',
  selectAllText: 'Select All',
  unSelectAllText: 'Unselect All',
  itemsShowLimit: 3,
  allowSearchFilter: true
};
