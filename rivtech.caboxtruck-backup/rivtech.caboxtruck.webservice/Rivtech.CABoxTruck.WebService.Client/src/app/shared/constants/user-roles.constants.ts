export const UserRole = {
    Agent: '7',
    Underwriter: '8',
    SpecialistUA: '9',
    SuperUser: '10',
    Admin: '12'
};
