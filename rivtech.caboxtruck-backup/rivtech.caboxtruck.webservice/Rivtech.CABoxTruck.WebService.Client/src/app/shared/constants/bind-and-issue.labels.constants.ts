export const BindAndIssueLabelConstants = {
    areYouSureYouWantToIssue: 'Are you sure you want to Issue?',
    issuedSuccessfully: 'Issued Successfully',
    cancelledSuccessfully: 'Canceled Successfully',
    reinstatedSuccessfully: 'Reinstated Successfully',
    rewriteSuccessfully: 'Policy rewrite has been completed',
    areYouSureYouWantToReset: 'By confirming, all the changes will be discarded. Are you sure you want to reset?',
    resetSuccessfully: 'Done resetting Policy Record. Page will reload after clicking "OK".',
    areYouSureYouWantToCancel: 'Are you sure you want to Cancel this Policy?',
    areYouSureYouWantToReinstate: 'Are you sure you want to Reinstate this Policy?',
    areYouSureYouWantToRewrite: 'Are you sure you want to Rewrite this Policy?',
};

export const PaymentPlanListConstants = {
  fullPayCode: 'PPO0',
  twoPayCode: 'PPO1',
  fourPayCode: 'PPO2',
  eightPayCode: 'PPO3',
  mortgageePayCode: 'PPO4',
  sixtyPercent: 60,
  fourtyPercent: 40,
  thirtyPercent: 30
};
