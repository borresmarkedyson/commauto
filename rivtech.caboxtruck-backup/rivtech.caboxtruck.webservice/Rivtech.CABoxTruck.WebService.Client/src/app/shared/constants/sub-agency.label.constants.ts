export const SubAgencyLabelConstants = {
    companyName: 'Company Name',
    workPhone: 'Work Phone',
    mobilePhone: 'Mobile Phone',
    workFax: 'Work Fax',
    emailAddress: 'Email Address',
    businessType: 'Business Type',
    dba: 'Doing Business As',
    yearEstablished: 'Year Established',
    federalIDNumber: 'Federal ID Number',
    naicsCodeID: 'NAICS Code ID',
    additionalNAICSCodeID: 'Additional NAICS Code ID',
    iccmcDocketNumber: 'ICCMC Docket Number',
    usDotNumber: 'US Dot Number',
    pucNumber: 'PUC Number',
    hasSubsidiaries: 'Has Subsidiaries?',
    isActive: 'Is Active?',
    underwriter: 'Underwriter'
  };