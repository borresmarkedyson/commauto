export const VehicleTextConstants = {
    sectionHeader: 'Vehicle Information',
    tableHeaders: {
        vin: 'VIN',
        year: 'Year',
        make: 'Make',
        model: 'Model',
        vehicleType: 'Vehicle Type',
        vehicleDescription: 'Vehicle Description',
        registeredState: 'State',
        autoLiability: 'Liab',
        isCoverageFireTheft: 'Comp / FT',
        isCoverageCollision: 'Coll',
        hasCoverageCargo: 'Cargo',
        hasCoverageRefrigeration: 'Reefer',
        statedAmount: 'Stated Amount',
        coverageFireTheftDeductible: 'Comp / FT Ded',
        coverageCollisionDeductible: 'Coll Ded',
        autoLiabilityPremium: 'AL Prem',
        cargoPremium: 'Cargo Prem',
        physicalDamagePremium: 'Physdam Prem',
        totalPremium: 'Total Prem'
    },
    tableHeadersPolicy: {
        vin: 'VIN',
        effectiveDate: 'Effective Date',
        expirationDate: 'Expiration Date',
        registeredState: 'State',
        autoLiability: 'Liab',
        isCoverageFireTheft: 'Comp / FT',
        isCoverageCollision: 'Coll',
        hasCoverageCargo: 'Cargo',
        hasCoverageRefrigeration: 'Reefer',
        statedAmount: 'Stated Amount',
        coverageFireTheftDeductible: 'Comp / FT Ded',
        coverageCollisionDeductible: 'Coll Ded',
        autoLiabilityPremium: 'AL Prem',
        cargoPremium: 'Cargo Prem',
        physicalDamagePremium: 'Physdam Prem',
        totalPremium: 'Total Prem'
    },
    labels: {
        option: 'Option'
    },
    messages: {
        confirmRemove: 'Are you sure you want to remove ?',
        successRemove: 'Record has been successfully removed.',
        errorRemove: 'There was an error trying to remove a record.',
        successAdd: 'Record has been added.',
        errorAdd: 'There was an error trying to add a record.',
        successUpdate: 'Record has been updated.',
        errorUpdate: 'There was an error trying to update a record.',
        vinDuplicate: 'There is an active vehicle with the same VIN',
        vinInvalid: 'VIN can only be numbers or 17 alphanumeric characters without I, O, Q',
        errorReinstate: 'There was an error reinstating vehicle.'
    }
}