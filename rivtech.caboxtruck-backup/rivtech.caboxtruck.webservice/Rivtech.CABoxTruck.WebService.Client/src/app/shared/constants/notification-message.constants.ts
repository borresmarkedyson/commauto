export const NotificationMessageConstant = {
    fileNotYetReadyErrorMessage: 'The document you are viewing is not yet available.<br>Generation is still in progress. Please try again later.',
    fileNotAllowedToEditDeleteErrorMessage: 'You are not allowed to edit or delete the document.',
  };
