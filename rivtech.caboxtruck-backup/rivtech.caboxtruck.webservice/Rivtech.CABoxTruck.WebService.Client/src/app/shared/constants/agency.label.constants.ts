export const AgencyLabelConstants = {
    companyName: 'Company Name',
    retailerCompanyName: 'Retail Company Name',
    workPhone: 'Work Phone',
    mobilePhone: 'Mobile Phone',
    workFax: 'Work Fax',
    emailAddress: 'Email',
    businessType: 'Business Type',
    dba: 'Doing Business As',
    yearEstablished: 'Year Established',
    federalIDNumber: 'Federal ID Number',
    naicsCodeID: 'NAICS Code ID',
    additionalNAICSCodeID: 'Additional NAICS Code ID',
    iccmcDocketNumber: 'ICCMC Docket Number',
    usDotNumber: 'US Dot Number',
    pucNumber: 'PUC Number',
    hasSubsidiaries: 'Has Subsidiaries?',
    isActive: 'Is Active?',
    contactName: 'Contact Name',
    eftAvailable: 'EFT Available',
    accountNumber: 'Account Number',
    accountType: 'Account Type',
    routingNumber: 'Routing Number',
    commissionEmail: 'Commission Email',
    commissionPaidToAgency: 'Commission Paid To Agency',
    hasBindingAuthority: 'Has Binding Authority',
    is1099Required: '1099 Required',
    regionalSalesManager: 'Regional Sales Manager',
    agencyManagementSystem: 'Agency Management System',
    network: 'Network',
    commissionType: 'Commission Type',
    commissionGroup: 'Commission Group',
    agencyCode: 'Agency Code',
    subAgencyCode: 'Sub Agency Code',
    lexisNexisCode: 'Lexis Nexis Code',
    effectiveDate: 'Effective Date',
    expirationDate: 'Expiration Date',

    newBusiness: 'New Business',
    renewalBusiness: 'Renewal Business',
    licenseEffectiveDate: 'License Effective Date',
    licenseExpirationDate: 'License Expiration Date',

    detailsTab: 'Details',
    addressTab: 'Address',
    subAgencyTab: 'Sub Agency',
    agentTab: 'Agent',
    programStateTab: 'Program-State',

    agencyModalAddedSuccessfulyMessage: 'Agency Added Successfully',
    retailerModalAddedSuccessfulyMessage: 'Retailer Added Successfully',
    subAgencyModalAddedSuccessfullyMessage: 'Sub Agency Added Successfully',
    agencyModalUpdatedSuccessfulyMessage: 'Agency Updated Successfully',
    retailerModalUpdatedSuccessfulyMessage: 'Retailer Updated Successfully',
    subAgencyModalUpdatedSuccessfullyMessage: 'Sub Agency Updated Successfully',
    addSubAgencyMessageModal: 'Add Sub Agency',
    editSubAgencyMessageModal: 'Edit Sub Agency',
    deleteConfirmationMessageModal: 'Are you sure you want delete?',
    agencyDeleteMessage: 'Agency Deleted Successfully',

    auditLog: {
      add: 'CREATE',
      edit: 'UPDATE',
      delete: 'DELETE'
    },

    methodName: {
      add: 'saveAgency',
      edit: 'updateAgency',
      delete: 'deleteAgency'
    }
  };