export const LvBusinessType = [
    {label: 'Individual', value: '1'},
    {label: 'Sole Proprietorship', value: '2'},
    {label: 'Partnership', value: '3'},
    {label: 'Corporation', value: '4'},
    {label: 'LLC', value: '5'},
    {label: 'S-Corp', value: '6'},
    {label: 'Other', value: '7'},
    {label: 'Government Entity', value: '8'},
    {label: 'Joint Venture', value: '9'},
];

export const LvMainUse = [
  { label: 'Courier', value: '1' },
  { label: 'Commercial', value: '2' },
  { label: 'Service', value: '3' }
];

export const LvAccountCategory = [
  { label: 'Courier', value: '1' },
  { label: 'Other For-Hire Trucking Operations', value: '2'},
  { label: 'Debris Removal', value: '3'},
  { label: 'Dirt, Sand & Gravel', value: '4'},
  { label: 'Furniture & Home Furnishing Stores', value: '5'},
  { label: 'Garbage & Trash', value: '6'},
  { label: 'Household Movers', value: '7'},
  { label: 'Machinery & Heavy Equipment', value: '8'},
  { label: 'Scrap Metal, Scrap Auto & Recycling Services', value: '9'},
];

  export const LvNewVenturePreviousExperience = [
    {label: 'New Venture - No Driving Experience', value: '1'},
    {label: 'New Venture - Owned Another Company', value: '2'},
    {label: 'New Venture - Owner Currently Owns Another Company', value: '3'},
    {label: 'New Venture - Owner Was Driver For Another Company', value: '4'},
  ];