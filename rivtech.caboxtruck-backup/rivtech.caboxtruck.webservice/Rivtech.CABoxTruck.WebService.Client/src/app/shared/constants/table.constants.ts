export const TableConstants = {
    add: 'Add',
    edit: 'Edit',
    delete: 'Delete',
    view: 'View',
    reinstate: 'Reinstate'
};