export const AddressType = {
  Billing: 1,
  Business: 2,
  Garaging: 3,
  Mailing: 4,
  Shipping: 5
};

export const LvAddressTypeList = [
  { id: 1, value: 'Billing' },
  { id: 2, value: 'Business' },
  { id: 3, value: 'Garaging' },
  { id: 4, value: 'Mailing' },
];
