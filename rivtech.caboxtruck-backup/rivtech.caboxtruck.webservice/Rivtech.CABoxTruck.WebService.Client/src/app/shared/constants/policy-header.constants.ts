export const PolicySummaryHeaderConstants = {
  policyStatusActive: 'Active',
  policyStatusPending: 'Pending',
  policyStatusCancelled: 'Canceled',
  policyStatusExpired: 'Expired',

  policySubStatusActive: 'Active',
  policySubStatusPendingEndorsement: 'Pending Endorsement',
  policySubStatusPendingCancellation: 'Pending Cancellation',
  policySubStatusPendingRenewal: 'Pending Renewal',
  policySubStatusPendingNonRenewal: 'Pending Non Renewal',
  policySubStatusDraft: 'Draft',
  policySubStatusRewrite: 'Rewrite',
};
