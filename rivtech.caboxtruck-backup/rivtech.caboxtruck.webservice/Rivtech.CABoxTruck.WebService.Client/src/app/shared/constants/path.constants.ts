export const PathConstants = {
  Home: {
    Index: 'home'
  },
  Login: {
    Index: 'login'
  },
  Auth: {
    Index: 'auth'
  },
  Account: {
    Index: 'account',
    ForgotPassword: 'forgot-password',
    ResetPassword: 'reset-password',
    ChangePassword: 'change-password',
    NewUserChangePassword: 'change-password-new-user',
    AccountRoutes: {
    }
  },
  Dashboard: {
    Index: 'dashboard',
  },
  Submission: {
    Index: 'submissions',
    List: 'list',
    Broker: {
      Index: 'broker',
      GeneralInfo: 'general-info'
    },
    Applicant: {
      Index: 'applicant',
      NameAndAddress: 'name-and-address',
      BusinessDetails: 'business-details',
      AdditionalInterests: 'additional-interest',
      Companies: 'companies',
      FilingsInformation: 'filings-information',
      AdditionalInterest: 'additional-interest',
      Manuscripts: 'manuscripts'
    },
    Limits: {
      Index: 'limits-page',
      LimitsPage: 'limits-page',
    },
    Coverages: {
      Index: 'historical-coverage',
      HistoricalCoverage: 'historical-coverage'
    },
    Claims: {
      Index: 'claims-history',
      ClaimsHistory: 'claims-history'
    },
    Filing: {
      Index: 'filing',
      GeneralInfo: 'general-info'
    },
    Quote: {
      Index: 'quote-options',
    },

    RiskSpecifics: {
      Index: 'risk-specifics',
      DestinationInfo: 'destination-info',
      GeneralInfo: 'general-info',
      DriverInfo: 'driver-info',
      DotInfo: 'dot-info',
      DrivingHiringCriteria: 'driving-hiring-criteria',
      DriverTraining: 'driver-training',
      MaintenanceSafety: 'maintenance-safety',
      GeneralLiabilityCargo: 'general-liability-cargo',
      GeneralLiabilityCargoName: 'General Liability & Cargo',
      UWQuestions: 'uw-questions',
      RiskSpecificsQuestions: 'risk-specifics-questions'
    },
    Vehicle: {
      Index: 'vehicle',
      // VehicleInfo: "vehicle-info",
    },
    Driver: {
      Index: 'driver',
      // DriverInfo: "driver-info",
    },
    Underwriting: {
      Index: 'underwriting',
      GeneralInfo: 'general-info'
    },
    Bind: {
      Index: 'bind',
    },
    Notes: {
      Index: 'note-page',
      NotesPage: 'note-page',
    },
    FormsSelection: {
      Index: 'forms-selection',
      FormsPage: 'forms-selection',
    },
    UnderwritingRating: 'underwriting-rating',
    Documents: 'documents',
    Tasks: 'tasks',
    SubmissionRoutes: {
      SubmissionRoute: 'submission/index'
    }
  },
  Management: {
    Index: 'management',
    Users: {
        Index: 'users',
        Agent: 'agent-list' ,
        User: 'system-user',
        UserType: 'user-type',
        UserGroup: 'user-group'
    },
    Agency: {
        Index: 'agency',
        New: 'new',
        Edit: 'edit',
    },
    Retailer: {
      Index: 'retailer',
      New: 'new',
      Edit: 'edit',
    },
    Roles: {
        Index: 'roles',
    }
  },

  Policy: {
    Index: 'policies',
    List: 'list',
    // Info: 'info',
    // History: 'history',
    // Transactions: 'transactions',
    // FinancialSummary: 'financial-summary',
    // Endorsements: {
    //   Index: 'endorsements',
    // },
    History: 'history',
    Billing: 'billing',
    Summary: 'summary',
    Driver: 'driver',
    AdditionalInterest: 'additional-interest',
    Manuscript: 'manuscript',
    Tasks: 'tasks',
    Documents: 'documents',
    Notes: 'notes',
    Issuance: 'issuance'
  },
  PaymentManagement: {
    Index: 'payment-portal',
    ClientPortal: 'client-portal',
    DepositPayment: 'deposit-payment'
  }
};
