export const PolicyBillingConstants = {
  paymentMethod: {
    adjustment: 'Adjustment',
    writeOff: 'WriteOff',
  },
};