// eslint-disable-next-line no-shadow
export enum PaymentMethod {
  CashCheck = 'PM0',
  CreditCard = 'PM1',
  ECheck = 'PM2',
  RecurringCreditCard = 'PM3',
  RecurringECheck = 'PM4',
  EFT = 'PM5',
}