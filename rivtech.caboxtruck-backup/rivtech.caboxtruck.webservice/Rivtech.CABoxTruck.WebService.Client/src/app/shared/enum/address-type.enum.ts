export enum AddressType {
  Billing = 'BL',
  Business = 'BS',
  Garaging = 'GA',
  Mailing = 'MA',
  Property = 'PR',
  Shipping = 'SH'
}