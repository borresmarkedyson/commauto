export enum ReversalType {
  Void = 'V',
  NSF = 'N',
  StopPay = 'S'
}