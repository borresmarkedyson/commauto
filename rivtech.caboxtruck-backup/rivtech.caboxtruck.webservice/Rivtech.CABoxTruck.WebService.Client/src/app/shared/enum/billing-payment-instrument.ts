export enum BillingPaymentInstrument {
  Cash = 'C',
  CreditCard = 'CC',
  EFT = 'EFT',
  Check = 'CHQ',
  Adjustment = 'ADJ',
  WriteOff = 'WO',
}