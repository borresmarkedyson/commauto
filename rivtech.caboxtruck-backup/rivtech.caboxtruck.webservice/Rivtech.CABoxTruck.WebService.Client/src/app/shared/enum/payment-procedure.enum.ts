export enum PaymentProcedure {
  PostBasic = 'PostBasic',
  PayBalance = 'PayBalance',
  PayToReinstate = 'PayToReinstate'
}