export enum CreditCardType {
    Amex = 'CCT0',
    Discover = 'CCT1',
    Visa = 'CCT2',
    Mastercard = 'CCT3'
  }