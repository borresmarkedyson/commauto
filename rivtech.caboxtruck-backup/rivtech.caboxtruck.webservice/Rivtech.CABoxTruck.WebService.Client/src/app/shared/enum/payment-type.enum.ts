export enum PaymentType {
  Payment = 'Payment',
  Refund = 'Refund',
  WriteOff = 'Write-Off',
}