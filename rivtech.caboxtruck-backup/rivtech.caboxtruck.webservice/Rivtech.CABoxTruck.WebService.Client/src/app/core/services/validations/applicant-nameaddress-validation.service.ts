import { Injectable } from '@angular/core';
import FormUtils from '../../../shared/utilities/form.utils';
import { ApplicantData } from '../../../modules/submission/data/applicant/applicant.data';

@Injectable()
export class ApplicantNameaddressValidationService {
  nameAndAddressSection: boolean;
  nameAndAddressSectionBinding: boolean;

  get NameAndAddressForm() {
    return this.applicantData.applicantForms.nameAndAddressForm;
  }
  constructor(public applicantData: ApplicantData) { }

  checkNameAndAddressPage() {
    this.nameAndAddressSection = this.checkNameAndAddressSection();
  }

  checkNameAndAddressPageBinding() {
    this.nameAndAddressSectionBinding = this.checkNameAndAddressSectionBinding();
  }

  checkNameAndAddressSection() {
    this.NameAndAddressForm.markAllAsTouched();
    // return FormUtils.validateForm(this.NameAndAddressForm);
    if (FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessNameInput']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessAddressInput']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessCityDropdown']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessStateInput']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessZipCodeInput']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['cityOperations']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['stateOperations']) &&
    FormUtils.validateFormControl(this.NameAndAddressForm.controls['zipCodeOperations'])) {
      return true;
    }

    return false;
  }

  checkNameAndAddressSectionBinding() {
    this.NameAndAddressForm.markAllAsTouched();
    if (FormUtils.validateFormControl(this.NameAndAddressForm.controls['businessPrincipalInput']) &&
      FormUtils.validateFormControl(this.NameAndAddressForm.controls['emailInput']) &&
      FormUtils.validateFormControl(this.NameAndAddressForm.controls['phoneExtInput'])) {
      return true;
    }

    return false;
  }
}

