import { Injectable } from '@angular/core';
import { SubmissionData } from '../../../modules/submission/data/submission.data';

@Injectable()
export class VehicleValidationService {
  vehicleSection: boolean;

  constructor(public submissionData: SubmissionData) { }

  checkVehiclePage() {
    this.vehicleSection = this.checkVehicleSection();
  }

  checkVehicleSection() {
    // put validation  requirements to display badge.
    //if (this.submissionData.riskDetail?.vehicles?.length > 0) {
    if (this.submissionData.riskDetail?.vehicles?.filter(x => x.deletedDate == null).length > 0 && !this.submissionData.riskDetail?.vehicles?.find(x => !x.isValidated)) {
      const vehicles = this.submissionData.riskDetail?.vehicles.filter(x => x.options !== null && x.options !== undefined);
      if (vehicles.length === 0) {
        console.log('No selected options for vehicles');
        return false;
      }

      const bindOption = this.submissionData.riskDetail?.binding?.bindOptionId;
      if (bindOption !== null && bindOption !== undefined) {
        const vehiclesBind = this.submissionData.riskDetail?.vehicles.filter(x => x.options !== null && x.options.indexOf(bindOption) !== -1);
        if (vehiclesBind.length === 0) {
          console.log('No vehicle selected for bind option', bindOption);
          return false;
        }
      }

      return true;
    }
    return false;
  }
}
