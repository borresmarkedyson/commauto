import { Injectable } from '@angular/core';
import { RadiusOfOperationsData } from '../../../modules/submission/data/riskspecifics/radius-of-operations.data';
import FormUtils from '../../../shared/utilities/form.utils';

@Injectable()
export class DestinationInfoValidationService {
  radiusSection: boolean;

  get RadiusOfOperationsForm() {
    return this.radiusOfOperationData.radiusOfOperationsForm;
  }
  constructor(public radiusOfOperationData: RadiusOfOperationsData) { }

  checkRadiusOfOperationPage() {
    this.radiusSection = this.checkRadiusOfOperationSection();
  }

  checkRadiusOfOperationSection() {
    this.RadiusOfOperationsForm.markAllAsTouched();
    return FormUtils.validateForm(this.RadiusOfOperationsForm);
  }
}
