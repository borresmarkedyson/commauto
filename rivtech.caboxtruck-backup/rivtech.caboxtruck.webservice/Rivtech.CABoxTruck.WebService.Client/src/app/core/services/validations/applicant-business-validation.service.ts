import { Injectable } from '@angular/core';
import FormUtils from '../../../shared/utilities/form.utils';
import { ApplicantData } from '../../../modules/submission/data/applicant/applicant.data';

@Injectable()
export class ApplicantBusinessValidationService {
  businessDetailsSection: boolean;
  businessDetailsSectionBinding: boolean;

  get BusinessDetailsForm() {
    return this.applicantData.applicantForms.businessDetails;
  }
  constructor(public applicantData: ApplicantData) { }

  checkBusinessDetailsPage() {
    this.businessDetailsSection = this.checkBusinessDetailsSection();
  }

  checkBusinessDetailsPageBinding() {
    this.businessDetailsSectionBinding = this.checkBusinessDetailsSectionBinding();
  }

  checkBusinessDetailsSection() {
    this.BusinessDetailsForm.markAllAsTouched();
    // return FormUtils.validateForm(this.BusinessDetailsForm);
    var isValid = FormUtils.validateFormControl(this.BusinessDetailsForm.controls['yearBusinessInput']) 
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['newVenturePreviousExperienceDropdown']) 
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['yearUnderCurrentInput']) 
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['businessTypeDropdown']) 
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['mainUseDropdown'])
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['accountCategoryDropdown']) 
      && FormUtils.validateFormControl(this.BusinessDetailsForm.controls['accountCategoryUWDropdown']);

    return isValid;
  }

  checkBusinessDetailsSectionBinding() {
    this.BusinessDetailsForm.markAllAsTouched();
    if (FormUtils.validateFormControl(this.BusinessDetailsForm.controls['federalIdInput'])) {
      return true;
    }

    return false;
  }
}
