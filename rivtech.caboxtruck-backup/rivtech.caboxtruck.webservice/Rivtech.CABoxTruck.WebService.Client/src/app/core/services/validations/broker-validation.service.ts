import { Injectable } from '@angular/core';
import { PolicyContactsData } from '@app/modules/submission/data/broker/policy-contacts.data';
import { SubmissionData } from '@app/modules/submission/data/submission.data';
import { BrokerGenInfoData } from '../../../modules/submission/data/broker/general-information.data';
import FormUtils from '../../../shared/utilities/form.utils';

@Injectable()
export class BrokerValidationService {
  brokerSection: boolean;
  brokerRequiredToBind: boolean;

  get BusinessDetailsForm() {
    return this.brokerData.formInfo;
  }
  constructor(public brokerData: BrokerGenInfoData,
    public policyContactsData: PolicyContactsData,
    public submissionData: SubmissionData) { }

  checkBrokerPage() {
    this.brokerSection = this.checkBrokerSection();
    this.brokerRequiredToBind = this.checkBrokerPageBinding();
  }

  checkBrokerSection() {
    // this.BusinessDetailsForm.controls['agencyId'].updateValueAndValidity();
    // this.BusinessDetailsForm.controls['agentId'].updateValueAndValidity();
    this.BusinessDetailsForm.markAllAsTouched();
    return FormUtils.validateFormControl(this.BusinessDetailsForm.controls['effectiveDate']);
  }

  checkBrokerPageBinding() {
    this.BusinessDetailsForm.controls['agencyId'].updateValueAndValidity();
    this.BusinessDetailsForm.controls['agentId'].updateValueAndValidity();
    this.BusinessDetailsForm.markAllAsTouched();
    return FormUtils.validateForm(this.BusinessDetailsForm) && this.checkPolicyContactsSection();
  }

  checkPolicyContactsSection() {
    if (this.submissionData.isQuoteIssued || this.submissionData.isIssued) {
      return this.submissionData.riskDetail?.riskPolicyContacts?.filter(x => x.typeId == 1)?.length > 0;
    }

    return true;
  }
}
