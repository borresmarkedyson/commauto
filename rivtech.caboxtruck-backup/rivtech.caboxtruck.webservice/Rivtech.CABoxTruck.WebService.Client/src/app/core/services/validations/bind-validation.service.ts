import { Injectable } from '@angular/core';
import { BindingData } from '../../../modules/submission/data/binding/binding.data';
import FormUtils from '../../../shared/utilities/form.utils';

@Injectable()
export class BindValidationService {
  bindSection: boolean;

  get BindForm() {
    return this.bindData.formBinding;
  }
  constructor(public bindData: BindingData) { }

  checkBindPage() {
    this.bindSection = this.checkBindSection();
  }

  checkBindSection() {
    this.BindForm.markAllAsTouched();
    return FormUtils.validateForm(this.BindForm);
  }
}
