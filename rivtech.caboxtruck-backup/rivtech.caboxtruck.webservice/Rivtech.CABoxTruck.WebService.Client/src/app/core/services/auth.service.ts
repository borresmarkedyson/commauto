import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginLabelsConstants } from '@app/shared/constants/login.labels.constants';
import { PathConstants } from '@app/shared/constants/path.constants';
import Utils from '@app/shared/utilities/utils';
import { BehaviorSubject, Observable, of, throwError as observableThrowError } from 'rxjs';
import { catchError, debounceTime, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { TokenResponse } from '../../shared/models/tokenResponse';
import { UserAccessRight } from '../../shared/models/userAccessRight';


@Injectable()
export class AuthService {

  isLogout: boolean;
  userType = new BehaviorSubject<'external' | 'internal'>('external');
  isPasswordChanged: boolean = false;

  public currentUserRole: string;

  LoginDateFormGroup = new FormGroup({
    loginDate: new FormControl(undefined)
  });

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute, private httpbackend: HttpBackend) {
    this.isLogout = true;
  }

  login(username: string, password: string): Observable<boolean> {
    let url = `/auth`;

    let data = {
      username: username,
      password: password,
    };

    return this.getAuthFromServer(url, data);
  }


  logout(): boolean {

    this.setAuth(null);

    return true;
  }


  setAuth(auth: TokenResponse | null): boolean {
    if (auth) {
      localStorage.setItem(environment.AuthKey, JSON.stringify(auth));

      const userInfo: string = atob(JSON.stringify(auth).split('.')[1]);

      localStorage.setItem(environment.UsernameKey, JSON.parse(userInfo).unique_name);
      localStorage.setItem(environment.UserIdentifierKey, JSON.parse(userInfo).sub);
      localStorage.setItem(environment.CurrentUserRole, JSON.parse(userInfo).role_id);
    } else {
      localStorage.removeItem(environment.AuthKey);
    }

    return true;
  }

  getAuth(): TokenResponse | null {
    const item = localStorage.getItem(environment.AuthKey);

    if (item) {
      return JSON.parse(item);
    }

    return null;
  }

  getUserName(): string {
    const userName = localStorage.getItem(environment.UsernameKey);
    return userName ? userName : null;
  }

  getCurrentUserRole(): string {
    const role = localStorage.getItem(environment.CurrentUserRole);
    return role ? role : null;
  }

  getCurrentUserId(): string {
    const userId = localStorage.getItem(environment.UserIdentifierKey);
    return userId ? userId : null;
  }

  isLoggedIn(): boolean {
    let authKey = localStorage.getItem(environment.AuthKey);

    return authKey != null && authKey != '';
  }


  getAuthFromServer(url: string, data: any): Observable<boolean> {
    return this.http.post<any>(`${environment.IdentityServiceUrl}${url}`, data).pipe(
      map((res) => {

        const token = res && res.token;
        if (token) {
          this.setAuth(res);
          const decodedJWT = JSON.parse(window.atob(token.split('.')[1]));
          this.isPasswordChanged = decodedJWT.password_changed;

          if (this.isPasswordChanged === true) {
              // this.saveUserInformation(token);
              return true;
          } else {
              this.router.navigate([`${PathConstants.Account.Index}/${PathConstants.Account.NewUserChangePassword}`]);
              Utils.unblockUI();
          }
      } else {
          return false;
      }
      }),
      catchError(error => {
        const exist = error.error.error.toLowerCase().includes(LoginLabelsConstants.errorMessage.exist);
        const incorrect = error.error.error.toLowerCase().includes(LoginLabelsConstants.errorMessage.incorrect);
        const inactive = error.error.error.toLowerCase().includes(LoginLabelsConstants.errorMessage.userInactive);
        if (exist || incorrect || inactive) {
            const msg = exist ? LoginLabelsConstants.errorMessage.exist :
                incorrect ? LoginLabelsConstants.errorMessage.incorrect : LoginLabelsConstants.errorMessage.userInactive;
            // this.loginAuditLog(msg, data.username);
        }
        return observableThrowError(error);
      }));
  }

  validateAuthFromServer(password: any): Observable<boolean> {
    const url = `/auth`;

    const data = {
      username: this.getUserName(),
      password: password,
    };

    this.http = new HttpClient(this.httpbackend);
    return this.http.post<any>(`${environment.IdentityServiceUrl}${url}`, data).pipe(
      map((res) => {
        const token = res && res.token;
        if (token) {
          return true;
        }
        return false;
      }),
      catchError(error => {
        return observableThrowError(error);
      }));
  }

  getUserAccessRights(): Observable<UserAccessRight[]> {

    return this.http.get<any>(`${environment.IdentityServiceUrl}/Auth/access-rights`).pipe(
      map((res) => {
        // console.log('access rights: ' + JSON.stringify(res));
        return res;
      }),
      catchError(error => {
        console.log(error);
        return observableThrowError(error);
      }));
  }

  getUserDetails(): Observable<any> {
    return this.http.get<any>(`${environment.IdentityServiceUrl}/api/Users/${this.getUserName()}/user-details`)
      .catch(error => observableThrowError(error));
  }

  public getCustomDate(isForUTCConversion: boolean = false): Date {
    const localStorageDate = localStorage.getItem('loginDate');
    if (localStorageDate !== 'undefined' && localStorageDate !== null && localStorageDate !== 'null') {
      this.LoginDateFormGroup.get('loginDate').setValue(this.reformatToDateTime(JSON.parse(localStorageDate), true));
      return this.LoginDateFormGroup.get('loginDate').value;
    } else {
      const date = new Date();
      const utcDate = new Date(date.toUTCString());
      utcDate.setMilliseconds(date.getMilliseconds());
      return !isForUTCConversion ? new Date() : utcDate;
    }

  }

  public reformatToDateTime(dateValue?: any, isFromCustomDatePicker?: boolean): any {
    const currentDate = new Date();
    switch (isFromCustomDatePicker) {
      case true:
        const date = dateValue?.singleDate?.date;
        return new Date(`${date.month}/${date.day}/${date.year} ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}.${currentDate.getMilliseconds()}`);
      case false:
        return new Date(`${dateValue.month}/${dateValue.day}/${dateValue.year} ${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}.${currentDate.getMilliseconds()}`);
    }
  }

  logUserLogin(username: string): any {
    return this.http.post(`${environment.ApiUrl}/User/login`, { username }, { responseType: 'text' }).catch(err => err);
  }

  logUserLogout(username: string): any {
    return this.http.post(`${environment.ApiUrl}/User/logout`, { username }, { responseType: 'text' }).catch(err => err);
  }

  getUserId(): number {
    const id = Number(localStorage.getItem(environment.UserIdentifierKey));
    return id ? id : 0;
  }

  savePasswordAuditLog(action: string, username: string): void {
    /*  const payload = {
       userId: this.getUserId(),
       keyId: username,
       auditType: '',
       description: '',
       method: ''
     };

     switch (action) {
         case PasswordFormConstants.auditLog.action.requestForgot:
             payload.auditType = PasswordFormConstants.auditLog.forgotPwRequestLog.auditType;
             payload.description = PasswordFormConstants.auditLog.forgotPwRequestLog.description.replace('{0}', username);
             payload.method = PasswordFormConstants.auditLog.methodName.requestForgot;
             break;
         case PasswordFormConstants.auditLog.action.successForgot:
             payload.auditType = PasswordFormConstants.auditLog.forgotPwSuccessLog.auditType;
             payload.description = PasswordFormConstants.auditLog.forgotPwSuccessLog.description.replace('{0}', username);
             payload.method = PasswordFormConstants.auditLog.methodName.successForgot;
             break;
         case PasswordFormConstants.auditLog.action.successChange:
             payload.auditType = PasswordFormConstants.auditLog.changePwSuccessLog.auditType;
             payload.description = PasswordFormConstants.auditLog.changePwSuccessLog.description.replace('{0}', username);
             payload.method = PasswordFormConstants.auditLog.methodName.change;
             break;
         case PasswordFormConstants.auditLog.action.successReset:
             payload.auditType = PasswordFormConstants.auditLog.resetPwSuccessLog.auditType;
             payload.description = PasswordFormConstants.auditLog.resetPwSuccessLog.description.replace('{0}', username);
             payload.method = PasswordFormConstants.auditLog.methodName.reset;
             break;
     }

     this.auditLogService.insertToAuditLog(payload).subscribe(); */
  }

  saveUserInfo(body: any | undefined): Observable<any> {
    /* const url_ = `${environment.ApiUrl}/User`;  //NOT IMPLEMENT YET IN BoxTruck API
    return this.http.post(url_, body).pipe(
        map((res) => {
            return res;
        }),
        catchError(error => {
            return observableThrowError(error);
        })); */
    return of({});
  }
}
