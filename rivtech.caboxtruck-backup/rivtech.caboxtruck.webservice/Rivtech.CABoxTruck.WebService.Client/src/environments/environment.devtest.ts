import { commonEnvironment } from './environment.common';

export const _settings = {
  envName: 'devtest',
  production: true,
  ApiUrl: `https://app-boxtruck-api-dev.azurewebsites.net/api`,
  EnvironmentServiceUrl: `https://rivtechenvironmentalapi-test.azurewebsites.net`,
  IdentityServiceUrl: `https://app-useridentity-test.azurewebsites.net`,
  GenericServiceUrl: `https://app-genericservices-test.azurewebsites.net`,
  ClientManagementUrl: `https://app-agencyservices-dev.azurewebsites.net`,
  LoginUrl: `https://app-clientmanagement-ui-test.azurewebsites.net`,
  RaterApiUrl: `https://boxtruckraterapitest.westus2.cloudapp.azure.com:3010/api`, //this can be change if we apply ssl enabled
  BillingServiceUrl: `https://app-boxtruckbilling-api-dev.azurewebsites.net`,
  BillingAppId: 'CABT',
  ApplicationId: 2,
  BoxtruckAzureFunction: {
    BaseUrl: "https://function-boxtruck-dev.azurewebsites.net",
    PaymentConfirmationCode: "code=RTimLexpmDoiXHjOGAGWjGZqlsirQbO6uNxRO8GbyxOpKe39Cwwy0A=="
  }
};

export const environment = Object.assign({}, commonEnvironment, _settings);
