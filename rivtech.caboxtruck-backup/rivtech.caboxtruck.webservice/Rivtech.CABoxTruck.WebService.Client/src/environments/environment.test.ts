import { commonEnvironment } from './environment.common';

export const _settings = {
  envName: 'test',
  production: true,
  ApiUrl: `https://app-boxtruck-api-test.azurewebsites.net/api`,
  EnvironmentServiceUrl: `https://rivtechenvironmentalapi-test.azurewebsites.net`,
  IdentityServiceUrl: `https://app-useridentity-test.azurewebsites.net`,
  GenericServiceUrl: `https://app-genericservices-test.azurewebsites.net`,
  ClientManagementUrl: `https://app-agencyservices-test.azurewebsites.net`,
  LoginUrl: `https://app-clientmanagement-ui-test.azurewebsites.net`,
  RaterApiUrl: `https://boxtruckraterapitest.westus2.cloudapp.azure.com:3010/api`, //this can be change if we apply ssl enabled
  BillingServiceUrl: `https://app-boxtruckbilling-api-test.azurewebsites.net`,
  BillingAppId: 'CABT',
  ApplicationId: 2,
  BoxtruckAzureFunction: {
    BaseUrl: "https://function-boxtruck-test.azurewebsites.net",
    PaymentConfirmationCode: "code=DOSx18B0TIxELLmAFetREkjJbyFn17eXhVbqRs7Ujv9zx3MzybzDCA=="
  }
};

export const environment = Object.assign({}, commonEnvironment, _settings);
