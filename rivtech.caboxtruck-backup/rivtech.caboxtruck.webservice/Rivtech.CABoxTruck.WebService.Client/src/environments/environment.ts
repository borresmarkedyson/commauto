import { commonEnvironment } from "./environment.common";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const _settings = {
  envName: 'local',
  production: false,
  ApiUrl: `http://${window.location.hostname}:62635/api`,
  EnvironmentServiceUrl: `https://rivtechenvironmentalapi-test.azurewebsites.net`,
  IdentityServiceUrl: `https://app-useridentity-test.azurewebsites.net`,
  GenericServiceUrl: `https://app-genericservices-test.azurewebsites.net`,
  ClientManagementUrl: `https://app-agencyservices-dev.azurewebsites.net`,
  RaterApiUrl: `https://localhost:44334/api`, //this can be change if we apply ssl enabled,
  BillingServiceUrl: `https://localhost:44337`,
  BillingAppId: 'CABT',
  ApplicationId: 2,
  BoxtruckAzureFunction: {
    BaseUrl: "http://localhost:7071",
    PaymentConfirmationCode: ""
  }
};

export const environment = Object.assign({}, commonEnvironment, _settings);
