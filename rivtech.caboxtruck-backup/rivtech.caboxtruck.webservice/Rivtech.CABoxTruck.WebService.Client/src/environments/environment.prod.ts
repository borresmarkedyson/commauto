import { commonEnvironment } from './environment.common';

export const _settings = {
  envName: 'production',
  production: true,
  ApiUrl: `https://app-boxtruck-api-prod.azurewebsites.net/api`,
  IdentityServiceUrl: `https://identity.rivtechglobal.com`,
  GenericServiceUrl: `https://genericapi.rivtechglobal.com`,
  ClientManagementUrl: `https://agencyapi.rivtechglobal.com`,
  LoginUrl: `https://app-clientmanagement-ui-test.azurewebsites.net`,
  RaterApiUrl: `http://vm-boxtruck-raterapi-prod.westus2.cloudapp.azure.com:3000/api`, //this can be change if we apply ssl enabled
  BillingServiceUrl: `https://app-boxtruckbilling-api-prod.azurewebsites.net`,
  BillingAppId: 'CABT',
  ApplicationId: 2,
  BoxtruckAzureFunction: {
    BaseUrl: 'https://func-boxtruck-nightlyjob-prod.azurewebsites.net',
    PaymentConfirmationCode: 'code=CTfw0G/0WliJlD324g6LJ4oWC2BPbDkcNxHhsU4V/LDWs4p8byePrA=='
  }
};

export const environment = Object.assign({}, commonEnvironment, _settings);
