export const commonEnvironment = {
  ApiUrl: `/api`,
  AuthKey: 'auth',
  UsernameKey: 'uname',
  ClientId: 'Environmental',
  GrantType: {
    Password: "password",
    RefreshToken: "refresh_token"
  },
  UserIdentifierKey: 'userIdentifier',
  CurrentUserRole: 'currentUserRole'
};
