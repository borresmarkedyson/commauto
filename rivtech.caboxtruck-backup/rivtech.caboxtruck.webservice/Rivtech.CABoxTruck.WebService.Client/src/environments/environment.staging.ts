import { commonEnvironment } from './environment.common';

export const _settings = {
  envName: 'staging',
  production: true,
  ApiUrl: `https://app-boxtruck-apistaging.azurewebsites.net/api`,
  IdentityServiceUrl: `https://app-useridentity-stg.azurewebsites.net`,
  GenericServiceUrl: `https://app-genericservices-stg.azurewebsites.net`,
  ClientManagementUrl: `https://app-agencyservices-stg.azurewebsites.net`,
  LoginUrl: `https://app-clientmanagement-ui-test.azurewebsites.net`,
  RaterApiUrl: `http://vm-boxtruck-raterapi-staging.westus2.cloudapp.azure.com:3000/api`, //this can be change if we apply ssl enabled
  BillingServiceUrl: `https://app-boxtruckbilling-api-stg.azurewebsites.net`,
  BillingAppId: 'CABT',
  ApplicationId: 2,
  BoxtruckAzureFunction: {
    BaseUrl: "https://function-boxtruck-staging.azurewebsites.net",
    PaymentConfirmationCode: "code=H2VUSnkFDlRPopDp8bfnbwaZB29OR/n5aEPaYkr0giaqClnzAiLOPQ=="
  }
};

export const environment = Object.assign({}, commonEnvironment, _settings);
