﻿using RivTech.CABoxTruck.WebService.DALDapper.Common;
using RivTech.CABoxTruck.WebService.DALDapper.IDataAccess;
using Dapper;
using RivTech.CABoxTruck.WebService.DTO.Report.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using RivTech.CABoxTruck.WebService.Data.DALDapperModels;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;

namespace RivTech.CABoxTruck.WebService.DALDapper.DataAccess
{
    public class ReportDataAccess : IReportDataAccess
    {
        private readonly IDbConnection _dbConnection;

        public ReportDataAccess(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        private SqlBuilder FilterQueryByPolicyHistoryEffectiveDate(SqlBuilder builder, ReportManagementFilter filter)
        {
            if (filter.DateMin.HasValue)
            {
                builder.Where("PolicyHistory.EffectiveDate >= @dateFrom");
            }

            if (filter.DateMax.HasValue)
            {
                builder.Where("PolicyHistory.EffectiveDate <= @dateTo");
            }

            return builder;
        }

        public async Task<IEnumerable<DMVReportModel>> GetDMVData(ReportManagementFilter reportManagementFilter)
        {
            SqlBuilder builder = new SqlBuilder();
            builder = FilterQueryByPolicyHistoryEffectiveDate(builder, reportManagementFilter);

            string query = $@"
				SELECT *
				FROM
				(
					SELECT 
					'16543' NaicNumber,
					'RPS' GACode,
					Vehicle.RegisteredState VehicleState,
					'F' IdType,
					Entity.FederalIDNumber,
					IIF(ISNULL(Entity.CompanyName, '') != '', Entity.CompanyName, Entity.FirstName + ' ' + Entity.LastName) CompanyName,
					Address.StreetAddress1,
					Address.City,
					Address.State,
					Address.ZipCode,
					Risk.PolicyNumber,
					PolicyHistory.PolicyStatus,
					PolicyHistory.EndorsementNumber,
					'2' PolicyType,
					BrokerInfo.EffectiveDate,
					BrokerInfo.ExpirationDate,
					PolicyHistory.EffectiveDate TransactionEffectiveDate,
					Vehicle.VIN,
					Vehicle.Year,
					CASE 
						WHEN 
							PolicyHistory.EndorsementNumber = 0 AND PolicyHistory.PolicyStatus = 'Active'
						THEN 
							'NBS'
						WHEN 
							PolicyHistory.PolicyStatus = 'Canceled'
						THEN 
							'XLC'
						WHEN 
							PolicyHistory.PolicyStatus = 'Reinstated'
						THEN 
							'REI'
						WHEN 
							PolicyHistory.EndorsementNumber != 0 AND PolicyHistory.PolicyStatus = 'Active'
						THEN
							CASE 
								WHEN 
									PolicyHistory.Details LIKE '%Removed VIN ''' + Vehicle.VIN + '''%'
								THEN 
									'DEL'
								WHEN 
									PolicyHistory.Details LIKE '%Added VIN ''' + Vehicle.VIN + '''%'
								THEN 
									'ADD'
								WHEN 
									PolicyHistory.Details LIKE '%Reinstated VIN ''' + Vehicle.VIN + '''%'
								THEN 
									'REI'
								ELSE
									''
							END
						ELSE
							''
					END TransactionCode,
					Vehicle.Make,
					Vehicle.Model,
					Vehicle.EffectiveDate VehicleEffectiveDate,
					Vehicle.ExpirationDate VehicleExpirationDate,
					IIF(cbsl.SingleLimit IS NOT NULL, 'CBSL', '') 'CoverageType1',
					'' 'IndividualLimit1',
					'' 'OccurrenceLimit1',
					IIF(cbsl.SingleLimit IS NOT NULL, cbsl.SingleLimit, 0) 'CSLLimit1',
					IIF(bpip.LimitDisplay != 'NONE', 'BPIP', '') 'CoverageType2',
					IIF(bpip.LimitDisplay != 'NONE', IIF(LEFT(bpip.LimitDisplay, 7) = '$15,000 ', LEFT(bpip.LimitDisplay, 7), LEFT(bpip.LimitDisplay, 8)), '') 'IndividualLimit2',
					'' 'OccurrenceLimit2',
					'' 'CSLLimit2',
					'' 'CoverageType3',
					'' 'IndividualLimit3',
					'' 'OccurrenceLimit3',
					'' 'CSLLimit3'
					FROM PolicyHistory
						INNER JOIN RiskDetailVehicle 
							ON RiskDetailVehicle.RiskDetailId = PolicyHistory.PreviousRiskDetailId
						INNER JOIN Vehicle 
							ON Vehicle.Id = RiskDetailVehicle.VehicleId
						INNER JOIN RiskDetail 
							ON RiskDetail.Id = PolicyHistory.RiskDetailId
						INNER JOIN Insured
							ON Insured.Id = RiskDetail.InsuredId
						INNER JOIN Entity
							ON Entity.Id = Insured.EntityId
						INNER JOIN Risk 
							ON Risk.Id = RiskDetail.RiskId
						INNER JOIN BrokerInfo
							ON BrokerInfo.RiskId = Risk.Id
						INNER JOIN RiskCoverage 
							ON RiskCoverage.RiskDetailId = RiskDetail.Id
						LEFT JOIN EntityAddress 
							ON EntityAddress.EntityId = Entity.Id
							AND EntityAddress.AddressTypeId = 2
						LEFT JOIN Address 
							ON Address.Id = EntityAddress.AddressId
						LEFT JOIN LvLimits cbsl 
							ON cbsl.Id = RiskCoverage.AutoBILimitId
						LEFT JOIN LvLimits bpip
							ON bpip.Id = RiskCoverage.PIPLimitId
					/**where**/
				) DMVData
				WHERE DMVData.TransactionCode != ''
				ORDER BY 
					PolicyStatus, EndorsementNumber, PolicyNumber, VIN
            ";

            SqlBuilder.Template sqlTemplate = new SqlBuilder.Template(builder, query, null);
            string statement = Regex.Replace(sqlTemplate.RawSql, @"\n", " ");

            return await _dbConnection.QueryAsync<DMVReportModel>(statement, new
            {
				dateFrom = reportManagementFilter.DateMin,
				dateTo = reportManagementFilter.DateMax
			});
        }

        public List<RiskDetail> GetAllRiskDetails()
        {
            string statement = $@"
                SELECT TOP 100
                RiskDetail.*,
                Insured.*,
                RiskDetailVehicle.*,
                Vehicle.*
				FROM RiskDetail
				INNER JOIN Insured ON Insured.Id = RiskDetail.InsuredId
				INNER JOIN RiskDetailVehicle ON RiskDetailVehicle.RiskDetailId = RiskDetail.Id
				INNER JOIN Vehicle ON Vehicle.Id = RiskDetailVehicle.VehicleId
			";

            return _dbConnection.Query<RiskDetail, Insured, RiskDetailVehicle, Vehicle, RiskDetail>(statement, (riskDetail, insured, riskDetailVehicle, vehicle) =>
            {

				riskDetail.Insured = insured;
				riskDetail.RiskDetailVehicles = new List<RiskDetailVehicle>();
				riskDetail.RiskDetailVehicles.Add(riskDetailVehicle);
				riskDetail.Vehicles.Add(vehicle);

				return riskDetail;
            }, splitOn: "Id,Id,VehicleId,Id").AsList();
        }
    }
}