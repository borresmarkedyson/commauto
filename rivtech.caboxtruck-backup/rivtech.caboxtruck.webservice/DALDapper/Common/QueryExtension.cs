﻿using System;

namespace RivTech.CABoxTruck.WebService.DALDapper.Common
{
    public static class QueryExtension
    {
        public static string FilterByCompanyId(string columnName)
        {
            return $"{columnName} = @companyId";
        }

        public static string FilterByAssignedEmailAddress(string assignedEmailAddress)
        {
            return $"{assignedEmailAddress} = @assignedEmailAddress";
        }

        public static string FilterByCustomerId(string columnName)
        {
            return $"{columnName} = @customerId";
        }

        public static string Sorting(this string query, string sortBy, string thenSortBy, bool ascending)
        {
            if(ascending)
            {
                if(!string.IsNullOrEmpty(thenSortBy))
                {
                    query += $" order by {sortBy} asc, {thenSortBy} asc";
                }
                else
                {
                    query += $" order by {sortBy} asc";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(thenSortBy))
                {
                    query += $" order by {sortBy} desc, {thenSortBy} desc";
                }
                else
                {
                    query += $" order by {sortBy} desc";
                }
            }

            return query;
        }

        public static string Pagination(this string query)
        {
            query += " limit @offset, @pageSize";

            return query;
        }
    }
}