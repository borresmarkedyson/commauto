﻿using RivTech.CABoxTruck.WebService.Data.DALDapperModels;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DTO.Report.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.DALDapper.IDataAccess
{
    public interface IReportDataAccess
    {
        Task<IEnumerable<DMVReportModel>> GetDMVData(ReportManagementFilter reportManagementFilter);
        List<RiskDetail> GetAllRiskDetails();
    }
}