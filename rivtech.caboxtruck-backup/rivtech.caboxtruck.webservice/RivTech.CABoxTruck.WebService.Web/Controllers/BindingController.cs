﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using RivTech.CABoxTruck.WebService.Service.IServices;

namespace RivTech.CABoxTruck.WebService.Controllers
{
    [Route("api/[controller]")]
    [Authorize] // this not advisable but for now disabled to allow form upload file
    //[AllowAnonymous]
    [ApiController]
    public class BindingController : ControllerBase
    {
        private readonly IBindingService _service;
        private readonly IStorageService _storageService;
        private readonly ISLNumberService _SLNumberService;
        private readonly IFileUploadDocumentService _fileUploadDocumentService;


        public BindingController(IBindingService service,
            IStorageService storageService,
            ISLNumberService sLNumberService,
            IFileUploadDocumentService fileUploadDocumentService
        )
        {
            _service = service;
            _storageService = storageService;
            _SLNumberService = sLNumberService;
            _fileUploadDocumentService = fileUploadDocumentService;
        }

      
        #region Binding
        [HttpPost("AddUpdate")]
        [Produces(typeof(BindingDTO))]
        public async Task<IActionResult> AddUpdate([FromBody] BindingDTO resource)
        {
            try
            {
                return Ok(await _service.AddUpdateAsync(resource));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpGet("getSLADefault")]
        [Produces(typeof(SLNumberDTO))]
        public async Task<IActionResult> getSLADefault(Int16? creditId, string state)
        {
            try
            {
                return Ok(await _SLNumberService.GetByCreditOfficeIdStateAsync(creditId, state));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }
        #endregion Binding

        #region Binding Requirements
        [HttpGet("Requirements/{id}")]
        [Produces(typeof(BindingRequirementsDTO))]
        public async Task<IActionResult> AddUpdateRequirements([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _service.GetByRiskDetailIdAsync(id));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("Requirements/AddUpdate")]
        [Produces(typeof(BindingRequirementsDTO))]
        public async Task<IActionResult> AddUpdateRequirements([FromBody] BindingRequirementsDTO resource)
        {
            try
            {
                return Ok(await _service.AddUpdateAsync(resource));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("Requirements/AddUpdateWithFile")]
        //[Produces(typeof(BindingRequirementsDTO))]
        public async Task<IActionResult> AddUpdateRequirementsWithFile()
        {
            try
            {
                var resource = JsonConvert.DeserializeObject<BindingRequirementsDTO>(Request.Form["data"]);
                resource.CreatedDate = DateTime.Now;
                if (resource.FileUploads != null && resource.FileUploads.Any())
                {
                    try
                    {
                        var fileNames = resource.FileUploads.Select(x => x.FileName).ToList();
                        if (await _fileUploadDocumentService.IsBindQuoteDuplicateFileNameAsync(resource.RiskDetailId.Value, fileNames, resource.Id))
                            return new JsonResult(new { data = resource, status = true, message = "Duplicate filename is not allowed" }, new JsonSerializerSettings());

                        if (Request.Form.Files.Count > 1)
                        {
                            // Create ZIP
                            var byteFiles = _fileUploadDocumentService.CreateZipFile(Request.Form.Files);
                            resource = await _service.AzureBlogSave(byteFiles, Request.Form["states"], "bind-req", resource);
                        } 
                        else
                        {
                            resource = await _service.AzureBlogSave(Request.Form.Files, Request.Form["states"], "bind-req", resource);
                        }
                    }
                    catch (Exception)
                    {
                        //if failed saving the file. 
                        resource.FileUploads.ToList().ForEach(f => {
                            f.CreatedDate = DateTime.Now;
                            f.IsUploaded = false; });

                        resource = await _service.AddUpdateAsync(resource);
                        return new JsonResult(new { data = resource, status = true, message = "A record has been saved but here was an error trying to upload the file. Please try again." }, new JsonSerializerSettings());
                    }
                }

               resource = await _service.AddUpdateAsync(resource);

                var documentLink = _fileUploadDocumentService.GetByRefIdAsync(resource.Id.Value).Result.FirstOrDefault();
                if (documentLink != null)
                    resource.FilePath = documentLink.FilePath;

                return new JsonResult(new { data = resource, status = true, message = "" }, new JsonSerializerSettings());
                //return Ok(await _service.AddUpdateAsync(resource));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("Requirements/Delete/{id}")]
        [Produces(typeof(BindingRequirementsDTO))]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            try
            {
                if (id == null || id == Guid.Empty) return BadRequest();
                await _service.RemoveAsync(id);
                return Ok();
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }
        #endregion Binding Requirements

        #region QuoteConditions

        [Authorize]
        [HttpPost("Quote/AddUpdateQuoteWithFile")]
        public async Task<IActionResult> AddUpdateQuoteWithFile()
        {
            try
            {
                var resource = JsonConvert.DeserializeObject<QuoteConditionsDTO>(Request.Form["data"]);
                resource.CreatedDate = DateTime.Now;
                if (resource.FileUploads != null && resource.FileUploads.Any())
                {
                    try
                    {
                        var fileNames = resource.FileUploads.Select(x => x.FileName).ToList();
                        if (await _fileUploadDocumentService.IsBindQuoteDuplicateFileNameAsync(resource.RiskDetailId.Value, fileNames, resource.Id))
                            return new JsonResult(new { data = resource, status = true, message = "Duplicate filename is not allowed" }, new JsonSerializerSettings());

                        if (Request.Form.Files.Count > 1)
                        {
                            // Create ZIP
                            var byteFiles = _fileUploadDocumentService.CreateZipFile(Request.Form.Files);
                            resource = await _service.AzureBlogSave<QuoteConditionsDTO>(byteFiles, Request.Form["states"], "quote-cond", resource);
                        }
                        else
                        {
                            resource = await _service.AzureBlogSave<QuoteConditionsDTO>(Request.Form.Files, Request.Form["states"], "quote-cond", resource);
                        }
                    }
                    catch (Exception)
                    {
                        //if failed saving the file. 
                        resource.FileUploads.ToList().ForEach(f => {
                            f.CreatedDate = DateTime.Now;
                            f.IsUploaded = false;
                        });

                        resource = await _service.AddUpdateAsync(resource);
                        return new JsonResult(new { data = resource, status = true, message = "A record has been saved but here was an error trying to upload the file. Please try again." }, new JsonSerializerSettings());
                    }
                }

                resource = await _service.AddUpdateAsync(resource);

                var documentLink = _fileUploadDocumentService.GetByRefIdAsync(resource.Id.Value).Result.FirstOrDefault();
                if (documentLink != null)
                    resource.FilePath = documentLink.FilePath;
                return new JsonResult(new { data = resource, status = true, message = "" }, new JsonSerializerSettings());
                //return Ok(await _service.AddUpdateAsync(resource));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("Quote/DeleteQuote/{id}")]
        [Produces(typeof(QuoteConditionsDTO))]
        public async Task<IActionResult> DeleteQuote([FromRoute] Guid id)
        {
            try
            {
                if (id == null || id == Guid.Empty) return BadRequest();
                await _service.RemoveQuoteAsync(id);
                return Ok();
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }


        [HttpGet("Quote/hasRecord/{id}")]
        [Produces(typeof(SLNumberDTO))]
        public async Task<IActionResult> hasRecord([FromRoute] Guid id)
        {
            try
            {
                return Ok(_service.GetQuoteByRiskDetailIdAsync(id).Result.Any());
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        #endregion
    }
}
