﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using RivTech.CABoxTruck.WebService.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EndorsementController : ControllerBase
    {
        private readonly IEndorsementService _endorsementService;
        private readonly ILogger<EndorsementController> _logger;

        public EndorsementController(
            ILogger<EndorsementController> logger,
            IEndorsementService endorsementService
        ) {
            _logger = logger;
            _endorsementService = endorsementService;
        }

        [HttpGet("PremiumChanges/{id}")]
        [Produces(typeof(EndorsementPremiumChangeSummaryDTO))]
        [AllowAnonymous]
        public async Task<IActionResult> GetPremiumChanges([FromRoute] Guid id)
        {
            try
            {
                var resources = await _endorsementService.GetPremiumChanges(id);
                return Ok(resources);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [AllowAnonymous]
        [HttpPost("CalculatePremiumChanges/{id}")]
        public async Task<IActionResult> CalculatePremiumChanges([FromRoute] Guid id, [FromRoute] DateTime effectiveDate)
        {
            try
            {
                await _endorsementService.CalculatePremiumChanges(id, effectiveDate, 1);
                return Ok();
            }
            catch(Exception e)
            {
                _logger.LogError("Error prorating vehicle premiums.", e);
                return new StatusCodeResult(500);
            }
        } 

        [HttpPut]
        [Route("UpdateEffectiveDate/{id}")]
        public async Task<IActionResult> UpdateEffectiveDate([FromRoute] Guid id, string effectiveDate)
        {
            try
            {
                var date = DateTime.Parse(effectiveDate);
                await _endorsementService.UpdateEffectiveDate(id, date);
                return Ok(date);
            }
            catch(Exception e)
            {
                _logger.LogError("Error updating endorsement effective date.", e);
                return BadRequest();
            }
        }

        [HttpPost("GeneratePolicyNumber/{id}")]
        public async Task<IActionResult> GeneratePolicyNumber([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _endorsementService.GeneratePolicyNumber(id));
            }
            catch (Exception e)
            {
                _logger.LogError("Error Generating Policy Number.", e);
                return new StatusCodeResult(500);
            }
        }
    }
}

