﻿using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.WebService.DTO.User;
using RivTech.CABoxTruck.WebService.Service.IServices.User;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Controllers.User
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserInfoService _userInfoService;

        public UserController(IUserInfoService userInfoService)
        {
            _userInfoService = userInfoService;
        }

        [HttpPost("login")]
        [Produces("application/json")]
        public async Task<ActionResult<string>> LoginAsync([FromBody] LoginDto model)
        {
            return Ok(await _userInfoService.LoginAsync(model));
        }

        [HttpPost("logout")]
        [Produces("application/json")]
        public async Task<ActionResult<string>> LogoutAsync([FromBody] LoginDto model)
        {
            return Ok(await _userInfoService.LogoutAsync(model));
        }
    }
}
