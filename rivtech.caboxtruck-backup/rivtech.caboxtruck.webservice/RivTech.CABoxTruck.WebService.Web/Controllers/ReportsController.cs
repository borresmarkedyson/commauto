﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Report.Filters;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Report;

namespace RivTech.CABoxTruck.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ReportsController : ControllerBase
    {
        private readonly IAccountsReceivableService _accountsReceivableService;
        private readonly IReportService _reportService;
        
        public ReportsController(IAccountsReceivableService accountsReceivableService, IReportService reportService)
        {
            _accountsReceivableService = accountsReceivableService;
            _reportService = reportService;
        }

        [HttpPost("SearchAccountsReceivable")]
        [Produces(typeof(List<AccountsReceivableDTO>))]
        public async Task<IActionResult> SearchAccountsReceivable([FromBody] AccountsReceivableInputDTO input)
        {
            try
            {
                return Ok(await _accountsReceivableService.SearchAccountsReceivable(input));
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("GetReportStream")]
        [Produces(typeof(FileStreamResult))]
        public async Task<IActionResult> Post([FromBody] ReportManagementFilter reportManagementFilter)
        {
            return new FileStreamResult(await _reportService.GetReportStream(reportManagementFilter),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            { FileDownloadName = reportManagementFilter.ReportName + ".xlsx" };
        }
    }
}