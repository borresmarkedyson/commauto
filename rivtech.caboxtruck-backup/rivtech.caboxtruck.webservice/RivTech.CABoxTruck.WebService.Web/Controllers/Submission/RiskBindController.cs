﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;

namespace RivTech.CABoxTruck.WebService.Controllers.Submission
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class RiskBindController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public RiskBindController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet("Issue/{id}")]
        //[Produces(typeof(List<RiskDTO>))]
        public async Task<IActionResult> Issue([FromRoute] Guid id)
        {
            try
            {
                var res = await _transactionService.IssueSubmission(id);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpGet("Reset/{id}")]
        //[Produces(typeof(List<RiskDTO>))]
        public async Task<IActionResult> Reset([FromRoute] Guid id)
        {
            try
            {
                var res = await _transactionService.ResetPolicyChanges(id);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpGet("GetPolicyChanges/{id}")]
        //[Produces(typeof(List<RiskDTO>))]
        public async Task<IActionResult> GetPolicyChanges([FromRoute] Guid id)
        {
            try
            {
                var res = await _transactionService.GetPolicyChanges(id);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("IssueEndorsement")]
        public async Task<IActionResult> IssueEndorsement([FromBody] JObject endorsementDetails)
        {
            try
            {
                Guid riskDetailId = Guid.Parse(endorsementDetails.SelectToken("riskDetailId").Value<string>());
                DateTime effectiveDate = endorsementDetails.SelectToken("effectiveDate").Value<DateTime>();
                string endorsementText = endorsementDetails.SelectToken("endorsementText").Value<string>();
                string policyChanges = endorsementDetails.SelectToken("policyChanges").Value<string>();

                var res = await _transactionService.IssueEndorsement(riskDetailId, effectiveDate, endorsementText, policyChanges);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("CancelPolicy")]
        public async Task<IActionResult> CancelPolicy([FromBody] JObject cancellationDetails)
        {
            try
            {
                Guid riskDetailId = Guid.Parse(cancellationDetails.SelectToken("riskDetailId").Value<string>());
                DateTime effectiveDate = cancellationDetails.SelectToken("effectiveDate").Value<DateTime>();
                string cancellationReasonType = cancellationDetails.SelectToken("cancellationReasonType").Value<string>();
                bool isShortRate = cancellationDetails.SelectToken("isShortRate").Value<bool>();

                await _transactionService.CancelPolicy(riskDetailId, effectiveDate, cancellationReasonType, isShortRate);
                return Ok();
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpGet("ReinstatePolicy/{id}")]
        public async Task<IActionResult> ReinstatePolicy([FromRoute] Guid id)
        {
            try
            {
                var res = await _transactionService.ReinstatePolicy(id);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
         }

        [HttpGet("RewritePolicy/{id}")]
        public async Task<IActionResult> RewritePolicy([FromRoute] Guid id)
        {
            try
            {
                await _transactionService.CancelPolicyForRewrite(id, DateTime.Now, true);
                var res = await _transactionService.RewritePolicyToSubmission(id, DateTime.Now);
                return Ok(res);
            }
            catch (ModelNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("AutoPolicyCancellation")]
        public async Task<IActionResult> AutoPolicyCancellation()
        {
            var jobDate = GetRequestDate(Request);
            await _transactionService.AutoPolicyCancellation(jobDate);
            return Ok();
        }

        [HttpPost]
        [Route("AutoNoticeCancellation")]
        public async Task<IActionResult> AutoNoticeCancellation()
        {
            var jobDate = GetRequestDate(Request);
            await _transactionService.AutoNoticeOfCancellation(jobDate);
            return Ok();
        }

        [HttpPost]
        [Route("AutoRescindNoticeCancellation")]
        public async Task<IActionResult> AutoRescindNoticeCancellation()
        {
            var jobDate = GetRequestDate(Request);
            await _transactionService.AutoRescindNoticeOfCancellation(jobDate);
            return Ok();
        }

        private DateTime GetRequestDate(HttpRequest request)
        {
            var jobDate = DateTime.Now;
            if (request.Headers.TryGetValue("userdate", out var headerValues))
            {
                jobDate = DateTime.Parse(headerValues.FirstOrDefault());
            }

            return jobDate;
        }
    }
}
 