﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Text;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.DTO;
using Microsoft.Extensions.Logging;

namespace RivTech.CABoxTruck.WebService.Controllers.Rater
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class ExperienceRaterController : ControllerBase
    {
        private readonly ICoverageLimitService _coverageLimitService;
        private readonly ICoverageHistoryService _coverageHistoryService;
        private readonly IRaterService _raterService;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<ExperienceRaterController> _logger;

        public ExperienceRaterController(
            ILogger<ExperienceRaterController> logger,
            ICoverageLimitService coverageLimitService,
            IRaterService raterService,
            ICoverageHistoryService coverageHistoryService,
            IHttpClientFactory httpClientFactory
        )
        {
            _logger = logger;
            _coverageLimitService = coverageLimitService;
            _coverageHistoryService = coverageHistoryService;
            _raterService = raterService;
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet]
        [Route("testconnection")]
        public async Task<string> TestConnection()
        {
            try
            {
                var client = _httpClientFactory.CreateClient("rater");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync($"api/PremiumRater/test");

                return (response.IsSuccessStatusCode) ? "Ok" : "Connection Failed.";
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return $"Unable to connect. Internal Server Error. Exception: {e.StackTrace}";
            }
        }

        [HttpPost]
        [Route("create/{id}")]
        public async Task<ActionResult> Create(string id, [FromQuery] string effectiveDate)
        {
            try
            {
                bool isExists = await IsRaterExcelExists(id);
                if (isExists) return Ok();

                using var httpClient = _httpClientFactory.CreateClient("rater");
                var response = await httpClient.PostAsync($"api/ExperienceRater/create/{id}?effectiveDate={effectiveDate}", new StringContent(string.Empty, Encoding.UTF8, "application/json"));
                return Ok(response);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"{e.Message}");
                return StatusCode(500, $"Failed creating rater file for {id}.");
            }
        }

        private async Task<bool> IsRaterExcelExists(string id)
        {
            bool isExist = true;
            try
            {
                using var httpClient = _httpClientFactory.CreateClient("rater");
                var response = await httpClient.GetAsync($"api/ExperienceRater/exists/{id}");
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);
                var content = await response.Content.ReadAsStringAsync();
                return bool.Parse(content);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return isExist;
            }
        }

        [HttpPost]
        [Route("download/{id}")]
        public async Task<ActionResult> Download(Guid id, [FromQuery] DateTime effectiveDate, [FromBody] JObject requestBody)
        {
            string submissionNo = requestBody["SubmissionNo"].ToString();

            try
            {
                requestBody = await _raterService.MapFields(id, requestBody);

                // Disabling certificate validation can expose you to a man-in-the-middle attack
                // which may allow your encrypted message to be read by an attacker
                // https://stackoverflow.com/a/14907718/740639
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (
                        object s,
                        X509Certificate certificate,
                        X509Chain chain,
                        SslPolicyErrors sslPolicyErrors
                    )
                    {
                        return true;
                    };

                //using var httpclient = new HttpClient();
                using var httpclient = _httpClientFactory.CreateClient("rater");
                var content = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json");
                var effDat = effectiveDate.ToString("MM-dd-yyyy");
                var endpoint = $"api/ExperienceRater/download/{submissionNo}?effectiveDate={effDat}";
                var res = await httpclient.PostAsync(endpoint, content);
                if (!res.IsSuccessStatusCode)
                {
                    return BadRequest(res.ReasonPhrase);
                }
                var contentBytes = await res.Content.ReadAsByteArrayAsync();

                var response = new FileContentResult(contentBytes, "application/vnd.ms-excel.sheet.binary.macroEnabled.12")
                {
                    FileDownloadName = $"Experience Rater.xlsb"
                };

                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, $"Failed downloading rater for {id}.");
            }
        }

        [HttpPost]
        [Route("upload/{id}")]
        public async Task<ActionResult> Upload(string id, [FromQuery] DateTime effectiveDate)
        {
            IFormFile postedFile = Request.Form.Files[0];
            string guid = Request.Form["guid"];
            try
            {
                if (postedFile.Length < 1)
                {
                    return BadRequest("No file found from request.");
                }

                //if (postedFile.FileName.IndexOf(id) == -1)
                //{
                //    return BadRequest($"Incorrect rater file uploaded for {id}.");
                //}

                using var stream = postedFile.OpenReadStream();
                byte[] content = new byte[postedFile.Length];
                stream.Read(content, 0, content.Length);

                // Create form content to be sent on http.
                var multiContent = new MultipartFormDataContent();
                // Create bytes content of uploaded file to be added on form content.
                var bytesContent = new ByteArrayContent(content);
                multiContent.Add(bytesContent, "file", postedFile.FileName);

                using var httpClient = _httpClientFactory.CreateClient("rater");
                var response = await httpClient.PostAsync($"api/ExperienceRater/upload/{id}?effectiveDate={effectiveDate}", multiContent);
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);

                var responseContent = await response.Content.ReadAsStringAsync();
                JObject uploadResults = JObject.Parse(responseContent);

                var experienceRatingFactor = uploadResults.SelectToken("data.ALExperienceRF")?.Value<decimal?>();
                var scheduleRatingFactor = uploadResults.SelectToken("data.ALScheduleRF")?.Value<decimal?>();

                if (experienceRatingFactor == null || scheduleRatingFactor == null)
                {
                    return new JsonResult(uploadResults);
                }

                var options = await _raterService.GetOptions(Guid.Parse(guid));

                foreach (string option in options)
                {
                    SaveRiskCoverageDTO saveRiskCoverage = new SaveRiskCoverageDTO()
                    {
                        RiskDetailId = Guid.Parse(guid),
                        OptionNumber = short.Parse(option),
                        LiabilityExperienceRatingFactor = decimal.Round(experienceRatingFactor ?? 0, 2, MidpointRounding.AwayFromZero),
                        LiabilityScheduleRatingFactor = decimal.Round(scheduleRatingFactor ?? 0, 2, MidpointRounding.AwayFromZero)
                    };
                    await _coverageLimitService.UpdateRatingFactorAsync(saveRiskCoverage);
                }

                for (int year = 0; year <= 4; year++)
                {
                    RiskHistoryDTO riskHistory = new RiskHistoryDTO()
                    {
                        RiskDetailId = Guid.Parse(guid),
                        HistoryYear = year,
                        NumberOfPowerUnitsAL = decimal.Round(uploadResults.SelectToken($"data.HIC")[year]?.SelectToken("NumberOfPowerUnitsAL").Value<decimal?>() ?? 0, 2, MidpointRounding.AwayFromZero),
                        NumberOfPowerUnitsAPD = decimal.Round(uploadResults.SelectToken($"data.HIC")[year]?.SelectToken("NumberOfPowerUnitsAPD").Value<decimal?>() ?? 0, 2, MidpointRounding.AwayFromZero)
                    };

                    await _coverageHistoryService.UpdatePowerUnitFieldsAsync(riskHistory);
                }

                return new JsonResult(uploadResults);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, $"Failed uploading rater for {id}.");
            }
        }
    }
}
