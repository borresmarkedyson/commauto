﻿using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Rater;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.Service.Services.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RivTech.CABoxTruck.WebService.Controllers.Rater
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class PremiumRaterController : ControllerBase
    {
        private readonly string raterUploadFilePath;
        private readonly IRaterService _raterService;
        private readonly ILogger<PremiumRaterController> log;
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITaxService _taxService;
        private readonly IBillingService _billingService;
        private readonly IEndorsementService _endorsementService;
        private readonly IVehicleService _vehicleService;


        public PremiumRaterController(
            ILogger<PremiumRaterController> logger,
            IRaterService raterService,
            AppDbContext context,
            IMapper mapper,
            IHttpClientFactory httpClientFactory,
            ITaxService taxService,
            IBillingService billingService,
            IEndorsementService endorsementService,
            IVehicleService vehicleService
        )
        {
            log = logger;
            _raterService = raterService;
            _context = context;
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
            _taxService = taxService;
            _billingService = billingService;
            _endorsementService = endorsementService;
            _vehicleService = vehicleService;
        }

        [HttpGet]
        [Route("testconnection")]
        public async Task<string> TestConnection()
        {
            try
            {
                var client = _httpClientFactory.CreateClient("rater");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync($"api/PremiumRater/test");

                return (response.IsSuccessStatusCode) ? "Ok" : "Connection Failed.";
            }
            catch (Exception e)
            {
                log.LogError(e, e.Message);
                return $"Unable to connect. Internal Server Error. Exception: {e.StackTrace}";
            }
        }

        [HttpPost]
        [Route("calculatePremium")]
        public async Task<ActionResult> CalculatePremium([FromBody] JObject raterRequest, [FromQuery] bool? fromPolicy = null, bool? forIssuance = null, DateTime? effectiveDate = null)
        {
            Guid riskDetailId = Guid.Parse(raterRequest.SelectToken("riskDetailId")?.Value<string>() ?? "");
            string submissionNumber = raterRequest.SelectToken("SubmissionNo")?.Value<string>();
            DateTime? inceptionDate = raterRequest.SelectToken("Broker.EffectiveDate")?.Value<DateTime?>();
            string optionId = raterRequest.SelectToken("OptionId").Value<string>();

            try
            {
                // Preload rater
                await _raterService.PreloadPremiumRater(submissionNumber, riskDetailId, fromPolicy);

                // Create input values
                var inputValues = await _raterService.MapFields(riskDetailId, raterRequest, int.Parse(optionId), true, fromPolicy);

                int? endorsementNumber = null;
                if (forIssuance == true)
                {
                    endorsementNumber = await _endorsementService.GetNextEndorsementNumber(riskDetailId);
                }

                raterRequest = new JObject(
                    new JProperty("SubmissionNumber", submissionNumber),
                    new JProperty("InceptionDate", inceptionDate),
                    new JProperty("OptionId", optionId),
                    new JProperty("EndorsementNumber", endorsementNumber),
                    new JProperty("InputValues", inputValues)
                );

                string results = string.Empty;
                
                results = await CalculateAsync(riskDetailId, submissionNumber, raterRequest, fromPolicy, forIssuance, effectiveDate);

                return new JsonResult(JObject.Parse(results));
            }
            catch(RaterException e)
            {
                return new JsonResult(new { 
                    status = false,
                    message = e.Message,
                });
            }
        }


        [HttpPost]
        [Route("preload/{submissionNumber}")]
        public async Task<JsonResult> Preload(string submissionNumber, Guid riskDetailId, bool? fromPolicy = null)
        {
            try
            {
                var result = await _raterService.PreloadPremiumRater(submissionNumber, riskDetailId, fromPolicy);
                return new JsonResult(new
                {
                    status = true,
                    message = result
                });
            }
            catch (RaterException e)
            {
                log.LogError(e, e.Message);
                return new JsonResult(new
                {
                    status = false,
                    message = e.Message
                });
            }
            catch (Exception e)
            {
                log.LogError(e, "Error preloading rater");
                return new JsonResult(new { status = false, message = "There was an error trying to pre-load the rating file. " + e.Message, sysErr = e.Message }, new JsonSerializerSettings());
            }
        }

        [HttpPost]
        [Route("unload/{submissionNumber}")]
        public async Task Unload(string submissionNumber, string riskDetailId = "", bool ispageunload = false, bool? fromPolicy = null)
        {
            try
            {
                Guid _riskDetailId = Guid.Parse(riskDetailId);
                // always pass the option count to cross reference on the running excel engine
                var options = await _raterService.GetOptions(_riskDetailId);
                using var httpClient = _httpClientFactory.CreateClient("rater");
                int count = ispageunload ? 0 : options.Count;

                var url = $"api/PremiumRater/unload/{submissionNumber}?itemCount={count}";
                url += $"&fromPolicy={fromPolicy}";

                await httpClient.DeleteAsync(url);
            }
            catch (Exception e)
            {
                log.LogError(e, "Error unloading rater");
            }
        }

        private async Task<string> CalculateAsync(Guid guid, string submissionNumber, JObject requestBody, bool? fromPolicy = null, bool? forIssuance = null, DateTime? effectiveDate = null)
        {
            try
            {
                using var httpClient = _httpClientFactory.CreateClient("rater");
                var content = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync($"api/PremiumRater/rate/{submissionNumber}?&fromPolicy={fromPolicy}&forIssuance={forIssuance}", content);
                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);
                var result = await response.Content.ReadAsStringAsync();

                result = await ExtractAndSaveRatingFactors(guid, result, fromPolicy ?? false);
                await _vehicleService.UpdatePremiums(guid);

                int optionId = requestBody.SelectToken("OptionId").Value<int>();

                if (fromPolicy == true)
                {
                    await _taxService.CalculateRiskTaxAsync(guid);
                    await _endorsementService.CalculatePremiumChanges(guid, effectiveDate.Value, optionId);
                }
                else
                {
                    await _taxService.CalculateRiskTaxAsync(guid, optionId);
                    await _billingService.UpdatePaymentOptions(guid, optionId);
                }

                return result.Replace("ERROR", "0.00");
            }
            catch (Exception e)
            {
                throw new Exception($"1. Error encountered while calculating premium. {e.Message}\n{e.StackTrace}");
            }
        }

        private async Task<string> ExtractAndSaveRatingFactors(Guid guid, string calculatedResults, bool fromPolicy)
        {
            try
            {
                JObject obj = JObject.Parse(calculatedResults);
                bool isSuccess = obj.SelectToken("success")?.Value<bool?>() ?? false;
                if (!isSuccess)
                {
                    string errorMsg = obj.SelectToken("error")?.Value<string>();
                    throw new ArgumentException($"2-B. Error encountered while calculating premium. {errorMsg}");
                }

                string optionVal = obj.SelectToken("data.optionId")?.Value<string>();
                int optionId = string.IsNullOrWhiteSpace(optionVal) ? 0 : Convert.ToInt32(optionVal);

                // extract vehicle factors
                var objVehicleList = obj.SelectToken("data.result.Vehicle")?.Select(current => new PremiumRaterVehicleFactorsDTO()
                {
                    RiskDetailId = guid,
                    VehicleId = current.SelectToken("VehicleID")?.Value<string>(),
                    // ALPremium = current.SelectToken("ALPremium").Value<decimal?>(),
                    TotalPremium = current.SelectToken("TotalPremium")?.Value<decimal?>(),
                    TotalALPremium = current.SelectToken("ALPremium")?.Value<decimal?>(),
                    // TotalALAddOns = current.SelectToken("TotalALAddOns").Value<decimal?>(),
                    TotalPDPremium = current.SelectToken("PDPremium")?.Value<decimal?>(),
                    BIPremium = current.SelectToken("BIPremium")?.Value<decimal?>(),
                    PropDamagePremium = current.SelectToken("PropDamagePremium")?.Value<decimal?>(),
                    MedPayPremium = current.SelectToken("MedPayPremium")?.Value<decimal?>(),
                    PIPPremium = current.SelectToken("PIPPremium")?.Value<decimal?>(),
                    // PIOtherPremium = current.SelectToken("PIOtherPremium").Value<decimal?>(),
                    UMUIM_BI_Premium = current.SelectToken("UMUIM_BI_Premium")?.Value<decimal?>(),
                    UMUIM_PD_Premium = current.SelectToken("UMUIM_PD_Premium")?.Value<decimal?>(),
                    CompPremium = current.SelectToken("CompPremium")?.Value<decimal?>(),
                    FireTheftPremium = current.SelectToken("FireTheftPremium")?.Value<decimal?>(),
                    CollisionPremium = current.SelectToken("CollisionPremium")?.Value<decimal?>(),
                    CargoPremium = current.SelectToken("CargoPremium")?.Value<decimal?>(),
                    // NTLPremium = current.SelectToken("NTLPremium").Value<decimal?>(),
                    // BIRiskMgmtFee = current.SelectToken("BIRiskMgmtFee").Value<decimal?>(),
                    // PDRiskMgmtFee = current.SelectToken("PDRiskMgmtFee").Value<decimal?>(),
                    // StatedAmount = current.SelectToken("StatedAmount").Value<decimal?>(),
                    // TIVPercentage = current.SelectToken("TIVPercentage").Value<decimal?>(),
                    PerVehiclePremium = current.SelectToken("PerVehiclePremium")?.Value<decimal?>(),
                    Radius = current.SelectToken("Radius")?.Value<decimal?>(),
                    RadiusTerritory = current.SelectToken("RadiusTerritory")?.Value<decimal?>(),
                    OptionId = optionId
                }).ToList();


                // Revert back the correct vehicle id. 
                string vehicleIdMappingKey = obj.SelectToken("data.inputValues.VehicleIdMappingKey")?.Value<string>();
                if (!string.IsNullOrWhiteSpace(vehicleIdMappingKey))
                {
                    var idMapping = PremiumRaterVehicleFactorsDTO.VehicleIdMappings[Guid.Parse(vehicleIdMappingKey)];
                    foreach (var veh in objVehicleList)
                        veh.VehicleId = idMapping[int.Parse(veh.VehicleId)].ToString();

                    foreach (var vehToken in obj.SelectToken("data.result.Vehicle"))
                        vehToken.SelectToken("VehicleID").Replace(idMapping[int.Parse(vehToken.SelectToken("VehicleID").Value<string>())].ToString());

                    PremiumRaterVehicleFactorsDTO.VehicleIdMappings.Remove(Guid.Parse(vehicleIdMappingKey));
                }

                //Add Refrigeration
                var vehicleRefs = obj.SelectToken("data.result.VehicleRef")?.Select(vr => new
                {
                    Value = vr?.SelectToken("value1")?.Value<decimal?>() + vr?.SelectToken("value2")?.Value<decimal?>()
                }).ToList();

                if (vehicleRefs?.Count > 0)
                {
                    var index = 0;
                    objVehicleList?.ForEach(v =>
                    {
                        if (vehicleRefs.ElementAtOrDefault(index) != null)
                        {
                            v.RefrigerationPremium = vehicleRefs[index].Value;
                        }
                        index++;
                    });
                }

                // extract endorsement premiums
                var hiredPhysicalDmg = obj.SelectToken("data.inputValues.EndorsementPremiums")?.SelectToken("HiredPhysicalDamage")?.Value<decimal?>();
                var trailerInterchange = obj.SelectToken("data.inputValues.EndorsementPremiums")?.SelectToken("TrailerInterchange")?.Value<decimal?>();
                var alManuscriptPremium = obj.SelectToken("data.inputValues.EndorsementPremiums")?.SelectToken("ALManuscriptPremium")?.Value<decimal?>();
                var pdManuscriptPremium = obj.SelectToken("data.inputValues.EndorsementPremiums")?.SelectToken("PDManuscriptPremium")?.Value<decimal?>();
                var endorsementPremiums = (hiredPhysicalDmg ?? 0) + (trailerInterchange ?? 0);
                // extract Driver factors
                var objDriverList = obj.SelectToken("data.result.Driver")?.Select(current => new PremiumRaterDriverFactorsDTO()
                {
                    RiskDetailId = guid,
                    DriverID = current.SelectToken("DriverID")?.Value<string>(),
                    FiveYearPoints = current.SelectToken("FiveYearPoints")?.Value<string>(),
                    // DriverKey = current.SelectToken("DriverKey").Value<string>(),
                    // PointsFactor = current.SelectToken("PointsFactor").Value<string>(),
                    // YearsExperienceFactor = current.SelectToken("YearsExperienceFactor").Value<string>(),
                    AgeFactor = current.SelectToken("AgeFactor")?.Value<string>(),
                    // OutOfStateFactor = current.SelectToken("OutOfStateFactor").Value<string>(),
                    TotalFactor = current.SelectToken("TotalFactor")?.Value<string>(),
                    // Included = current.SelectToken("Included").Value<string>(),
                    // TotalFactorIncl = current.SelectToken("TotalFactorIncl").Value<string>(),
                    // FactorIncl_AddDriver = current.SelectToken("FactorIncl_AddDriver").Value<string>(),
                    // UnderAge25 = current.SelectToken("UnderAge25").Value<string>(),
                    // UnderAge25AndClean = current.SelectToken("UnderAge25AndClean").Value<string>(),
                    // HasMultipleAFOrMovingViolation = current.SelectToken("HasMultipleAFOrMovingViolation").Value<string>(),
                    OptionId = optionId
                }).ToList();
                // extract other factors and premiums
                var objFactors = new PremiumRatingFactorsDTO()
                {
                    RiskDetailId = guid,
                    RiskCoverageId = Guid.Parse(obj.SelectToken("data.inputValues")?.SelectToken("RiskCoverageId")?.Value<string>()),
                    AccountDriverFactor = obj.SelectToken("data.result")?.SelectToken("AccountDriverFactor")?.Value<string>(),
                    AdditionalInsuredPremium = obj.SelectToken("data.inputValues.AdditionalInterest")?.SelectToken("AdditionalInsuredPremium")?.Value<string>(),
                    GeneralLiabilityPremium = obj.SelectToken("data.result")?.SelectToken("GeneralLiabilityPremium")?.Value<string>(),
                    //PremiumTaxesFees = obj.SelectToken("data.result")?.SelectToken("PremiumTaxesFees")?.Value<string>(),
                    PrimaryNonContributoryPremium = obj.SelectToken("data.inputValues.AdditionalInterest")?.SelectToken("PrimaryNonContributoryPremium")?.Value<string>(),
                    RiskManagementFeeAL = obj.SelectToken("data.inputValues.Vehicle")?.SelectToken("RiskManagementFeeAL")?.Value<string>(),
                    RiskManagementFeeAPD = obj.SelectToken("data.inputValues.Vehicle")?.SelectToken("RiskManagementFeePD")?.Value<string>(),
                    //SubTotalDue = obj.SelectToken("data.result")?.SelectToken("SubTotalDue")?.Value<string>(),
                    //TotalAnnualPremiumWithPremiumTax = obj.SelectToken("data.result")?.SelectToken("TotalAnnualPremiumWithPremiumTax")?.Value<string>(),
                    TotalCargoPremium = $"{objVehicleList.Sum(vehicles => vehicles.CargoPremium ?? 0.00m)}",
                    TotalLiabilityPremium = $"{objVehicleList.Sum(vehicles => vehicles.TotalALPremium ?? 0.00m) + (alManuscriptPremium ?? 0.00m)}",
                    //TotalOtherPolicyLevelPremium = obj.SelectToken("data.result")?.SelectToken("TotalOtherPolicyLevelPremium")?.Value<string>(),
                    TotalPhysDamPremium = $"{objVehicleList.Sum(vehicles => vehicles.TotalPDPremium ?? 0.00m) + endorsementPremiums + (pdManuscriptPremium ?? 0.00m)}",
                    ComprehensivePremium = $"{objVehicleList.Sum(vehicles => vehicles.CompPremium ?? 0.00m) + objVehicleList.Sum(vehicles => vehicles.FireTheftPremium ?? 0.00m)}",
                    CollisionPremium = $"{objVehicleList.Sum(vehicles => vehicles.CollisionPremium ?? 0.00m)}",
                    //TotalVehicleLevelPremium = obj.SelectToken("data.result")?.SelectToken("TotalVehicleLevelPremium")?.Value<string>(),
                    WaiverOfSubrogationPremium = obj.SelectToken("data.inputValues.AdditionalInterest")?.SelectToken("WaiverOfSubrogationPremium")?.Value<string>(),
                    HiredPhysicalDamage = $"{hiredPhysicalDmg ?? 0}",
                    TrailerInterchange = $"{trailerInterchange ?? 0}",
                    ALManuscriptPremium = $"{alManuscriptPremium ?? 0}",
                    PDManuscriptPremium = $"{pdManuscriptPremium ?? 0}",
                    OptionId = optionId,
                    VehicleFactors = objVehicleList,
                    DriverFactors = objDriverList
                };

                // save factors
                bool isFactorsSaved = objFactors != null && await SaveFactors(objFactors, fromPolicy);
                if (!isFactorsSaved)
                    throw new ArgumentException($"2-A. Error encountered while saving factors. The list of factors does not match with the number of factors saved.");

                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception e)
            {
                throw new ArgumentException($"2. Error encountered while saving factors. {e.Message}\n{e.StackTrace}");
            }
        }

        private async Task<bool> SaveFactors(PremiumRatingFactorsDTO factorsData, bool? fromPolicy = null)
        {
            bool isFactorsSaved = false;
            var executionStrategy = _context.Database.CreateExecutionStrategy();
            var forEndorsementVehicleFactorDTOs = new List<PremiumRaterVehicleFactorsDTO>();
            var premiumFactorDTOs = new List<PremiumRaterVehicleFactorsDTO>();

            executionStrategy.Execute(() =>
            {
                using var transaction = _context.Database.BeginTransaction();
                try
                {
                    #region Vehicle
                    foreach (PremiumRaterVehicleFactorsDTO vehicleFactor in factorsData.VehicleFactors)
                    {
                        premiumFactorDTOs.Add(vehicleFactor);
                        var veh = _context.Vehicle.FirstOrDefault(o => o.Id.ToString() == vehicleFactor.VehicleId);
                        if ((fromPolicy != true && veh != null))
                        {
                            veh.AutoLiabilityPremium = vehicleFactor.TotalALPremium ?? 0.00m;
                            veh.CargoPremium = vehicleFactor.CargoPremium ?? 0.00m;
                            veh.RefrigerationPremium = vehicleFactor.RefrigerationPremium ?? 0.00m;
                            veh.PhysicalDamagePremium = vehicleFactor.TotalPDPremium ?? 0.00m;
                            veh.ComprehensivePremium = (vehicleFactor.CompPremium ?? 0.00m) + (vehicleFactor.FireTheftPremium ?? 0.00m);
                            veh.CollisionPremium = vehicleFactor.CollisionPremium ?? 0.00m;
                            veh.TotalPremium = vehicleFactor.PerVehiclePremium ?? 0.00m;
                            veh.Radius = vehicleFactor.Radius ?? 0.00m;
                            veh.RadiusTerritory = vehicleFactor.RadiusTerritory ?? 0.00m;

                            //veh.ReassignProratedPremiums();
                            _context.SaveChanges();
                        }
                        else if (veh != null && fromPolicy == true)
                        {
                            forEndorsementVehicleFactorDTOs.Add(vehicleFactor);
                        }
                        //forEndorsementVehicleFactorDTOs.Add(vehicleFactor);
                    }
                    #endregion Vehicle

                    #region Vehicle Factors
                    // save vehicle factors 
                    foreach (PremiumRaterVehicleFactorsDTO vehicleFactor in factorsData.VehicleFactors)
                    {
                        var existingVehicles = _context.VehiclePremiumRatingFactor.Where(x => x.RiskDetailId == vehicleFactor.RiskDetailId && x.VehicleId == vehicleFactor.VehicleId && x.OptionId == vehicleFactor.OptionId);
                        // Remove existing records.
                        _context.VehiclePremiumRatingFactor.RemoveRange(existingVehicles);
                        // Add new.
                        _context.Add(_mapper.Map<VehiclePremiumRatingFactor>(vehicleFactor));

                        /*
                        else
                        {
                            existingVehicles.RiskDetailId = vehicleFactor.RiskDetailId;
                            existingVehicles.VehicleId = vehicleFactor.VehicleId;
                            //existingVehicles.ALPremium = vehicleFactor.ALPremium ?? 0.00m;
                            existingVehicles.TotalPremium = vehicleFactor.TotalPremium ?? 0.00m;
                            existingVehicles.TotalALPremium = vehicleFactor.TotalALPremium ?? 0.00m;
                            //existingVehicles.TotalALAddOns = vehicleFactor.TotalALAddOns ?? 0.00m;
                            existingVehicles.TotalPDPremium = vehicleFactor.TotalPDPremium ?? 0.00m;
                            existingVehicles.BIPremium = vehicleFactor.BIPremium ?? 0.00m;
                            existingVehicles.PropDamagePremium = vehicleFactor.PropDamagePremium ?? 0.00m;
                            existingVehicles.MedPayPremium = vehicleFactor.MedPayPremium ?? 0.00m;
                            existingVehicles.PIPremium = vehicleFactor.PIPPremium ?? 0.00m;
                            //existingVehicles.PIOtherPremium = vehicleFactor.PIOtherPremium ?? 0.00m;
                            existingVehicles.UMUIM_BI_Premium = vehicleFactor.UMUIM_BI_Premium ?? 0.00m;
                            existingVehicles.UMUIM_PD_Premium = vehicleFactor.UMUIM_PD_Premium ?? 0.00m;
                            existingVehicles.CompPremium = vehicleFactor.CompPremium ?? 0.00m;
                            existingVehicles.FireTheftPremium = vehicleFactor.FireTheftPremium ?? 0.00m;
                            existingVehicles.CollisionPremium = vehicleFactor.CollisionPremium ?? 0.00m;
                            existingVehicles.CargoPremium = vehicleFactor.CargoPremium ?? 0.00m;
                            existingVehicles.RefrigerationPremium = vehicleFactor.RefrigerationPremium ?? 0.00m;
                            //existingVehicles.NTLPremium = vehicleFactor.NTLPremium ?? 0.00m;
                            //existingVehicles.BIRiskMgmtFee = vehicleFactor.BIRiskMgmtFee ?? 0.00m;
                            //existingVehicles.PDRiskMgmtFee = vehicleFactor.PDRiskMgmtFee ?? 0.00m;
                            //existingVehicles.StatedAmount = vehicleFactor.StatedAmount ?? 0.00m;
                            //existingVehicles.TIVPercentage = vehicleFactor.TIVPercentage ?? 0.00m;
                            existingVehicles.PerVehiclePremium = vehicleFactor.PerVehiclePremium ?? 0.00m;
                            existingVehicles.Radius = vehicleFactor.Radius ?? 0.00m;
                            existingVehicles.RadiusTerritory = vehicleFactor.RadiusTerritory ?? 0.00m;
                            existingVehicles.OptionId = vehicleFactor.OptionId;
                            _context.Update(existingVehicles);
                        }
                        */
                        _context.SaveChanges();
                    }
                    #endregion Vehicle Factors

                    #region Driver Header Account Driver Factor
                    if (factorsData.OptionId == 1)
                    {
                        var DriverHeader = _context.DriverHeader.FirstOrDefault(o => o.RiskDetailId == factorsData.RiskDetailId);
                        if (DriverHeader != null)
                        {
                            DriverHeader.accDriverFactor = parseValueToDecimal(factorsData.AccountDriverFactor).ToString();
                            _context.SaveChanges();
                        }
                    }
                    #endregion Driver Header Account Driver Factor

                    #region Driver
                    foreach (PremiumRaterDriverFactorsDTO driverFactors in factorsData.DriverFactors)
                    {
                        //var Driver = _context.Driver.FirstOrDefault(o => o.RiskDetailId == driverFactors.RiskDetailId && o.Id.ToString() == driverFactors.DriverID);
                        var Driver = _context.RiskDetailDriver
                                .Include(i => i.Driver)
                                .Where(o => o.RiskDetailId == driverFactors.RiskDetailId && o.DriverId.ToString() == driverFactors.DriverID)
                                .Select(x => x.Driver)
                                .FirstOrDefault();

                        if (Driver != null)
                        {
                            // Fix for NEWCABT-783: Initially get 4 decimal places before reducing to 2 decimal places to prevent rounding errors
                            Driver.TotalFactor = decimal.Round(Convert.ToDecimal(driverFactors.TotalFactor), 4, MidpointRounding.AwayFromZero).ToString("N2");
                            Driver.AgeFactor = decimal.Round(Convert.ToDecimal(driverFactors.AgeFactor), 4, MidpointRounding.AwayFromZero).ToString("N2");
                            Driver.YearPoint = parseValueToDecimal(driverFactors.FiveYearPoints).ToString();
                            _context.SaveChanges();
                        }
                    }
                    #endregion Driver

                    #region Driver Factors
                    foreach (PremiumRaterDriverFactorsDTO driverFactors in factorsData.DriverFactors)
                    {
                        var existingDrivers = _context.DriverPremiumRatingFactor.FirstOrDefault(x => x.RiskDetailId == driverFactors.RiskDetailId && x.DriverID == driverFactors.DriverID && x.OptionId == driverFactors.OptionId);
                        if (existingDrivers == null)
                        {
                            _context.Add(_mapper.Map<DriverPremiumRatingFactor>(driverFactors));
                        }
                        else
                        {
                            existingDrivers.RiskDetailId = driverFactors.RiskDetailId;
                            existingDrivers.DriverID = driverFactors.DriverID;
                            existingDrivers.FiveYearPoints = $"{parseValueToDecimal(driverFactors.FiveYearPoints)}";
                            //existingDrivers.DriverKey = driverFactors.DriverKey;
                            //existingDrivers.PointsFactor = driverFactors.PointsFactor;
                            //existingDrivers.YearsExperienceFactor = driverFactors.YearsExperienceFactor;
                            existingDrivers.AgeFactor = $"{decimal.Round(Convert.ToDecimal(driverFactors.AgeFactor), 4, MidpointRounding.AwayFromZero):N2}";
                            //existingDrivers.OutOfStateFactor = driverFactors.OutOfStateFactor;
                            existingDrivers.TotalFactor = $"{decimal.Round(Convert.ToDecimal(driverFactors.FiveYearPoints), 4, MidpointRounding.AwayFromZero):N2}";
                            //existingDrivers.Included = driverFactors.Included;
                            //existingDrivers.TotalFactorIncl = driverFactors.TotalFactorIncl;
                            //existingDrivers.FactorIncl_AddDriver = driverFactors.FactorIncl_AddDriver;
                            //existingDrivers.UnderAge25 = driverFactors.UnderAge25;
                            //existingDrivers.UnderAge25AndClean = driverFactors.UnderAge25AndClean;
                            //existingDrivers.HasMultipleAFOrMovingViolation = driverFactors.HasMultipleAFOrMovingViolation;
                            existingDrivers.OptionId = driverFactors.OptionId;
                            _context.Update(existingDrivers);
                        }
                    }
                    #endregion Driver Factors

                    #region Risk Coverage Premium
                    var existingRiskCoveragePremium = _context.RiskCoveragePremium.FirstOrDefault(x => x.RiskCoverageId == factorsData.RiskCoverageId && x.OptionId == factorsData.OptionId);
                    if (existingRiskCoveragePremium == null)
                    {
                        decimal vehicleLevelPremium = factorsData.VehicleFactors.Sum(vehicles => vehicles.PerVehiclePremium) ?? 0.00m;
                        decimal totalPremium = parseValueToDecimal(factorsData.TotalCargoPremium) +
                                                parseValueToDecimal(factorsData.TotalLiabilityPremium) +
                                                parseValueToDecimal(factorsData.TotalPhysDamPremium) +
                                                parseValueToDecimal(factorsData.GeneralLiabilityPremium) +
                                                parseValueToDecimal(factorsData.AdditionalInsuredPremium) +
                                                parseValueToDecimal(factorsData.WaiverOfSubrogationPremium) +
                                                parseValueToDecimal(factorsData.PrimaryNonContributoryPremium);
                        decimal totalALPremium = parseValueToDecimal(factorsData.TotalLiabilityPremium) +
                                                 parseValueToDecimal(factorsData.AdditionalInsuredPremium) +
                                                 parseValueToDecimal(factorsData.WaiverOfSubrogationPremium) +
                                                 parseValueToDecimal(factorsData.PrimaryNonContributoryPremium);
                        decimal fees = parseValueToDecimal(factorsData.RiskManagementFeeAL) + parseValueToDecimal(factorsData.RiskManagementFeeAPD);

                        var riskCov = new RiskCoveragePremium()
                        {
                            RiskCoverageId = factorsData.RiskCoverageId,
                            AccountDriverFactor = parseValueToDecimal(factorsData.AccountDriverFactor),
                            AdditionalInsuredPremium = parseValueToDecimal(factorsData.AdditionalInsuredPremium),
                            AvgPerVehicle = (vehicleLevelPremium > 0 && factorsData.VehicleFactors.Count > 0) ? decimal.Round((vehicleLevelPremium / factorsData.VehicleFactors.Count), 2, MidpointRounding.AwayFromZero) : 0.00m,
                            GeneralLiabilityPremium = parseValueToDecimal(factorsData.GeneralLiabilityPremium),
                            Fees = decimal.Round(fees, 2, MidpointRounding.AwayFromZero),
                            //PremiumTaxesFees = parseValueToDecimal(factorsData.PremiumTaxesFees),
                            PrimaryNonContributoryPremium = parseValueToDecimal(factorsData.PrimaryNonContributoryPremium),
                            RiskMgrFeeAL = parseValueToDecimal(factorsData.RiskManagementFeeAL),
                            RiskMgrFeePD = parseValueToDecimal(factorsData.RiskManagementFeeAPD),
                            //SubTotalDue = parseValueToDecimal(factorsData.SubTotalDue),
                            //TotalAnnualPremiumWithPremiumTax = parseValueToDecimal(factorsData.TotalAnnualPremiumWithPremiumTax),
                            CargoPremium = parseValueToDecimal(factorsData.TotalCargoPremium),
                            AutoLiabilityPremium = decimal.Round(totalALPremium, 2, MidpointRounding.AwayFromZero),
                            //TotalOtherPolicyLevelPremium = parseValueToDecimal(factorsData.TotalOtherPolicyLevelPremium),
                            PhysicalDamagePremium = parseValueToDecimal(factorsData.TotalPhysDamPremium),
                            ComprehensivePremium = parseValueToDecimal(factorsData.ComprehensivePremium),
                            CollisionPremium = parseValueToDecimal(factorsData.CollisionPremium),
                            VehicleLevelPremium = decimal.Round(vehicleLevelPremium, 2, MidpointRounding.AwayFromZero),
                            Premium = decimal.Round(totalPremium, 2, MidpointRounding.AwayFromZero),
                            WaiverOfSubrogationPremium = parseValueToDecimal(factorsData.WaiverOfSubrogationPremium),
                            HiredPhysicalDamage = parseValueToDecimal(factorsData.HiredPhysicalDamage),
                            TrailerInterchange = parseValueToDecimal(factorsData.TrailerInterchange),
                            ALManuscriptPremium = parseValueToDecimal(factorsData.ALManuscriptPremium),
                            PDManuscriptPremium = parseValueToDecimal(factorsData.PDManuscriptPremium),
                            OptionId = factorsData.OptionId
                        };
                        _context.RiskCoveragePremium.Add(riskCov);
                    }
                    else
                    {
                        decimal vehicleLevelPremium = factorsData.VehicleFactors.Sum(vehicles => vehicles.PerVehiclePremium) ?? 0.00m;
                        decimal totalPremium = parseValueToDecimal(factorsData.TotalCargoPremium) +
                                                parseValueToDecimal(factorsData.TotalLiabilityPremium) +
                                                parseValueToDecimal(factorsData.TotalPhysDamPremium) +
                                                parseValueToDecimal(factorsData.GeneralLiabilityPremium) +
                                                parseValueToDecimal(factorsData.AdditionalInsuredPremium) +
                                                parseValueToDecimal(factorsData.WaiverOfSubrogationPremium) +
                                                parseValueToDecimal(factorsData.PrimaryNonContributoryPremium);
                        decimal totalALPremium = parseValueToDecimal(factorsData.TotalLiabilityPremium) +
                                                 parseValueToDecimal(factorsData.AdditionalInsuredPremium) +
                                                 parseValueToDecimal(factorsData.WaiverOfSubrogationPremium) +
                                                 parseValueToDecimal(factorsData.PrimaryNonContributoryPremium);
                        decimal fees = parseValueToDecimal(factorsData.RiskManagementFeeAL) + parseValueToDecimal(factorsData.RiskManagementFeeAPD);

                        existingRiskCoveragePremium.RiskCoverageId = factorsData.RiskCoverageId;
                        existingRiskCoveragePremium.AccountDriverFactor = parseValueToDecimal(factorsData.AccountDriverFactor);
                        existingRiskCoveragePremium.AdditionalInsuredPremium = parseValueToDecimal(factorsData.AdditionalInsuredPremium);
                        existingRiskCoveragePremium.AvgPerVehicle = (vehicleLevelPremium > 0 && factorsData.VehicleFactors.Count > 0) ? decimal.Round((vehicleLevelPremium / factorsData.VehicleFactors.Count), 2, MidpointRounding.AwayFromZero) : 0.00m;
                        existingRiskCoveragePremium.GeneralLiabilityPremium = parseValueToDecimal(factorsData.GeneralLiabilityPremium);
                        existingRiskCoveragePremium.Fees = decimal.Round(fees, 2, MidpointRounding.AwayFromZero);
                        //existingRiskCoveragePremium.PremiumTaxesFees = parseValueToDecimal(factorsData.PremiumTaxesFees);
                        existingRiskCoveragePremium.PrimaryNonContributoryPremium = parseValueToDecimal(factorsData.PrimaryNonContributoryPremium);
                        existingRiskCoveragePremium.RiskMgrFeeAL = parseValueToDecimal(factorsData.RiskManagementFeeAL);
                        existingRiskCoveragePremium.RiskMgrFeePD = parseValueToDecimal(factorsData.RiskManagementFeeAPD);
                        //existingRiskCoveragePremium.SubTotalDue = parseValueToDecimal(factorsData.SubTotalDue);
                        //existingRiskCoveragePremium.TotalAnnualPremiumWithPremiumTax = parseValueToDecimal(factorsData.TotalAnnualPremiumWithPremiumTax);
                        existingRiskCoveragePremium.CargoPremium = parseValueToDecimal(factorsData.TotalCargoPremium);
                        existingRiskCoveragePremium.AutoLiabilityPremium = decimal.Round(totalALPremium, 2, MidpointRounding.AwayFromZero);
                        //existingRiskCoveragePremium.TotalOtherPolicyLevelPremium = parseValueToDecimal(factorsData.TotalOtherPolicyLevelPremium);
                        existingRiskCoveragePremium.PhysicalDamagePremium = parseValueToDecimal(factorsData.TotalPhysDamPremium);
                        existingRiskCoveragePremium.ComprehensivePremium = parseValueToDecimal(factorsData.ComprehensivePremium);
                        existingRiskCoveragePremium.CollisionPremium = parseValueToDecimal(factorsData.CollisionPremium);
                        existingRiskCoveragePremium.VehicleLevelPremium = decimal.Round(vehicleLevelPremium, 2, MidpointRounding.AwayFromZero);
                        existingRiskCoveragePremium.Premium = decimal.Round(totalPremium, 2, MidpointRounding.AwayFromZero);
                        existingRiskCoveragePremium.WaiverOfSubrogationPremium = parseValueToDecimal(factorsData.WaiverOfSubrogationPremium);
                        existingRiskCoveragePremium.HiredPhysicalDamage = parseValueToDecimal(factorsData.HiredPhysicalDamage);
                        existingRiskCoveragePremium.TrailerInterchange = parseValueToDecimal(factorsData.TrailerInterchange);
                        existingRiskCoveragePremium.ALManuscriptPremium = parseValueToDecimal(factorsData.ALManuscriptPremium);
                        existingRiskCoveragePremium.PDManuscriptPremium = parseValueToDecimal(factorsData.PDManuscriptPremium);
                        existingRiskCoveragePremium.OptionId = factorsData.OptionId;
                        _context.Update<RiskCoveragePremium>(existingRiskCoveragePremium);
                    }
                    _context.SaveChanges();
                    #endregion Risk Coverage Premium

                    isFactorsSaved = true;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw new ArgumentException($"3. Error encountered while saving Rating Factors. {e.Message}\n{e.StackTrace}");
                }
                transaction.Commit();
            });

            foreach (var vehicleFactorDTO in forEndorsementVehicleFactorDTOs)
            {
                var veh = _context.Vehicle.FirstOrDefault(o => o.Id.ToString() == vehicleFactorDTO.VehicleId);
                if (veh != null)
                    _context.Entry(veh).State = EntityState.Detached;
                //veh.AutoLiabilityPremium = vehicleFactorDTO.TotalALPremium ?? 0.00m;
                //veh.CargoPremium = vehicleFactorDTO.CargoPremium ?? 0.00m;
                //veh.RefrigerationPremium = vehicleFactorDTO.RefrigerationPremium ?? 0.00m;
                //veh.PhysicalDamagePremium = vehicleFactorDTO.TotalPDPremium ?? 0.00m;
                //veh.ComprehensivePremium = (vehicleFactorDTO.CompPremium ?? 0.00m) + (vehicleFactorDTO.FireTheftPremium ?? 0.00m);
                //veh.CollisionPremium = vehicleFactorDTO.CollisionPremium ?? 0.00m;
                //veh.TotalPremium = vehicleFactorDTO.PerVehiclePremium ?? 0.00m;
                veh.Radius = vehicleFactorDTO.Radius ?? 0.00m;
                veh.RadiusTerritory = vehicleFactorDTO.RadiusTerritory ?? 0.00m;

                var vehDTO = _mapper.Map<VehicleDTO>(veh);
                vehDTO.isEndorsement = true;
                await _vehicleService.UpdateAsync(vehDTO);
            }

            return isFactorsSaved;
        }

        private decimal parseValueToDecimal(string value)
        {
            return (string.IsNullOrEmpty(value) || value == "ERROR") ? 0.00m : Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero);
        }
    }
}
