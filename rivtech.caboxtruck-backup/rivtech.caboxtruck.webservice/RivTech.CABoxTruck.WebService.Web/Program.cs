using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using RivTech.CABoxTruck.WebService.Extensions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace RivTech.CABoxTruck.WebService.Web
{
    public class Program
    {

        public static async Task Main(string[] args)
        {
            var host = await CreateHostBuilder(args).Build().MigrateDatabase();
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    var currentDirectory = Directory.GetCurrentDirectory();

                    config
                        .SetBasePath(currentDirectory)
                        .AddJsonFile("appsettings.json", false, true)
                        .AddEnvironmentVariables();

                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true);
                    if (args != null)
                        config.AddCommandLine(args);
                })

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
