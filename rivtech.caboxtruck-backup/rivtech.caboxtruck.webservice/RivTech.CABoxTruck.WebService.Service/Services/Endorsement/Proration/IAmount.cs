﻿using RivTech.CABoxTruck.WebService.Service.Services.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration
{
    public interface IAmount
    {
        AmountInfo Info { get; }
        decimal Amount { get; }
        decimal CancelledAmount { get; }
        public decimal AmountReduction { get; }
        DateTime? CancellationDate { get; }

        void Cancel(DateTime cancellationDate, bool? isFlatCancel = null);
    }
}
