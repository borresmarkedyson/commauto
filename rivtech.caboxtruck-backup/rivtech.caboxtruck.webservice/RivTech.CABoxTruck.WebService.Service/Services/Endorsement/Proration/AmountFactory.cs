﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO.Billing.Fees;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.Service.Services.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration
{
    public class AmountFactory : IAmountFactory
    {
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IVehiclePremiumRepository _vehiclePremiumRepository;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskManuscriptsRepository _riskManuscriptsRepository;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IBindingRepository _bindingRepository;
        private readonly IRiskFormRepository _riskFormRepository;
        private readonly IBillingService _billingService;

        public AmountFactory(
            IVehicleRepository vehicleRepository,
            IVehiclePremiumRepository vehiclePremiumRepository,
            IRiskDetailRepository riskDetailRepository,
            IRiskManuscriptsRepository riskManuscriptsRepository,
            IPolicyHistoryRepository policyHistoryRepository,
            IBindingRepository bindingRepository,
            IRiskFormRepository riskFormRepository,
            IBillingService billingService
        )
        {
            _vehicleRepository = vehicleRepository;
            _vehiclePremiumRepository = vehiclePremiumRepository;
            _riskDetailRepository = riskDetailRepository;
            _riskManuscriptsRepository = riskManuscriptsRepository;
            _policyHistoryRepository = policyHistoryRepository;
            _bindingRepository = bindingRepository;
            _riskFormRepository = riskFormRepository;
            _billingService = billingService;
        }

        public async Task<IAmount> CreateGeneralLiabilityAsync(RiskDetail riskDetail, DateRange policyPeriod)
        {
            var binding = await _bindingRepository.GetByRiskAsync(riskDetail.RiskId);

            var periods = new LinkedList<ProratedAmountPeriod>();
            PolicyHistory currentPolicyHistory = await _policyHistoryRepository.GetLatestByRisk(riskDetail.RiskId);
            RiskDetail currentRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(Guid.Parse(currentPolicyHistory.PreviousRiskDetailId));
            PolicyHistory previousHistory;
            DateTime currentEffectiveDate = currentPolicyHistory.EffectiveDate.Value;
            DateTime currentExpirationDate = policyPeriod.End;

            // Iterate backward
            while (currentRiskDetail != null)
            {
                RiskDetail previousRiskDetail;
                if (int.Parse(currentPolicyHistory.EndorsementNumber) > 0)
                {
                    previousHistory = await _policyHistoryRepository.GetPreviousHistory(currentPolicyHistory.Id);
                    previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(Guid.Parse(previousHistory.PreviousRiskDetailId));
                }
                else
                {
                    previousHistory = null;
                    previousRiskDetail = null;
                }

                var policyHistories = (await _policyHistoryRepository.GetByRiskDetailAsync(currentRiskDetail.Id)).ToList();
                bool policyHistoryWasCancelledOrReinstated = policyHistories.Any(x => x.PolicyStatus == RiskStatus.Canceled || x.PolicyStatus == RiskStatus.Reinstated);

                var currentRiskCov = currentRiskDetail.RiskCoverages.FirstOrDefault(x => x.OptionNumber == binding.BindOptionId);
                var previousRiskCov = previousRiskDetail?.RiskCoverages?.FirstOrDefault(x => x.OptionNumber == binding.BindOptionId);

                var currentGL = currentRiskCov.RiskCoveragePremium.GeneralLiabilityPremium ?? 0;
                var previousGL = previousRiskCov?.RiskCoveragePremium?.GeneralLiabilityPremium ?? 0;

                bool differentPremiums = (previousHistory != null && currentGL != previousGL);
                bool noPreviousRiskDetail = previousRiskDetail is null;

                ProratedAmountPeriod period = null;
                if (!policyHistoryWasCancelledOrReinstated && (differentPremiums || noPreviousRiskDetail))
                {
                    var effectivity = new DateRange(currentEffectiveDate.Date, currentExpirationDate.Date);
                    period = new ProratedAmountPeriod(currentRiskDetail.Id, currentGL,
                                    effectivity, policyPeriod,
                                    fromSubmission: noPreviousRiskDetail);
                    currentExpirationDate = currentEffectiveDate;
                }
                else if (!policyHistoryWasCancelledOrReinstated && !differentPremiums) // if there are endorsements that has no changes on General Liab Premium
                {
                    var effectivity = new DateRange(currentEffectiveDate.Date, currentExpirationDate.Date);
                    period = new ProratedAmountPeriod(currentRiskDetail.Id, currentGL,
                                    effectivity, policyPeriod,
                                    fromSubmission: noPreviousRiskDetail);
                    currentExpirationDate = currentEffectiveDate;
                }

                if (period != null && !periods.Any(x => x.Equals(period)))
                    periods.AddLast(period);

                currentEffectiveDate = previousRiskDetail?.EndorsementEffectiveDate ?? policyPeriod.Start;
                currentRiskDetail = previousRiskDetail;
                currentPolicyHistory = previousHistory;
            }

            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(riskDetail.Id, AmountKind.GeneralLiability, "General Liability"),
                periods, policyPeriod);
        }

        public async Task<IAmount> CreateManuscriptAsync(RiskManuscripts manuscript, DateRange policyPeriod)
        {
            var kind = manuscript.PremiumType == "AL" ? AmountKind.ManuscriptAL : AmountKind.ManuscriptPD;
            if (!manuscript.IsProrate)
            {
                return new FullyEarnedAmount(new AmountInfo(manuscript.CreatedDate.Ticks, kind, manuscript.Description), manuscript.Premium);
            }

            var riskDetail = await _riskDetailRepository.FindAsync(manuscript.RiskDetailId);
            var submission = await _riskDetailRepository.GetSubmissionRiskDetailAsync(riskDetail.RiskId);
            var submissionManuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == submission.Id);
            bool fromSubmission = submissionManuscripts.Any(x => x.CreatedDate.Ticks == manuscript.CreatedDate.Ticks);

            var premiumEffectiveDate = _riskManuscriptsRepository.GetEndorsementEffectiveDate(manuscript);

            var effectivity = new DateRange(premiumEffectiveDate, manuscript.ExpirationDate);
            var period = new ProratedAmountPeriod(manuscript.CreatedDate.Ticks, manuscript.Premium, effectivity, policyPeriod, fromSubmission);
            return ProratedAmount.CreateWithSinglePeriods(
                new AmountInfo(manuscript.CreatedDate.Ticks, kind, manuscript.Title),
                period, policyPeriod);
        }

        public async Task<IAmount> CreateVehicleAL(Vehicle vehicle, DateRange policyPeriod)
        {
            var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId, activeRiskDetailOnly: false);
            var periods = await CreateVehiclePeriodsAsync(vehiclePremiums, policyPeriod, AmountKind.VehicleAL);
            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(vehicle.Id, AmountKind.VehicleAL, vehicle.VIN),
                periods, policyPeriod);
        }

        public async Task<IAmount> CreateVehicleCargo(Vehicle vehicle, DateRange policyPeriod)
        {
            var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId, activeRiskDetailOnly: false);
            var periods = await CreateVehiclePeriodsAsync(vehiclePremiums, policyPeriod, AmountKind.VehicleCargo);
            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(vehicle.Id, AmountKind.VehicleCargo, vehicle.VIN),
                periods, policyPeriod);
        }

        public async Task<IAmount> CreateVehiclePDCollision(Vehicle vehicle, DateRange policyPeriod)
        {
            var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId, activeRiskDetailOnly: false);
            var periods = await CreateVehiclePeriodsAsync(vehiclePremiums, policyPeriod, AmountKind.VehiclePDCollision);
            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(vehicle.Id, AmountKind.VehiclePDCollision, vehicle.VIN),
                periods, policyPeriod);
        }

        public async Task<IAmount> CreateVehiclePDComprehensive(Vehicle vehicle, DateRange policyPeriod)
        {
            var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId, activeRiskDetailOnly: false);
            var periods = await CreateVehiclePeriodsAsync(vehiclePremiums, policyPeriod, AmountKind.VehiclePDComprehensive);
            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(vehicle.Id, AmountKind.VehiclePDComprehensive, vehicle.VIN),
                periods, policyPeriod);
        }

        public async Task<IAmount> CreateVehiclePD(Vehicle vehicle, DateRange policyPeriod)
        {
            var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId, activeRiskDetailOnly: false);
            var periods = await CreateVehiclePeriodsAsync(vehiclePremiums, policyPeriod, AmountKind.VehiclePD);
            return ProratedAmount.CreateWithManyPeriods(
                new AmountInfo(vehicle.Id, AmountKind.VehiclePD, vehicle.VIN),
                periods, policyPeriod);
        }


        private async Task<IEnumerable<ProratedAmountPeriod>> CreateVehiclePeriodsAsync(IEnumerable<VehiclePremium> vehiclePremiums, DateRange policyPeriod, AmountKind amountKind)
        {
            if (!(amountKind == AmountKind.VehicleAL
                || amountKind == AmountKind.VehiclePD
                || amountKind == AmountKind.VehicleCargo
                || amountKind == AmountKind.VehiclePDComprehensive
                || amountKind == AmountKind.VehiclePDCollision
            ))
            {
                throw new ArgumentException("Only Vehicle AL, PD, Cargo, Comp, & Collision are allowed.");
            }

            var periods = new LinkedList<ProratedAmountPeriod>();
            vehiclePremiums = vehiclePremiums.OrderByDescending(x => x.CreatedDate);

            VehiclePremium currentVehiclePremium = vehiclePremiums.First();

            DateTime currentEffectiveDate = currentVehiclePremium.EffectiveDate.Value;
            DateTime currentExpirationDate = policyPeriod.End;

            // Iterate backward
            while (currentVehiclePremium != null)
            {
                var policyHistories = await _policyHistoryRepository.GetAsync(x => x.PreviousRiskDetailId == currentVehiclePremium.RiskDetailId.ToString());
                bool policyWasCancelledAndReinstated = policyHistories.Any(x => x.PolicyStatus == RiskStatus.Canceled || x.PolicyStatus == RiskStatus.Reinstated);
                DateTime issuedDateTime = await _policyHistoryRepository.FindAsync(x => x.RiskId == policyHistories.Last().RiskId
                                                                                        && x.EndorsementNumber == "0")
                                                                        .ContinueWith((e) => e.Result.DateCreated.Value);

                bool differentPremiums =
                    (amountKind == AmountKind.VehicleAL && currentVehiclePremium.AL.Annual != currentVehiclePremium.Previous?.AL.Annual)
                    || (amountKind == AmountKind.VehiclePD && currentVehiclePremium.PD.Annual != currentVehiclePremium.Previous?.PD.Annual)
                    || (amountKind == AmountKind.VehicleCargo && currentVehiclePremium.Cargo.Annual != currentVehiclePremium.Previous?.Cargo.Annual)
                    || (amountKind == AmountKind.VehicleCargo && currentVehiclePremium.CompFT.Annual != currentVehiclePremium.Previous?.CompFT.Annual)
                    || (amountKind == AmountKind.VehicleCargo && currentVehiclePremium.Collision.Annual != currentVehiclePremium.Previous?.Collision.Annual);

                if (!policyWasCancelledAndReinstated && (differentPremiums || currentVehiclePremium.Previous is null))
                {
                    var effectivity = new DateRange(currentEffectiveDate, currentExpirationDate);
                    bool isFromSubmission = currentVehiclePremium.CreatedDate <= issuedDateTime && currentVehiclePremium.Previous is null; // all vehicle premiums created beyond issuance date will be tagged as from Endorsement

                    var annualAmount = amountKind switch
                    {
                        AmountKind.VehicleAL => currentVehiclePremium.AL.Annual,
                        AmountKind.VehiclePD => currentVehiclePremium.PD.Annual,
                        AmountKind.VehicleCargo => currentVehiclePremium.Cargo.Annual,
                        AmountKind.VehiclePDComprehensive => currentVehiclePremium.CompFT.Annual,
                        AmountKind.VehiclePDCollision => currentVehiclePremium.Collision.Annual,
                        _ => 0,
                    };

                    var period = new ProratedAmountPeriod(currentVehiclePremium.Id, annualAmount,
                                    effectivity, policyPeriod,
                                    fromSubmission: isFromSubmission);
                    periods.AddLast(period);
                    currentExpirationDate = currentEffectiveDate;
                }

                currentEffectiveDate = currentVehiclePremium?.Previous?.EffectiveDate ?? currentExpirationDate;
                currentVehiclePremium = currentVehiclePremium.Previous != null ?
                    vehiclePremiums.FirstOrDefault(x => x.Id == currentVehiclePremium.Previous.Id) : null;
            }

            return periods;
        }

        public IAmount CreateAdditionalPremium(RiskAdditionalInterest riskAdditionalInterest, AdditionalPremiumType type, bool isBlanket)
        {
            string name = (riskAdditionalInterest.Entity is null) ? "Blanket Coverage" : riskAdditionalInterest.Entity.FullName;
            return type switch
            {
                AdditionalPremiumType.AdditionalInsured => new FullyEarnedAmount(
                    new AmountInfo(riskAdditionalInterest.Id, AmountKind.AdditionalPremiumAI, name),
                    isBlanket ? 500 : 50),
                AdditionalPremiumType.WaiverOfSubrogation => new FullyEarnedAmount(
                    new AmountInfo(riskAdditionalInterest.Id, AmountKind.AdditionalPremiumWOS, name),
                    isBlanket ? 1000 : 250),
                AdditionalPremiumType.PrimaryNonContributory => new FullyEarnedAmount(
                    new AmountInfo(riskAdditionalInterest.Id, AmountKind.AdditionalPremiumPNC, name),
                    isBlanket ? 1500 : 500),
                _ => throw new ArgumentException($"Cannot create additional premium type of '{type}."),
            };
        }

        public async Task<IEnumerable<IAmount>> CreatePDPremiumsAsync(RiskDetail riskDetail)
        {
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetail.Id);
            var riskCov = riskDetail.RiskCoverages.First(x => x.OptionNumber == binding.BindOptionId);
            var riskCovPrem = riskCov.RiskCoveragePremium;
            bool hasHiredPhysicalDamage = riskForms.Any(x => x.FormId == "HiredPhysicalDamage" && (x.IsSelected ?? false));
            bool hasTrailerInterchange = riskForms.Any(x => x.FormId == "TrailerInterchangeCoverage" && (x.IsSelected ?? false));

            var amounts = new List<IAmount>();

            var hiredPd = riskCovPrem.HiredPhysicalDamage;
            if ((hiredPd ?? 0) > 0 && hasHiredPhysicalDamage)
            {
                amounts.Add(new FullyEarnedAmount(new AmountInfo(riskDetail.Id, AmountKind.PDHired, "Hired PD"), hiredPd.Value));
            }

            var trailerInterchange = riskCovPrem.TrailerInterchange;
            if ((trailerInterchange ?? 0) > 0 && hasTrailerInterchange)
            {
                amounts.Add(new FullyEarnedAmount(new AmountInfo(riskDetail.Id, AmountKind.PDTrailer, "Trailer Interchange"), trailerInterchange.Value));
            }

            return amounts;
        }

        public async Task<IEnumerable<IAmount>> CreateVehicleFees(Vehicle vehicle, RiskDetail riskDetail)
        {
            var amounts = new List<IAmount>();
            var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskId == riskDetail.RiskId.ToString() && x.PolicyStatus == RiskStatus.Active);
            policyHistories = policyHistories.OrderByDescending(x => x.DateCreated);
            var lastActiveRiskDetail = policyHistories.First();

            var lastIssuedRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(Guid.Parse(lastActiveRiskDetail.PreviousRiskDetailId));
            var riskCovPrem = lastIssuedRiskDetail.RiskCoverages.First().RiskCoveragePremium;

            decimal riskManagementFeeAL = (riskCovPrem.RiskMgrFeeAL ?? 0) / lastIssuedRiskDetail.Vehicles.Count;
            var sfAL = new FullyEarnedAmount(new AmountInfo(vehicle.Id, AmountKind.ServiceFeeAL, vehicle.VIN), riskManagementFeeAL);
            amounts.Add(sfAL);

            decimal riskManagementFeePD = (riskCovPrem.RiskMgrFeePD ?? 0) / lastIssuedRiskDetail.Vehicles.Count;
            var sfPD = new FullyEarnedAmount(new AmountInfo(vehicle.Id, AmountKind.ServiceFeePD, vehicle.VIN), riskManagementFeePD);
            amounts.Add(sfPD);

            return amounts;
        }


    }
}
