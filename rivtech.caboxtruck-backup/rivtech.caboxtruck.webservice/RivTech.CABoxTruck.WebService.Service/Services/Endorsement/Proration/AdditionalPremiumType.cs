﻿namespace RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration
{
    public enum AdditionalPremiumType
    {
        AdditionalInsured = 1,
        WaiverOfSubrogation = 2,
        PrimaryNonContributory= 3
    }
}
