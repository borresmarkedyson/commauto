﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Service.Services.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration
{
    public interface IAmountFactory
    {
        Task<IAmount> CreateVehicleAL(Vehicle vehicle, DateRange policyPeriod);
        Task<IAmount> CreateVehiclePD(Vehicle vehicle, DateRange policyPeriod);
        Task<IAmount> CreateVehiclePDCollision(Vehicle vehicle, DateRange policyPeriod);
        Task<IAmount> CreateVehiclePDComprehensive(Vehicle vehicle, DateRange policyPeriod);
        Task<IAmount> CreateVehicleCargo(Vehicle vehicle, DateRange policyPeriod);
        Task<IAmount> CreateGeneralLiabilityAsync(RiskDetail riskDetail, DateRange policyPeriod);
        Task<IAmount> CreateManuscriptAsync(RiskManuscripts manuscript, DateRange policyPeriod);
        IAmount CreateAdditionalPremium(RiskAdditionalInterest riskAdditionalInterest, AdditionalPremiumType type, bool isBlanket);

        Task<IEnumerable<IAmount>> CreatePDPremiumsAsync(RiskDetail riskDetail);
        Task<IEnumerable<IAmount>> CreateVehicleFees(Vehicle vehicle, RiskDetail riskDetail);
    }
}
