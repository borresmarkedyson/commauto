﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Policy;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants;
using RivTech.CABoxTruck.WebService.Service.Services.Endorsement.PremiumChanges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class EndorsementService : IEndorsementService
    {
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IBindingRepository _bindingRepository;
        private readonly IRiskManuscriptsRepository _riskManuscriptsRepository;
        private readonly IRiskFormRepository _riskFormRepository;
        private readonly IEndorsementPendingChangeRepository _endorsementPendingChangeRepository;
        private readonly IBindingService _bindingService;
        private readonly ITaxService _taxService;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IRiskCoverageRepository _riskCoverageRepository;
        private readonly IVehiclePremiumRepository _vehiclePremiumRepository;
        private readonly IRiskSequenceService _riskSequence;
        private readonly IMapper _mapper;
        private readonly ILog4NetService _errorLogservice;

        public EndorsementService(
            IRiskDetailRepository riskDetailRepository,
            IBindingRepository bindingRepository,
            IRiskManuscriptsRepository riskManuscriptsRepository,
            IRiskFormRepository riskFormRepository,
            IEndorsementPendingChangeRepository endorsementPendingChangeRepository,
            IBindingService bindingService,
            ITaxService taxService,
            IVehicleRepository vehicleRepository,
            IRiskRepository riskRepository,
            IBrokerInfoRepository brokerInfoRepository,
            IPolicyHistoryRepository policyHistoryRepository,
            IRiskCoverageRepository riskCoverageRepository,
            IVehiclePremiumRepository vehiclePremiumRepository,
            IRiskSequenceService riskSequence,
            IMapper mapper,
            ILog4NetService errorLogservice)
        {
            _mapper = mapper;
            _errorLogservice = errorLogservice;
            _riskDetailRepository = riskDetailRepository;
            _bindingRepository = bindingRepository;
            _riskManuscriptsRepository = riskManuscriptsRepository;
            _riskFormRepository = riskFormRepository;
            _vehicleRepository = vehicleRepository;
            _endorsementPendingChangeRepository = endorsementPendingChangeRepository;
            _brokerInfoRepository = brokerInfoRepository;
            _policyHistoryRepository = policyHistoryRepository;
            _riskCoverageRepository = riskCoverageRepository;
            _vehiclePremiumRepository = vehiclePremiumRepository;
            _riskSequence = riskSequence;
            _bindingService = bindingService;
            _taxService = taxService;
            _riskRepository = riskRepository;
        }

        public async Task SetServiceFeeChanges(RiskDetail currentRiskDetail)
        {
            var currentRiskDetailId = currentRiskDetail.Id;
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == currentRiskDetail.RiskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var bindData = await _bindingService.GetBindingByRiskIdAsync(currentRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var currentRiskCoverage = currentRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var previousSFAL = previousRiskCoverage.RiskCoveragePremium.RiskMgrFeeAL ?? 0;
            var previousSFPD = previousRiskCoverage.RiskCoveragePremium.RiskMgrFeePD ?? 0;
            var currentSFAL = currentRiskCoverage.RiskCoveragePremium.RiskMgrFeeAL ?? 0;
            var currentSFPD = currentRiskCoverage.RiskCoveragePremium.RiskMgrFeePD ?? 0;

            //currentSFAL = (currentSFAL < previousSFAL) ? previousSFAL : currentSFAL; // Service Fee should only log for Added Vehicles only
            //currentSFPD = (currentSFPD < previousSFPD) ? previousSFPD : currentSFPD; // Service Fee should only log for Added Vehicles only

            var pendingChangesAL = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.ServiceFeeAL);
            if (pendingChangesAL != null)
            {
                pendingChangesAL.PremiumBeforeEndorsement = previousSFAL;
                pendingChangesAL.PremiumAfterEndorsement = currentSFAL;
                pendingChangesAL.PremiumDifference = FindDifference(currentSFAL, previousSFAL);
                pendingChangesAL.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesAL);
            }
            else
            {
                var newPendingChangesAL = new EndorsementPendingChange();
                newPendingChangesAL.RiskDetailId = currentRiskDetailId;
                newPendingChangesAL.AmountSubTypeId = BillingConstants.ServiceFeeAL;
                newPendingChangesAL.PremiumBeforeEndorsement = previousSFAL;
                newPendingChangesAL.PremiumAfterEndorsement = currentSFAL;
                newPendingChangesAL.PremiumDifference = FindDifference(currentSFAL, previousSFAL);
                newPendingChangesAL.DateCreated = DateTime.Now;
                newPendingChangesAL.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesAL);
            }

            var pendingChangesPD = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.ServiceFeePD);
            if (pendingChangesPD != null)
            {
                pendingChangesPD.PremiumBeforeEndorsement = previousSFPD;
                pendingChangesPD.PremiumAfterEndorsement = currentSFPD;
                pendingChangesPD.PremiumDifference = FindDifference(currentSFPD, previousSFPD);
                pendingChangesPD.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesPD);
            }
            else
            {
                var newPendingChangesPD = new EndorsementPendingChange();
                newPendingChangesPD.RiskDetailId = currentRiskDetailId;
                newPendingChangesPD.AmountSubTypeId = BillingConstants.ServiceFeePD;
                newPendingChangesPD.PremiumBeforeEndorsement = previousSFPD;
                newPendingChangesPD.PremiumAfterEndorsement = currentSFPD;
                newPendingChangesPD.PremiumDifference = FindDifference(currentSFPD, previousSFPD);
                newPendingChangesPD.DateCreated = DateTime.Now;
                newPendingChangesPD.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesPD);
            }

            await _endorsementPendingChangeRepository.SaveChangesAsync();
        }

        public async Task SetTaxesChanges(RiskDetail currentRiskDetail, DateTime effectiveDate)
        {
            var currentRiskDetailId = currentRiskDetail.Id;
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == currentRiskDetail.RiskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var bindData = await _bindingService.GetBindingByRiskIdAsync(currentRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var currentRiskCoverage = currentRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();

            var pendingChanges = await _endorsementPendingChangeRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);

            var (taxBefore, taxAfter) = await _taxService.CalculateTaxPremiumChangesAsync(currentRiskDetail.Id, effectiveDate, currentRiskCoverage.NumberOfInstallments ?? 0, pendingChanges.ToList());

            var pendingChangesTaxSL = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.SurplusLinesTax);
            if (pendingChangesTaxSL != null)
            {
                pendingChangesTaxSL.PremiumBeforeEndorsement = taxBefore.TotalSLTax;
                pendingChangesTaxSL.PremiumAfterEndorsement = taxAfter.TotalSLTax;
                pendingChangesTaxSL.PremiumDifference = FindDifference(taxAfter.TotalSLTax, taxBefore.TotalSLTax);
                pendingChangesTaxSL.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesTaxSL);
            }
            else
            {
                var newPendingChangesTaxSL = new EndorsementPendingChange();
                newPendingChangesTaxSL.RiskDetailId = currentRiskDetailId;
                newPendingChangesTaxSL.AmountSubTypeId = BillingConstants.SurplusLinesTax;
                newPendingChangesTaxSL.PremiumBeforeEndorsement = taxBefore.TotalSLTax;
                newPendingChangesTaxSL.PremiumAfterEndorsement = taxAfter.TotalSLTax;
                newPendingChangesTaxSL.PremiumDifference = FindDifference(taxAfter.TotalSLTax, taxBefore.TotalSLTax);
                newPendingChangesTaxSL.DateCreated = DateTime.Now;
                newPendingChangesTaxSL.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesTaxSL);
            }

            var pendingChangesTaxSF = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.StampingFee);
            if (pendingChangesTaxSF != null)
            {
                pendingChangesTaxSF.PremiumBeforeEndorsement = taxBefore.TotalStampingFee;
                pendingChangesTaxSF.PremiumAfterEndorsement = taxAfter.TotalStampingFee;
                pendingChangesTaxSF.PremiumDifference = FindDifference(taxAfter.TotalStampingFee, taxBefore.TotalStampingFee);
                pendingChangesTaxSF.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesTaxSF);
            }
            else
            {
                var newPendingChangesTaxSF = new EndorsementPendingChange();
                newPendingChangesTaxSF.RiskDetailId = currentRiskDetailId;
                newPendingChangesTaxSF.AmountSubTypeId = BillingConstants.StampingFee;
                newPendingChangesTaxSF.PremiumBeforeEndorsement = taxBefore.TotalStampingFee;
                newPendingChangesTaxSF.PremiumAfterEndorsement = taxAfter.TotalStampingFee;
                newPendingChangesTaxSF.PremiumDifference = FindDifference(taxAfter.TotalStampingFee, taxBefore.TotalStampingFee);
                newPendingChangesTaxSF.DateCreated = DateTime.Now;
                newPendingChangesTaxSF.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesTaxSF);
            }

            await _endorsementPendingChangeRepository.SaveChangesAsync();
        }

        public async Task SetAdditionalPremiumChanges(RiskDetail currentRiskDetail)
        {
            var currentRiskDetailId = currentRiskDetail.Id;
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == currentRiskDetail.RiskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var bindData = await _bindingService.GetBindingByRiskIdAsync(currentRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var currentRiskCoverage = currentRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var prevAddtl = previousRiskCoverage.RiskCoveragePremium.AdditionalInsuredPremium ?? 0;
            var prevWaiver = previousRiskCoverage.RiskCoveragePremium.WaiverOfSubrogationPremium ?? 0;
            var prevPnc = previousRiskCoverage.RiskCoveragePremium.PrimaryNonContributoryPremium ?? 0;
            var prevTrailer = previousRiskCoverage.RiskCoveragePremium.TrailerInterchange ?? 0;
            var prevHired = previousRiskCoverage.RiskCoveragePremium.HiredPhysicalDamage ?? 0;

            var curAddtl = currentRiskCoverage.RiskCoveragePremium.AdditionalInsuredPremium ?? 0;
            var curWaiver = currentRiskCoverage.RiskCoveragePremium.WaiverOfSubrogationPremium ?? 0;
            var curPnc = currentRiskCoverage.RiskCoveragePremium.PrimaryNonContributoryPremium ?? 0;
            var curTrailer = currentRiskCoverage.RiskCoveragePremium.TrailerInterchange ?? 0;
            var curHired = currentRiskCoverage.RiskCoveragePremium.HiredPhysicalDamage ?? 0;

            // Additional Insured
            var pendingChangesAddtl = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.AdditionalInsured);
            if (pendingChangesAddtl != null)
            {
                pendingChangesAddtl.PremiumBeforeEndorsement = prevAddtl;
                pendingChangesAddtl.PremiumAfterEndorsement = curAddtl;
                pendingChangesAddtl.PremiumDifference = FindDifference(curAddtl, prevAddtl);
                pendingChangesAddtl.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesAddtl);
            }
            else
            {
                var newPendingChangesAddtl = new EndorsementPendingChange();
                newPendingChangesAddtl.RiskDetailId = currentRiskDetailId;
                newPendingChangesAddtl.AmountSubTypeId = BillingConstants.AdditionalInsured;
                newPendingChangesAddtl.PremiumBeforeEndorsement = prevAddtl;
                newPendingChangesAddtl.PremiumAfterEndorsement = curAddtl;
                newPendingChangesAddtl.PremiumDifference = FindDifference(curAddtl, prevAddtl);
                newPendingChangesAddtl.DateCreated = DateTime.Now;
                newPendingChangesAddtl.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesAddtl);
            }

            // Waiver of Subrogation
            var pendingChangesWaiver = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation);
            if (pendingChangesWaiver != null)
            {
                pendingChangesWaiver.PremiumBeforeEndorsement = prevWaiver;
                pendingChangesWaiver.PremiumAfterEndorsement = curWaiver;
                pendingChangesWaiver.PremiumDifference = FindDifference(curWaiver, prevWaiver);
                pendingChangesWaiver.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesWaiver);
            }
            else
            {
                var newPendingChangesWaiver = new EndorsementPendingChange();
                newPendingChangesWaiver.RiskDetailId = currentRiskDetailId;
                newPendingChangesWaiver.AmountSubTypeId = BillingConstants.WaiverOfSubrogation;
                newPendingChangesWaiver.PremiumBeforeEndorsement = prevWaiver;
                newPendingChangesWaiver.PremiumAfterEndorsement = curWaiver;
                newPendingChangesWaiver.PremiumDifference = FindDifference(curWaiver, prevWaiver);
                newPendingChangesWaiver.DateCreated = DateTime.Now;
                newPendingChangesWaiver.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesWaiver);
            }

            // Primary Non Contributory
            var pendingChangesPnc = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory);
            if (pendingChangesPnc != null)
            {
                pendingChangesPnc.PremiumBeforeEndorsement = prevPnc;
                pendingChangesPnc.PremiumAfterEndorsement = curPnc;
                pendingChangesPnc.PremiumDifference = FindDifference(curPnc, prevPnc);
                pendingChangesPnc.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesPnc);
            }
            else
            {
                var newPendingChangesPnc = new EndorsementPendingChange();
                newPendingChangesPnc.RiskDetailId = currentRiskDetailId;
                newPendingChangesPnc.AmountSubTypeId = BillingConstants.PrimaryAndNoncontributory;
                newPendingChangesPnc.PremiumBeforeEndorsement = prevPnc;
                newPendingChangesPnc.PremiumAfterEndorsement = curPnc;
                newPendingChangesPnc.PremiumDifference = FindDifference(curPnc, prevPnc);
                newPendingChangesPnc.DateCreated = DateTime.Now;
                newPendingChangesPnc.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesPnc);
            }

            // Trailer Interchange
            var pendingChangesTrailer = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.TrailerInterchangeEndst);
            if (pendingChangesTrailer != null)
            {
                pendingChangesTrailer.PremiumBeforeEndorsement = prevTrailer;
                pendingChangesTrailer.PremiumAfterEndorsement = curTrailer;
                pendingChangesTrailer.PremiumDifference = FindDifference(curTrailer, prevTrailer);
                pendingChangesTrailer.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesTrailer);
            }
            else
            {
                var newPendingChangesTrailer = new EndorsementPendingChange();
                newPendingChangesTrailer.RiskDetailId = currentRiskDetailId;
                newPendingChangesTrailer.AmountSubTypeId = BillingConstants.TrailerInterchangeEndst;
                newPendingChangesTrailer.PremiumBeforeEndorsement = prevTrailer;
                newPendingChangesTrailer.PremiumAfterEndorsement = curTrailer;
                newPendingChangesTrailer.PremiumDifference = FindDifference(curTrailer, prevTrailer);
                newPendingChangesTrailer.DateCreated = DateTime.Now;
                newPendingChangesTrailer.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesTrailer);
            }

            // Hired Physical
            var pendingChangesHired = await _endorsementPendingChangeRepository.FindAsync(x => x.RiskDetailId == currentRiskDetailId &&
                    x.AmountSubTypeId == BillingConstants.HiredPhysicalDamageEndst);
            if (pendingChangesHired != null)
            {
                pendingChangesHired.PremiumBeforeEndorsement = prevHired;
                pendingChangesHired.PremiumAfterEndorsement = curHired;
                pendingChangesHired.PremiumDifference = FindDifference(curHired, prevHired);
                pendingChangesHired.DateCreated = DateTime.Now;
                _endorsementPendingChangeRepository.Update(pendingChangesHired);
            }
            else
            {
                var newPendingChangesHired = new EndorsementPendingChange();
                newPendingChangesHired.RiskDetailId = currentRiskDetailId;
                newPendingChangesHired.AmountSubTypeId = BillingConstants.HiredPhysicalDamageEndst;
                newPendingChangesHired.PremiumBeforeEndorsement = prevHired;
                newPendingChangesHired.PremiumAfterEndorsement = curHired;
                newPendingChangesHired.PremiumDifference = FindDifference(curHired, prevHired);
                newPendingChangesHired.DateCreated = DateTime.Now;
                newPendingChangesHired.CreatedBy = Data.Common.Services.GetCurrentUser().ToString();
                await _endorsementPendingChangeRepository.AddAsync(newPendingChangesHired);
            }

            await _endorsementPendingChangeRepository.SaveChangesAsync();
        }

        public async Task UpdateEffectiveDate(Guid riskDetailId, DateTime effectiveDate)
        {
            var riskDetail = await _riskDetailRepository.FindAsync(x => x.Id == riskDetailId);
            riskDetail.EndorsementEffectiveDate = effectiveDate;

            var vehicles = await _vehicleRepository.GetByRiskDetailAsync(riskDetailId, true);
            var previousVehicles = await _vehicleRepository.GetPreviousVehiclesByRiskDetail(riskDetail);
            foreach (var vehicle in vehicles)
            {
                bool isNewlyAdded = !previousVehicles.Any(x => x.Id == vehicle.Id) && vehicle.PreviousVehicleVersionId is null;
                bool isPreviouslyDeleted = previousVehicles.FirstOrDefault(x => x.Id == vehicle.PreviousVehicleVersionId)?.DeletedDate != null;
                bool justReinstated = isPreviouslyDeleted && !vehicle.IsDeleted;
                bool justDeleted = !isPreviouslyDeleted && vehicle.IsDeleted;
                bool justUpdated = !previousVehicles.Any(x => x.Id == vehicle.Id) && vehicle.PreviousVehicleVersionId != null
                                   && !(justReinstated || justDeleted);

                if (isNewlyAdded || justReinstated || justUpdated)
                {
                    vehicle.EffectiveDate = riskDetail.EndorsementEffectiveDate;
                }
                else if (justDeleted)
                {
                    vehicle.Delete(riskDetail.EndorsementEffectiveDate);
                }
            }


            await _riskCoverageRepository.SaveChangesAsync();
        }

        public async Task<int> GetNextEndorsementNumber(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.FindAsync(riskDetailId);
            var history = await _policyHistoryRepository.GetLatestByRisk(riskDetail.RiskId);
            return history == null ? 0 : (int.Parse(history.EndorsementNumber) + 1);
        }

        public async Task<EndorsementPremiumChangeSummaryDTO> GetPremiumChanges(Guid riskDetailId)
        {
            var result = new EndorsementPremiumChangeSummaryDTO();
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var coverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();
            var pendingChanges = _endorsementPendingChangeRepository.GetAsync(x => x.RiskDetailId == riskDetailId).Result.ToList();
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            var riskManuscript = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            var taxDetails = await _taxService.GetTaxDetails(riskDetail.RiskCoverages.Select(rc => rc.RiskCoveragePremium?.TaxDetailsId ?? Guid.Empty).ToList());

            // Previous RiskDetail
            var previousRiskDetails = await _riskDetailRepository.GetAsync(rd => rd.RiskId == riskDetail.RiskId && rd.Id != riskDetailId && rd.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var isPreviousRiskDetailSubmission = previousRiskDetails.Last().Id == previousRiskDetailId;
            var previousPendingChanges = _endorsementPendingChangeRepository.GetByRiskDetail(previousRiskDetailId);
            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();
            // policy history
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);
            var policyHistory = await _policyHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            var lastEndorsementDate = policyHistory.OrderByDescending(x => x.DateCreated).Select(x => x.DateCreated).First() ?? brokerInfo.EffectiveDate;
            var lastEndorsementEffectiveDate = policyHistory.OrderByDescending(x => x.DateCreated).Select(x => x.EffectiveDate).First() ?? brokerInfo.EffectiveDate;
            var previousCoverage = previousRiskDetail.RiskCoverages.FirstOrDefault(x => x.OptionNumber == binding.BindOptionId);

            // Vehicle
            var previousVehicles = await _vehiclePremiumRepository.GetByRiskDetail(previousRiskDetailId);
            // previousVehicles = previousVehicles.Where(v => v.Options.Contains(binding.BindOptionId.ToString()));
            var currentVehicles = await _vehiclePremiumRepository.GetByRiskDetail(riskDetailId);
            // currentVehicles = currentVehicles.Where(v => v.Options.Contains(binding.BindOptionId.ToString())).ToList();

            var totalVehicleAL = currentVehicles.Sum(x => x.AL.Annual.RoundTo2Decimals());
            var totalVehiclePD = currentVehicles.Sum(x => x.PD.Annual.RoundTo2Decimals());
            var totalVehicleCP = currentVehicles.Sum(x => x.Cargo.Annual.RoundTo2Decimals());

            var HasCargo = coverage.CargoLimitId > LimitConstants.CargoNoneCoverageId;
            var totalVehicle = totalVehicleAL +
                                totalVehiclePD +
                                 totalVehicleCP;

            result.VehicleChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = "Vehicle",
                CurrentPremium = totalVehicle,
                EndorsementPendingChange = SetEndorsementPendingChange("Vehicle", pendingChanges)
            });
            result.VehicleChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = Padding(25, "Auto Liability Premium"),
                CurrentPremium = totalVehicleAL,
                EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.VehicleALP, pendingChanges)
            });

            var pdpChanges = SetEndorsementPendingChange(BillingConstants.VehiclePDP, pendingChanges);
            var pdp = totalVehiclePD;
            if (pdp != 0 || pdpChanges.PremiumDifference != 0)
            {
                result.VehicleChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "Physical Damage Premium"),
                    CurrentPremium = pdp,
                    EndorsementPendingChange = pdpChanges
                });
            }

            //if (HasCargo) {}
            var cargoChanges = SetEndorsementPendingChange(BillingConstants.VehicleCP, pendingChanges);
            var motorCargoPremium = totalVehicleCP;
            if (motorCargoPremium != 0 || cargoChanges.PremiumDifference != 0)
            {
                result.VehicleChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "Motortruck Cargo Premium"),
                    CurrentPremium = motorCargoPremium,
                    EndorsementPendingChange = cargoChanges
                });
            }

            // Policy
            var endorsementEffectiveDate = (riskDetail.EndorsementEffectiveDate ?? DateTime.Now).Date;
            var hasApd = coverage.CompDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                         || coverage.FireDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                         || coverage.CollDeductibleId > LimitConstants.CollisionNoCoverageId;
            var hasMtc = coverage.CargoLimitId > LimitConstants.CargoNoneCoverageId;
            //var glPremium = ((coverage.RiskCoveragePremium?.GeneralLiabilityPremium ?? 0) == 0
            //                 && previousRiskCoverage.RiskCoveragePremium.GeneralLiabilityPremium > 0)
            //                    ? previousRiskCoverage.RiskCoveragePremium.GeneralLiabilityPremium
            //                    : coverage.RiskCoveragePremium.GeneralLiabilityPremium;
            var additionalPremium = (coverage.RiskCoveragePremium?.AdditionalInsuredPremium ?? 0) +
                                    (coverage.RiskCoveragePremium?.WaiverOfSubrogationPremium ?? 0) +
                                    (coverage.RiskCoveragePremium?.PrimaryNonContributoryPremium ?? 0);
            var alManuscriptPremium = riskManuscript.Where(m => (!m.IsDeleted || (m.ExpirationDate >= lastEndorsementEffectiveDate && !m.IsFlatCancelled && m.DeletedDate >= lastEndorsementDate && m.EffectiveDate < endorsementEffectiveDate)) && m.PremiumType == "AL").Sum(m => m.Premium);
            var pdManuscriptPremium = riskManuscript.Where(m => (!m.IsDeleted || (m.ExpirationDate >= lastEndorsementEffectiveDate && !m.IsFlatCancelled && m.DeletedDate >= lastEndorsementDate && m.EffectiveDate < endorsementEffectiveDate)) && m.PremiumType == "PD").Sum(m => m.Premium);
            var hiredPhysdamOtherContent = riskForms.FirstOrDefault(f => f.IsSelected.GetValueOrDefault() && f.FormId == "HiredPhysicalDamage")?.Other;
            var hiredPhysdamPremium = coverage.RiskCoveragePremium?.HiredPhysicalDamage ?? 0;
            var trailerOtherContent = riskForms.FirstOrDefault(f => f.IsSelected.GetValueOrDefault() && f.FormId == "TrailerInterchangeCoverage")?.Other;
            var premiumsSum = coverage.RiskCoveragePremium?.TrailerInterchange ?? 0;
            var glPremium = coverage.RiskCoveragePremium?.GeneralLiabilityPremium ?? 0;
            var totalPolicy = additionalPremium + glPremium + alManuscriptPremium +
                        pdManuscriptPremium + hiredPhysdamPremium + premiumsSum;

            result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = "Policy",
                CurrentPremium = totalPolicy,
                EndorsementPendingChange = SetEndorsementPendingChange("Policy", pendingChanges)
            });

            var attlInsuredChange = SetEndorsementPendingChange(BillingConstants.AdditionalInsured, pendingChanges);
            var waiveChange = SetEndorsementPendingChange(BillingConstants.WaiverOfSubrogation, pendingChanges);
            var primaryChange = SetEndorsementPendingChange(BillingConstants.PrimaryAndNoncontributory, pendingChanges);
            var attpP = coverage.RiskCoveragePremium?.AdditionalInsuredPremium ?? 0;
            var waivPre = coverage.RiskCoveragePremium?.WaiverOfSubrogationPremium ?? 0;
            var primaryCont = coverage.RiskCoveragePremium?.PrimaryNonContributoryPremium ?? 0;

            //if ()
            if (additionalPremium != 0 ||
                (attpP != 0 || attlInsuredChange.PremiumDifference != 0) ||
                (waivPre != 0 || waiveChange.PremiumDifference != 0) ||
                (primaryCont != 0 || primaryChange.PremiumDifference != 0))
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "Additional Premium (Fully Earned)"),
                    CurrentPremium = additionalPremium,
                    EndorsementPendingChange = SetEndorsementPendingChange("Additional Premium (Fully Earned)", pendingChanges)
                });

                if (attpP != 0 || attlInsuredChange.PremiumDifference != 0)
                {
                    result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                    {
                        Description = Padding(50, "Additional Insured"),
                        CurrentPremium = attpP,
                        EndorsementPendingChange = attlInsuredChange
                    });
                }

                if (waivPre != 0 || waiveChange.PremiumDifference != 0)
                {
                    result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                    {
                        Description = Padding(50, "Waiver of Subrogation"),
                        CurrentPremium = waivPre,
                        EndorsementPendingChange = waiveChange
                    });
                }

                if (primaryCont != 0 || primaryChange.PremiumDifference != 0)
                {
                    result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                    {
                        Description = Padding(50, "Primary & Non-Contributory"),
                        CurrentPremium = primaryCont,
                        EndorsementPendingChange = primaryChange
                    });
                }
            }

            //if (hasApd || hasMtc) {}
            var glPremiumChange = SetEndorsementPendingChange(BillingConstants.GeneralLiabilityPremium, pendingChanges);
            if (glPremium != 0 || glPremiumChange.PremiumDifference != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "General Liability Premium"),
                    CurrentPremium = glPremium,
                    EndorsementPendingChange = glPremiumChange
                });
            }
            else if ((previousRiskCoverage.RiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) > 0 && (coverage.RiskCoveragePremium?.GeneralLiabilityPremium ?? 0) == 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "General Liability Premium"),
                    CurrentPremium = 0,
                    EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.GeneralLiabilityPremium, pendingChanges)
                });
            }

            var alManuscriptPremiumChange = SetEndorsementPendingChange(BillingConstants.ALManuscriptPremium, pendingChanges);
            var isAllManuscriptALDeletedButHaveProrateAmt = riskManuscript.Where(x => x.IsActive && x.ProratedPremium != 0 && x.PremiumType == "AL").Count() == 0;
            if (alManuscriptPremium != 0 || alManuscriptPremiumChange.PremiumDifference != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "AL Manuscript Premium"),
                    CurrentPremium = alManuscriptPremium,
                    EndorsementPendingChange = alManuscriptPremiumChange
                });
            }
            else if (isAllManuscriptALDeletedButHaveProrateAmt && riskManuscript.Count(x => x.PremiumType == "AL") > 0 && alManuscriptPremiumChange.PremiumBeforeEndorsement != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "AL Manuscript Premium"),
                    CurrentPremium = 0,
                    EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.ALManuscriptPremium, pendingChanges)
                });
            }

            var pdManuscriptPremiumChange = SetEndorsementPendingChange(BillingConstants.PDManuscriptPremium, pendingChanges);
            var isAllManuscriptPDDeletedButHaveProrateAmt = riskManuscript.Where(x => x.IsActive && x.ProratedPremium != 0 && x.PremiumType == "PD").Count() == 0;
            if (pdManuscriptPremium != 0 || pdManuscriptPremiumChange.PremiumDifference != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "PD Manuscript Premium"),
                    CurrentPremium = pdManuscriptPremium,
                    EndorsementPendingChange = pdManuscriptPremiumChange
                });
            }
            else if (isAllManuscriptPDDeletedButHaveProrateAmt && riskManuscript.Count(x => x.PremiumType == "PD") > 0 && pdManuscriptPremiumChange.PremiumBeforeEndorsement != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "PD Manuscript Premium"),
                    CurrentPremium = 0,
                    EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.PDManuscriptPremium, pendingChanges)
                });
            }


            //if (hiredPhysdamOtherContent != null) {}
            var hiredPhysdamChange = SetEndorsementPendingChange(BillingConstants.HiredPhysicalDamageEndst, pendingChanges);
            if (hiredPhysdamPremium != 0 || hiredPhysdamChange.PremiumDifference != 0)
            {
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "PD Hired Physical Damage"),
                    CurrentPremium = hiredPhysdamPremium,
                    EndorsementPendingChange = hiredPhysdamChange
                });
            }

            var trailerOtherChange = SetEndorsementPendingChange(BillingConstants.TrailerInterchangeEndst, pendingChanges);
            if (premiumsSum != 0 || trailerOtherChange.PremiumDifference != 0)
            {
                //if (premiumsSum != 0) {}
                result.PolicyChangeList.Add(new EndorsementPremiumChangeDTO()
                {
                    Description = Padding(25, "PD Trailer Interchange"),
                    CurrentPremium = premiumsSum,
                    EndorsementPendingChange = trailerOtherChange
                });
            }

            // Total Premium (!m.IsDeleted || (m.ExpirationDate >= lastEndorsementEffectiveDate && !m.IsFlatCancelled ))
            var deletedProratedManuscriptPremium = riskManuscript.Where(x => x.IsDeleted && x.IsProrate && x.ProratedPremium != 0
                                                                            && x.ExpirationDate >= lastEndorsementEffectiveDate
                                                                            && x.DeletedDate >= lastEndorsementDate).Sum(x => x.Premium); // include annual premium of deleted prorated manuscripts
            result.TotalPremiumChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = "Total Premium",
                CurrentPremium = coverage.RiskCoveragePremium?.Premium + deletedProratedManuscriptPremium,
                EndorsementPendingChange = SetEndorsementPendingChange("Total Premium", pendingChanges)
            });

            var taxDetail = taxDetails.FirstOrDefault(td => td.Id == coverage.RiskCoveragePremium?.TaxDetailsId);
            result.TotalPremiumChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = Padding(25, "Surplus Lines Tax"),
                CurrentPremium = taxDetail?.TotalSLTax ?? 0,
                EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.SurplusLinesTax, pendingChanges)
            });
            result.TotalPremiumChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = Padding(25, "Stamping Fee"),
                CurrentPremium = taxDetail?.TotalStampingFee ?? 0,
                EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.StampingFee, pendingChanges)
            });

            // Fees
            decimal? totalFees = (coverage.RiskCoveragePremium?.RiskMgrFeeAL ?? 0) + (coverage.RiskCoveragePremium?.RiskMgrFeePD ?? 0);
            result.FeesChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = "Fees (Fully Earned)",
                CurrentPremium = totalFees ?? 0,
                EndorsementPendingChange = SetEndorsementPendingChange("Fees (Fully Earned)", pendingChanges)
            });
            result.FeesChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = Padding(25, "Service Fee - AL"),
                CurrentPremium = ((coverage.RiskCoveragePremium?.RiskMgrFeeAL ?? 0) < (previousRiskCoverage?.RiskCoveragePremium?.RiskMgrFeeAL ?? 0)) ? previousRiskCoverage.RiskCoveragePremium?.RiskMgrFeeAL ?? 0 : coverage.RiskCoveragePremium?.RiskMgrFeeAL ?? 0,
                EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.ServiceFeeAL, pendingChanges)
            });
            result.FeesChangeList.Add(new EndorsementPremiumChangeDTO()
            {
                Description = Padding(25, "Service Fee - PD"),
                CurrentPremium = ((coverage.RiskCoveragePremium?.RiskMgrFeePD ?? 0) < (previousRiskCoverage?.RiskCoveragePremium?.RiskMgrFeePD ?? 0)) ? previousRiskCoverage.RiskCoveragePremium?.RiskMgrFeePD ?? 0 : coverage.RiskCoveragePremium?.RiskMgrFeePD ?? 0,
                EndorsementPendingChange = SetEndorsementPendingChange(BillingConstants.ServiceFeePD, pendingChanges)
            });

            // Get all previous riskdetail endorsement Premium After changes

            return result;
        }

        public static decimal FindDifference(decimal nr1, decimal nr2)
        {
            //return Math.Abs(nr1 - nr2);
            return nr1 - nr2;
        }

        private static string Padding(int size, dynamic value)
        {
            if (size == 25)
                return $"<div class='pad-25'>{value}</div>";
            return $"<div class='pad-50'>{value}</div>";
        }

        private static EndorsementPendingChangeDTO SetEndorsementPendingChange(string amountSubTypeId, List<EndorsementPendingChange> pendingChanges)
        {
            var result = new EndorsementPendingChangeDTO();
            if (pendingChanges.Count() > 0)
            {
                switch (amountSubTypeId)
                {
                    case BillingConstants.VehicleALP:
                        var vehicleAL = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleALP).ToList();
                        result.PremiumBeforeEndorsement = vehicleAL.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = vehicleAL.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = vehicleAL.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case BillingConstants.VehiclePDP:
                        var vehiclePD = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehiclePDP).ToList();
                        result.PremiumBeforeEndorsement = vehiclePD.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = vehiclePD.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = vehiclePD.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case BillingConstants.VehicleCP:
                        var vehicleCP = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleCP).ToList();
                        result.PremiumBeforeEndorsement = vehicleCP.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = vehicleCP.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = vehicleCP.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case "Vehicle":
                        var vehicles = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleALP ||
                                    x.AmountSubTypeId == BillingConstants.VehiclePDP ||
                                    x.AmountSubTypeId == BillingConstants.VehicleCP).ToList();
                        result.PremiumBeforeEndorsement = vehicles.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = vehicles.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = vehicles.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case "Policy":
                        var policy = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.AdditionalInsured || x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation ||
                                    x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory || x.AmountSubTypeId == BillingConstants.GeneralLiabilityPremium ||
                                    x.AmountSubTypeId == BillingConstants.ALManuscriptPremium || x.AmountSubTypeId == BillingConstants.PDManuscriptPremium ||
                                    x.AmountSubTypeId == BillingConstants.HiredPhysicalDamageEndst || x.AmountSubTypeId == BillingConstants.TrailerInterchangeEndst).ToList();
                        result.PremiumBeforeEndorsement = policy.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = policy.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = policy.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case "Additional Premium (Fully Earned)":
                        var addtlPrem = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.AdditionalInsured || x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation ||
                                    x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory).ToList();
                        result.PremiumBeforeEndorsement = addtlPrem.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = addtlPrem.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = addtlPrem.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case "Total Premium":
                        var totalPrem = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleALP ||
                                    x.AmountSubTypeId == BillingConstants.VehiclePDP || x.AmountSubTypeId == BillingConstants.VehicleCP ||
                                    x.AmountSubTypeId == BillingConstants.AdditionalInsured || x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation ||
                                    x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory || x.AmountSubTypeId == BillingConstants.GeneralLiabilityPremium ||
                                    x.AmountSubTypeId == BillingConstants.ALManuscriptPremium || x.AmountSubTypeId == BillingConstants.PDManuscriptPremium ||
                                    x.AmountSubTypeId == BillingConstants.HiredPhysicalDamageEndst || x.AmountSubTypeId == BillingConstants.TrailerInterchangeEndst).ToList();
                        result.PremiumBeforeEndorsement = totalPrem.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = totalPrem.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = totalPrem.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    case "Fees (Fully Earned)":
                        var feesData = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ServiceFeeAL || x.AmountSubTypeId == BillingConstants.ServiceFeePD).ToList();
                        result.PremiumBeforeEndorsement = feesData.Sum(x => (x.PremiumBeforeEndorsement ?? 0));
                        result.PremiumAfterEndorsement = feesData.Sum(x => (x.PremiumAfterEndorsement ?? 0));
                        result.PremiumDifference = feesData.Sum(x => (x.PremiumDifference ?? 0));
                        break;
                    default:
                        // Additional Insured
                        // Waiver of Subrogation
                        // Primary & Non-Contributory
                        // General Liability Premium
                        // AL Manuscript Premium
                        // PD Manuscript Premium
                        // PD Hired Physical Damage
                        // PD Trailer Interchange
                        // Surplus Lines Tax
                        // Stamping Fee
                        // Service Fee - AL
                        // Service Fee - PD
                        var defaultData = pendingChanges.Where(x => x.AmountSubTypeId == amountSubTypeId).FirstOrDefault();
                        result.PremiumBeforeEndorsement = defaultData?.PremiumBeforeEndorsement ?? 0;
                        result.PremiumAfterEndorsement = defaultData?.PremiumAfterEndorsement ?? 0;
                        result.PremiumDifference = defaultData?.PremiumDifference ?? 0;
                        break;
                }
            }

            return result;
        }

        public async Task CalculatePremiumChanges(Guid riskDetailId, DateTime effectiveDate, int optionId)
        {
            // Current RiskDetail
            var currentRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(currentRiskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(risk.Id);
            var existingPendingChanges = _endorsementPendingChangeRepository.GetByRiskDetail(riskDetailId);
            var policyHistory = await _policyHistoryRepository.GetAsync(x => x.RiskDetailId == currentRiskDetail.Id);
            var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskId == risk.Id.ToString());
            var currentManuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == currentRiskDetail.Id);
            var currentVehicles = await _vehicleRepository.GetByRiskDetailAsync(riskDetailId, true);
            //currentVehicles = currentVehicles.Where(v => !v.IsDeleted || (v.IsDeleted && v.PreviousVehicleVersion != null));

            // Previous RiskDetail
            PolicyHistory previousActiveHistory = policyHistory.OrderByDescending(x => x.DateCreated).First(x => x.PolicyStatus == RiskStatus.Active);
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == currentRiskDetail.RiskId && riskDetail.Id != riskDetailId
                                            && riskDetail.Status == RiskStatus.Active);
            //var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.CreatedDate).Select(riskDetail => riskDetail.Id).FirstOrDefault();
            var previousRiskDetailId = Guid.Parse(previousActiveHistory.PreviousRiskDetailId);
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var previousVehicles = await _vehicleRepository.GetByRiskDetailAsync(previousRiskDetailId, true);
            var previousPendingChanges = _endorsementPendingChangeRepository.GetByRiskDetail(previousRiskDetailId);
            var previousManuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == previousRiskDetail.Id);
            var vehiclePremiumChangeCalculator = new VehiclePremiumChangeCalculator();
            var proratedPremiumCalculator = new ProratedPremiumCalculator();

            var annualDays = (brokerInfo.ExpirationDate - brokerInfo.EffectiveDate)?.TotalDays ?? 0;
            var pendingChanges = new List<EndorsementPendingChange>();

            // remove previous changes
            foreach (var change in existingPendingChanges)
            {
                _endorsementPendingChangeRepository.Delete(change);
            }

            pendingChanges.AddRange(await GetVehiclePremiumChanges(previousRiskDetail, currentRiskDetail));

            #region Policy Premium changes 

            var policyLevelChanges = new List<EndorsementPendingChange>();

            var currentRiskCoveragePremium = currentRiskDetail.RiskCoverages.First(x => x.OptionNumber == optionId).RiskCoveragePremium;
            var previousRiskCoveragePremium = previousRiskDetail.RiskCoverages.First(x => x.OptionNumber == optionId).RiskCoveragePremium;
            bool IsPreviousGLPremiumExpired = await IsPreviousGeneralLiabilityExpired(risk.Id, effectiveDate);

            // General Liability
            var generalLiabilityChanges = new EndorsementPendingChange();
            // checks if GL is not existing from previous risk detail
            if ((currentRiskCoveragePremium.GeneralLiabilityPremium ?? 0) > 0 && (previousRiskCoveragePremium.GeneralLiabilityPremium ?? 0) == 0)
            {
                decimal addedProratedAmount = proratedPremiumCalculator.CalculateAdded(currentRiskCoveragePremium.GeneralLiabilityPremium ?? 0, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate);
                generalLiabilityChanges.PremiumBeforeEndorsement = ((previousRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) == 0) ? 0 : previousRiskCoveragePremium.ProratedGeneralLiabilityPremium;
                generalLiabilityChanges.PremiumAfterEndorsement = (IsPreviousGLPremiumExpired && generalLiabilityChanges.PremiumBeforeEndorsement != 0) ? (addedProratedAmount + generalLiabilityChanges.PremiumBeforeEndorsement) : addedProratedAmount;
                generalLiabilityChanges.AmountSubTypeId = BillingConstants.GeneralLiabilityPremium;
                generalLiabilityChanges.PremiumDifference = (generalLiabilityChanges.PremiumAfterEndorsement - generalLiabilityChanges.PremiumBeforeEndorsement);
                generalLiabilityChanges.DateCreated = DateTime.Now;
                policyLevelChanges.Add(generalLiabilityChanges);
            }
            // checks if GL is exisiting from previous risk detail and removed
            else if ((currentRiskCoveragePremium.GeneralLiabilityPremium ?? 0) == 0 && (previousRiskCoveragePremium.GeneralLiabilityPremium ?? 0) > 0)
            {
                if ((brokerInfo.ExpirationDate.Value - effectiveDate).TotalDays > 0)
                {
                    var previousGLPremiumProrated = proratedPremiumCalculator.Calculate(previousRiskCoveragePremium.GeneralLiabilityPremium ?? 0, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate, brokerInfo.ExpirationDate.Value);

                    generalLiabilityChanges.PremiumBeforeEndorsement = ((previousRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) == 0) ? previousRiskCoveragePremium.GeneralLiabilityPremium : previousRiskCoveragePremium.ProratedGeneralLiabilityPremium;
                    generalLiabilityChanges.PremiumAfterEndorsement = generalLiabilityChanges.PremiumBeforeEndorsement - previousGLPremiumProrated; // ((previousRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) == 0) ? previousGLPremiumProrated : (previousRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) - previousGLPremiumProrated;
                }
                else
                {
                    generalLiabilityChanges.PremiumBeforeEndorsement = previousRiskCoveragePremium.GeneralLiabilityPremium;
                    generalLiabilityChanges.PremiumAfterEndorsement = proratedPremiumCalculator.CalculateRemoved(previousRiskCoveragePremium.GeneralLiabilityPremium ?? 0, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate);
                }

                generalLiabilityChanges.AmountSubTypeId = BillingConstants.GeneralLiabilityPremium;
                generalLiabilityChanges.PremiumDifference = (generalLiabilityChanges.PremiumAfterEndorsement - generalLiabilityChanges.PremiumBeforeEndorsement);
                generalLiabilityChanges.DateCreated = DateTime.Now;
                policyLevelChanges.Add(generalLiabilityChanges);
            }
            else
            {
                if ((currentRiskCoveragePremium.GeneralLiabilityPremium ?? 0) != (previousRiskCoveragePremium.GeneralLiabilityPremium))
                {
                    if ((effectiveDate - brokerInfo.EffectiveDate.Value).TotalDays == 0)
                    {
                        generalLiabilityChanges.PremiumBeforeEndorsement = previousRiskCoveragePremium.GeneralLiabilityPremium;
                        generalLiabilityChanges.PremiumAfterEndorsement = currentRiskCoveragePremium.GeneralLiabilityPremium;
                    }
                    else
                    {
                        var previousGLPremiumProrated = proratedPremiumCalculator.Calculate(previousRiskCoveragePremium.GeneralLiabilityPremium.Value, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate, brokerInfo.ExpirationDate.Value);
                        var currentGLPremiumProrated = proratedPremiumCalculator.Calculate(currentRiskCoveragePremium.GeneralLiabilityPremium.Value, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate, brokerInfo.ExpirationDate.Value);

                        generalLiabilityChanges.PremiumBeforeEndorsement = previousRiskCoveragePremium.GLPremium ?? 0;
                        generalLiabilityChanges.PremiumAfterEndorsement = ((previousRiskCoveragePremium.GLPremium ?? 0) - previousGLPremiumProrated) + currentGLPremiumProrated;
                    }
                }
                else
                {
                    //generalLiabilityChanges.PremiumBeforeEndorsement = ((currentRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) == 0) ? currentRiskCoveragePremium.GeneralLiabilityPremium : currentRiskCoveragePremium.ProratedGeneralLiabilityPremium;
                    //generalLiabilityChanges.PremiumAfterEndorsement = ((currentRiskCoveragePremium.ProratedGeneralLiabilityPremium ?? 0) == 0) ? currentRiskCoveragePremium.GeneralLiabilityPremium : currentRiskCoveragePremium.ProratedGeneralLiabilityPremium;
                    var previousGLPremiumProrated = proratedPremiumCalculator.Calculate(previousRiskCoveragePremium.GeneralLiabilityPremium.Value, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate, brokerInfo.ExpirationDate.Value);
                    var currentGLPremiumProrated = proratedPremiumCalculator.Calculate(currentRiskCoveragePremium.GeneralLiabilityPremium.Value, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate, brokerInfo.ExpirationDate.Value);

                    generalLiabilityChanges.PremiumBeforeEndorsement = ((previousRiskCoveragePremium.GLPremium ?? 0) - previousGLPremiumProrated) + currentGLPremiumProrated;
                    generalLiabilityChanges.PremiumAfterEndorsement = ((previousRiskCoveragePremium.GLPremium ?? 0) - previousGLPremiumProrated) + currentGLPremiumProrated;
                }

                generalLiabilityChanges.AmountSubTypeId = BillingConstants.GeneralLiabilityPremium;
                generalLiabilityChanges.PremiumDifference = (generalLiabilityChanges.PremiumAfterEndorsement - generalLiabilityChanges.PremiumBeforeEndorsement);
                generalLiabilityChanges.DateCreated = DateTime.Now;

                policyLevelChanges.Add(generalLiabilityChanges);
            }

            // Manuscript
            var manuscriptPremiumChanges = new List<EndorsementPendingChange>();
            if (currentManuscripts.Count() > 0)
            {
                var addedManuscripts = currentManuscripts.Where(newObj => previousManuscripts.All(oldObj => oldObj.CreatedDate != newObj.CreatedDate) && newObj.DeletedDate == null);

                // get all added manuscripts
                foreach (var addedManuscript in addedManuscripts)
                {
                    var addedManuscriptEndorsmentChange = new EndorsementPendingChange();
                    if ((addedManuscript.IsSelected ?? false))
                    {
                        addedManuscriptEndorsmentChange.AmountSubTypeId = (addedManuscript.PremiumType == "AL") ? BillingConstants.ALManuscriptPremium : BillingConstants.PDManuscriptPremium;
                        addedManuscriptEndorsmentChange.PremiumBeforeEndorsement = 0;
                        if (addedManuscript.IsProrate)
                        {
                            addedManuscriptEndorsmentChange.PremiumAfterEndorsement = proratedPremiumCalculator.CalculateAdded(addedManuscript.Premium, brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value, effectiveDate);
                            // update manuscript prorated premium
                            await UpdateManuscriptProratedPremium(addedManuscript, addedManuscriptEndorsmentChange.PremiumAfterEndorsement ?? 0);
                        }
                        else
                        {
                            addedManuscriptEndorsmentChange.PremiumAfterEndorsement = addedManuscript.Premium;
                        }
                        addedManuscriptEndorsmentChange.PremiumDifference = (addedManuscriptEndorsmentChange.PremiumAfterEndorsement - addedManuscriptEndorsmentChange.PremiumBeforeEndorsement);

                        manuscriptPremiumChanges.Add(addedManuscriptEndorsmentChange);
                    }
                }

                // check all existing manuscripts
                foreach (var previousManuscript in previousManuscripts)
                {
                    var currentManuscript = currentManuscripts.FirstOrDefault(x => x.CreatedDate == previousManuscript.CreatedDate);

                    // if manuscript is deleted
                    if (previousManuscript.DeletedDate == null && currentManuscript.DeletedDate != null)
                    {
                        var removedManuscriptEndorsementChange = new EndorsementPendingChange();
                        removedManuscriptEndorsementChange.AmountSubTypeId = (previousManuscript.PremiumType == "AL") ? BillingConstants.ALManuscriptPremium : BillingConstants.PDManuscriptPremium;
                        if (previousManuscript.IsProrate)
                        {
                            removedManuscriptEndorsementChange.PremiumBeforeEndorsement = previousManuscript.ProratedPremium;
                            removedManuscriptEndorsementChange.PremiumAfterEndorsement = proratedPremiumCalculator.CalculateRemoved(previousManuscript.ProratedPremium, currentManuscript.EffectiveDate, brokerInfo.ExpirationDate.Value, effectiveDate);
                            // update manuscript prorated premium
                            await UpdateManuscriptProratedPremium(currentManuscript, removedManuscriptEndorsementChange.PremiumAfterEndorsement ?? 0);
                        }
                        else
                        {
                            removedManuscriptEndorsementChange.PremiumBeforeEndorsement = previousManuscript.Premium;
                            removedManuscriptEndorsementChange.PremiumAfterEndorsement = 0;
                        }
                        removedManuscriptEndorsementChange.PremiumDifference = (removedManuscriptEndorsementChange.PremiumAfterEndorsement - removedManuscriptEndorsementChange.PremiumBeforeEndorsement);

                        manuscriptPremiumChanges.Add(removedManuscriptEndorsementChange);
                    }

                    // if there are no changes on the manuscript return annual premium
                    if (((previousManuscript.DeletedDate == null && currentManuscript.DeletedDate == null)
                            && (previousManuscript.Premium == currentManuscript.Premium))
                         || (previousManuscript.DeletedDate != null && currentManuscript.DeletedDate != null &&
                            previousManuscript.IsProrate && currentManuscript.IsProrate &&
                            previousManuscript.ProratedPremium != 0 && currentManuscript.ProratedPremium != 0))
                    {
                        var currentManuscriptEndorsementChange = new EndorsementPendingChange();
                        currentManuscriptEndorsementChange.AmountSubTypeId = (previousManuscript.PremiumType == "AL") ? BillingConstants.ALManuscriptPremium : BillingConstants.PDManuscriptPremium;
                        currentManuscriptEndorsementChange.PremiumBeforeEndorsement = currentManuscript.ProratedPremium;
                        currentManuscriptEndorsementChange.PremiumAfterEndorsement = currentManuscript.ProratedPremium;
                        currentManuscriptEndorsementChange.PremiumDifference = (currentManuscriptEndorsementChange.PremiumAfterEndorsement - currentManuscriptEndorsementChange.PremiumBeforeEndorsement);
                        manuscriptPremiumChanges.Add(currentManuscriptEndorsementChange);
                    }
                }

                // after collecting all the manuscript AL and PD, need to separate records
                // by amount sub type and save only single record in DB
                var collectedALManuscriptPremiumChanges = new EndorsementPendingChange();
                var collectedPDManuscriptPremiumChanges = new EndorsementPendingChange();

                // collect all AL Manuscripts
                if (manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.ALManuscriptPremium).Count() > 0)
                {
                    collectedALManuscriptPremiumChanges.AmountSubTypeId = BillingConstants.ALManuscriptPremium;
                    collectedALManuscriptPremiumChanges.PremiumBeforeEndorsement = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.ALManuscriptPremium).Sum(x => x.PremiumBeforeEndorsement);
                    collectedALManuscriptPremiumChanges.PremiumAfterEndorsement = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.ALManuscriptPremium).Sum(x => x.PremiumAfterEndorsement);
                    collectedALManuscriptPremiumChanges.PremiumDifference = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.ALManuscriptPremium).Sum(x => x.PremiumDifference);
                    collectedALManuscriptPremiumChanges.DateCreated = DateTime.Now;
                    policyLevelChanges.Add(collectedALManuscriptPremiumChanges);
                }

                // collect all PD Manuscripts
                if (manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.PDManuscriptPremium).Count() > 0)
                {
                    collectedPDManuscriptPremiumChanges.AmountSubTypeId = BillingConstants.PDManuscriptPremium;
                    collectedPDManuscriptPremiumChanges.PremiumBeforeEndorsement = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.PDManuscriptPremium).Sum(x => x.PremiumBeforeEndorsement);
                    collectedPDManuscriptPremiumChanges.PremiumAfterEndorsement = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.PDManuscriptPremium).Sum(x => x.PremiumAfterEndorsement);
                    collectedPDManuscriptPremiumChanges.PremiumDifference = manuscriptPremiumChanges.Where(x => x.AmountSubTypeId == BillingConstants.PDManuscriptPremium).Sum(x => x.PremiumDifference);
                    collectedPDManuscriptPremiumChanges.DateCreated = DateTime.Now;
                    policyLevelChanges.Add(collectedPDManuscriptPremiumChanges);
                }
            }

            if (policyLevelChanges.Count > 0)
            {
                pendingChanges.AddRange(policyLevelChanges);
            }

            #endregion

            pendingChanges = pendingChanges.Where(x => !(x.PremiumBeforeEndorsement == 0 && x.PremiumAfterEndorsement == 0)).ToList();

            pendingChanges.ForEach(x => x.RiskDetailId = riskDetailId);
            // update risk coverage premium
            await UpdateCurrentRiskCoveragePremium(riskDetailId, pendingChanges, currentVehicles, optionId, currentRiskDetail);

            await _endorsementPendingChangeRepository.AddRangeAsync(pendingChanges);
            await _endorsementPendingChangeRepository.SaveChangesAsync();

            #region Additional Premiums
            await SetAdditionalPremiumChanges(currentRiskDetail);
            #endregion

            #region Fees change
            await SetServiceFeeChanges(currentRiskDetail);
            #endregion

            #region Taxes
            await SetTaxesChanges(currentRiskDetail, effectiveDate);
            #endregion
        }

        public async Task UpdateCurrentRiskCoveragePremium(Guid riskDetailId, List<EndorsementPendingChange> pendingChanges, IEnumerable<Vehicle> currentVehicles, int optionNumber, RiskDetail currentRiskDetail)
        {
            var riskCoverages = await _riskCoverageRepository.GetRiskCoveragesAndPremiumsAsync(riskDetailId);
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            bool hasHiredPhysicalDamage = riskForms.Any(x => x.FormId == "HiredPhysicalDamage" && (x.IsSelected ?? false));
            bool hasTrailerInterchange = riskForms.Any(x => x.FormId == "TrailerInterchangeCoverage" && (x.IsSelected ?? false));
            var riskCoverage = riskCoverages.Where(x => x.OptionNumber == optionNumber).FirstOrDefault();
            var riskCoveragePremium = riskCoverage.RiskCoveragePremium;

            var generalLiabilityPremiumChange = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.GeneralLiabilityPremium).Sum(x => x.PremiumAfterEndorsement ?? 0);
            var alManuscriptPremiumChange = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ALManuscriptPremium).Sum(x => x.PremiumAfterEndorsement ?? 0);
            var pdManuscriptPremiumChange = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.PDManuscriptPremium).Sum(x => x.PremiumAfterEndorsement ?? 0);

            riskCoveragePremium.ProratedALManuscriptPremium = (alManuscriptPremiumChange == 0) ? riskCoveragePremium.ALManuscriptPremium : alManuscriptPremiumChange;
            riskCoveragePremium.ProratedPDManuscriptPremium = (pdManuscriptPremiumChange == 0) ? riskCoveragePremium.PDManuscriptPremium : pdManuscriptPremiumChange;
            riskCoveragePremium.ProratedGeneralLiabilityPremium = (generalLiabilityPremiumChange == 0) ? riskCoveragePremium.GeneralLiabilityPremium : generalLiabilityPremiumChange;

            string[] NonPremiumAmountSubTypeId = { "PDSF", "ALSF", "SF", "SLT" };
            var totalPolicyPremium = pendingChanges.Where(x => !NonPremiumAmountSubTypeId.Contains(x.AmountSubTypeId)).Sum(x => (x.PremiumAfterEndorsement ?? 0).RoundTo2Decimals());

            if (hasHiredPhysicalDamage)
            {
                totalPolicyPremium += riskCoveragePremium.HiredPhysicalDamage ?? 0;
            }

            if (hasTrailerInterchange)
            {
                totalPolicyPremium += riskCoveragePremium.TrailerInterchange ?? 0;
            }

            riskCoveragePremium.ProratedPremium = totalPolicyPremium +
                                                    (riskCoveragePremium.AdditionalInsuredPremium ?? 0) +
                                                    (riskCoveragePremium.WaiverOfSubrogationPremium ?? 0) +
                                                    (riskCoveragePremium.PrimaryNonContributoryPremium ?? 0);

            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == currentRiskDetail.RiskId && riskDetail.Id != currentRiskDetail.Id && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);

            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == optionNumber).FirstOrDefault();
            var previousSFAL = previousRiskCoverage.RiskCoveragePremium.RiskMgrFeeAL ?? 0;
            var previousSFPD = previousRiskCoverage.RiskCoveragePremium.RiskMgrFeePD ?? 0;
            var currentSFAL = riskCoveragePremium.RiskMgrFeeAL ?? 0;
            var currentSFPD = riskCoveragePremium.RiskMgrFeePD ?? 0;

            riskCoveragePremium.RiskMgrFeeAL = (currentSFAL < previousSFAL) ? previousSFAL : currentSFAL; // Service Fee should only log for Added Vehicles only
            riskCoveragePremium.RiskMgrFeePD = (currentSFPD < previousSFPD) ? previousSFPD : currentSFPD; // Service Fee should only log for Added Vehicles only

            riskCoverage.RiskCoveragePremium = riskCoveragePremium;
            _riskCoverageRepository.Update(riskCoverage);
            await _riskCoverageRepository.SaveChangesAsync();
        }

        private async Task UpdateManuscriptProratedPremium(RiskManuscripts manuscripts, decimal proratedPremium)
        {
            manuscripts.ProratedPremium = proratedPremium;
            _riskManuscriptsRepository.Update(manuscripts);
            await _riskManuscriptsRepository.SaveChangesAsync();
        }

        private async Task<bool> IsPreviousGeneralLiabilityExpired(Guid riskId, DateTime effectiveDate)
        {
            var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskId == riskId.ToString());
            bool isGLPremiumExpired = false;

            foreach (var policyHistory in policyHistories.OrderByDescending(x => x.DateCreated))
            {
                if (policyHistory.EffectiveDate.Value > effectiveDate) continue; // skip all currently in effect endorsements
                if (int.Parse(policyHistory.EndorsementNumber) < 1) continue; // skips NEW transaction type
                if (policyHistory.Details is null) continue; // skips history with empty details i.e. NEW and REINSTATED

                string[] policyChanges = JsonConvert.DeserializeObject<string[]>(policyHistory.Details);

                foreach (var detail in policyChanges)
                {
                    if (detail.StartsWith("Changed General Liability - Bodily Injury") && detail.EndsWith("To: 'NONE'"))
                    {
                        isGLPremiumExpired = true;
                    }
                }

                if (isGLPremiumExpired) break;
            }

            return isGLPremiumExpired;
        }

        private async Task<IEnumerable<EndorsementPendingChange>> GetVehiclePremiumChanges(RiskDetail previousRiskDetail, RiskDetail riskDetail)
        {
            var previousVehiclePremiums = await _vehiclePremiumRepository.GetByRiskDetail(previousRiskDetail.Id);
            var vehiclePremiums = await _vehiclePremiumRepository.GetByRiskDetail(riskDetail.Id);

            var vehicleEndorsementChange = new List<EndorsementPendingChange>();

            var autoLiabilityEndorsementChanges = new EndorsementPendingChange();
            autoLiabilityEndorsementChanges.AmountSubTypeId = BillingConstants.VehicleALP;
            autoLiabilityEndorsementChanges.PremiumAfterEndorsement = vehiclePremiums.Sum(x => x.AL.GrossProrated.RoundTo2Decimals());
            autoLiabilityEndorsementChanges.PremiumBeforeEndorsement = previousVehiclePremiums.Sum(x => x.AL.GrossProrated.RoundTo2Decimals());
            autoLiabilityEndorsementChanges.PremiumDifference = autoLiabilityEndorsementChanges.PremiumAfterEndorsement - autoLiabilityEndorsementChanges.PremiumBeforeEndorsement;

            vehicleEndorsementChange.Add(autoLiabilityEndorsementChanges);

            var physicalDamageEndorsementChanges = new EndorsementPendingChange();
            physicalDamageEndorsementChanges.AmountSubTypeId = BillingConstants.VehiclePDP;
            physicalDamageEndorsementChanges.PremiumAfterEndorsement = vehiclePremiums.Sum(x => x.PD.GrossProrated.RoundTo2Decimals());
            physicalDamageEndorsementChanges.PremiumBeforeEndorsement = previousVehiclePremiums.Sum(x => x.PD.GrossProrated.RoundTo2Decimals());
            physicalDamageEndorsementChanges.PremiumDifference = physicalDamageEndorsementChanges.PremiumAfterEndorsement - physicalDamageEndorsementChanges.PremiumBeforeEndorsement;
            vehicleEndorsementChange.Add(physicalDamageEndorsementChanges);

            var cargoEndorsementChanges = new EndorsementPendingChange();
            cargoEndorsementChanges.AmountSubTypeId = BillingConstants.VehicleCP;
            cargoEndorsementChanges.PremiumAfterEndorsement = vehiclePremiums.Sum(x => x.Cargo.GrossProrated.RoundTo2Decimals());
            cargoEndorsementChanges.PremiumBeforeEndorsement = previousVehiclePremiums.Sum(x => x.Cargo.GrossProrated.RoundTo2Decimals());
            cargoEndorsementChanges.PremiumDifference = cargoEndorsementChanges.PremiumAfterEndorsement - cargoEndorsementChanges.PremiumBeforeEndorsement;
            vehicleEndorsementChange.Add(cargoEndorsementChanges);

            return vehicleEndorsementChange;
        }

        public async Task<string> GeneratePolicyNumber(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.GetInsuredAddressAsync(riskDetailId);
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var state = riskDetail.Insured.Entity.EntityAddresses.Find(x => x.AddressTypeId == null).Address.State;
            if (risk.PolicyNumber == null)
            {
                risk.PolicyNumber = _riskSequence.GeneratePolicyNumber(state, brokerInfo.ExpirationDate.Value, brokerInfo.EffectiveDate.Value);
            }

            await _riskRepository.SaveChangesAsync();

            return risk.PolicyNumber;
        }
    }
}
