﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Forms;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.Services.Common;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class FormsService : IFormsService
    {
        private readonly IFormRepository _formRepository;
        private readonly IMapper _mapper;
        private readonly INameAndAddressService _namedAddressService;
        private readonly IRiskFormRepository _riskFormRepository;
        private readonly IQuoteOptionsService _quoteOptionsService;
        private readonly IBindingService _bindingService;
        private readonly IBrokerInfoService _brokerInfoService;
        private readonly ILog4NetService _errorLogservice;
        private readonly IRiskDetailRepository _riskDetailRepository;

        public FormsService(IRiskFormRepository riskFormRepository, 
                            IFormRepository formRepository, 
                            INameAndAddressService namedAddressService, 
                            IQuoteOptionsService quoteOptionsService,
                            IBindingService bindingService,
                            IBrokerInfoService brokerInfoService,
                            IRiskDetailRepository riskDetailRepository,
                            IMapper mapper, ILog4NetService errorLogservice)
        {
            _riskFormRepository = riskFormRepository;
            _formRepository = formRepository;
            _namedAddressService = namedAddressService;
            _quoteOptionsService = quoteOptionsService;
            _bindingService = bindingService;
            _brokerInfoService = brokerInfoService;
            _errorLogservice = errorLogservice;
            _riskDetailRepository = riskDetailRepository;
            _mapper = mapper;
        }

        public JsonSerializerSettings JsonSerializerSettings =>
            new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter> { new DecimalFormatConverter() }
            };

        public async Task<List<RiskFormDTO>> GetByRiskIdAsync(Guid riskId)
        {
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskId && x.Form.IsActive, r => r.Form);
            return _mapper.Map<List<RiskFormDTO>>(riskForms);
        }

        public async Task<List<RiskFormDTO>> GetSelectedByRiskIdAsync(Guid riskId)
        {
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskId && x.Form.IsActive && x.IsSelected == true, r => r.Form);
            return _mapper.Map<List<RiskFormDTO>>(riskForms);
        }

        public async Task<bool> HasInitialDriverExclusionFormAsync(Guid riskId)
        {
            var riskDetail = await _riskDetailRepository.GetFirstRiskDetailByRiskIdAsync(riskId);
            var riskForm = await _riskFormRepository.FindAsync(x => x.RiskDetailId == riskDetail.Id && x.Form.IsActive && x.IsSelected == true && x.FormId == "DriverExclusion");
            if (riskForm != null) return riskForm.Other != null;
            return false;
        }

        public async Task<List<RiskFormDTO>> UpdateAsync(List<RiskFormDTO> riskForms)
        {
            foreach (var riskForm in riskForms)
            {
                var entity =
                    await _riskFormRepository.FindAsync(e =>
                        e.RiskDetailId == riskForm.RiskDetailId && e.FormId == riskForm.FormId);
                if (entity == null) continue;
                entity.IsSelected = riskForm.IsSelected;
                entity.Other = riskForm.Other;
                _riskFormRepository.Update(entity);
            }

            await _riskFormRepository.SaveChangesAsync();
            return riskForms;
        }

        public async Task<bool> UpdateSelectedAsync(List<RiskFormDTO> riskForms)
        {
            foreach (var riskForm in riskForms)
            {
                var entity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == riskForm.RiskDetailId && e.FormId == riskForm.FormId);
                if (entity == null) continue;
                entity.IsSelected = riskForm.IsSelected;
                _riskFormRepository.Update(entity);
            }
            await _riskFormRepository.SaveChangesAsync();
            return true;
        }

        public async Task<RiskFormDTO> InsertUserFormAsync(RiskFormDTO riskForm)
        {
            var maxSortOrder = await _formRepository.GetAsync(f => f.IsActive);
            var newId = Guid.NewGuid().ToString();
            var entity = _mapper.Map<RiskForm>(riskForm);
            entity.IsSelected = true;
            entity.FormId = newId;
            entity.Form.Id = newId;
            entity.Form.SortOrder = maxSortOrder.Max(s => s.SortOrder) + 1;
            entity.Form.IsActive = true;
            entity.Form.DecPageOrder = 1;

            var newRiskForm = await _riskFormRepository.AddAsync(entity);
            await _riskFormRepository.SaveChangesAsync();

            return _mapper.Map<RiskFormDTO>(newRiskForm);
        }

        public async Task<List<RiskFormDTO>> InsertDefaultsAsync(Guid riskId)
        {
            var forms = await _formRepository.GetAsync(f => f.IsActive && !f.IsSupplementalDocument);

            var userStates = await GetUserStates(riskId);
            var baseForms = new List<RiskForm>();
            foreach (var form in forms)
            {
                var riskForm = new RiskForm
                {
                    Id = Guid.NewGuid(),
                    RiskDetailId = riskId,
                    FormId = form.Id,
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now,
                    Form = form,
                    IsSelected = form.IsDefault
                };
                riskForm.Other = form.Id switch
                {
                    "ExclusionRadiusEndorsement" => JsonConvert.SerializeObject(
                        new ExclusionRadiusDTO {Radius = 300}, JsonSerializerSettings),
                    "TerritoryExclusion" => JsonConvert.SerializeObject(
                        new TerritoryExclusionDTO { States = string.Join(",", userStates.Distinct()), StatesDefault = true}, JsonSerializerSettings),
                    "DriverRequirements" => JsonConvert.SerializeObject(
                        new DriverRequirementDTO
                        {
                            Requirement = "No more than one (1) moving violation or at-fault accident in the last thirty-six (36) months"
                        }, JsonSerializerSettings),
                    _ => riskForm.Other
                };

                baseForms.Add(riskForm);
            }
            
            var riskForms = _mapper.Map<List<RiskFormDTO>>(baseForms);

            baseForms.ForEach(rf => rf.Form = null);
            await _riskFormRepository.AddRangeAsync(baseForms);
            await _riskFormRepository.SaveChangesAsync();

            return riskForms;
        }

        public async Task<RiskFormDTO> UpdateDefaultStatesAsync(Guid id)
        {
            var entity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == id && e.FormId == "TerritoryExclusion");
            if (entity == null) return null;

            var userStates = await GetUserStates(id);
            entity.Other = JsonConvert.SerializeObject(
                new TerritoryExclusionDTO {States = string.Join(",", userStates.Distinct()), StatesDefault = true},
                JsonSerializerSettings);

            await _riskFormRepository.SaveChangesAsync();
            return _mapper.Map<RiskFormDTO>(entity);
        }

        public async Task<RiskFormDTO> SingleFormUpdateAsync(RiskFormDTO riskForm)
        {
            var entity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == riskForm.RiskDetailId && e.FormId == riskForm.FormId);

            if(entity != null)
            {
                if (entity.IsSelected == riskForm.IsSelected) return riskForm;
                entity.IsSelected = riskForm.IsSelected;
                _riskFormRepository.Update(entity);

                if (riskForm.IsSelected == false)
                {
                    switch (entity.FormId)
                    {
                        case "HiredPhysicalDamage":
                            await _quoteOptionsService.RemoveHiredPhysicalDamage(riskForm.RiskDetailId);
                            break;
                        case "TrailerInterchangeCoverage":
                            await _quoteOptionsService.RemoveTrailerInterchange(riskForm.RiskDetailId);
                            break;
                        default:
                            break;
                    }
                }
                await _riskFormRepository.SaveChangesAsync();
            }
            return riskForm;
        }

        public async Task<RiskFormDTO> UpdateOtherAsync<T>(RiskFormOtherDTO resource)
        {
            resource.UpdatedDate = null;
            var otherJson = JsonConvert.SerializeObject(_mapper.Map<T>(resource), JsonSerializerSettings);

            var entity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == resource.FormId);
            entity.Other = otherJson;
            entity.IsSelected = true;

            _riskFormRepository.Update(entity);
            await _riskFormRepository.SaveChangesAsync();
            return _mapper.Map<RiskFormDTO>(entity);
        }

        public async Task SaveFormsContentAsync(RiskDetail riskDetail, DateTime? effectiveDate = null)
        {
            try
            {
                var risk = _mapper.Map<RiskDTO>(riskDetail);
                risk.Binding = await _bindingService.GetBindingByRiskIdAsync(riskDetail.RiskId);
                risk.BrokerInfo = await _brokerInfoService.GetByRiskDetailIdAsync(riskDetail.RiskId);

                var optionId = (risk.Binding?.BindOptionId ?? 1).ToString();
                var optionIndex = int.Parse(optionId) - 1;

                var isEndorsement = effectiveDate != null;
                effectiveDate = (isEndorsement ? effectiveDate : risk.BrokerInfo?.EffectiveDate) ?? DateTime.Now;

                if (risk.RiskCoverages == null || !risk.RiskCoverages.Any()) throw new Exception("Missing RiskCoverages");

                var coverage = optionIndex < risk.RiskCoverages.Count ? risk.RiskCoverages[optionIndex] : risk.RiskCoverages[0];

                var excludeDrivers = isEndorsement
                    ? risk.RiskDetailDrivers.Where(d => IsDriverDeletedOrInKoRule(d, effectiveDate.Value, risk.RiskDetailDrivers.Select(rd => rd.PreviousDriverVersionId.GetValueOrDefault()).ToList()) ||
                                                        (d.IsActive != false && (d.Options == null || !d.Options.Contains(optionId))))
                    : risk.RiskDetailDrivers.Where(d => d.IsActive != false && (d.Options == null || !d.Options.Contains(optionId)));

                var resource = new RiskFormOtherDTO
                {
                    RiskDetailId = risk.Id.GetValueOrDefault(),
                    AutoBILimitId = coverage?.AutoBILimitId,
                    UMBILimitId = coverage?.UMBILimitId,
                    Drivers = _mapper.Map<List<FormDriverDTO>>(excludeDrivers.Distinct()),
                    UpdatedDate = effectiveDate
                };

                var uiiaJson = JsonConvert.SerializeObject(_mapper.Map<FormUIIADTO>(resource), JsonSerializerSettings);
                var uiiaEntity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "UIIAUpdated");
                if (uiiaEntity != null)
                {
                    if (uiiaEntity.IsSelected == false) uiiaEntity.Other = null;
                    else
                    {
                        if (uiiaEntity.Other == null) uiiaEntity.Other = uiiaJson;
                        else
                        {
                            var entityOther = JsonConvert.DeserializeObject<FormUIIADTO>(uiiaEntity.Other);
                            if (entityOther.AutoBILimitId != resource.AutoBILimitId) uiiaEntity.Other = uiiaJson;
                        }
                    }
                }

                var uninsuredMotoristBIJson = JsonConvert.SerializeObject(_mapper.Map<FormUMBIDTO>(resource), JsonSerializerSettings);
                var cauninsuredMotoristBI = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "UninsuredMotoristCoverageBodilyInjury");
                if (cauninsuredMotoristBI != null)
                {
                    if (cauninsuredMotoristBI.IsSelected == false) cauninsuredMotoristBI.Other = null;
                    else
                    {
                        if (cauninsuredMotoristBI.Other == null) cauninsuredMotoristBI.Other = uninsuredMotoristBIJson;
                        else
                        {
                            var entityOther = JsonConvert.DeserializeObject<FormUMBIDTO>(cauninsuredMotoristBI.Other);
                            if (entityOther.UMBILimitId != resource.UMBILimitId) cauninsuredMotoristBI.Other = uninsuredMotoristBIJson;
                        }
                    }
                }
                var splituninsuredMotoristBI = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "SplitBodilyInjuryUninsuredMotoristCoverage");
                if (splituninsuredMotoristBI != null)
                {
                    if (splituninsuredMotoristBI.IsSelected == false) splituninsuredMotoristBI.Other = null;
                    else
                    {
                        if (splituninsuredMotoristBI.Other == null) splituninsuredMotoristBI.Other = uninsuredMotoristBIJson;
                        else
                        {
                            var entityOther = JsonConvert.DeserializeObject<FormUMBIDTO>(splituninsuredMotoristBI.Other);
                            if (entityOther.UMBILimitId != resource.UMBILimitId) splituninsuredMotoristBI.Other = uninsuredMotoristBIJson;
                        }
                    }
                }

                var hasPhysicalDamage = coverage != null && (coverage.CompDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                                                             || coverage.FireDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                                                             || coverage.CollDeductibleId > LimitConstants.CollisionNoCoverageId);

                var pdLimitEntity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "PhysicalDamageLimitOfInsurance");
                if (pdLimitEntity != null) pdLimitEntity.IsSelected = hasPhysicalDamage;
                SetUpdatedDate<FormBaseDTO>(pdLimitEntity, resource);

                var trailerEntity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "TrailerInterchangeCoverage");
                SetUpdatedDate<TrailerInterchangeDTO>(trailerEntity, resource);

                var territoryEntity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "TerritoryExclusion");
                SetUpdatedDate<TerritoryExclusionDTO>(territoryEntity, resource);

                // Driver
                var firstRiskDetail = await _riskDetailRepository.GetFirstRiskDetailByRiskIdAsync(riskDetail.RiskId);
                var firstRiskDetailForm = await _riskFormRepository.FindAsync(x => x.RiskDetailId == firstRiskDetail.Id && x.RiskDetailId != riskDetail.Id &&
                                                x.Form.IsActive && x.IsSelected == true && x.FormId == "DriverExclusion");
                if (firstRiskDetailForm != null && firstRiskDetailForm?.Other != null)
                {
                    var entityOther = JsonConvert.DeserializeObject<FormDriverExclusionDTO>(firstRiskDetailForm.Other);
                    resource.Drivers.AddRange(entityOther.Drivers);
                }
                var driverJson = JsonConvert.SerializeObject(_mapper.Map<FormDriverExclusionDTO>(resource), JsonSerializerSettings);
                var driverEntity = await _riskFormRepository.FindAsync(e => e.RiskDetailId == resource.RiskDetailId && e.FormId == "DriverExclusion");
                if (resource.Drivers.Any()) driverEntity.Other = driverJson;

                _riskFormRepository.Update(uiiaEntity);
                _riskFormRepository.Update(pdLimitEntity);
                _riskFormRepository.Update(trailerEntity);
                _riskFormRepository.Update(territoryEntity);
                _riskFormRepository.Update(driverEntity);
                _riskFormRepository.Update(cauninsuredMotoristBI);
                _riskFormRepository.Update(splituninsuredMotoristBI);

                await _riskFormRepository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _errorLogservice.Error(ex);
            }

        }

        private void SetUpdatedDate<T>(RiskForm entity, RiskFormOtherDTO resource) where T : IDialogUpdate
        {
            if (entity == null) return;
            if (!entity.IsSelected.GetValueOrDefault()) entity.Other = null;
            else
            {
                var otherJson = JsonConvert.SerializeObject(_mapper.Map<T>(resource), JsonSerializerSettings);
                if (entity.Other == null) entity.Other = otherJson;
                else
                {
                    var entityOther = JsonConvert.DeserializeObject<T>(entity.Other);
                    if (entityOther.UpdatedDate != null) return;
                    entityOther.UpdatedDate = resource.UpdatedDate;
                    entity.Other = JsonConvert.SerializeObject(_mapper.Map<T>(entityOther), JsonSerializerSettings);
                }
            }
        }

        private async Task<List<string>> GetUserStates(Guid id)
        {
            var userStates = new List<string>();

            var nameAddress = await _namedAddressService.GetNameAndAddress(id);

            userStates.Add(nameAddress.State);
            userStates.Add(nameAddress.StateOperations);

            var garagingAddress = nameAddress.Addresses.Where(na => na.AddressTypeId == 3).Select(na => na.Address?.State);
            userStates.AddRange(garagingAddress);

            return userStates;
        }

        public async Task<bool> RemoveFormAsync(Guid id)
        {
            var riskForm = await _riskFormRepository.FindAsync(e => e.Id == id);
            var form = await _formRepository.FindAsync(e => e.Id == riskForm.FormId);
            riskForm.IsActive = false;
            riskForm.DeletedDate = DateTime.Now;
            riskForm.IsSelected = false;
            var result = _riskFormRepository.Update(riskForm);
            await _riskFormRepository.SaveChangesAsync();

            form.IsActive = false;
            _formRepository.Update(form);
            await _formRepository.SaveChangesAsync();

            return true;
        }

        private static bool IsDriverDeletedOrInKoRule(DriverDTO driver, DateTime brokerEffectiveDate, ICollection<Guid> previousVersionIds)
        {
            var endDate = brokerEffectiveDate.Date;
            var startDate = endDate.AddMonths(-36).Date;

            if (driver.DeletedDate != null && !previousVersionIds.Contains(driver.Id.GetValueOrDefault())) return true;

            var isAgeStringValid = int.TryParse(driver.Age, out var driverAge);
            if (!isAgeStringValid) return false;
            if (driverAge <= 22) return true;
            if ((driverAge == 23 || driverAge == 24) && driver.IsMvr == false && driver.isExcludeKO == false) return true;

            var koIncidentType = new[] {"1", "2", "4", "5", "8"};
            var koIncidents = driver.DriverIncidents.Where(di => koIncidentType.Contains(di.IncidentType)
                                                                 && di.IncidentDate <= endDate &&
                                                                 di.IncidentDate >= startDate
                                                                 && driver.isExcludeKO == false).ToList();

            return koIncidents.Count > 1;
        }

    }
}