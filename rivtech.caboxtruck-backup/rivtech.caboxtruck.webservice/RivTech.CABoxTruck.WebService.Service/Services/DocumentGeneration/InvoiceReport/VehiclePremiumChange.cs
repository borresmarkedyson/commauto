﻿namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration
{
    public class VehiclePremiumChange
    {
        public string ChangeType { get; set; }
        public string Vin { get; set; }
        public decimal Amount { get; set; }
    }
}