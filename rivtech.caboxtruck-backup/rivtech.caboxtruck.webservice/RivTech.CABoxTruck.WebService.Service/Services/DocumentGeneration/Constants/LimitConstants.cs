﻿namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants
{
    public static class LimitConstants
    {
        public const int CollisionNoCoverageId = 65;
        public const int ComprehensiveNoCoverageId = 76;
        public const int GLNoneCoverageId = 87;
        public const int CargoNoneCoverageId = 53;
        public const int RefNoneCoverageId = 157;
    }
}