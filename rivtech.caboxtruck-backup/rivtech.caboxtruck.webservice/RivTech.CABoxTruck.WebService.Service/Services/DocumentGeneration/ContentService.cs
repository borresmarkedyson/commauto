﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.Service.IServices.DocumentGeneration;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration
{
    public class ContentService : IContentService
    {
        //TODO: Use AppSettings
        private const string STATESERVICEURL = "https://app-genericservices-test.azurewebsites.net/api/State";

        private readonly ICacheService _cacheService;
        private readonly ILimitsService _limitsService;
        private readonly IPolicyContactsService _policyContactsService;
        private readonly IRiskPolicyContactService _riskPolicyContactService;
        private readonly INameAndAddressService _nameAndAddressService;
        private readonly ITaxService _taxService;
        private readonly IBillingService _billingService;
        private readonly IVehicleService _vehicleService;
        private readonly IRiskService _riskService;
        private readonly IConfiguration _configuration;

        private bool _isEndorsement;

        public ContentService(IRiskService riskService,
            ILimitsService limitsService,
            IPolicyContactsService policyContactsService,
            INameAndAddressService nameAndAddressService,
            ITaxService taxService,
            IBillingService billingService,
            IVehicleService vehicleService,
            ICacheService cacheService,
            IConfiguration configuration, IRiskPolicyContactService riskPolicyContactService)
        {
            _riskService = riskService;
            _limitsService = limitsService;
            _policyContactsService = policyContactsService;
            _nameAndAddressService = nameAndAddressService;
            _taxService = taxService;
            _billingService = billingService;
            _vehicleService = vehicleService;
            _cacheService = cacheService;
            _configuration = configuration;
            _riskPolicyContactService = riskPolicyContactService;
        }

        public async Task<ReportData> GetReportData(Guid riskId, bool isEndorsement = false)
        {
            _isEndorsement = isEndorsement;
            var riskDto = await _riskService.GetByIdIncludeAsync(riskId);
            return new ReportData(riskDto) { DataSets = await GetDataSets(riskId) };
        }

        public async Task<Dictionary<string, List<dynamic>>> GetDataSets(Guid riskId)
        {
            var riskDto = await _riskService.GetByIdIncludeAsync(riskId);
            var limitsDataSets = await _limitsService.GetPerStateAsync(riskDto.NameAndAddress?.State);
            var policyContacts = await _policyContactsService.GetAllPolicyContacts();
            var riskPolicyContacts = await _riskPolicyContactService.GetByRiskDetailIdAsync(riskId);
            var nameAndAddress = await _nameAndAddressService.GetNameAndAddress(riskId);
            var brokerAddresses = riskDto.BrokerInfo?.Agency?.Entity?.EntityAddresses;
            var paymentPortalLink = _configuration?.GetSection("PaymentPortal")?.GetValue<string>("BaseUrl");
            
            var taxDetails = await _taxService.GetTaxDetails(riskDto.RiskCoverages.Select(rc => rc.RiskCoveragePremium?.TaxDetailsId ?? Guid.Empty).ToList());
            var states = await GetStatesList();

            //TODO: Check source DB/Generic service
            var slBrokers = new[] {
                new { Id = "Rivington", Description = "Rivington Partners", Address1 = "1120 6th Ave 21st Floor", CityStateZip = "New York, NY 10036" },
                new { Id = "Vale", Description = "Vale Insurance Partners", Address1 = "1120 6th Ave 21st Floor", CityStateZip = "New York, NY 10036" },
                new { Id = "Applied", Description = "Applied Risk Services", Address1 = "1120 6th Ave 21st Floor", CityStateZip = "New York, NY 10036" }
            };

            //TODO: Update when this dataset is transfered to db
            var mainUse = new[]
            {
                new {Id = 1, Description = "Courier"},
                new {Id = 2, Description = "Commercial"},
                new {Id = 3, Description = "Service"}
            };

            var commonValues = new[]
            {
                new { Id = "PaymentPortalLink", Value = paymentPortalLink }
            };

            var installmentSchedule = _isEndorsement ? await _billingService.GetEndorsementInstallmentSchedule(riskDto.RiskId):
                await _billingService.GetInstallmentSchedule(riskDto);

            var policyVehicles = await _vehicleService.GetByRiskDetailIdIncludeAsync(riskId, true);

            var dataSets = new Dictionary<string, List<dynamic>>
            {
                {"BindRequirementsOption", await _cacheService.GetKeyValueList("BindRequirementsOption")},
                {"CreditedOffice", await _cacheService.GetKeyValueList("CreditedOffice")},
                {"QuoteConditionsOption", await _cacheService.GetKeyValueList("QuoteConditionsOption")},
                {"BusinessTypes", await _cacheService.GetKeyValueList("BusinessType")},
                {"Symbols", await _cacheService.GetKeyValueList("Symbol")},
                {"PolicyContacts", policyContacts?.Cast<dynamic>().ToList()},
                {"RiskPolicyContacts", riskPolicyContacts?.Cast<dynamic>().ToList()},
                {"Addresses", nameAndAddress.Addresses?.Cast<dynamic>().ToList()},
                {"BrokerAddresses", brokerAddresses?.Cast<dynamic>().ToList()},

                {"AutoBILimits", limitsDataSets.AutoBILimits.Cast<dynamic>().ToList()},
                {"AutoPDLimits", limitsDataSets.AutoPDLimits.Cast<dynamic>().ToList()},
                {"MedPayLimits", limitsDataSets.MedPayLimits.Cast<dynamic>().ToList()},
                {"PipLimits", limitsDataSets.PipLimits.Cast<dynamic>().ToList()},
                {"UMBILimits", limitsDataSets.UMBILimits.Cast<dynamic>().ToList()},
                {"UIMBILimits", limitsDataSets.UIMBILimits.Cast<dynamic>().ToList()},
                {"UMPDLimits", limitsDataSets.UMPDLimits.Cast<dynamic>().ToList()},
                {"UIMPDLimits", limitsDataSets.UIMPDLimits.Cast<dynamic>().ToList()},
                {"LiabilityDeductibleLimits", limitsDataSets.LiabilityDeductibleLimits.Cast<dynamic>().ToList()},
                {"CargoLimits", limitsDataSets.CargoLimits.Cast<dynamic>().ToList()},
                {"GlLimits", limitsDataSets.GlLimits.Cast<dynamic>().ToList()},
                {"RefLimits", limitsDataSets.RefLimits.Cast<dynamic>().ToList()},
                {"CollisionLimits", limitsDataSets.CollisionLimits.Cast<dynamic>().ToList()},
                {"ComprehensiveLimits", limitsDataSets.ComprehensiveLimits.Cast<dynamic>().ToList()},
                {"TaxDetails", taxDetails.Cast<dynamic>().ToList()},
                {"SLBrokers", slBrokers.Cast<dynamic>().ToList()},
                {"MainUse", mainUse.Cast<dynamic>().ToList()},
                {"States", states.Cast<dynamic>().ToList()},
                {"InstallmentSchedule", installmentSchedule.Cast<dynamic>().ToList()},
                {"PolicyVehicles", policyVehicles.Cast<dynamic>().ToList()},
                {"VehiclePremiumChanges", new List<VehiclePremiumChange>().Cast<dynamic>().ToList()},
                {"CommonValues", commonValues.Cast<dynamic>().ToList()}
            };

            return dataSets;
        }

        private static async Task<List<StateInfo>> GetStatesList()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(STATESERVICEURL);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<List<StateInfo>>(await response.Content.ReadAsStringAsync());
        }

    }
}
