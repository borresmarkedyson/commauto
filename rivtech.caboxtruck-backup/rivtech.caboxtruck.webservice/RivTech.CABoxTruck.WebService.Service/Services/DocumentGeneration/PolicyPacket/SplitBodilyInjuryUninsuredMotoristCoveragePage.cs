﻿using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Forms;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class SplitBodilyInjuryUninsuredMotoristCoveragePage : CommonValuesPage
    {
        public SplitBodilyInjuryUninsuredMotoristCoveragePage(ReportData reportData) : base(reportData)
        {
            TemplateName = "Split Bodily Injury Uninsured Motorist Coverage.docx";
            PageOrder = 40;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            var effectiveDate = EffectiveDate;

            var formOtherContent = ReportData.Risk.RiskForms.FirstOrDefault(f => f.FormId == "SplitBodilyInjuryUninsuredMotoristCoverage")?.Other;

            if (formOtherContent != null)
            {
                var formValues = JsonConvert.DeserializeObject<FormUMBIDTO>(formOtherContent);
                effectiveDate = formValues.UpdatedDate.ToString(DateFormat);
            }

            FormValues.Add("EndorsementEffectiveDate", effectiveDate);
        }
    }
}