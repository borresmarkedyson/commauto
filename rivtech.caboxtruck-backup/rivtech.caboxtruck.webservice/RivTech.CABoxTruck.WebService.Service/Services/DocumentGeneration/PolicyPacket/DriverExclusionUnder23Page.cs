﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class DriverExclusionUnder23Page : CommonValuesPage
    {
        public DriverExclusionUnder23Page(ReportData reportData) : base(reportData)
        {
            TemplateName = "Driver Exclusion under 23.docx";
            PageOrder = 21;
        }
    }
}