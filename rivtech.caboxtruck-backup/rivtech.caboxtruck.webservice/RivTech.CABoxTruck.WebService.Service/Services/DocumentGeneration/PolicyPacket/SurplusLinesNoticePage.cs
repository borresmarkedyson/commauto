﻿using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class SurplusLinesNoticePage : CommonValuesPage
    {
        public SurplusLinesNoticePage(ReportData reportData) : base(reportData)
        {
            var applicant = ReportData.Risk.NameAndAddress;
            var templateName = GetSurplusLineStates().Contains(applicant?.State) ? applicant?.State : "Not-Available";

            TemplateName = $"Surplus Lines Notices/{templateName}.docx";
            PageOrder = 28;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            var applicant = ReportData.Risk.NameAndAddress;
            var bind = ReportData.Risk.Binding;
            var creditedOffice = ReportData.DataSets["CreditedOffice"]
                .FirstOrDefault(bt => bt.Id == bind?.CreditedOfficeId)?.Description;
            var slBroker = ReportData.DataSets["SLBrokers"].FirstOrDefault(d => d.Id == creditedOffice);
            FormValues.Add("SLBrokerCompanyName", SetAppliedCompanyNameByState(slBroker, applicant?.State));
            FormValues.Add("SLBrokerAddressLineOne", $"{slBroker?.Address1},");
            FormValues.Add("SLBrokerAddressCityStateZip", slBroker?.CityStateZip);
            FormValues.Add("SLProducerName", "Aaron Lynch");
            FormValues.Add("FormSignatory", "");
            FormValues.Add("PolicyState", applicant?.State);
        }

        private static IEnumerable<string> GetSurplusLineStates()
        {
            return new[]
            {
                "AL", "AZ", "CA", "CO", "DE", "GA", "IL", "KY", "MD", "NC",
                "NJ", "NV", "OH", "OR", "PA", "SC", "TN", "TX", "VA", "WA"
            };
        }

        private string SetAppliedCompanyNameByState(dynamic surplusLinesBroker, string state)
        {
            string slBrokerCompanyName = string.Empty;

            if (surplusLinesBroker?.Id == CreditedOffice.Applied.Description)
            {
                switch (state)
                {
                    case "CA":
                    case "FL":
                    case "MA":
                    case "NY":
                    case "TX":
                    case "WA":
                        slBrokerCompanyName = "Vale Insurance Partners";
                        break;
                    default:
                        slBrokerCompanyName = "Rivington Partners";
                        break;
                }
            } else
            {
                slBrokerCompanyName = surplusLinesBroker?.Description;
            }

            return slBrokerCompanyName;
        }
    }
}