﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class MotorTruckCargoEndorsementPage : CommonValuesPage
    {
        public MotorTruckCargoEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "Motor Truck Cargo Endorsement.docx";
            PageOrder = 31;
        }
    }
}