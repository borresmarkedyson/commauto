﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class RefrigerationCargoEndorsementPage : CommonValuesPage
    {
        public RefrigerationCargoEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "BT Refrigeration Cargo Endorsement.docx";
            PageOrder = 38;
        }
    }
}