﻿using System.Linq;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class ManuscriptEndorsementPage : CommonValuesPage
    {
        public ManuscriptEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "Manuscript Endorsement.docx";
            PageOrder = 32;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            var manuscript = ReportData.Risk.RiskManuscripts.FirstOrDefault(m => m.Id == ReportData.Id);

            var premium = (IsEndorsement ? manuscript?.ProratedPremium : manuscript?.Premium) ?? 0;
            FormValues.Add("Premium", premium.ToCurrency());

            FormValues.Add("Description", manuscript?.Description);
        }
    }
}