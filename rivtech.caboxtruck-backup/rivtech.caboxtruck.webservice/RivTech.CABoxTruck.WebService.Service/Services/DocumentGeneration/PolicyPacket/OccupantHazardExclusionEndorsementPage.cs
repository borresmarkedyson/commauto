﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class OccupantHazardExclusionEndorsementPage : CommonValuesPage
    {
        public OccupantHazardExclusionEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "OccupantHazardExclusion.docx";
            PageOrder = 8;
        }
    }
}