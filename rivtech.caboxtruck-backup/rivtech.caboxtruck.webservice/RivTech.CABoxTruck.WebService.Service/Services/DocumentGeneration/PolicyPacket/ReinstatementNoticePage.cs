﻿using System.Linq;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class ReinstatementNoticePage : CommonValuesPage
    {
        public ReinstatementNoticePage(ReportData reportData) : base(reportData)
        {
            TemplateName = "ReinstatementNotice.docx";
            PageOrder = 1;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            var nameAddress = ReportData.Risk.NameAndAddress;
            var billingAddress = ReportData.DataSets["Addresses"].FirstOrDefault(a => a.AddressTypeId == (short) AddressTypesEnum.Billing)?.Address;

            FormValues.Add("Company1", CompanyName);
            FormValues.Add("AddressOne", "1900 L Don Dodson Drive");
            FormValues.Add("AddressTwo", "Bedford, TX 76021");

            FormValues.Add("Contact1", nameAddress?.InsuredDBA);
            FormValues.Add("Insured2", nameAddress?.BusinessName);
            FormValues.Add("InsuredAddressOne1", billingAddress?.StreetAddress1);
            FormValues.Add("InsuredAddressTwo1", $"{billingAddress?.City} {billingAddress?.State} {billingAddress?.ZipCode}");

            FormValues.Add("Contact", nameAddress?.InsuredDBA);
            FormValues.Add("Insured1", nameAddress?.BusinessName);
            FormValues.Add("InsuredAddressOne", billingAddress?.StreetAddress1);
            FormValues.Add("InsuredAddressTwo", $"{billingAddress?.City} {billingAddress?.State} {billingAddress?.ZipCode}");

            FormValues.Add("Company", CompanyName);
            FormValues.Add("Insured", nameAddress?.BusinessName);

            FormValues.Add("Contact2", nameAddress?.InsuredDBA);
            FormValues.Add("Insured3", nameAddress?.BusinessName);
            FormValues.Add("InsuredAddressOne2", billingAddress?.StreetAddress1);
            FormValues.Add("InsuredAddressTwo2", $"{billingAddress?.City} {billingAddress?.State} {billingAddress?.ZipCode}");
        }
    }
}