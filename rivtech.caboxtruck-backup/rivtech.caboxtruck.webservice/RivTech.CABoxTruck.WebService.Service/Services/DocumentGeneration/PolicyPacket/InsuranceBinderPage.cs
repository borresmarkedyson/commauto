﻿using System;
using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class InsuranceBinderPage : CommonValuesPage
    {
        public InsuranceBinderPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "Insurance Binder New System.docx";
            PageOrder = 2;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            FormValues.Add("Insured", BusinessName);
            FormValues.Add("Address", BusinessAddress);
            FormValues.Add("CurrentDate", DateTime.Now.ToString(DateFormat));

            var taxDetail = ReportData.DataSets["TaxDetails"].FirstOrDefault(td => td.Id == Coverage.RiskCoveragePremium?.TaxDetailsId);
            var (slPercent, sfPercent) = GetTaxPercentage();
            FormValues.Add("TotalPremium", $"${Coverage.RiskCoveragePremium?.Premium ?? 0:N2}");
            FormValues.Add("TotalTaxes", $"${Coverage.RiskCoveragePremium?.TaxesAndStampingFees ?? 0:N2}");
            FormValues.Add("SurplusLineTax", $"${taxDetail?.TotalSLTax ?? 0:N2} ({slPercent}%)");
            FormValues.Add("StampingFee", $"${taxDetail?.TotalStampingFee ?? 0:N2} ({sfPercent}%)");

            var fees = (Coverage.RiskCoveragePremium?.RiskMgrFeeAL ?? 0) + (Coverage.RiskCoveragePremium?.RiskMgrFeePD ?? 0);
            FormValues.Add("PolicyFees", $"${fees:N2}");
            FormValues.Add("TotalDue", $"${Coverage.RiskCoveragePremium?.TotalAmounDueFull ?? 0:N2}");
            FormValues.Add("MinimumEarned", $"{ReportData.Risk.Binding?.MinimumEarned:0.#}%");

            FormValues.Add("ALLimit", GetLimitsText(Coverage.AutoBILimitId, "AutoBILimits"));
            FormValues.Add("ALPremium", $"${Coverage.RiskCoveragePremium?.AutoLiabilityPremium ?? 0:N0}");

            var alManuscriptPremium = Coverage?.RiskCoveragePremium?.ALManuscriptPremium ?? 0;
            if (alManuscriptPremium > 0)
            {
                FormValues.Add("ALManuscriptPremium", $"AL Manuscript Premium: ${alManuscriptPremium:N0}");
            }

            TableValues = new Dictionary<string, List<Dictionary<string, string>>>();
            SetAutoPhysicalDamage();
            SetGeneralLiability();
            SetMotorTruckCargo();
            SetRefrigeration();
        }

        private void SetAutoPhysicalDamage()
        {
            var apdItems = new List<string>();
            if (Coverage.CollDeductibleId > LimitConstants.CollisionNoCoverageId)
            {
                apdItems.Add($"Collision Deductible: {GetLimitsText(Coverage.CollDeductibleId, "CollisionLimits")}");
                apdItems.Add($"Premium: {CollisionPremium.ToCurrency(0)}");
            }

            if (Coverage.CompDeductibleId > LimitConstants.ComprehensiveNoCoverageId)
            {
                apdItems.Add($"Comprehensive Deductible: {GetLimitsText(Coverage.CompDeductibleId, "ComprehensiveLimits")}");
                apdItems.Add($"Premium: {ComprehensivePremium.ToCurrency(0)}");
            }

            if (Coverage.FireDeductibleId > LimitConstants.ComprehensiveNoCoverageId)
            {
                apdItems.Add($"Fire / Theft Deductible: {GetLimitsText(Coverage.FireDeductibleId, "ComprehensiveLimits")}");
                apdItems.Add($"Premium: {ComprehensivePremium.ToCurrency(0)}");
            }
            var entries = apdItems.Select(apdItem => new Dictionary<string, string> {{"Table_APD_Item", apdItem}}).ToList();
            TableValues.Add("APD_", entries);

            var pdAdditionals = new List<Dictionary<string, string>>();

            var pdManuscriptPremium = Coverage?.RiskCoveragePremium?.PDManuscriptPremium ?? 0;
            if (pdManuscriptPremium > 0)
            {
                pdAdditionals.Add(new Dictionary<string, string> {{"Table_APDAdditional_Item", $"PD Manuscript Premium: ${pdManuscriptPremium:N0}"}});
            }

            var hiredPdPremium = Coverage?.RiskCoveragePremium?.HiredPhysicalDamage ?? 0;
            if (IsFormSelected("RPCA015") && hiredPdPremium > 0)
            {
                pdAdditionals.Add(new Dictionary<string, string> { { "Table_APDAdditional_Item", $"Hired Physical Damage Premium: ${hiredPdPremium:N0}" } });
            }

            var trailerPremium = Coverage?.RiskCoveragePremium?.TrailerInterchange ?? 0;
            if (IsFormSelected("RPCA029") && trailerPremium > 0)
            {
                pdAdditionals.Add(new Dictionary<string, string> { { "Table_APDAdditional_Item", $"Trailer Interchange Premium: ${trailerPremium:N0}" } });
            }

            TableValues.Add("APDAdditional_", pdAdditionals);
        }

        private void SetGeneralLiability()
        {
            var glItems = new List<string>();
            if (Coverage.GLBILimitId > LimitConstants.GLNoneCoverageId)
            {
                glItems.Add($"Occurrence: ${GetUpperLimitsText(Coverage.GLBILimitId, "GlLimits")}");
                glItems.Add($"Aggregate: ${GetLowerLimitsText(Coverage.GLBILimitId, "GlLimits")}");
                glItems.Add($"Premium: ${Coverage.RiskCoveragePremium?.GeneralLiabilityPremium ?? 0:N0} ");
            }
            var entries = glItems.Select(glItem => new Dictionary<string, string> {{"Table_GL_Item", glItem}}).ToList();
            TableValues.Add("GL_", entries);
        }

        private void SetMotorTruckCargo()
        {
            var mtcItems = new List<string>();
            if (Coverage.CargoLimitId > LimitConstants.CargoNoneCoverageId)
            {
                mtcItems.Add($"Limit: ${GetUpperLimitsText(Coverage.CargoLimitId, "CargoLimits")}");
                mtcItems.Add($"Premium: ${Coverage.RiskCoveragePremium?.CargoPremium ?? 0:N0} ");
            }
            var entries = mtcItems.Select(glItem => new Dictionary<string, string> {{"Table_MTC_Item", glItem}}).ToList();
            TableValues.Add("MTC_", entries);
        }

        private void SetRefrigeration()
        {
            var refItems = new List<string>();
            if (Coverage.RefCargoLimitId > LimitConstants.RefNoneCoverageId)
            {
                refItems.Add($"Limit: ${GetUpperLimitsText(Coverage.RefCargoLimitId, "RefLimits")}");
                refItems.Add($"Premium: {RefrigerationPremium} ");
            }
            var entries = refItems.Select(refItem => new Dictionary<string, string> {{"Table_Ref_Item", refItem}}).ToList();
            TableValues.Add("Ref_", entries);
        }

        private bool IsFormSelected(string formNumber)
        {
            var riskForm = ReportData.Risk.RiskForms?.FirstOrDefault(rf =>
                rf.Form?.FormNumber == formNumber && rf.IsSelected.GetValueOrDefault());
            return riskForm != null;
        }

        private (string taxPercent, string stampingFeePercent) GetTaxPercentage()
        {
            var taxPercent = "0";
            var stampingFeePercent = "0";
            switch (PolicyState)
            {
                case "CA":
                    taxPercent = "3";
                    stampingFeePercent = "0.25";
                    break;
                case "NJ":
                    taxPercent = "5";
                    stampingFeePercent = "0";
                    break;
                case "TX":
                    taxPercent = "4.85";
                    stampingFeePercent = "0.075";
                    break;
            }
            return (taxPercent, stampingFeePercent);
        }
    }
}