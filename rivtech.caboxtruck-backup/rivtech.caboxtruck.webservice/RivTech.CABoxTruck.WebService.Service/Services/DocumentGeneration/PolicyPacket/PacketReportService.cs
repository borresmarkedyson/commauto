﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.DocumentGeneration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.DTO.Enum;
using RivTech.CABoxTruck.WebService.Service.Services.Submission;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DTO.Constants;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class PacketReportService : IPacketReportService
    {
        private const string UPLOAD_PATH = "risk-document";
        private readonly IReportService _reportService;
        private readonly IContentService _contentService;
        private readonly IFileUploadDocumentService _fileUploadDocumentService;
        private readonly IApplogService _applogService;
        private readonly ILog4NetService _errorLogservice;
        private readonly IConfiguration _configuration;
        private readonly IFormsService _formsService;
        private readonly IRiskFactory _riskFactory;
        private readonly IFTPClientServerSevice _FTPClientServerSevice;

        public PacketReportService(IReportService reportService, 
            IContentService contentService,
            IFileUploadDocumentService fileUploadDocumentService,
            IApplogService applogService,
            ILog4NetService errorLogservice,
            IConfiguration configuration,
            IFormsService formsService,
            IRiskFactory riskFactory,
            IFTPClientServerSevice FTPClientServerSevice
            )
        {
            _reportService = reportService;
            _contentService = contentService;
            _fileUploadDocumentService = fileUploadDocumentService;
            _applogService = applogService;
            _errorLogservice = errorLogservice;
            _configuration = configuration;
            _formsService = formsService;
            _riskFactory = riskFactory;
            _FTPClientServerSevice = FTPClientServerSevice;
        }

        public async Task<string> GeneratePolicyPacketReport(List<RiskFormDTO> forms)
        {
            var riskDetailId = forms[0].RiskDetailId;
            var reportData = await _contentService.GetReportData(riskDetailId);
            forms = await _riskFactory.GetRiskFormsWithConditionals(reportData.Risk, riskForms: forms);
            reportData.Risk.RiskForms = forms;

            var packetFactory = new PolicyPacketPageFactory(riskDetailId, reportData, GetAttachedPages(forms));
            var formsIncluded = packetFactory.Factories.Values.Where(x => x.isAttached).ToList();

            var fileName = forms.Count == 1 
                ? forms[0].Form?.IsSupplementalDocument == true ? RemoveInvalidChars(forms[0].Form?.Name) : forms[0].Form?.FormNumber 
                : "PolicyPacket";
            var packetReportBuilder = ReportBuilder.Init();
            var reportPayload = packetReportBuilder.SetUploadPath($"{UPLOAD_PATH}/CA/{DateTime.Now:yyyy-MM}/{riskDetailId}/{fileName}.pdf")
            .AddPage(formsIncluded)
            .Build();

            var result = await _reportService.GenerateFileReportLink(reportPayload);

            await _applogService.LogDocumentGeneration($"RiskDetailId:{riskDetailId}", JsonConvert.SerializeObject(reportPayload), result ?? "Generate file unsuccessful");
            return result;
        }

        public async Task<string> GenerateSavePolicyPacketReport(Guid riskDetailId, EndorsementDetailsDTO endorsement, string fileName = null, Guid? previousRiskDetailId = null)
        {
            var newRiskDetailId = endorsement.Risk.Id.GetValueOrDefault();
            var reportData = await _contentService.GetReportData(riskDetailId, true);
            var isEndorsement = fileName != null;
            var forms = await _riskFactory.GetRiskFormsWithConditionals(reportData.Risk, isEndorsement);
            reportData.Risk.RiskForms = forms;
            reportData.Endorsement = endorsement;
            if (previousRiskDetailId.HasValue)
                riskDetailId = previousRiskDetailId.Value;

            var packetFactory = new PolicyPacketPageFactory(riskDetailId, reportData, GetAttachedPages(forms));
            var formsIncluded = packetFactory.Factories.Values.Where(x => x.isAttached).ToList();

            fileName ??= forms.Count == 1 ? forms[0].Form?.FormNumber : "PolicyPacket";

            var packetReportBuilder = ReportBuilder.Init();
            var reportPayload = packetReportBuilder.SetUploadPath($"{UPLOAD_PATH}/CA/{DateTime.Now:yyyy-MM}/{newRiskDetailId}/{fileName}.pdf")
                .AddPage(formsIncluded)
                .Build();

            #region SaveFileUploadDocument
            // Save to FileUploadDocument
            var reportServiceSection = _configuration.GetSection("Report");
            var baseUrl = reportServiceSection.GetValue<string>("FileUrl");
            var descPrefix = isEndorsement ? $"Endorsement {endorsement.EndorsementNumber} " : ""; 
            var policypacketdocument = new FileUploadDocumentDTO()
            {
                tableRefId = newRiskDetailId,
                RiskDetailId = newRiskDetailId,
                FileCategoryId = 1, // Miscellaneous
                Description = $"{descPrefix}Policy Packet - {DateTime.Now:MM/dd/yyyy}",
                FileName = $"{fileName}.pdf",
                FilePath = $"{baseUrl}{reportPayload.Upload}",
                IsUploaded = false,
                IsSystemGenerated = true,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
            };
            var fileresult = await _fileUploadDocumentService.AddUpdateAsync(policypacketdocument);
            #endregion

            GenerateFile(new[] {new {fileresult.Id, Payload = reportPayload}});
            
            return reportPayload.Upload;
        }

        public async Task GenerateEndorsementDocuments(List<EndorsementFormDTO> endorsementForms, InvoiceDTO billing, EndorsementDetailsDTO endorsement)
        {
            var riskDetailId = endorsement.Risk.Id.GetValueOrDefault();
            var endorsementDocuments = new List<dynamic>();
            var reportData = await _contentService.GetReportData(riskDetailId, true);
            reportData.Risk.RiskForms = await _riskFactory.GetRiskFormsWithConditionals(reportData.Risk, true);
            reportData.Billing = billing;
            reportData.Endorsement = endorsement;
            foreach (var endorsementForm in endorsementForms)
            {
                var packetFactory = new PolicyPacketPageFactory(riskDetailId, reportData, GetAttachedPages(endorsementForm.RiskForms));
                var formsIncluded = packetFactory.Factories.Values.Where(x => x.isAttached).ToList();

                var fileName = endorsementForm.FileName;
                var packetReportBuilder = ReportBuilder.Init();
                var reportPayload = packetReportBuilder.SetUploadPath($"{UPLOAD_PATH}/CA/{DateTime.Now:yyyy-MM}/{riskDetailId}/{fileName}.pdf")
                    .AddPage(formsIncluded)
                    .Build();

                var reportServiceSection = _configuration.GetSection("Report");
                var baseUrl = reportServiceSection.GetValue<string>("FileUrl");
                var policypacketdocument = new FileUploadDocumentDTO
                {
                    tableRefId = riskDetailId,
                    RiskDetailId = riskDetailId,
                    FileCategoryId = (short?)((endorsementForm.Description == "Final Audit Invoice") ? 3 : 1),
                    Description = endorsementForm.Description,
                    FileName = $"{fileName}.pdf",
                    FilePath = $"{baseUrl}{reportPayload.Upload}",
                    IsUploaded = false,
                    IsSystemGenerated = true,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                };
                var fileresult = await _fileUploadDocumentService.AddUpdateAsync(policypacketdocument);
                endorsementDocuments.Add(new {fileresult.Id, Payload = reportPayload});
            }
            GenerateFile(endorsementDocuments);
        }

        private void GenerateFile(IReadOnlyCollection<dynamic> documents)
        {
            var thread = new Thread(() =>
            {
                var contextFactory = new DbContextFactory();
                var connectionString = _configuration.GetSection("ConnectionStrings").GetValue<string>("ConCABoxTruck");
                using var context = contextFactory.Create(connectionString);
                
                foreach (var document in documents)
                {
                    try
                    {
                        var documentId = (Guid)document.Id;
                        var model = context.FileUploadDocument.FirstOrDefault(x => x.Id == documentId);
                        if (model == null) throw new Exception("File Upload Document not found");

                        var result = _reportService.GenerateFileReportLink(document.Payload).Result;
                        if (result == null) throw new Exception($"Generate file unsuccessful:{model.RiskDetailId}");

                        model.IsUploaded = true;
                        context.FileUploadDocument.Update(model);

                        _applogService.LogDocumentGeneration($"Document:{document.Id}", JsonConvert.SerializeObject(document.Payload), result);

                        // Save to FTPDocument for Covenir
                        var source = "";
                        if (model.Description == GenerateDocumentDescription.Cancellation || model.Description == GenerateDocumentDescription.FinalCancellation)
                            source = GenerateDocumentSource.Cancellation;

                        if (model.Description == GenerateDocumentDescription.NonPayNoticePendingCancellation)
                            source = GenerateDocumentSource.NonPayNoticePendingCancellation;

                        if (!string.IsNullOrEmpty(source))
                        {
                            var riskDetails = context.RiskDetail.FirstOrDefault(x => x.Id == model.RiskDetailId);
                            context.FTPDocumentTemporary.Add(new FTPDocumentTemporary()
                            {
                                RiskId = riskDetails.RiskId,
                                RiskDetailId = model.RiskDetailId,
                                Description = model.Description,
                                FileName = model.FileName,
                                FilePath = model.FilePath,
                                IsUploaded = false,
                                Source = source,
                                BatchId = DateTime.Now.ToString("MMddyyyy"),
                                IsCompiled = false,
                                Category = "MISC"
                            });
                            context.FTPDocument.Add(new FTPDocument()
                            {
                                RiskId = riskDetails.RiskId,
                                RiskDetailId = model.RiskDetailId,
                                Description = model.Description,
                                FileName = model.FileName,
                                FilePath = model.FilePath,
                                IsUploaded = false,
                                Source = source,
                                BatchId = DateTime.Now.ToString("MMddyyyy"),
                                IsCompiled = false,
                                Category = "MISC"
                            });
                        }

                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        _errorLogservice.Error(ex);
                        _applogService.LogDocumentGeneration($"Document:{document.Id}", JsonConvert.SerializeObject(document.Payload), "Generate file unsuccessful");

                    }
                }
            });

            thread.Start();
        }

        private static IEnumerable<PageInfo> GetAttachedPages(IEnumerable<RiskFormDTO> forms)
        {
            var attachedForms = new List<PageInfo>();
            
            foreach (var form in forms)
            {
                attachedForms.Add(new PageInfo
                {
                    Id = form.Id.ToString(),
                    FormId = form.Form.Id,
                    PageOrder = form.Form.SortOrder,
                    TemplateName = form.Form.FileName
                });
            }

            return attachedForms;
        }

        public string RemoveInvalidChars(string filename)
        {
            return string.Concat(filename.Split(Path.GetInvalidFileNameChars()));
        }
    }
}
