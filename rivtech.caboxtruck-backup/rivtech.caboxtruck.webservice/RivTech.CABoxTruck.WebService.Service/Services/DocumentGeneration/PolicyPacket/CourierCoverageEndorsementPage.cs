﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class CourierCoverageEndorsementPage : CommonValuesPage
    {
        public CourierCoverageEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "BT Courier Coverage Endorsement.docx";
            PageOrder = 37;
        }
    }
}