﻿using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class GeneralLiabilityEndorsementPage : CommonValuesPage
    {
        public GeneralLiabilityEndorsementPage(ReportData reportData) : base(reportData)
        {
            TemplateName = "General Liability Endorsement.docx";
            PageOrder = 30;
        }
    }
}