﻿using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket
{
    public class InstallmentSchedulePage : CommonValuesPage
    {
        public InstallmentSchedulePage(ReportData reportData) : base(reportData)
        {
            TemplateName = "Installment Schedule.docx";
            PageOrder = 4;
        }

        protected override void SetFormValues()
        {
            base.SetFormValues();

            var installments = ReportData.DataSets["InstallmentSchedule"].Cast<InstallmentAndInvoiceDTO>().ToList();
            var deposit = installments.FirstOrDefault();
            if (deposit != null) deposit.Premium += deposit.Fee;

            var installmentSchedules = new List<Dictionary<string, string>>();
            foreach (var installment in installments)
                installmentSchedules.Add(new Dictionary<string, string>
                {
                    {"Table_Installments_DueDate", $"{installment.DueDate.ToString(DateFormat)}"},
                    {"Table_Installments_PremiumDue", installment.Premium.ToCurrency()},
                    {"Table_Installments_Fee", installment.FutureInstallmentFee.ToCurrency()},
                    {"Table_Installments_TaxDue", installment.Tax.ToCurrency()},
                    {"Table_Installments_TotalDue", installment.TotalBilled.ToCurrency()}
                });

            TableValues = new Dictionary<string, List<Dictionary<string, string>>>
            {
                {"Installments_", installmentSchedules}
            };
        }

    }

}