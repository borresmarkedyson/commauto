﻿using System;
using RivTech.CABoxTruck.WebService.DTO.DocumentGeneration;
using RivTech.CABoxTruck.WebService.Service.IServices.DocumentGeneration;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants;

namespace RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.QuoteReport
{
    public class QuotePage : SinglePage, IPage
    {
        private decimal[] _optionTotals;
        public string TemplateName => "Quote.docx";
        public int PageOrder => 1;

        public QuotePage(ReportData reportData) : base(reportData)
        {
        }

        public Form GetData()
        {
            var formData = new Form
            {
                PageOrder = PageOrder,
                TemplateName = SetTemplate(),
                FormValues = GetFormValues(),
                TableValues = GetTableValues()
            };
            SetPremiumTotals(formData);
            return formData;
        }

        private string SetTemplate()
        {
            if (ReportData.Risk.RiskCoverages.Count == 1)
                return "documenttemplate/Quote/Quote1optn.docx";

            if (ReportData.Risk.RiskCoverages.Count == 2)
                return "documenttemplate/Quote/Quote2optn.docx";

            return $"documenttemplate/Quote/{TemplateName}";
        }

        private void SetPremiumTotals(Form formData)
        {
            for (var i = 0; i < ReportData.Risk.RiskCoverages.Count; i++)
            {
                var totalValue = ReportData.Risk.RiskCoverages[i].RiskCoveragePremium?.Premium > 0 ? $"${_optionTotals[i]:N2}" : "";
                formData.FormValues.Add($"PremiumTotalOpt{i + 1}", totalValue);
            }
        }

        private Dictionary<string, string> GetFormValues()
        {
            const string dateFormat = "MMMM d, yyyy";
            var agentName = ReportData.Risk.BrokerInfo?.Agent?.Entity?.FullName;
            var agencyName = ReportData.Risk.BrokerInfo?.Agency?.Entity?.CompanyName;
            var applicant = ReportData.Risk.NameAndAddress;
            var policyEffectiveDate = ReportData.Risk.BrokerInfo?.EffectiveDate?.ToString(dateFormat);
            var quoteExpirationDate = DateTime.Now.AddDays(30).ToString(dateFormat);
            var businessType = ReportData.DataSets["BusinessTypes"]
                .FirstOrDefault(bt => bt.Id == ReportData.Risk.BusinessDetails?.BusinessTypeId)?.Description;
            var policyContact = ReportData.DataSets["PolicyContacts"].FirstOrDefault(pc => pc.Position == "Product Underwriter" && pc.Id == ReportData.Risk.BrokerInfo?.ProductUWUserId);
            var creditedOffice = ReportData.DataSets["CreditedOffice"]
                .FirstOrDefault(bt => bt.Id == ReportData.Risk.Binding?.CreditedOfficeId)?.Description;
            var formValues = new Dictionary<string, string>
            {
                {"QuoteDate", DateTime.Now.ToString(dateFormat)},
                {"AgentName", agentName},
                {"AgentName2", agentName},
                {"AgencyName", agencyName},
                {"AgencyName2", agencyName},
                {"ProposedInsured", ReportData.Risk.NameAndAddress?.BusinessName},
                {"PolicyEffectiveDate", policyEffectiveDate},
                {"PolicyEffectiveDate2", policyEffectiveDate},
                {"PolicyEffectiveDate3", policyEffectiveDate},
                {"UWName", policyContact?.Name},
                {"UWEmail", policyContact?.Email},
                {"QuoteNumber", ReportData.Risk.SubmissionNumber}, // TBC
                {"QuoteNumber2", ReportData.Risk.SubmissionNumber}, // TBC
                {"QuoteType", "New"}, // TBD (New/Renewal)
                {"Industry", businessType},
                {"QuoteDateTime", DateTime.Now.ToString("MMMM dd, yyyy hh:mm tt")},
                {"QuoteDateTime2", DateTime.Now.ToString("MMMM dd, yyyy hh:mm tt")},
                {"QuoteExpirationDate", quoteExpirationDate},
                {"QuoteExpirationDate2", quoteExpirationDate},
                {"PresentedBy", creditedOffice},

                //APPLICANT
                {"ApplicantName", applicant?.BusinessName},
                {"ApplicantName2", applicant?.BusinessName},
                {"ApplicantAddress1", applicant?.BusinessAddress},
                {"ApplicantAddress2", applicant?.BusinessAddress},
                {"ApplicantCity", applicant?.City},
                {"ApplicantState", applicant?.State},
                {"ApplicantZipCode", applicant?.ZipCode},

                {"DisclaimerText", GetDisclaimerText()},
            };

            return formValues;
        }

        private Dictionary<string, List<Dictionary<string, string>>> GetTableValues()
        {
            var tableValues = new Dictionary<string, List<Dictionary<string, string>>>
            {
                {"Premium", GetFromPremiums() },
                {"BindRequirement", GetFromBindRequirements() },
                {"QuoteCondition", GetFromQuoteConditions() },
                {"Coverage", GetFromCoverages() },
                {"PolPrem", GetFromPolicyPremiums() },
                {"PolFee", GetFromPolicyFees() },
                {"PolTotal", GetFromPolicyTotals() },
                {"PolPayFull", GetFromPolicyPaymentOptions() },
                {"PolPayInst", GetFromPolicyPaymentInstalls() },
                {"PolPayInstPrem", GetFromPolicyPaymentInstallPremiums() }
            };

            return tableValues;
        }

        private List<Dictionary<string, string>> GetFromPolicyPremiums()
        {
            var premiumEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{"Table_PolPremDesc", "Liability" } },
                new Dictionary<string, string> {{"Table_PolPremDesc", "Cargo" } },
                new Dictionary<string, string> {{"Table_PolPremDesc", "Physical Damage" } },
                new Dictionary<string, string> {{"Table_PolPremDesc", "Vehicle Level Total" } },
                new Dictionary<string, string> {{"Table_PolPremDesc", "General Liability" } }
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PolPremOption{i + 1}";
                var liability = "";
                var cargo = "";
                var physicalDamage = "";
                var vehicleLevelTotal = "";
                var generalLiability = "";

                if (HasPremiumForOption(i))
                {
                    liability = $"${coverages[i].RiskCoveragePremium?.AutoLiabilityPremium ?? 0:N2}";
                    cargo = $"${coverages[i].RiskCoveragePremium?.CargoPremium ?? 0:N2}";
                    physicalDamage = $"${coverages[i].RiskCoveragePremium?.PhysicalDamagePremium ?? 0:N2}";
                    vehicleLevelTotal = $"${coverages[i].RiskCoveragePremium?.VehicleLevelPremium ?? 0:N2}";
                    generalLiability = $"${coverages[i].RiskCoveragePremium?.GeneralLiabilityPremium ?? 0:N2}";
                }

                var hasApd = coverages[0].CompDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                             || coverages[0].FireDeductibleId > LimitConstants.ComprehensiveNoCoverageId
                             || coverages[0].CollDeductibleId > LimitConstants.CollisionNoCoverageId;

                premiumEntries[0].Add(bookmark, liability);
                premiumEntries[1].Add(bookmark, coverages[0].CargoLimitId == 53 ? "NA" : cargo);
                premiumEntries[2].Add(bookmark, !hasApd ? "NA" : physicalDamage);
                premiumEntries[3].Add(bookmark, vehicleLevelTotal);
                premiumEntries[4].Add(bookmark, coverages[0].GLBILimitId == 87 ? "NA" : generalLiability);
            }

            premiumEntries.RemoveAll(HasNoCoveragePremium);
            return premiumEntries;
        }

        private QuoteTableEntry<string> CreateFeeValuesEntry(string description, string propertyName)
        {
            var fee = new QuoteTableEntry<string> {Description = description};
            string[] feeValues = {"","",""};
            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                if (HasPremiumForOption(i))
                {
                    var sourceObject = coverages[i].RiskCoveragePremium;
                    var feeValue = sourceObject.GetType().GetProperty(propertyName)?.GetValue(sourceObject, null) ?? 0;

                    switch (propertyName)
                    {
                        case "RiskMgrFeeAL":
                        case "RiskMgrFeePD":
                        case "AdditionalInsuredPremium":
                        case "WaiverOfSubrogationPremium":
                        case "PrimaryNonContributoryPremium":
                            feeValues[i] = $"${feeValue:N0}";
                            break;
                        default:
                            feeValues[i] = $"${feeValue:N2}";
                            break;
                    }
                }
            }
            fee.Option1Value = feeValues[0];
            fee.Option2Value = feeValues[1];
            fee.Option3Value = feeValues[2];

            return fee;
        }

        private List<Dictionary<string, string>> GetFromPolicyFees()
        {
            var fees = new List<QuoteTableEntry<string>>();

            var coverages = ReportData.Risk.RiskCoverages;

            if (coverages.Any(cov => (cov.RiskCoveragePremium?.AdditionalInsuredPremium ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Additional Insured Charge", "AdditionalInsuredPremium"));
            if (coverages.Any(cov => (cov.RiskCoveragePremium?.WaiverOfSubrogationPremium ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Waiver of Subrogation Charge", "WaiverOfSubrogationPremium"));
            if (coverages.Any(cov => (cov.RiskCoveragePremium?.PrimaryNonContributoryPremium ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Primary & Noncontributory Charge", "PrimaryNonContributoryPremium"));

            if (coverages.Any(cov => (cov.RiskCoveragePremium?.RiskMgrFeeAL ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Service Fee - AL", "RiskMgrFeeAL"));
            if (coverages.Any(cov => (cov.RiskCoveragePremium?.RiskMgrFeePD ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Service Fee - PD", "RiskMgrFeePD"));
            if (coverages.Any(cov => (cov.RiskCoveragePremium?.InstallmentFee ?? 0) > 0)) fees.Add(CreateFeeValuesEntry("Installment Fee", "TotalInstallmentFee"));

            return fees.Select(fee => new Dictionary<string, string>
                {
                    {"Table_PolFeeDesc", fee.Description}, 
                    {"Table_PolFeeOption1", fee.Option1Value}, 
                    {"Table_PolFeeOption2", fee.Option2Value}, 
                    {"Table_PolFeeOption3", fee.Option3Value},
                })
                .ToList();
        }

        private List<Dictionary<string, string>> GetFromPolicyTotals()
        {
            var tableEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{"Table_PolTotalDesc", "Premium" } },
                new Dictionary<string, string> {{"Table_PolTotalDesc", "Fees" } },
                new Dictionary<string, string> {{"Table_PolTotalDesc", "Taxes" } },
                new Dictionary<string, string> {{"Table_PolTotalDesc", "Stamping Fee" } }
            };

            _optionTotals = new decimal[] {0, 0, 0};

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PolTotalOption{i + 1}";
                var premium = "";
                var fees = "";
                var taxes = "";
                var stampingFee = "";

                if (HasPremiumForOption(i))
                {
                    var premiumValue = coverages[i].RiskCoveragePremium?.Premium ?? 0;
                    premium = $"${premiumValue:N2}";

                    var feesValue = coverages[i].RiskCoveragePremium?.Fees ?? 0;
                    fees = $"${feesValue:N2}";

                    var taxDetail = ReportData.DataSets["TaxDetails"].FirstOrDefault(td => td.Id == coverages[i].RiskCoveragePremium?.TaxDetailsId);

                    var taxesValue = taxDetail?.TotalSLTax ?? 0;
                    taxes = $"${taxesValue:N2}";

                    var stampingFeeValue = taxDetail?.TotalStampingFee ?? 0;
                    stampingFee = $"${stampingFeeValue:N2}";

                    var totalAmountDueInstallments = coverages[i].RiskCoveragePremium?.TotalAmountDueInstallment ?? 0;
                    _optionTotals[i] = totalAmountDueInstallments;
                }

                tableEntries[0].Add(bookmark, premium);
                tableEntries[1].Add(bookmark, fees);
                tableEntries[2].Add(bookmark, taxes);
                tableEntries[3].Add(bookmark, stampingFee);
            }

            return tableEntries;
        }

        private List<Dictionary<string, string>> GetFromPolicyPaymentOptions()
        {
            var tableEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{"Table_PolPayFullDesc", "Total" } },
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PolPayFullOption{i + 1}";
                var fullPolicyAmount = "";
                if (HasPremiumForOption(i))
                {
                    fullPolicyAmount = $"${coverages[i].RiskCoveragePremium?.TotalAmounDueFull ?? 0:N2}";
                }
                tableEntries[0].Add(bookmark, fullPolicyAmount);
            }

            return tableEntries;
        }

        private List<Dictionary<string, string>> GetFromPolicyPaymentInstalls()
        {
            var tableEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{"Table_PolPayInstDesc", "Deposit" } },
                new Dictionary<string, string> {{"Table_PolPayInstDesc", "# of Installments" } },
                new Dictionary<string, string> {{"Table_PolPayInstDesc", "Installment Total Due" } }
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PolPayInstOption{i + 1}";
                var depositAmount = "";
                var numberOfInstallments = "";
                var installmentTotalDue = "";
                
                if (HasPremiumForOption(i))
                {
                    depositAmount = $"${coverages[i].RiskCoveragePremium.DepositAmount ?? 0:N2}";
                    numberOfInstallments = coverages[i].NumberOfInstallments?.ToString();
                    installmentTotalDue = $"${coverages[i].RiskCoveragePremium?.PerInstallment ?? 0:N2}";
                   
                }
                tableEntries[0].Add(bookmark, depositAmount);
                tableEntries[1].Add(bookmark, numberOfInstallments ?? "");
                tableEntries[2].Add(bookmark, installmentTotalDue);
            }

            return tableEntries;
        }

        private List<Dictionary<string, string>> GetFromPolicyPaymentInstallPremiums()
        {
            var tableEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{"Table_PolPayInstPremDesc", "Premium" } },
                new Dictionary<string, string> {{"Table_PolPayInstPremDesc", "Fees" } },
                new Dictionary<string, string> {{"Table_PolPayInstPremDesc", "Taxes" } }
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PolPayInstPremOpt{i + 1}";
                var premium = "";
                var fees = "";
                var taxes = "";
               
                if (HasPremiumForOption(i))
                {
                    premium = $"${coverages[i].RiskCoveragePremium?.PerInstallmentPremium ?? 0:N2}";
                    fees = $"${coverages[i].RiskCoveragePremium?.PerInstallmentFee ?? 0:N2}";
                    taxes = $"${coverages[i].RiskCoveragePremium?.PerInstallmentTax ?? 0:N2}";
                }
                tableEntries[0].Add(bookmark, premium);
                tableEntries[1].Add(bookmark, fees);
                tableEntries[2].Add(bookmark, taxes);
            }

            return tableEntries;
        }

        private List<Dictionary<string, string>> GetFromPremiums()
        {
            var premiumEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> { { "Table_PremiumItem", "Estimated Premium" } },
                new Dictionary<string, string> { { "Table_PremiumItem", "Liability Commission %" } },
                new Dictionary<string, string> { { "Table_PremiumItem", "Liability Commission $" } },
                new Dictionary<string, string> { { "Table_PremiumItem", "Physical Damage Commission %" } },
                new Dictionary<string, string> { { "Table_PremiumItem", "Physical Damage Commission $" } }
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmark = $"Table_PremiumOption{i + 1}";
                var premium = "";
                var alCommission = "";
                var alCommissionAmount = "";
                var pdCommission = "";
                var pdCommissionAmount = "";

                if (HasPremiumForOption(i))
                {
                    var premiumValue = coverages[i].RiskCoveragePremium?.Premium ?? 0;
                    premium = $"${premiumValue:N2}";

                    var alCommissionValue = (string.IsNullOrEmpty(coverages[i].BrokerCommisionAL)) ? 0 : Convert.ToDecimal(coverages[i].BrokerCommisionAL);
                    alCommission = $"{alCommissionValue:0.00}%";
                    var alPremiumGroup = (coverages[i].RiskCoveragePremium?.AutoLiabilityPremium ?? 0)
                                         + (coverages[i].RiskCoveragePremium?.GeneralLiabilityPremium ?? 0)
                                         + (coverages[i].RiskCoveragePremium?.CargoPremium ?? 0);
                    alCommissionAmount = $"${alCommissionValue / 100 * alPremiumGroup:N2}";

                    var pdCommissionValue = string.IsNullOrEmpty(coverages[i].BrokerCommisionPD) ? 0 : Convert.ToDecimal(coverages[i].BrokerCommisionPD);
                    pdCommission = $"{pdCommissionValue:0.00}%";
                    var pdPremiumGroup = coverages[i].RiskCoveragePremium?.PhysicalDamagePremium ?? 0;
                    pdCommissionAmount = $"${pdCommissionValue / 100 * pdPremiumGroup:N2}";
                }

                premiumEntries[0].Add(bookmark, premium);
                premiumEntries[1].Add(bookmark, alCommission);
                premiumEntries[2].Add(bookmark, alCommissionAmount);
                premiumEntries[3].Add(bookmark, pdCommission);
                premiumEntries[4].Add(bookmark, pdCommissionAmount);
            }

            return premiumEntries;
        }

        private List<Dictionary<string, string>> GetFromBindRequirements()
        {
            var bindingRequirements = ReportData.Risk.BindingRequirements.Where(br => br.BindStatusId == BindStatus.Pending.Id);

            var bindRequirementItems = new List<string>();
            foreach (var requirement in bindingRequirements)
            {
                var bindRequirement = ReportData.DataSets["BindRequirementsOption"]
                    .FirstOrDefault(l => l.Id == requirement.BindRequirementsOptionId);
                bindRequirementItems.Add(bindRequirement?.Description == "Other" ? requirement.Describe : bindRequirement?.Description);
            }

            var tableValuesBindRequirements = new List<Dictionary<string, string>>();
            foreach (var item in bindRequirementItems.OrderBy(b => b))
            {
                tableValuesBindRequirements.Add(new Dictionary<string, string>
                {
                    { "Table_BindRequirementItem", $"{item}" }
                });
            }
            return tableValuesBindRequirements;
        }

        private List<Dictionary<string, string>> GetFromQuoteConditions()
        {
            var tableValuesQuoteConditions = new List<Dictionary<string, string>>();

            var quoteConditions = ReportData.Risk.QuoteConditions;

            var quoteConditionItems = new List<string>();
            foreach (var condition in quoteConditions)
            {
                var quoteCondition = ReportData.DataSets["QuoteConditionsOption"]
                    .FirstOrDefault(cc => cc.Id == condition.QuoteConditionsOptionId);
                quoteConditionItems.Add(quoteCondition?.Description == "Other" ? condition.Describe : quoteCondition?.Description);
            }

            foreach (var item in quoteConditionItems.OrderBy(q => q))
            {
                tableValuesQuoteConditions.Add(new Dictionary<string, string>
                {
                    { "Table_QuoteConditionItem", $"{item}" }
                });
            }

            return tableValuesQuoteConditions;
        }

        private static string GetDisclaimerText()
        {
            // TBD source
            return "Disclaimer Text";
        }

        private List<Dictionary<string, string>> GetFromCoverages()
        {
            var coverageEntries = new List<Dictionary<string, string>>
            {
                new Dictionary<string, string> {{ "Table_CoverageItem", "Limit of Liability" }},
                //new Dictionary<string, string> {{ "Table_CoverageItem", "Property Damage" } },  // not MVP
                new Dictionary<string, string> {{ "Table_CoverageItem", "Medical Payments" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Personal Injury Protection" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Uninsured Motorists BI" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Underinsured Motorists BI" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Uninsured Motorists PD" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Underinsured Motorists PD" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Deductible Per Accident" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Motor Truck Cargo" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Refrigerated Cargo Limits" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "General Liability" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Vehicles with Physical Damage" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Collision Deductible" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Comprehensive Deductible" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Fire/Theft Deductible" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Number of Drivers" } },
                new Dictionary<string, string> {{ "Table_CoverageItem", "Number of Vehicles" } },
            };

            var coverages = ReportData.Risk.RiskCoverages;
            for (var i = 0; i < 4; i++)
            {
                var bookmarkOption = $"Table_CoverageOption{i + 1}";
                var bookmarkOptionSymbol = $"Table_CoverageOptionSymbol{i + 1}";

                if (HasPremiumForOption(i))
                {
                    var liabilityLimit = GetLimitsText(coverages[i].AutoBILimitId, "AutoBILimits") ?? "";
                    coverageEntries[0].Add(bookmarkOption, liabilityLimit);
                    //coverageEntries[1].Add(bookmarkOption, liabilityLimit.Contains("/") ? GetLimitsText(coverages[i].AutoPDLimitId, "AutoPDLimits") : "");
                    coverageEntries[1].Add(bookmarkOption, coverages[0].MedPayLimitId == 110 ? "NA" : GetLimitsText(coverages[i].MedPayLimitId, "MedPayLimits"));
                    coverageEntries[2].Add(bookmarkOption, coverages[0].PIPLimitId == 130 ? "NA" : GetLimitsText(coverages[i].PIPLimitId, "PipLimits"));
                    coverageEntries[3].Add(bookmarkOption, coverages[0].UMBILimitId == 388 ? "NA" : GetLimitsText(coverages[i].UMBILimitId, "UMBILimits"));
                    coverageEntries[4].Add(bookmarkOption, coverages[0].UIMBILimitId == 243 ? "NA" : GetLimitsText(coverages[i].UIMBILimitId, "UIMBILimits"));
                    coverageEntries[5].Add(bookmarkOption, coverages[0].UMPDLimitId == 509 ? "NA" : GetLimitsText(coverages[i].UMPDLimitId, "UMPDLimits"));
                    coverageEntries[6].Add(bookmarkOption, coverages[0].UIMPDLimitId == 298 ? "NA" : GetLimitsText(coverages[i].UIMPDLimitId, "UIMPDLimits"));
                    coverageEntries[7].Add(bookmarkOption, GetLimitsText(coverages[i].LiabilityDeductibleId, "LiabilityDeductibleLimits"));
                    coverageEntries[8].Add(bookmarkOption, coverages[0].CargoLimitId == 53 ? "NA" : GetLimitsText(coverages[i].CargoLimitId, "CargoLimits"));
                    coverageEntries[9].Add(bookmarkOption, coverages[0].RefCargoLimitId == 157 ? "NA" : GetLimitsText(coverages[i].RefCargoLimitId, "RefLimits"));
                    coverageEntries[10].Add(bookmarkOption, coverages[0].GLBILimitId == 87 ? "NA" : GetLimitsText(coverages[i].GLBILimitId, "GlLimits"));
                    coverageEntries[11].Add(bookmarkOption, GetVehiclesWithPhysicalDamage(i));
                    coverageEntries[12].Add(bookmarkOption, coverages[0].CollDeductibleId == 65 ? "NA" : GetLimitsText(coverages[i].CollDeductibleId, "CollisionLimits"));
                    coverageEntries[13].Add(bookmarkOption, coverages[0].CompDeductibleId == 76 ? "NA" : GetLimitsText(coverages[i].CompDeductibleId, "ComprehensiveLimits"));
                    coverageEntries[14].Add(bookmarkOption, coverages[0].FireDeductibleId == 76 ? "NA" : GetLimitsText(coverages[i].FireDeductibleId, "ComprehensiveLimits"));
                    coverageEntries[15].Add(bookmarkOption, GetDriversForOption(i));
                    coverageEntries[16].Add(bookmarkOption, GetVehiclesForOption(i));

                    //Symbols
                    coverageEntries[0].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[i].ALSymbolId));
                    //coverageEntries[1].Add(bookmarkOptionSymbol, liabilityLimit.Contains("/") ? GetSymbolsText(coverages[0].PDSymbolId) : "");
                    coverageEntries[1].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].MedPayLimitSymbolId));
                    coverageEntries[2].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].PIPLimitSymbolId));
                    coverageEntries[3].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].UMBILimitSymbolId));
                    coverageEntries[4].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].UIMBILimitSymbolId));
                    coverageEntries[5].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].UMPDLimitSymbolId));
                    coverageEntries[6].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].UIMPDLimitSymbolId));
                    coverageEntries[7].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].LiabilityDeductibleSymbolId));
                    coverageEntries[8].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].CargoLimitSymbolId));
                    coverageEntries[9].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].RefCargoLimitSymbolId));
                    coverageEntries[10].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].GLBILimitSymbolId));
                    coverageEntries[11].Add(bookmarkOptionSymbol, "");
                    coverageEntries[12].Add(bookmarkOptionSymbol, GetSymbolsText(coverages[0].CollDeductibleSymbolId));
                    var option1Symbol = coverages[0].CompDeductibleSymbolId != null ? GetSymbolsText(coverages[0].CompDeductibleSymbolId) 
                        : GetSymbolsText(coverages[0].FireDeductibleSymbolId);
                    coverageEntries[13].Add(bookmarkOptionSymbol, coverages[i].CompDeductibleId != null ? option1Symbol : "");
                    coverageEntries[14].Add(bookmarkOptionSymbol, coverages[i].FireDeductibleId != null ? option1Symbol : "");
                    coverageEntries[15].Add(bookmarkOptionSymbol, "");
                    coverageEntries[16].Add(bookmarkOptionSymbol, "");
                }
                else
                {
                    WriteEmpty(coverageEntries, bookmarkOption, bookmarkOptionSymbol);
                }
            }

            coverageEntries.RemoveAll(HasNoCoverage);
            return coverageEntries;
        }

        private static bool HasNoCoverage(Dictionary<string, string> limit)
        {
            var option1Limit = limit.FirstOrDefault(f => f.Key == "Table_CoverageOption1").Value;
            var option2Limit = limit.FirstOrDefault(f => f.Key == "Table_CoverageOption2").Value;
            var option3Limit = limit.FirstOrDefault(f => f.Key == "Table_CoverageOption3").Value;

            return option1Limit == "NA" || option2Limit == "NA" || option3Limit == "NA";
        }

        private static bool HasNoCoveragePremium(Dictionary<string, string> limit)
        {
            var option1Premium = limit.FirstOrDefault(f => f.Key == "Table_PolPremOption1").Value;
            var option2Premium = limit.FirstOrDefault(f => f.Key == "Table_PolPremOption2").Value;
            var option3Premium = limit.FirstOrDefault(f => f.Key == "Table_PolPremOption3").Value;

            return option1Premium == "NA" || option2Premium == "NA" || option3Premium == "NA";
        }

        private static void WriteEmpty(IReadOnlyList<Dictionary<string, string>> coverageEntries, string bookmarkOption, string bookmarkOptionSymbol)
        {
            for (var i = 0; i < 17; i++)
            {
                coverageEntries[i].Add(bookmarkOption, "");
                coverageEntries[i].Add(bookmarkOptionSymbol, "");
            }
        }

        private string GetLimitsText(int? id, string dataSetName)
        {
            if (id == null) return "NA";

            var text = ReportData.DataSets[dataSetName].FirstOrDefault(d => d.Id == id)?.LimitDisplay;
            //DB text value has different white space encoding which breaks the display
            return Regex.Replace(text?.ToString() ?? "" , @"\s+", " ");
        }

        private string GetSymbolsText(int? id)
        {
            string symbolText = ReportData.DataSets["Symbols"].FirstOrDefault(d => d.Id == id)?.Description ?? "";
            if (symbolText.Contains(")"))
            {
                symbolText = symbolText.Substring(0, 1);
            }
            return symbolText;
        }

        private string GetVehiclesWithPhysicalDamage(int optionId)
        {
            const int comprehensiveNoCoverageId = 76;
            const int collisionNoCoverageId = 65;

            var option = (optionId + 1).ToString();

            var vehicleCount = ReportData.Risk.Vehicles.Where(v => v.Options != null && v.Options.Contains(option) && v.IsActive != false).Count(v =>
                (v.CoverageFireTheftDeductibleId != null && v.CoverageFireTheftDeductibleId != comprehensiveNoCoverageId)
                || (v.CoverageCollisionDeductibleId != null && v.CoverageCollisionDeductibleId != collisionNoCoverageId));
            return vehicleCount.ToString();
        }

        private string GetDriversForOption(int option)
        {
            var optionNumber = (option + 1).ToString();
            var drivers = ReportData.Risk.Drivers
                .Count(d => d.Options != null && d.Options.Contains(optionNumber) && d.IsActive != false);
            return drivers.ToString();
        }

        private string GetVehiclesForOption(int option)
        {
            var optionNumber = (option + 1).ToString();
            var vehicles = ReportData.Risk.Vehicles
                .Count(d => d.Options != null && d.Options.Contains(optionNumber) && d.IsActive != false);
            return vehicles.ToString();
        }

        private bool HasPremiumForOption(int optionNumber)
        {
            var coverages = ReportData.Risk.RiskCoverages;
            return optionNumber < coverages.Count && coverages[optionNumber].RiskCoveragePremium?.Premium > 0;
        }
    }

}