﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Rater;
using RivTech.CABoxTruck.WebService.DTO.Submission.Applicant;
using RivTech.CABoxTruck.WebService.DTO.Submission.RiskSpecifics;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission.RiskSpecifics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.Service.Services.Submission;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class RiskService : IRiskService
    {
        private readonly IMapper _mapper;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IRiskSpecificMaintenanceSafetyService _maintenanceSafetyService;
        private readonly IRiskSpecificGeneralLiabilityCargoService _generalLiabilityCargoService;
        private readonly IBusinessDetailsService _businessDetailsService;
        private readonly ICoverageHistoryService _coverageHistoryService;
        private readonly IClaimsHistoryService _claimsHistoryService;
        private readonly IRiskStatusHistoryService _riskStatusHistoryService;
        private readonly IRiskNotesService _riskNotesService;
        private readonly IFormsService _formsService;
        private readonly IBrokerInfoService _brokerInfoService;
        private readonly IBindingService _bindingService;
        private readonly IFileUploadDocumentService _documentService;
        private readonly IApplicantAdditionalInterestService _additionalInterestService;
        private readonly IRiskManuscriptsService _manuscriptsService;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IRiskPolicyContactService _riskPolicyContactService;
        private readonly IRiskFactory _riskFactory;

        public RiskService(
            IMapper mapper,
            IRiskDetailRepository riskDetailRepository,
            IRiskRepository riskRepository,
            IEntityAddressRepository entityAddressRepository,
            IRiskSpecificMaintenanceSafetyService maintenanceSafetyService,
            IRiskSpecificGeneralLiabilityCargoService generalLiabilityCargoService,
            IBusinessDetailsService businessDetailsService,
            ICoverageHistoryService coverageHistoryService,
            IClaimsHistoryService claimsHistoryService,
            IRiskStatusHistoryService riskStatusHistoryService,
            IRiskNotesService riskNotesService,
            IFormsService formsService, 
            IBrokerInfoService brokerInfoService,
            IBindingService bindingService,
            IFileUploadDocumentService documentService,
            IApplicantAdditionalInterestService additionalInterestService,
            IRiskManuscriptsService manuscriptsService,
            IPolicyHistoryRepository policyHistoryRepository,
            IRiskPolicyContactService riskPolicyContactService,
            IRiskFactory riskFactory
            )
        {
            _mapper = mapper;
            _riskRepository = riskRepository;
            _riskDetailRepository = riskDetailRepository;
            _entityAddressRepository = entityAddressRepository;
            _maintenanceSafetyService = maintenanceSafetyService;
            _generalLiabilityCargoService = generalLiabilityCargoService;
            _businessDetailsService = businessDetailsService;
            _coverageHistoryService = coverageHistoryService;
            _claimsHistoryService = claimsHistoryService;
            _riskStatusHistoryService = riskStatusHistoryService;
            _riskNotesService = riskNotesService;
            _formsService = formsService;
            _brokerInfoService = brokerInfoService;
            _bindingService = bindingService;
            _documentService = documentService;
            _additionalInterestService = additionalInterestService;
            _manuscriptsService = manuscriptsService;
            _policyHistoryRepository = policyHistoryRepository;
            _riskPolicyContactService = riskPolicyContactService;
            _riskFactory = riskFactory;
        }
        public async Task<bool> IsExistAsync(Guid id)
        {
            return await _riskDetailRepository.AnyAsync(x => x.Id == id);
        }

        public async Task<List<RiskDTO>> GetAllAsync()
        {
            var result = await _riskDetailRepository.GetAllAsync();
            return _mapper.Map<List<RiskDTO>>(result);
        }

        public async Task<RiskDTO> GetRiskAsync(Guid id)
        {
            var result = await _riskDetailRepository.FindAsync(x => x.Id == id);
            return _mapper.Map<RiskDTO>(result);
        }

        public async Task<RiskDTO> InsertRiskAsync(RiskDTO model)
        {
            var result = await _riskDetailRepository.AddAsync(_mapper.Map<Data.Entity.RiskDetail>(model));
            await _riskDetailRepository.SaveChangesAsync();
            return _mapper.Map<RiskDTO>(result);
        }

        public async Task<string> RemoveRiskAsync(Guid id)
        {
            var entity = await _riskDetailRepository.FindAsync(x => x.Id == id);
            //entity.IsActive = false;
            //entity.DeletedDate = DateTime.Now;
            //await _riskDetailRepository.SaveChangesAsync();acc
            //return entity.Id.ToString();
            return id.ToString();
        }

        public async Task<string> UpdateRiskAsync(RiskDTO model)
        {
            var result = _riskDetailRepository.Update(_mapper.Map<Data.Entity.RiskDetail>(model));
            await _riskDetailRepository.SaveChangesAsync();
            return result.Id.ToString();
        }

        public async Task<bool> UpdateRiskStatusAsync(RiskDTO model)
        {
            var risk = await _riskDetailRepository.FindAsync(r => r.Id == model.Id);

            if (risk.Status != model.Status)
            {
                var statusHistory = CreateStatusHistoryModel(risk.Id, "Status", risk.Status, model.Status);
                await _riskStatusHistoryService.InsertAsync(statusHistory);
            }
            if (risk.AssignedToId != model.AssignedToId)
            {
                var assignedToHistory = CreateStatusHistoryModel(risk.Id, "AssignedToId", risk.AssignedToId?.ToString(), model.AssignedToId?.ToString());
                await _riskStatusHistoryService.InsertAsync(assignedToHistory);
            }

            risk.Status = model.Status;
            risk.AssignedToId = model.AssignedToId;

            await _riskDetailRepository.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateRiskStatusOnlyAsync(Guid riskDetailId, string status)
        {
            var risk = await _riskDetailRepository.FindAsync(r => r.Id == riskDetailId);

            if (risk.Status != status)
            {
                var statusHistory = CreateStatusHistoryModel(risk.Id, "Status", risk.Status, status);
                await _riskStatusHistoryService.InsertAsync(statusHistory);
            }

            risk.Status = status;

            await _riskDetailRepository.SaveChangesAsync();
            return true;
        }

        public async Task<string> UpdatePendingEnrosementAsync(Guid riskDetailId)
        {
            return await UpdateRiskStatusPendingEndorsement(riskDetailId);
        }

        private async Task<string> UpdateRiskStatusPendingEndorsement(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.FindAsync(r => r.Id == riskDetailId);
            var risk = await _riskRepository.FindAsync(r => r.Id == riskDetail.RiskId);
            var changes = await _riskFactory.GetPolicyChanges(riskDetailId, riskDetail.RiskId, true);

            if (changes.Count() > 0)
            {
                var statusHistory = CreateStatusHistoryModel(riskDetail.Id, "Status", riskDetail.Status, RiskDetailStatus.PendingEndorsement);
                await _riskStatusHistoryService.InsertAsync(statusHistory);
                if (risk.Status == RiskStatus.Active)
                    riskDetail.Status = RiskDetailStatus.PendingEndorsement;
            }
            else
            {
                if (risk.Status == RiskStatus.Active)
                    riskDetail.Status = RiskDetailStatus.Active;
            }
            
            await _riskDetailRepository.SaveChangesAsync();

            return riskDetail.Status;
        }

        private static RiskStatusHistory CreateStatusHistoryModel(Guid riskId, string field, string oldValue, string newValue)
        {
            var riskStatusHistory = new RiskStatusHistory
            {
                RiskDetailId = riskId,
                ChangedBy = 0,
                ChangedDate = DateTime.Now,
                ChangedField = field,
                OldValue = oldValue,
                NewValue = newValue
            };
            return riskStatusHistory;
        }

        public async Task<List<RiskDTO>> GetAllIncludeAsync()
        {
            var result = await _riskDetailRepository.GetAllIncludeAsync();
            return _mapper.Map<List<RiskDTO>>(result).ToList();
        }

        public async Task<Guid> GetRiskDetailIdByStatusAsync(Guid riskId, string status)
        {
            var result = await _riskDetailRepository.GetAsync(rd => rd.RiskId == riskId && rd.Status == status);
            return result.OrderByDescending(r => r.UpdatedDate).FirstOrDefault()?.Id ?? Guid.Empty;
        }

        public async Task<RiskDTO> GetByIdIncludeAsync(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var vehicleFactors = await _riskDetailRepository.GetVehicleRatingFactorsByRiskDetailId(riskDetailId);
            var result = _mapper.Map<RiskDTO>(riskDetail);

            if (riskDetail != null)
            {
                var riskDetails = await _riskDetailRepository.GetAsync(x => x.RiskId == riskDetail.RiskId);
                var previousRiskDetails = await _riskDetailRepository.GetAsync(x => x.RiskId == riskDetail.RiskId && x.Id != riskDetail.Id);
                RiskDetail previousRiskDetail = null;
                if (previousRiskDetails.Count() > 0)
                    previousRiskDetail = previousRiskDetails?.OrderByDescending(x => x.UpdatedDate).FirstOrDefault();
                var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
                SetNameAndAddress(riskDetail, result);
                SetFilingsInformation(riskDetail, result);
                if (riskDetails.Count() > 1)
                    result.IsPolicy = true;
                result.Status = risk.Status;
                result.SubStatus = riskDetail.Status;
                if (risk.Status == RiskStatus.Canceled)
                {
                    result.SubStatus = CancelationReason(riskDetail) ?? riskDetail.Status;
                }
                result.PendingCancellationDate = risk.PendingCancellationDate;
                result.PolicyNumber = risk?.PolicyNumber;
                result.EndorsementEffectiveDate = riskDetail.EndorsementEffectiveDate;
                result.PrevEndorsementEffectiveDate = riskDetail.EndorsementEffectiveDate;
                if (previousRiskDetail != null)
                    result.PrevEndorsementEffectiveDate = previousRiskDetail.EndorsementEffectiveDate;
                result.Vehicles = result.Vehicles?.Where(x => x.IsActive != false)?.ToList();
                result.VehicleFactors = _mapper.Map<List<PremiumRaterVehicleFactorsDTO>>(vehicleFactors);
                result.Drivers = result.Drivers?.Where(x => x.IsActive != false).OrderBy(d=> d.CreatedDate)?.ToList();

                result.BrokerInfo = await _brokerInfoService.GetByRiskDetailIdAllIncludeAsync(riskDetail.RiskId);
                result.RiskHistory = await _coverageHistoryService.GetByIdAsync(riskDetailId);
                result.BusinessDetails = await _businessDetailsService.GetBusinessDetails(riskDetailId);
                result.MaintenanceSafety = await _maintenanceSafetyService.GetMaintenanceSafety(riskDetailId);
                result.GeneralLiabilityCargo = await _generalLiabilityCargoService.GetGeneralLiabilityCargo(riskDetailId);
                result.GeneralLiabilityCargo.CommoditiesHauled = _mapper.Map<List<RiskSpecificCommoditiesHauledDto>>(riskDetail.CommoditiesHauled);
                result.ClaimsHistory = await _claimsHistoryService.GetByRiskDetailIdAsync(riskDetailId);
                result.RiskNotes = await _riskNotesService.GetByRiskIdAsync(riskDetailId);
                result.RiskForms = await _formsService.GetByRiskIdAsync(riskDetailId);

                result.Binding = await _bindingService.GetBindingByRiskIdAsync(riskDetail.RiskId);
                result.BindingRequirements = await _bindingService.GetByRiskDetailIdAsync(riskDetailId);

                result.QuoteConditions = await _bindingService.GetQuoteByRiskDetailIdAsync(riskDetailId);

                result.BlanketCoverage = _mapper.Map<BlanketCoverageDTO>(riskDetail);
                result.AdditionalInterest = (await _additionalInterestService.GetByRiskDetailIdActiveAsync(riskDetailId)).ToList();
                result.RiskManuscripts = await _manuscriptsService.GetByRiskIdAsync(riskDetailId);
                result.RiskDocuments = await _documentService.GetByRefIdAsync(riskDetailId);
                result.RiskPolicyContacts = await _riskPolicyContactService.GetByRiskDetailIdAsync(riskDetailId);
                result.HasInitialDriverExclusion = await _formsService.HasInitialDriverExclusionFormAsync(riskDetail.RiskId);
            }
            return result;
        }

        private void SetFilingsInformation(RiskDetail risk, RiskDTO riskDto)
        {
            var filingsInfoDto = _mapper.Map<FilingsInformationDTO>(risk);

            if (risk?.Insured?.Entity != null)
            {
                filingsInfoDto.USDOTNumber = risk.Insured.Entity.USDOTNumber;
            }
            if (filingsInfoDto.IsFilingRequired.GetValueOrDefault(false) && risk?.RiskFilings != null)
            {
                filingsInfoDto.RequiredFilingIds = risk.RiskFilings.Select(rf => rf.FilingTypeId).ToList();
            }
            riskDto.FilingsInformation = filingsInfoDto;
        }

        private void SetNameAndAddress(RiskDetail risk, RiskDTO riskDto)
        {
            riskDto.NameAndAddress = new NameAndAddressDTO();
            riskDto.NameAndAddress.BusinessName = risk.Insured?.Entity?.CompanyName;
            riskDto.NameAndAddress.InsuredDBA = risk.Insured?.Entity?.DBA;
            riskDto.NameAndAddress.BusinessPrincipal = risk.Insured?.Entity?.BusinessPrincipal;
            //riskDto.NameAndAddress.PrimaryCityZipCodeOfOperationsId = risk.PrimaryCityZipCodeOfOperationsId;
            riskDto.NameAndAddress.Email = risk.Insured?.Entity?.WorkEmailAddress;
            riskDto.NameAndAddress.PhoneExt = risk.Insured?.Entity?.WorkPhoneExtension;

            var businessAddress = _entityAddressRepository.FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business && x.EntityId == risk.Insured.EntityId,
                i => i.Address).Result;
            riskDto.NameAndAddress.BusinessAddress = businessAddress?.Address?.StreetAddress1;
            riskDto.NameAndAddress.City = businessAddress?.Address?.City;
            riskDto.NameAndAddress.State = businessAddress?.Address?.State;
            riskDto.NameAndAddress.ZipCode = businessAddress?.Address?.ZipCode;

            var primaryCityOfOperations = _entityAddressRepository.FindAsync(x => x.AddressTypeId == null && x.EntityId == risk.Insured.EntityId,
                i => i.Address).Result;

            riskDto.NameAndAddress.CityOperations = primaryCityOfOperations?.Address?.City;
            riskDto.NameAndAddress.StateOperations = primaryCityOfOperations?.Address?.State;
            riskDto.NameAndAddress.ZipCodeOperations = primaryCityOfOperations?.Address?.ZipCode;

            var entityAddresses = _mapper.Map<List<EntityAddressDTO>>(risk?.Insured?.Entity?.EntityAddresses);
            riskDto.NameAndAddress.Addresses = entityAddresses;
        }

        private string CancelationReason(RiskDetail riskDetail)
        {
            var history = _policyHistoryRepository.FindAsync(x => x.RiskDetailId == riskDetail.Id && x.PolicyStatus == RiskDetailStatus.Rewrite).Result;
            if (history != null)
            {
                return RiskDetailStatus.Rewrite;
            }

            return null;
        }

        public async Task<PaymentPortalSearchResponseDTO> SearchRiskByPolicyAndZipCodeAsync(string policyNumber, string zipCode)
        {
            var risk = await _riskRepository.GetRiskByPolicyNumberAsync(policyNumber);
            var riskDetail = await _riskDetailRepository.GetLatestRiskDetailByRiskIdAsync(risk?.Id);
            var result = new PaymentPortalSearchResponseDTO();

            var businessAddress = _entityAddressRepository.FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business && x.EntityId == riskDetail.Insured.EntityId,
                i => i.Address).Result;

            if (riskDetail != null && businessAddress?.Address?.ZipCode == zipCode)
            {
                result = _mapper.Map<PaymentPortalSearchResponseDTO>(riskDetail);
                result.NameAndAddress = new NameAndAddressDTO();
                result.NameAndAddress.BusinessName = riskDetail?.Insured?.Entity?.CompanyName;
            }
            return result;
        }
    }
}
