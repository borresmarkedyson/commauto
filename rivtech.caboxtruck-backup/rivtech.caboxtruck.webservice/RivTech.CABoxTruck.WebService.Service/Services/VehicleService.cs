﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.IServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Service.Services.ExcelManager;
using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.WebService.Service.Services.Common;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DTO.Rater;
using RivTech.CABoxTruck.WebService.DTO.Constants;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IVehiclePremiumRepository _vehiclePremiumRepository;
        private readonly ILvLimitsRepository _lvLimitsRepository;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IRiskCoverageRepository _riskCoverageRepository;
        private readonly IMapper _mapper;

        public VehicleService(
            IVehicleRepository vehicleRepository,
            IVehiclePremiumRepository vehiclePremiumRepository,
            ILvLimitsRepository lvLimitsRepository,
            IRiskDetailRepository riskDetailRepository,
            IBrokerInfoRepository brokerInfoRepository,
            IPolicyHistoryRepository policyHistoryRepository,
            IRiskCoverageRepository riskCoverageRepository,
            IMapper mapper)
        {
            _vehicleRepository = vehicleRepository;
            _vehiclePremiumRepository = vehiclePremiumRepository;
            _lvLimitsRepository = lvLimitsRepository;
            _riskDetailRepository = riskDetailRepository;
            _brokerInfoRepository = brokerInfoRepository;
            _policyHistoryRepository = policyHistoryRepository;
            _riskCoverageRepository = riskCoverageRepository;
            _mapper = mapper;
        }

        public async Task<List<VehicleDTO>> GetAllAsync()
        {
            var result = await _vehicleRepository.GetAllAsync();
            return _mapper.Map<List<VehicleDTO>>(result);
        }

        public async Task<List<VehicleDTO>> GetByRiskDetailIdAsync(Guid id)
        {
            var result = await _vehicleRepository.GetByRiskDetailAsync(id);
            return result.Select(obj => _mapper.Map<VehicleDTO>(result)).ToList();
        }

        public async Task<bool> IsExistAsync(Guid id)
        {
            return await _vehicleRepository.AnyAsync(x => x.Id == id);
        }

        public async Task<VehicleDTO> GetByIdAsync(Guid id)
        {
            var result = await _vehicleRepository.FindAsync(x => x.Id == id);
            return _mapper.Map<VehicleDTO>(result);
        }

        public async Task<VehicleDTO> InsertAsync(VehicleDTO model)
        {
            var vehicle = _mapper.Map<Vehicle>(model);
            vehicle.Id = Guid.NewGuid();
            vehicle.MainId = Guid.NewGuid();
            vehicle.CreatedDate = DateTime.Now;
            await _vehicleRepository.AddAsync(vehicle);

            // Link risk detail.
            var riskDetail = await _riskDetailRepository.FindAsync(r => r.Id == model.RiskDetailId);
            vehicle.LinkRiskDetail(riskDetail);

            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllAsync(riskDetail.RiskId);
            vehicle.EffectiveDate = riskDetail.EndorsementEffectiveDate ?? brokerInfo.EffectiveDate;
            vehicle.ExpirationDate = brokerInfo.ExpirationDate;

            //vehicle.VehicleProratedPremium = new VehicleProratedPremium(vehicle);

            //await UpdateDeductibles(vehicle, riskDetail);
            await _vehicleRepository.SaveChangesAsync();

            return _mapper.Map<VehicleDTO>(vehicle);
        }

        public async Task<List<VehicleDTO>> InsertRangeAsync(List<VehicleDTO> model)
        {
            var vehicles = _mapper.Map<List<Vehicle>>(model);
            var riskDetailId = model[0].RiskDetailId;
            var riskDetail = await _riskDetailRepository.FindAsync(r => r.Id == riskDetailId);

            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);

            foreach (var vehicle in vehicles)
            {
                vehicle.Id = Guid.NewGuid();
                vehicle.MainId = Guid.NewGuid();
                vehicle.CreatedDate = DateTime.Now;
                vehicle.CreatedBy = Data.Common.Services.GetCurrentUser();
                vehicle.LinkRiskDetail(riskDetail);
                //await UpdateDeductibles(vehicle, riskDetail);

                vehicle.EffectiveDate = riskDetail.EndorsementEffectiveDate ?? brokerInfo.EffectiveDate;
                vehicle.ExpirationDate = brokerInfo.ExpirationDate;

                //var vehicleProratedPremium = new VehicleProratedPremium();
                //vehicleProratedPremium.Id = Guid.NewGuid();
                //vehicle.VehicleProratedPremium = vehicleProratedPremium;
                //vehicle.VehicleProratedPremiumId = vehicle.VehicleProratedPremium.Id;
                //vehicle.VehicleProratedPremium.VehicleId = vehicle.Id;
                //_vehicleRepository.Entry(vehicleProratedPremium).State = EntityState.Added;
            }

            await _vehicleRepository.AddRangeAsync(vehicles);
            await _vehicleRepository.SaveChangesAsync();

            return _mapper.Map<List<VehicleDTO>>(vehicles);
        }

        public async Task<VehicleDTO> UpdateAsync(VehicleDTO dto)
        {
            if (dto?.Id is null)
                throw new ArgumentException("Vehicle has no ID.");

            Vehicle vehicle = _mapper.Map<Vehicle>(dto);
            Vehicle savedVehicle = _vehicleRepository.GetById(dto.Id.Value);
            vehicle.MainId = savedVehicle.MainId;

            if (_vehicleRepository.IsOldVersion(dto.Id))
            {
                throw new InvalidOperationException($"Cannot update an old version of vehicle {vehicle.VIN}({vehicle.MainId}).");
            }

            vehicle.PreviousVehicleVersionId = savedVehicle?.PreviousVehicleVersionId;
            vehicle.PreviousVehicleVersion = savedVehicle?.PreviousVehicleVersion;
            vehicle.RiskDetailVehicles = savedVehicle?.RiskDetailVehicles;
            vehicle.VehiclePremiums = savedVehicle?.VehiclePremiums;
            vehicle.VehiclePremiumRatingFactor = savedVehicle?.VehiclePremiumRatingFactor;

            //await UpdateDeductibles(vehicle, savedVehicle.LatestRiskDetail);

            // Get vehicles from previous risk
            var previousRiskDetailVehicles = await _vehicleRepository.GetPreviousVehiclesByRiskDetail(savedVehicle?.RiskDetail);
            bool isFromPreviousEndorsement = previousRiskDetailVehicles?.Contains(savedVehicle) ?? false;

            // Previous vehicle version
            Vehicle previousVersion = null;
            if (savedVehicle.PreviousVehicleVersionId != null)
                previousVersion = _vehicleRepository.GetById(savedVehicle.PreviousVehicleVersionId.Value);

            bool isNew = savedVehicle.PreviousVehicleVersionId is null;
            bool justReinstated = (previousVersion?.IsDeleted ?? false) && !vehicle.IsDeleted;
            bool justDeleted = !(previousVersion?.IsDeleted ?? false) && vehicle.IsDeleted;
            bool shouldCreateNewVersion = dto.isEndorsement == true && isFromPreviousEndorsement && savedVehicle.HasChanges(vehicle);
            bool shouldRollbackVersion = dto.isEndorsement == true && !isNew && !isFromPreviousEndorsement && !(previousVersion?.HasChanges(vehicle) ?? true)
                                        && !justReinstated && !justDeleted;

            Vehicle updatedVehicle;
            if (shouldCreateNewVersion)
            {
                var vehicleRatingFactor = _vehicleRepository.GetRatingFactors(vehicle.Id, vehicle.LatestRiskDetail.Id);
                // If update is from endorsement with no new version and has changes,
                // then create a new version with new changes.
                Vehicle newVehicleVersion = vehicle.CloneForEndorsement();
                newVehicleVersion = UpdateLimitsData(newVehicleVersion);
                updatedVehicle = await _vehicleRepository.AddAsync(newVehicleVersion);
                var rdv = savedVehicle.UnlinkLatestRiskDetail();
                await _vehiclePremiumRepository.SaveChangesAsync();

                var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(rdv.RiskDetailId, vehicle.Id);
                if (vehiclePremium != null)
                {
                    _vehiclePremiumRepository.Remove(vehiclePremium);
                }
                if (vehicleRatingFactor != null)
                {
                    vehicleRatingFactor.VehicleId = newVehicleVersion.Id.ToString();
                    await _vehicleRepository.UpdateRatingFactors(vehicleRatingFactor);
                }

                _vehicleRepository.Update(savedVehicle);
                //await UpdatePremiumByPremiumFactor(savedVehicle?.VehiclePremiumRatingFactor);
            }
            else if (shouldRollbackVersion)
            {
                // If update is from endorsement with new version already but no difference to previous,
                // Then remove the new version and re-assign the previous version to current riskdetail.
                var currentRiskDetail = savedVehicle.UnlinkLatestRiskDetail();
                _vehicleRepository.Remove(savedVehicle);

                var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(currentRiskDetail.RiskDetailId, savedVehicle.Id);
                if (vehiclePremium != null)
                    _vehiclePremiumRepository.Remove(vehiclePremium);

                // Link back the previous vehicle to current riskdetail.
                previousVersion.LinkRiskDetail(currentRiskDetail.RiskDetail);

                updatedVehicle = previousVersion;
            }
            else // If from submission or no need to update versions, just update.
            {
                vehicle = UpdateLimitsData(vehicle);

                // Detached the same vehicle queried stored vehicle in db to avoid conflict when updating dbcontext.
                _vehicleRepository.Detach(savedVehicle);

                updatedVehicle = _vehicleRepository.Update(vehicle);
            }

            await _vehicleRepository.SaveChangesAsync();
            return _mapper.Map<VehicleDTO>(updatedVehicle);
        }



        private Vehicle UpdateLimitsData(Vehicle v)
        {
            if (!(v.IsCoverageCollision ?? false))
                v.CoverageCollisionDeductibleId = null;

            if (!(v.IsCoverageFireTheft ?? false))
                v.CoverageFireTheftDeductibleId = null;

            if (!(v.HasCoverageCargo ?? false))
                v.HasCoverageRefrigeration = false;

            return v;
        }

        private async Task UpdateDeductibles(VehicleDTO model, RiskCoverage riskCoverage, bool hasAllNoCargo, bool hasAllNoRef)
        {
            if (model.DeletedDate != null)
                return; // Cannot update coverage of deleted vehicle.

            var compLimits = await _lvLimitsRepository.GetAsync(x => x.LimitType.Equals(LimitType.Comprehensive));
            var compNoCoverage = compLimits.SingleOrDefault(x => x.LimitValue.Equals("No Coverage"));

            var collLimits = await _lvLimitsRepository.GetAsync(x => x.LimitType.Equals(LimitType.Collision));
            var collNoCoverage = collLimits.SingleOrDefault(x => x.LimitValue.Equals("No Coverage"));

            var cargoLimits = await _lvLimitsRepository.GetAsync(x => x.LimitType.Equals(LimitType.Cargo));
            var cargoNoCoverage = cargoLimits.SingleOrDefault(x => x.LimitValue.Equals("NONE"));

            var refLimits = await _lvLimitsRepository.GetAsync(x => x.LimitType.Equals(LimitType.RefBreakdown));
            var refNoCoverage = refLimits.SingleOrDefault(x => x.LimitValue.Equals("NONE"));

            if (model.CoverageFireTheftDeductibleId == null)
            {
                model.CoverageFireTheftDeductibleId = (short?)compNoCoverage?.Id ?? null;
            }
            if (model.CoverageCollisionDeductibleId == null)
            {
                model.CoverageCollisionDeductibleId = (short?)collNoCoverage?.Id ?? null;
            }

            bool limitHasComp = (riskCoverage.CompDeductibleId != null) && (riskCoverage.CompDeductibleId != compNoCoverage.Id);
            bool limitHasFT = (riskCoverage.FireDeductibleId != null) && (riskCoverage.FireDeductibleId != compNoCoverage.Id);
            bool limitHasColl = (riskCoverage.CollDeductibleId != null) && (riskCoverage.CollDeductibleId != collNoCoverage.Id);
            bool limitHasCargo = (riskCoverage.CargoLimitId != null) && (riskCoverage.CargoLimitId != cargoNoCoverage.Id);
            bool limitHasRef = (riskCoverage.RefCargoLimitId != null) && (riskCoverage.RefCargoLimitId != refNoCoverage.Id);
            bool vehicleHasCompFT = model.CoverageFireTheftDeductibleId != compNoCoverage.Id;
            bool vehicleHasColl = model.CoverageCollisionDeductibleId != collNoCoverage.Id;

            if (limitHasComp || limitHasFT)
            {
                //model.IsCoverageFireTheft = true;
                if (!vehicleHasCompFT)
                {
                    // If Limit has CompfT coverage but vehicle has no CompFT, update vehicle to have CompFT.
                    if (limitHasComp)
                        model.CoverageFireTheftDeductibleId = riskCoverage.CompDeductibleId;
                    else if (limitHasFT)
                        model.CoverageFireTheftDeductibleId = riskCoverage.FireDeductibleId;
                }
            }
            else
            {
                model.IsCoverageFireTheft = false;
                if (vehicleHasCompFT)
                {
                    // If Limit CompfT has No Coverage but vehicle has CompFT, update vehicle CompFT to No Cov.
                    model.CoverageFireTheftDeductibleId = (short)compNoCoverage.Id;
                }
            }

            if (limitHasColl)
            {
                //model.IsCoverageCollision = true;
                if (!vehicleHasColl)
                {
                    // If Limit has Coll coverage but vehicle has no Coll, update vehicle to have Coll.
                    model.CoverageCollisionDeductibleId = riskCoverage.CollDeductibleId;
                }
            }
            else
            {
                model.IsCoverageCollision = false;
                if (vehicleHasColl)
                {
                    // If Limit Coll has No Coverage but vehicle has Coll, update vehicle Coll to No Cov.
                    model.CoverageCollisionDeductibleId = (short)collNoCoverage.Id;
                }
            }

            //if (limitHasCargo)
            //{
            //    if (hasAllNoCargo)
            //        model.HasCoverageCargo = true;
            //}
            //else
            //{
            //    model.HasCoverageCargo = false;
            //}

            //if (limitHasRef)
            //{
            //    if(hasAllNoRef)
            //        model.HasCoverageRefrigeration = true;
            //}
            //else
            //{
            //    model.HasCoverageRefrigeration = false;
            //}
        }

        public async Task<List<VehicleDTO>> UpdateAddressesAsync(List<VehicleDTO> model)
        {
            var vehicles = await _vehicleRepository.GetAsync(o => model.Select(s => s.Id).Contains(o.Id));
            vehicles.ToList().ForEach(v =>
            {
                var updatingVehicle = model.FirstOrDefault(m => m.Id == v.Id);
                v.GaragingAddressId = updatingVehicle?.GaragingAddressId;
                v.PrimaryOperatingZipCode = updatingVehicle?.PrimaryOperatingZipCode;
                _vehicleRepository.Entry(v).Property(p => p.GaragingAddressId).IsModified = true;
                _vehicleRepository.Entry(v).Property(p => p.PrimaryOperatingZipCode).IsModified = true;
            });

            await _vehicleRepository.SaveChangesAsync();
            return model;
        }

        public async Task<List<VehicleDTO>> UpdateOtherCoveragesAsync(List<VehicleDTO> model)
        {
            if (model.Count == 0)
                return await Task.FromResult(model);

            var riskCoverage = await _riskCoverageRepository.GetRiskCoverageBySelectedBindOption(model[0].RiskDetailId.Value);

            var activeVehicles = model.Where(x => x.DeletedDate == null);

            var vehicleNoCargoCount = activeVehicles.Count(x => x.HasCoverageCargo != true);
            var vehicleNoRefCount = activeVehicles.Count(x => x.HasCoverageRefrigeration != true);

            bool hasAllNoCargo = activeVehicles.Count() == vehicleNoCargoCount;
            bool hasAllNoRef = activeVehicles.Count() == vehicleNoRefCount;

            foreach (var vehicle in model)
            {
                await UpdateDeductibles(vehicle, riskCoverage, hasAllNoCargo, hasAllNoRef);
                await UpdateAsync(vehicle);
            }

            await _vehicleRepository.SaveChangesAsync();
            return model;
        }

        public async Task<List<VehicleDTO>> UpdateListOptionAsync(List<VehicleDTO> model)
        {
            var vehicleList = _mapper.Map<List<Vehicle>>(model);
            var result = (await _vehicleRepository.GetAsync(o => vehicleList.Select(s => s.Id).Contains(o.Id)));

            //lightweight saving of one field only. 
            result.ToList().ForEach(o =>
            {
                o.Options = model.Where(m => m.Id == o.Id).Select(s => s.Options ?? "").FirstOrDefault() ?? "";
                _vehicleRepository.Entry<Vehicle>(o).Property(p => p.Options).IsModified = true;
            });

            await _vehicleRepository.SaveChangesAsync();
            return model;
        }

        public async Task<string> RemoveAsync(Guid id, bool? fromEndorsement = false)
        {
            var vehicle = _vehicleRepository.GetById(id);
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(vehicle.RiskDetail.Id);
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);

            // Get vehicles from previous risk
            var previousRiskDetailVehicles = await _vehicleRepository.GetPreviousVehiclesByRiskDetail(vehicle.RiskDetail);
            bool isFromPreviousRiskDetail = previousRiskDetailVehicles?.Contains(vehicle) ?? false;

            if (isFromPreviousRiskDetail) // If vehicle version same previous with previous risk detail, create new version
            {
                Vehicle newVehicleVersion = vehicle.CloneForEndorsement();
                newVehicleVersion.Delete(riskDetail.EndorsementEffectiveDate ?? brokerInfo.EffectiveDate);
                await _vehicleRepository.AddAsync(newVehicleVersion);
                var rdv = vehicle.UnlinkLatestRiskDetail();
                var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(rdv.RiskDetailId, vehicle.Id);
                if (vehiclePremium != null)
                    _vehiclePremiumRepository.Remove(vehiclePremium);
                _vehicleRepository.Update(vehicle);
            }
            else
            {
                if (vehicle.PreviousVehicleVersionId != null) // If not same with previous riskdetail but with previous version, just soft  delete .
                {
                    Vehicle deletedVehicle = vehicle;
                    if (vehicle.PreviousVehicleVersion?.HasChanges(vehicle) ?? false)
                    {
                        // If changes has been made on new version, hard delete it and
                        // just clone the previous version to revert changes.
                        var latestRiskdetailVehicle = vehicle.UnlinkLatestRiskDetail();
                        deletedVehicle = vehicle.PreviousVehicleVersion.CloneForEndorsement();
                        deletedVehicle.LinkRiskDetail(latestRiskdetailVehicle.RiskDetail);
                        await _vehicleRepository.AddAsync(deletedVehicle);

                        var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(latestRiskdetailVehicle.RiskDetailId, vehicle.Id);
                        if (vehiclePremium != null)
                            _vehiclePremiumRepository.Remove(vehiclePremium);

                        _vehicleRepository.Remove(vehicle);
                    }
                    deletedVehicle.Delete(riskDetail.EndorsementEffectiveDate ?? brokerInfo.EffectiveDate);
                    _vehicleRepository.Update(deletedVehicle);
                }
                else
                {
                    // Hard delete vehicles which are added but have not been issued previously.
                    _vehicleRepository.Remove(vehicle); // If from submission or just added in endrosement and not yet issued, hard delete.
                }
            }

            await _vehicleRepository.SaveChangesAsync();
            return riskDetail.Id.ToString();
        }


        public async Task<VehicleDTO> ReinstateVehicleAsync(Guid vehicleId)
        {
            var vehicle = _vehicleRepository.GetById(vehicleId);
            if (!vehicle.IsDeleted)
                throw new InvalidOperationException("Cannot reinstate an active vehicle.");

            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(vehicle.RiskDetail.Id);
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);

            var reinstatedClone = vehicle.CloneForEndorsement();
            reinstatedClone.Reinstate();
            reinstatedClone.EffectiveDate = riskDetail.EndorsementEffectiveDate;
            reinstatedClone.ExpirationDate = brokerInfo.ExpirationDate;
            await _vehicleRepository.AddAsync(reinstatedClone);

            vehicle.UnlinkLatestRiskDetail();

            var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(riskDetail.Id, vehicleId);
            if (vehiclePremium != null)
                _vehiclePremiumRepository.Remove(vehiclePremium);

            await _vehicleRepository.SaveChangesAsync();

            return _mapper.Map<VehicleDTO>(reinstatedClone);
        }

        public async Task<List<VehicleDTO>> GetByRiskDetailIdIncludeAsync(Guid id, bool? fromPolicy = false)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(id);
            var policyHistory = await _policyHistoryRepository.FindAsync(x => x.PreviousRiskDetailId == riskDetail.Id.ToString());
            var previousActivePolicyHistory = await _policyHistoryRepository.GetLastActive(riskDetail.RiskId);
            RiskDetail previousActiveRiskDetail = null;
            IEnumerable<Vehicle> previousVehicles = new List<Vehicle>();

            if (previousActivePolicyHistory != null)
            {
                previousActiveRiskDetail = await _riskDetailRepository.FindAsync(Guid.Parse(previousActivePolicyHistory.PreviousRiskDetailId));
                previousVehicles = await _vehicleRepository.GetPreviousVehiclesByRiskDetail(previousActiveRiskDetail, fromPolicy);
            }

            var result = await _vehicleRepository.GetByRiskDetailAsync(id, fromPolicy);
            var vehicleDTOs = _mapper.Map<List<VehicleDTO>>(result);
            foreach (var vehicleDTO in vehicleDTOs)
            {
                var vehicle = result.First(x => x.Id == vehicleDTO.Id);
                if (vehicleDTO.DeletedDate != null)
                {
                    var previousVehicle = previousVehicles.FirstOrDefault(x => x.MainId == vehicle.MainId);
                    vehicleDTO.CanReinstate = previousVehicle != null && previousVehicle.IsDeleted == true;
                }
            }
            return vehicleDTOs.OrderBy(v => v.CreatedDate).ToList();
        }

        public async Task<List<VehicleDTO>> GetAllIncludeAsync()
        {
            var result = await _vehicleRepository.GetAllIncludeAsync();
            return _mapper.Map<List<VehicleDTO>>(result).ToList();

        }

        public async Task<List<VehicleDTO>> GetByIdIncludeAsync(Guid id)
        {
            var result = await _vehicleRepository.GetAllIncludeAsync(o => o.Id == id);
            return _mapper.Map<List<VehicleDTO>>(result).ToList();
        }

        public async Task<List<VehicleDTO>> GetPreviousVehicles(Guid id)
        {
            var vehicles = new List<Vehicle>();
            var currentVehicle = _vehicleRepository.GetById(id);
            while (currentVehicle.PreviousVehicleVersionId != null)
            {
                currentVehicle = _vehicleRepository.GetById(currentVehicle.PreviousVehicleVersionId.Value);
                vehicles.Add(currentVehicle);
            }

            return _mapper.Map<List<VehicleDTO>>(vehicles).ToList();
        }


        public async Task<List<VehicleDTO>> AddFromExcelFileAsync(Stream fileStream)
        {
            //IExcelManager excelManager = new InteropExcelManager(); //Requires MS Excel installed on Web API

            IExcelManager excelManager = new NpoiExcelManager(); // Check if will work without MS Excel installed
            excelManager.HeaderMap = HeaderMap;

            return await Task.Run(() => excelManager.AddVehicles(fileStream));
        }

        private List<PropertyHeader> _headerMap;
        public List<PropertyHeader> HeaderMap
        {
            get
            {
                return _headerMap ??= new List<PropertyHeader>
                {
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "VIN", Header = "VIN"},
                    new PropertyHeader {PropertyType = typeof(short), PropertyName = "Year", Header = "Year"},
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "Make", Header = "Make"},
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "Model", Header = "Model"},
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "StateFull", Header = "State"},
                    new PropertyHeader {PropertyType = typeof(decimal), PropertyName = "StatedAmount", Header = "TIV (stated Amount)"},
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "Description", Header = "Vehicle Description"},
                    new PropertyHeader {PropertyType = typeof(string), PropertyName = "BusinessClass", Header = "Vehicle Business Class"},
                };
            }
        }

        public async Task UpdatePremiums(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.FindAsync(riskDetailId);
            var submissRiskDetail = await _riskDetailRepository.GetSubmissionRiskDetailAsync(riskDetail.RiskId);
            bool fromPolicy = riskDetail.Id != submissRiskDetail.Id;
            var vehicles = await _vehicleRepository.GetByRiskDetailAsync(riskDetailId, fromPolicy);
            foreach (var vehicle in vehicles)
            {
                var annualPremiums = vehicle.VehiclePremiumRatingFactor ?? VehiclePremiumRatingFactor.Empty(vehicle.Id);
                await UpdatePremiumByPremiumFactor(annualPremiums);
            }
        }

        private async Task UpdatePremiumByPremiumFactor(VehiclePremiumRatingFactor premiumRatingFactor)
        {
            var vehicleId = Guid.Parse(premiumRatingFactor.VehicleId);
            var vehicle = _vehicleRepository.GetById(vehicleId);
            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(vehicle.LatestRiskDetail.RiskId);

            // Get existing premium change else create new
            VehiclePremium vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(vehicle.LatestRiskDetail.Id, vehicle.Id);
            if (vehiclePremium is null)
            {
                vehiclePremium = new VehiclePremium(vehicle.LatestRiskDetail.Id, vehicle.Id);
                await _vehiclePremiumRepository.AddAsync(vehiclePremium);
                await _vehiclePremiumRepository.SaveChangesAsync();
            }

            vehiclePremium.TransactionDate = DateTime.Now.Date;
            // Set previous VehiclePremium to current.
            VehiclePremium previousPremium = await _vehiclePremiumRepository.GetPrevious(vehiclePremium);
            VehiclePremium previousActivePremium = null;
            bool isPolicyReinstated = false;
            vehiclePremium.ChangePrevious(previousPremium);
            if (vehiclePremium.Previous != null)
            {
                var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskId == vehicle.LatestRiskDetail.RiskId.ToString());
                var previousPolicyHistory = await _policyHistoryRepository.GetLatestByRisk(vehicle.LatestRiskDetail.RiskId);
                isPolicyReinstated = policyHistories.OrderByDescending(x => x.DateCreated).First().PolicyStatus == RiskStatus.Reinstated;

                if (isPolicyReinstated)
                {
                    var vehiclePremiums = await _vehiclePremiumRepository.GetByMainId(vehicle.MainId);
                    previousActivePremium = await _vehiclePremiumRepository.GetPreviousActive(vehiclePremium);

                    vehiclePremium.ChangePreviousGrossAL(previousActivePremium.AL.GrossProrated);
                    vehiclePremium.ChangePreviousGrossPD(previousActivePremium.PD.GrossProrated);
                    vehiclePremium.ChangePreviousGrossCargo(previousActivePremium.Cargo.GrossProrated);
                    vehiclePremium.ChangePreviousGrossCompFT(previousActivePremium.CompFT.GrossProrated);
                    vehiclePremium.ChangePreviousGrossCollision(previousActivePremium.Collision.GrossProrated);
                    vehiclePremium.ChangePreviousGrossRefrigeration(previousActivePremium.Refrigeration.GrossProrated);
                }
            }

            // Update annual premiums.
            vehiclePremium.ChangeAnnualAL(premiumRatingFactor.TotalALPremium);
            vehiclePremium.ChangeAnnualPD(premiumRatingFactor.TotalPDPremium);
            vehiclePremium.ChangeAnnualCargo(premiumRatingFactor.CargoPremium);
            vehiclePremium.ChangeAnnualCompFT((premiumRatingFactor.CompPremium ?? 0) + (premiumRatingFactor.FireTheftPremium ?? 0));
            vehiclePremium.ChangeAnnualCollision(premiumRatingFactor.CollisionPremium);
            vehiclePremium.ChangeAnnualRefrigeration(premiumRatingFactor.RefrigerationPremium);

            // Prorate premiums
            DateTime premiumEffective = vehicle.LatestRiskDetail.EndorsementEffectiveDate?.Date ?? brokerInfo.EffectiveDate.Value.Date;
            DateTime premiumExpiration = brokerInfo.ExpirationDate.Value;
            vehiclePremium.Prorate(
                premiumEffective, premiumExpiration,  // Effective dates
                brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value); // Policy Effective dates

            if (isPolicyReinstated && previousActivePremium != null)
            {
                var previous = await _vehiclePremiumRepository.GetPrevious(vehiclePremium);

                vehiclePremium.ChangePreviousGrossAL(previous.AL.GrossProrated);
                vehiclePremium.ChangePreviousGrossPD(previous.PD.GrossProrated);
                vehiclePremium.ChangePreviousGrossCargo(previous.Cargo.GrossProrated);

                vehiclePremium.ChangePreviousGrossCompFT(previous.CompFT.GrossProrated);
                vehiclePremium.ChangePreviousGrossCollision(previous.Collision.GrossProrated);
                vehiclePremium.ChangePreviousGrossRefrigeration(previous.Refrigeration.GrossProrated);

                vehiclePremium.RecalculateNet();

                // reset previous reference.
                vehiclePremium.ChangePrevious(previous);
            }
            else
            {
                vehiclePremium.RecalculateNet();
            }


            //await _vehiclePremiumRepository.UpdateAsync(vehiclePremium);
            _vehiclePremiumRepository.Update(vehiclePremium);
            await _vehiclePremiumRepository.SaveChangesAsync();
        }
        public async Task UpdateVehicleEffectiveDateExpirationDate(Guid riskDetailId, DateTime effectiveDate, DateTime expirationDate)
        {
            var vehicles = await _vehicleRepository.GetByRiskDetailAsync(riskDetailId);

            if (vehicles?.Count() > 0)
            {
                vehicles = vehicles.Select(c => { c.EffectiveDate = effectiveDate; c.ExpirationDate = expirationDate; return c; }).ToList();

                _vehicleRepository.UpdateRange(vehicles);
                await _vehicleRepository.SaveChangesAsync();
            }
        }


        public async Task UpdateMainUseAsync(Guid riskDetailId, short mainUseId)
        {
            var vehicles = await GetByRiskDetailIdIncludeAsync(riskDetailId);
            foreach (var vehicle in vehicles)
            {
                vehicle.UseClassId = mainUseId;
                await UpdateAsync(vehicle);
            }
        }


        public async Task UpdateAccountCategoryAsync(Guid riskDetailId, short accountCategoryId)
        {
            var vehicles = await GetByRiskDetailIdIncludeAsync(riskDetailId);
            foreach (var vehicle in vehicles)
            {
                vehicle.VehicleBusinessClassId = accountCategoryId;
                await UpdateAsync(vehicle);
            }
        }
    }


}
