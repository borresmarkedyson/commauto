﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class BrokerInfoService : IBrokerInfoService
    {
        private readonly IBrokerInfoRepository _repository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskAdditionalInterestRepository _riskAdditionalInterestRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IRiskSequenceService _riskSequence;
        private readonly IVehicleService _vehicleService;
        private readonly IMapper _mapper;

        public BrokerInfoService(IBrokerInfoRepository repository,
            IMapper mapper,
            IEntityAddressRepository entityAddressRepository,
            IRiskDetailRepository riskDetailRepository,
            IRiskRepository riskRepository,
            IRiskSequenceService riskSequence,
            IVehicleService vehicleService,
            IRiskAdditionalInterestRepository riskAdditionalInterestRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _entityAddressRepository = entityAddressRepository;
            _riskDetailRepository = riskDetailRepository;
            _riskAdditionalInterestRepository = riskAdditionalInterestRepository;
            _riskRepository = riskRepository;
            _riskSequence = riskSequence;
            _vehicleService = vehicleService;
        }

        public async Task<List<BrokerInfoDTO>> GetAllAsync()
        {
            var result = await _repository.GetAllAsync();
            return _mapper.Map<List<BrokerInfoDTO>>(result);
        }

        public async Task<BrokerInfoDTO> GetByRiskDetailIdAsync(Guid id)
        {
            var result = await _repository.FindAsync(x => x.RiskId == id);
            return _mapper.Map<BrokerInfoDTO>(result);
        }

        public async Task<BrokerInfoDTO> GetByRiskDetailIdAllIncludeAsync(Guid id)
        {
            var result = await _repository.GetByRiskIdAllIncludeAsync(id);
            return _mapper.Map<BrokerInfoDTO>(result);
        }

        public async Task<List<EntityAddressDTO>> GetAddressesAsync(Guid entityId)
        {
            var addresses = await _entityAddressRepository.GetAsync(x => x.EntityId == entityId && x.AddressTypeId != null, i => i.Address);
            return _mapper.Map<List<EntityAddressDTO>>(addresses);
        }

        public async Task<bool> IsExistAsync(Guid id)
        {
            return await _repository.AnyAsync(x => x.RiskId == id);
        }

        public async Task<BrokerInfoDTO> GetByIdAsync(Guid id)
        {
            var result = await _repository.FindAsync(x => x.Id == id);
            return _mapper.Map<BrokerInfoDTO>(result);
        }

        public async Task<BrokerInfoDTO> InsertAsync(BrokerInfoDTO model)
        {
            var obj = _mapper.Map<BrokerInfo>(model);
            obj.Id = Guid.NewGuid();

            var result = await _repository.AddAsync(obj);
            await _repository.SaveChangesAsync();

            var risk = await _riskRepository.FindAsync(x => x.Id == model.RiskId);

            var riskDetail = await _riskDetailRepository.GetInsuredAddressByRiskIdAsync(risk.Id);

            // Update Policy number
            if (risk.PolicyNumber != null)
            {
                var state = riskDetail.Insured.Entity.EntityAddresses.Find(x => x.AddressTypeId == null).Address.State;
                risk.PolicyNumber = _riskSequence.UpdatePolicyNumber(risk.PolicyNumber, obj.ExpirationDate.Value, obj.EffectiveDate.Value, state);
                await _riskRepository.SaveChangesAsync();
            }

            await _vehicleService.UpdateVehicleEffectiveDateExpirationDate(riskDetail.Id, obj.EffectiveDate.Value, obj.ExpirationDate.Value);

            var returnData = _mapper.Map<BrokerInfoDTO>(obj);
            returnData.PolicyNumber = risk.PolicyNumber;
            return returnData;
        }

        public async Task<BrokerInfoDTO> UpdateAsync(BrokerInfoDTO model)
        {
            if (model.ExpirationDate == null) // set expiration date as defaultif null
            {
                model.ExpirationDate = model.EffectiveDate.Value.AddYears(1);
            }
            var obj = _repository.Update(_mapper.Map<BrokerInfo>(model));
            await _repository.SaveChangesAsync();

            // update interests' effective and expiration date
            var riskDetail = await _riskDetailRepository.FindAsync(riskDetail => riskDetail.RiskId == model.RiskId);
            var additionalInterest = await _riskAdditionalInterestRepository.GetAsync(interest => interest.RiskDetailId == riskDetail.Id);

            foreach (var interest in additionalInterest)
            {
                interest.EffectiveDate = (interest.EffectiveDate >= model.EffectiveDate && interest.EffectiveDate <= model.ExpirationDate) ? interest.EffectiveDate : model.EffectiveDate;
                interest.ExpirationDate = (interest.ExpirationDate >= model.EffectiveDate && interest.ExpirationDate <= model.ExpirationDate) ? interest.ExpirationDate : model.ExpirationDate;
                _riskAdditionalInterestRepository.Update(interest);
            }
            await _riskAdditionalInterestRepository.SaveChangesAsync();

            // Update Policy number
            var risk = await _riskRepository.FindAsync(x => x.Id == model.RiskId);
            if (risk.PolicyNumber != null)
            {
                var riskDetailInsured = await _riskDetailRepository.GetInsuredAddressAsync(riskDetail.Id);
                var state = riskDetailInsured.Insured.Entity.EntityAddresses.Find(x => x.AddressTypeId == null).Address.State;
                risk.PolicyNumber = _riskSequence.UpdatePolicyNumber(risk.PolicyNumber, model.ExpirationDate.Value, model.EffectiveDate.Value, state);
                await _riskRepository.SaveChangesAsync();
            }

            await _vehicleService.UpdateVehicleEffectiveDateExpirationDate(riskDetail.Id, obj.EffectiveDate.Value, obj.ExpirationDate.Value);

            var returnData = _mapper.Map<BrokerInfoDTO>(obj);
            returnData.PolicyNumber = risk.PolicyNumber;
            return returnData;
        }

        public async Task<string> RemoveAsync(Guid id)
        {
            var entity = await _repository.FindAsync(x => x.RiskId == id);
             _repository.Remove(entity);
            await _repository.SaveChangesAsync();
            return id.ToString();
        }
    }
}
