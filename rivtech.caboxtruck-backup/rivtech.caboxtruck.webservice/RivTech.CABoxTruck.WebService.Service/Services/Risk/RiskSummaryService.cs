﻿using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.DataSets;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.DTO.Submission;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Risk;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services.Risk
{
    public class RiskSummaryService : IRiskSummaryService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IPolicyContactsRepository _policyContactsRepository;
        private readonly IBrokerInfoRepository _brokerRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IRiskNotesService _riskNotes;
        private readonly IMainUseRepository _mainUseRepository;
        private readonly IAgencyRepository _agencyRepository;
        private readonly IAgentRepository _agentRepository;

        public RiskSummaryService(IRiskRepository riskRepository, 
                                    IEntityAddressRepository entityAddressRepository,
                                    IPolicyContactsRepository policyContactsRepository,
                                    IBrokerInfoRepository brokerRepository,
                                    IVehicleRepository vehicleRepository,
                                    IRiskNotesService riskNotes,
                                    IMainUseRepository mainUseRepository,
                                    IAgencyRepository agencyRepository,
                                    IAgentRepository agentRepository
                                    )
        {
            _riskRepository = riskRepository;
            _entityAddressRepository = entityAddressRepository;
            _policyContactsRepository = policyContactsRepository;
            _brokerRepository = brokerRepository;
            _vehicleRepository = vehicleRepository;
            _riskNotes = riskNotes;
            _mainUseRepository = mainUseRepository;
            _agencyRepository = agencyRepository;
            _agentRepository = agentRepository;
        }

        public async Task<List<RiskSummaryDto>> GetFilteredSummaryListAsync(SubmissionFilterDto filter)
        {
            var policyContacts = _policyContactsRepository.GetAllAsync().Result;
            var risksFromDb = await Task.Run(() => _riskRepository.GetFilteredSubmissionSummaryListAsync(filter).OrderByDescending(s => s.CreatedDate).ToList());

            foreach (var risk in risksFromDb)
            {
                var businessAddress = _entityAddressRepository.FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business && x.EntityId == risk.InsuredEntityId, i => i.Address).Result;
                var brokerInfo = _brokerRepository.GetByRiskIdAllAsync(risk.RiskId).Result;
                var notes = _riskNotes.GetByRiskIdAsync(risk.CurrentRiskId).Result;
                var vehicles = await _vehicleRepository.GetByRiskDetailAsync(risk.CurrentRiskId);
                //risk.Broker = brokerInfo?.Agency?.Entity?.CompanyName;
                //risk.BrokerContact = $"{brokerInfo?.Agent?.Entity?.FirstName} {brokerInfo?.Agent?.Entity?.LastName}";
                risk.AgencyId = brokerInfo?.AgencyId;
                risk.AgentId = brokerInfo?.AgentId;
                risk.InceptionDate = brokerInfo?.EffectiveDate;
                risk.ExpirationDate = brokerInfo?.ExpirationDate;
                risk.State = businessAddress?.Address?.State;
                risk.AUSpecialist = (brokerInfo != null && brokerInfo?.AssistantUnderwriterId != null) ? policyContacts.FirstOrDefault(x => x.Id == brokerInfo?.AssistantUnderwriterId).Name : "";
                risk.AURep = (brokerInfo != null && brokerInfo?.TeamLeadUserId != null) ? policyContacts.FirstOrDefault(x => x.Id == brokerInfo?.TeamLeadUserId).Name : "";
                risk.AUPSR = (brokerInfo != null && brokerInfo?.PsrUserId != null) ? policyContacts.FirstOrDefault(x => x.Id == brokerInfo?.PsrUserId).Name : "";
                risk.Underwriter = (brokerInfo != null && brokerInfo?.ProductUWUserId != null) ? policyContacts.FirstOrDefault(x => x.Id == brokerInfo?.ProductUWUserId).Name : "";
                risk.NumberOfUnits = vehicles.Count();
                risk.LastNoteAdded = notes?.LastOrDefault(note => note.IsActive)?.CreatedDate;
            }

            // Get all AgencyIds on AgencyApi
            var agencies = await _agencyRepository.GetAllAgenciesByIds(risksFromDb.Where(x => x.AgencyId.HasValue).Select(x => x.AgencyId.Value).ToList());
            var agents = await _agentRepository.GetAllAgentsByIds(risksFromDb.Where(x => x.AgentId.HasValue).Select(x => x.AgentId.Value).ToList());
            foreach (var risk in risksFromDb)
            {
                var agency = agencies.Where(x => x.Id == risk.AgencyId).FirstOrDefault();
                if (agency != null)
                    risk.Broker = agency.Entity.CompanyName;

                var agent = agents.Where(x => x.Id == risk.AgentId).FirstOrDefault();
                if (agent != null)
                    risk.BrokerContact = $"{agent?.Entity?.FirstName} {agent?.Entity?.LastName}";
            }

            if (!string.IsNullOrEmpty(filter.SubmissionNumber))
            {
                risksFromDb = risksFromDb.Where(risk => risk.SubmissionNumber != null && risk.SubmissionNumber.Contains(filter.SubmissionNumber)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.InsuredName))
            {
                filter.InsuredName = filter.InsuredName.ToLower();
                risksFromDb = risksFromDb.Where(risk => risk.InsuredName.ToLower().Contains(filter.InsuredName)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Broker))
            {
                filter.Broker = filter.Broker.ToLower();
                risksFromDb = risksFromDb.Where(risk => !string.IsNullOrEmpty(risk.Broker) && risk.Broker.ToLower().Contains(filter.Broker)).ToList();
            }

            if (filter.InceptionDateFrom != null)
            {
                risksFromDb = risksFromDb.Where(risk => risk.InceptionDate != null && risk.InceptionDate >= filter.InceptionDateFrom).ToList();
            }

            if (filter.InceptionDateTo != null)
            {
                risksFromDb = risksFromDb.Where(risk => risk.InceptionDate != null && risk.InceptionDate <= filter.InceptionDateTo).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Status))
            {
                if (filter.Status == RiskDetailStatus.Active)
                {
                    risksFromDb = risksFromDb.Where(risk => risk.Status != null && (risk.Status.Contains(RiskDetailStatus.Active) || risk.Status.Contains(RiskDetailStatus.PendingEndorsement))).ToList();
                }
                else
                {
                    risksFromDb = risksFromDb.Where(risk => risk.Status != null && risk.Status.Contains(filter.Status)).ToList();
                }
            }

            if (!string.IsNullOrEmpty(filter.PolicyStatus))
            {
                risksFromDb = risksFromDb.Where(risk => risk.PolicyStatus != null && risk.PolicyStatus.Contains(filter.PolicyStatus)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Owner))
            {
                filter.Owner = filter.Owner.ToLower();
                risksFromDb = risksFromDb.Where(risk => risk.Owner != null && risk.Owner.ToLower().Contains(filter.Owner)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Underwriter))
            {
                filter.Underwriter = filter.Underwriter.ToLower();
                risksFromDb = risksFromDb.Where(risk => risk.Underwriter != null && risk.Underwriter.ToLower().Contains(filter.Underwriter)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Au_rep))
            {
                filter.Au_rep = filter.Au_rep.ToLower();
                risksFromDb = risksFromDb.Where(risk => risk.AURep != null && risk.AURep.ToLower().Contains(filter.Au_rep)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.Au_psr))
            {
                filter.Au_psr = filter.Au_psr.ToLower();
                risksFromDb = risksFromDb.Where(risk => risk.AUPSR != null && risk.AUPSR.ToLower().Contains(filter.Au_psr)).ToList();
            }

            if (!string.IsNullOrEmpty(filter.State))
            {
                risksFromDb = risksFromDb.Where(risk => risk.State != null && risk.State == filter.State).ToList();
            }

            if (!string.IsNullOrEmpty(filter.SearchText))
            {
                var searchText = filter.SearchText.Trim().ToLower();

                DateTime.TryParse(searchText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime datevalue);
                var intvalue = ToNullableInt(searchText);

                var mainUseList = await _mainUseRepository.GetAsync(x => x.IsActive);
                var includeMainUse = mainUseList.Where(x => x.Description.ToLower().Contains(searchText)).Select(x => x.Id).ToList();

                // temporary implementation until user management
                var assignedToList = new Dictionary<string, string>() {
                    { "1", "Ashley Bond" },
                    { "2", "Kurt Jensen" },
                    { "3", "William Muller" },
                    { "4", "Kirstine Paulsen" },
                    { "5", "Taylor Redman" },
                    { "6", "Jeff Russell" },
                    { "7", "Thomas Tegley III" },
                    { "8", "Christina Millard" },
                    { "9", "Jonathan Runyan" },
                    { "10", "Debbie Bulik" }
                };

                risksFromDb = risksFromDb.Where(a =>
                    a.SubmissionNumber.ToLower().Contains(searchText) ||
                    (!string.IsNullOrEmpty(a.PolicyNumber) && a.PolicyNumber.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.InsuredName) && a.InsuredName.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.Broker) && a.Broker.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.Owner) && a.Owner.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(assignedToList.GetValueOrDefault(a.AssignedToId ?? string.Empty, string.Empty)) && assignedToList.GetValueOrDefault(a.AssignedToId ?? string.Empty, string.Empty).ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.Underwriter) && a.Underwriter.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.AUSpecialist) && a.AUSpecialist.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.AURep) && a.AURep.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.AUPSR) && a.AUPSR.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.NewVenture) && a.NewVenture.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.NeededBy) && a.NeededBy.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.BrokerContact) && a.BrokerContact.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.QAC) && a.QAC.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.MidTerm) && a.MidTerm.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.ExpiringPolicy) && a.ExpiringPolicy.ToLower().Contains(searchText)) ||
                    (!string.IsNullOrEmpty(a.SubmissionType) && a.SubmissionType.ToLower() == searchText) ||
                    (!string.IsNullOrEmpty(a.PolicyLimit) && a.SubmissionType.ToLower() == searchText) ||
                    (!string.IsNullOrEmpty(a.State) && a.State.ToLower() == searchText) ||
                    (!string.IsNullOrEmpty(a.LossRatio) && a.LossRatio == searchText) ||
                    (!string.IsNullOrEmpty(a.BrokerZip) && a.BrokerZip == searchText) ||
                    (!string.IsNullOrEmpty(a.BrokerState) && a.BrokerState.ToLower() == searchText) ||
                    (!string.IsNullOrEmpty(a.BrokerCity) && a.BrokerCity.ToLower().Contains(searchText)) ||
                    ((includeMainUse.Count > 0 && a.UseClass.HasValue) && includeMainUse.Contains(a.UseClass.Value)) ||
                    a.InceptionDate == datevalue ||
                    a.LastNoteAdded == datevalue ||
                    a.DateSubmitted == datevalue ||
                    (intvalue.HasValue && a.NumberOfUnits == intvalue.Value) ||
                    a.Status.ToLower().Contains(searchText)
                    ).ToList();
            }

            return risksFromDb;
        }

        private int? ToNullableInt(string s)
        {
            int i;
            if (int.TryParse(s, out i)) return i;
            return null;
        }
    }
}
