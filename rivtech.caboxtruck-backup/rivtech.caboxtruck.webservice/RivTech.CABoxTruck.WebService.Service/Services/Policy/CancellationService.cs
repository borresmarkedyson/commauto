﻿using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.Data.Entity.Policy;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Policy;
using RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration;
using RivTech.CABoxTruck.WebService.Service.Services.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IAmount = RivTech.CABoxTruck.WebService.Service.Services.Endorsement.Proration.IAmount;

namespace RivTech.CABoxTruck.WebService.Service.Services.Policy
{
    public class CancellationService : ICancellationService
    {
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly IBindingRepository _bindingRepository;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskManuscriptsRepository _riskManuscriptsRepository;
        private readonly IRiskAdditionalInterestRepository _riskAdditionalInterestRepository;
        private readonly IVehicleService _vehicleService;
        private readonly IAmountFactory _premiumFactory;
        private readonly ICancellationBreakdownRepository _premiumCancellationBreakdownRepository;
        private readonly IRiskCoverageRepository _riskCoverageRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IVehiclePremiumRepository _vehiclePremiumRepository;
        private readonly IRiskFormRepository _riskFormRepository;

        public CancellationService(
            IBrokerInfoRepository brokerInfoRepository,
            IBindingRepository bindingRepository,
            IPolicyHistoryRepository policyHistoryRepository,
            IRiskDetailRepository riskDetailRepository,
            IRiskManuscriptsRepository riskManuscriptsRepository,
            IRiskAdditionalInterestRepository riskAdditionalInterestRepository,
            IVehicleService vehicleService,
            IAmountFactory premiumFactory,
            ICancellationBreakdownRepository premiumCancellationBreakdownRepository,
            IRiskCoverageRepository riskCoverageRepository,
            IVehicleRepository vehicleRepository,
            IVehiclePremiumRepository vehiclePremiumRepository,
            IRiskFormRepository riskFormRepository
        )
        {
            _brokerInfoRepository = brokerInfoRepository;
            _bindingRepository = bindingRepository;
            _policyHistoryRepository = policyHistoryRepository;
            _riskDetailRepository = riskDetailRepository;
            _riskManuscriptsRepository = riskManuscriptsRepository;
            _riskAdditionalInterestRepository = riskAdditionalInterestRepository;
            _vehicleService = vehicleService;
            _premiumFactory = premiumFactory;
            _premiumCancellationBreakdownRepository = premiumCancellationBreakdownRepository;
            _riskCoverageRepository = riskCoverageRepository;
            _vehicleRepository = vehicleRepository;
            _vehiclePremiumRepository = vehiclePremiumRepository;
            _riskFormRepository = riskFormRepository;
        }

        public async Task<RiskAmounts> CancelPremiumFees(Guid riskDetailId, DateTime cancellationDate, bool? withShortRate = null, bool? isFlatCancel = null)
        {
            RiskDetail forEndorsementRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var policyHistory = await _policyHistoryRepository.GetLatestByRisk(forEndorsementRiskDetail.RiskId);
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(Guid.Parse(policyHistory.PreviousRiskDetailId));

            BrokerInfo brokerInfo = await _brokerInfoRepository.GetByRiskIdAllIncludeAsync(riskDetail.RiskId);
            Binding binding = await _bindingRepository.FindAsync(b => b.RiskId == riskDetail.RiskId);

            DateRange policyPeriod = new DateRange(brokerInfo.EffectiveDate.Value, brokerInfo.ExpirationDate.Value);
            RiskAmounts riskAmounts = new RiskAmounts(policyPeriod, binding.MinimumEarned);
            RiskAmounts riskAmountsForSubPDTypes = new RiskAmounts(policyPeriod, binding.MinimumEarned);

            await AddVehiclePremiumsFees(riskAmounts, riskDetail);
            var glPremium = await _premiumFactory.CreateGeneralLiabilityAsync(riskDetail, policyPeriod);
            riskAmounts.Add(glPremium);
            await AddManuscriptsAsync(riskAmounts, riskDetail);
            await AddAdditionalPremiumsAsync(riskAmounts, riskDetail);
            await AddPDPremiums(riskAmounts, riskDetail);

            riskAmounts.Cancel(cancellationDate, withShortRate ?? false, isFlatCancel ?? false);
            await SaveCancellationBreakdowns(riskDetailId, riskAmounts);

            await UpdateCurrentRiskCoveragePremium(riskDetailId, riskAmounts, binding.BindOptionId ?? 1, isFlatCancel ?? false);
            await UpdateVehicleProratedPremiums(riskAmounts, forEndorsementRiskDetail, cancellationDate);
            await UpdateManuscriptProratedPremiums(riskAmounts, forEndorsementRiskDetail);

            return riskAmounts;
        }

        private async Task AddPDPremiums(RiskAmounts riskAmounts, RiskDetail riskDetail)
        {
            var premiums = await _premiumFactory.CreatePDPremiumsAsync(riskDetail);
            foreach (var premium in premiums)
                riskAmounts.Add(premium);
        }

        private async Task SaveCancellationBreakdowns(Guid riskDetailId, RiskAmounts riskAmounts)
        {
            // use to validate canceled amounts that has no reduction but has a cancellation amount, as these should be included in cancellation calculation process
            var vehiclePremiumTypes = new AmountKind[] { AmountKind.VehicleAL, AmountKind.VehiclePD, AmountKind.VehicleCargo };
            foreach (var premium in riskAmounts.Amounts.Where(x => x.AmountReduction < 0 || vehiclePremiumTypes.Contains(x.Info.Kind)))
            {
                if (premium.Info.Kind == AmountKind.VehiclePDCollision || premium.Info.Kind == AmountKind.VehiclePDComprehensive) continue;

                var breakdown = new CancellationBreakdown
                {
                    RiskDetailId = riskDetailId,
                    AmountSubTypeId = GetAmountSubTypeId(premium.Info.Kind),
                    Description = premium.Info.Description,
                    ActiveAmount = premium.Amount,
                    CancelledAmount = premium.CancelledAmount,
                    AmountReduction = premium.AmountReduction,
                };

                if(premium is ProratedAmount proratedPremium)
                {
                    breakdown.AnnualMinimumPremium = proratedPremium.AnnualMinimumEarned;
                    breakdown.MinimumPremium = proratedPremium.MinimumEarned;
                    breakdown.MinimumPremiumAdjustment = proratedPremium.MinimumEarnedAdjustment;
                    breakdown.ShortRatePremium = proratedPremium.ShortRatePremium;
                }

                await _premiumCancellationBreakdownRepository.AddAsync(breakdown);
            }

            await _premiumCancellationBreakdownRepository.SaveChangesAsync();
        }

        private async Task UpdateCurrentRiskCoveragePremium(Guid riskDetailId, RiskAmounts riskAmounts, short optionNumber, bool isFlatCancel)
        {
            var riskCoverages = await _riskCoverageRepository.GetRiskCoveragesAndPremiumsAsync(riskDetailId);
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            bool hasHiredPhysicalDamage = riskForms.Any(x => x.FormId == "HiredPhysicalDamage" && (x.IsSelected ?? false));
            bool hasTrailerInterchange = riskForms.Any(x => x.FormId == "TrailerInterchangeCoverage" && (x.IsSelected ?? false));
            var riskCoverage = riskCoverages.Where(x => x.OptionNumber == optionNumber).FirstOrDefault();
            var riskCoveragePremium = riskCoverage.RiskCoveragePremium;

            // use to validate canceled amounts that has no reduction but has a cancellation amount, as these should be included in cancellation calculation process
            var vehiclePremiumTypes = new AmountKind[] { AmountKind.VehicleAL, AmountKind.VehiclePD, AmountKind.VehicleCargo };
            var cancellationBrkdown = riskAmounts.Amounts.Where(x => x.AmountReduction < 0 || vehiclePremiumTypes.Contains(x.Info.Kind)).ToList();
            
            decimal canceledProratedGeneralLiabilityPremium = cancellationBrkdown.Where(x => GetAmountSubTypeId(x.Info.Kind) == BillingConstants.GeneralLiabilityPremium).Sum(x => x.CancelledAmount.RoundTo2Decimals());
            decimal canceledProratedManuscriptALPremium = cancellationBrkdown.Where(x => GetAmountSubTypeId(x.Info.Kind) == BillingConstants.ALManuscriptPremium).Sum(x => x.CancelledAmount.RoundTo2Decimals());
            decimal canceledProratedManuscriptPDPremium = cancellationBrkdown.Where(x => GetAmountSubTypeId(x.Info.Kind) == BillingConstants.PDManuscriptPremium).Sum(x => x.CancelledAmount.RoundTo2Decimals());
            decimal reductionAmountManuscriptALPremium = cancellationBrkdown.Where(x => GetAmountSubTypeId(x.Info.Kind) == BillingConstants.ALManuscriptPremium).Sum(x => x.AmountReduction.RoundTo2Decimals());
            decimal reductionAmountManuscriptPDPremium = cancellationBrkdown.Where(x => GetAmountSubTypeId(x.Info.Kind) == BillingConstants.PDManuscriptPremium).Sum(x => x.AmountReduction.RoundTo2Decimals());
            decimal canceledProratedTotalPremium = cancellationBrkdown.Where(x => x.Info.Kind != AmountKind.VehiclePDCollision 
                                                                                && x.Info.Kind != AmountKind.VehiclePDComprehensive
                                                                                && x.Info.Kind != AmountKind.PDHired
                                                                                && x.Info.Kind != AmountKind.PDTrailer
                                                                                && x.Info.Kind != AmountKind.ManuscriptPD
                                                                                && x.Info.Kind != AmountKind.ManuscriptAL)
                                                                        .Sum(x => x.CancelledAmount.RoundTo2Decimals()).RoundTo2Decimals();

            riskCoveragePremium.ProratedGeneralLiabilityPremium = canceledProratedGeneralLiabilityPremium;
            riskCoveragePremium.ProratedALManuscriptPremium = (canceledProratedManuscriptALPremium == 0) ? riskCoveragePremium.ALManuscriptPremium : (riskCoveragePremium.ALManuscriptPremium + reductionAmountManuscriptALPremium); // reduction amount is negative already
            riskCoveragePremium.ProratedPDManuscriptPremium = (canceledProratedManuscriptPDPremium == 0) ? riskCoveragePremium.PDManuscriptPremium : (riskCoveragePremium.PDManuscriptPremium + reductionAmountManuscriptPDPremium); // reduction amount is negative already

            if (isFlatCancel)
            {
                riskCoveragePremium.ProratedGeneralLiabilityPremium = 0;
                riskCoveragePremium.GeneralLiabilityPremium = 0;
                riskCoveragePremium.ProratedALManuscriptPremium = 0;
                riskCoveragePremium.ProratedPDManuscriptPremium = 0;
                riskCoveragePremium.AdditionalInsuredPremium = 0;
                riskCoveragePremium.WaiverOfSubrogationPremium = 0;
                riskCoveragePremium.PrimaryNonContributoryPremium = 0;
                riskCoveragePremium.ALManuscriptPremium = 0;
                riskCoveragePremium.PDManuscriptPremium = 0;
                riskCoveragePremium.HiredPhysicalDamage = 0;
                riskCoveragePremium.TrailerInterchange = 0;
                riskCoveragePremium.Premium = 0;
                riskCoveragePremium.ProratedPremium = 0;

                // update riskforms
                if (hasHiredPhysicalDamage)
                {
                    var hiredPDForm = riskForms.FirstOrDefault(x => x.FormId == "HiredPhysicalDamage");
                    if (!string.IsNullOrEmpty(hiredPDForm.Other))
                    {
                        var hiredPhysicalDmg = JsonConvert.DeserializeObject<HiredPhysdamDTO>(hiredPDForm.Other);
                        hiredPhysicalDmg.EstimatedAdditionalPremium = 0;
                        hiredPDForm.Other = JsonConvert.SerializeObject(hiredPhysicalDmg);
                        await _riskFormRepository.UpdateRiskForm(hiredPDForm);
                    }
                }

                if (hasTrailerInterchange)
                {
                    var trailerPDForm = riskForms.FirstOrDefault(x => x.FormId == "TrailerInterchangeCoverage");
                    if (!string.IsNullOrEmpty(trailerPDForm.Other))
                    {
                        var trailerInterchangeCoverage = JsonConvert.DeserializeObject<TrailerInterchangeDTO>(trailerPDForm.Other);
                        trailerInterchangeCoverage.CollisionPremium = 0;
                        trailerInterchangeCoverage.ComprePremium = 0;
                        trailerInterchangeCoverage.FirePremium = 0;
                        trailerInterchangeCoverage.FireTheftPremium = 0;
                        trailerInterchangeCoverage.LossPremium = 0;
                        trailerPDForm.Other = JsonConvert.SerializeObject(trailerInterchangeCoverage);
                        await _riskFormRepository.UpdateRiskForm(trailerPDForm);
                    }
                }
            }

            // include fully earned premiums + prorated manuscripts
            canceledProratedTotalPremium = canceledProratedTotalPremium + 
                                            (riskCoveragePremium.ProratedALManuscriptPremium ?? 0) + 
                                            (riskCoveragePremium.ProratedPDManuscriptPremium ?? 0) +
                                            (riskCoveragePremium.AdditionalInsuredPremium ?? 0) +
                                            (riskCoveragePremium.WaiverOfSubrogationPremium ?? 0) +
                                            (riskCoveragePremium.PrimaryNonContributoryPremium ?? 0);
            
            if (hasHiredPhysicalDamage && !isFlatCancel)
            {
                canceledProratedTotalPremium += riskCoveragePremium.HiredPhysicalDamage ?? 0;
            }

            if (hasTrailerInterchange && !isFlatCancel)
            {
                canceledProratedTotalPremium += riskCoveragePremium.TrailerInterchange ?? 0;
            }

            riskCoveragePremium.ProratedPremium = canceledProratedTotalPremium;

            riskCoverage.RiskCoveragePremium = riskCoveragePremium;
            _riskCoverageRepository.Update(riskCoverage);
            await _riskCoverageRepository.SaveChangesAsync();
        }

        private async Task UpdateVehicleProratedPremiums(RiskAmounts riskAmounts, RiskDetail newRiskDetail, DateTime cancellationDate)
        {
            foreach (var riskAmount in riskAmounts.Amounts.Where(x => x.AmountReduction < 0))
            {
                var vehiclePremiumTypes = new AmountKind[] { AmountKind.VehicleAL, AmountKind.VehiclePD, AmountKind.VehicleCargo, AmountKind.VehiclePDCollision, AmountKind.VehiclePDComprehensive };

                if (!vehiclePremiumTypes.Contains(riskAmount.Info.Kind)) continue; // if manuscript or GL, should not proceed

                Vehicle vehicle = newRiskDetail.Vehicles.Find(vehicle => vehicle.PreviousVehicleVersionId == Guid.Parse(riskAmount.Info.Id.ToString()));

                if (vehicle is null) continue;

                var vehiclePremium = await _vehiclePremiumRepository.GetByRiskDetailVehicleIdAsync(newRiskDetail.Id, vehicle.Id);
                if (vehiclePremium is null)
                {
                    var latestVehiclePremium = await _vehiclePremiumRepository.GetLatestByMainId(vehicle.MainId);
                    vehiclePremium = latestVehiclePremium.Clone();
                    vehiclePremium.RiskDetailId = newRiskDetail.Id;
                    vehiclePremium.VehicleId = vehicle.Id;
                    await _vehiclePremiumRepository.AddAsync(vehiclePremium);
                    await _vehicleRepository.SaveChangesAsync();
                }

                vehiclePremium.TransactionDate = DateTime.Now.Date;
                vehiclePremium.EffectiveDate = cancellationDate;

                switch (riskAmount.Info.Kind)
                {
                    case AmountKind.VehicleAL:
                        vehiclePremium.SetCancelAL(riskAmount.CancelledAmount.RoundTo2Decimals());
                        break;
                    case AmountKind.VehiclePD:
                        vehiclePremium.SetCancelPD(riskAmount.CancelledAmount.RoundTo2Decimals());
                        break;
                    case AmountKind.VehicleCargo:
                        vehiclePremium.SetCancelCargo(riskAmount.CancelledAmount.RoundTo2Decimals());
                        break;
                    case AmountKind.VehiclePDCollision:
                        vehiclePremium.SetCancelCollision(riskAmount.CancelledAmount.RoundTo2Decimals());
                        break;
                    case AmountKind.VehiclePDComprehensive:
                        vehiclePremium.SetCancelCompFT(riskAmount.CancelledAmount.RoundTo2Decimals());
                        break;
                }

                //await _vehiclePremiumRepository.UpdateAsync(vehiclePremium);
                _vehiclePremiumRepository.Update(vehiclePremium);
                await _vehicleRepository.SaveChangesAsync();
            }
        }

        private async Task UpdateManuscriptProratedPremiums(RiskAmounts riskAmounts, RiskDetail newRiskDetail)
        {
            foreach (var riskAmount in riskAmounts.Amounts.Where(x => x.AmountReduction < 0))
            {
                if (riskAmount.Info.Kind != AmountKind.ManuscriptAL &&
                    riskAmount.Info.Kind != AmountKind.ManuscriptPD) continue;

                await _riskManuscriptsRepository.CancelManuscript(newRiskDetail.Id, (long)riskAmount.Info.Id, riskAmount.CancellationDate.Value, riskAmount.CancelledAmount);
            }
        }
        
        private string GetAmountSubTypeId(AmountKind premiumKind)
        {
            switch(premiumKind)
            {
                case AmountKind.VehicleAL: return BillingConstants.VehicleALP;
                case AmountKind.VehiclePD: return BillingConstants.VehiclePDP;
                case AmountKind.VehicleCargo: return BillingConstants.VehicleCP;
                case AmountKind.GeneralLiability: return BillingConstants.GeneralLiabilityPremium;
                case AmountKind.ManuscriptAL: return BillingConstants.ALManuscriptPremium;
                case AmountKind.ManuscriptPD: return BillingConstants.PDManuscriptPremium;
                case AmountKind.AdditionalPremiumAI: return BillingConstants.AdditionalInsured;
                case AmountKind.AdditionalPremiumPNC: return BillingConstants.PrimaryAndNoncontributory;
                case AmountKind.AdditionalPremiumWOS: return BillingConstants.WaiverOfSubrogation;
                case AmountKind.PDHired: return BillingConstants.HiredPhysicalDamageEndst;
                case AmountKind.PDTrailer: return BillingConstants.TrailerInterchangeEndst;
                case AmountKind.ServiceFeeAL: return BillingConstants.ServiceFeeAL;
                case AmountKind.ServiceFeePD: return BillingConstants.ServiceFeePD;
                default: return null;
            }
        }

        private async Task AddVehiclePremiumsFees(RiskAmounts riskAmounts, RiskDetail riskDetail)
        {
            foreach (var vehicle in riskDetail.Vehicles)
            {
                IAmount premiumVehicleAL = await _premiumFactory.CreateVehicleAL(vehicle, riskAmounts.PolicyPeriod);
                riskAmounts.Add(premiumVehicleAL);

                IAmount premiumVehiclePD = await _premiumFactory.CreateVehiclePD(vehicle, riskAmounts.PolicyPeriod);
                riskAmounts.Add(premiumVehiclePD);

                IAmount premiumVehicleCargo = await _premiumFactory.CreateVehicleCargo(vehicle, riskAmounts.PolicyPeriod);
                riskAmounts.Add(premiumVehicleCargo);

                IAmount premiumVehicleCollision = await _premiumFactory.CreateVehiclePDCollision(vehicle, riskAmounts.PolicyPeriod);
                riskAmounts.Add(premiumVehicleCollision);
                
                IAmount premiumVehicleComprehensive = await _premiumFactory.CreateVehiclePDComprehensive(vehicle, riskAmounts.PolicyPeriod);
                riskAmounts.Add(premiumVehicleComprehensive);

                var vehicleFees = await _premiumFactory.CreateVehicleFees(vehicle, riskDetail);
                riskAmounts.Add(vehicleFees);
            }
        }

        private async Task AddManuscriptsAsync(RiskAmounts riskAmounts, RiskDetail riskDetail)
        {
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetail.Id);

            foreach (var manuscript in manuscripts)
            {
                IAmount manuscriptPremium = await _premiumFactory.CreateManuscriptAsync(manuscript, riskAmounts.PolicyPeriod);
                riskAmounts.Add(manuscriptPremium);
            }
        }

        private async Task AddAdditionalPremiumsAsync(RiskAmounts riskAmounts, RiskDetail riskDetail)
        {
            var additionalInterests = (await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetail.Id))?.ToList()
                    ?? new List<RiskAdditionalInterest>();

            // if there are no records, and yet blankets are selected. Create a placeholder.
            if (additionalInterests.Count < 1)
            {
                var tmpAdditionalInterest = new RiskAdditionalInterest()
                {
                    Id = riskDetail.Id,
                    IsPrimaryNonContributory = riskDetail.IsPrimaryNonContributory ?? false,
                    AdditionalInterestTypeId = (riskDetail.IsAdditionalInsured ?? false) ? AdditionalInterestType.AdditionalInsured.Id : ((short)-1),
                    IsWaiverOfSubrogation = riskDetail.IsWaiverOfSubrogation ?? false
                };

                additionalInterests.Add(tmpAdditionalInterest);
            }

            // Additional insured
            bool isAIBlanket = riskDetail.IsAdditionalInsured == true;
            var aiCount = additionalInterests.Count(i => i.AdditionalInterestTypeId == AdditionalInterestType.AdditionalInsured.Id);
            if(isAIBlanket && aiCount > 0)
            {
                aiCount = 1;
            }
            for (int i = 0; i < aiCount; i++)
            {
                IAmount premium = _premiumFactory.CreateAdditionalPremium(additionalInterests[i], AdditionalPremiumType.AdditionalInsured, isAIBlanket);
                riskAmounts.Add(premium);
            }

            // Waiver of Subrogation
            bool isWoSBlanket = riskDetail.IsWaiverOfSubrogation == true;
            var wosCount = additionalInterests.Count(i => i.IsWaiverOfSubrogation);
            if(isWoSBlanket && wosCount > 0)
            {
                wosCount = 1;
            }
            for (int i = 0; i < wosCount; i++)
            {
                IAmount premium = _premiumFactory.CreateAdditionalPremium(additionalInterests[i], AdditionalPremiumType.WaiverOfSubrogation, isWoSBlanket);
                riskAmounts.Add(premium);
            }

            // Primary Non-Contributory
            bool isPnCBlanket = riskDetail.IsPrimaryNonContributory == true;
            var pncCount = additionalInterests.Count(i => i.IsPrimaryNonContributory);
            if(isPnCBlanket && pncCount > 0)
            {
                pncCount = 1;
            }
            for (int i = 0; i < pncCount; i++)
            {
                IAmount premium = _premiumFactory.CreateAdditionalPremium(additionalInterests[i], AdditionalPremiumType.PrimaryNonContributory, isPnCBlanket);
                riskAmounts.Add(premium);
            }
        }
    }
}
