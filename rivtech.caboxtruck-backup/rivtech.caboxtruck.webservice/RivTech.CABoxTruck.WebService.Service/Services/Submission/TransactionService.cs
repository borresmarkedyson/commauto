﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.Service.IServices.DocumentGeneration;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.DTO.Enums;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Service.IServices.Policy;
using RivTech.CABoxTruck.WebService.Service.IServices.Emails;

namespace RivTech.CABoxTruck.WebService.Service.Services.Submission
{
    public class TransactionService : ITransactionService
    {
        private readonly IErrorLogRepository _errorLogRepository;
        private readonly IMapper _mapper;
        private readonly IRiskFactory _riskFactory;
        private readonly IPacketReportService _packetReportService;
        private readonly IQuoteReportService _quoteReportService;
        private readonly IInvoiceReportService _invoiceReportService;
        private readonly IBillingService _billingService;
        private readonly IBindingRepository _bindingRepository;
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskSequenceService _riskSequenceService;
        private readonly IPolicyHistoryService _policyHistoryService;
        private readonly IEndorsementPendingChangeRepository _endorsementPendingChangeRepository;
        private readonly IEndorsementService _endorsementService;
        private readonly ICancellationService _cancellationService;
        private readonly IReinstatementService _reinstatementService;
        private readonly ICancellationBreakdownRepository _cancellationBreakdownRepository;
        private readonly IFormsService _formsService;
        private readonly IRaterService _raterService;
        private readonly IRiskService _riskService;
        private readonly IFileUploadDocumentService _fileUploadDocumentService;
        private readonly IVehicleService _vehicleService;
        private readonly ILog4NetService _errorLogger;
        private readonly IEmailQueueService _emailQueueService;
        private readonly IInvoiceEmailService _invoiceEmailService;

        public TransactionService(
            IErrorLogRepository errorLogRepository,
            IMapper mapper,
            IRiskFactory riskFactory,
            IPacketReportService packetReportService,
            IQuoteReportService quoteReportService,
            IInvoiceReportService invoiceReportService,
            IBillingService billingService,
            IBindingRepository bindingRepository,
            IBrokerInfoRepository brokerInfoRepository,
            IRiskDetailRepository riskDetailRepository,
            IRiskSequenceService riskSequenceService,
            IRiskRepository riskRepository,
            IPolicyHistoryService policyHistoryService,
            IEndorsementPendingChangeRepository endorsementPendingChangeRepository,
            IEndorsementService endorsementService,
            ICancellationService cancellationService,
            ICancellationBreakdownRepository cancellationBreakdownRepository,
            IFormsService formsService,
            IRaterService raterService,
            IRiskService riskService,
            IFileUploadDocumentService fileUploadDocumentService,
            IVehicleService vehicleService,
            ILog4NetService errorLogger,
            IReinstatementService reinstatementService,
            IInvoiceEmailService invoiceEmailService
        ) {
            _errorLogRepository = errorLogRepository;
            _mapper = mapper;
            _riskFactory = riskFactory;
            _packetReportService = packetReportService;
            _quoteReportService = quoteReportService;
            _invoiceReportService = invoiceReportService;
            _billingService = billingService;
            _bindingRepository = bindingRepository;
            _brokerInfoRepository = brokerInfoRepository;
            _riskRepository = riskRepository;
            _riskDetailRepository = riskDetailRepository;
            _riskSequenceService = riskSequenceService;
            _policyHistoryService = policyHistoryService;
            _endorsementPendingChangeRepository = endorsementPendingChangeRepository;
            _endorsementService = endorsementService;
            _cancellationService = cancellationService;
            _cancellationBreakdownRepository = cancellationBreakdownRepository;
            _reinstatementService = reinstatementService;
            _formsService = formsService;
            _raterService = raterService;
            _cancellationBreakdownRepository = cancellationBreakdownRepository;
            _riskService = riskService;
            _fileUploadDocumentService = fileUploadDocumentService;
            _vehicleService = vehicleService;
            _errorLogger = errorLogger;
            _invoiceEmailService = invoiceEmailService;
        }

        public async Task<RiskDTO> IssueSubmission(Guid riskDetailId)
        {
            // billing api bind process
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var vehicles = riskDetail.RiskDetailVehicles.Select(rdv => rdv.Vehicle).Where(x => x.Options != null && x.Options.Contains(binding.BindOptionId.Value.ToString()) && x.IsActive != false).ToList();
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // Generate Policy Number if Issued without one
            var state = riskDetail.Insured.Entity.EntityAddresses.Find(x => x.AddressTypeId == null).Address.State;
            if (risk.PolicyNumber == null)
            {
                risk.PolicyNumber = _riskSequenceService.GeneratePolicyNumber(state, brokerInfo.ExpirationDate.Value, brokerInfo.EffectiveDate.Value);
            }

            // Billing Bind
            var billingBind = _billingService.ProcessBillingBind(risk.PolicyNumber, brokerInfo.EffectiveDate.Value, riskDetail, binding, riskCoverage, vehicles);

            await _formsService.SaveFormsContentAsync(riskDetail);

            // record data generation
            var policyDetail = await _riskFactory.GeneratePolicy(riskDetailId, risk.PolicyNumber);

            // policy packet data generation
            var endorsementPlaceholder = new EndorsementDetailsDTO { Risk = _mapper.Map<RiskDTO>(policyDetail) };
            await _packetReportService.GenerateSavePolicyPacketReport(riskDetailId, endorsementPlaceholder);

            //Quote Proposal Save
            await _quoteReportService.GenerateSaveQuoteReport(riskDetailId); // Generate quote proposal using quoted RiskDetailId

            //Invoice Save
            var reportResponse = await _invoiceReportService.GenerateInvoiceReport<string>(_mapper.Map<InvoiceDTO>(billingBind), returnUrl: true);
            //await _invoiceEmailService.SendEmailInvoiceToBrokerAgent(riskDetail.RiskId, reportResponse.Output);

            // create policy History
            await _policyHistoryService.CreatePolicyHistory(policyDetail.Id, riskDetailId, risk.Id, riskCoverage.RiskCoveragePremium, brokerInfo.EffectiveDate.Value, policyDetail.Status);

            return _mapper.Map<RiskDTO>(policyDetail);
        }

        public async Task<RiskDTO> IssueEndorsement(Guid riskDetailId, DateTime effectiveDate, string endorsementText, string policyChanges)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();
            var pendingChanges = _endorsementPendingChangeRepository.GetAsync(x => x.RiskDetailId == riskDetailId).Result.ToList();

            // get premium changes for endorsement documents
            var endorsementPremiumChange = await _endorsementService.GetPremiumChanges(riskDetail.Id);
            var aiChanges = await _riskFactory.GetAdditionalInterestChanges(riskDetailId, riskDetail.RiskId);

            // Endorsement Billing Bind
            var billingBind = _billingService.ProcessBillingBindEndorsement(riskDetail.RiskId, effectiveDate, pendingChanges);

            await _formsService.SaveFormsContentAsync(riskDetail, effectiveDate);

            // record data generation
            var policyDetailForEndorsement = await _riskFactory.GeneratePolicyDetailForEndorsement(riskDetailId, effectiveDate);

            // create policy History
            var endorsementNumber = await _policyHistoryService.CreatePolicyHistory(policyDetailForEndorsement.Id, riskDetailId, policyDetailForEndorsement.RiskId, riskCoverage.RiskCoveragePremium, effectiveDate, policyDetailForEndorsement.Status, policyChanges);

            //Invoice Save
            if (billingBind != null)
            {
                try
                {
                    var invoice = _mapper.Map<InvoiceDTO>(billingBind);
                    invoice.InvoiceType = InvoiceType.Endorsement;
                    invoice.RiskDetailId = riskDetailId;
                    invoice.PolicyChanges = policyChanges;
                    await _invoiceReportService.GenerateInvoiceReport<string>(invoice, returnUrl: true);
                }
                catch { }
            }

            // endorsement document
            var endorsementDetail = new EndorsementDetailsDTO
            {
                EndorsementNumber = endorsementNumber,
                Description = endorsementText,
                PolicyChanges = policyChanges,
                EffectiveDate = effectiveDate,
                PremiumChange = endorsementPremiumChange,
                AdditionalInterestChanges = aiChanges,
                Risk = _mapper.Map<RiskDTO>(policyDetailForEndorsement)
            };

            // Initial RiskDetail handles the useruploaded forms
            var initialRiskDetails = await _riskDetailRepository.GetAsync(x => x.RiskId == riskDetail.RiskId
                                                && x.Id != riskDetail.Id && x.Status == RiskDetailStatus.Quoted);
            var initialRiskDetailId = initialRiskDetails.OrderBy(x => x.UpdatedDate).Select(x => x.Id).FirstOrDefault();

            // policy packet data generation
            var fileName = $"Policy Packet_{risk.PolicyNumber}_{DateTime.Now:MMddyyyy_HHmmss}";
            await _packetReportService.GenerateSavePolicyPacketReport(riskDetailId, endorsementDetail, fileName, initialRiskDetailId);

            var endorsementForms = GetEndorsementForms(endorsementNumber, risk.PolicyNumber, policyChanges, endorsementPremiumChange);
            await _packetReportService.GenerateEndorsementDocuments(endorsementForms, _mapper.Map<InvoiceDTO>(billingBind), endorsementDetail);

            return _mapper.Map<RiskDTO>(policyDetailForEndorsement);
        }

        public async Task<RiskDTO> ResetPolicyChanges(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.FindAsync(x => x.Id == riskDetailId);

            if (riskDetail is null) throw new ArgumentNullException(nameof(Data.Entity.RiskDetail));

            var previousPolicyDetail = await _riskFactory.GetPreviousPolicyDetail(riskDetailId, riskDetail.RiskId);

            var policyHistory = await _policyHistoryService.GetLastActiveByRiskId(previousPolicyDetail.RiskId);
            var nextEndorsement = Convert.ToInt32(policyHistory.EndorsementNumber) + 1;
            await _raterService.DeleteArchive(previousPolicyDetail.SubmissionNumber, nextEndorsement);

            return _mapper.Map<RiskDTO>(previousPolicyDetail);
        }

        public async Task<List<string>> GetPolicyChanges(Guid riskDetailId)
        {
            try
            {
                var riskDetail = await _riskDetailRepository.FindAsync(x => x.Id == riskDetailId);

                return await _riskFactory.GetPolicyChanges(riskDetailId, riskDetail.RiskId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<EndorsementFormDTO> GetEndorsementForms(int endorsementNumber, string policyNumber, string policyChanges, EndorsementPremiumChangeSummaryDTO endorsementPremiumChange)
        {
            bool hasPremiumChanges = endorsementPremiumChange.TotalPremiumChangeList.Where(x => x.Description == "Total Premium").Select(x => x.EndorsementPendingChange.PremiumDifference).FirstOrDefault() != 0;
            var decPageTriggers = new[] { "Added Form", "Removed Form" };
            var namedInsuredTriggers = new[] { "Changed Business Name" };
            var endorsementForm = new EndorsementFormDTO
            {
                Description = $"Endorsement {endorsementNumber}",
                FileName = $"Endorsement_{endorsementNumber}_{policyNumber}_{DateTime.Now:MMddyyyy_HHmmss}",
                RiskForms = new List<RiskFormDTO>()
            };
            endorsementForm.RiskForms.Add(new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "PotentialChangeEndorsement" } });

            if (namedInsuredTriggers.Any(policyChanges.Contains))
            {
                endorsementForm.RiskForms.Add(new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "NamedInsuredEndorsement" } });
            }
            if (decPageTriggers.Any(policyChanges.Contains) || hasPremiumChanges)
            {
                endorsementForm.RiskForms.Add(new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "BusinessAutoPolicyDeclarations" } });
            }

            var endorsementForms = new List<EndorsementFormDTO> { endorsementForm };
            return endorsementForms;
        }

        public async Task<RiskDTO> CancelPolicy(Guid riskDetailId, DateTime effectiveDate, string cancellationReasonType, bool isShortRatePremium, bool? isFlatCancel = null, bool isAutoCancel = false)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            if (cancellationReasonType == Data.Entity.DataSets.CancellationReasons.CancelFlat.Id.ToString())
            {
                isFlatCancel = true;
            }

            // reset and re-clone riskdetail
            var policyDetailForCancellation = await _riskFactory.GeneratePolicyDetailForCancellation(riskDetailId, riskDetail.RiskId, effectiveDate);

            // pro-rate/short rate
            // Cancel premium/fees.
            // Generate transactions from riskAmounts.
            await _cancellationService.CancelPremiumFees(policyDetailForCancellation.Id, effectiveDate, isShortRatePremium, isFlatCancel ?? false);

            // pass premium changes to billing
            var cancellationBreakdowns = _cancellationBreakdownRepository.GetByRiskDetailId(riskDetailId);
            var billingCancellation = await _billingService.ProcessBillingCancellation(risk.Id, cancellationBreakdowns, isFlatCancel ?? false);

            // policy history
            var canceledRiskCoverage = policyDetailForCancellation.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();
            var endorsementNumber = await _policyHistoryService.CreatePolicyHistory(policyDetailForCancellation.Id, riskDetail.Id, risk.Id, canceledRiskCoverage.RiskCoveragePremium, effectiveDate, RiskStatus.Canceled, cancellationReasonType);

            // generate cancellation notice/final audit invoice
            await GenerateCancellationDocuments(policyDetailForCancellation, billingCancellation, effectiveDate, cancellationReasonType, isAutoCancel);

            return _mapper.Map<RiskDTO>(policyDetailForCancellation);
        }

        public async Task<RiskDTO> NoticeCancelPolicy(Guid riskDetailId, DateTime effectiveDate, string cancellationReasonType)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // generate cancellation notice/final audit invoice
            await GenerateCancellationDocumentsOnly(riskDetail, effectiveDate, cancellationReasonType);

            return _mapper.Map<RiskDTO>(riskDetail);
        }

        public async Task<RiskDTO> ReinstatePolicy(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var vehicles = riskDetail.RiskDetailVehicles.Select(rdv => rdv.Vehicle).Where(x => x.Options != null && x.Options.Contains(binding.BindOptionId.Value.ToString()) && x.IsActive != false).ToList();
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // clone riskdetail before cancellation
            var reinstatedPolicyDetail = await _riskFactory.GeneratePolicyDetailForReinstatement(riskDetailId, riskDetail.RiskId);

            // generate policy history
            var reinstatedRiskCoverage = reinstatedPolicyDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();
            await _policyHistoryService.CreatePolicyHistory(reinstatedPolicyDetail.Id, riskDetail.Id, risk.Id, reinstatedRiskCoverage.RiskCoveragePremium, DateTime.Now, RiskDetailStatus.Reinstated);

            // process billing
            await _reinstatementService.ReverseCancellationAmounts(risk.Id, reinstatedPolicyDetail.Id);
            var resinstatementBreakdowns = await _reinstatementService.GetLatestAmountBreakdownsByRisk(risk.Id);
            await _billingService.ProcessBillingReinstatement(risk.Id, resinstatementBreakdowns);

            // generate reinstatement notice
            await GenerateReinstatementDocuments(reinstatedPolicyDetail);

            return _mapper.Map<RiskDTO>(reinstatedPolicyDetail);
        }

        public async Task<RiskDTO> ReinstatePendingCancellationPolicy(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var vehicles = riskDetail.RiskDetailVehicles.Select(rdv => rdv.Vehicle).Where(x => x.Options != null && x.Options.Contains(binding.BindOptionId.Value.ToString()) && x.IsActive != false).ToList();
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // generate reinstatement notice
            await GenerateReinstatementDocuments(riskDetail);

            return _mapper.Map<RiskDTO>(riskDetail);
        }

        private async Task GenerateCancellationDocuments(RiskDetail policyDetail, InvoiceDTO billing, DateTime effectiveDate, string cancellationReasonId, bool isAutoCancel = false)
        {
            billing.InvoiceType = InvoiceType.Cancellation;
            billing.InvoiceNumber = null;
            await _invoiceReportService.GenerateInvoiceReport<string>(_mapper.Map<InvoiceDTO>(billing));

            var cancellationForm = new EndorsementFormDTO
            {
                Description = isAutoCancel ? "Final Cancellation" : "Cancellation Notice",
                FileName = $"Cancellation_Notice_{DateTime.Now:MMddyyyy_HHmmss}",
                RiskForms = new List<RiskFormDTO> { new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "CancellationNotice" } } }
            };
            var forms = new List<EndorsementFormDTO> { cancellationForm };
            var endorsement = new EndorsementDetailsDTO { Risk = _mapper.Map<RiskDTO>(policyDetail), EffectiveDate = effectiveDate, CancellationReasonId = cancellationReasonId };

            await _packetReportService.GenerateEndorsementDocuments(forms, billing, endorsement);
        }

        private async Task GenerateCancellationDocumentsOnly(RiskDetail policyDetail, DateTime effectiveDate, string cancellationReasonId)
        {
            var cancellationForm = new EndorsementFormDTO
            {
                Description = "Notice of Pending Cancellation",
                FileName = $"Notice_of_Pending_Cancellation_{DateTime.Now:MMddyyyy_HHmmss}",
                RiskForms = new List<RiskFormDTO> { new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "CancellationNotice" } } }
            };
            var forms = new List<EndorsementFormDTO> { cancellationForm };
            var endorsement = new EndorsementDetailsDTO { Risk = _mapper.Map<RiskDTO>(policyDetail), EffectiveDate = effectiveDate, CancellationReasonId = cancellationReasonId };

            await _packetReportService.GenerateEndorsementDocuments(forms, new InvoiceDTO(), endorsement);
        }

        private async Task GenerateReinstatementDocuments(RiskDetail policyDetail)
        {
            var reinstatementForm = new EndorsementFormDTO
            {
                Description = "Reinstatement Notice",
                FileName = $"Reinstatement_Notice_{DateTime.Now:MMddyyyy_HHmmss}",
                RiskForms = new List<RiskFormDTO> { new RiskFormDTO { Id = Guid.NewGuid(), Form = new FormDTO { Id = "ReinstatementNotice" } } }
            };

            var forms = new List<EndorsementFormDTO> { reinstatementForm };
            var endorsement = new EndorsementDetailsDTO { Risk = _mapper.Map<RiskDTO>(policyDetail) };
            await _packetReportService.GenerateEndorsementDocuments(forms, null, endorsement);
        }


        public async Task<RiskDTO> RewritePolicyToSubmission(Guid riskDetailId, DateTime effectiveDate, bool? isFlatCancel = true)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // rewrite: re-clone quoted risk detail
            string newSubmissionNumber = _riskSequenceService.GenerateSubmissionNumber();
            string newQuoteNumber = _riskSequenceService.GenerateQuoteNumber();
            var newSubmissionDetail = await _riskFactory.GenerateSubmissionDetailFromPolicy(riskDetailId, risk.Id, newSubmissionNumber, newQuoteNumber);

            await _raterService.CopyRaterAsync(riskDetail.SubmissionNumber, newSubmissionNumber);

            try
            {
                var result = await _quoteReportService.GenerateQuoteReport(newSubmissionDetail.Id);
                await _riskService.UpdateRiskStatusOnlyAsync(newSubmissionDetail.Id, RiskDetailStatus.Quoted);
                var quoteUploadData = new FileUploadDocumentDTO()
                {
                    RiskDetailId = newSubmissionDetail.Id,
                    tableRefId = newSubmissionDetail.Id,
                    FileName = "Pre-Bind Quote",
                    FilePath = result,
                    IsConfidential = true,
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                };
                await _fileUploadDocumentService.AddUpdateAsync(quoteUploadData);
            }
            catch (Exception e)
            {

            }

            await _vehicleService.UpdatePremiums(newSubmissionDetail.Id);

            return _mapper.Map<RiskDTO>(newSubmissionDetail);
        }

        public async Task CancelPolicyForRewrite(Guid riskDetailId, DateTime effectiveDate, bool? isFlatCancel = true)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            var brokerInfo = await _brokerInfoRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var binding = await _bindingRepository.FindAsync(x => x.RiskId == riskDetail.RiskId);
            var riskCoverage = riskDetail.RiskCoverages.Where(x => x.OptionNumber == binding.BindOptionId).FirstOrDefault();

            // still proceed if policy effective date is in future and need to rewrite policy
            if (effectiveDate < brokerInfo.EffectiveDate.Value)
            {
                effectiveDate = brokerInfo.EffectiveDate.Value;
            }

            // cancellation: reset and re-clone riskdetail
            var policyDetailForCancellation = await _riskFactory.GeneratePolicyDetailForCancellation(riskDetailId, riskDetail.RiskId, effectiveDate);

            // pro-rate/short rate
            // Cancel premium/fees.
            // Generate transactions from riskAmounts.
            await _cancellationService.CancelPremiumFees(riskDetailId, effectiveDate, false, isFlatCancel ?? false);

            // pass premium changes to billing
            var cancellationBreakdowns = _cancellationBreakdownRepository.GetByRiskDetailId(riskDetailId);
            await _billingService.ProcessBillingCancellation(risk.Id, cancellationBreakdowns, isFlatCancel ?? false);

            // policy history
            var cancellationForRewriteDetails = new List<string>() { "Cancellation Information not found." };
            var endorsementNumber = await _policyHistoryService.CreatePolicyHistory(policyDetailForCancellation.Id, riskDetail.Id, risk.Id, riskCoverage.RiskCoveragePremium, effectiveDate, RiskDetailStatus.Rewrite, JsonConvert.SerializeObject(cancellationForRewriteDetails));
        }

        public async Task AutoPolicyCancellation(DateTime effectiveDate)
        {
            var riskids = await _riskRepository.GetAllRiskIdForCancellationAsync(effectiveDate);
            if (riskids != null && riskids.Count() > 0)
            {
                foreach (Guid riskId in riskids)
                {
                    try
                    {
                        var riskDetail = await _riskDetailRepository.GetLatestRiskDetailByRiskIdAsync(riskId);

                        if (riskDetail != null)
                        {
                            Guid riskDetailId = riskDetail.Id;
                            string cancellationReasonType = "7";
                            bool isShortRate = false;

                            await CancelPolicy(riskDetailId, effectiveDate, cancellationReasonType, isShortRate, null, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        await _logError(string.Concat("Job: AutoPolicyCancellation,", DateTime.Now, " ", riskId), ex.ToString());
                    }
                }
            }
        }

        public async Task AutoNoticeOfCancellation(DateTime effectiveDate)
        {
            var riskids = await _billingService.GetRisksForNoticeOfCancellation(effectiveDate);
            effectiveDate = (effectiveDate).AddDays(29);
            if (riskids != null && riskids.Count() > 0)
            {
                foreach (Guid riskId in riskids)
                {
                    try
                    {
                        var risk = await _riskRepository.FindAsync(x => x.Id == riskId && x.PendingCancellationDate == null && x.Status == RiskStatus.Active);
                        if (risk != null)
                        {
                            var riskDetail = await _riskDetailRepository.GetLatestRiskDetailByRiskIdAsync(riskId);

                            // Set PendingCancellationDate
                            risk.PendingCancellationDate = effectiveDate;
                            _riskRepository.Update(risk);

                            Guid riskDetailId = riskDetail.Id;
                            string cancellationReasonType = "7";

                            // Generate cancellation notice documents
                            await NoticeCancelPolicy(riskDetailId, effectiveDate, cancellationReasonType);

                            await _riskRepository.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        await _logError(string.Concat("Job: AutoNoticeOfCancellation,", DateTime.Now, " ", riskId), ex.ToString());
                    }
                }
            }
        }

        public async Task AutoRescindNoticeOfCancellation(DateTime effectiveDate)
        {
            var riskids = await _billingService.GetRisksForNoticeOfCancellation(effectiveDate);
            var riskWithPendingCancellation = await _riskRepository.GetAllRiskIdForPendingCancellationAsync();
            if (riskWithPendingCancellation != null && riskWithPendingCancellation.Count() > 0)
            {
                foreach (var risk in riskWithPendingCancellation)
                {
                    try
                    {
                        if (riskids.Contains(risk.Id) == false)
                        {
                            var riskDetail = await _riskDetailRepository.GetLatestRiskDetailByRiskIdAsync(risk.Id);

                            // Clear PendingCancellationDate
                            risk.PendingCancellationDate = null;
                            _riskRepository.Update(risk);

                            // Generate Reinstatement Document
                            await ReinstatePendingCancellationPolicy(riskDetail.Id);

                            await _riskRepository.SaveChangesAsync();
                        }
                    }
                    catch (Exception ex)
                    {
                        await _logError(string.Concat("Job: AutoRescindNoticeOfCancellation,", DateTime.Now, " ", risk.Id), ex.ToString());
                    }
                }
            }
        }

        private async Task _logError(string error, string jsonmsg)
        {
            ErrorLogDTO errLog = new ErrorLogDTO
            {
                UserId = Data.Common.Services.GetCurrentUser(),
                ErrorMessage = error,
                JsonMessage = jsonmsg,
                CreatedDate = DateTime.Now
            };
            await _errorLogger.ErrorAsync(errLog);
        }
    }
}
