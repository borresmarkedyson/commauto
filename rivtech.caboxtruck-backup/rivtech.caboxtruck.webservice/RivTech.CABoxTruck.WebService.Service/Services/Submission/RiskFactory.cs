﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Data.Common;
using RivTech.CABoxTruck.WebService.Data.Context;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Drivers;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission.Risk_Specifics;
using RivTech.CABoxTruck.WebService.Data.Entity.Vehicles;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.DataSets;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission.RiskSpecifics;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;
using RivTech.CABoxTruck.WebService.Service.Services.Common;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.Constants;
using RivTech.CABoxTruck.WebService.Service.Services.DocumentGeneration.PolicyPacket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using static System.Guid;

namespace RivTech.CABoxTruck.WebService.Service.Services.Submission
{
    public class RiskFactory : IRiskFactory
    {
        private readonly AppDbContext _context;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskResponseRepository _riskResponseRepository;
        private readonly IRiskAdditionalInterestRepository _riskAdditionalInterestRepository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IRiskSequenceService _riskSequence;
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly IRiskFormRepository _riskFormRepository;
        private readonly IRiskHistoryRepository _riskHistoryRepository;
        private readonly IRiskSpecificSafetyDeviceRepository _riskSpecificSafetyDeviceRepository;
        private readonly IRiskSpecificDriverInfoRepository _riskSpecificDriverInfoRepository;
        private readonly IRiskSpecificDotInfoRepository _riskSpecificDotInfoRepository;
        private readonly IRiskSpecificDriverHiringCriteriaRepository _riskSpecificDriverHiringCriteriaRepository;
        private readonly IRiskNotesRepository _riskNotesRepository;
        private readonly IMapper _mapper;
        private readonly IFormsService _formsService;
        private readonly IFileUploadDocumentRepository _fileUploadDocumentRepository;
        private readonly IRiskManuscriptsRepository _riskManuscriptsRepository;
        private readonly IBindingRequirementsRepository _bindingRequirementsRepository;
        private readonly IQuoteConditionsRepository _quoteConditionsRepository;
        private readonly IBindingService _bindingService;
        private readonly IClaimsHistoryRepository _claimsHistoryRepository;
        private readonly IRiskSpecificDestinationRepository _riskSpecificDestinationRepository;
        private readonly IEntitySubsidiaryRepository _entitySubsidiaryRepository;
        private readonly IBackgroundCheckOptionRepository _backgroundCheckOptionRepository;
        private readonly IDriverTrainingOptionRepository _driverTrainingOptionRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly ILimitsService _limitsService;
        private readonly IPolicyHistoryRepository _policyHistoryRepository;
        private readonly IDriverRepository _driverRepository;
        private readonly IFormRepository _formRepository;
        private readonly IDriverIncidentsRepository _driverIncidentsRepository;
        private readonly IRiskPolicyContactRepository _riskPolicyContactRepository;
        private readonly string[] ValidSurplusLinesState = { "AL", "AZ", "CA", "CO", "DE", "GA", "IL", "KY", "MD", "NC", "NJ", "NV", "OH", "OR", "PA", "SC", "TN", "TX", "VA", "WA" };
        private List<string> _paths = new List<string>();
        private LimitListDTO _limitList = null;
        private const string STATESERVICEURL = "https://app-genericservices-test.azurewebsites.net/api/State";
        private static string currentCargoCoverageValue = string.Empty; // to temporarily hold value from risk coverage for later use on vehicle level
        private static string currentRefrigerationCoverageValue = string.Empty; // to temporarily hold value from risk coverage for later use on vehicle level

        public RiskFactory(
            AppDbContext context,
            IRiskDetailRepository riskDetailRepository,
            IRiskResponseRepository riskResponseRepository,
            IRiskAdditionalInterestRepository riskAdditionalInterestRepository,
            IEntityAddressRepository entityAddressRepository,
            IRiskRepository riskRepository,
            IRiskSequenceService riskSequence,
            IBrokerInfoRepository brokerInfoRepository,
            IRiskFormRepository riskFormRepository,
            IRiskHistoryRepository riskHistoryRepository,
            IRiskSpecificSafetyDeviceRepository riskSpecificSafetyDeviceRepository,
            IRiskSpecificDriverInfoRepository riskSpecificDriverInfoRepository,
            IRiskSpecificDotInfoRepository riskSpecificDotInfoRepository,
            IRiskSpecificDriverHiringCriteriaRepository riskSpecificDriverHiringCriteriaRepository,
            IRiskNotesRepository riskNotesRepository,
            IFormsService formsService,
            IFileUploadDocumentRepository fileUploadDocumentRepository,
            IRiskManuscriptsRepository riskManuscriptsRepository,
            IBindingRequirementsRepository bindingRequirementsRepository,
            IQuoteConditionsRepository quoteConditionsRepository,
            IBindingService bindingService,
            IClaimsHistoryRepository claimsHistoryRepository,
            IRiskSpecificDestinationRepository riskSpecificDestinationRepository,
            IEntitySubsidiaryRepository entitySubsidiaryRepository,
            IBackgroundCheckOptionRepository backgroundCheckOptionRepository,
            IDriverTrainingOptionRepository driverTrainingOptionRepository,
            IVehicleRepository vehicleRepository,
            ILimitsService limitsService,
            IPolicyHistoryRepository policyHistoryRepository,
            IDriverRepository driverRepository,
            IFormRepository formRepository,
            IDriverIncidentsRepository driverIncidentsRepository,
            IRiskPolicyContactRepository riskPolicyContactRepository,
            IMapper mapper)
        {
            _context = context;
            _riskDetailRepository = riskDetailRepository;
            _riskResponseRepository = riskResponseRepository;
            _riskAdditionalInterestRepository = riskAdditionalInterestRepository;
            _entityAddressRepository = entityAddressRepository;
            _riskRepository = riskRepository;
            _riskSequence = riskSequence;
            _brokerInfoRepository = brokerInfoRepository;
            _riskFormRepository = riskFormRepository;
            _riskHistoryRepository = riskHistoryRepository;
            _riskSpecificSafetyDeviceRepository = riskSpecificSafetyDeviceRepository;
            _riskSpecificDriverInfoRepository = riskSpecificDriverInfoRepository;
            _riskSpecificDotInfoRepository = riskSpecificDotInfoRepository;
            _riskSpecificDriverHiringCriteriaRepository = riskSpecificDriverHiringCriteriaRepository;
            _riskNotesRepository = riskNotesRepository;
            _formsService = formsService;
            _fileUploadDocumentRepository = fileUploadDocumentRepository;
            _riskManuscriptsRepository = riskManuscriptsRepository;
            _bindingRequirementsRepository = bindingRequirementsRepository;
            _quoteConditionsRepository = quoteConditionsRepository;
            _bindingService = bindingService;
            _claimsHistoryRepository = claimsHistoryRepository;
            _riskSpecificDestinationRepository = riskSpecificDestinationRepository;
            _entitySubsidiaryRepository = entitySubsidiaryRepository;
            _backgroundCheckOptionRepository = backgroundCheckOptionRepository;
            _driverTrainingOptionRepository = driverTrainingOptionRepository;
            _vehicleRepository = vehicleRepository;
            _limitsService = limitsService;
            _policyHistoryRepository = policyHistoryRepository;
            _driverRepository = driverRepository;
            _formRepository = formRepository;
            _driverIncidentsRepository = driverIncidentsRepository;
            _riskPolicyContactRepository = riskPolicyContactRepository;
            _mapper = mapper;
        }

        public async Task<RiskDetail> GeneratePolicy(Guid riskDetailId, string policyNumber)
        {
            var newRiskDetailId = NewGuid();

            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;
            RiskCoverage boundRiskCoverage = toCloneRiskDetail.RiskCoverages.Single(rc => rc.OptionNumber == bindOption);

            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == riskDetailId);

            #region Risk
            var risk = await _riskRepository.FindAsync(x => x.Id == toCloneRiskDetail.RiskId);
            risk.PolicyNumber = policyNumber;
            risk.Status = RiskStatus.Active;
            #endregion

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = toCloneRiskDetail.Clone();
            newRiskDetail.Id = newRiskDetailId;
            newRiskDetail.Status = RiskDetailStatus.Active;
            newRiskDetail.CreatedDate = DateTime.Now;
            //newRiskDetail.FirstIssueDate = DateTime.Now;
            #endregion

            #region RiskResponse
            var riskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskResponses != null)
                newRiskDetail.RiskResponses = riskResponses.Select(x => (RiskResponse)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.RiskDetailId = newRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                // Clone only the bound risk coverage.
                newRiskDetail.RiskCoverages = newRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).Select(x => (RiskCoverage)SetId(x, newRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = newRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = newRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = newRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = newRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = newRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        addressIdMapping.Add(x.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            var riskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskHistories != null)
            {
                var riskDetailDrivers = await _context.RiskDetailDriver
                                        .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
                                        .Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();
                var boundCoverages = newRiskDetail.RiskCoverages?.Where(riskCov => riskCov?.OptionNumber == bindOption)?.FirstOrDefault();
                var calculatedVehicles = await _context.VehiclePremiumRatingFactor.Where(vrf => vrf.OptionId == bindOption && vrf.RiskDetailId == riskDetailId).ToListAsync();
                foreach (var x in riskHistories)
                {
                    x.NumberOfDrivers = riskDetailDrivers?.Where(driver => ((driver.Driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count() > 10 ? x.NumberOfDrivers : null;
                    x.NumberOfSpareVehicles = calculatedVehicles?.Count() > 10 ? x.NumberOfSpareVehicles : null;
                    x.ComprehensiveDeductible = boundCoverages?.CompDeductibleId != 76 ? x.ComprehensiveDeductible : null;
                    x.CollisionDeductible = boundCoverages?.CollDeductibleId != 65 ? x.CollisionDeductible : null;
                    x.CargoLimits = boundCoverages?.CargoLimitId != 53 ? x.CargoLimits : null;
                    x.RefrigeratedCargoLimits = boundCoverages?.RefCargoLimitId != 157 ? x.RefrigeratedCargoLimits : null;

                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = newRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                    _riskHistoryRepository.Update(x);
                }
            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = newRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = newRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = newRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.RiskDetailId = newRiskDetailId;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (bindingRequirements != null)
            {
                foreach (var x in bindingRequirements)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 4 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
                    newBindingRequirements.Id = newGuid;
                    newBindingRequirements.RiskDetailId = newRiskDetailId;
                    await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
                }
            }
            #endregion

            #region QuoteConditions
            var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (quoteCond != null)
            {
                foreach (var x in quoteCond)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 5 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
                    newQuoteCond.Id = newGuid;
                    newQuoteCond.RiskDetailId = newRiskDetailId;
                    await _quoteConditionsRepository.AddAsync(newQuoteCond);
                }
            }
            #endregion

            #region Claims
            var claims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = newRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = newRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            #region RiskPolicyContacts
            var riskPolicyContacts = await _riskPolicyContactRepository.GetAllIncludeAsync(riskDetailId);
            if (riskPolicyContacts != null)
            {
                foreach (var item in riskPolicyContacts)
                {
                    var newRiskPolicyContact = SystemExtension.Clone<RiskPolicyContact>(item);
                    newRiskPolicyContact.Id = Guid.NewGuid();
                    newRiskPolicyContact.RiskDetailId = newRiskDetailId;
                    newRiskPolicyContact.Entity.Id = Empty;
                    foreach (var x in newRiskPolicyContact.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }
                    await _riskPolicyContactRepository.AddAsync(newRiskPolicyContact);
                }
            }
            #endregion

            newRiskDetail.DriverInfo = null;

            // Set all documents to IsSystemGenerated = true. Disable editing and deletion
            if (fileUploadDocuments != null)
            {
                foreach (var item in fileUploadDocuments)
                {
                    item.IsSystemGenerated = true; // Set to true to disable editing and deletion after issuance.
                }
                _fileUploadDocumentRepository.UpdateRange(fileUploadDocuments);
            }

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            newRiskDetail.RiskDetailVehicles = toCloneRiskDetail.RiskDetailVehicles;
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles
                // Include only those selected in bind option.
                .Where(rdv => rdv.Vehicle.Options != null && rdv.Vehicle.Options.Split(',').Any(opt => opt.Equals(bindOption.ToString())) && !rdv.Vehicle.IsDeleted)
                .ToList();

            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            // The 'newRiskDetail.RiskDetailDrivers' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailDrivers = newRiskDetail.RiskDetailDrivers.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailDrivers();

            await _riskDetailRepository.AddAsync(newRiskDetail);

            #region Vehicles
            var brokerInfo = risk.BrokerInfo;
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            Guid garagingIdDefault = newRiskDetail.Insured.Entity.EntityAddresses.FirstOrDefault(addr => addr.Address.IsMainGarage).Address.Id;
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                //var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == vehicle.Id.ToString()).ToList(); // get vehicle rating factors
                //foreach (var vrf in vehicleRatingFactors)
                //{
                //    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                //    newVehicleRatingFactor.Id = 0;
                //    newVehicleRatingFactor.VehicleId = vehicle.Id.ToString();
                //    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                //    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                //}
                var rdv = riskDetailVehicle.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                vehicle.RiskDetailVehicles.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption);
                //vehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //vehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //vehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //vehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //vehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //vehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //vehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //vehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                //vehicle.EffectiveDate = brokerInfo.EffectiveDate;
                //vehicle.ExpirationDate = brokerInfo.ExpirationDate;
                //vehicle.ProratedPremium = vehicle.AutoLiabilityPremium + vehicle.PhysicalDamagePremium + vehicle.CargoPremium;
                vehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value, garagingIdDefault);

                vehicle.UpdateLimits(boundRiskCoverage);

                _vehicleRepository.Update(vehicle);                                     // Update vehicle in db context.
            }
            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region Drivers
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailDriver in oldRiskDetailDrivers)
            {
                var driver = _driverRepository.GetById(riskDetailDriver.DriverId);  // Get vehicle.
                if (driver.Options != null && driver.Options.Split(',').Any(opt => opt.Equals(bindOption.ToString())) && !driver.IsDeleted)
                {
                    var rdv = riskDetailDriver.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                    driver.RiskDetailDrivers.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                    _driverRepository.Update(driver);                                     // Update drvr in db context.
                }
            }
            await _driverRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            await _riskDetailRepository.SaveChangesAsync();

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = newRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            #endregion

            _riskRepository.Update(risk);
            await _riskRepository.SaveChangesAsync();

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _riskPolicyContactRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _bindingRequirementsRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            _context.SaveChanges();
            _context.ChangeTracker.Clear();
            return newRiskDetail;
        }

        public async Task<RiskDetail> GeneratePolicyDetailForEndorsement(Guid riskDetailId, DateTime effectiveDate)
        {
            _context.ChangeTracker.Clear();

            var newRiskDetailId = NewGuid();

            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == riskDetailId);

            // Previous RiskDetail
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == toCloneRiskDetail.RiskId && riskDetail.Id != riskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);
            var previousManuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == previousRiskDetail.Id);
            var previousAdditionalInterest = await _riskAdditionalInterestRepository.GetAsync(x => x.RiskDetailId == previousRiskDetail.Id);

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = SystemExtension.Clone<RiskDetail>(toCloneRiskDetail);
            newRiskDetail.Id = newRiskDetailId;
            newRiskDetail.Status = RiskDetailStatus.Active;
            newRiskDetail.CreatedDate = DateTime.Now;
            //newRiskDetail.FirstIssueDate = DateTime.Now;
            #endregion

            #region Policy History
            // update only existing histories to new riskdetail
            var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (policyHistories != null)
            {
                foreach (var policyHistory in policyHistories)
                {
                    policyHistory.RiskDetailId = newRiskDetailId;
                }
                _policyHistoryRepository.UpdateRange(policyHistories);
            }
            #endregion PolicyHistory

            #region RiskResponse
            var riskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskResponses != null)
                newRiskDetail.RiskResponses = riskResponses.Select(x => (RiskResponse)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var isNewlyAdded = previousAdditionalInterest.All(oldObj => oldObj.AddProcessDate != item.AddProcessDate);
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.EffectiveDate = (item.RemoveProcessDate == null) && isNewlyAdded ? effectiveDate : item.EffectiveDate;
                    newAdditionalInterest.ExpirationDate = (item.RemoveProcessDate >= policyHistories.LastOrDefault().DateCreated.Value) ? effectiveDate : item.ExpirationDate;
                    newAdditionalInterest.RiskDetailId = newRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    // update current additional interests to prevent on showing in policy changes after issuance
                    item.EffectiveDate = newAdditionalInterest.EffectiveDate;
                    item.ExpirationDate = newAdditionalInterest.ExpirationDate;
                    _riskAdditionalInterestRepository.Update(item);
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            //#region RiskDetailDriver
            //if (newRiskDetail.RiskDetailDrivers != null)
            //{
            //    var riskDetailDrivers = await _context.RiskDetailDriver
            //        .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
            //        .Where(x => x.RiskDetailId == riskDetailId).ToListAsync();
            //        //.Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();

            //    foreach (var x in riskDetailDrivers)
            //    {
            //        var newRiskDetailDriver = SystemExtension.Clone<RiskDetailDriver>(x);
            //        newRiskDetailDriver.RiskDetailId = newRiskDetailId;
            //        await _context.RiskDetailDriver.AddAsync(newRiskDetailDriver);
            //    }
            //}
            //newRiskDetail.RiskDetailDrivers = null;
            //#endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                newRiskDetail.RiskCoverages.Select(x => (RiskCoverage)SetId(x, newRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = newRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = newRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = newRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = newRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = newRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        addressIdMapping.Add(x.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            var riskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskHistories != null)
            {
                var riskDetailDrivers = await _context.RiskDetailDriver
                                        .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
                                        .Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();
                var boundCoverages = newRiskDetail.RiskCoverages?.Where(riskCov => riskCov?.OptionNumber == bindOption)?.FirstOrDefault();
                var calculatedVehicles = await _context.VehiclePremiumRatingFactor.Where(vrf => vrf.OptionId == bindOption && vrf.RiskDetailId == riskDetailId).ToListAsync();
                foreach (var x in riskHistories)
                {
                    x.NumberOfDrivers = riskDetailDrivers?.Where(driver => ((driver.Driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count() > 10 ? x.NumberOfDrivers : null;
                    x.NumberOfSpareVehicles = calculatedVehicles?.Count() > 10 ? x.NumberOfSpareVehicles : null;
                    x.ComprehensiveDeductible = boundCoverages?.CompDeductibleId != 76 ? x.ComprehensiveDeductible : null;
                    x.CollisionDeductible = boundCoverages?.CollDeductibleId != 65 ? x.CollisionDeductible : null;
                    x.CargoLimits = boundCoverages?.CargoLimitId != 53 ? x.CargoLimits : null;
                    x.RefrigeratedCargoLimits = boundCoverages?.RefCargoLimitId != 157 ? x.RefrigeratedCargoLimits : null;

                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = newRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                    _riskHistoryRepository.Update(x);
                }
            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = newRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = newRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = newRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var isNewlyAdded = previousManuscripts.All(oldObj => oldObj.CreatedDate != x.CreatedDate);
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.EffectiveDate = (x.IsProrate && x.DeletedDate == null) && isNewlyAdded ? effectiveDate : x.EffectiveDate;
                    newManuscripts.ExpirationDate = (x.IsProrate && x.Premium != x.ProratedPremium && x.DeletedDate >= policyHistories.LastOrDefault().DateCreated.Value) ? effectiveDate : x.ExpirationDate;
                    newManuscripts.RiskDetailId = newRiskDetailId;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (bindingRequirements != null)
            {
                foreach (var x in bindingRequirements)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 4 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
                    newBindingRequirements.Id = newGuid;
                    newBindingRequirements.RiskDetailId = newRiskDetailId;
                    await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
                }
            }
            #endregion

            #region QuoteConditions
            var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (quoteCond != null)
            {
                foreach (var x in quoteCond)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 5 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
                    newQuoteCond.Id = newGuid;
                    newQuoteCond.RiskDetailId = newRiskDetailId;
                    await _quoteConditionsRepository.AddAsync(newQuoteCond);
                }
            }
            #endregion

            #region Claims
            var claims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = newRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = newRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            #region FileUploadDocuments
            // Set all documents to IsSystemGenerated = true. Disable editing and deletion
            if (fileUploadDocuments != null)
            {
                foreach (var item in fileUploadDocuments)
                {
                    item.IsSystemGenerated = true; // Set to true to disable editing and deletion after issuance.
                }
                _fileUploadDocumentRepository.UpdateRange(fileUploadDocuments);
            }
            #endregion

            #region RiskPolicyContacts
            var riskPolicyContacts = await _riskPolicyContactRepository.GetAllIncludeAsync(riskDetailId);
            if (riskPolicyContacts != null)
            {
                foreach (var item in riskPolicyContacts)
                {
                    var newRiskPolicyContact = SystemExtension.Clone<RiskPolicyContact>(item);
                    newRiskPolicyContact.Id = Guid.NewGuid();
                    newRiskPolicyContact.RiskDetailId = newRiskDetailId;
                    newRiskPolicyContact.Entity.Id = Empty;
                    foreach (var x in newRiskPolicyContact.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }
                    await _riskPolicyContactRepository.AddAsync(newRiskPolicyContact);
                }
            }
            #endregion

            newRiskDetail.DriverInfo = null;

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            var oldRiskDetailDrivers = newRiskDetail.RiskDetailDrivers.ToList();
            newRiskDetail.ClearRiskDetailDrivers();

            await _riskDetailRepository.AddAsync(newRiskDetail);

            #region Vehicles
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            Guid garagingIdDefault = newRiskDetail.Insured.Entity.EntityAddresses.FirstOrDefault(addr => addr.Address.IsMainGarage).Address.Id;
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                //var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == vehicle.Id.ToString()).ToList(); // get vehicle rating factors
                //foreach (var vrf in vehicleRatingFactors)
                //{
                //    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                //    newVehicleRatingFactor.Id = 0;
                //    newVehicleRatingFactor.VehicleId = vehicle.Id.ToString();
                //    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                //    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                //}
                var rdv = riskDetailVehicle.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                vehicle.RiskDetailVehicles.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption || vrf.OptionId == 0); // 0 id are for policies.
                //vehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //vehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //vehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //vehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //vehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //vehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //vehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //vehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                vehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value, garagingIdDefault);
                _vehicleRepository.Update(vehicle);                                     // Update vehicle in db context.
            }
            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion Vehicles

            #region Drivers
            foreach (var riskDetailDriver in oldRiskDetailDrivers)
            {
                var driver = _driverRepository.GetById(riskDetailDriver.DriverId);  // Get vehicle.
                var rdv = riskDetailDriver.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                driver.RiskDetailDrivers.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                _driverRepository.Update(driver);                                     // Update drvr in db context.
            }
            await _driverRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = newRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            newRiskDetail.DriverInfo = null;
            #endregion

            await _riskDetailRepository.SaveChangesAsync();

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _riskPolicyContactRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _bindingRequirementsRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            await _policyHistoryRepository.SaveChangesAsync();
            _context.SaveChanges();
            _context.ChangeTracker.Clear();
            return newRiskDetail;
        }

        public async Task<RiskDetail> GeneratePolicyDetailForCancellation(Guid currentRiskDetailId, Guid riskId, DateTime effectiveDate)
        {
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var riskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            // save file upload documents data first
            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);

            var insuredAddress = await _riskDetailRepository.GetInsuredAddressAsync(currentRiskDetailId);
            var currentAddresses = insuredAddress.Insured.Entity.EntityAddresses;

            // delete current policy details
            await DeleteCurrentPolicyDetail(currentRiskDetailId, riskDetailId);

            #region Risk
            var risk = await _riskRepository.FindAsync(x => x.Id == toCloneRiskDetail.RiskId);
            risk.Status = RiskStatus.Canceled;
            #endregion

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = SystemExtension.Clone<RiskDetail>(toCloneRiskDetail);
            newRiskDetail.Id = currentRiskDetailId;
            newRiskDetail.Status = RiskDetailStatus.Canceled;
            newRiskDetail.CreatedDate = DateTime.Now;
            //newRiskDetail.FirstIssueDate = DateTime.Now;
            #endregion

            #region RiskResponse
            var riskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskResponses != null)
                newRiskDetail.RiskResponses = riskResponses.Select(x => (RiskResponse)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.RiskDetailId = currentRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    newAdditionalInterest.ExpirationDate = effectiveDate;
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                newRiskDetail.RiskCoverages.Select(x => (RiskCoverage)SetId(x, currentRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = currentRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = currentRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = currentRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = currentRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = currentRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        var currentAddress = currentAddresses.Find(addr => addr.Address.UniqueId == x.Address.UniqueId);
                        addressIdMapping.Add(currentAddress.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            var riskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskHistories != null)
            {
                foreach (var x in riskHistories)
                {
                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = currentRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                }

            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = currentRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = currentRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = currentRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.RiskDetailId = currentRiskDetailId;
                    newManuscripts.ExpirationDate = (x.IsProrate && !x.IsDeleted) ? effectiveDate : x.ExpirationDate;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            // should not include bind requirements and QC
            //var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            //if (bindingRequirements != null)
            //{
            //    foreach (var x in bindingRequirements)
            //    {
            //        var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
            //        newBindingRequirements.Id = Empty;
            //        newBindingRequirements.RiskDetailId = currentRiskDetailId;
            //        await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
            //    }
            //}
            #endregion

            #region QuoteConditions
            //var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            //if (quoteCond != null)
            //{
            //    foreach (var x in quoteCond)
            //    {
            //        var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
            //        newQuoteCond.Id = Empty;
            //        newQuoteCond.RiskDetailId = currentRiskDetailId;
            //        await _quoteConditionsRepository.AddAsync(newQuoteCond);
            //    }
            //}
            #endregion

            #region Claims
            var claims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = currentRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = currentRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            #region RiskPolicyContacts
            // Disabled by Krist Iann Tablan to fix NEWCABT-2089.
            // Reason: Cancellation reuses the current risk detail id. With this enabled only duplicating the record.
            //var riskPolicyContacts = await _riskPolicyContactRepository.GetAllIncludeAsync(riskDetailId);
            //if (riskPolicyContacts != null)
            //{
            //    foreach (var item in riskPolicyContacts)
            //    {
            //        var newRiskPolicyContact = SystemExtension.Clone<RiskPolicyContact>(item);
            //        newRiskPolicyContact.Id = Guid.NewGuid();
            //        newRiskPolicyContact.RiskDetailId = currentRiskDetailId;
            //        newRiskPolicyContact.Entity.Id = Empty;
            //        foreach (var x in newRiskPolicyContact.Entity.EntityAddresses)
            //        {
            //            x.Id = Empty;
            //            x.Address.Id = Empty;
            //        }
            //        await _riskPolicyContactRepository.AddAsync(newRiskPolicyContact);
            //    }
            //}
            #endregion

            newRiskDetail.DriverInfo = null;

            if (fileUploadDocuments != null)
            {
                foreach (var item in fileUploadDocuments)
                {
                    item.RiskDetailId = riskDetailId;
                }
                await _fileUploadDocumentRepository.AddRangeAsync(fileUploadDocuments);
            }

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailDrivers = newRiskDetail.RiskDetailDrivers.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailDrivers();

            await _riskDetailRepository.AddAsync(newRiskDetail);

            #region Vehicles
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                var clonedVehicle = vehicle.CloneForEndorsement();
                clonedVehicle.ClearRiskDetailVehicles();
                await _vehicleRepository.AddAsync(clonedVehicle);                                     // Update vehicle in db context.
                if (clonedVehicle.PreviousVehicleVersionId != null)
                {
                    var previousVersion = _vehicleRepository.GetById(clonedVehicle.PreviousVehicleVersionId.Value); // get previous version
                    clonedVehicle.PreviousVehicleVersionId = (previousVersion is null) ? null : clonedVehicle.PreviousVehicleVersionId; // check previous version if deleted already
                }
                var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == clonedVehicle.Id.ToString()).ToList(); // get vehicle rating factors
                foreach (var vrf in vehicleRatingFactors)
                {
                    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                    newVehicleRatingFactor.Id = 0;
                    newVehicleRatingFactor.VehicleId = clonedVehicle.Id.ToString();
                    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                }
                var rdv = new RiskDetailVehicle(newRiskDetail, clonedVehicle);           // Create new RiskDetailVehicle with updated RiskDetail.
                clonedVehicle.RiskDetailVehicles.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption || vrf.OptionId == 0);
                //clonedVehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //clonedVehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //clonedVehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //clonedVehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //clonedVehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //clonedVehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //clonedVehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //clonedVehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                clonedVehicle.ExpirationDate = effectiveDate;                                 // set expiration to cancellation date
                clonedVehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value);
            }
            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region Drivers
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailDriver in oldRiskDetailDrivers)
            {
                var driver = _driverRepository.GetById(riskDetailDriver.DriverId);  // Get vehicle.

                if (driver.IsDeleted)
                {
                    var previousVersions = await _driverRepository.GetPreviousRiskDetailDrivers(newRiskDetail.RiskId); // get previous versions
                    var isDriverAPreviousVersion = previousVersions.Any(x => x.Id == driver.Id);
                    if (isDriverAPreviousVersion == false)
                    {
                        driver.IsActive = null; // re-enable record
                        driver.DeletedDate = null; // re-enable record
                    }
                }

                bool hasBindOptionSelected = driver.Options != null && driver.Options.Split(',').Any(opt => opt.Equals(bindOption.ToString())) && !driver.IsDeleted;
                if (!hasBindOptionSelected && toCloneRiskDetail.Status == RiskDetailStatus.Quoted) continue; // any driver not bound from submision should not be carried over to policy

                var rdv = riskDetailDriver.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                driver.RiskDetailDrivers.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                _driverRepository.Update(driver);                                     // Update drvr in db context.
            }
            await _driverRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = currentRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            #endregion

            _riskRepository.Update(risk);
            await _riskRepository.SaveChangesAsync();

            await _riskDetailRepository.SaveChangesAsync();

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _claimsHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _riskPolicyContactRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            _context.SaveChanges();
            return newRiskDetail;
        }

        public async Task<RiskDetail> GeneratePolicyDetailForReinstatement(Guid currentRiskDetailId, Guid riskId)
        {
            _context.ChangeTracker.Clear();

            var newRiskDetailId = NewGuid();

            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var riskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;

            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == riskDetailId);

            var insuredAddress = await _riskDetailRepository.GetInsuredAddressAsync(currentRiskDetailId);
            var currentAddresses = insuredAddress.Insured.Entity.EntityAddresses;

            #region Risk
            var risk = await _riskRepository.FindAsync(x => x.Id == toCloneRiskDetail.RiskId);
            risk.Status = RiskStatus.Active;
            #endregion

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = SystemExtension.Clone<RiskDetail>(toCloneRiskDetail);
            newRiskDetail.Id = newRiskDetailId;
            newRiskDetail.Status = RiskDetailStatus.Active;
            newRiskDetail.CreatedDate = DateTime.Now;
            //newRiskDetail.FirstIssueDate = DateTime.Now;
            #endregion

            #region Policy History
            // update only existing histories to new riskdetail
            var policyHistories = await _policyHistoryRepository.GetAsync(x => x.RiskId == riskId.ToString()); // should use risk id
            if (policyHistories != null)
            {
                foreach (var policyHistory in policyHistories)
                {
                    policyHistory.RiskDetailId = newRiskDetailId;
                }
                _policyHistoryRepository.UpdateRange(policyHistories);
            }
            #endregion PolicyHistory

            #region RiskResponse
            var riskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskResponses != null)
                newRiskDetail.RiskResponses = riskResponses.Select(x => (RiskResponse)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.RiskDetailId = newRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            #region RiskDetailDriver
            if (newRiskDetail.RiskDetailDrivers != null)
            {
                var riskDetailDrivers = await _context.RiskDetailDriver
                    .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
                    .Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();

                foreach (var x in riskDetailDrivers)
                {
                    var newRiskDetailDriver = SystemExtension.Clone<RiskDetailDriver>(x);
                    newRiskDetailDriver.RiskDetailId = newRiskDetailId;
                    await _context.RiskDetailDriver.AddAsync(newRiskDetailDriver);
                }
            }
            newRiskDetail.RiskDetailDrivers = null;
            #endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                newRiskDetail.RiskCoverages.Select(x => (RiskCoverage)SetId(x, newRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = newRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = newRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = newRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = newRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            newRiskDetail.DriverInfo = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = newRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = newRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        var currentAddress = currentAddresses.Find(addr => addr.Address.UniqueId == x.Address.UniqueId);
                        addressIdMapping.Add(currentAddress.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            var riskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskHistories != null)
            {
                var riskDetailDrivers = await _context.RiskDetailDriver
                                        .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
                                        .Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();
                var boundCoverages = newRiskDetail.RiskCoverages?.Where(riskCov => riskCov?.OptionNumber == bindOption)?.FirstOrDefault();
                var calculatedVehicles = await _context.VehiclePremiumRatingFactor.Where(vrf => (vrf.OptionId == bindOption || vrf.OptionId == 0) && vrf.RiskDetailId == riskDetailId).ToListAsync();
                foreach (var x in riskHistories)
                {
                    x.NumberOfDrivers = riskDetailDrivers?.Where(driver => ((driver.Driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count() > 10 ? x.NumberOfDrivers : null;
                    x.NumberOfSpareVehicles = calculatedVehicles?.Count() > 10 ? x.NumberOfSpareVehicles : null;
                    x.ComprehensiveDeductible = boundCoverages?.CompDeductibleId != 76 ? x.ComprehensiveDeductible : null;
                    x.CollisionDeductible = boundCoverages?.CollDeductibleId != 65 ? x.CollisionDeductible : null;
                    x.CargoLimits = boundCoverages?.CargoLimitId != 53 ? x.CargoLimits : null;
                    x.RefrigeratedCargoLimits = boundCoverages?.RefCargoLimitId != 157 ? x.RefrigeratedCargoLimits : null;

                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = newRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                    _riskHistoryRepository.Update(x);
                }
            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = newRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = newRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = newRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.EffectiveDate = x.EffectiveDate;
                    newManuscripts.ExpirationDate = x.ExpirationDate;
                    newManuscripts.RiskDetailId = newRiskDetailId;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (bindingRequirements != null)
            {
                foreach (var x in bindingRequirements)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 4 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
                    newBindingRequirements.Id = newGuid;
                    newBindingRequirements.RiskDetailId = newRiskDetailId;
                    await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
                }
            }
            #endregion

            #region QuoteConditions
            var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (quoteCond != null)
            {
                foreach (var x in quoteCond)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = fileUploadDocuments.Where(relatedDoc => relatedDoc.FileCategoryId == 5 && relatedDoc.tableRefId == x.Id).FirstOrDefault();
                    if (relatedDocuments != null)
                    {
                        relatedDocuments.tableRefId = newGuid;
                        _fileUploadDocumentRepository.Update(relatedDocuments);
                    }

                    var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
                    newQuoteCond.Id = newGuid;
                    newQuoteCond.RiskDetailId = newRiskDetailId;
                    await _quoteConditionsRepository.AddAsync(newQuoteCond);
                }
            }
            #endregion

            #region Claims
            var claims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = newRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = newRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            #region RiskPolicyContacts
            var riskPolicyContacts = await _riskPolicyContactRepository.GetAllIncludeAsync(riskDetailId);
            if (riskPolicyContacts != null)
            {
                foreach (var item in riskPolicyContacts)
                {
                    var newRiskPolicyContact = SystemExtension.Clone<RiskPolicyContact>(item);
                    newRiskPolicyContact.Id = Guid.NewGuid();
                    newRiskPolicyContact.RiskDetailId = newRiskDetailId;
                    newRiskPolicyContact.Entity.Id = Empty;
                    foreach (var x in newRiskPolicyContact.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }
                    await _riskPolicyContactRepository.AddAsync(newRiskPolicyContact);
                }
            }
            #endregion

            // Set all documents to IsSystemGenerated = true. Disable editing and deletion
            if (fileUploadDocuments != null)
            {
                foreach (var item in fileUploadDocuments)
                {
                    item.IsSystemGenerated = true; // Set to true to disable editing and deletion after issuance.
                }
                _fileUploadDocumentRepository.UpdateRange(fileUploadDocuments);
            }

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            await _riskDetailRepository.AddAsync(newRiskDetail);

            #region Vehicles
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == vehicle.Id.ToString()).ToList(); // get vehicle rating factors
                foreach (var vrf in vehicleRatingFactors)
                {
                    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                    newVehicleRatingFactor.Id = 0;
                    newVehicleRatingFactor.VehicleId = vehicle.Id.ToString();
                    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                }
                var rdv = riskDetailVehicle.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                vehicle.RiskDetailVehicles.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption || vrf.OptionId == 0); // 0 id are for policies.
                //vehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //vehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //vehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //vehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //vehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //vehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //vehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //vehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                vehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value);
                _vehicleRepository.Update(vehicle);                                     // Update vehicle in db context.
            }
            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion Vehicles

            _riskRepository.Update(risk);
            await _riskRepository.SaveChangesAsync();

            await _riskDetailRepository.SaveChangesAsync();

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _riskPolicyContactRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _bindingRequirementsRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            await _policyHistoryRepository.SaveChangesAsync();
            _context.SaveChanges();
            return newRiskDetail;
        }

        public async Task<RiskDetail> GenerateSubmissionDetailFromPolicy(Guid currentRiskDetailId, Guid riskId, string newSubmissionNumber, string newQuoteNumber)
        {
            _context.ChangeTracker.Clear();

            var newRiskDetailId = Guid.NewGuid();
            var newRiskId = Guid.NewGuid();

            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status == RiskDetailStatus.Quoted);
            var riskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;
            RiskCoverage boundRiskCoverage = toCloneRiskDetail.RiskCoverages.Single(rc => rc.OptionNumber == bindOption);

            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == riskDetailId);

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = toCloneRiskDetail.Clone();
            newRiskDetail.Id = newRiskDetailId;
            newRiskDetail.RiskId = newRiskId;
            newRiskDetail.SubmissionNumber = newSubmissionNumber;
            newRiskDetail.Status = RiskDetailStatus.Quoted;
            newRiskDetail.CreatedDate = DateTime.Now;
            #endregion

            #region Risk
            var toCloneRisk = await _riskRepository.FindAsync(x => x.Id == toCloneRiskDetail.RiskId, i => i.BrokerInfo);
            _riskRepository.Detach(toCloneRisk);

            var newRisk = new Data.Entity.Submission.Risk();
            newRisk = toCloneRisk.Clone();
            newRisk.Id = newRiskId;
            newRisk.Status = RiskStatus.Pending;
            newRisk.SubmissionNumber = newSubmissionNumber;
            newRisk.QuoteNumber = newQuoteNumber;
            newRisk.PolicyNumber = null;
            newRisk.RiskDetails = new List<RiskDetail>();
            newRisk.Binding.Id = Guid.Empty;
            newRisk.BrokerInfo.Id = Guid.Empty;
            newRisk.RiskDetails.Add(newRiskDetail);
            #endregion

            #region RiskResponse
            var riskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskResponses != null)
                newRiskDetail.RiskResponses = riskResponses.Select(x => (RiskResponse)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.RiskDetailId = newRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                // Clone only the bound risk coverage.
                newRiskDetail.RiskCoverages.Select(x => (RiskCoverage)SetId(x, newRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = newRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, newRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = newRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = newRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = newRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = newRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        addressIdMapping.Add(x.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            var riskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskHistories != null)
            {
                var riskDetailDrivers = await _context.RiskDetailDriver
                                        .Include(x => x.Driver).ThenInclude(x => x.DriverIncidents)
                                        .Where(x => x.RiskDetailId == riskDetailId && !x.Driver.DeletedDate.HasValue).ToListAsync();
                var boundCoverages = newRiskDetail.RiskCoverages?.Where(riskCov => riskCov?.OptionNumber == bindOption)?.FirstOrDefault();
                var calculatedVehicles = await _context.VehiclePremiumRatingFactor.Where(vrf => vrf.OptionId == bindOption && vrf.RiskDetailId == riskDetailId).ToListAsync();
                foreach (var x in riskHistories)
                {
                    x.NumberOfDrivers = riskDetailDrivers?.Where(driver => ((driver.Driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count() > 10 ? x.NumberOfDrivers : null;
                    x.NumberOfSpareVehicles = calculatedVehicles?.Count() > 10 ? x.NumberOfSpareVehicles : null;
                    x.ComprehensiveDeductible = boundCoverages?.CompDeductibleId != 76 ? x.ComprehensiveDeductible : null;
                    x.CollisionDeductible = boundCoverages?.CollDeductibleId != 65 ? x.CollisionDeductible : null;
                    x.CargoLimits = boundCoverages?.CargoLimitId != 53 ? x.CargoLimits : null;
                    x.RefrigeratedCargoLimits = boundCoverages?.RefCargoLimitId != 157 ? x.RefrigeratedCargoLimits : null;

                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = newRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                    _riskHistoryRepository.Update(x);
                }
            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = newRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = newRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = newRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.RiskDetailId = newRiskDetailId;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            if (bindingRequirements != null)
            {
                foreach (var x in bindingRequirements)
                {
                    var newGuid = Guid.NewGuid();

                    var relatedDocuments = await _fileUploadDocumentRepository.GetAsync(relatedDoc => relatedDoc.FileCategoryId == 4 && relatedDoc.tableRefId == x.Id);
                    if (relatedDocuments != null)
                    {
                        foreach (var fileDocument in relatedDocuments)
                        {
                            fileDocument.tableRefId = newGuid;
                            _fileUploadDocumentRepository.Update(fileDocument);
                        }
                    }

                    var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
                    newBindingRequirements.Id = newGuid;
                    newBindingRequirements.RiskDetailId = newRiskDetailId;
                    await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
                }
            }
            #endregion

            #region QuoteConditions
            var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            if (quoteCond != null)
            {
                foreach (var x in quoteCond)
                {
                    var newGuid = Guid.NewGuid();
                    var relatedDocuments = await _fileUploadDocumentRepository.GetAsync(relatedDoc => relatedDoc.FileCategoryId == 5 && relatedDoc.tableRefId == x.Id);
                    if (relatedDocuments != null)
                    {
                        foreach (var fileDocument in relatedDocuments)
                        {
                            fileDocument.tableRefId = newGuid;
                            _fileUploadDocumentRepository.Update(fileDocument);
                        }
                    }

                    var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
                    newQuoteCond.Id = newGuid;
                    newQuoteCond.RiskDetailId = newRiskDetailId;
                    await _quoteConditionsRepository.AddAsync(newQuoteCond);
                }
            }
            #endregion

            #region Claims
            var claims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = newRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = newRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            #region RiskPolicyContacts
            var riskPolicyContacts = await _riskPolicyContactRepository.GetAllIncludeAsync(riskDetailId);
            if (riskPolicyContacts != null)
            {
                foreach (var item in riskPolicyContacts)
                {
                    var newRiskPolicyContact = SystemExtension.Clone<RiskPolicyContact>(item);
                    newRiskPolicyContact.Id = Guid.NewGuid();
                    newRiskPolicyContact.RiskDetailId = newRiskDetailId;
                    newRiskPolicyContact.Entity.Id = Empty;
                    foreach (var x in newRiskPolicyContact.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }
                    await _riskPolicyContactRepository.AddAsync(newRiskPolicyContact);
                }
            }
            #endregion

            newRiskDetail.DriverInfo = null;

            var userFileUploadDocuments = fileUploadDocuments.Where(x => x.RiskDetailId == riskDetailId
                                            && x.IsUserUploaded.HasValue && x.IsUserUploaded.Value
                                            && x.FileCategoryId != 4 && x.FileCategoryId != 5).ToList();

            if (userFileUploadDocuments != null)
            {
                foreach (var item in userFileUploadDocuments)
                {
                    var newFileUploadDocument = SystemExtension.Clone<FileUploadDocument>(item);
                    newFileUploadDocument.Id = Empty;
                    newFileUploadDocument.tableRefId = newRiskDetailId;
                    newFileUploadDocument.RiskDetailId = newRiskDetailId;
                    await _fileUploadDocumentRepository.AddAsync(newFileUploadDocument);
                }
            }

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            newRiskDetail.RiskDetailVehicles = toCloneRiskDetail.RiskDetailVehicles;
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles
                // Include only those selected in bind option.
                .Where(rdv => rdv.Vehicle.Options != null && rdv.Vehicle.Options.Split(',').Any(opt => opt.Equals(bindOption.ToString())) && !rdv.Vehicle.IsDeleted)
                .ToList();

            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            // The 'newRiskDetail.RiskDetailDrivers' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailDrivers = newRiskDetail.RiskDetailDrivers.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailDrivers();

            await _riskRepository.AddAsync(newRisk);
            await _riskRepository.SaveChangesAsync();

            #region Vehicles
            var brokerInfo = newRisk.BrokerInfo;
            Guid garagingIdDefault = newRiskDetail.Insured.Entity.EntityAddresses.FirstOrDefault(addr => addr.Address.IsMainGarage).Address.Id;

            // Copy each vehicle
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                var clonedVehicle = vehicle.Clone();
                clonedVehicle.ClearRiskDetailVehicles();
                //clonedVehicle.VehicleProratedPremium = vehicle.VehicleProratedPremium.Clone();
                clonedVehicle.Id = Guid.Empty;
                clonedVehicle.MainId = Guid.NewGuid();
                clonedVehicle.PreviousVehicleVersionId = null;
                await _vehicleRepository.AddAsync(clonedVehicle);

                var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == vehicle.Id.ToString()).ToList(); // get vehicle rating factors
                foreach (var vrf in vehicleRatingFactors)
                {
                    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                    newVehicleRatingFactor.Id = 0;
                    newVehicleRatingFactor.VehicleId = clonedVehicle.Id.ToString();
                    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                }
                clonedVehicle.LinkRiskDetail(newRiskDetail);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption);
                //vehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //vehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //vehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //vehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //vehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //vehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //vehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //vehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                //vehicle.EffectiveDate = brokerInfo.EffectiveDate;
                //vehicle.ExpirationDate = brokerInfo.ExpirationDate;
                //vehicle.ProratedPremium = vehicle.AutoLiabilityPremium + vehicle.PhysicalDamagePremium + vehicle.CargoPremium;

                clonedVehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value, garagingIdDefault);

                clonedVehicle.UpdateLimits(boundRiskCoverage);

                //_vehicleRepository.Update(vehicle);                                     // Update vehicle in db context.
            }

            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region Drivers
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailDriver in oldRiskDetailDrivers)
            {
                var driver = _driverRepository.GetById(riskDetailDriver.DriverId);  // Get vehicle.
                var rdv = riskDetailDriver.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                driver.RiskDetailDrivers.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                _driverRepository.Update(driver);                                     // Update drvr in db context.
            }
            await _driverRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = newRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            #endregion

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _riskPolicyContactRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _bindingRequirementsRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            _context.SaveChanges();

            return newRiskDetail;
        }
        
        private dynamic SetId(dynamic entity, Guid riskDetailId)
        {
            entity.Id = Empty;
            entity.RiskDetailId = riskDetailId;
            return entity;
        }

        public async Task<List<RiskFormDTO>> GetRiskForms(Guid riskDetailId)
        {
            var riskDetail = await _riskDetailRepository.GetInsuredAddressAsync(riskDetailId);
            var state = riskDetail.Insured.Entity.EntityAddresses.Find(x => x.AddressTypeId == null).Address.State;
            var forms = await _formsService.GetSelectedByRiskIdAsync(riskDetailId);
            if (!ValidSurplusLinesState.Any(x => x == state))
            {
                var surplusLines = forms.Where(x => x.FormId == "SurplusLinesNotice").FirstOrDefault();
                if (surplusLines != null)
                    forms.Remove(surplusLines);
            }
            return forms;
        }

        public async Task<List<RiskFormDTO>> GetRiskFormsWithManuscripts(Guid riskDetailId)
        {
            var forms = await GetRiskForms(riskDetailId);
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                var ofacSortOrder = forms.Where(x => x.FormId == "OFACNotice").FirstOrDefault().Form.SortOrder;
                foreach (var manu in manuscripts)
                {
                    forms.Add(new RiskFormDTO()
                    {
                        Id = manu.Id,
                        IsSelected = manu.IsSelected,
                        RiskDetailId = manu.RiskDetailId,
                        Form = new FormDTO()
                        {
                            FormNumber = "RPCAMANU",
                            Id = "ManuscriptEndorsement",
                            Name = manu.Title,
                            SortOrder = ofacSortOrder - 1
                        }
                    });
                }
            }

            // Excldue forms
            forms = forms.Where(x => 
                (x.IsSelected.Value || x.Form.FormNumber == "RPCAMANU")
                && x.Form.FormNumber != "RPCA030").ToList();

            return forms;
        }

        public async Task<List<RiskFormDTO>> GetRiskFormsWithConditionals(RiskDTO riskDetail, bool isEndorsement = false, List<RiskFormDTO> riskForms = null)
        {
            if (riskForms != null && riskForms.Count == 1 && riskForms.First().Form.IsSupplementalDocument) return riskForms;

            var conditionalFormNumbers = new[] { "RPCA017", "RPCA018", "RPCA023", "RPCA028", "RPCA032" };
            var conditionalForms = _mapper.Map<List<FormDTO>>(await _formRepository.GetAsync(f => conditionalFormNumbers.Contains(f.FormNumber)));

            riskForms ??= await GetRiskFormsWithManuscripts(riskDetail.Id.GetValueOrDefault());

            var lossPayeeOther = riskForms.FirstOrDefault(rf => rf.FormId == "LossPayeeEndorsement")?.Other;

            riskForms.RemoveAll(rf => conditionalFormNumbers.Contains(rf.Form.FormNumber));
            if (isEndorsement) riskForms.RemoveAll(rf => rf.Form.FormNumber == "RPCA002");

            var pncs = riskDetail.AdditionalInterest.Where(ai => ai.IsPrimaryNonContributory);
            if (!isEndorsement && riskDetail.BlanketCoverage.IsPrimaryNonContributory.GetValueOrDefault()) pncs = pncs.Take(1);
            riskForms.AddRange(pncs.Select(pnc => new RiskFormDTO { Id = pnc.Id, IsSelected = true, Form = conditionalForms.FirstOrDefault(cf => cf.FormNumber == "RPCA017") }));

            var waivers = riskDetail.AdditionalInterest.Where(ai => ai.IsWaiverOfSubrogation);
            if (!isEndorsement && riskDetail.BlanketCoverage.IsWaiverOfSubrogation.GetValueOrDefault()) waivers = waivers.Take(1);
            riskForms.AddRange(waivers.Select(waiver => new RiskFormDTO { Id = waiver.Id, IsSelected = true, Form = conditionalForms.FirstOrDefault(cf => cf.FormNumber == "RPCA018") }));

            var additionalInsureds = riskDetail.AdditionalInterest.Where(ai => ai.AdditionalInterestTypeId == 1);
            if (!isEndorsement && riskDetail.BlanketCoverage.IsAdditionalInsured.GetValueOrDefault()) additionalInsureds = additionalInsureds.Take(1);
            riskForms.AddRange(additionalInsureds.Select(ai => new RiskFormDTO { Id = ai.Id, IsSelected = true, Form = conditionalForms.FirstOrDefault(cf => cf.FormNumber == "RPCA023") }));

            var designatedInsureds = riskDetail.AdditionalInterest.Where(ai => ai.AdditionalInterestTypeId == 12);
            riskForms.AddRange(designatedInsureds.Select(di => new RiskFormDTO { Id = di.Id, IsSelected = true, Form = conditionalForms.FirstOrDefault(cf => cf.FormNumber == "RPCA028") }));

            var lossPayees = riskDetail.AdditionalInterest.Where(ai => ai.AdditionalInterestTypeId == 2);
            riskForms.AddRange(lossPayees.Select(di => new RiskFormDTO { Id = di.Id, IsSelected = true, Other = lossPayeeOther, Form = conditionalForms.FirstOrDefault(cf => cf.FormNumber == "RPCA032") }));

            return riskForms;
        }

        public async Task<RiskDetail> GetPreviousPolicyDetail(Guid currentRiskDetailId, Guid riskId)
        {
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var riskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();
            var toCloneRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);
            _riskDetailRepository.Detach(toCloneRiskDetail);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(toCloneRiskDetail.RiskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;
            RiskCoverage boundRiskCoverage = toCloneRiskDetail.RiskCoverages.Single(rc => rc.OptionNumber == bindOption);

            // save file upload documents data first
            var fileUploadDocuments = await _fileUploadDocumentRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            // save current addresses
            var insuredAddress = await _riskDetailRepository.GetInsuredAddressAsync(currentRiskDetailId);
            var currentAddresses = insuredAddress.Insured.Entity.EntityAddresses;

            var backupRiskResponse = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            var backupRiskHistories = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            var backupClaims = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);

            // delete current policy details
            await DeleteCurrentPolicyDetail(currentRiskDetailId, riskDetailId);

            #region RiskDetail
            var newRiskDetail = new RiskDetail();
            newRiskDetail = SystemExtension.Clone<RiskDetail>(toCloneRiskDetail);
            newRiskDetail.Id = currentRiskDetailId;
            newRiskDetail.Status = RiskDetailStatus.Active;
            newRiskDetail.CreatedDate = DateTime.Now;
            //newRiskDetail.FirstIssueDate = DateTime.Now;
            #endregion

            #region RiskResponse
            if (backupRiskResponse != null)
                newRiskDetail.RiskResponses = backupRiskResponse.Select(x => (RiskResponse)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region RiskAdditionalInterest
            var riskAdditionalInterest = await _riskAdditionalInterestRepository.GetAllIncludeAsync(riskDetailId);
            if (riskAdditionalInterest != null)
            {
                newRiskDetail.AdditionalInterests = new List<RiskAdditionalInterest>();
                foreach (var item in riskAdditionalInterest)
                {
                    var newAdditionalInterest = SystemExtension.Clone<RiskAdditionalInterest>(item);
                    newAdditionalInterest.Id = Guid.NewGuid();
                    newAdditionalInterest.RiskDetailId = currentRiskDetailId;
                    newAdditionalInterest.Entity.Id = Empty;
                    foreach (var x in newAdditionalInterest.Entity.EntityAddresses)
                    {
                        x.Id = Empty;
                        x.Address.Id = Empty;
                    }

                    newRiskDetail.AdditionalInterests.Add(newAdditionalInterest);
                }
            }
            #endregion

            #region RiskFilings
            if (newRiskDetail.RiskFilings != null)
                newRiskDetail.RiskFilings.Select(x => (RiskFiling)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region RiskCoverages
            if (newRiskDetail.RiskCoverages != null)
            {
                newRiskDetail.RiskCoverages.Select(x => (RiskCoverage)SetId(x, currentRiskDetailId)).ToList();
                foreach (var item in newRiskDetail.RiskCoverages)
                {
                    if (item.RiskCoveragePremium != null)
                    {
                        item.RiskCoveragePremium.Id = Empty;
                    }
                }
            }
            #endregion

            #region RadiusOfOperation
            if (newRiskDetail.RadiusOfOperations != null)
                newRiskDetail.RadiusOfOperations.Id = currentRiskDetailId;
            #endregion

            #region CommoditiesHauled
            if (newRiskDetail.CommoditiesHauled != null)
                newRiskDetail.CommoditiesHauled.Select(x => (RiskSpecificCommoditiesHauled)SetId(x, currentRiskDetailId)).ToList();
            #endregion

            #region DriverHeader
            if (newRiskDetail.DriverHeader != null)
            {
                newRiskDetail.DriverHeader.Id = Empty;
                newRiskDetail.DriverHeader.RiskDetailId = currentRiskDetailId;
            }
            #endregion

            #region DriverHiringCriteria
            var driverHiringCriteria = await _riskSpecificDriverHiringCriteriaRepository.GetWithIncludesNoTracking(riskDetailId);
            if (driverHiringCriteria != null)
            {
                var newDriverHiringCriteria = new RiskSpecificDriverHiringCriteria();
                newDriverHiringCriteria.Id = currentRiskDetailId;
                newDriverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years = driverHiringCriteria.AreDriversDrivingSimilarVehicleCommerciallyForMoreThan2Years;
                newDriverHiringCriteria.DoDriversHave5YearsDrivingExperience = driverHiringCriteria.DoDriversHave5YearsDrivingExperience;
                newDriverHiringCriteria.IsAgreedToReportAllDriversToRivington = driverHiringCriteria.IsAgreedToReportAllDriversToRivington;
                newDriverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto = driverHiringCriteria.AreFamilyMembersUnder21PrimaryDriversOfCompanyAuto;
                newDriverHiringCriteria.AreDriversProperlyLicensedDotCompliant = driverHiringCriteria.AreDriversProperlyLicensedDotCompliant;
                newDriverHiringCriteria.IsDisciplinaryPlanDocumented = driverHiringCriteria.IsDisciplinaryPlanDocumented;
                newDriverHiringCriteria.IsThereDriverIncentiveProgram = driverHiringCriteria.IsThereDriverIncentiveProgram;

                newDriverHiringCriteria.BackGroundCheckIncludes = new List<Data.Entity.DataSets.BackgroundCheckOption>();
                if (driverHiringCriteria.BackGroundCheckIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.BackGroundCheckIncludes)
                    {
                        var optionFromDb = await _backgroundCheckOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.BackGroundCheckIncludes.Add(optionFromDb);
                        }
                    }
                }

                newDriverHiringCriteria.DriverTrainingIncludes = new List<Data.Entity.DataSets.DriverTrainingOption>();
                if (driverHiringCriteria.DriverTrainingIncludes.Count > 0)
                {
                    foreach (var option in driverHiringCriteria.DriverTrainingIncludes)
                    {
                        var optionFromDb = await _driverTrainingOptionRepository.FindAsync(option.Id);
                        if (optionFromDb != null)
                        {
                            newDriverHiringCriteria.DriverTrainingIncludes.Add(optionFromDb);
                        }
                    }
                }

                await _riskSpecificDriverHiringCriteriaRepository.AddAsync(newDriverHiringCriteria);
            }

            newRiskDetail.DriverHiringCriteria = null;
            #endregion

            #region DotInfo
            var newDotInfo = await _riskSpecificDotInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDotInfo != null)
            {
                newDotInfo.Id = currentRiskDetailId;
                await _riskSpecificDotInfoRepository.AddAsync(newDotInfo);
            }
            newRiskDetail.DotInfo = null;
            #endregion

            #region UnderwritingQuestions
            if (newRiskDetail.UnderwritingQuestions != null)
                newRiskDetail.UnderwritingQuestions.Id = currentRiskDetailId;
            #endregion

            #region Insured Subsidiary
            var addressIdMapping = new Dictionary<Guid, Guid>();
            if (newRiskDetail.Insured != null)
            {
                var newEntityId = Guid.NewGuid();
                var subsidiaries = await _entitySubsidiaryRepository.GetAsync(x => x.EntityId == newRiskDetail.Insured.EntityId);
                foreach (var sub in subsidiaries)
                {
                    var newSubsidiaries = SystemExtension.Clone<EntitySubsidiary>(sub);
                    newSubsidiaries.Id = Empty;
                    newSubsidiaries.EntityId = newEntityId;
                    await _entitySubsidiaryRepository.AddAsync(newSubsidiaries);
                }

                newRiskDetail.Insured.Id = Empty;
                newRiskDetail.Insured.Entity.Id = newEntityId;
                foreach (var x in newRiskDetail.Insured.Entity.EntityAddresses)
                {
                    var newAddressId = Guid.NewGuid();
                    if (x.AddressTypeId == (short)AddressTypesEnum.Garaging)
                    {
                        var currentAddress = currentAddresses.Find(addr => addr.Address.UniqueId == x.Address.UniqueId);
                        addressIdMapping.Add(currentAddress.Address.Id, newAddressId);
                    }
                    x.Id = Empty;
                    x.Address.Id = newAddressId;
                }
            }
            #endregion

            #region RiskHistory
            if (backupRiskHistories != null)
            {
                foreach (var x in backupRiskHistories)
                {
                    var newRiskHistory = SystemExtension.Clone<RiskHistory>(x);
                    newRiskHistory.Id = Empty;
                    newRiskHistory.RiskDetailId = currentRiskDetailId;
                    await _riskHistoryRepository.AddAsync(newRiskHistory);
                }
            }
            #endregion

            #region RiskSpecificSafetyDevice
            var riskSpecificSafetyDevice = await _riskSpecificSafetyDeviceRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskSpecificSafetyDevice != null)
            {
                foreach (var x in riskSpecificSafetyDevice)
                {
                    x.Id = 0;
                    x.RiskDetailId = currentRiskDetailId;
                    await _riskSpecificSafetyDeviceRepository.AddAsync(x);
                }
            }
            #endregion

            #region RiskNotes
            var riskNotes = await _riskNotesRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskNotes != null)
            {
                foreach (var riskNote in riskNotes)
                {
                    riskNote.Id = Empty;
                    riskNote.RiskDetailId = currentRiskDetailId;
                    await _riskNotesRepository.AddAsync(riskNote);
                }
            }
            #endregion

            #region RiskForms
            var riskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (riskForms != null)
            {
                foreach (var x in riskForms)
                {
                    var newRiskForms = SystemExtension.Clone<RiskForm>(x);
                    newRiskForms.Id = Empty;
                    newRiskForms.RiskDetailId = currentRiskDetailId;
                    await _riskFormRepository.AddAsync(newRiskForms);
                }
            }
            #endregion

            #region Manuscripts
            var manuscripts = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (manuscripts != null)
            {
                foreach (var x in manuscripts)
                {
                    var newManuscripts = SystemExtension.Clone<RiskManuscripts>(x);
                    newManuscripts.Id = Empty;
                    newManuscripts.RiskDetailId = currentRiskDetailId;
                    await _riskManuscriptsRepository.AddAsync(newManuscripts);
                }
            }
            #endregion

            #region BindingRequirements
            // should not include bind requirements and QC
            //var bindingRequirements = await _bindingRequirementsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            //if (bindingRequirements != null)
            //{
            //    foreach (var x in bindingRequirements)
            //    {
            //        var newBindingRequirements = SystemExtension.Clone<BindingRequirements>(x);
            //        newBindingRequirements.Id = Empty;
            //        newBindingRequirements.RiskDetailId = currentRiskDetailId;
            //        await _bindingRequirementsRepository.AddAsync(newBindingRequirements);
            //    }
            //}
            #endregion

            #region QuoteConditions
            //var quoteCond = await _quoteConditionsRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            //if (quoteCond != null)
            //{
            //    foreach (var x in quoteCond)
            //    {
            //        var newQuoteCond = SystemExtension.Clone<QuoteConditions>(x);
            //        newQuoteCond.Id = Empty;
            //        newQuoteCond.RiskDetailId = currentRiskDetailId;
            //        await _quoteConditionsRepository.AddAsync(newQuoteCond);
            //    }
            //}
            #endregion

            #region Claims
            if (backupClaims != null)
            {
                foreach (var claim in backupClaims)
                {
                    var newClaim = SystemExtension.Clone<ClaimsHistory>(claim);
                    newClaim.Id = Empty;
                    newClaim.RiskDetailId = currentRiskDetailId;
                    await _claimsHistoryRepository.AddAsync(newClaim);
                }
            }
            #endregion

            #region Destinations
            var destinations = await _riskSpecificDestinationRepository.GetAsync(x => x.RiskDetailId == riskDetailId);
            if (destinations != null)
            {
                foreach (var destination in destinations)
                {
                    var newDestination = SystemExtension.Clone<RiskSpecificDestination>(destination);
                    newDestination.Id = Empty;
                    newDestination.RiskDetailId = currentRiskDetailId;
                    await _riskSpecificDestinationRepository.AddAsync(newDestination);
                }
            }
            #endregion

            newRiskDetail.DriverInfo = null;

            if (fileUploadDocuments != null)
            {
                foreach (var item in fileUploadDocuments)
                {
                    item.RiskDetailId = riskDetailId;
                }
                await _fileUploadDocumentRepository.AddRangeAsync(fileUploadDocuments);
            }

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailVehicles = newRiskDetail.RiskDetailVehicles.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailVehicles();

            // The 'newRiskDetail.RiskDetailVehicles' after cloning from old RiskDetail
            // must be updated so we back up its reference.
            var oldRiskDetailDrivers = newRiskDetail.RiskDetailDrivers.ToList();
            // Then clear it to avoid error while adding 'newRiskDetail' in db context. 
            newRiskDetail.ClearRiskDetailDrivers();

            await _riskDetailRepository.AddAsync(newRiskDetail);

            #region Vehicles
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailVehicle in oldRiskDetailVehicles)
            {
                var vehicle = _vehicleRepository.GetById(riskDetailVehicle.VehicleId);  // Get vehicle.
                if (vehicle.PreviousVehicleVersionId != null)
                {
                    var previousVersion = _vehicleRepository.GetById(vehicle.PreviousVehicleVersionId.Value); // get previous version
                    vehicle.PreviousVehicleVersionId = (previousVersion is null) ? null : vehicle.PreviousVehicleVersionId; // check previous version if deleted already
                }
                var vehicleRatingFactors = _context.VehiclePremiumRatingFactor.Where(x => x.VehicleId == vehicle.Id.ToString()).ToList(); // get vehicle rating factors
                foreach (var vrf in vehicleRatingFactors)
                {
                    var newVehicleRatingFactor = SystemExtension.Clone<VehiclePremiumRatingFactor>(vrf);
                    newVehicleRatingFactor.Id = 0;
                    newVehicleRatingFactor.VehicleId = vehicle.Id.ToString();
                    newVehicleRatingFactor.RiskDetailId = newRiskDetail.Id;
                    _context.VehiclePremiumRatingFactor.Add(newVehicleRatingFactor);
                }
                var rdv = riskDetailVehicle.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                vehicle.RiskDetailVehicles.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                // update rating factors
                //var boundRatingFactors = vehicleRatingFactors.Find(vrf => vrf.OptionId == bindOption || vrf.OptionId == 0);
                //vehicle.AutoLiabilityPremium = boundRatingFactors?.TotalALPremium ?? 0;
                //vehicle.CargoPremium = boundRatingFactors?.CargoPremium ?? 0;
                //vehicle.PhysicalDamagePremium = boundRatingFactors?.TotalPDPremium ?? 0;
                //vehicle.ComprehensivePremium = (boundRatingFactors?.CompPremium ?? 0) + (boundRatingFactors?.FireTheftPremium ?? 0);
                //vehicle.CollisionPremium = boundRatingFactors?.CollisionPremium ?? 0;
                //vehicle.TotalPremium = boundRatingFactors?.PerVehiclePremium ?? 0;
                //vehicle.Radius = boundRatingFactors?.Radius ?? 0;
                //vehicle.RadiusTerritory = boundRatingFactors?.RadiusTerritory ?? 0;
                vehicle.GaragingAddressId = addressIdMapping.GetValueOrDefault(vehicle.GaragingAddressId.Value);
                // vehicle.UpdateLimits(boundRiskCoverage); // update limits
                _vehicleRepository.Update(vehicle);                                     // Update vehicle in db context.
            }
            await _vehicleRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region Drivers
            // After 'newRiskDetail' is added to db context, we now fetch the vehicles 
            // from 'oldRiskDetailVehicles' and add new RiskDetailVehicle reference 
            // with its RiskDetail set to 'newRiskDetail'
            foreach (var riskDetailDriver in oldRiskDetailDrivers)
            {
                var driver = _driverRepository.GetById(riskDetailDriver.DriverId);  // Get vehicle.
                if (driver.PreviousDriverVersionId != null)
                {
                    var previousVersion = _driverRepository.GetById(driver.PreviousDriverVersionId.Value); // get previous version
                    driver.PreviousDriverVersionId = (previousVersion is null) ? null : driver.PreviousDriverVersionId; // check previous version if deleted already
                }
                //if (driver.IsDeleted)
                //{
                //    var previousVersions = await _driverRepository.GetPreviousRiskDetailDrivers(newRiskDetail.RiskId); // get previous versions
                //    var isDriverAPreviousVersion = previousVersions.Any(x => x.Id == driver.Id);
                //    if (isDriverAPreviousVersion == false)
                //    {
                //        driver.IsActive = null; // re-enable record
                //        driver.DeletedDate = null; // re-enable record
                //    }
                //}

                bool hasBindOptionSelected = driver.Options != null && driver.Options.Split(',').Any(opt => opt.Equals(bindOption.ToString())) && !driver.IsDeleted;
                if (!hasBindOptionSelected && toCloneRiskDetail.Status == RiskDetailStatus.Quoted) continue; // any driver not bound from submision should not be carried over to policy

                var rdv = riskDetailDriver.WithNewRiskDetail(newRiskDetail);           // Create new RiskDetailVehicle with updated RiskDetail.
                driver.RiskDetailDrivers.Add(rdv);                                    // Add the new RiskDetailVehicle to vehicle.
                _driverRepository.Update(driver);                                     // Update drvr in db context.
            }
            await _driverRepository.SaveChangesAsync();                                // Save all vehicle changes.
            #endregion

            #region DriverInfo
            var newDriverInfo = await _riskSpecificDriverInfoRepository.FindAsync(x => x.Id == riskDetailId);
            if (newDriverInfo != null)
            {
                newDriverInfo.Id = currentRiskDetailId;
                newDriverInfo.TotalNumberOfDrivers = newRiskDetail.Drivers.Where(driver => ((driver.Options ?? "")?.Split(',')).Contains($"{bindOption}")).Count(); // should update always depending on bound drivers
                await _riskSpecificDriverInfoRepository.AddAsync(newDriverInfo);
            }
            #endregion

            await _riskDetailRepository.SaveChangesAsync();

            await _riskFormRepository.SaveChangesAsync();
            await _riskManuscriptsRepository.SaveChangesAsync();
            await _riskHistoryRepository.SaveChangesAsync();
            await _claimsHistoryRepository.SaveChangesAsync();
            await _riskSpecificSafetyDeviceRepository.SaveChangesAsync();
            await _riskSpecificDriverInfoRepository.SaveChangesAsync();
            await _riskSpecificDotInfoRepository.SaveChangesAsync();
            await _riskSpecificDriverHiringCriteriaRepository.SaveChangesAsync();
            await _fileUploadDocumentRepository.SaveChangesAsync();
            await _quoteConditionsRepository.SaveChangesAsync();
            _context.SaveChanges();
            return newRiskDetail;
        }

        private async Task DeleteCurrentPolicyDetail(Guid riskDetailId, Guid previousRiskDetailId)
        {
            var toDeleteRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);

            if (toDeleteRiskDetail == null) { return; }

            _riskDetailRepository.Detach(toDeleteRiskDetail);

            var executionStrategy = _context.Database.CreateExecutionStrategy();
            executionStrategy.Execute(() =>
            {
                using var transaction = _context.Database.BeginTransaction();
                // total clean up on data upon reset, to avoid data duplication
                try
                {
                    string deletePolicyDetail = string.Format(@"
                        DELETE FROM [dbo].[EntityAddress] WHERE [EntityId] = '{1}'
                        DELETE FROM [dbo].[EntitySubsidiary] WHERE [EntityId] = '{1}'
                        DELETE FROM [dbo].[RiskSpecificCommoditiesHauled] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificDriverInfo] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[DriverHeader] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificDriverHiringCriteria] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificDotInfo] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificDestination] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[ClaimsHistory] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskManuscripts] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificUnderwritingQuestion] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[RiskCoveragePremium] WHERE [RiskCoverageId] IN (SELECT [Id] FROM [RiskCoverage] WHERE [RiskDetailId] = '{0}')
                        DELETE FROM [dbo].[RiskCoverage] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskHistory] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificSafetyDevice] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskSpecificRadiusOfOperation] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[RiskFiling] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskForm] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[FileUploadDocument] WHERE [RiskDetailId] = '{0}'

                        UPDATE  [dbo].[Driver]
                        SET     [PreviousDriverVersionId] = NULL
                        WHERE   [PreviousDriverVersionId] IS NOT NULL 
                                AND [PreviousDriverVersionId] NOT IN (SELECT [DriverId] FROM [RiskDetailDriver] WHERE [RiskDetailId] = '{3}') 
                                AND [PreviousDriverVersionId] IN (SELECT [DriverId] FROM [RiskDetailDriver] WHERE [RiskDetailId] = '{0}')

                        DELETE FROM [dbo].[Driver] 
                        WHERE   [Id] NOT IN (SELECT [DriverId] FROM [RiskDetailDriver] WHERE [RiskDetailId] = '{3}') 
                                AND [Id] IN (SELECT [DriverId] FROM [RiskDetailDriver] WHERE [RiskDetailId] = '{0}')

                        UPDATE  [dbo].[Vehicle]
                        SET     [PreviousVehicleVersionId] = NULL
                        WHERE   [PreviousVehicleVersionId] IS NOT NULL 
                                AND [PreviousVehicleVersionId] NOT IN (SELECT [VehicleId] FROM [RiskDetailVehicle] WHERE [RiskDetailId] = '{3}') 
                                AND [PreviousVehicleVersionId] IN (SELECT [VehicleId] FROM [RiskDetailVehicle] WHERE [RiskDetailId] = '{0}')

                        DELETE FROM [dbo].[Vehicle]
                        WHERE   [Id] NOT IN (SELECT [VehicleId] FROM [RiskDetailVehicle] WHERE [RiskDetailId] = '{3}') 
                                AND [Id] IN (SELECT [VehicleId] FROM [RiskDetailVehicle] WHERE [RiskDetailId] = '{0}')

                        DELETE FROM [dbo].[RIskDetailVehicle] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskDetailDriver] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[VehiclePremiumRatingFactor] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[VehiclePremium] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskResponse] WHERE [RiskDetailId] = '{0}'
                        DELETE FROM [dbo].[RiskAdditionalInterest] WHERE [RiskDetailId] = '{0}'                     
                        DELETE FROM [dbo].[RiskDetail] WHERE [Id] = '{0}'
                        DELETE FROM [dbo].[Insured] WHERE [Id] = '{2}'
                        DELETE FROM [dbo].[Entity] WHERE [Id] = '{1}'
                    ", riskDetailId, toDeleteRiskDetail.Insured.EntityId, toDeleteRiskDetail.InsuredId, previousRiskDetailId);

                    _context.Database.ExecuteSqlRaw(deletePolicyDetail);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }
                transaction.Commit();
            });
        }

        public async Task<List<string>> GetPolicyChanges(Guid currentRiskDetailId, Guid riskId, bool includeAll = false)
        {
            // reset temp variables
            currentCargoCoverageValue = string.Empty;
            currentRefrigerationCoverageValue = string.Empty;

            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();

            var currentRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(currentRiskDetailId);
            var previousRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);

            var bindData = await _bindingService.GetBindingByRiskIdAsync(riskId);
            int bindOption = (bindData != null) ? bindData?.BindOptionId ?? 1 : 1;
            var businessAddress = _entityAddressRepository.FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business && x.EntityId == currentRiskDetail.Insured.EntityId, i => i.Address).Result;
            _limitList = _limitsService.GetPerStateAsync(businessAddress.Address?.State).Result;

            var changes = new List<string>();

            // Applicant - Business Name
            changes.AddRange(Compare(previousRiskDetail.Insured.Entity, currentRiskDetail.Insured.Entity, nameof(Entity)));
            // Applicant - Address
            changes.AddRange(CompareList(previousRiskDetail.Insured.Entity.EntityAddresses.AsQueryable(), currentRiskDetail.Insured.Entity.EntityAddresses.AsQueryable()));

            // Limits
            var previousRiskCoverage = previousRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            var currentRiskCoverage = currentRiskDetail.RiskCoverages.Where(x => x.OptionNumber == bindOption).FirstOrDefault();
            changes.AddRange(Compare(previousRiskCoverage, currentRiskCoverage, nameof(RiskCoverage)));

            // Additional Interest
            var previousAdditionalInterest = await _riskAdditionalInterestRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId, i => i.Entity);
            var currentAdditionalInterest = await _riskAdditionalInterestRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId, i => i.Entity);
            changes.AddRange(CompareList(previousAdditionalInterest.AsQueryable(), currentAdditionalInterest.AsQueryable()));

            // Manuscript
            var previousManuscript = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId);
            var currentManuscript = await _riskManuscriptsRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            changes.AddRange(CompareList(previousManuscript.AsQueryable(), currentManuscript.AsQueryable()));

            // Vehicle
            changes.AddRange(CompareList(previousRiskDetail.Vehicles.AsQueryable(), currentRiskDetail.Vehicles.AsQueryable(), bindOption, includeAll));

            // driver header
            changes.AddRange(Compare(previousRiskDetail.DriverHeader, currentRiskDetail.DriverHeader, nameof(DriverHeader)));

            // Drivers
            changes.AddRange(CompareList(previousRiskDetail.Drivers.AsQueryable(), currentRiskDetail.Drivers.AsQueryable(), bindOption, includeAll));

            // Risk History
            // var previousRiskHistory = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId);
            // var currentRiskHistory = await _riskHistoryRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            // changes.AddRange(CompareList(previousRiskHistory.AsQueryable(), currentRiskHistory.AsQueryable()));

            // Claims History
            // var previousClaimsHistory = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId);
            // var currentClaimsHistory = await _claimsHistoryRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId);
            // changes.AddRange(CompareList(previousClaimsHistory.AsQueryable(), currentClaimsHistory.AsQueryable()));

            // Responses
            // var previousRiskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId, x => x.Question);
            // var currentRiskResponses = await _riskResponseRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId, x => x.Question);
            // changes.AddRange(CompareList(previousRiskResponses.AsQueryable(), currentRiskResponses.AsQueryable()));

            // Commodities
            // var previousCommodities = previousRiskDetail.CommoditiesHauled;
            // var currentCommodities = currentRiskDetail.CommoditiesHauled;
            // changes.AddRange(CompareList(previousCommodities.AsQueryable(), currentCommodities.AsQueryable()));

            // Risk Coverage Premium
            // changes.AddRange(Compare(ValidateEndorsementPremiums(previousRiskCoverage), ValidateEndorsementPremiums(currentRiskCoverage), nameof(RiskCoveragePremium)));

            // Risk Forms
            var previousRiskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId, i => i.Form);
            var currentRiskForms = await _riskFormRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId, i => i.Form);
            changes.AddRange(CompareList(previousRiskForms.AsQueryable(), currentRiskForms.AsQueryable()));

            // Update RiskDetail Status to Pending Endorsement if changes > 0
            if (changes.Count() > 0 && currentRiskDetail.Status == RiskDetailStatus.Active)
            {
                currentRiskDetail.Status = RiskDetailStatus.PendingEndorsement;
                _riskDetailRepository.Update(currentRiskDetail);
                await _riskDetailRepository.SaveChangesAsync();
            }

            return changes;
        }

        #region Compare Policy Details

        public bool IsDriverChanged(Driver oldDriver, Driver newDriver)
        {
            var compareResult = new List<string>();
            compareResult.AddRange(Compare(oldDriver.Entity, newDriver.Entity, nameof(Entity), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} "));
            compareResult.AddRange(Compare(oldDriver, newDriver, nameof(Driver), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} "));
            if (compareResult.Count() > 0)
                return true;

            compareResult.AddRange(CompareList(oldDriver.DriverIncidents.AsQueryable(), newDriver.DriverIncidents.AsQueryable(), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} "));
            if (compareResult.Count() > 0)
                return true;

            return false;
        }

        private List<string> Compare(object oldProp, object newProp, string path, string label = "", bool includeAll = false)
        {
            var result = new List<string>();
            if (oldProp == null && newProp == null)
                return result;

            if ((oldProp ?? newProp) is IEnumerable<object>)
            {
                return result;
            }

            var propType = (oldProp ?? newProp).GetType();
            var propertyInfos = propType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propInfo in propertyInfos)
            {
                if (_paths.Any(x => x == $"{propType.Name}.{propInfo.Name}")
                    // && !propType.Name.Equals("ClaimsHistory")
                    // && !propType.Name.Equals("RiskHistory")
                    && !propType.Name.Equals("Vehicle")
                    && !propType.Name.Equals("Driver")
                    && !propType.Name.Equals("Entity")
                    && !propType.Name.Equals("Address")
                    && !propType.Name.Equals("DriverHeader")
                    && !propType.Name.Equals("DriverIncidents")
                    && !propType.Name.Equals("RiskAdditionalInterest")
                    // && !propType.Name.Equals("RiskCoveragePremium")
                 )
                    continue; 
                _paths.Add($"{propType.Name}.{propInfo.Name}");

                var fullName = $"{path}.{propInfo.Name}";

                if (!FieldLabels.Labels.ContainsKey(fullName) && !(includeAll && FieldLabels.ExcludedLabels.ContainsKey(fullName))) continue;

                object oldValue = null;
                object newValue = null;

                if (oldProp != null) oldValue = propInfo.GetValue(oldProp, null);
                if (newProp != null) newValue = propInfo.GetValue(newProp, null);

                var nullableType = Nullable.GetUnderlyingType(propInfo.PropertyType);

                var isNullablePrimitive = (nullableType != null && (nullableType.IsPrimitive
                    || nullableType == typeof(string)
                    || nullableType == typeof(decimal)
                    || propInfo == typeof(double)
                    || propInfo == typeof(short)
                    || propInfo == typeof(int)
                    || propInfo == typeof(long)
                    || propInfo == typeof(bool)
                ));

                if (propInfo.PropertyType.IsPrimitive
                    || propInfo.PropertyType == typeof(string)
                    || propInfo.PropertyType == typeof(decimal)
                    || propInfo.PropertyType == typeof(double)
                    || propInfo.PropertyType == typeof(short)
                    || propInfo.PropertyType == typeof(int)
                    || propInfo.PropertyType == typeof(long)
                    || propInfo.PropertyType == typeof(bool)
                    || propInfo.PropertyType == typeof(DateTime?)
                    || propInfo.PropertyType == typeof(Guid?)
                    || isNullablePrimitive)
                {
                    if (propInfo.PropertyType == typeof(bool) || propInfo.PropertyType == typeof(bool?))
                    {
                        oldValue = Convert.ToBoolean(oldValue ?? false).ToYesNo();
                        newValue = Convert.ToBoolean(newValue ?? false).ToYesNo();
                    }

                    if (propInfo.PropertyType == typeof(decimal?))
                    {
                        oldValue = Convert.ToDecimal(oldValue ?? 0);
                        newValue = Convert.ToDecimal(newValue ?? 0);
                    }

                    if (((newValue == null && oldValue != null) || (newValue == "No" && oldValue == "Yes")))
                    {
                        if (FormatPropertyName(fullName) == "Main Garaging Address" || FormatPropertyName(fullName) == "Is MVR?" || FormatPropertyName(fullName) == "Is Out of State Driver?")
                        {
                            result.Add($"Changed {label} {FormatPropertyName(fullName)} From: '{oldValue}' To: '{newValue}'");
                        }
                        else
                        {
                            if (path == nameof(Vehicle))
                            {
                                if (fullName == "Vehicle.HasCoverageCargo") result.Add($"Removed {label}{FormatPropertyName(fullName)} '{currentCargoCoverageValue}'");
                                if (fullName == "Vehicle.HasCoverageRefrigeration") result.Add($"Removed {label}{FormatPropertyName(fullName)} '{currentRefrigerationCoverageValue}'");
                                if (fullName == "Vehicle.CoverageCollisionDeductibleId") result.Add($"Removed {label}{FormatPropertyName(fullName)} '{FormatValues(fullName, oldValue)}'");
                                if (fullName == "Vehicle.CoverageFireTheftDeductibleId") result.Add($"Removed {label}{FormatPropertyName(fullName)} '{FormatValues(fullName, oldValue)}'");
                            }
                            else
                            {
                                result.Add($"Removed {label}{FormatPropertyName(fullName)}");
                            }
                        }
                    }
                    else if (propInfo.PropertyType == typeof(string) && Convert.ToString(newValue) == String.Empty && oldValue == null)
                    {
                        continue;
                    }
                    else if (propInfo.PropertyType == typeof(decimal?) && Convert.ToDecimal(newValue ?? 0) == 0 && Convert.ToDecimal(oldValue ?? 0) == 0)
                    {
                        continue;
                    }
                    else if ((path == nameof(RiskCoveragePremium) || path == nameof(Vehicle) || path == nameof(RiskHistory) || path == nameof(ClaimsHistory))
                                && propInfo.PropertyType == typeof(decimal?) && Convert.ToDecimal(newValue ?? 0) != 0 && Convert.ToDecimal(oldValue ?? 0) == 0)
                    {
                        result.Add($"Added {label}{FormatPropertyName(fullName)}: '{FormatValues(fullName, newValue)}'");
                    }
                    else if ((newValue != null && oldValue == null))
                    {
                        if (propInfo.PropertyType == typeof(DateTime?))
                        {
                            result.Add($"Added {label}{FormatPropertyName(fullName)}: '{newValue:MM/dd/yyyy}'");
                        }
                        else if (propInfo.PropertyType == typeof(int?) || propInfo.PropertyType == typeof(short?))
                        {
                            if (path == nameof(Vehicle) || path == nameof(RiskCoverage))
                            {
                                result.Add($"Added {label}{FormatPropertyName(fullName)}: '{FormatValues(fullName, newValue)}'");
                            }
                            else
                            {
                                result.Add($"Added {label}{FormatPropertyName(fullName)}: '{newValue:N0}'");
                            }
                        }
                        else
                        {
                            result.Add($"Added {label}{FormatPropertyName(fullName)}: '{FormatValues(fullName, newValue)}'");
                        }
                    }
                    else if (propInfo.PropertyType == typeof(DateTime?) && $"{oldValue:MM/dd/yyyy}" != $"{newValue:MM/dd/yyyy}")
                    {
                        // result.Add($"Changed {label}{FormatPropertyName(fullName)} From: {oldValue:MM/dd/yyyy} To: {newValue:MM/dd/yyyy}");
                        result.Add($"Changed {label}{FormatPropertyName(fullName)} To: {newValue:MM/dd/yyyy}");
                    }
                    else if (newValue == "Yes" && oldValue == "No" && path == nameof(Vehicle))
                    {
                        if (fullName == "Vehicle.HasCoverageCargo") result.Add($"Added {label}{FormatPropertyName(fullName)} To: '{currentCargoCoverageValue}'");
                        if (fullName == "Vehicle.HasCoverageRefrigeration") result.Add($"Added {label}{FormatPropertyName(fullName)} To: '{currentRefrigerationCoverageValue}'");
                    }
                    else if (newValue != null && oldValue != null && !oldValue.Equals(newValue))
                    {
                        switch (fullName) // ui computed values in Vehicle Forms
                        {
                            case "Vehicle.Year":
                                int currentYear = DateTime.Now.Year;
                                int newVehicleAge = currentYear - Convert.ToInt32(newValue);
                                int oldVehicleAge = currentYear - Convert.ToInt32(oldValue);
                                // result.Add($"Changed {label} Age From: '{FormatValues(fullName, oldVehicleAge)}' To: '{FormatValues(fullName, newVehicleAge)}'");
                                result.Add($"Changed {label} Age To: '{FormatValues(fullName, newVehicleAge)}'");
                                break;
                            case "Vehicle.GrossVehicleWeightId":
                                // result.Add($"Changed {label}{FormatPropertyName("Vehicle.GrossVehicleWeightCategory")} From: '{FormatValues("Vehicle.GrossVehicleWeightCategory", oldValue)}' To: '{FormatValues("Vehicle.GrossVehicleWeightCategory", newValue)}'");
                                result.Add($"Changed {label}{FormatPropertyName("Vehicle.GrossVehicleWeightCategory")} To: '{FormatValues("Vehicle.GrossVehicleWeightCategory", newValue)}'");
                                break;
                            default:
                                break;
                        }

                        // result.Add($"Changed {label}{FormatPropertyName(fullName)} From: '{FormatValues(fullName, oldValue)}' To: '{FormatValues(fullName, newValue)}'");
                        result.Add($"Changed {label}{FormatPropertyName(fullName)} To: '{FormatValues(fullName, newValue)}'");
                    }

                    if (fullName == "RiskCoverage.CargoLimitId")
                    {
                        currentCargoCoverageValue = (FormatValues(fullName, newValue) == "NONE") ? FormatValues(fullName, oldValue) : FormatValues(fullName, newValue);
                    }
                        
                    if (fullName == "RiskCoverage.RefCargoLimitId")
                    {
                        currentRefrigerationCoverageValue = (FormatValues(fullName, newValue) == "NONE") ? FormatValues(fullName, oldValue) : FormatValues(fullName, newValue);
                    }
                }
                else if (propInfo.PropertyType.IsArray)
                {
                    return result;
                }
                else
                {
                    if (newValue != null || oldValue != null)
                    {
                        result.AddRange(Compare(oldValue, newValue, fullName));
                    }
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<RiskAdditionalInterest> oldList, IQueryable<RiskAdditionalInterest> newList)
        {
            var result = new List<string>();
            var formChanges = new List<string>();

            var addedInterests = newList.Where(newObj => oldList.All(oldObj => oldObj.AddProcessDate != newObj.AddProcessDate));

            foreach (var interest in addedInterests)
            {
                var interestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                string interestAddress = $"{interestEntityAddress.Address.StreetAddress1}, {interestEntityAddress.Address.City}, {interestEntityAddress.Address.State}, {interestEntityAddress.Address.ZipCode}";
                result.Add($"Added Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", interest.AdditionalInterestTypeId)}': '{interest.Entity.FullName}', '{interestAddress}'");
            }

            foreach (var oldInterest in oldList.OrderBy(x => x.AddProcessDate))
            {
                var interest = newList.FirstOrDefault(newObj => newObj.AddProcessDate.Ticks == oldInterest.AddProcessDate.Ticks);

                if (interest is null) continue;
                
                var interestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                
                string interestAddress = $"{interestEntityAddress.Address.StreetAddress1}, {interestEntityAddress.Address.City}, {interestEntityAddress.Address.State}, {interestEntityAddress.Address.ZipCode}";

                if (interest.RemoveProcessDate != null && oldInterest.RemoveProcessDate == null)
                {
                    result.Add($"Removed Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", interest.AdditionalInterestTypeId)}': '{interest.Entity.FullName}', '{interestAddress}'");
                }
                else
                {
                    var oldInterestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == oldInterest.EntityId, e => e.Address).Result;
                    var currentInterestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                    string oldInterestAddressStr = $"{oldInterestEntityAddress.Address.StreetAddress1}, {oldInterestEntityAddress.Address.City}, {oldInterestEntityAddress.Address.State}, {oldInterestEntityAddress.Address.ZipCode}";
                    result.AddRange(Compare(oldInterest.Entity, interest.Entity, nameof(Entity), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));
                    result.AddRange(Compare(oldInterestEntityAddress.Address, currentInterestEntityAddress.Address, nameof(Address), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));
                    result.AddRange(Compare(oldInterest, interest, nameof(RiskAdditionalInterest), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));
                }
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 1 && x.RemoveProcessDate == null) == 0 && newList.Count(x => x.AdditionalInterestTypeId == 1 && x.RemoveProcessDate == null) > 0) // Additional Insured
            {
                formChanges.Add("Added Form 'Additional Insured Endorsement'");
            }

            if (oldList.Count(x => x.IsPrimaryNonContributory && x.RemoveProcessDate == null) == 0 && newList.Count(x => x.IsPrimaryNonContributory && x.RemoveProcessDate == null) > 0)
            {
                formChanges.Add("Added Form 'Primary and Noncontributory'");
            }

            if (oldList.Count(x => x.IsWaiverOfSubrogation && x.RemoveProcessDate == null) == 0 && newList.Count(x => x.IsWaiverOfSubrogation && x.RemoveProcessDate == null) > 0)
            {
                formChanges.Add("Added Form 'Waiver of Transfer of Right'");
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 2 && x.RemoveProcessDate == null) == 0 && newList.Count(x => x.AdditionalInterestTypeId == 2 && x.RemoveProcessDate == null) > 0) // Loss Payee
            {
                formChanges.Add("Added Form 'Loss Payee Endorsement'");
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 12 && x.RemoveProcessDate == null) == 0 && newList.Count(x => x.AdditionalInterestTypeId == 12 && x.RemoveProcessDate == null) > 0) // Designated Insured
            {
                formChanges.Add("Added Form 'Designated Insured'");
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 1 && x.RemoveProcessDate == null) > 0 && newList.Count(x => x.AdditionalInterestTypeId == 1 && x.RemoveProcessDate == null) == 0) // Additional Insured
            {
                formChanges.Add("Removed Form 'Additional Insured Endorsement'");
            }

            if (oldList.Count(x => x.IsPrimaryNonContributory && x.RemoveProcessDate == null) > 0 && newList.Count(x => x.IsPrimaryNonContributory && x.RemoveProcessDate == null) == 0)
            {
                formChanges.Add("Removed Form 'Primary and Noncontributory'");
            }

            if (oldList.Count(x => x.IsWaiverOfSubrogation && x.RemoveProcessDate == null) > 0 && newList.Count(x => x.IsWaiverOfSubrogation && x.RemoveProcessDate == null) == 0)
            {
                formChanges.Add("Removed Form 'Waiver of Transfer of Right'");
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 2 && x.RemoveProcessDate == null) > 0 && newList.Count(x => x.AdditionalInterestTypeId == 2 && x.RemoveProcessDate == null) == 0) // Loss Payee
            {
                formChanges.Add("Removed Form 'Loss Payee Endorsement'");
            }

            if (oldList.Count(x => x.AdditionalInterestTypeId == 12 && x.RemoveProcessDate == null) > 0 && newList.Count(x => x.AdditionalInterestTypeId == 12 && x.RemoveProcessDate == null) == 0) // Designated Insured
            {
                formChanges.Add("Removed Form 'Designated Insured'");
            }

            if (formChanges.Count > 0) result.AddRange(formChanges);

            return result;
        }

        private List<string> CompareList(IQueryable<RiskSpecificCommoditiesHauled> oldList, IQueryable<RiskSpecificCommoditiesHauled> newList)
        {
            var result = new List<string>();

            var addedCommodity = newList.Where(newObj => oldList.All(oldObj => oldObj.CreatedDate != newObj.CreatedDate));
            var removedCommodity = oldList.Where(oldObj => newList.All(newObj => newObj.CreatedDate != oldObj.CreatedDate));

            foreach (var commodity in addedCommodity)
            {
                result.Add($"Added Commodity '{commodity.Commodity}'; '{commodity.PercentageOfCarry:N1}%' of Carry");
            }

            foreach (var commodity in removedCommodity)
            {
                result.Add($"Removed Commodity '{commodity.Commodity}'; '{commodity.PercentageOfCarry:N1}%' of Carry");
            }

            foreach (var oldCommodity in oldList)
            {
                var currentCommodity = newList.FirstOrDefault(x => x.CreatedDate.Ticks == oldCommodity.CreatedDate.Ticks);

                if (currentCommodity is null) continue;

                result.AddRange(Compare(oldCommodity, currentCommodity, nameof(RiskSpecificCommoditiesHauled), $"{oldCommodity.Commodity}: "));
            }

            return result;
        }

        private List<string> CompareList(IQueryable<RiskManuscripts> oldList, IQueryable<RiskManuscripts> newList)
        {
            var result = new List<string>();
            var formChanges = new List<string>();

            var addedManuscripts = newList.Where(newObj => oldList.All(oldObj => oldObj.CreatedDate != newObj.CreatedDate) && newObj.DeletedDate == null);

            foreach (var manuscript in addedManuscripts)
            {
                result.Add($"Added Manuscript '{manuscript.Title}'");
                formChanges.Add($"Added Form 'Manuscript {manuscript.Title}'");
            }

            foreach (var oldManuscript in oldList.OrderBy(x => x.Title))
            {
                var newManuscript = newList.FirstOrDefault(x => x.CreatedDate == oldManuscript.CreatedDate);

                if (newManuscript is null) continue;

                if ((newManuscript.DeletedDate != null && oldManuscript.DeletedDate == null))
                {
                    result.Add($"Removed Manuscript '{oldManuscript.Title}'");
                    formChanges.Add($"Removed Form 'Manuscript {oldManuscript.Title}'");
                    continue;
                }
                else
                {
                    var compareResult = Compare(oldManuscript, newManuscript, nameof(RiskManuscripts), $"{oldManuscript.Title} ");
                    result.AddRange(compareResult);
                }
            }

            if (formChanges.Count > 0) result.AddRange(formChanges);
                
            return result;
        }

        private List<string> CompareList(IQueryable<RiskHistory> oldList, IQueryable<RiskHistory> newList)
        {
            var result = new List<string>();

            foreach (var oldRiskHistory in oldList)
            {
                var newRiskHistory = newList.FirstOrDefault(x => x.HistoryYear == oldRiskHistory.HistoryYear);

                if (newRiskHistory != null)
                {
                    var compareResult = Compare(oldRiskHistory, newRiskHistory, nameof(RiskHistory), $"{GetHistoryYear(newRiskHistory.HistoryYear.Value)} ");
                    result.AddRange(compareResult);
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<ClaimsHistory> oldList, IQueryable<ClaimsHistory> newList)
        {
            var result = new List<string>();

            foreach (var oldClaimsHistory in oldList)
            {
                var newClaimsHistory = newList.FirstOrDefault(x => x.Order == oldClaimsHistory.Order);

                if (newClaimsHistory != null)
                {
                    var compareResult = Compare(oldClaimsHistory, newClaimsHistory, nameof(ClaimsHistory), $"{GetHistoryYear(newClaimsHistory.Order.Value)} ");
                    result.AddRange(compareResult);
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<RiskResponse> oldList, IQueryable<RiskResponse> newList)
        {
            var result = new List<string>();

            var addedResponses = newList.Where(newObj => oldList.All(oldObj => oldObj.QuestionId != newObj.QuestionId)).GroupBy(x => x.QuestionId).Select(x => x.First()).Distinct();
            var removedResponses = oldList.Where(oldObj => newList.All(newObj => newObj.QuestionId != oldObj.QuestionId));

            var skippedResponses = new string[] { "haveOperationsOtherThanTruckingExplain", "hasApplicantHadGeneralLiabilityLossExplain", "areCommoditiesStoredInTruckOvernightExplain", "hadCargoLossExplain" };

            foreach (var response in addedResponses)
            {
                if (skippedResponses.Contains(response.Question.Description)) continue;
                if (response.ResponseValue is null) continue;
                //result.Add($"Added '{FormatPropertyName(response.Question.Description)}': '{(response.ResponseValue == "True" ? "Yes" : "No")}'");
                result.Add($"Added '{FormatPropertyName(response.Question.Description)}'");
            }

            foreach (var response in removedResponses)
            {
                if (skippedResponses.Contains(response.Question.Description)) continue;
                result.Add($"Removed '{FormatPropertyName(response.Question.Description)}'");
            }

            foreach (var oldResponses in oldList.GroupBy(x => x.QuestionId).Select(x => x.First()).Distinct())
            {
                var newResponses = newList.FirstOrDefault(x => x.QuestionId == oldResponses.QuestionId);

                if (newResponses != null)
                {
                    if (!FieldLabels.Labels.ContainsKey(newResponses.Question.Description)) continue;
                    if (oldResponses.ResponseValue != null && newResponses.ResponseValue != null && !newResponses.ResponseValue.Equals(oldResponses.ResponseValue))
                    {
                        if (newResponses.Question.Description != "glAnnualPayroll")
                        {
                            string oldResponse = oldResponses.ResponseValue == "True" ? "Yes" : "No";
                            string newResponse = newResponses.ResponseValue == "True" ? "Yes" : "No";
                            result.Add($"Changed '{FormatPropertyName(newResponses.Question.Description)}' From: '{oldResponse}' To: '{newResponse}'");
                        }
                        else
                        {
                            result.Add($"Changed '{FormatPropertyName(newResponses.Question.Description)}' From: '${Convert.ToDecimal(oldResponses.ResponseValue ?? "0"):N0}' To: '${Convert.ToDecimal(newResponses.ResponseValue ?? "0"):N0}'");
                        }
                    }
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<Vehicle> oldList, IQueryable<Vehicle> newList, int bindOption, bool includeAll = false)
        {
            var result = new List<string>();

            var addedVehicles = newList.Where(newObj => oldList.All(oldObj => oldObj.VIN != newObj.VIN) && newObj.DeletedDate == null);

            foreach (var vehicle in addedVehicles)
            {
                result.Add($"Added VIN '{vehicle.VIN}'");
            }

            foreach (var oldVehicle in oldList.OrderBy(x => x.VIN))
            {
                var newVehicle = newList.FirstOrDefault(vehicle => vehicle.VIN == oldVehicle.VIN && (vehicle.PreviousVehicleVersionId ?? Empty) == oldVehicle.Id);

                if (newVehicle is null) continue;

                if (newVehicle.IsDeleted && newList.Any(x => (x.PreviousVehicleVersionId ?? Empty) == oldVehicle.Id))
                {
                    newVehicle = newList.FirstOrDefault(vehicle => vehicle.VIN == oldVehicle.VIN && (vehicle.PreviousVehicleVersionId ?? Empty) == oldVehicle.Id);
                }

                if (newVehicle.IsDeleted && !oldVehicle.IsDeleted)
                {
                    result.Add($"Removed VIN '{oldVehicle.VIN}'");
                    continue;
                }

                if (!newVehicle.IsDeleted && !oldVehicle.IsDeleted || (newVehicle.PreviousVehicleVersionId ?? Empty) == oldVehicle.Id)
                {
                    var compareResult = Compare(oldVehicle, newVehicle, nameof(Vehicle), $"VIN {oldVehicle.VIN} ", includeAll);
                    result.AddRange(compareResult);
                }

                if ((newVehicle.ExpirationDate.Value >= oldVehicle.ExpirationDate.Value) && (!newVehicle.IsDeleted && oldVehicle.IsDeleted)) // reinstated vehicle
                {
                    result.Add($"Reinstated VIN '{newVehicle.VIN}'");
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<Driver> oldList, IQueryable<Driver> newList, int bindOption, bool includeAll = false)
        {
            var result = new List<string>();

            var addedDrivers = newList.Where(newObj => oldList.All(oldObj => oldObj.LicenseNumber != newObj.LicenseNumber) && newObj.DeletedDate == null);

            foreach (var driver in addedDrivers)
            {
                result.Add($"Added Driver '{driver.Entity.FirstName} {driver.Entity.LastName}'");
            }

            foreach (var oldDriver in oldList.OrderBy(x => x.Entity.FirstName))
            {
                var newDriver = newList.FirstOrDefault(driver => driver.LicenseNumber == oldDriver.LicenseNumber && driver.Id == oldDriver.Id);

                if (newDriver is null)
                {
                    var isDeletedDriver = newList.FirstOrDefault(driver => driver.LicenseNumber == oldDriver.LicenseNumber && (driver.PreviousDriverVersionId ?? driver.Id) == oldDriver.Id);

                    if (isDeletedDriver is null) continue;

                    newDriver = isDeletedDriver;
                }

                if (newDriver.IsDeleted && newList.Any(x => (x.PreviousDriverVersionId ?? Empty) == oldDriver.Id))
                {
                    newDriver = newList.FirstOrDefault(driver => driver.LicenseNumber == oldDriver.LicenseNumber && (driver.PreviousDriverVersionId ?? Empty) == oldDriver.Id);
                }

                if (newDriver.IsDeleted && !oldDriver.IsDeleted) // all previous records are deleted
                {
                    result.Add($"Removed Driver '{oldDriver.Entity.FirstName} {oldDriver.Entity.LastName}'");
                    continue;
                }

                if (!newDriver.IsDeleted && !oldDriver.IsDeleted || ((newDriver.PreviousDriverVersionId ?? Empty) == oldDriver.Id && !newDriver.IsDeleted))
                {
                    var compareResult = new List<string>();
                    compareResult.AddRange(Compare(oldDriver.Entity, newDriver.Entity, nameof(Entity), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} "));
                    compareResult.AddRange(Compare(oldDriver, newDriver, nameof(Driver), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} ", includeAll));
                    if (includeAll)
                    {
                        if (!(newDriver.Options ?? "").Split(',').Any(x => x == bindOption.ToString()) && (oldDriver.Options ?? "").Split(',').Any(x => x == bindOption.ToString()))
                        {
                            compareResult.Add($"Changed Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} Pass Rating? From: 'Yes' To: 'No'");
                        }
                        else if ((newDriver.Options ?? "").Split(',').Any(x => x == bindOption.ToString()) && !(oldDriver.Options ?? "").Split(',').Any(x => x == bindOption.ToString()))
                        {
                            compareResult.Add($"Changed Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} Pass Rating? From: 'No' To: 'Yes'");
                        }
                    }
                    result.AddRange(compareResult);

                    // driver incidents
                    var changedDriverIncidentsData = _driverIncidentsRepository.GetAsync(x => x.DriverId == newDriver.Id).Result;
                    var oldDriverIncidentsData = _driverIncidentsRepository.GetAsync(x => x.DriverId == oldDriver.Id).Result;
                    result.AddRange(CompareList(oldDriverIncidentsData.AsQueryable(), changedDriverIncidentsData.AsQueryable(), $"Driver {oldDriver.Entity.FirstName} {oldDriver.Entity.LastName} "));
                }

                if (!newDriver.IsDeleted && oldDriver.IsDeleted) // reinstated driver
                {
                    result.Add($"Reinstated Driver '{newDriver.Entity.FirstName} {newDriver.Entity.LastName}'");
                }
            }

            return result;
        }

        private List<string> CompareList(IQueryable<DriverIncidents> oldList, IQueryable<DriverIncidents> newList, string label)
        {
            var result = new List<string>();
            label = $"Incident for {label}";

            var newDriverIncidents = newList.Where(newObj => oldList.All(oldObj => oldObj.UniqueId != newObj.UniqueId));
            var removedDriverIncidents = oldList.Where(oldObj => newList.All(newObj => newObj.UniqueId != oldObj.UniqueId));

            foreach (var newDriverIncident in newDriverIncidents)
            {
                string addedDriverIncidentStr = $"Added {label} '{FormatValues("DriverIncidents.IncidentType", newDriverIncident.IncidentType)}' Dated: '{FormatValues("DriverIncidents.IncidentDate", newDriverIncident.IncidentDate)}'";

                if (newDriverIncident.ConvictionDate != null) addedDriverIncidentStr += $" Conviction Date: '{FormatValues("DriverIncidents.ConvictionDate", newDriverIncident.ConvictionDate)}'";

                result.Add(addedDriverIncidentStr);
            }

            foreach (var removedDriverIncident in removedDriverIncidents)
            {
                string removedDriverIncidentStr = $"Removed {label} '{FormatValues("DriverIncidents.IncidentType", removedDriverIncident.IncidentType)}' Dated: '{FormatValues("DriverIncidents.IncidentDate", removedDriverIncident.IncidentDate)}'";

                if (removedDriverIncident.ConvictionDate != null) removedDriverIncidentStr += $" Conviction Date: '{FormatValues("DriverIncidents.ConvictionDate", removedDriverIncident.ConvictionDate)}'";

                result.Add(removedDriverIncidentStr);
            }

            foreach (var oldIncident in oldList)
            {
                var currentIncident = newList.Where(newObj => newObj.UniqueId == oldIncident.UniqueId).FirstOrDefault();
                string incidentType = FormatValues("DriverIncidents.IncidentType", oldIncident.IncidentType);
                result.AddRange(Compare(oldIncident, currentIncident, nameof(DriverIncidents), $"{label.Trim()}: {incidentType} "));
            }

            return result;
        }

        private List<string> CompareList(IQueryable<EntityAddress> oldList, IQueryable<EntityAddress> newList)
        {
            var result = new List<string>();

            var addedAddress = newList.Where(newObj => oldList.All(oldObj => oldObj.CreatedDate != newObj.CreatedDate));
            var removedAddress = oldList.Where(oldObj => newList.All(newObj => newObj.CreatedDate != oldObj.CreatedDate));

            foreach (var address in addedAddress)
            {
                if (string.IsNullOrEmpty(address.Address.StreetAddress1)) continue;

                result.Add($"Added Garaging Address '{address.Address.StreetAddress1}, {address.Address.City}, {address.Address.State} {address.Address.ZipCode}'");
            }

            foreach (var address in removedAddress)
            {
                if (string.IsNullOrEmpty(address.Address.StreetAddress1)) continue;

                result.Add($"Removed Garaging Address '{address.Address.StreetAddress1}, {address.Address.City}, {address.Address.State} {address.Address.ZipCode}'");
            }

            foreach (var oldAddress in oldList)
            {
                if (string.IsNullOrEmpty(oldAddress.Address.StreetAddress1)) continue;

                var newAddress = newList.FirstOrDefault(x => x.CreatedDate == oldAddress.CreatedDate && x.Address.UniqueId == oldAddress.Address.UniqueId);

                if (newAddress is null) continue;

                var compareResult = Compare(oldAddress.Address, newAddress.Address, nameof(Address), $"{FormatValues("Address.AddressTypeId", oldAddress.AddressTypeId)} ");

                result.AddRange(compareResult);
            }

            return result;
        }

        private List<string> CompareList(IQueryable<RiskForm> oldList, IQueryable<RiskForm> newList)
        {
            var result = new List<string>();
            var addedForms = newList.Where(newObj => oldList.All(oldObj => oldObj.FormId != newObj.FormId));
            var removedForms = oldList.Where(oldObj => newList.All(newObj => newObj.FormId != oldObj.FormId));

            var exemptedFormIdForChecking = new string[] { "AdditionalInsuredBlanket", "DesignatedInsured", "LossPayeeEndorsement", "PrimaryAndNoncontributory", "WaiverOfTransferOfRights" }; // these forms are already checked and validated on Additional Interest section
            
            foreach (var addedForm in addedForms.Where(x => !exemptedFormIdForChecking.Contains(x.FormId)).OrderBy(x => x.Form.DecPageOrder))
            {
                result.Add($"Added Form '{addedForm.Form.Name}'");
            }

            foreach (var removedForm in removedForms.Where(x => !exemptedFormIdForChecking.Contains(x.FormId)).OrderBy(x => x.Form.DecPageOrder))
            {
                result.Add($"Removed Form '{removedForm.Form.Name}'");
            }

            foreach (var oldForm in oldList.OrderBy(x => x.Form.DecPageOrder))
            {
                if (oldForm.Form.IsMandatory == true) continue;
                
                if (exemptedFormIdForChecking.Contains(oldForm.FormId)) continue;

                var newForm = newList.FirstOrDefault(x => x.FormId == oldForm.FormId);

                if ((oldForm.IsSelected ?? false) == false && (newForm.IsSelected ?? false) == false && newForm.FormId == "PhysicalDamageLimitOfInsurance") continue; // skip if not selected

                if (newForm != null)
                {
                    string tag = string.Empty;
                    if (oldForm.IsSelected == (newForm.IsSelected ?? false))
                    {
                        if (oldForm.Other == newForm.Other) continue;

                        var eachFormChanges = new List<string>();
                        switch (newForm.FormId)
                        {
                            case "ExclusionRadiusEndorsement":
                                var newExclusionRadiusEndorsement = JsonConvert.DeserializeObject<ExclusionRadiusDTO>(newForm.Other);
                                var oldExclusionRadiusEndorsement = JsonConvert.DeserializeObject<ExclusionRadiusDTO>(oldForm.Other);
                                eachFormChanges.AddRange(Compare(oldExclusionRadiusEndorsement, newExclusionRadiusEndorsement, nameof(ExclusionRadiusDTO), $"Form '{oldForm?.Form?.Name}' "));
                                break;
                            case "HiredPhysicalDamage":
                                var newHiredPhysicalDmg = JsonConvert.DeserializeObject<HiredPhysdamDTO>(newForm.Other);
                                var oldHiredPhysicalDmg = JsonConvert.DeserializeObject<HiredPhysdamDTO>(oldForm.Other);
                                eachFormChanges.AddRange(Compare(oldHiredPhysicalDmg, newHiredPhysicalDmg, nameof(HiredPhysdamDTO), $"Form '{oldForm?.Form?.Name}' "));
                                break;
                            case "DriverRequirements":
                                var newDriverRequirements = JsonConvert.DeserializeObject<DriverRequirementDTO>(newForm.Other);
                                var oldDriverRequirements = JsonConvert.DeserializeObject<DriverRequirementDTO>(oldForm.Other);
                                eachFormChanges.AddRange(Compare(oldDriverRequirements, newDriverRequirements, nameof(DriverRequirementDTO), $"Form '{oldForm?.Form?.Name}' "));
                                break;
                            case "TerritoryExclusion":
                                var newTerritoryExclusion = JsonConvert.DeserializeObject<TerritoryExclusionDTO>(newForm.Other);
                                var oldTerritoryExclusion = JsonConvert.DeserializeObject<TerritoryExclusionDTO>(oldForm.Other);
                                eachFormChanges.AddRange(Compare(oldTerritoryExclusion, newTerritoryExclusion, nameof(TerritoryExclusionDTO), $"Form '{oldForm?.Form?.Name}' "));
                                break;
                            case "TrailerInterchangeCoverage":
                                var newTrailerInterchangeCoverage = JsonConvert.DeserializeObject<TrailerInterchangeDTO>(newForm.Other);
                                var oldTrailerInterchangeCoverage = JsonConvert.DeserializeObject<TrailerInterchangeDTO>(oldForm.Other);
                                eachFormChanges.AddRange(Compare(oldTrailerInterchangeCoverage, newTrailerInterchangeCoverage, nameof(TrailerInterchangeDTO), $"Form '{oldForm?.Form?.Name}' "));
                                break;
                            default:
                                break;
                        }

                        result.AddRange(eachFormChanges);
                        continue;
                    }
                    if ((oldForm.IsSelected ?? false) && !(newForm.IsSelected ?? false)) tag = "Removed";
                    if (!(oldForm.IsSelected ?? false) && (newForm.IsSelected ?? false)) tag = "Added";

                    result.Add($"{tag} Form '{oldForm?.Form?.Name}'");
                }
            }

            return result;
        }

        private string FormatPropertyName(string propertyName)
        {
            return FieldLabels.Labels.ContainsKey(propertyName) ? FieldLabels.Labels.GetValueOrDefault(propertyName) : propertyName;
        }

        private string GetHistoryYear(int historyYear)
        {
            string label = string.Empty;
            switch (historyYear)
            {
                case 0:
                    label = "Current/Expiring";
                    break;
                case 1:
                    label = "1st Prior";
                    break;
                case 2:
                    label = "2nd Prior";
                    break;
                case 3:
                    label = "3rd Prior";
                    break;
                case 4:
                    label = "4th Prior";
                    break;
                default:
                    break;
            }

            return label;
        }

        private string FormatValues(string propertyName, object value)
        {
            string formattedValues = $"{value}";

            var grossVehicleWeight = new Dictionary<string, string>()
            {
                { "1", "1 - 10,000" },
                { "2", "10,001 - 20,000" },
                { "3", "20,001 - 45,000" },
                { "4", "45,001 +" },
            };

            var grossVehicleWeightCategory = new Dictionary<string, string>()
            {
                { "1", "Light" },
                { "2", "Medium" },
                { "3", "Heavy" },
                { "4", "Extra Heavy (Including tractors)" },
            };

            var vehicleDescription = new Dictionary<string, string>()
            {
                { "38", "Delivery Van>10k GVW" },
                { "16", "Dry Freight Trailer" },
                { "21", "Dump Body Trailer" },
                { "2", "Dump Truck 0-16k GVW" },
                { "66", "Dump Truck 33-45k GVW" },
                { "65", "Dump Truck >45k GVW" },
                { "8", "Full size Van" },
                { "60", "Garbage Truck 0-45 GVW" },
                { "68", "Garbage Truck >45 GVW" },
                { "5", "Pickup<= 1/2 Ton 4x2" },
                { "6", "Pickup<= 1/2 Ton 4x4" },
                { "7", "Pickup >  1/2 Ton 4x4" },
                { "34", "Private Passenger Auto" },
                { "22", "Refrigerated Dry Freight Trailer" },
                { "44", "Refrigerated Truck 0-16k GVW" },
                { "45", "Refrigerated Truck 16-26k GVW" },
                { "46", "Refrigerated Truck >26k GVW" },
                { "37", "Step Van 0-10k GVW" },
                { "41", "Straight Truck 0-16k GVW" },
                { "42", "Straight Truck 16-26k GVW" },
                { "43", "Straight Truck >26K GVW" },
                { "17", "Tank Trailer" },
                { "1", "Tractor" }
            };

            var businessClass = new Dictionary<string, string>()
            {
                { "1", "Courier" },
                { "2", "Other For-Hire Trucking Operations" },
                { "3", "Debris Removal" },
                { "4", "Dirt, Sand & Gravel" },
                { "5", "Furniture & Home Furnishing Stores" },
                { "6", "Garbage & Trash" },
                { "7", "Household Movers" },
                { "8", "Machinery & Heavy Equipment" },
                { "9", "Scrap Metal, Scrap Auto & Recycling Services" },
            };

            var limitsSymbols = new Dictionary<string, string>()
            {
                { "1", "7, 8 & 9" },
                { "2", "7 & 9" },
                { "3", "2, 8 & 9" },
                { "4", "1) Any Auto" },
                { "5", "2) All Owned Autos" },
                { "6", "3) Owned Private Passenger Autos" },
                { "7", "4) Owned Autos Other Than Private Passenger" },
                { "8", "5) All Owned Autos Which Require No-Fault Coverage" },
                { "9", "6) Owned Autos Subject to Compulsory UM Law" },
                { "10", "7) Autos Specified on  Schedule" },
                { "11", "8) Hired Autos" },
                { "12", "9) Non-Owned Autos" },
            };

            var additionalInterestType = new Dictionary<string, string>()
            {
                { "1", "Additional Insured" },
                { "2", "Loss Payee" },
                { "3", "Breach of Warranty" },
                { "4", "Mortgagee" },
                { "5", "Co-Owner" },
                { "6", "Owner" },
                { "7", "Employee As Lessor" },
                { "8", "Registrant" },
                { "9", "Leaseback Owner" },
                { "10", "Trustee" },
                { "11", "Lienholder" },
                { "12", "Designated Insured" }
            };

            var useClass = new Dictionary<string, string>()
            {
                { "1", "Courier" },
                { "2", "Commercial" },
                { "3", "Service" },
            };

            var incidentTypes = new Dictionary<string, string>()
            {
                { "1", "Majors" },
                { "2", "DUI/DWI/Drug" },
                { "3", "Minors (No Speed)" },
                { "4", "At-Fault" },
                { "5", "Major License Issues (Foreign, Unverified)" },
                { "6", "Not At-Fault" },
                { "7", "Equipment" },
                { "8", "Speed" },
                { "9", "Other" },
            };

            var addressTypeList = new Dictionary<string, string>()
            {
                { "1", "Billing Address" },
                { "2", "Business Address" },
                { "3", "Garaging Address" },
                { "4", "Mailing Address" },
            };

            if (propertyName == "RiskAdditionalInterest.AdditionalInterestTypeId" && value != null)
            {
                formattedValues = additionalInterestType.GetValueOrDefault($"{value}");
            }

            if ((propertyName == "TerritoryExclusionDTO.States" ||
                propertyName == "Vehicle.State" ||
                propertyName == "Vehicle.RegisteredState" ||
                propertyName == "Address.State" ||
                propertyName == "Driver.State") && value != null)
            {
                var states = GetStatesList().Result;
                var evaluatedStates = value.ToString()
                                            .Split(',', StringSplitOptions.RemoveEmptyEntries)
                                            .Select(stateCode => (string)states.FirstOrDefault(state => state.StateCode == stateCode)?.FullStateName?.Replace("[E]", ""))
                                            .OrderBy(x => x).ToArray();
                formattedValues = string.Join(", ", evaluatedStates);
            }

            if (propertyName == "Address.AddressTypeId")
            {
                formattedValues = addressTypeList.GetValueOrDefault($"{value}");
            }

            if (propertyName == "RiskSpecificCommoditiesHauled.PercentageOfCarry")
            {
                formattedValues = $"{value:N1}%";
            }

            if (FieldLabels.Labels.ContainsKey(propertyName) && propertyName.StartsWith(nameof(RiskCoverage)))
            {
                switch (propertyName)
                {
                    case "RiskCoverage.AutoBILimitId":
                        formattedValues = _limitList.AutoBILimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.UMBILimitId":
                        formattedValues = _limitList.UMBILimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.UMPDLimitId":
                        formattedValues = _limitList.UMPDLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.UIMBILimitId":
                        formattedValues = _limitList.UIMBILimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.UIMPDLimitId":
                        formattedValues = _limitList.UIMPDLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.MedPayLimitId":
                        formattedValues = _limitList.MedPayLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.PIPLimitId":
                        formattedValues = _limitList.PipLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.LiabilityDeductibleId":
                        formattedValues = _limitList.LiabilityDeductibleLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.CompDeductibleId":
                        formattedValues = _limitList.ComprehensiveLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.CollDeductibleId":
                        formattedValues = _limitList.CollisionLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.FireDeductibleId":
                        formattedValues = _limitList.ComprehensiveLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.GLBILimitId":
                        formattedValues = _limitList.GlLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.CargoLimitId":
                        formattedValues = _limitList.CargoLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.RefCargoLimitId":
                        formattedValues = _limitList.RefLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "RiskCoverage.ALSymbolId":
                    case "RiskCoverage.UMBILimitSymbolId":
                    case "RiskCoverage.UMPDLimitSymbolId":
                    case "RiskCoverage.UIMBILimitSymbolId":
                    case "RiskCoverage.UIMPDLimitSymbolId":
                    case "RiskCoverage.MedPayLimitSymbolId":
                    case "RiskCoverage.PIPLimitSymbolId":
                        formattedValues = limitsSymbols.GetValueOrDefault($"{value}");
                        break;
                    default:
                        formattedValues = $"{value}";
                        break;
                }
            }

            if (FieldLabels.Labels.ContainsKey(propertyName) && propertyName.StartsWith(nameof(Vehicle)))
            {
                switch (propertyName)
                {
                    case "Vehicle.GrossVehicleWeightId":
                        formattedValues = grossVehicleWeight.GetValueOrDefault($"{value}");
                        break;
                    case "Vehicle.VehicleBusinessClassId":
                        formattedValues = businessClass.GetValueOrDefault($"{value}");
                        break;
                    case "Vehicle.UseClassId":
                        formattedValues = useClass.GetValueOrDefault($"{value}");
                        break;
                    case "Vehicle.VehicleDescriptionId":
                        formattedValues = vehicleDescription.GetValueOrDefault($"{value}");
                        break;
                    case "Vehicle.CoverageCollisionDeductibleId":
                        formattedValues = _limitList.CollisionLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "Vehicle.CoverageFireTheftDeductibleId":
                        formattedValues = _limitList.ComprehensiveLimits.FirstOrDefault(x => x.Id == int.Parse($"{value}"))?.LimitDisplay;
                        break;
                    case "Vehicle.StatedAmount":
                    case "Vehicle.AutoLiabilityPremium":
                    case "Vehicle.CargoPremium":
                    case "Vehicle.PhysicalDamagePremium":
                    case "Vehicle.TotalPremium":
                    case "Vehicle.CollisionPremium":
                    case "Vehicle.ComprehensivePremium":
                    case "Vehicle.RefrigerationPremium":
                        formattedValues = $"${value:N0}";
                        break;
                    case "Vehicle.IsCoverageFireTheft":
                    case "Vehicle.IsCoverageCollision":
                    case "Vehicle.HasCoverageCargo":
                    case "Vehicle.HasCoverageRefrigeration":
                        formattedValues = $"{value}";
                        break;
                    case "Vehicle.GrossVehicleWeightCategory":
                        formattedValues = grossVehicleWeightCategory.GetValueOrDefault($"{value}");
                        break;
                    case "Vehicle.State":
                    case "Vehicle.RegisteredState":
                        break;
                    case "Vehicle.GaragingAddressId":
                        var entityAddress = _entityAddressRepository.GetEntityAddressByAddressId(Guid.Parse(value.ToString())).Result;
                        if (entityAddress != null)
                        {
                            formattedValues = $"{entityAddress.Address.StreetAddress1}, {entityAddress.Address.City}, {entityAddress.Address.State}, {entityAddress.Address.ZipCode}";
                        }                        
                        break;
                    default:
                        formattedValues = $"{value}";
                        break;
                }
            }

            if (FieldLabels.Labels.ContainsKey(propertyName) && propertyName.StartsWith(nameof(RiskCoveragePremium)))
            {
                switch (propertyName)
                {
                    case "RiskCoveragePremium.AutoLiabilityPremium":
                    case "RiskCoveragePremium.CargoPremium":
                    case "RiskCoveragePremium.PhysicalDamagePremium":
                    case "RiskCoveragePremium.GeneralLiabilityPremium":
                    case "RiskCoveragePremium.Premium":
                    case "RiskCoveragePremium.RiskMgrFeeAL":
                    case "RiskCoveragePremium.RiskMgrFeePD":
                    case "RiskCoveragePremium.AdditionalInsuredPremium":
                    case "RiskCoveragePremium.PrimaryNonContributoryPremium":
                    case "RiskCoveragePremium.WaiverOfSubrogationPremium":
                    case "RiskCoveragePremium.CollisionPremium":
                    case "RiskCoveragePremium.ComprehensivePremium":
                    case "RiskCoveragePremium.HiredPhysicalDamage":
                    case "RiskCoveragePremium.TrailerInterchange":
                    case "RiskCoveragePremium.ALManuscriptPremium":
                    case "RiskCoveragePremium.PDManuscriptPremium":
                        formattedValues = string.Format("${0:N2}", value);
                        break;
                    default:
                        formattedValues = $"{value}";
                        break;
                }
            }

            if (FieldLabels.Labels.ContainsKey(propertyName) && (propertyName.StartsWith(nameof(RiskHistory)) || propertyName.StartsWith(nameof(ClaimsHistory))))
            {
                switch (propertyName)
                {
                    case "RiskHistory.LiabilityLimits":
                    case "RiskHistory.LiabilityDeductible":
                    case "RiskHistory.CollisionDeductible":
                    case "RiskHistory.ComprehensiveDeductible":
                    case "RiskHistory.CargoLimits":
                    case "RiskHistory.RefrigeratedCargoLimits":
                    case "RiskHistory.AutoLiabilityPremiumPerVehicle":
                    case "RiskHistory.PhysicalDamagePremiumPerVehicle":
                    case "RiskHistory.TotalPremiumPerVehicle":
                    case "RiskHistory.NumberOfPowerUnitsAL":
                    case "RiskHistory.NumberOfPowerUnitsAPD":
                    case "ClaimsHistory.BiPiPmpIncLossTotal":
                    case "ClaimsHistory.BiPiPmpIncLoss100KCap":
                    case "ClaimsHistory.PropertyDamageIncLoss":
                    case "ClaimsHistory.NetLoss":
                    case "ClaimsHistory.CargoNetLoss":
                    case "ClaimsHistory.GeneralLiabilityNetLoss":
                    case "ClaimsHistory.PhysicalDamageNetLoss":
                        formattedValues = string.Format("${0:N0}", value);
                        break;
                    default:
                        formattedValues = $"{value}";
                        break;
                }
            }

            if (FieldLabels.Labels.ContainsKey(propertyName) && propertyName.StartsWith(nameof(DriverIncidents)))
            {
                switch (propertyName)
                {
                    case "DriverIncidents.IncidentType":
                        formattedValues = incidentTypes.GetValueOrDefault($"{value}");
                        break;
                    case "DriverIncidents.IncidentDate":
                    case "DriverIncidents.ConvictionDate":
                        var date = (DateTime?)value;
                        formattedValues = (date.HasValue) ? $"{date:MM/dd/yyyy}" : "";
                        break;
                }
            }

            return formattedValues;
        }

        private bool IsFormSelected(Guid riskDetailId, string formId)
        {
            var riskForm = _riskFormRepository.FindAsync(x => x.FormId == formId && x.RiskDetailId == riskDetailId).Result;
            return riskForm.IsSelected ?? false;
        }

        private RiskCoveragePremium ValidateEndorsementPremiums(RiskCoverage riskCoverage)
        {
            var riskDetailId = riskCoverage.RiskDetailId;
            var riskCoveragePremium = riskCoverage.RiskCoveragePremium;

            // excluding Manuscript; checked separately
            riskCoveragePremium.HiredPhysicalDamage = (riskCoveragePremium.HiredPhysicalDamage != null && !IsFormSelected(riskDetailId, "HiredPhysicalDamage")) ? 0 : riskCoveragePremium.HiredPhysicalDamage;
            riskCoveragePremium.TrailerInterchange = (riskCoveragePremium.TrailerInterchange != null && !IsFormSelected(riskDetailId, "TrailerInterchangeCoverage")) ? 0 : riskCoveragePremium.TrailerInterchange;

            return riskCoveragePremium;
        }

        private static async Task<List<StateInfo>> GetStatesList()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(STATESERVICEURL);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<List<StateInfo>>(await response.Content.ReadAsStringAsync());
        }

        #endregion Compare Policy Details

        //For Additional Interests related forms effective dating
        public async Task<List<AdditionalInterestChange>> GetAdditionalInterestChanges(Guid currentRiskDetailId, Guid riskId)
        {
            var previousRiskDetails = await _riskDetailRepository.GetAsync(riskDetail => riskDetail.RiskId == riskId && riskDetail.Id != currentRiskDetailId && riskDetail.Status != RiskDetailStatus.Canceled);
            var previousRiskDetailId = previousRiskDetails.OrderByDescending(riskDetail => riskDetail.UpdatedDate).Select(riskDetail => riskDetail.Id).First();

            var currentRiskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(currentRiskDetailId);
            await _riskDetailRepository.GetOneIncludeByIdAsync(previousRiskDetailId);

            await _bindingService.GetBindingByRiskIdAsync(riskId);
            var businessAddress = _entityAddressRepository.FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business && x.EntityId == currentRiskDetail.Insured.EntityId, i => i.Address).Result;
            _limitList = _limitsService.GetPerStateAsync(businessAddress.Address?.State).Result;

            var changes = new List<AdditionalInterestChange>();

            var previousAdditionalInterest = await _riskAdditionalInterestRepository.GetAsync(x => x.RiskDetailId == previousRiskDetailId, i => i.Entity);
            var currentAdditionalInterest = await _riskAdditionalInterestRepository.GetAsync(x => x.RiskDetailId == currentRiskDetailId, i => i.Entity);
            changes.AddRange(CompareAdditionalInterestList(previousAdditionalInterest.AsQueryable(), currentAdditionalInterest.AsQueryable()));

            return changes;
        }

        private IEnumerable<AdditionalInterestChange> CompareAdditionalInterestList(IQueryable<RiskAdditionalInterest> oldList, IQueryable<RiskAdditionalInterest> newList)
        {
            var result = new List<AdditionalInterestChange>();
            var addedInterests = newList.Where(newObj => oldList.All(oldObj => oldObj.AddProcessDate != newObj.AddProcessDate));

            foreach (var interest in addedInterests)
            {
                var interestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                //var interestAddress = $"{interestEntityAddress.Address.StreetAddress1}, {interestEntityAddress.Address.City}, {interestEntityAddress.Address.State}, {interestEntityAddress.Address.ZipCode}";
                //result.Add($"Added Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", interest.AdditionalInterestTypeId)}': '{interest.Entity.FullName}', '{interestAddress}'");
                var change = new AdditionalInterestChange { Id = interest.Id, ChangedDate = DateTime.Now };
                result.Add(change);
            }

            foreach (var oldInterest in oldList)
            {
                var interest = newList.FirstOrDefault(newObj => newObj.AddProcessDate.Ticks == oldInterest.AddProcessDate.Ticks);
                if (interest is null) continue;
                //var interestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                //string interestAddress = $"{interestEntityAddress.Address.StreetAddress1}, {interestEntityAddress.Address.City}, {interestEntityAddress.Address.State}, {interestEntityAddress.Address.ZipCode}";
                if (interest.RemoveProcessDate != null)
                {
                    //result.Add($"Removed Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", interest.AdditionalInterestTypeId)}': '{interest.Entity.FullName}', '{interestAddress}'");
                    var change = new AdditionalInterestChange { Id = interest.Id, ChangedDate = DateTime.Now };
                    result.Add(change);
                }
                else
                {
                    var oldInterestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == oldInterest.EntityId, e => e.Address).Result;
                    var currentInterestEntityAddress = _entityAddressRepository.FindAsync(x => x.EntityId == interest.EntityId, e => e.Address).Result;
                    //string oldInterestAddressStr = $"{oldInterestEntityAddress.Address.StreetAddress1}, {oldInterestEntityAddress.Address.City}, {oldInterestEntityAddress.Address.State}, {oldInterestEntityAddress.Address.ZipCode}";
                    //result.AddRange(Compare(oldInterest.Entity, interest.Entity, nameof(Entity), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));
                    //result.AddRange(Compare(oldInterestEntityAddress.Address, currentInterestEntityAddress.Address, nameof(Address), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));
                    //result.AddRange(Compare(oldInterest, interest, nameof(RiskAdditionalInterest), $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': "));

                    var change1 = Compare(oldInterest.Entity, interest.Entity, nameof(Entity),
                        $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': ");
                    var change2 = Compare(oldInterestEntityAddress.Address, currentInterestEntityAddress.Address,
                        nameof(Address),
                        $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': ");

                    var change3 = Compare(oldInterest, interest, nameof(RiskAdditionalInterest),
                        $"Additional Interest '{FormatValues("RiskAdditionalInterest.AdditionalInterestTypeId", oldInterest.AdditionalInterestTypeId)}' '{oldInterest.Entity.FullName}': ");

                    if (!change1.Any() && !change2.Any() && !change3.Any()) continue;
                    var change = new AdditionalInterestChange { Id = interest.Id, ChangedDate = DateTime.Now };
                    result.Add(change);
                }
            }

            return result;
        }
    }
}
