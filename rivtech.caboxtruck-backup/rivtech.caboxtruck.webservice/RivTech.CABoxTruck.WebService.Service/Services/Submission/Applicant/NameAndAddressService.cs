﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Constants;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Service.IServices.Submission;
using RivTech.CABoxTruck.WebService.Service.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services.Submission.Applicant
{
    public class NameAndAddressService : INameAndAddressService
    {
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IRiskSequenceService _riskSequence;
        private readonly IRiskRepository _riskRepository;
        private readonly IBrokerInfoService _brokerInfoService;
        private readonly IMapper _mapper;

        public NameAndAddressService(IRiskDetailRepository riskRepository,
            IAddressRepository addressRepository,
            IEntityAddressRepository entityAddressRepository,
            IRiskSequenceService riskSequence,
            IRiskRepository riskRepository1,
            IBrokerInfoService brokerInfoService,
            IMapper mapper)
        {
            _riskDetailRepository = riskRepository;
            _entityAddressRepository = entityAddressRepository;
            _riskSequence = riskSequence;
            _riskRepository = riskRepository1;
            _addressRepository = addressRepository;
            _brokerInfoService = brokerInfoService;
            _mapper = mapper;
        }

        public async Task<NameAndAddressDTO> GetNameAndAddress(Guid id)
        {
            //Get Risk and AddressList
            var risk = await _riskDetailRepository.GetInsuredAsync(id);
            var entityAddresses = await _entityAddressRepository.GetAsync(x => x.EntityId == risk.Insured.EntityId && x.AddressTypeId != null, 
                                    i => i.Address);

            //Container for Business Address
            var businessAddress = new AddressDTO();
            var entityAddressDtos = new List<EntityAddressDTO>();
            foreach(var item in entityAddresses) //Iterate IEnumerable. Better if this is a List to have linq or lambda features
            {
                //Get Business Address
                if(item.AddressTypeId == (short)AddressTypesEnum.Business)
                {
                    businessAddress.StreetAddress1 = item.Address.StreetAddress1;
                    businessAddress.ZipCode = item.Address.ZipCode;
                    businessAddress.City = item.Address.City;
                    businessAddress.State = item.Address.State;
                }

                //Collect Addresses
                entityAddressDtos.Add(new EntityAddressDTO()
                {
                    Id = item.Id,
                    AddressTypeId = item.AddressTypeId,
                    AddressId = item.AddressId,
                    Address = new AddressDTO()
                    {
                        Id = item.Address.Id,
                        StreetAddress1 = item.Address.StreetAddress1,
                        ZipCode = item.Address.ZipCode,
                        City = item.Address.City,
                        State = item.Address.State,
                        County = item.Address.County,
                        IsMainGarage = item.Address.IsMainGarage,
                    }
                });
            }

            var primaryCityAddress = await _entityAddressRepository.FindAsync(x => x.EntityId == risk.Insured.EntityId && x.AddressTypeId == null,
                                    i => i.Address);

            return new NameAndAddressDTO()
            {
                RiskDetailId = risk.Id,
                SubmissionNumber = risk.SubmissionNumber,
                BusinessName = risk.Insured.Entity.CompanyName,
                InsuredDBA = risk.Insured.Entity.DBA,
                BusinessPrincipal = risk.Insured.Entity.BusinessPrincipal,
                Email = risk.Insured.Entity.WorkEmailAddress,
                PhoneExt = risk.Insured.Entity.WorkPhoneExtension,
                BusinessAddress = businessAddress.StreetAddress1,
                City = businessAddress.City,
                ZipCode = businessAddress.ZipCode,
                State = businessAddress.State,
                CityOperations = primaryCityAddress?.Address?.City,
                StateOperations = primaryCityAddress?.Address?.State,
                ZipCodeOperations = primaryCityAddress?.Address?.ZipCode,
                Addresses = entityAddressDtos,
            };
        }

        public async Task<AddressDTO> GetAddress(Guid id)
        {
            var address = await _addressRepository.FindAsync(x => x.Id == id);
            return _mapper.Map<AddressDTO>(address);
        }

        public async Task<NameAndAddressDTO> InsertAsync(NameAndAddressDTO model)
        {
            var entityId = Guid.NewGuid();
            model.RiskDetailId = Guid.NewGuid();
            var tempEffectiveDate = DateTime.Now;

            var risk = new Data.Entity.Submission.Risk
            {
                QuoteNumber = _riskSequence.GenerateQuoteNumber(),
                SubmissionNumber = _riskSequence.GenerateSubmissionNumber(),
                Status = RiskStatus.Pending,
                RiskDetails = new List<RiskDetail>()
            };

            risk.RiskDetails.Add(new RiskDetail()
            {
                Id = model.RiskDetailId.Value,
                SubmissionNumber = risk.SubmissionNumber,
                Status = RiskDetailStatus.Received,
                AssignedToId = 0,
                Insured = new Insured()
                {
                    Id = Guid.NewGuid(),
                    Entity = new Entity()
                    {
                        Id = entityId,
                        CompanyName = model.BusinessName,
                        DBA = model.InsuredDBA.NullIfEmpty(),
                        BusinessPrincipal = model.BusinessPrincipal.NullIfEmpty(),
                        WorkEmailAddress = model.Email.NullIfEmpty(),
                        PersonalEmailAddress = model.Email.NullIfEmpty(),
                        WorkPhoneExtension = model.PhoneExt.NullIfEmpty()
                    }
                }
            });

            var newRisk = await _riskRepository.AddAsync(risk);
            model.SubmissionNumber = newRisk.SubmissionNumber;
            model.PolicyNumber = newRisk.PolicyNumber;
            model.RiskId = newRisk.Id;

            //PrimaryCityOfOperations Address
            await _entityAddressRepository.AddAsync(new EntityAddress()
            {
                Id = Guid.NewGuid(),
                EntityId = entityId,
                Address = new Address()
                {
                    Id = Guid.NewGuid(),
                    ZipCode = model.ZipCodeOperations,
                    City = model.CityOperations,
                    State = model.StateOperations
                }
            });

            model.Addresses.ForEach(x =>
            {
                x.Id = Guid.NewGuid();
                x.Address.Id = Guid.NewGuid();
            });

            var addresses = model.Addresses.Select(item => new EntityAddress
            {
                Id = item.Id.Value,
                EntityId = entityId,
                AddressTypeId = item.AddressTypeId,
                Address = new Address()
                {
                    Id = item.Address.Id,
                    StreetAddress1 = item.Address.StreetAddress1,
                    ZipCode = item.Address.ZipCode,
                    City = item.Address.City,
                    State = item.Address.State,
                    County = item.Address.County,
                    IsMainGarage = item.Address.IsMainGarage
                }

            });
            await _entityAddressRepository.AddRangeAsync(addresses);

            await _riskRepository.SaveChangesAsync();
            await _entityAddressRepository.SaveChangesAsync();

            return model;
        }

        public async Task<NameAndAddressDTO> UpdateAsync(NameAndAddressDTO model)
        {
            //Get Risk and AddressList
            var riskDetail = await _riskDetailRepository.GetInsuredAsync(model.RiskDetailId.Value);
            var entityAddresses = await _entityAddressRepository.GetAsync(x => x.EntityId == riskDetail.Insured.EntityId,
                                    i => i.Address);
            foreach(var item in entityAddresses)
            {
                _addressRepository.Remove(item.Address);
            }
            var result = _addressRepository.SaveChangesAsync().Result;

            // Update policyNumber 
            var risk = await _riskRepository.FindAsync(x => x.Id == riskDetail.RiskId);
            if (risk.PolicyNumber != null)
            {
                var brokerInfo = await _brokerInfoService.GetByRiskDetailIdAsync(riskDetail.RiskId);
                risk.PolicyNumber = _riskSequence.UpdatePolicyNumber(risk.PolicyNumber, brokerInfo.ExpirationDate.Value, brokerInfo.EffectiveDate.Value, model.StateOperations);
            }
            model.PolicyNumber = risk.PolicyNumber;

            //risk.PrimaryCityZipCodeOfOperationsId = model.PrimaryCityZipCodeOfOperationsId;
            riskDetail.Insured.Entity.CompanyName = model.BusinessName;
            riskDetail.Insured.Entity.DBA = StringExtensions.NullIfEmpty(model.InsuredDBA);
            riskDetail.Insured.Entity.BusinessPrincipal = StringExtensions.NullIfEmpty(model.BusinessPrincipal);
            riskDetail.Insured.Entity.WorkPhoneExtension = StringExtensions.NullIfEmpty(model.PhoneExt);
            riskDetail.Insured.Entity.WorkEmailAddress = StringExtensions.NullIfEmpty(model.Email);
            riskDetail.Insured.Entity.PersonalEmailAddress = StringExtensions.NullIfEmpty(model.Email);

            //PrimaryCityOfOperations Address
            await _entityAddressRepository.AddAsync(new Data.Entity.EntityAddress()
            {
                Id = Guid.NewGuid(),
                EntityId = riskDetail.Insured.EntityId,
                CreatedBy = 1, //temp
                Address = new Data.Entity.Address()
                {
                    Id = Guid.NewGuid(),
                    ZipCode = model.ZipCodeOperations,
                    City = model.CityOperations,
                    State = model.StateOperations,
                    UniqueId = Guid.NewGuid()
                }
            });

            //Save EntityAddresses
            foreach (var item in model.Addresses)
            {
                var id = item.Id.HasValue ? item.Id.Value : Guid.NewGuid();
                await _entityAddressRepository.AddAsync(new Data.Entity.EntityAddress()
                {
                    Id = id,
                    EntityId = riskDetail.Insured.EntityId,
                    AddressTypeId = item.AddressTypeId,
                    CreatedBy = 1, //temp
                    Address = new Data.Entity.Address()
                    {
                        Id = id,
                        StreetAddress1 = item.Address.StreetAddress1,
                        ZipCode = item.Address.ZipCode,
                        City = item.Address.City,
                        State = item.Address.State,
                        County = item.Address.County,
                        IsMainGarage = item.Address.IsMainGarage,
                        UniqueId = Guid.NewGuid()
                    }
                });
            }

            await _riskRepository.SaveChangesAsync();
            await _riskDetailRepository.SaveChangesAsync();
            await _entityAddressRepository.SaveChangesAsync();

            return model;
        }

        public async Task<NameAndAddressDTO> UpdateMidtermAsync(NameAndAddressDTO model)
        {
            //Get Risk and AddressList
            var risk = await _riskDetailRepository.GetInsuredAsync(model.RiskDetailId.Value);
            var entityAddress = await _entityAddressRepository.GetAsync(x => x.EntityId == risk.Insured.EntityId, i => i.Address);

            foreach (var item in entityAddress)
            {
                var addr = model.Addresses.FirstOrDefault(x => x.AddressId == item.AddressId);
                if (addr == null) continue;
                item.AddressTypeId = addr.AddressTypeId;
                item.Address.IsMainGarage = addr.Address.IsMainGarage;
                item.Address.StreetAddress1 = addr.Address.StreetAddress1;
                item.Address.City = addr.Address.City;
                item.Address.State = addr.Address.State;
                item.Address.ZipCode = addr.Address.ZipCode;
                _addressRepository.Update(item.Address);
            }
            var result = _addressRepository.SaveChangesAsync().Result;

            //Save EntityAddresses
            //var newAddr = model.Addresses.Where(x => entityAddress.All(current => current.AddressId != x.AddressId));
            //var newAddresses = new List<EntityAddress>();
            //foreach (var addr in newAddr)
            //{
            //    var id = addr.Id.HasValue ? addr.Id.Value : Guid.NewGuid();
            //    var newEntityAddress = new Data.Entity.EntityAddress()
            //    {
            //        Id = id,
            //        EntityId = risk.Insured.EntityId,
            //        AddressTypeId = addr.AddressTypeId,
            //        CreatedBy = 1, //temp
            //        Address = new Data.Entity.Address()
            //        {
            //            Id = id,
            //            StreetAddress1 = addr.Address.StreetAddress1,
            //            ZipCode = addr.Address.ZipCode,
            //            City = addr.Address.City,
            //            State = addr.Address.State,
            //            County = addr.Address.County,
            //            IsMainGarage = addr.Address.IsMainGarage
            //        }
            //    };
            //    newAddresses.Add(newEntityAddress);
            //}

            //await _entityAddressRepository.AddRangeAsync(newAddresses);

            // update only business name
            risk.Insured.Entity.CompanyName = model.BusinessName;

            await _riskDetailRepository.SaveChangesAsync();
            await _entityAddressRepository.SaveChangesAsync();

            return model;
        }

        public async Task RemoveAddressAsync(Guid id)
        {
            //Get Risk and AddressList
            var entityAddress = await _entityAddressRepository.GetAsync(x => x.AddressId == id, i => i.Address);

            _addressRepository.Remove(entityAddress.FirstOrDefault().Address);
            _entityAddressRepository.Remove(entityAddress.FirstOrDefault());

            var result = _addressRepository.SaveChangesAsync().Result;
            await _entityAddressRepository.SaveChangesAsync();
        }

        public async Task InsertEntityAddressAsync(EntityAddressDTO model)
        {
            var risk = await _riskDetailRepository.GetInsuredAsync(model.RiskDetailId);
            var id = Guid.NewGuid();
            var newAddress = _mapper.Map<EntityAddress>(model);
            newAddress.Id = id;
            newAddress.EntityId = risk.Insured.EntityId;
            newAddress.AddressTypeId = model.AddressTypeId;
            newAddress.CreatedBy = 1; //temp
            newAddress.Address.UniqueId = Guid.NewGuid();

            await _entityAddressRepository.AddAsync(newAddress);
            await _entityAddressRepository.SaveChangesAsync();
        }
    }
}
