﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity.Billing;
using RivTech.CABoxTruck.WebService.Data.Entity.Policy;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Billing;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.DTO.Billing.Taxes;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class TaxService : ITaxService
    {
        private readonly IMapper _mapper;
        private readonly IRiskDetailRepository _riskDetailRepository;
        private readonly IRiskPremiumFeesFactory _riskPremiumFeesRepository;
        private readonly IEntityAddressRepository _entityAddressRepository;
        private readonly IStateTaxRepository _stateTaxRepository;
        private readonly IBrokerInfoRepository _brokerInfoRepository;
        private readonly ITaxDetailsRepository _taxDetailsReposiotry;
        private readonly ITaxDetailsFactory _taxDetailsFactory;
        private readonly IBillingService _billingService;

        public TaxService(
            IMapper mapper,
            IRiskDetailRepository riskDetailRepository,
            IRiskPremiumFeesFactory riskPremiumFeesFactory,
            IEntityAddressRepository entityAddressRepository,
            IStateTaxRepository stateTaxRepository,
            IBrokerInfoRepository brokerInfoRepository,
            ITaxDetailsFactory taxDetailsFactory,
            ITaxDetailsRepository taxDetailsReposiotry,
            IBillingService billingService
        )
        {
            _mapper = mapper;
            _riskDetailRepository = riskDetailRepository;   
            _riskPremiumFeesRepository = riskPremiumFeesFactory;
            _entityAddressRepository = entityAddressRepository;
            _brokerInfoRepository = brokerInfoRepository;
            _stateTaxRepository = stateTaxRepository;
            _taxDetailsReposiotry = taxDetailsReposiotry;
            _taxDetailsFactory = taxDetailsFactory;
            _billingService = billingService;
        }


        public async Task CalculateRiskTaxAsync(Guid riskDetailId, int? optionId = null)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);

            var brokerInfo = await _brokerInfoRepository.GetByRiskIdAllAsync(riskDetail.RiskId);
            if (!brokerInfo.EffectiveDate.HasValue)
                throw new InvalidOperationException("Cannot calculate tax without effective date.");

            var businessAddress = await _entityAddressRepository
                .FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business
                                && x.EntityId == riskDetail.Insured.EntityId, i => i.Address);
            StateTax stateTax = _stateTaxRepository.Get(businessAddress.Address.State, brokerInfo.EffectiveDate.Value);

            foreach (var riskCov in riskDetail.RiskCoverages.Where(riskcoverages => riskcoverages.RiskCoveragePremium != null))
            {
                if (optionId.HasValue && riskCov.OptionNumber != optionId) continue;
                try
                {
                    List<TransactionDetailDTO> transactionDetails = new List<TransactionDetailDTO>();

                    if (riskCov.RiskCoveragePremium.AutoLiabilityPremium.HasValue && riskCov.RiskCoveragePremium.AutoLiabilityPremium.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.AutoLiabilityPremium.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleALP } });
                    }
                    if (riskCov.RiskCoveragePremium.PhysicalDamagePremium.HasValue && riskCov.RiskCoveragePremium.PhysicalDamagePremium.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.PhysicalDamagePremium.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehiclePDP } });
                    }
                    if (riskCov.RiskCoveragePremium.CargoPremium.HasValue && riskCov.RiskCoveragePremium.CargoPremium.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.CargoPremium.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleCP } });
                    }
                    if (riskCov.RiskCoveragePremium.GeneralLiabilityPremium.HasValue && riskCov.RiskCoveragePremium.GeneralLiabilityPremium.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.GeneralLiabilityPremium.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.GeneralLiabilityPremium } });
                    }
                    if (riskCov.RiskCoveragePremium.RiskMgrFeeAL.HasValue && riskCov.RiskCoveragePremium.RiskMgrFeeAL.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.RiskMgrFeeAL.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeeAL } });
                    }
                    if (riskCov.RiskCoveragePremium.RiskMgrFeePD.HasValue && riskCov.RiskCoveragePremium.RiskMgrFeePD.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.RiskMgrFeePD.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeePD } });
                    }
                    if (riskCov.RiskCoveragePremium.InstallmentFee.HasValue && riskCov.RiskCoveragePremium.InstallmentFee.Value > 0)
                    {
                        transactionDetails.Add(new TransactionDetailDTO { Amount = riskCov.RiskCoveragePremium.InstallmentFee.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.InstallmentFee } });
                    }

                    //Compute risk coverage tax from billing
                    TaxDetailsDTO taxDetails = _billingService.ProcessComputeRiskTax(riskDetail.RiskId, brokerInfo.EffectiveDate.Value, stateTax.StateCode, transactionDetails, riskCov.NumberOfInstallments).TaxDetails;

                    // Save tax details.
                    var taxDetailsData = _mapper.Map<TaxDetailsData>(taxDetails);
                    if(riskCov.RiskCoveragePremium.TaxDetailsId.HasValue)
                    {
                        var existingTaxDetails = await _taxDetailsReposiotry.FindAsync(riskCov.RiskCoveragePremium.TaxDetailsId.Value);
                        if(existingTaxDetails != null)
                            _taxDetailsReposiotry.Remove(existingTaxDetails);
                    }
                    taxDetailsData  = await _taxDetailsReposiotry.AddAsync(taxDetailsData);

                    // Assign tax details to risk coverage.
                    riskCov.RiskCoveragePremium.TaxDetailsId = taxDetailsData.Id;
                    riskCov.RiskCoveragePremium.TaxesAndStampingFees = taxDetails.TotalTax;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}");
                }
            }

            await _taxDetailsReposiotry.SaveChangesAsync();
            _riskDetailRepository.Update(riskDetail);
            await _riskDetailRepository.SaveChangesAsync();
        }

        public async Task<(TaxDetailsDTO, TaxDetailsDTO)> CalculateTaxPremiumChangesAsync(Guid riskDetailId, DateTime effectiveDate, decimal installment, List<EndorsementPendingChange> pendingChanges)
        {
            var riskDetail = await _riskDetailRepository.GetOneIncludeByIdAsync(riskDetailId);

            var businessAddress = await _entityAddressRepository
                .FindAsync(x => x.AddressTypeId == (short)AddressTypesEnum.Business
                                && x.EntityId == riskDetail.Insured.EntityId, i => i.Address);
            StateTax stateTax = _stateTaxRepository.Get(businessAddress.Address.State, effectiveDate);

            try
            {
                List<TransactionDetailDTO> transactionDetailsBefore = new List<TransactionDetailDTO>();
                List<TransactionDetailDTO> transactionDetailsAfter = new List<TransactionDetailDTO>();

                var alPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleALP ||
                                                                x.AmountSubTypeId == BillingConstants.ALManuscriptPremium ||
                                                                x.AmountSubTypeId == BillingConstants.AdditionalInsured || 
                                                                x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation ||
                                                                x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory).Sum(x => x.PremiumBeforeEndorsement);
                if (alPremiumBefore.HasValue && alPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = alPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleALP } });
                }
                var alPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleALP ||
                                                                x.AmountSubTypeId == BillingConstants.ALManuscriptPremium ||
                                                                x.AmountSubTypeId == BillingConstants.AdditionalInsured ||
                                                                x.AmountSubTypeId == BillingConstants.WaiverOfSubrogation ||
                                                                x.AmountSubTypeId == BillingConstants.PrimaryAndNoncontributory).Sum(x => x.PremiumAfterEndorsement);
                if (alPremiumAfter.HasValue && alPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = alPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleALP } });
                }

                var pdPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehiclePDP ||
                                                                x.AmountSubTypeId == BillingConstants.PDManuscriptPremium ||
                                                                x.AmountSubTypeId == BillingConstants.TrailerInterchangeEndst ||
                                                                x.AmountSubTypeId == BillingConstants.HiredPhysicalDamageEndst).Sum(x => x.PremiumBeforeEndorsement);
                if (pdPremiumBefore.HasValue && pdPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = pdPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehiclePDP } });
                }
                var pdPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehiclePDP ||
                                                                x.AmountSubTypeId == BillingConstants.PDManuscriptPremium ||
                                                                x.AmountSubTypeId == BillingConstants.TrailerInterchangeEndst ||
                                                                x.AmountSubTypeId == BillingConstants.HiredPhysicalDamageEndst).Sum(x => x.PremiumAfterEndorsement);
                if (pdPremiumAfter.HasValue && pdPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = pdPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehiclePDP } });
                }

                var cpPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleCP).Sum(x => x.PremiumBeforeEndorsement);
                if (cpPremiumBefore.HasValue && cpPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = cpPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleCP } });
                }
                var cpPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.VehicleCP).Sum(x => x.PremiumAfterEndorsement);
                if (cpPremiumAfter.HasValue && cpPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = cpPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.VehicleCP } });
                }

                var glPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.GeneralLiabilityPremium).Sum(x => x.PremiumBeforeEndorsement);
                if (glPremiumBefore.HasValue && glPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = glPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.GeneralLiabilityPremium } });
                }
                var glPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.GeneralLiabilityPremium).Sum(x => x.PremiumAfterEndorsement);
                if (glPremiumAfter.HasValue && glPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = glPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.GeneralLiabilityPremium } });
                }

                var sfalPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ServiceFeeAL).Sum(x => x.PremiumBeforeEndorsement);
                if (sfalPremiumBefore.HasValue && sfalPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = sfalPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeeAL } });
                }
                var sfalPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ServiceFeeAL).Sum(x => x.PremiumAfterEndorsement);
                if (sfalPremiumAfter.HasValue && sfalPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = sfalPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeeAL } });
                }

                var sfpdPremiumBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ServiceFeePD).Sum(x => x.PremiumBeforeEndorsement);
                if (sfpdPremiumBefore.HasValue && sfpdPremiumBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = sfpdPremiumBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeePD } });
                }
                var sfpdPremiumAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.ServiceFeePD).Sum(x => x.PremiumAfterEndorsement);
                if (sfpdPremiumAfter.HasValue && sfpdPremiumAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = sfpdPremiumAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.ServiceFeePD } });
                }

                var installmentFeeBefore = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.InstallmentFee).Sum(x => x.PremiumBeforeEndorsement);
                if (installmentFeeBefore.HasValue && installmentFeeBefore.Value > 0)
                {
                    transactionDetailsBefore.Add(new TransactionDetailDTO { Amount = installmentFeeBefore.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.InstallmentFee } });
                }
                var installmentFeeAfter = pendingChanges.Where(x => x.AmountSubTypeId == BillingConstants.InstallmentFee).Sum(x => x.PremiumAfterEndorsement);
                if (installmentFeeAfter.HasValue && installmentFeeAfter.Value > 0)
                {
                    transactionDetailsAfter.Add(new TransactionDetailDTO { Amount = installmentFeeAfter.Value, AmountSubType = new AmountSubTypeDTO { Id = BillingConstants.InstallmentFee } });
                }


                //Compute risk coverage tax from billing
                TaxDetailsDTO taxDetailsBefore = _billingService.ProcessComputeRiskTax(riskDetail.RiskId, effectiveDate, stateTax.StateCode, transactionDetailsBefore, installment).TaxDetails;
                TaxDetailsDTO taxDetailsAfter = _billingService.ProcessComputeRiskTax(riskDetail.RiskId, effectiveDate, stateTax.StateCode, transactionDetailsAfter, installment).TaxDetails;

                return (taxDetailsBefore, taxDetailsAfter);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}");
                return (null, null);
            }
        }

        public async Task<IEnumerable<TaxDetailsData>> GetTaxDetails(List<Guid> ids)
        {
            return await _taxDetailsReposiotry.GetAsync(td => ids.Contains(td.Id));
        }
    }
}
