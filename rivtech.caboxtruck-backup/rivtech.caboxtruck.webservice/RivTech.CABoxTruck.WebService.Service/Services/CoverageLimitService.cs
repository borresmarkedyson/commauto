﻿using AutoMapper;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class CoverageLimitService : ICoverageLimitService
    {
        private readonly IRiskCoverageRepository _riskCoverageRepository;
        private readonly ICoverageHistoryService _coverageHistoryService;
        private readonly IRiskFormRepository _riskFormRepository;
        private readonly IMapper _mapper;
        private Int16 optionNumber = 1;
        private List<int> _UMBISingleLimits = new List<int> { 314, 318, 319, 320, 321, 322 };
        private List<int> _UMBISplitLimits = new List<int> { 310, 311, 312, 313, 315, 316, 317 };

        public CoverageLimitService(IRiskCoverageRepository riskCoverageRepositoryy,
            ICoverageHistoryService coverageHistoryService,
            IRiskFormRepository riskFormRepository,
            IMapper mapper)
        {
            _riskCoverageRepository = riskCoverageRepositoryy;
            _coverageHistoryService = coverageHistoryService;
            _riskFormRepository = riskFormRepository;
            _mapper = mapper;
        }

        public async Task<List<RiskCoverageDTO>> GetAllAsync()
        {
            var result = await _riskCoverageRepository.GetAllAsync();
            return _mapper.Map<List<RiskCoverageDTO>>(result);
        }

        public async Task<RiskCoverageDTO> GetByIdAsync(Guid id)
        {
            var result = await _riskCoverageRepository.FindAsync(x => x.RiskDetailId == id && x.OptionNumber== 0);
            return _mapper.Map<RiskCoverageDTO>(result);
        }

        public async Task<bool> IsExistAsync(Guid id)
        {
            return await _riskCoverageRepository.AnyAsync(x => x.RiskDetailId == id);
        }

        public async Task<SaveRiskCoverageDTO> InsertAsync(SaveRiskCoverageDTO model)
        {
            model.OptionNumber = this.optionNumber;
            var result = await _riskCoverageRepository.AddAsync(_mapper.Map<RiskCoverage>(model));
            await _riskCoverageRepository.SaveChangesAsync();
            return model;
        }

        public async Task<SaveRiskCoverageDTO> UpdateAsync(SaveRiskCoverageDTO model)
        {
            var data = await _riskCoverageRepository.FindAsync(x => x.RiskDetailId == model.RiskDetailId && x.OptionNumber == model.OptionNumber);

            data.AutoBILimitId = model.AutoBILimitId;
            data.AutoPDLimitId = model.AutoPDLimitId;
            data.LiabilityDeductibleId = model.LiabilityDeductibleId;
            data.MedPayLimitId = model.MedPayLimitId;
            data.PIPLimitId = model.PIPLimitId;
            data.UMBILimitId = model.UMBILimitId;
            data.UMPDLimitId = model.UMPDLimitId;
            data.UIMBILimitId = model.UIMBILimitId;
            data.UIMPDLimitId = model.UIMPDLimitId;
            data.CoveredSymbolsId = model.CoveredSymbolsId;
            data.SpecifiedPerils = model.SpecifiedPerils;
            data.CompDeductibleId = model.CompDeductibleId;
            data.FireDeductibleId = model.FireDeductibleId;
            data.CollDeductibleId = model.CollDeductibleId;
            data.GLBILimitId = model.GLBILimitId;
            data.CargoLimitId = model.CargoLimitId;
            data.RefCargoLimitId = model.RefCargoLimitId;

            await _coverageHistoryService.DeleteFieldsAsync(model.RiskDetailId,
                    false,
                    false,
                    !data.CompDeductibleId.HasValue || data.CompDeductibleId.Value == 1,
                    !data.CollDeductibleId.HasValue || data.CollDeductibleId.Value == 1,
                    !data.CargoLimitId.HasValue || data.CargoLimitId.Value == 1,
                    !data.RefCargoLimitId.HasValue || data.RefCargoLimitId.Value == 1
                );

            data.ALSymbolId = model.ALSymbolId;
            data.PDSymbolId = model.PDSymbolId;
            data.LiabilityDeductibleSymbolId = model.LiabilityDeductibleSymbolId;
            data.MedPayLimitSymbolId = model.MedPayLimitSymbolId;
            data.PIPLimitSymbolId = model.PIPLimitSymbolId;
            data.UMBILimitSymbolId = model.UMBILimitSymbolId;
            data.UMPDLimitSymbolId = model.UMPDLimitSymbolId;
            data.UIMBILimitSymbolId = model.UIMBILimitSymbolId;
            data.UIMPDLimitSymbolId = model.UIMPDLimitSymbolId;
            data.CompDeductibleSymbolId = model.CompDeductibleSymbolId;
            data.FireDeductibleSymbolId = model.FireDeductibleSymbolId;
            data.CollDeductibleSymbolId = model.CollDeductibleSymbolId;
            data.GLBILimitSymbolId = model.GLBILimitSymbolId;
            data.CargoLimitSymbolId = model.CargoLimitSymbolId;
            data.RefCargoLimitSymbolId = model.RefCargoLimitSymbolId;

            data.EndorsementNumber = model.EndorsementNumber;
            data.OptionNumber = model.OptionNumber;
            data.NumAddtlInsured = model.NumAddtlInsured;
            data.WaiverSubrogation = model.WaiverSubrogation;
            data.PrimeNonContribEndorsement = model.PrimeNonContribEndorsement;

            var result = _riskCoverageRepository.Update(data);
            await _riskCoverageRepository.SaveChangesAsync();

            // Update UMBI form
            var umbiform = await _riskFormRepository.GetAsync(x => x.RiskDetailId == model.RiskDetailId &&
                                (x.FormId == "UninsuredMotoristCoverageBodilyInjury" || x.FormId == "SplitBodilyInjuryUninsuredMotoristCoverage"));
            if (umbiform != null)
            {
                foreach(var form in umbiform)
                {
                    form.IsSelected = false;
                    if (_UMBISingleLimits.Any(x => x == result.UMBILimitId) || _UMBISplitLimits.Any(x => x == result.UMBILimitId))
                        form.IsSelected = true;
                    _riskFormRepository.Update(form);
                }
                await _riskFormRepository.SaveChangesAsync();
            }

            // Update RefCargo form
            var refcargoform = await _riskFormRepository.FindAsync(x => x.RiskDetailId == model.RiskDetailId && x.FormId == "RefrigerationCargoEndorsement");
            if (refcargoform != null)
            {
                refcargoform.IsSelected = false;
                if (data.RefCargoLimitId.HasValue && data.RefCargoLimitId.Value != 157)
                    refcargoform.IsSelected = true;
                _riskFormRepository.Update(refcargoform);
                await _riskFormRepository.SaveChangesAsync();
            }

            return model;
        }

        public async Task<SaveRiskCoverageDTO> UpdateRatingFactorAsync(SaveRiskCoverageDTO model)
        {
            var data = await _riskCoverageRepository.FindAsync(x => x.RiskDetailId == model.RiskDetailId && x.OptionNumber == model.OptionNumber);

            data.LiabilityExperienceRatingFactor = model.LiabilityExperienceRatingFactor;
            data.LiabilityScheduleRatingFactor = model.LiabilityScheduleRatingFactor;

            var result = _riskCoverageRepository.Update(data);
            await _riskCoverageRepository.SaveChangesAsync();
            return model;
        }

        public async Task UpdateLimitDataAsync(SaveRiskCoverageDTO model)
        {
            var data = await _riskCoverageRepository.GetAsync(x => x.RiskDetailId == model.RiskDetailId);
            if (data != null)
            {
                foreach (var item in data)
                {
                    item.AutoBILimitId = model.AutoBILimitId;
                    item.AutoPDLimitId = model.AutoPDLimitId;
                    item.MedPayLimitId = model.MedPayLimitId;
                    item.PIPLimitId = model.PIPLimitId;
                    item.UMBILimitId = model.UMBILimitId;
                    item.UMPDLimitId = model.UMPDLimitId;
                    item.UIMBILimitId = model.UIMBILimitId;
                    item.UIMPDLimitId = model.UIMPDLimitId;

                    _riskCoverageRepository.Update(item);
                }
                await _riskCoverageRepository.SaveChangesAsync();
            }
        }
    }
}
