﻿using RivTech.CABoxTruck.WebService.DTO.User;
using RivTech.CABoxTruck.WebService.Service.IServices.User;
using System;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DTO.Constants;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IAuditLogRepository _auditLogRepository;

        public UserInfoService(IAuditLogRepository auditLogRepository)
        {
            _auditLogRepository = auditLogRepository;
        }

        public async Task<string> LoginAsync(LoginDto model)
        {
            await _auditLogRepository.AddAsync(new AuditLog()
            {
                UserId = Data.Common.Services.GetCurrentUser(),
                AuditType = AuditType.Login,
                Method = "LoginAsync",
                Description = $"{model.Username} has been successfully logged in",
                KeyID = model.Username
            });

            await _auditLogRepository.SaveChangesAsync();

            return "Ok";
        }

        public async Task<string> LogoutAsync(LoginDto model)
        {
            await _auditLogRepository.AddAsync(new AuditLog()
            {
                UserId = Data.Common.Services.GetCurrentUser(),
                AuditType = AuditType.Logout,
                Method = "LogoutAsync",
                Description = $"{model.Username} has been successfully logged out",
                KeyID = model.Username
            });

            await _auditLogRepository.SaveChangesAsync();
            return "Ok";
        }
    }
}
