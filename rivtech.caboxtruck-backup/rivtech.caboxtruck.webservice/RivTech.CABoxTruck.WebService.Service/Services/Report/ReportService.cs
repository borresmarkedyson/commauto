﻿using AutoMapper;
using Newtonsoft.Json;
using RivTech.CABoxTruck.WebService.Common;
using RivTech.CABoxTruck.WebService.DALDapper.IDataAccess;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Report.Filters;
using RivTech.CABoxTruck.WebService.Service.IServices.Report;
using RivTech.CABoxTruck.WebService.Service.Services.Report.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.Services.Report
{
    public class ReportService : IReportService
    {
        private readonly IMapper _mapper;
        private readonly IReportDataAccess _reportDataAccess;
        private readonly IReportGenerator _reportGenerator;

        public ReportService(IMapper mapper, IReportDataAccess reportDataAccess, IReportGenerator reportGenerator)
        {
            _mapper = mapper;
            _reportDataAccess = reportDataAccess;
            _reportGenerator = reportGenerator;
        }
        public async Task<Stream> GetReportStream(ReportManagementFilter reportManagementFilter)
        {
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                string reportContent = string.Empty;
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    FloatParseHandling = FloatParseHandling.Decimal
                };

                switch (reportManagementFilter.ReportName)
                {
                    case "DMVReport":
                        DMVReportHandler dMVReportHandler = new DMVReportHandler(_mapper, _reportDataAccess, _reportGenerator);
                        var dMVReportTemplates = await dMVReportHandler.Handler(reportManagementFilter);
                        reportContent = JsonConvert.SerializeObject(dMVReportTemplates, settings);
                        break;
                }

                _reportGenerator.GenerateReportToExcel(reportManagementFilter.ReportName, reportContent, memoryStream);
            }
            catch (Exception e)
            {
            }

            return memoryStream;

        }

        public async Task<List<RiskDTO>> GetAllRiskDetails()
        {
            var riskDetailModel = _reportDataAccess.GetAllRiskDetails();
            return _mapper.Map<List<RiskDTO>>(riskDetailModel);
        }
    }
}
