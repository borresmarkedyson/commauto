﻿using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.WebService.Service.IServices;
using RivTech.CABoxTruck.WebService.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.Service.IServices.Billing;
using RivTech.CABoxTruck.WebService.DTO.Billing.Fees;
using RivTech.CABoxTruck.WebService.DTO.Rater;
using RivTech.CABoxTruck.WebService.DTO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Web;
using RivTech.CABoxTruck.WebService.Service.Exceptions;
using System.Text;
using RivTech.CABoxTruck.WebService.Service.Services.Rater.Mapping;

namespace RivTech.CABoxTruck.WebService.Service.Services
{
    public class RaterService : IRaterService
    {
        private readonly IRiskService _riskService;
        private readonly IVehicleService _vehicleService;
        private readonly ICoverageHistoryService _coverageHistoryService;
        private readonly IApplicantAdditionalInterestService _applicantAdditionalInterestService;
        private readonly ILimitsService _limitsService;
        private readonly IBillingService _billingService;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IQuoteOptionsService _quoteOptionsService;
        private readonly IRaterInputMapperResolver _raterInputMappingResolver;

        public RaterService(IRiskService riskService,
            IVehicleService vehicleService,
            ICoverageHistoryService coverageHistoryService,
            IApplicantAdditionalInterestService riskAdditionalInterestService,
            IQuoteOptionsService quoteOptionsService,
            ILimitsService limitsService,
            IBillingService billingService,
            IHttpClientFactory httpClientFactory,
            IRaterInputMapperResolver raterInputMappingResolver
            )
        {
            _riskService = riskService;
            _vehicleService = vehicleService;
            _coverageHistoryService = coverageHistoryService;
            _applicantAdditionalInterestService = riskAdditionalInterestService;
            _quoteOptionsService = quoteOptionsService;
            _limitsService = limitsService;
            _billingService = billingService;
            _httpClientFactory = httpClientFactory;
            _raterInputMappingResolver = raterInputMappingResolver;
        }

        public async Task<dynamic> MapFields(Guid id, JObject requestBody, int optionNo = 1, bool isPremiumMapping = false, bool? fromPolicy = null)
        {
            int ADDITIONAL_INSURED_PREMIUM = 50;
            int WAIVER_OF_SUBROGATION_PREMIUM = 250;
            int PRIMARY_AND_NONCONTRIBUTORY_PREMIUM = 500;
            var getRiskDto = await _riskService.GetByIdIncludeAsync(id);
            var getLimitsList = await _limitsService.GetPerStateAsync(getRiskDto?.NameAndAddress?.State);
            var feeDetails = _billingService.GetFeeDetailsByState(getRiskDto?.NameAndAddress?.State);

            decimal RISK_MANAGEMENT_FEE_AL_SINGLE_UNIT = feeDetails?.FirstOrDefault(feeDetail => feeDetail.Description == FeeDetails.SingleALServiceFee)?.Amount ?? 0;
            decimal RISK_MANAGEMENT_FEE_AL_MULTIPLE_UNIT = feeDetails?.FirstOrDefault(feeDetail => feeDetail.Description == FeeDetails.MultipleALServiceFee)?.Amount ?? 0;
            decimal RISK_MANAGEMENT_FEE_PD_SINGLE_UNIT = feeDetails?.FirstOrDefault(feeDetail => feeDetail.Description == FeeDetails.SinglePDServiceFee)?.Amount ?? 0;
            decimal RISK_MANAGEMENT_FEE_PD_MULTIPLE_UNIT = feeDetails?.FirstOrDefault(feeDetail => feeDetail.Description == FeeDetails.MultiplePDServiceFee)?.Amount ?? 0;

            // can we call the vehicle from getRiskDto similar to driver?
            var veh = (await _vehicleService.GetByRiskDetailIdIncludeAsync(id))?
                    //.Where(vehicle => (vehicle.Options ?? "").Split(",").Any(opt => opt == $"{optionNo}"))
                    .OrderBy(vehicle => vehicle.VIN).ToList();

            var covHistory = await _coverageHistoryService.GetByIdAsync(id);
            var additionalInterest = await _applicantAdditionalInterestService.GetByRiskDetailIdActiveAsync(id);

            RiskCoverageDTO rskCov = getRiskDto?.RiskCoverages?.Where(x => x.OptionNumber == optionNo).FirstOrDefault();

            var drivers = getRiskDto?.Drivers?
                //.Where(driver => (driver.Options ?? "").Split(",").Any(opt => opt == $"{optionNo}"))
                .OrderBy(driver => driver.LicenseNumber).ToList();

            var idMapping = new Dictionary<int, Guid>();

            foreach (var x in requestBody)
            {
                string moduleName = x.Key;
                JToken value = x.Value;

                switch (moduleName)
                {
                    case "SubmissionNo":
                        {
                            break;
                        }
                    case "Applicant":
                        {
                            var bussnsDet = getRiskDto?.BusinessDetails;
                            string useClass = ParseListToValue(bussnsDet?.MainUseId, requestBody[moduleName]["BusinessDetails"]["UseClass"]);
                            string accountCategory = ParseListToValue(bussnsDet?.AccountCategoryUWId, requestBody[moduleName]["BusinessDetails"]["AccountCategory"]); //Account Category (UW)
                            string businessType = ParseListToValue(bussnsDet?.BusinessTypeId, requestBody[moduleName]["BusinessDetails"]["BusinessType"]);
                            string newVenture = ParseListToValue(bussnsDet?.NewVenturePreviousExperienceId, requestBody[moduleName]["BusinessDetails"]["NewVenturePreviousExperience"]);

                            requestBody[moduleName]["BusinessDetails"]["UseClass"] = useClass;
                            requestBody[moduleName]["BusinessDetails"]["AccountCategory"] = accountCategory;
                            requestBody[moduleName]["BusinessDetails"]["BusinessType"] = businessType;
                            requestBody[moduleName]["BusinessDetails"]["ZipCode"] = $"'{getRiskDto?.NameAndAddress?.ZipCode}"; // getRiskDto?.NameAndAddress?.ZipCode:D5
                            requestBody[moduleName]["BusinessDetails"]["OperatingZipCode"] = $"'{getRiskDto?.NameAndAddress?.ZipCodeOperations}"; // getRiskDto?.NameAndAddress?.ZipCodeOperations:D5

                            requestBody[moduleName]["BusinessDetails"]["YearsInBusiness"] = bussnsDet?.YearsInBusiness <= 0 ? newVenture : bussnsDet?.YearsInBusiness.ToString(); //autogenerated from ui
                            break;
                        }
                    case "Driver":
                        {
                            if (getRiskDto?.DriverHeader != null)
                            {
                                requestBody[moduleName]["UseDriverRatingTab"] = getRiskDto?.DriverHeader?.driverRatingTab.ToYesNo();
                                requestBody[moduleName]["UseAccidentsOrViolationInfo"] = getRiskDto?.DriverHeader?.accidentInfo.ToYesNo();
                                requestBody[moduleName]["KOUnder25"] = getRiskDto?.DriverHeader?.applyFilter?.Split(',').Any(x => x == "1").ToYesNo();
                                requestBody[moduleName]["KOAFOrMovingViolations"] = getRiskDto?.DriverHeader?.applyFilter?.Split(',').Any(x => x == "2").ToYesNo();
                            }

                            drivers = drivers.Where(x => ((x.Options ?? "").Split(',')).Contains(optionNo.ToString()) == true && x.IsValidated == true && x.IsValidatedKO == true).ToList();

                            if (drivers.Count > 0)
                            {
                                JArray myarray = new JArray();

                                foreach (var dvr in drivers)
                                {
                                    var jObject = new JObject
                                    {
                                        { "DriverID", dvr.Id },
                                        { "DateOfBirth", dvr.BirthDate?.ToString("MM/dd/yyyy") },
                                        { "Age", dvr.Age },
                                        { "OutOfState", dvr.IsOutOfState.ToYesNo() },
                                        { "YrsDrivingExperience", dvr.YrsDrivingExp ?? 0 },
                                        { "YrsCommercialDriverExperience", dvr.YrsCommDrivingExp },
                                        { "Majors", dvr.DriverIncidents.Any(x=>x.IncidentType == "1") ? "1" : "0" },
                                        { "Drug", dvr.DriverIncidents.Any(x=>x.IncidentType == "2")  ? "1" : "0"  },
                                        { "Minors", dvr.DriverIncidents.Any(x=>x.IncidentType == "3") ? "1" : "0"  },
                                        { "AtFault", dvr.DriverIncidents.Any(x=>x.IncidentType == "4") ? "1" : "0"  },
                                        { "MajorLicenseIssue", dvr.DriverIncidents.Any(x=>x.IncidentType == "5") ? "1" : "0"  },
                                        { "NAF", dvr.DriverIncidents.Any(x=>x.IncidentType == "6") ? "1" : "0"  },
                                        { "Equipment", dvr.DriverIncidents.Any(x=>x.IncidentType == "7") ? "1" : "0"  },
                                        { "Speed", dvr.DriverIncidents.Any(x=>x.IncidentType == "8") ? "1" : "0"  },
                                        { "Other", dvr.DriverIncidents.Any(x=>x.IncidentType == "9") ? "1" : "0"  },
                                    };

                                    myarray.Add(jObject);
                                }
                                requestBody[moduleName]["DriverList"] = myarray;
                            }

                            break;
                        }
                    case "Vehicle":
                        {

                            veh = veh.Where(x => ((x.Options ?? "").Split(',')).Contains(optionNo.ToString()) == true && x.IsValidated == true).ToList();

                            if (veh.Count > 0)
                            {
                                JArray myarray = new JArray();
                                int autoLiabilityUnits = veh.Count; // all vehicles are autoliability units
                                int physicalDamageUnits = 0;

                                requestBody[moduleName]["TotalVehicles"] = veh.Count;
                                bool isPDLimitsEnabled = (rskCov.CollDeductibleId ?? 65) != 65 || ((rskCov.CompDeductibleId ?? 76) != 76 || (rskCov.FireDeductibleId ?? 76) != 76);

                                var row = 1;
                                foreach (var _veh in veh)
                                {
                                    bool hasCovCollision = (_veh.IsCoverageCollision.HasValue && _veh.IsCoverageCollision.Value == true) && _veh.CoverageCollisionDeductibleId != 65;
                                    bool hasCovFireTheft = (_veh.IsCoverageFireTheft.HasValue && _veh.IsCoverageFireTheft.Value == true) && _veh.CoverageFireTheftDeductibleId != 76;

                                    if ((hasCovCollision || hasCovFireTheft) && isPDLimitsEnabled)
                                    {
                                        physicalDamageUnits++;
                                    }

                                    idMapping[row] = _veh.Id.Value;
                                    var jObject = new JObject
                                    {
                                        { "Number", row },
                                        { "VIN", _veh.VIN },
                                        { "Year", _veh.Year },
                                        { "VehicleType", _veh.VehicleType }, // { "VehicleType", parseListToValue(_veh.VehicleType, requestBody[moduleName]["VehicleDropdowns"]["VehicleType"])  },
                                        { "Description", ParseListToValue(_veh.VehicleDescriptionId, requestBody[moduleName]["VehicleDropdowns"]["Description"]) },
                                        { "CompDeductible", ParseListToValue(_veh.CoverageFireTheftDeductibleId ?? 76, requestBody[moduleName]["VehicleDropdowns"]["CompDeductible"]) },
                                        { "CollDeductible", ParseListToValue(_veh.CoverageCollisionDeductibleId ?? 65, requestBody[moduleName]["VehicleDropdowns"]["CollDeductible"]) },
                                        { "CollStateAmount", _veh.StatedAmount },
                                        { "IncCargoLimit", _veh.HasCoverageCargo.ToYesNo() },
                                        { "IncRefCargoLimit",  _veh.HasCoverageRefrigeration.ToYesNo() },
                                        { "UseClass", ParseListToValue(_veh.UseClassId, requestBody[moduleName]["VehicleDropdowns"]["UseClass"]) },
                                        { "BusinessClass", ParseListToValue(_veh.VehicleBusinessClassId, requestBody[moduleName]["VehicleDropdowns"]["BusinessClass"]) },
                                    };

                                    myarray.Add(jObject);
                                    row++;
                                }

                                decimal riskManagementFeeAL = (autoLiabilityUnits > 1) ? (autoLiabilityUnits * RISK_MANAGEMENT_FEE_AL_MULTIPLE_UNIT) : (autoLiabilityUnits == 1) ? RISK_MANAGEMENT_FEE_AL_SINGLE_UNIT : 0;
                                decimal riskManagementFeePD = (autoLiabilityUnits > 1) ? (physicalDamageUnits * RISK_MANAGEMENT_FEE_PD_MULTIPLE_UNIT) : (physicalDamageUnits == 1) ? RISK_MANAGEMENT_FEE_PD_SINGLE_UNIT : 0;
                                requestBody[moduleName]["RiskManagementFeeAL"] = riskManagementFeeAL;
                                requestBody[moduleName]["RiskManagementFeePD"] = riskManagementFeePD;
                                requestBody[moduleName]["VehicleList"] = myarray;

                            }

                            //removing unnecessary nodes.
                            var remooveVehicleDropdowns = (requestBody[moduleName] as JObject);
                            var token = remooveVehicleDropdowns.SelectToken("VehicleDropdowns");
                            if (token != null) token.Parent.Remove();

                            break;
                        }
                    case "AdditionalInterest":
                        {
                            bool IsBlanketCoverageAdditionalInsured = getRiskDto?.BlanketCoverage?.IsAdditionalInsured ?? false;
                            bool IsBlanketCoveragePrimaryAndNonContributory = getRiskDto?.BlanketCoverage?.IsPrimaryNonContributory ?? false;
                            bool IsBlanketCoverageWaiverOfSubrogation = getRiskDto?.BlanketCoverage?.IsWaiverOfSubrogation ?? false;

                            if (additionalInterest.Count() > 0)
                            {
                                requestBody[moduleName]["NoOfAdditionalInsured"] = additionalInterest.Count(x => x.AdditionalInterestTypeId == AdditionalInterestType.AdditionalInsured.Id);
                                requestBody[moduleName]["WaiverOfSubrogation"] = additionalInterest.Any(x => x.IsWaiverOfSubrogation == true).ToYesNo();
                                requestBody[moduleName]["PrimaryNonContributoryCoverage"] = additionalInterest.Any(x => x.IsPrimaryNonContributory == true).ToYesNo();
                                // additional interest premiums & blanket coverage
                                requestBody[moduleName]["AdditionalInsuredPremium"] = (IsBlanketCoverageAdditionalInsured) ? 500 : (((int)requestBody[moduleName]["NoOfAdditionalInsured"]) * ADDITIONAL_INSURED_PREMIUM);
                                requestBody[moduleName]["PrimaryNonContributoryPremium"] = (IsBlanketCoveragePrimaryAndNonContributory) ? 1500 : (additionalInterest.Where(x => x.IsPrimaryNonContributory == true).Count() * PRIMARY_AND_NONCONTRIBUTORY_PREMIUM);
                                requestBody[moduleName]["WaiverOfSubrogationPremium"] = (IsBlanketCoverageWaiverOfSubrogation) ? 1000 : (additionalInterest.Where(x => x.IsWaiverOfSubrogation == true).Count() * WAIVER_OF_SUBROGATION_PREMIUM);
                            }
                            else
                            {
                                requestBody[moduleName]["AdditionalInsuredPremium"] = (IsBlanketCoverageAdditionalInsured) ? 500 : 0;
                                requestBody[moduleName]["PrimaryNonContributoryPremium"] = (IsBlanketCoveragePrimaryAndNonContributory) ? 1500 : 0;
                                requestBody[moduleName]["WaiverOfSubrogationPremium"] = (IsBlanketCoverageWaiverOfSubrogation) ? 1000 : 0;
                            }
                            break;
                        }
                    case "Policy":
                    case "Broker":
                    case "HistoricalCoverage":
                    case "ClaimsHistory":
                    case "RiskSpecifics":
                    case "RequestedCoverages":
                        {
                            var inputMapping = _raterInputMappingResolver.Resolve(moduleName);
                            if(inputMapping != null)
                            {
                                await inputMapping.Map(getRiskDto, rskCov, requestBody); 
                            }
                            break;
                        }
                }

            }

            if (isPremiumMapping)
            {
                var vehicleIdMappingKey = Guid.NewGuid();
                PremiumRaterVehicleFactorsDTO.VehicleIdMappings.Add(vehicleIdMappingKey, idMapping);
                requestBody["VehicleIdMappingKey"] = vehicleIdMappingKey.ToString();
            }

            return requestBody;
        }

        public async Task<List<string>> GetOptions(Guid id)
        {
            var res = await _quoteOptionsService.GetRiskCoverageAndPremiumByIdAsync(id);
            return res.Select(s => s.OptionNumber.ToString()).ToList();
        }

        public dynamic ParseListToValue(dynamic dbVal, JToken node)
        {
            if (dbVal != null)
            {
                var val = node
                        ?.Where(x => (string)x["value"] == dbVal.ToString())
                        ?.Select(c => (string)c["label"]).FirstOrDefault();
                return SanitizeStringVal(val);
            }
            else return null;
        }

        private string SanitizeStringVal(string str)
        {
            return str?.Replace('\u00A0', ' ').Replace("$", "");
        }


        public async Task<bool> CopyRaterAsync(string submissionNumber, string newSubmissionNumber)
        {
            try
            {
                // Create content.
                var content = new
                {
                    From = submissionNumber,
                    To = newSubmissionNumber
                };
                var stringContent = new StringContent(JsonConvert.SerializeObject(content), System.Text.Encoding.UTF8, "application/json");

                // Send api request.
                using var httpClient = _httpClientFactory.CreateClient("rater");
                var endpoint = "api/ExperienceRater/CopyToNewSubmission";
                var response = await httpClient.PostAsync(endpoint, stringContent);

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.ReasonPhrase);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task DeleteArchive(string submissionNumber, int endorsementNumber)
        {
            var client = _httpClientFactory.CreateClient("rater");
            var qryParams = $"submissionNumber={submissionNumber}"
                            + $"&endorsementNumber={endorsementNumber}";
            var result = await client.DeleteAsync($"api/PremiumRaterArchive?{qryParams}");
            if (!result.IsSuccessStatusCode)
                Console.WriteLine(result.ReasonPhrase);
        }

        public async Task<string> PreloadPremiumRater(string submissionNumber, Guid riskDetailId, bool? fromPolicy)
        {
            //Guid _riskDetailId = Guid.Parse(riskDetailId);
            NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);
            var risk = await _riskService.GetByIdIncludeAsync(riskDetailId);
            var brokerinfo = risk.BrokerInfo;
            short yearsInBusiness = risk.BusinessDetails?.YearsInBusiness ?? 0;

            if (brokerinfo?.EffectiveDate == null)
            {
                throw new RaterException("Inception Date is Required.");
            }

            DateTime inceptionDate = brokerinfo.EffectiveDate.Value;

            short optionNum = 1;
            if (risk.Binding != null)
            {
                optionNum = risk.Binding.BindOptionId ?? 1;
            }
            var covPrem = risk.RiskCoverages.FirstOrDefault(c => c.OptionNumber == optionNum);

            if (covPrem?.LiabilityExperienceRatingFactor == null && covPrem?.LiabilityScheduleRatingFactor == null)
            {
                throw new RaterException("Liability Schedule RF and Liability Experience RF is not defined");
            }

            //var options = await _raterService.GetOptions(riskDetailId);
            //if (fromPolicy != true && options.Count < 1)
            //    return new JsonResult(new { status = false, message = "No Option Selected." }, new JsonSerializerSettings());

            // itemCount =  // total Number of pool item from ui
            string url = $"api/PremiumRater/preload/{submissionNumber}?inceptionDate={inceptionDate:yyyy-MM-dd}";
            if (fromPolicy == true)
                url += $"&fromPolicy={fromPolicy}";

            using var httpClient = _httpClientFactory.CreateClient("rater");
            var content = new StringContent(string.Empty, Encoding.UTF8, "application/json");
            var preloadResponse = await httpClient.PostAsync(url, content);

            var result = await preloadResponse.Content.ReadAsStringAsync();
            if (!preloadResponse.IsSuccessStatusCode)
            {
                if (preloadResponse.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    result = "Error occured from rater server";
                }
                throw new RaterException($"{result}. You may refresh your browser.");
            }

            return result;
        }
    }
}
