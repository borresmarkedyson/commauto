﻿using RivTech.CABoxTruck.WebService.DataProvider.IRepositories;
using RivTech.CABoxTruck.WebService.DataProvider.IRepositories.Submission;
using RivTech.CABoxTruck.WebService.DTO.Dashboard;
using RivTech.CABoxTruck.WebService.Service.IServices.Dashboard;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.Service.Exceptions;

namespace RivTech.CABoxTruck.WebService.Service.Services.Dashboard
{
    public class DashboardService : IDashboardService
    {
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IRiskRepository _riskRepository;

        public DashboardService(IDashboardRepository dashboardRepository, IRiskRepository riskRepository)
        {
            _dashboardRepository = dashboardRepository;
            _riskRepository = riskRepository;
        }

        public async Task<DashboardPolicySearchDTO> GetRiskByPolicyNumber(string policyNumber)
        {
            var result = await _riskRepository.GetRiskByPolicyNumberAsync(policyNumber);

            if (result == null)
            {
                throw new ModelNotFoundException();
            }

            return new DashboardPolicySearchDTO()
            {
                RiskId = result.Id,
                LatestRiskDetailId = result.RiskDetails.OrderByDescending(x => x.UpdatedDate).FirstOrDefault().Id
            };
        }

        public DashboardCountDto GetDashboardCount()
        {
            var results = _dashboardRepository.GetDashboardCount();
            var dashboardCount = new DashboardCountDto();
            foreach (var result in results)
            {
                switch (result.Status)
                {
                    case "Complete":
                        dashboardCount.CompleteCount = result.Count;
                        break;
                    case "Declined":
                        dashboardCount.DeclinedCount = result.Count;
                        break;
                    case "In Research":
                        dashboardCount.InResearchCount = result.Count;
                        break;
                    case "Incomplete":
                        dashboardCount.InCompleteCount = result.Count;
                        break;
                    case "Quoted":
                        dashboardCount.QuotedCount = result.Count;
                        break;
                    case "Received":
                        dashboardCount.ReceivedCount = result.Count;
                        break;
                    case "Withdrawn":
                        dashboardCount.WithdrawnCount = result.Count;
                        break;
                    case "Submission":
                        dashboardCount.SubmissionCount = result.Count;
                        break;
                    case "Active":
                        dashboardCount.ActivePoliciesCount = result.Count;
                        break;
                    case "Canceled":
                        dashboardCount.CancelledPoliciesCount = result.Count;
                        break;
                }
            }
            return dashboardCount;
        }
    }
}
