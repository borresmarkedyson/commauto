﻿using RivTech.CABoxTruck.WebService.DTO.User;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices.User
{
    public interface IUserInfoService
    {
        Task<string> LoginAsync(LoginDto model);
        Task<string> LogoutAsync(LoginDto model);
    }
}
