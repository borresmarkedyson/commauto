﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.DataSets;
using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Rater;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices
{
    public interface IVehicleService
    {
        Task<List<VehicleDTO>> GetAllAsync();
        Task<List<VehicleDTO>> GetByRiskDetailIdAsync(Guid id);

        Task<bool> IsExistAsync(Guid id);
        Task<VehicleDTO> GetByIdAsync(Guid id);
        Task<VehicleDTO> InsertAsync(VehicleDTO model);
        Task<List<VehicleDTO>> InsertRangeAsync(List<VehicleDTO> model);
        Task<VehicleDTO> UpdateAsync(VehicleDTO model);
        Task<List<VehicleDTO>> UpdateAddressesAsync(List<VehicleDTO> model);
        Task<List<VehicleDTO>> UpdateOtherCoveragesAsync(List<VehicleDTO> model);

        Task<List<VehicleDTO>> UpdateListOptionAsync(List<VehicleDTO> model);

        Task<string> RemoveAsync(Guid id, bool? fromEndorsement = false);

        Task<List<VehicleDTO>> GetByRiskDetailIdIncludeAsync(Guid id, bool? withDeleted = false);
        Task<List<VehicleDTO>> GetAllIncludeAsync();
        Task<List<VehicleDTO>> GetByIdIncludeAsync(Guid id);
        Task<List<VehicleDTO>> GetPreviousVehicles(Guid id);

        Task<List<VehicleDTO>> AddFromExcelFileAsync(Stream fileStream);

        Task<VehicleDTO> ReinstateVehicleAsync(Guid id);

        Task UpdatePremiums(Guid riskDetailId);
        Task UpdateVehicleEffectiveDateExpirationDate(Guid riskDetailId, DateTime effectiveDate, DateTime expirationDate);
        Task UpdateMainUseAsync(Guid riskDetailId, short mainUseId);
        Task UpdateAccountCategoryAsync(Guid riskDetailId, short value);
    }
}
