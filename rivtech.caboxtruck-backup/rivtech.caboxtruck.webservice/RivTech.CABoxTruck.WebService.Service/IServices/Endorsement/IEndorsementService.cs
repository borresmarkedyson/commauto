﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices
{
    public interface IEndorsementService
    {
        Task<EndorsementPremiumChangeSummaryDTO> GetPremiumChanges(Guid riskDetailId);
        Task CalculatePremiumChanges(Guid riskDetailId, DateTime effectiveDate, int optionNumber);
        Task UpdateEffectiveDate(Guid riskDetailId, DateTime effectiveDate);
        Task<string> GeneratePolicyNumber(Guid riskDetailId);
        Task<int> GetNextEndorsementNumber(Guid riskDetailId);
    }
}
