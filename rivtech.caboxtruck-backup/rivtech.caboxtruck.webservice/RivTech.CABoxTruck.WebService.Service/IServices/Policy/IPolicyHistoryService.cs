﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices
{
    public interface IPolicyHistoryService
    {
        Task<PolicyHistoryDTO> GetLastActiveByRiskId(Guid riskId);
        Task<List<PolicyHistoryDTO>> GetEndorsementHistoryByRiskDetailId(Guid riskDetailId);
        Task<List<PolicyHistoryDTO>> GetEndorsementHistoryByRiskId(Guid riskId);
        Task<int> CreatePolicyHistory(Guid policyDetailId, Guid previousRiskDetailId, Guid riskId, RiskCoveragePremium riskCovPrem, DateTime effectiveDate, string policyStatus, string details = null);
    }
}
