﻿using System;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.DTO;

namespace RivTech.CABoxTruck.WebService.Service.IServices.Submission
{
    public interface ITransactionService
    {
        Task<RiskDTO> IssueSubmission(Guid riskDetailId);
        Task<RiskDTO> ResetPolicyChanges(Guid riskDetailId);
        Task<System.Collections.Generic.List<string>> GetPolicyChanges(Guid riskDetailId);
        Task<RiskDTO> IssueEndorsement(Guid riskDetailId, DateTime effectiveDate, string endorsementText, string policyChanges);
        Task<RiskDTO> CancelPolicy(Guid riskDetailId, DateTime effectiveDate, string cancellationReasonType, bool isShortRatePremium, bool? isFlatCancel = null, bool isAutoCancel = false);
        Task<RiskDTO> ReinstatePolicy(Guid riskDetailId);
        Task<RiskDTO> RewritePolicyToSubmission(Guid riskDetailId, DateTime effectiveDate, bool? isFlatCancel = true);
        Task CancelPolicyForRewrite(Guid riskDetailId, DateTime effectiveDate, bool? isFlatCancel = true);
        Task AutoPolicyCancellation(DateTime effectiveDate);
        Task AutoNoticeOfCancellation(DateTime effectiveDate);
        Task AutoRescindNoticeOfCancellation(DateTime effectiveDate);
    }
}
