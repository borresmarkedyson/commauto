﻿using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Report;
using RivTech.CABoxTruck.WebService.DTO.Report.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices.Report
{
    public interface IReportService
    {
        Task<Stream> GetReportStream(ReportManagementFilter reportManagementFilter);
        Task<List<RiskDTO>> GetAllRiskDetails();
    }
}
