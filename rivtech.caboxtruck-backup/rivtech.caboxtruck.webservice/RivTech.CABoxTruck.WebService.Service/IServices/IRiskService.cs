﻿using RivTech.CABoxTruck.WebService.DTO;
using RivTech.CABoxTruck.WebService.DTO.Submission;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices
{
    public interface IRiskService
    {
        Task<bool> IsExistAsync(Guid id);
        Task<RiskDTO> InsertRiskAsync(RiskDTO model);
        Task<List<RiskDTO>> GetAllAsync();
        Task<RiskDTO> GetRiskAsync(Guid id);
        Task<string> UpdateRiskAsync(RiskDTO model);
        Task<bool> UpdateRiskStatusAsync(RiskDTO model);
        Task<bool> UpdateRiskStatusOnlyAsync(Guid riskDetailId, string status);
        Task<string> UpdatePendingEnrosementAsync(Guid riskDetailId);
        Task<string> RemoveRiskAsync(Guid id);
        Task<List<RiskDTO>> GetAllIncludeAsync();
        Task<RiskDTO> GetByIdIncludeAsync(Guid riskDetailId);

        Task<Guid> GetRiskDetailIdByStatusAsync(Guid riskId, string status);
        Task<PaymentPortalSearchResponseDTO> SearchRiskByPolicyAndZipCodeAsync(string policyNumber, string zipCode);
    }
}
