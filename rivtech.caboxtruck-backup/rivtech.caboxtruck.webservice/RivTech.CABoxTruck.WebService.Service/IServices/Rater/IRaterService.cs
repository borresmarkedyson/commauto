﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.WebService.Service.IServices
{
    public interface IRaterService
    {
        Task<dynamic> MapFields(Guid id, JObject requestBody, int optionNo = 1, bool isPremiumMapping = false, bool? fromPolicy = null);

        Task<List<string>> GetOptions(Guid id);

        dynamic ParseListToValue(dynamic dbVal, JToken node);
        Task<bool> CopyRaterAsync(string submissionNumber, string newSubmissionNumber);

        Task DeleteArchive(string submissionNumber, int endorsementNumber);
        Task<string> PreloadPremiumRater(string submissionNumber, Guid riskDetailId, bool? fromPolicy);
    }
}
