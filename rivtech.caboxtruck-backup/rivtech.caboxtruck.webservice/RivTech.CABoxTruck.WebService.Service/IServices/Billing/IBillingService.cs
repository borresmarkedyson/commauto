﻿using RivTech.CABoxTruck.WebService.Data.Entity;
using RivTech.CABoxTruck.WebService.Data.Entity.Policy;
using RivTech.CABoxTruck.WebService.DTO.Billing;
using RivTech.CABoxTruck.WebService.DTO.Billing.Fees;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RivTech.CABoxTruck.WebService.DTO;

namespace RivTech.CABoxTruck.WebService.Service.IServices.Billing
{
    public interface IBillingService
    {
        Task UpdatePaymentOptions(Guid riskDetailId, int? optionId = null);
        List<FeeDetails> GetFeeDetailsByState(string stateCode);

        BillingBindResponseDTO ProcessBillingBind(
            string policyNumber, 
            DateTime effectiveDate, 
            RiskDetail riskDetail, 
            Binding bindingDetails, 
            RiskCoverage riskCoverage, 
            List<Vehicle> vehicles, 
            string authorizeId = null);
        BillingBindResponseDTO ProcessBillingBindEndorsement(
            Guid riskId,
            DateTime effectiveDate,
            List<EndorsementPendingChange> pendingChange,
            string authorizeId = null);
        Task<string> AuthorizePayment(string policyNumber, DateTime effectiveDate, RiskDetail riskDetail, Binding bindingDetails, RiskCoverage riskCoverage, List<Vehicle> vehicles);
        BillingComputeRiskTaxResponseDTO ProcessComputeRiskTax(Guid riskId, DateTime effectiveDate, string stateCode, List<TransactionDetailDTO> transactionDetails, decimal? installmentCount);

        Task<List<InstallmentAndInvoiceDTO>> GetInstallmentSchedule(RiskDTO risk);
        Task<List<InstallmentAndInvoiceDTO>> GetEndorsementInstallmentSchedule(Guid riskId);
        Task<BillingCancellationDTO> ProcessBillingCancellation(Guid riskId, IEnumerable<CancellationBreakdown> breakdowns, bool isFlatCancel);
        Task ProcessBillingReinstatement(Guid riskId, IEnumerable<ReinstatementBreakdown> resinstatementBreakdowns);
        //void ProcessBillingCancellation();

        Task<List<Guid>> GetRisksForNoticeOfCancellation(DateTime effectiveDate);
    }
}
