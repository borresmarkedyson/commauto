﻿using System;

namespace RivTech.CABoxTruck.WebService.Service.Exceptions
{
    public class ObjectAlreadyExistsException : Exception
    {
        public ObjectAlreadyExistsException() : base(null)
        {
        }
    }
}
