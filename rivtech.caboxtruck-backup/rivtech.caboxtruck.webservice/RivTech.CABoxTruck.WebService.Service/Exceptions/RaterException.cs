﻿using System;

namespace RivTech.CABoxTruck.WebService.Service.Exceptions
{
    public class RaterException : Exception
    {
        public RaterException(string msg) : base(msg)
        {
        }
    }
}
