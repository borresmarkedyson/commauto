﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.WebService.Data.Common;
using RivTech.CABoxTruck.WebService.Data.Entity.Submission;

namespace RivTech.CABoxTruck.WebService.Data.EntityConfiguration.Submission
{
    public class RiskConfiguration : IEntityTypeConfiguration<Risk>
    {
        public void Configure(EntityTypeBuilder<Risk> builder)
        {
            builder.HasMany(x => x.RiskDetails).WithOne();
            builder.Property(e => e.QuoteNumber).HasColumnType("varchar(20)").HasMaxLength(20);
            builder.HasOne(x => x.BrokerInfo)
                .WithOne();
            builder.Property(e => e.Id).HasValueGenerator<PrimaryIdGenerator>();
            builder.Ignore(x => x.CurrentRiskDetail);

            builder.HasOne(x => x.Binding).WithOne();
            //builder.HasMany(x => x.BindingRequirements).WithOne();
            builder.Property(e => e.PolicyNumber).HasColumnType("varchar(30)").HasMaxLength(30);
            builder.Property(e => e.PolicySuffix).HasColumnType("varchar(20)").HasMaxLength(20);
        }
    }
}
