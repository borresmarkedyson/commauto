﻿using System;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public interface IUpdatedData
    {
        DateTime UpdatedDate { get; set; }
    }
}
