﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public interface IDeletedAt
    {
        DateTime DeletedAt { get; set; }
    }
}
