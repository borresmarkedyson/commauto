﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.WebService.Data.Entity.Submission
{
    public class Risk: BaseEntity<Guid>
    {
        public string SubmissionNumber { get; set; }
        public string QuoteNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicySuffix { get; set; }
        public string Status { get; set; }
        public DateTime? PendingCancellationDate { get; set; }
        public RiskDetail CurrentRiskDetail { get; set; }
        public List<RiskDetail> RiskDetails { get; set; }

        public BrokerInfo BrokerInfo { get; set; }

        //public List<BindingRequirements> BindingRequirements { get; set; }
        public Binding Binding { get; set; }
    }
}
