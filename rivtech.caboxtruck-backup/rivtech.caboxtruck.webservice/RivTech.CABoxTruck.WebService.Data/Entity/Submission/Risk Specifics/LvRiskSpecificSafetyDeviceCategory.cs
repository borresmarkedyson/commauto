﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.Data.Entity.Submission.Risk_Specifics
{
    public class LvRiskSpecificSafetyDeviceCategory : Enumeration<short>
    {
        public LvRiskSpecificSafetyDeviceCategory()
        {
            this.IsActive = true;
        }
    }
}
