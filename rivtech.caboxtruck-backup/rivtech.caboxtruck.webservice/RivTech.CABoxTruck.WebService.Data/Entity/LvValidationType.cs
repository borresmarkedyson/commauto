﻿using System;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public class LvValidationType : Enumeration<short>
    {
        public LvValidationType()
        {
            this.IsActive = true;
        }
    }
}
