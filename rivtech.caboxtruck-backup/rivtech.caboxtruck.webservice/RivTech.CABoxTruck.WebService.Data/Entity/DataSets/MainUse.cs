﻿namespace RivTech.CABoxTruck.WebService.Data.Entity.DataSets
{
    public class MainUse : Enumeration<short>
    {
        public static MainUse Courier = new MainUse(1, "Courier");
        public static MainUse Commercial = new MainUse(2, "Commercial");
        public static MainUse Service = new MainUse(3, "Service");

        public MainUse(short id, string name)
            : base(id, name)
        {
        }

        public MainUse()
        {
        }
    }
}