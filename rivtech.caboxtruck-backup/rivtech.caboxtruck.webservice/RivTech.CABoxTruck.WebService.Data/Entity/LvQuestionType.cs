﻿using System;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public class LvQuestionType : Enumeration<short>
    {
        public LvQuestionType()
        {
            this.IsActive = true;
        }
    }
}