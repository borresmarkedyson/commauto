﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public interface IIsActive
    {
        bool IsActive { get; set; }
    }
}
