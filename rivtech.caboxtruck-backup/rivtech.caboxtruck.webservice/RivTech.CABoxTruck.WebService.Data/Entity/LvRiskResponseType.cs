﻿using System;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public class LvRiskResponseType : Enumeration<short>
    {
        public LvRiskResponseType()
        {
            this.IsActive = true;
        }
    }
}
