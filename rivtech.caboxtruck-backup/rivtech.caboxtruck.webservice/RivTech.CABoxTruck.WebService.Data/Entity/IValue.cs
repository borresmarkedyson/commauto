﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.WebService.Data.Entity
{
    public interface IValue
    {
        public decimal Value { get; set; }
    }
}
