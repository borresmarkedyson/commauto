/****** Object:  UserDefinedFunction [dbo].[fnBilling]    Script Date: 20/01/2022 12:25:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER FUNCTION [dbo].[fnBilling]
(   
    @asOf Date = NULL
)
RETURNS TABLE 
AS
RETURN 
(
    select Risk.*,
		CASE WHEN PastInvoiced.PastBilled - Payment.PremiumPaymentAmount <= 0 THEN NULL ELSE PastInvoiced.PastDueDate END as PastDueDate,
		CASE WHEN PastInvoiced.PastBilled - Payment.PremiumPaymentAmount < 0 THEN 0 ELSE PastInvoiced.PastBilled - Payment.PremiumPaymentAmount END as PastDueAmount,
		CurrentInvoiced.DueDate,
		CurrentInvoiced.TotalBilled - Payment.PremiumPaymentAmount as AmountDue,
		Payment.PremiumPaymentAmount
	from Risk
	cross apply (select isnull(SUM(InvoiceDetail.InvoicedAmount), 0) PastBilled,
				MAX(Invoice.DueDate) PastDueDate from [Billing_Invoice] Invoice
				inner join [Billing_InvoiceDetail] InvoiceDetail on Invoice.Id = InvoiceDetail.InvoiceId
				inner join [Billing_AmountSubType] AmountSubType on AmountSubType.Id = InvoiceDetail.AmountSubTypeId
		where Invoice.RiskId = Risk.Id
		and (@asOf IS NULL or Invoice.DueDate < @asOf)
		and AmountSubType.AmountTypeId = 'P'
	) PastInvoiced
	cross apply (select isnull(SUM(InvoiceDetail.InvoicedAmount), 0) TotalBilled,
				MAX(Invoice.DueDate) DueDate from [Billing_Invoice] Invoice
				inner join [Billing_InvoiceDetail] InvoiceDetail on Invoice.Id = InvoiceDetail.InvoiceId
				inner join [Billing_AmountSubType] AmountSubType on AmountSubType.Id = InvoiceDetail.AmountSubTypeId
		where Invoice.RiskId = Risk.Id
		and (@asOf IS NULL or Invoice.InvoiceDate <= @asOf)
		and AmountSubType.AmountTypeId = 'P'
	) CurrentInvoiced
	cross apply (select isnull(SUM(PaymentDetail.Amount), 0) PremiumPaymentAmount from [Billing_Payment] Payment
		inner join [Billing_PaymentDetail] PaymentDetail on Payment.Id = PaymentDetail.PaymentId
		where Payment.RiskId = Risk.Id
		and (@asOf IS NULL or Payment.EffectiveDate <= @asOf)
		and PaymentDetail.AmountTypeId = 'P'
	) Payment
)