﻿namespace RivTech.CABoxTruck.Rater.App.Domain
{
    public class RaterSettings
    {
        public const string Key = "Rater";

        public string DefaultRaterVersion { get; set; }
        public string ExcelRaterDirectory { get; set; }
        public bool AllowDump { get; set; }
        public string PremiumRaterDumpDirectory { get; set; }
        public string PremiumRaterArchiveDirectory { get; set; }
        public string RaterPoolDirectory { get; set; }
        public int RaterSuspendedTimeout { get; set; }
        public int RaterIdleTimeout { get; set; }
        public double RaterPreloadTimeout { get; set; }
        public string ScheduleRemoveIdleRater { get; set; }
        public string ScheduleRemoveSuspendedRater { get; set; }
        public string ScheduleRemoveUnloadedRater { get; set; }
        public string ScheduleClearTempDir { get; set; }
    }
}
