﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Rater.App.Domain
{
    /// <summary>
    /// A value object that contains the values used as input in rating.
    /// </summary>
    public class InputValues
    {
        private InputValues() { }
        public InputValues(JObject values)
        {
            Values = values ?? new JObject();
        }

        //public ImmutableDictionary<string, object> Values { get => _values.ToImmutableDictionary(); }
        public JObject Values { get; }

        public override string ToString() => JsonConvert.SerializeObject(Values);

        public object ParseValue(string fullPropertyName, object property = null)
        {
            if (property == null && Values == null)
                return null;

            property ??= Values;

            if (!fullPropertyName.Contains("."))
            {
                return ((JObject)property)[fullPropertyName]?.ToObject<object>() ?? null;
            }

            string[] splitPropertyName = fullPropertyName.Split('.');
            string currentPropertyName = splitPropertyName[0];
            string childFullPropertyName = fullPropertyName.Substring((currentPropertyName + ".").Length);

            object currentProperty;
            Match match = Regex.Match(currentPropertyName, @"_(0|[1-9][0-9]*)_");
            if (match.Success)
            {
                string arrayPropertyName =  currentPropertyName.Substring(0, currentPropertyName.IndexOf("_"));
                int index = Convert.ToInt32(match.Captures[0].Value.Replace("_", ""));
                var prop = ((JObject)property)[arrayPropertyName];
                var asArrayProperty = (JArray)prop;
                currentProperty = asArrayProperty[index];
            }
            else
            {
                currentProperty = ((JObject)property)[currentPropertyName];
            }

            return ParseValue(childFullPropertyName, currentProperty);
        }

    }
}
