﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace RivTech.CABoxTruck.Rater.App.Domain
{
    /// <summary>
    /// A value object that wraps output values from a rater.
    /// </summary>
    public class OutputValues
    {
        protected Dictionary<string, object> _values = new Dictionary<string, object>();

        public OutputValues() { }

        public OutputValues(Dictionary<string, object> values)
        {
            _values = values ?? new Dictionary<string, object>();
        }

        public ImmutableDictionary<string, object> Data => _values.OrderBy(x => x.Key).ToImmutableDictionary();

        public void AddValues(Dictionary<string, object> values)
        {
            foreach(var kvp in values)
                 _values.Add(kvp.Key, kvp.Value);
        }
    }
}
