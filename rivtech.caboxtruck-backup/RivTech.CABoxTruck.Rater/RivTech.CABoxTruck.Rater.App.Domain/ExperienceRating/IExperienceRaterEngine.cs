﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating
{
    /// <summary>
    /// An interface for interacting with the experirence rater file such as mapping inputs, reading values, etc. 
    /// </summary>
    public interface IExperienceRaterEngine : IDisposable
    {
        /// <summary>
        /// Initializes engine before doing any operations.
        /// </summary>
        void Initialize();

        /// <summary>
        ///  Maps InputValues into rater file.
        /// </summary>
        void MapInput(InputValues inputValues);

        /// <summary>
        /// Validates rater in the engine.
        /// </summary>
        /// <exception cref="InvalidExperienceRaterFileException"></exception> 
        void Validate(ExperienceRater experienceRater);

        /// <summary>
        /// Invokes wokrbook calculation.
        /// </summary>
        void Calculate();

        /// <summary>
        /// Reads factors of experience rater in the engine.
        /// </summary>
        OutputValues ReadOutput(ExperienceRater experienceRater);

        /// <summary>
        /// Set information about the rater such as submission number, and rater version.
        /// </summary>
        void SetRaterInfo(ExperienceRater experienceRater);
        
        /// <summary>
        /// Unlocks system inputs.
        /// </summary>
        void UnlockInputs();

        /// <summary>
        /// Applies changes made in this engine.
        /// </summary>
        Task SaveChangesAsync();

        /// <summary>
        /// Locks system inputs.
        /// </summary>
        void LockInputs();

        /// <summary>
        /// Finalizes engine by reading its content before disposing it. 
        /// </summary>
        Task<FileContent> EndAsync();
    }
}