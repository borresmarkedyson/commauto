﻿using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating
{
    /// <summary>
    /// An entity that represents Experience Rater. 
    /// All operations done in Engine(IExperienceRaterEngine) is invoked in this object. 
    /// </summary>
    public class ExperienceRater : IDisposable, IAsyncDisposable
    {
        private ExperienceRater() { } // For db migration only.

        public ExperienceRater(string submissionNumber, string version, FileContent content)
        {
            SubmissionNumber = submissionNumber;
            Version = version;
            Content = content;
            DateModified = DateTime.UtcNow;
        }

        public IExperienceRaterEngine Engine { get; protected set; }
        public string SubmissionNumber { get; protected set; }
        public FileContent Content { get; protected set; }
        public string Version { get; protected set; }
        public DateTime DateModified { get; protected set; }

        /// <summary>
        /// Call this method to finalize rater by reading its content from Engine before disposing it.
        /// </summary>
        public async Task WrapUpAsync()
        {
            CheckIfEngineIsLoaded();
            await Engine.SaveChangesAsync();
            Content = await Engine.EndAsync();
        }

        public void MapInput(InputValues inputValues)
        {
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            CheckIfEngineIsLoaded();
            Engine.UnlockInputs();
            Engine.MapInput(inputValues);
            Engine.LockInputs();
            watch.Stop();
            Console.WriteLine($"Mapped input: {watch.Elapsed.TotalSeconds}");
        }

        /// <summary>
        /// Reads the output from the engine.
        /// </summary>
        public OutputValues ReadOutput()
        {
            CheckIfEngineIsLoaded();
            Engine.Calculate();
            return Engine.ReadOutput(this);
        }

        /// <summary>
        /// Set information about the rater such as rater version, submission number.
        /// </summary>
        public void SetRaterInfo()
        {
            CheckIfEngineIsLoaded();
            Engine.SetRaterInfo(this);
        }

        /// <summary>
        /// Loads engine to be able to make changes to rater file.
        /// </summary>
        /// <param name="engineLoader"></param>
        /// <returns></returns>
        public async Task LoadEngineAsync(IExperienceRaterEngineLoader engineLoader)
        {
            Engine = await engineLoader.LoadAsync(Content);
        }

        /// <summary>
        /// Loads an engine from the content and validates. If valid, assigns loaded engine and content to this ExprienceRater.  
        /// </summary>
        public async Task ChangeContent(FileContent newContent, IExperienceRaterEngineLoader engineLoader)
        {
            IExperienceRaterEngine engine = await engineLoader.LoadAsync(newContent);
            engine.Validate(this);
            Engine = engine;
            Content = newContent;
            DateModified = DateTime.UtcNow;
        }

        private void CheckIfEngineIsLoaded()
        {
            if (Engine is null)
                throw new ExperienceRaterEngineNotLoadedException(SubmissionNumber);
        }
        
        public void Dispose()
        {
            Engine?.Dispose();
            Engine = null;
        }

        public async ValueTask DisposeAsync()
        {
            await Task.Run(() => Dispose());
        }

        public ExperienceRater CloneWithNewSubmission(string submissionNumber)
        {
            var clone = (ExperienceRater)MemberwiseClone();
            clone.SubmissionNumber = submissionNumber;
            clone.DateModified = DateTime.UtcNow;
            clone.Content = Content.Copy();
            return clone;
        }
    }
}
