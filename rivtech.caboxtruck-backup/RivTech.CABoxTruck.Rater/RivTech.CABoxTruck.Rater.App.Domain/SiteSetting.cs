﻿namespace RivTech.CABoxTruck.Rater.App.Domain
{
    public class SiteSetting
    {
        public const string NAME_API_KEY = "ApiKey";

        private SiteSetting() { } 
        public SiteSetting(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }
        public string Value { get; }
    }
}
