﻿using System;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping
{
    public class PremiumRaterDumpFilter
    {
        public int? RecentCount { get; set; } = null;
        public DateTime? Date { get; set; } = null;
        public int? TimezoneOffset { get; set; } = 0;
        public string Submission { get; set; } = null;
        public string Option { get; set; } = null;
        public string TimeFrom { get; set; } = null;
        public string TimeTo { get; set; } = null;
    }
}
