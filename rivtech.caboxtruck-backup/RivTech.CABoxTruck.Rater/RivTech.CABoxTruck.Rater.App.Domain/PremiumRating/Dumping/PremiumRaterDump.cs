﻿using System;
using System.IO;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping
{
    public class PremiumRaterDump
    {
        const string dateFormat = "yyyyMMdd-HHmmss";
        const char filenameSeprator = '_';

        public static PremiumRaterDump FromRater(IPremiumRater rater)
        {
            var dump = new PremiumRaterDump
            {
                SubmissionNumber = rater.SubmissionNumber,
                CreatedDateUtc = DateTime.UtcNow,
                Content = rater.FileContent,
                OptionId = rater.OptionId
            };

            string filenameWithoutExt = string.Join(filenameSeprator, 
                dump.CreatedDateUtc.ToString(dateFormat), 
                dump.SubmissionNumber, 
                dump.OptionId);
            dump.Filename = $"{filenameWithoutExt}{dump.Content.Extension}";
            return dump;
        } 

        public static PremiumRaterDump FromFilename(string filename)
        {
            string[] parts = Path.GetFileNameWithoutExtension(filename).Split(filenameSeprator);
            var dump = new PremiumRaterDump
            {
                Filename = filename,
                SubmissionNumber = parts[1],
                OptionId = parts[2],
                CreatedDateUtc = DateTime.ParseExact(parts[0], dateFormat, null)
            };
            return dump;
        }

        public string SubmissionNumber { get; set; }
        public string OptionId { get; set; }
        public DateTime CreatedDateUtc { get; private set; }
        public string Filename { get; private set; }  
        public FileContent Content { get; set; } 
    }
}
