﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating
{
    /// <summary>
    /// An interface for interacting with premium rater file.
    /// </summary>
    public interface IPremiumRaterEngine : IAsyncDisposable
    {
        string Location { get; }
        Task InitializeAsync();
        void MapInput(InputValues inputValues);
        void Calculate();
        OutputValues GetOutput();
        Task SaveChangesAsync();
        Task<FileContent> ReadFileAsync();
        Task StopAsync();
        void Close();
    }
}
