﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling
{
    /// <summary>
    /// A singleton List of IPremiumRater. 
    /// </summary>
    public class PremiumRaterPoolCollection : List<IPremiumRater>
    {
        private static readonly Lazy<PremiumRaterPoolCollection> _instance = new Lazy<PremiumRaterPoolCollection>(new PremiumRaterPoolCollection());

        public static PremiumRaterPoolCollection INSTANCE => _instance.Value;

        private PremiumRaterPoolCollection() { }
    }
}
