﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling
{
    public interface IPremiumRaterPool : IPremiumRaterObserver
    {
        IEnumerable<IPremiumRater> PoolItems { get; }

        IPremiumRater GetItemReadyForRating(string submissionNumber);
        IPremiumRater GetUnloadableItem(string submissionNumber);
        Task PreloadRaterAsync(string submissionNumber, DateTime inceptionDate);
        Task RemoveIdleAsync();
        Task RemoveSuspendedRatersAsync();
        Task RequestShutdownAsync();
        void SuspendRaters(string submissionNumber);
        Task UnloadRatersBySubmissionAsync(string submissionNumber);
    }
}