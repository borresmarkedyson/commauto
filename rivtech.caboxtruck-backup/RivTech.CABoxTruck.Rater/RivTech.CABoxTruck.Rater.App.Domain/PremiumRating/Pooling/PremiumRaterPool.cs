﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling
{
    public class PremiumRaterPool : IPremiumRaterPool
    {
        private PremiumRaterPoolCollection _premiumRaterPoolCollection;
        private readonly ILogger<PremiumRaterPool> _logger;
        private readonly IPremiumRaterFactory _premiumRaterFactory;
        private readonly RaterSettings _raterSettings;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        #region Consructor
        public PremiumRaterPool(
            PremiumRaterPoolCollection premiumRaterPoolCollection,
            ILogger<PremiumRaterPool> logger,
            IPremiumRaterFactory premiumRaterFactory,
            RaterSettings raterSettings,
            IServiceScopeFactory serviceScopeFactory
        )
        {
            _premiumRaterPoolCollection = premiumRaterPoolCollection;
            _logger = logger;
            _premiumRaterFactory = premiumRaterFactory;
            _raterSettings = raterSettings;
            _serviceScopeFactory = serviceScopeFactory;
        }
        #endregion

        public IEnumerable<IPremiumRater> PoolItems
        {
            get
            {
                if (_premiumRaterPoolCollection is null)
                {
                    _premiumRaterPoolCollection = PremiumRaterPoolCollection.INSTANCE;
                }
                _premiumRaterPoolCollection.RemoveAll(x => x is null);
                return _premiumRaterPoolCollection?.ToList() ?? new List<IPremiumRater>();
            }
        }
        protected bool ShutdownRequested { get; private set; } = false;

        ILogger<PremiumRaterPool> Logger
        {
            get
            {
                if (_logger != null)
                    return _logger;
                return _serviceScopeFactory.CreateScope().ServiceProvider.GetService<ILogger<PremiumRaterPool>>();
            }
        }

        public async Task RemoveIdleAsync()
        {
            var timeout = _raterSettings.RaterIdleTimeout;
            var ratersToUnload = PoolItems.Where(x => x.IsIdle(timeout));
            await UnloadRatersAsync(ratersToUnload);
        }

        public async Task RemoveSuspendedRatersAsync()
        {
            IEnumerable<IPremiumRater> suspendedRaters = PoolItems.Where(x =>
                x.Status == PremiumRaterStatus.Suspended
                && x.SuspendedTimeSpan?.TotalSeconds >= _raterSettings.RaterSuspendedTimeout
            );
            await UnloadRatersAsync(suspendedRaters);
        }

        public async Task PreloadRaterAsync(string submissionNumber, DateTime inceptionDate)
        {
            _logger.LogInformation($"Pre-loading rater for {submissionNumber}.");

            // Add delay to ensure the previous same request has been registered 
            // to prevent double preloading of rater. 
            System.Threading.Thread.Sleep(100);

            IPremiumRater premiumRater = null;
            try
            {
                premiumRater = GetUnloadableItem(submissionNumber);
                if (premiumRater != null)
                {
                    if (GetItemReadyForRating(submissionNumber) != null)
                    {
                        _logger.LogInformation($"Rater of {submissionNumber} is already preloaded.");
                    }
                    else
                    {
                        // If a premium rater exists but suspended,
                        // just resume to use it.
                        premiumRater.ResumeFromSuspension();
                    }
                }
                else
                {
                    // Create premium rater if non exists.
                    premiumRater = await _premiumRaterFactory.CreateAsync(submissionNumber, inceptionDate);
                    premiumRater.AddObserver(this);
                    await premiumRater.PreloadAsync();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occured while preloading premium raters.");

                if (premiumRater != null)
                {
                    if (premiumRater.Status != PremiumRaterStatus.Ready)
                    {
                        _logger.LogInformation($"Removing rater of {submissionNumber} after failed preload.");
                        premiumRater.RemoveObserver(this);
                        _premiumRaterPoolCollection?.Remove(premiumRater);
                        _ = UnloadRatersAsync(new[] { premiumRater });
                    }
                }
            }
        }

        public void SuspendRaters(string submissionNumber)
        {
            IPremiumRater premiumRater = PoolItems.FirstOrDefault(x => x.SubmissionNumber.Equals(submissionNumber));
            if (premiumRater is null)
                return;
            try
            {
                premiumRater.Suspend();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        #region IPremiumRaterObserver methods

        public void OnPreloaded(IPremiumRater premiumRater)
        {
            if (GetUnloadableItem(premiumRater.SubmissionNumber) != null)
            {
                Logger.LogInformation("Unloading a duplicate premium rater.");
                // If rater pool already contains the same rater,
                // unload it immediately.
                _ = UnloadRatersAsync(new[] { premiumRater });
            }
            else
            {
                _premiumRaterPoolCollection?.Add(premiumRater);
            }
        }

        public void OnPreloadFailed(IPremiumRater premiumRater)
        {
            _premiumRaterPoolCollection?.Remove(premiumRater);
            _ = UnloadRatersAsync(new[] { premiumRater });
        }

        public void OnRatingStarted(IPremiumRater premiumRater)
        {
            _ = Task.Run(async () =>
              {
                  using var scope = _serviceScopeFactory.CreateScope();
                  var premiumRaterFactory = scope.ServiceProvider.GetService<IPremiumRaterFactory>();
                  if (!ShutdownRequested)
                  {
                      // Create a new premium rater.
                      try
                      {
                          IPremiumRater newPremiumRater = await premiumRaterFactory
                              .CreateAsync(premiumRater.SubmissionNumber, premiumRater.InceptionDate);
                          newPremiumRater.AddObserver(this);
                          await newPremiumRater.PreloadAsync();
                      }
                      catch (Exception e)
                      {
                          _logger.LogError(e, e.Message);
                      }
                  }
              });
        }

        public void OnUnloaded(IPremiumRater premiumRater)
        {
            try
            {
                if (premiumRater != null && PoolItems.Contains(premiumRater))
                {
                    _logger.LogInformation($"Premium rater {premiumRater?.SubmissionNumber} removed from pool.");
                    _premiumRaterPoolCollection.Remove(premiumRater);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Error removing premium rater from pool.", e);
            }
        }

        #endregion

        public IPremiumRater GetUnloadableItem(string submissionNumber)
        {
            return PoolItems.OrderBy(x => x.CreatedDate)
                .FirstOrDefault(x => x.SubmissionNumber.Equals(submissionNumber) && x.CanBeUnloaded);
        }

        public IPremiumRater GetItemReadyForRating(string submissionNumber)
        {
            if (ShutdownRequested)
                throw new RaterException("Getting item from rater pool is no longer available as it is being requested for shutdown.");

            IPremiumRater premiumRater = PoolItems
                .FirstOrDefault(x => x.SubmissionNumber.Equals(submissionNumber)
                    && x.Status.Equals(PremiumRaterStatus.Ready));
            return premiumRater;
        }

        public async Task UnloadRatersBySubmissionAsync(string submissionNumber)
        {
            var items = PoolItems.Where(r => r.SubmissionNumber.Equals(submissionNumber));
            await UnloadRatersAsync(items);
        }

        public async Task RequestShutdownAsync()
        {
            ShutdownRequested = true;
            _logger.LogInformation($"Shutting down rater pool.");
            var raters = PoolItems.Where(x => x.CanBeUnloaded);
            await UnloadRatersAsync(raters);
        }

        private async Task UnloadRatersAsync(IEnumerable<IPremiumRater> premiumRaters)
        {
            try
            {
                var ratersArray = premiumRaters?.ToArray() ?? new IPremiumRater[0];
                int count = ratersArray.Length;
                var unloadTasks = new List<Task>();

                for (int i = 0; i < count; i++)
                {
                    var rater = ratersArray[i];
                    if (rater is null)
                        continue;

                    rater.RemoveObserver(this);
                    unloadTasks.Add(Task.Run(async () =>
                    {
                        var sp = _serviceScopeFactory.CreateScope().ServiceProvider;
                        var logger = sp.GetService<ILogger<PremiumRaterPool>>();

                        if (rater != null)
                        {
                            logger.LogInformation($"Unloading rater of {rater.SubmissionNumber}.");
                            try
                            {
                                await using (rater)
                                {
                                    await rater.UnloadAsync();
                                    logger.LogInformation($"Unloaded rater of {rater.SubmissionNumber}.");
                                }
                            }
                            catch (Exception e)
                            {
                                logger?.LogError(e, $"Faield to unload rater of {rater.SubmissionNumber}.");
                            }
                        }
                    }));
                }

                await Task.WhenAll(unloadTasks);
            }
            catch (Exception e)
            {
                Logger?.LogError(e, "Error unloading premium raters.");
            }
        }
    }
}
