﻿namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling
{
    public interface IPremiumRaterObserver
    {
        void OnRatingStarted(IPremiumRater poolItem);
        void OnPreloaded(IPremiumRater premiumRater);
        void OnPreloadFailed(IPremiumRater premiumRater);
        void OnUnloaded(IPremiumRater premiumRater);
    }
}