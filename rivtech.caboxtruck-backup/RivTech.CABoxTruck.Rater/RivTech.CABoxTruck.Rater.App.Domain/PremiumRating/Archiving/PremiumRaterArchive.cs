﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive
{
    public class PremiumRaterArchive
    {
        public const string FOLDER_NAME = "premium-rater";

        public static PremiumRaterArchive CreateFromRater(IPremiumRater premiumRater)
        {
            var archive = new PremiumRaterArchive
            {
                Id = Guid.NewGuid(),
                SubmissionNumber = premiumRater.SubmissionNumber,
                EndorsementNumber = premiumRater.EndorsementNumber.Value,
                InputValues = JsonConvert.SerializeObject(premiumRater.InputValues),
                OutputValues = JsonConvert.SerializeObject(premiumRater.Result),
                RatingElapsed = premiumRater.RatingElapsed,
                CreatedDate = premiumRater.CreatedDate
            };

            archive.FileName = $"{archive.SubmissionNumber}_Endorsement-{archive.EndorsementNumber}_{archive.CreatedDate:yyyyMMddHHmmss}{premiumRater.FileContent.Extension}";
            return archive;
        }

        public Guid Id { get; set; }
        public string SubmissionNumber { get; set; }
        public int EndorsementNumber { get; set; }
        public string InputValues { get; set; }
        public string OutputValues { get; set; }
        public double RatingElapsed { get; set; }
        public string FileName { get; set; }
        public string RelativePath
        {
            set { }
            get => Path.Combine(FOLDER_NAME, SubmissionNumber, FileName);
        }
        public DateTime CreatedDate { get; set; }
    }
}
