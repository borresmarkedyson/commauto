﻿using System;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving
{
    public class PremiumRaterArchiveFilter
    {
        public int? RecentCount { get; set; } = null;
        public DateTime? Date { get; set; } = null;
        public int? TimezoneOffset { get; set; } = 0;
        public string Submission { get; set; } = null;
        public int? EndorsementNumber { get; set; }
    }
}
