﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating
{
    public interface IPremiumRater : IAsyncDisposable
    {
        string SubmissionNumber { get; }
        DateTime InceptionDate { get; }
        FileContent FileContent { get; }
        IPremiumRaterEngine Engine { get; }
        PremiumRaterStatus Status { get; }
        DateTime? EngineLoadedAt { get; }
        DateTime CreatedDate { get; }
        bool CanBeUnloaded { get; }
        double Elapsed { get; }
        InputValues InputValues { get; }
        OutputValues Result { get; }
        double RatingElapsed { get; }
        DateTime? SuspendedTimeUtc { get; }
        TimeSpan? SuspendedTimeSpan { get; }
        string OptionId { get; }
        int? EndorsementNumber { get; set; }

        void AddObserver(IPremiumRaterObserver observer);
        void RemoveObserver(IPremiumRaterObserver observer);
        bool IsIdle(int timeout);
        Task UnloadAsync();
        bool Equals(IPremiumRater item);

        /// <summary>
        /// Reads the content of engine after saving and unloading it.
        /// </summary>
        Task WrapUpAsync();
        void Rate(InputValues inputValues, string optionId);
        Task PreloadAsync();
        void Suspend();
        void ResumeFromSuspension();
        void ForceCloseEngine();
    }
}