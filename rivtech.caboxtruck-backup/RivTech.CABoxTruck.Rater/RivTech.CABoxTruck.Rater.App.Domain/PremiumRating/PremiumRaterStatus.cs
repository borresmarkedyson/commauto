﻿namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating
{
    public enum PremiumRaterStatus
    {
        Initializing = 1,
        Ready = 2, 
        Running = 3, 
        Finished = 4,
        Unloading = 5,
        Unloaded = 6,
        Suspended = 7
    }
}
