﻿using System;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating
{

    public interface IPremiumRaterId
    {
        string SubmissionNumber { get; }
    }

    /// <summary>
    /// A value object. A composite ID for premium rater which comprises
    /// of Submission Number and Option ID.
    /// </summary>
    public class SubmissionPremiumRaterId : IPremiumRaterId
    {
        public SubmissionPremiumRaterId(string submissionNumber, int optionId)
        {
            SubmissionNumber = submissionNumber;
            OptionId = optionId;
        }

        public string SubmissionNumber { get; }
        public int OptionId { get; }

        public override string ToString()
        {
            return $"{SubmissionNumber}({OptionId})";
        }

        public override bool Equals(object obj)
        {
            return obj is SubmissionPremiumRaterId other
                && SubmissionNumber.Equals(other.SubmissionNumber)
                && OptionId.Equals(other.OptionId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SubmissionNumber, OptionId);
        }
    }

    public class PolicyPremiumRaterId : IPremiumRaterId
    {
        public PolicyPremiumRaterId(string submissionNumber)
        {
            SubmissionNumber = submissionNumber;
        }

        public string SubmissionNumber { get; }

        public override string ToString()
        {
            return $"{SubmissionNumber}(POLICY)";
        }

        public override bool Equals(object obj)
        {
            var other = obj as PolicyPremiumRaterId;
            return other != null
                   && SubmissionNumber.Equals(other.SubmissionNumber);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SubmissionNumber);
        }
    }
}
