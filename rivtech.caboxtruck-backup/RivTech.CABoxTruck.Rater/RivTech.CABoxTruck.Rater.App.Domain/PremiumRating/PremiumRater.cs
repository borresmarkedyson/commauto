﻿using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.PremiumRating
{
    /// <summary>
    /// An entity that represents the Premium Rater. 
    /// All operations done in IPremiumRaterEngine is invoked thru this object.
    /// </summary>
    public class PremiumRater : IPremiumRater
    {
        public const string Version_1 = "v1";
        public const string Version_2 = "v2";

        private List<IPremiumRaterObserver> _premiumRaterPoolItemObservers;

        private PremiumRater()
        {
            Id = Guid.NewGuid();
        }

        public PremiumRater(
            string submissionNumber,
            DateTime inceptionDate,
            FileContent content,
            IPremiumRaterEngine engine
        ) : this()
        {
            CreatedDate = DateTime.UtcNow;
            Status = PremiumRaterStatus.Initializing;
            SubmissionNumber = submissionNumber;
            InceptionDate = inceptionDate;
            FileContent = content;
            Engine = engine ?? throw new ArgumentNullException("Premium rater engine is required.");
            _premiumRaterPoolItemObservers = new List<IPremiumRaterObserver>();
        }

        public Guid Id { get; set; }
        public string SubmissionNumber { get; }
        public string OptionId { get; protected set; }
        public int? EndorsementNumber { get; set; } = null;
        public DateTime InceptionDate { get; }
        public InputValues InputValues { get; protected set; }
        public OutputValues Result { get; protected set; }
        public FileContent FileContent { get; protected set; }
        public IPremiumRaterEngine Engine { get; protected set; }
        public PremiumRaterStatus Status { get; protected set; }
        public DateTime CreatedDate { get; protected set; }
        public DateTime? EngineLoadedAt { get; protected set; }
        public double RatingElapsed { get; protected set; }
        public double Elapsed => (DateTime.UtcNow - CreatedDate).TotalSeconds;
        public bool CanBeUnloaded => Status.Equals(PremiumRaterStatus.Initializing)
            || Status.Equals(PremiumRaterStatus.Ready)
            || Status.Equals(PremiumRaterStatus.Suspended);
        public DateTime? SuspendedTimeUtc { get; protected set; }
        public TimeSpan? SuspendedTimeSpan => SuspendedTimeUtc.HasValue ? DateTime.UtcNow - SuspendedTimeUtc : null;

        public void Rate(InputValues inputValues, string optionId)
        {
            if (!Status.Equals(PremiumRaterStatus.Ready))
                throw new RaterException("Rater is not ready yet.");

            var sw = new Stopwatch();
            sw.Restart();
            OptionId = optionId;
            Status = PremiumRaterStatus.Running;

            NotifyObserversOnStarted();

            InputValues = inputValues;

            // Run engine
            Engine.MapInput(InputValues);
            Engine.Calculate();
            Result = Engine.GetOutput();

            sw.Stop();
            RatingElapsed = sw.Elapsed.TotalSeconds;
            Console.WriteLine($"Rate ellapsed time: {sw.Elapsed.Seconds}");
        }

        public void AddObserver(IPremiumRaterObserver observer)
        {
            _premiumRaterPoolItemObservers ??= new List<IPremiumRaterObserver>();
            _premiumRaterPoolItemObservers.Add(observer);
        }

        public void RemoveObserver(IPremiumRaterObserver observer)
        {
            _premiumRaterPoolItemObservers?.Remove(observer);
        }

        #region Observer's related methods

        private void NotifyObserversOnPreloaded()
        {
            foreach (var observer in _premiumRaterPoolItemObservers)
                observer.OnPreloaded(this);
        }

        private void NotifyObserversOnPreloadFailed()
        {
            foreach (var observer in _premiumRaterPoolItemObservers)
                observer.OnPreloadFailed(this);
        }

        private void NotifyObserversOnStarted()
        {
            foreach (var observer in _premiumRaterPoolItemObservers)
                observer.OnRatingStarted(this);
        }

        private void NotifyObserverOnUnloaded()
        {
            foreach (var observer in _premiumRaterPoolItemObservers)
                observer.OnUnloaded(this);
        }

        #endregion

        public bool IsIdle(int timeout)
        {
            return Status.Equals(PremiumRaterStatus.Ready)
                && (DateTime.UtcNow - EngineLoadedAt.Value).TotalMinutes >= timeout;
        }

        public async Task PreloadAsync()
        {
            NotifyObserversOnPreloaded();
            try
            {
                await Engine.InitializeAsync();
                EngineLoadedAt = DateTime.UtcNow;

                if (Status != PremiumRaterStatus.Unloading && Status != PremiumRaterStatus.Suspended)
                    Status = PremiumRaterStatus.Ready;
            }
            catch (Exception)
            {
                NotifyObserversOnPreloadFailed();
            }
        }

        public void Suspend()
        {
            if (!CanBeUnloaded)
                return;
            Status = PremiumRaterStatus.Suspended;
            SuspendedTimeUtc = DateTime.UtcNow;
        }

        public void ResumeFromSuspension()
        {
            if (Status != PremiumRaterStatus.Suspended)
                return;
            if (EngineLoadedAt == null)
                Status = PremiumRaterStatus.Initializing;
            else
                Status = PremiumRaterStatus.Ready;
            SuspendedTimeUtc = null;
            EngineLoadedAt = DateTime.UtcNow; // To reset idle time checking.
        }

        public void ForceCloseEngine()
        {
            if (Status == PremiumRaterStatus.Unloaded)
            {
                Engine?.Close();
            }
        }

        public async Task WrapUpAsync()
        {
            if (Status != PremiumRaterStatus.Unloading && Status != PremiumRaterStatus.Unloaded)
            {
                await Engine.SaveChangesAsync();
                await UnloadAsync();
                FileContent = await Engine.ReadFileAsync();
            }
        }

        public async Task UnloadAsync()
        {
            if (Status.Equals(PremiumRaterStatus.Unloading)
                || Status.Equals(PremiumRaterStatus.Unloaded))
                return;

            try
            {
                Status = PremiumRaterStatus.Unloading;
                await Engine.StopAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Status = PremiumRaterStatus.Unloaded;
                NotifyObserverOnUnloaded();
            }
        }

        public bool Equals(IPremiumRater item)
        {
            return item != null && SubmissionNumber.Equals(item.SubmissionNumber);
        }

        public override int GetHashCode()
        {
            return SubmissionNumber.GetHashCode();
        }

        public async ValueTask DisposeAsync()
        {
            try
            {
                await Engine.DisposeAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error disposing rater.");
            }
        }
    }
}
