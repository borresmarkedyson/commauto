﻿using System;

namespace RivTech.CABoxTruck.Rater.App.Domain
{
    public class RaterTemplate
    {
        public const string TYPE_EXPERIENCE = "X";
        public const string TYPE_PREMIUM = "P";

        private RaterTemplate() { }

        public RaterTemplate(string version, string type, DateTime effectiveDate, string filePath)
        {
            Version = version;
            Type = type;
            EffectiveDate = effectiveDate;
            FilePath = filePath;
        }

        public string Version { get; }
        public string Type { get; }
        public DateTime EffectiveDate { get;  }
        public string FilePath { get; }
        public FileContent Content { get;  set; }
    }
}
