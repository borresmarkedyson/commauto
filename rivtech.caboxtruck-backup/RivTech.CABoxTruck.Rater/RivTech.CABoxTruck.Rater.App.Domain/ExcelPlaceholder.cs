﻿using System;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Rater.App.Domain
{
    /// <summary>
    /// Value Object. Used in placeholder mapping in excel rater file.
    /// </summary>
    public class ExcelPlaceholder
    {
        public ExcelPlaceholder(string placeholder)
        {
            PlaceholderText = placeholder;
        }

        public string PlaceholderText { get; }
        public string Path => PlaceholderText.Replace("__", "");

        public bool IsValid()
        {
            Match match = Regex.Match(
                $"{this}" ?? string.Empty,
                "^__[a-zA-Z]([a-zA-Z0-9]*|((_(0|[1-9][0-9]*)_)?.[a-zA-Z][a-zA-Z0-9]*)*)*__$",
                RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline);
            return match.Success;
        }

        public override string ToString() => PlaceholderText;

        /// <summary>
        /// Returns a new instance with updated index of the first occurence of array property.
        /// </summary>
        /// <param name="newIndex"></param>
        /// <returns></returns>
        public ExcelPlaceholder UpdateIndex(int newIndex)
        {
            Match match = Regex.Match($"{this}", "_(0|[1-9][0-9]*)_.");
            string placeholder = $"{PlaceholderText}";
            if (match.Success)
                placeholder = PlaceholderText.Replace(match.Captures[0].Value, $"_{newIndex}_.");
            return new ExcelPlaceholder(placeholder);
        }

        /// <summary>
        /// Returns null if placeholder does not contain array index.
        /// </summary>
        public int? Index
        {
            get
            {
                Match match = Regex.Match(PlaceholderText, "_(0|[1-9][0-9]*)_.");
                if (!match.Success)
                    return null;
                Match indexMatch = Regex.Match(match.Captures[0].Value, "(0|[1-9][0-9]*)");
                return Convert.ToInt32(indexMatch.Captures[0].Value);
            }
        }
    }
}
