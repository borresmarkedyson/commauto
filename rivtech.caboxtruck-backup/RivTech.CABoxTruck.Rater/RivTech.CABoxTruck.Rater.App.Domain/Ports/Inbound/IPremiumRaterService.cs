﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound
{
    public interface IPremiumRaterService
    {
        IEnumerable<IPremiumRater> GetRaterPoolItems(bool? runningOnly = null);
        Task PreloadRatersAsync(string submissionNumber, DateTime inceptionDate);
        IPremiumRater Rate(RateCommand command);
        Task DumpRaterAsync(IPremiumRater premiumRater);
        Task UnloadRaterAsync(string submissionNumber);
        PremiumRaterDump[] Get(PremiumRaterDumpFilter filter);
        FileContent DownloadDumpFile(DateTime date, string filename);

        Task<PremiumRaterArchive[]> GetArchivesAsync(PremiumRaterArchiveFilter filter);
        Task<PremiumRaterArchive> GetArchiveByIdAsync(Guid id);
        Task<byte[]> DownloadArchiveFileAsync(Guid id);
        Task DeleteArchive(string submissionNumber, int endorsementNumber);

        Task ShutdownRaterPoolAsync();
        Task RemoveIdleAsync();
        Task RemoveSuspendedRaters();
        Task ForceUnloadAsync(string submissionNumber);
        void SuspendRater(string submissionNumber);
        Task ForceCloseStuckUnloaded();
        Task SaveRaterFromIssuanceAsync(IPremiumRater rater);
    }

    public class RateCommand
    {
        public RateCommand(
            string submissionNumber,
            DateTime inceptionDate,
            InputValues inputValues,
            string optionId = null,
            int? endorsementNumber = null,
            bool? fromPolicy = null,
            bool? forIssuance = null
        ) {
            SubmissionNumber = submissionNumber;
            InceptionDate = inceptionDate;
            OptionId = optionId;
            EndorsementNumber = endorsementNumber;
            InputValues = inputValues;
            FromPolicy = fromPolicy;
            ForIssuance = forIssuance;
        }

        public string OptionId { get; }
        public int? EndorsementNumber { get; }
        public InputValues InputValues { get; }
        public bool? FromPolicy { get; }
        public bool? ForIssuance { get; }
        public string SubmissionNumber { get; }
        public DateTime InceptionDate { get; }
    }

}