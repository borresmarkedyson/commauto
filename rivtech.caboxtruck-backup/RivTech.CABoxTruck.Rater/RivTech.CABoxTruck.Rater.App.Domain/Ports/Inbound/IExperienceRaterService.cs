﻿using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound
{
    public interface IExperienceRaterService
    {
        Task CreateInitialAsync(string submissionNumber, DateTime effectiveDate);
        Task<ExperienceRater> DownloadAsync(string submissionNumber, DateTime effectiveDate, InputValues inputValues);
        Task<OutputValues> UploadAsync(string submissionNumber, DateTime effectiveDate, FileContent content);
        bool Exists(string submissionNumber);
        Task DeleteAsync(string submissionNumber);
        Task CopyToNewSubmission(string from, string to);
    }
}