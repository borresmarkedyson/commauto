﻿using RivTech.CABoxTruck.Rater.App.Domain.Security;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound
{
    public interface IAuthService
    {
        User Authenticate(User user);
    }
}
