﻿using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound
{
    public interface IUserService
    {
        User GetByUsername(string username);
        IEnumerable<User> GetAll();
        void Add(User user);
        void Update(User user);
        void Remove(string username);
        void ChangePassword(string username, string newPassword);
    }
}
