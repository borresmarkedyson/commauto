﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating
{
    public interface IPremiumRaterArchiveRepository
    {
        Task<PremiumRaterArchive> CreateAsync(PremiumRaterArchive archive);
        Task<PremiumRaterArchive> GetAsync(string submissionNumber, int endorsementNumber);
        Task<IEnumerable<PremiumRaterArchive>> GetAsync(PremiumRaterArchiveFilter filter);
        Task<PremiumRaterArchive> GetByIdAsync(Guid id);
        Task RemoveAsync(PremiumRaterArchive storedArchive);
    }
}
