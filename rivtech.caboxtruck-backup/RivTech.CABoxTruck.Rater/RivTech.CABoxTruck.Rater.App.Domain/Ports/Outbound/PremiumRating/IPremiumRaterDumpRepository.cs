﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating
{
    public interface IPremiumRaterDumpRepository
    {
        Task CreateAsync(PremiumRaterDump premiumRater);
        IEnumerable<PremiumRaterDump> Get(PremiumRaterDumpFilter filter);
        FileContent GetDumpFile(DateTime date, string filename);
    }
}
