﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound
{
    public interface IPremiumRaterFactory
    {
        Task<IPremiumRater> CreateAsync(string submissionNumber, DateTime inceptionDate);
    }
}
