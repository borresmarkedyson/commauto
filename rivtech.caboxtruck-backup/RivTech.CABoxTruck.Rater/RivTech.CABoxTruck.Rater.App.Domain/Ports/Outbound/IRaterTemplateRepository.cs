﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound
{
    public interface IRaterTemplateRepository
    {
        Task<RaterTemplate> GetByEffectivityAsync(DateTime effectiveDate, string type);
        string GetVersionFromEffectiveDate(DateTime effectiveDate, string type);
    }
}
