﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound
{
    public interface IStorageService
    {

        /// <summary>
        /// Creates file on the given relative path. 
        /// </summary>
        Task SaveBytesAsync(byte[] bytes, string relativePath);
        Task<byte[]> GetAsync(string relativePath);
        Task DeleteAsync(string relativePath);
    }
}
