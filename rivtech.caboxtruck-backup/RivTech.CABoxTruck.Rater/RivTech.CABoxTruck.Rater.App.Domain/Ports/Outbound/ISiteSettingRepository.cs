﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound
{
    public interface ISiteSettingRepository
    {
        SiteSetting Get(string key);
    }
}
