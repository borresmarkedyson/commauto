﻿using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating
{
    public interface IExperienceRaterRepository 
    {
        Task<ExperienceRater> GetAsync(string submissionNumber);
        bool Exists(string submissionNumber);
        Task<ExperienceRater> CreateAsync(ExperienceRater experienceRater);
        Task UpdateAsync(ExperienceRater raterFile);
        Task DeleteAsync(string submissionNumber);
    }
}
