﻿using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating
{
    public interface IExperienceRaterFactory
    {
        /// <summary>
        /// Creates an ExperienceRater object with its loaded engine.
        /// </summary>
        /// <returns></returns>
        Task<ExperienceRater> CreateFromTemplateAsync(string submissionNumber, DateTime effectiveDate);
    }
}
