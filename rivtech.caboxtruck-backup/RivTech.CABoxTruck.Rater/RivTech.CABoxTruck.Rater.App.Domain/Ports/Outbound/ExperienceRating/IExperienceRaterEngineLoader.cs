﻿using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating
{
    public interface IExperienceRaterEngineLoader
    {
        Task<IExperienceRaterEngine> LoadAsync(FileContent fileContent);
    }
}
