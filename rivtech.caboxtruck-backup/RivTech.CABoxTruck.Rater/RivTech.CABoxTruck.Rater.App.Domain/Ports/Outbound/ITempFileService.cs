﻿using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound
{
    public interface ITempFileService
    {
        Task<string> FromContentAsync(FileContent fileContent, string subPath = null);

        void ClearTempFiles();
    }
}
