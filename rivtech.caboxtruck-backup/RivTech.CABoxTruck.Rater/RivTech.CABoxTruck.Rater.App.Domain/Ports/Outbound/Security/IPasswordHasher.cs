﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security
{
    public interface IPasswordHasher
    {
        string Hash(string password, string salt);
    }
}
