﻿using RivTech.CABoxTruck.Rater.App.Domain.Security;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security
{
    public interface IJwtTokenGenerator
    {
        string Generate(User user);
    }
}
