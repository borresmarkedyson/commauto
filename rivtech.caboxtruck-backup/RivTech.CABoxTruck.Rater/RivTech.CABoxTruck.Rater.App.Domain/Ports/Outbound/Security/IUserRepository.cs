﻿using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetByUsername(string username);
        User Get(int userId);
        void Add(User user);
        void Update(User user);
        void Delete(string username);
    }
}
