﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Security
{
    public enum PermissionsEnum
    {
        ReadUserDetails = 100,
        ManageUsers = 101,
        ReadRaterPoolItems = 200,
        ManageRaterPoolItems = 201,
        ReadUsedRaters = 300,
    }
}
