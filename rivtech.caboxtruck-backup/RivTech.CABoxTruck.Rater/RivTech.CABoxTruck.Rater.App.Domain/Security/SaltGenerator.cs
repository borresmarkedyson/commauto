﻿using System.Security.Cryptography;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Domain.Security
{
    public class SaltGenerator
    {
        public static string Generate()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return Encoding.UTF8.GetString(salt);
        }
    }
}
