﻿using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using System;
using System.Security.Cryptography;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Domain.Security
{
    public class User
    {

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string DisplayName { get; set; }
        public int[] Permissions { get; set; }
        public string Token { get; set; }

        public void HashPassword(IPasswordHasher passwordHasher)
        {
            Salt = SaltGenerator.Generate();
            Password = passwordHasher.Hash(Password, Salt);
        }

        public void ClearPasswordAndSalt()
        {
            Salt = null;
            Password = null;
        }

    }
}
