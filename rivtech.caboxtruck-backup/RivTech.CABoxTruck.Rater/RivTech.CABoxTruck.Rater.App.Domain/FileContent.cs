﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Domain
{
    /// <summary>
    /// A value object that contains a file content as byte[] and its extension. 
    /// </summary>
    public class FileContent : ValueObject
    {
        private FileContent() { } // For db migration only.

        public FileContent(byte[] bytes, string extension)
        {
            Bytes = bytes;
            Extension = extension;
        }

        public byte[] Bytes { get; }
        public string Extension { get; }

        public FileContent Copy()
        {
            byte[] contentCopy = new byte[Bytes.Length];
            Bytes.CopyTo(contentCopy, 0);
            return new FileContent(contentCopy, Extension);
        }

        public FileContent UpdateContent(byte[] content)
        {
            return new FileContent(content, Extension);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Extension;
            yield return Bytes;
        }
    }
}
