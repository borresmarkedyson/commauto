﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class ExperienceRaterEngineNotLoadedException : RaterException
    {
        public ExperienceRaterEngineNotLoadedException(string submissionNumber)
            : base($"The experience rater engine of submission {submissionNumber} is not loaded.")
        {
        }
    }
}
