﻿using System;

namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class RaterException : Exception
    {
        public RaterException(string message) : base(message) { }
    }
}
