﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class InvalidExperienceRaterFileException : RaterException
    {
        public InvalidExperienceRaterFileException(string message) : base(message)
        {
        }
    }
}
