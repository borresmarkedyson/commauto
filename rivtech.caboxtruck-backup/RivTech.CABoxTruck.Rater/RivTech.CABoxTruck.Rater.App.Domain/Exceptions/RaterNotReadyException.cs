﻿using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;

namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class RaterNotReadyException : RaterException
    {
        public RaterNotReadyException(SubmissionPremiumRaterId id)
            : base($"Rater of {id.SubmissionNumber}({id.OptionId}) is not yet loaded in rater pool.")
        {
        }
    }
}
