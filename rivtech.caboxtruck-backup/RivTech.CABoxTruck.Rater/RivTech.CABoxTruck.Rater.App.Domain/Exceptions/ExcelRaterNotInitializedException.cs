﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class ExcelRaterNotInitializedException : RaterException
    {
        public ExcelRaterNotInitializedException(string message) : base(message)
        {
        }
    }
}
