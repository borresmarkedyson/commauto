﻿namespace RivTech.CABoxTruck.Rater.App.Domain.Exceptions
{
    public class ExperienceRaterNotFoundException : RaterException
    {
        public ExperienceRaterNotFoundException(string submissionNumber) 
            : base($"No experience rater exists for {submissionNumber}")
        {
        }
    }
}
