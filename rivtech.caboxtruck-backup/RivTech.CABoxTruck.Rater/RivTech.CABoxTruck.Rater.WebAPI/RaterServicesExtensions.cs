﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RivTech.CABoxTruck.Rater.App.Adapters.Inbound;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.Security;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;

namespace RivTech.CABoxTruck.Rater.WebAPI
{
    public static class RaterServicesExtensions
    {
        public static void AddRaterServices(this IServiceCollection services)
        {
            var sp = services.BuildServiceProvider();
            services.AddSingleton(sp.GetService<IOptions<RaterSettings>>().Value);

            #region Singletons

            services.AddSingleton(sp => PremiumRaterPoolCollection.INSTANCE);

            #endregion

            #region Scoped 
            services.AddScoped<ISiteSettingRepository, SiteSettingRepository>();
            services.AddScoped<IPasswordHasher, DefaultPasswordHasher>();
            services.AddScoped<IJwtTokenGenerator, JwtTokenGenerator>();

            services.AddScoped<ITempFileService, TempFileService>();
            services.AddScoped<IRaterTemplateRepository, RaterTemplateRepository>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPremiumRaterDumpRepository, PremiumRaterDumpRepository>();
            services.AddScoped<IPremiumRaterArchiveRepository, PremiumRaterArchiveRepository>();

            services.AddScoped<IExperienceRaterService, ExperienceRaterService>();
            services.AddScoped<IPremiumRaterService, PremiumRaterService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IPremiumRaterFactory, PremiumRaterFactory>();

            services.AddScoped<IExperienceRaterFactory, ExperienceRaterFactory>();
            services.AddScoped<IExperienceRaterRepository, ExperienceRaterRepository>();
            services.AddScoped<IExcelPlaceholderInputMapper, ExcelPlaceholderInputMapper>();
            services.AddScoped<IExperienceRaterEngineLoader, ExperienceRaterEngineLoader>();

            services.AddScoped<IStorageService, AzureStorageService>();

            services.AddScoped<PremiumRaterPool>();
            #endregion
        }

    }
}
