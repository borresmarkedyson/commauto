﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.WebAPI.Attributes
{
    [AttributeUsage(validOn: AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string HEADER_API_KEY = "ApiKey";

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.HttpContext.Request.Headers.TryGetValue(HEADER_API_KEY, out var extractedApiKey))
            {
                context.Result = new ContentResult()
                {
                    StatusCode = 401,
                    Content = "Api Key was not provided"
                };
                return;
            }

            IServiceProvider serviceProvider = context.HttpContext.RequestServices;
            var siteSettingRepository = serviceProvider.GetRequiredService<ISiteSettingRepository>();

            var apiKey = siteSettingRepository.Get(SiteSetting.NAME_API_KEY);

            if (!apiKey.Value.Equals(extractedApiKey))
            {
                context.Result = new ContentResult()
                {
                    StatusCode = 401,
                    Content = "Api Key is not valid"
                };
                return;
            }

            await next();
        }
    }
}
