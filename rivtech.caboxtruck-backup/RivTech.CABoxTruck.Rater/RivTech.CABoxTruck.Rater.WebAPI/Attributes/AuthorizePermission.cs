﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System;
using System.Linq;
using System.Security.Claims;

namespace RivTech.CABoxTruck.Rater.WebAPI.Attributes
{
    public class AuthorizePermission : AuthorizeAttribute, IAuthorizationFilter
    {
        public PermissionsEnum Permission { get; set; }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var identityClaims = context.HttpContext.User.Identity as ClaimsIdentity;
            var permissionsClaim = identityClaims.Claims.Where(x => x.Type.Equals("permissions")).SingleOrDefault();
            if(permissionsClaim == null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var userPermissions = permissionsClaim.Value.ToString().Split(',').Select(x => Convert.ToInt32(x));
            if (!userPermissions.Contains((int)Permission))
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
