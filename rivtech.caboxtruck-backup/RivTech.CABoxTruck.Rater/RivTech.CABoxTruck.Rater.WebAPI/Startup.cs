using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System;
using System.IO;
using System.Text;

namespace RivTech.CABoxTruck.Rater.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = Int32.MaxValue;
            });

            services.AddCors();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("default")));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["Jwt:SecretKey"])
                        ),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddOptions();
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                }
            );
            services.Configure<RaterSettings>(Configuration.GetSection(RaterSettings.Key));

            IFileProvider physicalProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            services.AddSingleton(physicalProvider);

            services.AddRaterServices();

            services.AddHangfire(c => c.UseMemoryStorage());
            services.AddHangfireServer();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IHostApplicationLifetime applicationLifetime,
            IPremiumRaterService premiumRaterSerivce,
            IRecurringJobManager recurringJobManager,
            ITempFileService tempFileService,
            RaterSettings raterSettings,
            AppDbContext dbContext,
            ILogger<Startup> logger
        )
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder.WithOrigins(Configuration["AllowedOrigins"].Split(",".ToCharArray()))
                //builder.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod()
                );

            if (env.IsProduction())
                app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });

            // All temporary files generated will be here while the system is running.
            Directory.CreateDirectory("temp");

            // Create background job for removing idle raters.
            string idleRaterJobId = Guid.NewGuid().ToString();
            recurringJobManager.AddOrUpdate(
                idleRaterJobId,
                () => premiumRaterSerivce.RemoveIdleAsync(),
                raterSettings.ScheduleRemoveIdleRater);

            // Create background job for removing idle raters.
            string clearTempFilesJobId = Guid.NewGuid().ToString();
            recurringJobManager.AddOrUpdate(
                clearTempFilesJobId,
                () => tempFileService.ClearTempFiles(),
                raterSettings.ScheduleClearTempDir);

            // Create background job for removing suspended raters.
            string suspendedRaterJobId = Guid.NewGuid().ToString();
            recurringJobManager.AddOrUpdate(
                suspendedRaterJobId,
                () => premiumRaterSerivce.RemoveSuspendedRaters(),
                raterSettings.ScheduleRemoveSuspendedRater);

            // Create background job for removing stuck unloaded raters.
            string unloadedRaterJobId = Guid.NewGuid().ToString();
            recurringJobManager.AddOrUpdate(
                unloadedRaterJobId,
                () => premiumRaterSerivce.ForceCloseStuckUnloaded(),
                raterSettings.ScheduleRemoveUnloadedRater);

            applicationLifetime.ApplicationStopping.Register(() =>
            {
                // Remove job.
                recurringJobManager.RemoveIfExists(idleRaterJobId);
                recurringJobManager.RemoveIfExists(suspendedRaterJobId);
                recurringJobManager.RemoveIfExists(unloadedRaterJobId);
                recurringJobManager.RemoveIfExists(clearTempFilesJobId);
                logger.LogInformation("Hangfire jobs have been removed.");

                // Clean up resources used by ExcelRaterPoolCollection before shutting down system.

                Console.WriteLine("Hangfire jobs have been removed.");
                try
                {
                    premiumRaterSerivce.ShutdownRaterPoolAsync().Wait();
                    logger.LogInformation("Premium rater pool shutdown.");
                }
                catch(Exception e)
                {
                    logger.LogError("Failed shutting down premium rater pool.", e);
                }
            });

            dbContext.Database.Migrate();
        }

    }
}
