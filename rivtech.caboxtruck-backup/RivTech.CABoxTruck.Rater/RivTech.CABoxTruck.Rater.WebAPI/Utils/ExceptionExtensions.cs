﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.WebAPI.Utils
{
    public static class ExceptionExtensions
    {
        public static Exception GetInnerMostException(this Exception e)
        {
            Exception innerEx = e;
            while(innerEx.InnerException != null)
            {
                innerEx = e.InnerException;
            }
            return e;
        }
    }
}
