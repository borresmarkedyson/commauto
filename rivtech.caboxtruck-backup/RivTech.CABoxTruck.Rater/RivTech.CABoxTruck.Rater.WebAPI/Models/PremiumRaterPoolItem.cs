﻿using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;

namespace RivTech.CABoxTruck.Rater.WebAPI.Models
{
    public class PremiumRaterPoolItem
    {
        public static PremiumRaterPoolItem CreateFromPremiumRater(IPremiumRater premiumRater, RaterSettings raterSettings)
        {
            return new PremiumRaterPoolItem
            {
                SubmissionNumber = premiumRater.SubmissionNumber,
                OptionId = premiumRater.OptionId,
                Status = premiumRater.Status.ToString(),
                CreatedAt = premiumRater.CreatedDate,
                IsIdle = premiumRater.IsIdle(raterSettings.RaterIdleTimeout),
                CanBeUnloaded = premiumRater.CanBeUnloaded,
                Elapsed = premiumRater.Elapsed,
                ResumableAt = premiumRater.Status == PremiumRaterStatus.Suspended
                    ? raterSettings.RaterSuspendedTimeout - (int)premiumRater.SuspendedTimeSpan.Value.TotalSeconds
                    : 0
            };
        }


        public string SubmissionNumber { get; set; }
        public string OptionId { get; private set; } = null;
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsIdle { get; set; }
        public bool CanBeUnloaded { get; set; }
        public double Elapsed { get; set; }
        public int ResumableAt { get; set; }
    }
}
