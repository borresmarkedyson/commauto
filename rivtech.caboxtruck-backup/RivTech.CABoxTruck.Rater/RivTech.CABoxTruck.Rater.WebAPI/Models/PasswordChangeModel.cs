﻿using System.ComponentModel.DataAnnotations;

namespace RivTech.CABoxTruck.Rater.WebAPI.Models
{
    public class PasswordChangeModel
    {
        [Required]
        public string CurrentPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmNewPassword { get; set; }
    }
}
