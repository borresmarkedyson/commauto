﻿using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.WebAPI.Models
{
    public class RaterResponse
    {
        public string Error { get; private set; }
        public dynamic Data { get; private set; }
        public bool Success { get; private set; }

        internal static RaterResponse FromRater(IPremiumRater premiumRater)
        {
            return new RaterResponse
            {
                Success = true,
                Data = new
                {
                    premiumRater.SubmissionNumber,
                    premiumRater.OptionId,
                    premiumRater.InceptionDate,
                    InputValues = premiumRater.InputValues.Values,
                    Result = premiumRater.Result.Data,
                    premiumRater.RatingElapsed
                }
            };
        }

        internal static ActionResult<RaterResponse> Failed(string message, Dictionary<string, object> data = null)
        {
            return new RaterResponse
            {
                Success = false,
                Error = message,
                Data = data
            };
        }
    }
}
