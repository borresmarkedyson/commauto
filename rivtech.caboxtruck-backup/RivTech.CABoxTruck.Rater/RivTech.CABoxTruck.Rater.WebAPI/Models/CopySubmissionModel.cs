﻿namespace RivTech.CABoxTruck.Rater.WebAPI.Models
{
    public class CopySubmissionModel
    {
        public string From { get; set; }
        public string To { get; set; }
    }
}
