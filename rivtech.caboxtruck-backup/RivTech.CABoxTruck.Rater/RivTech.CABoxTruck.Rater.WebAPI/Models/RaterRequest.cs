﻿using Newtonsoft.Json.Linq;
using System;

namespace RivTech.CABoxTruck.Rater.WebAPI.Models
{
    public class RaterRequest
    {
        public string SubmissionNumber { get; set; }
        public DateTime InceptionDate { get; set; }
        public string OptionId { get; set; }
        public int? EndorsementNumber { get; set; }
        public JObject InputValues { get; set; }
        public string VehicleIdMappingKey { get; set; }
    }
}
