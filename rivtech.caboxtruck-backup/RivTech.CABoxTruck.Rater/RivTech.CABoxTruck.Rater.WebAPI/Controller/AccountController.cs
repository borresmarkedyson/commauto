﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.WebAPI.Models;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IAuthService _authService;
        private readonly IUserService _userService;

        public AccountController(
            ILogger<AccountController> logger,
            IAuthService authService,
            IUserService userService
        )
        {
            _logger = logger;
            _authService = authService;
            _userService = userService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            string username = HttpContext.User.Identity.Name;
            User user = _userService.GetByUsername(username);
            return Ok(user);
        }

        [HttpPut]
        [Route("change-password")]
        [Authorize]
        public IActionResult ChangePassword([FromBody] PasswordChangeModel model)
        {
            try
            {
                string username = HttpContext.User.Identity.Name;
                var user = new User
                {
                    Username = username,
                    Password = model.CurrentPassword
                };

                if (!model.NewPassword.Equals(model.ConfirmNewPassword))
                    return BadRequest("New and Confirm Password did not match.");

                if (_authService.Authenticate(user) is null)
                    return BadRequest("Incorrect current password.");

                _userService.ChangePassword(username, model.NewPassword);

                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while changing password");
                return new StatusCodeResult(500);
            }
        }


    }
}
