﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.WebAPI.Models;
using RivTech.CABoxTruck.Rater.WebAPI.Attributes;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller
{

    [Route("api/[controller]")]
    [ApiController]
    public class PremiumRaterController : ControllerBase
    {
        private readonly RaterSettings _raterSettings;
        private readonly IPremiumRaterService _premiumRaterService;
        private readonly ILogger<PremiumRaterController> _logger;

        public PremiumRaterController(
            ILogger<PremiumRaterController> logger,
            IPremiumRaterService premiumRaterService,
            IOptions<RaterSettings> raterSettingsOptions
        )
        {
            _logger = logger;
            _premiumRaterService = premiumRaterService;
            _raterSettings = raterSettingsOptions.Value;
        }

        [HttpGet]
        [Route("test")]
        public string Test()
        {
            return "OK";
        }


        [HttpPost]
        [Route("rate/{id}")]
        public ActionResult<RaterResponse> Rate(string id, [FromBody] RaterRequest request, bool? fromPolicy = null, bool? forIssuance = null)
        {
            IPremiumRater rater = null;
            RateCommand rateCommand = null;
            try
            {
                rateCommand = new RateCommand(
                    submissionNumber: id,
                    inceptionDate: request.InceptionDate,
                    inputValues: new InputValues(request.InputValues),
                    optionId: fromPolicy == true ? "" : request.OptionId,
                    endorsementNumber: request.EndorsementNumber,
                    fromPolicy: fromPolicy,
                    forIssuance: forIssuance
                );
                rater = _premiumRaterService.Rate(rateCommand);
                return Ok(RaterResponse.FromRater(rater));
            }
            catch (RaterNotReadyException e)
            {
                _logger.LogError(e, e.Message);
                return RaterResponse.Failed(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return RaterResponse.Failed(e.Message);
            }
            finally
            {
                _ = Task.Run(async () =>
                  {
                      try
                      {
                          if (rater != null)
                          {
                              await using (rater)
                              {
                                  await rater.WrapUpAsync();

                                  if (_raterSettings.AllowDump)
                                  {
                                      await _premiumRaterService.DumpRaterAsync(rater);
                                  }
                                  if (forIssuance == true)
                                  {
                                      rater.EndorsementNumber = request.EndorsementNumber;
                                      await _premiumRaterService.SaveRaterFromIssuanceAsync(rater);
                                  }
                              }
                          }
                      }
                      catch (Exception e)
                      {
                          _logger.LogError(e, "Error cleaning up rater.");
                          //Console.WriteLine($"Failed to dump rater.\n{e.StackTrace}");
                      }
                  });
            }
        }

        [HttpGet]
        [AuthorizePermission(Permission = PermissionsEnum.ReadRaterPoolItems)]
        [Route("items")]
        public ActionResult GetAll([FromQuery] bool? runningOnly = null)
        {
            try
            {

                var list = _premiumRaterService.GetRaterPoolItems(runningOnly)
                    .Select(x => PremiumRaterPoolItem.CreateFromPremiumRater(x, _raterSettings));
                return Ok(list);
            }
            catch (Exception e)
            {
                _logger?.LogError(e, e.Message);
                return StatusCode(500, $"Error fetching rater pool items.");
            }
        }

        [HttpPost]
        [Route("preload/{id}")]
        [ApiKey]
        public async Task<ActionResult> Preload(string id,
            [FromQuery] DateTime inceptionDate,
            [FromQuery] int? itemCount = null, // total Number of pool item from ui
            [FromQuery] bool? fromPolicy = null
        )
        {
            try
            {
                await _premiumRaterService.PreloadRatersAsync(id, inceptionDate);

                return Ok($"Rater(s) of {id} has been preloaded.");
            }
            catch (TimeoutException e)
            {
                _logger.LogError($"Submission: {id}. {e.Message}", e);
                return StatusCode(500, e.Message);
            }
            catch (Exception e)
            {
                e.Data.Add("submissionNumber", id);
                _logger?.LogError(e, e.Message);
                return StatusCode(500, $"Error preloading rater.");
            }
        }

        [HttpDelete]
        [Route("unload/{id}")]
        [ApiKey]
        public ActionResult Unload(
            string id,  // SubmissionNumber
            [FromQuery] int? itemCount = null, // total Number of pool item from ui
            [FromQuery] bool? fromPolicy = null
         )
        {
            try
            {
                _premiumRaterService.SuspendRater(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger?.LogError(e, e.Message);
                return StatusCode(500, $"Error unloading raters of {id}.");
            }
        }

        [HttpDelete]
        [AuthorizePermission(Permission = PermissionsEnum.ManageRaterPoolItems)]
        [Route("force-unload/{id}")]
        public async Task<ActionResult> ForceUnload(string id)
        {
            try
            {
                await _premiumRaterService.ForceUnloadAsync(id);
                return Ok($"Successfully unload raters of {id}.");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return new StatusCodeResult(500);
            }
        }
    }
}
