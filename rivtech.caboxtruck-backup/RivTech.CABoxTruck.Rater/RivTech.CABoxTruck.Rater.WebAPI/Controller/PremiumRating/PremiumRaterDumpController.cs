﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.WebAPI.Models;
using RivTech.CABoxTruck.Rater.WebAPI.Attributes;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller.PremiumRating
{
    [Route("api/[controller]")]
    [ApiController]
    public class PremiumRaterDumpController : ControllerBase
    {
        private readonly ILogger<PremiumRaterDumpController> _logger;
        private readonly IPremiumRaterService _premiumRaterService;

        public PremiumRaterDumpController(
            ILogger<PremiumRaterDumpController> logger,
            IPremiumRaterService premiumRaterService
        )
        {
            _logger = logger;
            _premiumRaterService = premiumRaterService;
        }

        [HttpGet]
        [Route("Get")]
        [AuthorizePermission(Permission = PermissionsEnum.ReadUsedRaters)]
        public ActionResult<PremiumRaterDump[]> Get([FromQuery] PremiumRaterDumpFilter filter)
        {
            return Ok(_premiumRaterService.Get(filter));
        }

        [HttpGet]
        [Route("Download")]
        [AuthorizePermission(Permission = PermissionsEnum.ReadUsedRaters)]
        public ActionResult Download(
            [FromQuery] DateTime date,
            [FromQuery] string filename
        ) {
            try
            {
                FileContent content = _premiumRaterService.DownloadDumpFile(date, filename);
                string mimeType = "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                var result = new FileContentResult(content.Bytes, mimeType)
                {
                    FileDownloadName = filename
                };
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while downloading dump file.");
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
