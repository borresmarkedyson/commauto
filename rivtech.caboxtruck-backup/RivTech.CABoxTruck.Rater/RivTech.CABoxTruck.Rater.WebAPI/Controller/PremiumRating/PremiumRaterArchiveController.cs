﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.WebAPI.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller.PremiumRating
{
    [Route("api/[controller]")]
    [ApiController]
    public class PremiumRaterArchiveController : ControllerBase
    {
        private readonly ILogger<PremiumRaterArchiveController> _logger;
        private readonly IPremiumRaterService _premiumRaterService;

        public PremiumRaterArchiveController(
            ILogger<PremiumRaterArchiveController> logger,
            IPremiumRaterService premiumRaterService
        ) {
            _logger = logger;
            _premiumRaterService = premiumRaterService;
        }


        [HttpGet]
        [Route("Get")]
        public async Task<PremiumRaterArchive[]> Get([FromQuery] PremiumRaterArchiveFilter filter)
        {
            return await _premiumRaterService.GetArchivesAsync(filter);
        }


        [HttpGet]
        [Route("Download/{id}")]
        [AuthorizePermission(Permission = PermissionsEnum.ReadUsedRaters)]
        public async Task<ActionResult> Download(Guid id)
        {
            try
            {
                byte[] content = await _premiumRaterService.DownloadArchiveFileAsync(id);
                var archive = await _premiumRaterService.GetArchiveByIdAsync(id);
                string mimeType = "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                var result = new FileContentResult(content, mimeType)
                {
                    FileDownloadName = archive.FileName
                };
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while downloading dump file.");
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete]
        public  async Task<ActionResult> Delete([FromQuery] string submissionNumber, [FromQuery] int endorsementNumber)
        {
            try
            {
                await _premiumRaterService.DeleteArchive(submissionNumber, endorsementNumber);
                return Ok();
            }
            catch(Exception e)
            { 
                _logger.LogError(e, $"Error deleting rater for submission {submissionNumber}({endorsementNumber}).");
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
