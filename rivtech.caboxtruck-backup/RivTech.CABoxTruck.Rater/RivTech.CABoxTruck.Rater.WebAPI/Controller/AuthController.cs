﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System;
using System.Security;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IAuthService _authService;

        public AuthController(
            ILogger<AuthController> logger,
            IAuthService authService
        )
        {
            _logger = logger;
            _authService = authService;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] User login)
        {
            try
            {
                var user = _authService.Authenticate(login);
                if (user is null)
                    return Unauthorized();

                return Ok(new
                {
                    user.Username,
                    user.DisplayName,
                    user.Token,
                    user.Permissions
                });
            }
            catch(SecurityException e)
            {
                return Unauthorized(e.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return new StatusCodeResult(500);
            }
        }

    }
}
