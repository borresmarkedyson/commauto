﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.WebAPI.Attributes;
using System;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;

        public UserController(
            ILogger<UserController> logger,
            IUserService userService
        )
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        [Route("all")]
        [AuthorizePermission(Permission = PermissionsEnum.ReadUserDetails)]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_userService.GetAll());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error getting users.");
                return new StatusCodeResult(500);
            }
        }

        [HttpPost]
        [AuthorizePermission(Permission = PermissionsEnum.ManageUsers)]
        public IActionResult Post([FromBody] User user)
        {
            try
            {

                _userService.Add(user);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error creating user.");
                return new StatusCodeResult(500);
            }
        }

        [HttpPut]
        [AuthorizePermission(Permission = PermissionsEnum.ManageUsers)]
        public IActionResult Put([FromBody] User user)
        {
            try
            {
                _userService.Update(user);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error updating user.");
                return new StatusCodeResult(500);
            }
        }

        [HttpDelete]
        [AuthorizePermission(Permission = PermissionsEnum.ManageUsers)]
        public IActionResult Delete()
        {
            throw new NotImplementedException();
        }
    }
}
