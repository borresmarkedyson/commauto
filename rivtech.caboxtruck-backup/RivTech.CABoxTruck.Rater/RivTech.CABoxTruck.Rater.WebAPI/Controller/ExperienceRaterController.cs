﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.WebAPI.Attributes;
using RivTech.CABoxTruck.Rater.WebAPI.Models;
using RivTech.CABoxTruck.Rater.WebAPI.Utils;

namespace RivTech.CABoxTruck.Rater.WebAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiKeyAttribute]
    public class ExperienceRaterController : ControllerBase
    {
        private readonly IExperienceRaterService _experienceRaterService;

        public ExperienceRaterController(
            ILogger<ExperienceRaterController> logger,
            IExperienceRaterService experienceRaterService
        )
        {
            Logger = logger;
            _experienceRaterService = experienceRaterService;
        }

        ILogger<ExperienceRaterController> Logger { get; }

        /// <summary>
        /// Must be called everytime a new submission is created.
        /// </summary>
        [HttpPost]
        [Route("create/{id}")]
        public async Task<IActionResult> Create(string id, [FromQuery] DateTime effectiveDate)
        {
            try
            {
                await _experienceRaterService.CreateInitialAsync(id, effectiveDate);
                return Ok();
            }
            catch(Exception e)
            {
                Logger.LogError(e, e.Message);
                return StatusCode(500, "Error creating experience rater.");
            }
        }

        [HttpPost]
        [Route("download/{id}")]
        public async Task<ActionResult> Download(string id, [FromQuery] DateTime effectiveDate, [FromBody] JObject requestBody)
        {
            try
            {
                using ExperienceRater experienceRater = await _experienceRaterService
                    .DownloadAsync(id, effectiveDate, new InputValues(requestBody));

                string mimeType = "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                var result = new FileContentResult(experienceRater.Content.Bytes, mimeType)
                {
                    FileDownloadName = $"Experience Rater{experienceRater.Content.Extension}"
                };
                return result;
            }
            catch (Exception e)
            {
                Logger.LogError(e, e.Message);
                return StatusCode(500, $"Failed downloading rater of {id}.");
            }
        }

        [HttpPost]
        [Route("upload/{id}")]
        public async Task<ActionResult> Upload(string id, [FromQuery] DateTime effectiveDate)
        {
            try
            {
                IFormFile postedFile = Request.Form.Files[0];
                if (postedFile.Length == 0)
                    return BadRequest("No file found from request.");

                // Copy uploaded file into fileContent.
                using Stream fileStream = postedFile.OpenReadStream();
                byte[] content = new byte[postedFile.Length];
                fileStream.Read(content, 0, (int)postedFile.Length);

                // Save experience rater then return factors.
                string extension = Path.GetExtension(postedFile.FileName);
                var fileContent = new FileContent(content, extension);

                OutputValues factors = await _experienceRaterService.UploadAsync(id, effectiveDate, fileContent);
                return Ok(factors);
            }
            catch (RaterException e)
            {
                Logger.LogError(e, e.Message);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                Logger.LogError(e.GetInnerMostException(), e.Message);
                return StatusCode(500, $"Failed uploading rater for risk {id}.");
            }
        }

        [HttpGet]
        [Route("exists/{id}")]
        public ActionResult<bool> Exists(string id)
        {
            try
            {
                bool exists = _experienceRaterService.Exists(id);
                return Ok(exists);
            }
            catch (Exception e)
            {
                Logger.LogError(e, e.Message);
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        [Route("CopyToNewSubmission")]
        public async Task<ActionResult> CopyToNewSubmission([FromBody] CopySubmissionModel model)
        {
            try
            {
                await _experienceRaterService.CopyToNewSubmission(model.From, model.To);
                if (!_experienceRaterService.Exists(model.To))
                    throw new RaterException("Failed to copy experience rater.");
                return Ok();
            }
            catch(Exception e)
            {
                Logger.LogError(e, $"Error copying submission from {model.From} to {model.To}.");
                return BadRequest();
            }
        }
    }
}
