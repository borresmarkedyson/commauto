import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';


export const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./pages/dashboard-page/dashboard-page.module').then(m => m.DashboardPageModule)
      },

      {
        path: 'premium-rater-pool',
        loadChildren: () => import('./pages/premium-rater-pool-page/premium-rater-pool-page.module').then(m => m.PremiumRaterPoolPageModule)
      },

      {
        path: 'premium-rater-dump',
        loadChildren: () => import('./pages/premium-rater-dump-page/premium-rater-dump-page.module').then(m => m.PremiumRaterDumpPageModule)
      },

      {
        path: 'policy-premium-rater',
        loadChildren: () => import('./pages/policy-premium-rater-page/policy-premium-rater-page.module').then(m => m.PolicyPremiumRaterPageModule)
      },

      {
        path: 'users',
        loadChildren: () => import('./pages/user-page/user-page.module').then(m => m.UserPageModule)
      },
       
      {
        path: 'account',
        loadChildren: () => import('./pages/account-page/account-page.module').then(m => m.AccountPageModule)
      },


      { path: '**', redirectTo: '/login' }

    ]
  },

  { 
    path: 'login',
    component: LoginPageComponent
  },

  { path: '**', redirectTo: '/login' }
  // { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
