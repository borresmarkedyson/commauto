import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-sidebar-layout',
  templateUrl: './main-sidebar-layout.component.html',
  styleUrls: ['./main-sidebar-layout.component.scss']
})
export class MainSidebarLayoutComponent implements OnInit {


  sidebarVisible: boolean = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => { this.sidebarVisible = true; });
  }

}
