import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit {

  constructor(private  router: Router) { 
        // Redirect to login page when not authenticated.
    if (!AuthService.getToken()) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit(): void {
  }

}
