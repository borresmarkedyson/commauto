import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../features/user/user';
import { AuthService } from '../../../services/auth.service';
import { AssetUtils } from '../../../utilities/asset-utils';

@Component({
  selector: 'app-header-admin',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() leftSidebarAvailable: boolean = true;

  uname: string;
  userIconPath: string;
  fullLogoPath: string;
  canManageUsers: boolean = false;
  user: User;
  mainMenuList: any[] = [
    { controllerName: "['/users']", menuLabel: 'Users' }
  ];

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.userIconPath = AssetUtils.mapPath("assets/img/avatars/user-icon.png");
    this.fullLogoPath = AssetUtils.mapPath("assets/img/brand/logo.png");
  }

  ngOnInit() {
    this.user = AuthService.getUser();
    this.canManageUsers = this.user.permissions.find(x => x == 101) != null;

    console.log(this.canManageUsers);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
