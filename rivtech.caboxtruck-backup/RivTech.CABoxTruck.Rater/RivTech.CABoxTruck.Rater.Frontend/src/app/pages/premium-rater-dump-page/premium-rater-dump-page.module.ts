import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRaterDumpPageComponent } from './premium-rater-dump-page.component';
import { PremiumRaterDumpModule } from '../../features/premium-rater-dump/premium-rater-dump.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PremiumRaterDumpPageComponent,
  }
];

@NgModule({
  declarations: [
    PremiumRaterDumpPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PremiumRaterDumpModule
  ],

})
export class PremiumRaterDumpPageModule { }
