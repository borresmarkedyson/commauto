import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PremiumRaterDumpFilterComponent } from '../../features/premium-rater-dump/premium-rater-dump-filter/premium-rater-dump-filter.component';
import { PremiumRaterDumpListComponent } from '../../features/premium-rater-dump/premium-rater-dump-list/premium-rater-dump-list.component';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-premium-rater-dump-page',
  templateUrl: './premium-rater-dump-page.component.html',
  styleUrls: ['./premium-rater-dump-page.component.css']
})
export class PremiumRaterDumpPageComponent implements OnInit, AfterViewInit {

  @ViewChild(PremiumRaterDumpFilterComponent) filterView: PremiumRaterDumpFilterComponent;
  @ViewChild(PremiumRaterDumpListComponent) listView: PremiumRaterDumpListComponent;

  constructor(
    private uiService: UiService
  ) { }

  async ngAfterViewInit() {
    await this.filterView.filter();
  }

  ngOnInit(): void {
  }
  
  async onFilter(data) {
    try {
      this.uiService.blockUI();
      await this.listView.filterList(data);
    } catch (e) {
      this.uiService.toastError("Error filtering list.");
    } finally {
      this.uiService.unblockUI();
    }
  }

}
