import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumRaterDumpPageComponent } from './premium-rater-dump-page.component';

describe('PremiumRaterDumpPageComponent', () => {
  let component: PremiumRaterDumpPageComponent;
  let fixture: ComponentFixture<PremiumRaterDumpPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumRaterDumpPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumRaterDumpPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
