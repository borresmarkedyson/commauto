import { Component, ViewChild } from '@angular/core';
import { UserListComponent } from '../../features/user/user-list/user-list.component';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent {

  @ViewChild(UserListComponent) 
  userList: UserListComponent;

}
