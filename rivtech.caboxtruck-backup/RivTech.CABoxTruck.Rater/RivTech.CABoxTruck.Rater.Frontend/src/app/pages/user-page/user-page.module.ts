import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPageComponent } from './user-page.component';
import { UserModule } from '../../features/user/user.module';
import { RouterModule, Routes } from '@angular/router';
import { UserFormModalComponent } from '../../features/user/user-form/user-form-modal/user-form-modal.component';


const routes: Routes = [
  {
    path: '',
    component: UserPageComponent,
  }
];

@NgModule({
  declarations: [UserPageComponent],
  imports: [
    CommonModule,
    UserModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  entryComponents: [
    UserFormModalComponent
  ]
})
export class UserPageModule { }
