import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumRaterPoolPageComponent } from './premium-rater-pool-page.component';

describe('PremiumRaterPoolPageComponent', () => {
  let component: PremiumRaterPoolPageComponent;
  let fixture: ComponentFixture<PremiumRaterPoolPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumRaterPoolPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumRaterPoolPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
