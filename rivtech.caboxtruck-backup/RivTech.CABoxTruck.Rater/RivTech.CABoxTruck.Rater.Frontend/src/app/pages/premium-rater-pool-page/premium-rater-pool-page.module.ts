import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRaterPoolPageComponent } from './premium-rater-pool-page.component';
import { PremiumRaterPoolModule } from '../../features/premium-rater-pool/premium-rater-pool.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PremiumRaterPoolPageComponent,
  }
];

@NgModule({
  declarations: [PremiumRaterPoolPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PremiumRaterPoolModule,
  ],
  exports: [RouterModule]
})
export class PremiumRaterPoolPageModule { }
