import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PolicyPremiumRaterModule } from '../../features/policy-premium-rater/policy-premium-rater.module';
import { PolicyPremiumRaterPageComponent } from './policy-premium-rater-page.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PolicyPremiumRaterPageComponent,
  }
];

@NgModule({
  declarations: [
    PolicyPremiumRaterPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PolicyPremiumRaterModule,
  ]
})
export class PolicyPremiumRaterPageModule { }
