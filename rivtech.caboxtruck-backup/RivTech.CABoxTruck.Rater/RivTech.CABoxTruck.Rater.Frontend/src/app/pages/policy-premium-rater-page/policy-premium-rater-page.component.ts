import { Component, OnInit, ViewChild } from '@angular/core';
import { of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { PolicyPremiumRaterFilterComponent } from '../../features/policy-premium-rater/policy-premium-rater-filter/policy-premium-rater-filter.component';
import { PolicyPremiumRaterListComponent } from '../../features/policy-premium-rater/policy-premium-rater-list/policy-premium-rater-list.component';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-policy-premium-rater-page',
  templateUrl: './policy-premium-rater-page.component.html',
  styleUrls: ['./policy-premium-rater-page.component.css']
})
export class PolicyPremiumRaterPageComponent implements OnInit {

  @ViewChild(PolicyPremiumRaterFilterComponent) filterView: PolicyPremiumRaterFilterComponent;
  @ViewChild(PolicyPremiumRaterListComponent) listView: PolicyPremiumRaterListComponent;

  constructor(private uiService: UiService) { }

  ngAfterViewInit() {
    this.filterView.filter();
  }

  ngOnInit(): void {
  }
  
  onFilter(data) {
    this.uiService.blockUI();
    this.listView.filterList(data).pipe(
      catchError(() =>{
         this.uiService.toastError("Error filtering list.") 
         return of();
      }),
      finalize(() => this.uiService.unblockUI())
    ).subscribe();
  }
}
