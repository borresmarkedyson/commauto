import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AccountPageComponent } from './account-page.component';
import { AccountModule } from '../../features/account/account.module';


const routes: Routes = [
  {
    path: '',
    component: AccountPageComponent,
    data: {
      controllerName: '/account'
    },
  }
];

@NgModule({
  declarations: [AccountPageComponent],
  imports: [
    CommonModule,
    AccountModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountPageModule { }
