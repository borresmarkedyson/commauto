import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {

  allowDump: boolean;

  constructor() {
    this.allowDump = environment.allowDump;
   }

  ngOnInit(): void {
  }

}
