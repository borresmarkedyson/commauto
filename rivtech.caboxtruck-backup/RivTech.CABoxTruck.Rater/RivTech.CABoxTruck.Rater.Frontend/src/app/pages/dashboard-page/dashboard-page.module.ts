import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRaterPoolModule } from '../../features/premium-rater-pool/premium-rater-pool.module';
import { DashboardPageComponent } from './dashboard-page.component';
import { DashboardModule } from '../../features/dashboard/dashboard.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DashboardPageComponent,
    data: {
      controllerName: '/dashboard'
    },
  }
];

@NgModule({
  declarations: [DashboardPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PremiumRaterPoolModule,
    DashboardModule
  ],
  exports: [RouterModule],

})
export class DashboardPageModule { }
