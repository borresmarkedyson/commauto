import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { JwtModule } from "@auth0/angular-jwt";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';



import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { LeftSidebarComponent } from './layout/components/left-sidebar/left-sidebar.component';
import { RightSidebarComponent } from './layout/components/right-sidebar/right-sidebar.component';
import { MainSidebarLayoutComponent } from './layout/main-sidebar-layout/main-sidebar-layout.component';
import { FooterComponent } from './layout/components/footer/footer.component';
import { HeaderComponent } from './layout/components/header/header.component';
import { ServicesModule } from './services/services.module';
import { PremiumRaterDumpPageModule } from './pages/premium-rater-dump-page/premium-rater-dump-page.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginModule } from './features/login/login.module';
import { ToastrModule } from 'ngx-toastr';
import { DashboardPageModule } from './pages/dashboard-page/dashboard-page.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TokenInterceptor } from './shared/token.interceptor';
import { UserPageModule } from './pages/user-page/user-page.module';
import { AuthService } from './services/auth.service';
import { UiService } from './services/ui.service';
import { AuthInterceptor } from './shared/auth-interceptor';
import { PolicyPremiumRaterPageComponent } from './pages/policy-premium-rater-page/policy-premium-rater-page.component';

@NgModule({
  imports: [

    LoginModule,
    DashboardPageModule,
    UserPageModule,
    ServicesModule,
    AppRoutingModule,
    
    // angular modules
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    ToastrModule.forRoot(),

    // 3rd party modules
    NgbModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: AuthService.getToken
      }
    }),
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftSidebarComponent,
    MainLayoutComponent,
    MainSidebarLayoutComponent,
    RightSidebarComponent,
    FooterComponent,
    LoginPageComponent,
  ],
  providers: [
    UiService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
