export interface AuthResult {
    success: boolean;
    error?: string;
    data?: any;
}