
export class DateUtils {

    public static getTimezoneOffsetInHours() {
        var dt = new Date();
        let offset = dt.getTimezoneOffset() / -60;
        return offset;
    }

}