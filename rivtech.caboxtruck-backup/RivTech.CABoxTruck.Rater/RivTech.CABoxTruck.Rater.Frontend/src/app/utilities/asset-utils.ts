import { enableProdMode, isDevMode } from "@angular/core";
import { environment } from "../../environments/environment";

export class AssetUtils {


    public static mapPath(path: string) {
        if(isDevMode()) {
            return path;
        }
        if(path[0] == '/')
            path = path.substring(1);
        return '/raterUI/' + path;
    }

}