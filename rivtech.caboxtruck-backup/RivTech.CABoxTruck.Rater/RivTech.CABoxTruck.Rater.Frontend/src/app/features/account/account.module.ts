import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { PasswordChangeFormComponent } from './password-change-form/password-change-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from './account.service';
import { PasswordChangeModalComponent } from './password-change-modal/password-change-modal.component';


@NgModule({
  declarations: [AccountDetailsComponent, PasswordChangeFormComponent, PasswordChangeModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule
  ],
  exports: [
    AccountDetailsComponent,
    PasswordChangeModalComponent,
    PasswordChangeFormComponent
  ],
  entryComponents: [
    PasswordChangeModalComponent
  ],
  providers: [
    AccountService,
  ]
})
export class AccountModule { }
