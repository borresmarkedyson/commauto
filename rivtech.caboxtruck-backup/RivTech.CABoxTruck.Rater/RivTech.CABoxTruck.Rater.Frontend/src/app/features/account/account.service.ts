import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { PasswordChangeModel } from "./password-change-form/password-change-model";

@Injectable()
export class AccountService {

    baseUrl: string;

    constructor(
        private httpClient: HttpClient
    ) {
        this.baseUrl = environment.apiUrl;
    }

    changePassword(model: PasswordChangeModel): Promise<Object> {
        let url = `${this.baseUrl}/api/account/change-password`
        return this.httpClient.put(url, model).toPromise();
    }

}