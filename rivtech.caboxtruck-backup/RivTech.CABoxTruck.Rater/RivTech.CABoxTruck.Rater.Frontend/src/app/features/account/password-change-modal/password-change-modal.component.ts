import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UiService } from '../../../services/ui.service';
import { PasswordChangeFormComponent } from '../password-change-form/password-change-form.component';

@Component({
  selector: 'app-password-change-modal',
  templateUrl: './password-change-modal.component.html',
  styleUrls: ['./password-change-modal.component.css']
})
export class PasswordChangeModalComponent implements OnInit {

  @ViewChild('frmChangePassword')
  frmChangePassword: PasswordChangeFormComponent;

  constructor(
    public activeModal: NgbActiveModal,
    private uiService: UiService,
  ) { }

  ngOnInit(): void {
  }

  async submit() {
    let result: any = {};
    try {
      this.frmChangePassword.clearValidation();
      if (!this.frmChangePassword.validate()) {
        return;
      }
      this.uiService.blockUI();
      let result = await this.frmChangePassword.submit();

      if (!result.success)
        throw result.error;

      this.uiService.toastSuccess("Password Changed!");
      this.activeModal.close(true);
    } catch (error) {
      this.uiService.toastError(error);
    } finally {
      this.uiService.unblockUI();
    }
  }

}
