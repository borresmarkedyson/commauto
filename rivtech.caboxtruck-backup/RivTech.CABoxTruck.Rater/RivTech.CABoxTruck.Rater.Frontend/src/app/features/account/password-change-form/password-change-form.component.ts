import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from '../../../services/ui.service';
import { AccountService } from '../account.service';
import { PasswordChangeModel } from './password-change-model';

@Component({
  selector: 'app-password-change-form',
  templateUrl: './password-change-form.component.html',
  styleUrls: ['./password-change-form.component.css']
})
export class PasswordChangeFormComponent implements OnInit {

  frmPasswordChange: FormGroup;
  requiredFieldError: boolean = false;
  newAndConfirmNotMatchError: boolean = false;

  constructor(
    private fb: FormBuilder,
    private accountService: AccountService,
  ) {
    this.frmPasswordChange = this.fb.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  async submit(): Promise<any> {
    let result : any = {
      success: false
    };
    try {
      await this.accountService.changePassword(this.frmPasswordChange.value);
      result.success = true;

      return new Promise(resolve => resolve(result));
    } catch (e) {
      result.error = e.error; 
      return new Promise(resolve => resolve(result));
    }
  }

  public validate() : boolean {
      if (!this.frmPasswordChange.valid) {
        this.requiredFieldError = true;
      }

      let formValues = this.frmPasswordChange.value;
      if (formValues.newPassword != formValues.confirmNewPassword){
        this.newAndConfirmNotMatchError = true;
      }
      return !(this.requiredFieldError || this.newAndConfirmNotMatchError);
  }

  public clearValidation() {
    this.requiredFieldError = false;
    this.newAndConfirmNotMatchError = false;
  }

}
