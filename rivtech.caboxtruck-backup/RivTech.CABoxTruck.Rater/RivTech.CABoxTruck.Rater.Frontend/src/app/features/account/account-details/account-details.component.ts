import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { allowedNodeEnvironmentFlags } from 'process';
import { AuthService } from '../../../services/auth.service';
import { UiService } from '../../../services/ui.service';
import { User } from '../../user/user';
import { UserFormComponent } from '../../user/user-form/user-form.component';
import { PasswordChangeModalComponent } from '../password-change-modal/password-change-modal.component';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {

  user: User;

  passwordChangeModalRef: NgbModalRef;

  constructor(
    private modalService: NgbModal,
    private router: Router,
  ) {

  }

  ngOnInit(): void {
    this.user = AuthService.getUser();
  }

  async onPasswordChange() {
    this.passwordChangeModalRef = this.modalService.open(PasswordChangeModalComponent);
    let result = await this.passwordChangeModalRef.result;
    if (result?.success) {
      this.passwordChangeModalRef.close();
      this.router.navigateByUrl('/login');
    }
  }

}
