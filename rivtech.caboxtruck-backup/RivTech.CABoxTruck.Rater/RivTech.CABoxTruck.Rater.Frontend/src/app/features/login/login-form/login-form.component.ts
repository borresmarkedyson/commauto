import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../services/auth.service';
import { UiService } from '../../../services/ui.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  frmLogin: FormGroup;

  constructor(
    private fb: FormBuilder,
    private uiService: UiService,
    private authService: AuthService,
    private toastrService: ToastrService,
    private router: Router
  ) {
    this.frmLogin = this.fb.group({
      username: '',
      password: ''
    });
  }

  ngOnInit(): void {
    if (!(!AuthService.getToken())) {
      this.router.navigate(['/dashboard']);
    }
  }

  async onSubmit() {
    try {
      this.uiService.blockUI();
      var login = this.frmLogin.value;
      let authResult = await this.authService.login(login.username, login.password);
      if (authResult.success) {
        // redirect 
        this.router.navigate(['/dashboard']);
      } else {
        this.toastrService.error(authResult.error);
      }

    } catch (error) {
      console.log(error);
      this.uiService.toastError("Error logging in.");
    } finally {
      this.uiService.unblockUI();
    }
  }


}
