import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRecentRaterComponent } from './dashboard-recent-rater.component';

describe('DashboardRecentRaterComponent', () => {
  let component: DashboardRecentRaterComponent;
  let fixture: ComponentFixture<DashboardRecentRaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRecentRaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRecentRaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
