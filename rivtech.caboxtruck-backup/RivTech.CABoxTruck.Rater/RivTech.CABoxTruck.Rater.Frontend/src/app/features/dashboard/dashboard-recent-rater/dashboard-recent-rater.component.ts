import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PremiumRaterDumpListComponent } from '../../premium-rater-dump/premium-rater-dump-list/premium-rater-dump-list.component';

@Component({
  selector: 'app-dashboard-recent-rater',
  templateUrl: './dashboard-recent-rater.component.html',
  styleUrls: ['./dashboard-recent-rater.component.css']
})
export class DashboardRecentRaterComponent implements AfterViewInit {

  @ViewChild(PremiumRaterDumpListComponent) listView : PremiumRaterDumpListComponent;

  constructor() { }

  async ngAfterViewInit()  {
    console.log(this.listView.recentCount);
    await this.listView.filterList(null);
  }

}
