import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRunningRaterComponent } from './dashboard-running-rater/dashboard-running-rater.component';
import { DashboardRecentRaterComponent } from './dashboard-recent-rater/dashboard-recent-rater.component';
import { PremiumRaterPoolModule } from '../premium-rater-pool/premium-rater-pool.module';
import { RouterModule } from '@angular/router';
import { PremiumRaterDumpModule } from '../premium-rater-dump/premium-rater-dump.module';
import { DashboardPolicyRaterComponent } from './dashboard-policy-rater/dashboard-policy-rater.component';
import { PolicyPremiumRaterModule } from '../policy-premium-rater/policy-premium-rater.module';



@NgModule({
  declarations: [
    DashboardRunningRaterComponent,
    DashboardRecentRaterComponent,
    DashboardPolicyRaterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    PremiumRaterPoolModule,
    PremiumRaterDumpModule,
    PolicyPremiumRaterModule,
  ],
  exports: [
    DashboardRunningRaterComponent,
    DashboardRecentRaterComponent,
    DashboardPolicyRaterComponent
  ]
})
export class DashboardModule { }
