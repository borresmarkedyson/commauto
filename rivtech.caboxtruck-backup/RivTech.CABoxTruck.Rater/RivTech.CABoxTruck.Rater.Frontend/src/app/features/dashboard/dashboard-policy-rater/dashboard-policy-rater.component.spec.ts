import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPolicyRaterComponent } from './dashboard-policy-rater.component';

describe('DashboardPolicyRaterComponent', () => {
  let component: DashboardPolicyRaterComponent;
  let fixture: ComponentFixture<DashboardPolicyRaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPolicyRaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPolicyRaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
