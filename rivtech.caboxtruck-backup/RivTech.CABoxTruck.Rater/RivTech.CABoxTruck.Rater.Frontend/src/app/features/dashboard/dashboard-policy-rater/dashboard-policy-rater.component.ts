import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PolicyPremiumRaterListComponent } from '../../policy-premium-rater/policy-premium-rater-list/policy-premium-rater-list.component';

@Component({
  selector: 'app-dashboard-policy-rater',
  templateUrl: './dashboard-policy-rater.component.html',
  styleUrls: ['./dashboard-policy-rater.component.css']
})
export class DashboardPolicyRaterComponent implements OnInit, AfterViewInit {

  @ViewChild(PolicyPremiumRaterListComponent) listView : PolicyPremiumRaterListComponent;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.listView.filterList().subscribe();
  }

}
