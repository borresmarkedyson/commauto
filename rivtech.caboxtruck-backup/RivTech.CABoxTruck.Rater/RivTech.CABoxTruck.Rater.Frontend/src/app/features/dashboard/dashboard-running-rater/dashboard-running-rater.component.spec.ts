import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRunningRaterComponent } from './dashboard-running-rater.component';

describe('DashboardRunningRaterComponent', () => {
  let component: DashboardRunningRaterComponent;
  let fixture: ComponentFixture<DashboardRunningRaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRunningRaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRunningRaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
