import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumRaterPoolListComponent } from './premium-rater-pool-list.component';

describe('PremiumRaterPoolListComponent', () => {
  let component: PremiumRaterPoolListComponent;
  let fixture: ComponentFixture<PremiumRaterPoolListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumRaterPoolListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumRaterPoolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
