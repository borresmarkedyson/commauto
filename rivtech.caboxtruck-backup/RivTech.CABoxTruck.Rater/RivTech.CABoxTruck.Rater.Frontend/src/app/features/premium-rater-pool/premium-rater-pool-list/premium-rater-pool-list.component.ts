import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import NotifUtils from '../../../utilities/notif-utils';
import { PremiumRaterPoolItem } from '../premium-rater-pool-item';
import { PremiumRaterPoolService } from '../premium-rater-pool.service';

@Component({
  selector: 'app-premium-rater-pool-list',
  templateUrl: './premium-rater-pool-list.component.html',
  styleUrls: ['./premium-rater-pool-list.component.css']
})
export class PremiumRaterPoolListComponent implements OnInit, OnDestroy {

  @Input() runningOnly: boolean = false;
  @Input() emptyText: string = "No raters in rater pool.";

  currentInterval: any;

  public items: PremiumRaterPoolItem[] = [];
  
  intervalList = [
    { id: 1, text: "1s" },
    { id: 2, text: "2s" },
    { id: 3, text: "3s" },
    { id: 5, text: "5s" },
    { id: 10, text: "10s" },
  ];

  constructor(
    private premiumRaterPoolService: PremiumRaterPoolService,
    private toastrService: ToastrService
  ) { }

  async ngOnInit() {
    this.onIntervalChanged(this.intervalList[0].id);
  }

  ngOnDestroy() {
    // Remove interval once component is destroyed.
    clearInterval(this.currentInterval);
  }

  async unloadIdle(item: PremiumRaterPoolItem) {
    NotifUtils.showInfo(`Unload idle rater of ${item.submissionNumber} (${item.optionId})?`, async () => {
      await this.premiumRaterPoolService.forceUnload(item);
    });
  }

  async forceUnload(item: PremiumRaterPoolItem) {
    NotifUtils.showWarning(`All raters of ${item.submissionNumber} will be unloaded. Do you want to continue?`, async () => {
      await this.premiumRaterPoolService.forceUnload(item);
    });
  }

  onIntervalChanged(interval) {

    clearInterval(this.currentInterval);

    let newInterval = 1000 * interval;
    console.log(newInterval);

    this.currentInterval = setInterval(
      async () => await this.reloadItems(),
      newInterval);

    console.log(this.currentInterval);
  }

  async reloadItems() {
    try {
      this.items = await this.premiumRaterPoolService.getAll({
        runningOnly: this.runningOnly
      });
    } catch(e) {
      console.log(e);
      this.toastrService.error("Error fetching data from server.");
      this.items = [];
      clearInterval(this.currentInterval);
    }
  }

}
