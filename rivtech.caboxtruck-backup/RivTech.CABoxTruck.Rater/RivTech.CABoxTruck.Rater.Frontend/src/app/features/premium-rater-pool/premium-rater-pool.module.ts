import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRaterPoolListComponent } from './premium-rater-pool-list/premium-rater-pool-list.component';
import { PremiumRaterPoolService } from './premium-rater-pool.service';



@NgModule({
  declarations: [
    PremiumRaterPoolListComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PremiumRaterPoolListComponent
  ], 
  providers: [PremiumRaterPoolService]
})
export class PremiumRaterPoolModule { }
