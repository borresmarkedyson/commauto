import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { PremiumRaterPoolItem } from "./premium-rater-pool-item";

@Injectable()
export class PremiumRaterPoolService {

    baseUrl: string;

    constructor(private httpClient: HttpClient) {
        this.baseUrl = environment.apiUrl;
    }

    public getAll(filter?: any) : Promise<PremiumRaterPoolItem[]> {
        let endpoint = `${this.baseUrl}/api/PremiumRater/items`;
        let queryParms = `&runningOnly=${filter?.runningOnly}`;
        let url = `${endpoint}?${queryParms}`;
        return this.httpClient.get<PremiumRaterPoolItem[]>(url)
            .toPromise().then(data => data);
    }

    public forceUnload(item: PremiumRaterPoolItem) : Promise<Object> {
        var url = `${this.baseUrl}/api/PremiumRater/force-unload/${item.submissionNumber}`;
        return this.httpClient.delete(url).toPromise();
    }
}