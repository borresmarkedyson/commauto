export class PremiumRaterPoolItem {
    submissionNumber: string;
    optionId: string;
    status: string;
    createdAt: Date;
    canUnload: boolean;
    resumableAt: number;
}