export interface User {
    userId: number,
    username: string,
    displayName: string,
    password: string,
    permissions: number[],
}