import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { User } from "./user";

@Injectable()
export class UserService {


    baseUrl: string;

    constructor(
        private httpClient: HttpClient
    ) {
        this.baseUrl = environment.apiUrl;
    }

    getAll(): Promise<User[]> {
        let endpoint = `/api/user/all`;
        let url = `${this.baseUrl}${endpoint}`;
        return this.httpClient.get<User[]>(url).toPromise().then(data => data);
    }

    async add(user: User) {
        let endpoint = '/api/user';
        let url = `${this.baseUrl}${endpoint}`;
        await this.httpClient.post(url, user).toPromise();
    }

    async update(user: User) {
        let endpoint = '/api/user';
        let url = `${this.baseUrl}${endpoint}`;
        await this.httpClient.put(url, user).toPromise();
    }

}