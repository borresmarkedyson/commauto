import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './user.service';
import { UserFormModalComponent } from './user-form/user-form-modal/user-form-modal.component';



@NgModule({
  declarations: [UserListComponent, UserFormComponent, UserFormModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    UserListComponent,
    UserFormComponent,
    UserFormModalComponent
  ],
  entryComponents: [
    UserFormModalComponent,
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
