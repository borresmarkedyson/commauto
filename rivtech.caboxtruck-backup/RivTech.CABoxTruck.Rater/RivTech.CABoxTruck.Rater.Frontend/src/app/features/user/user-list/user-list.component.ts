import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../user';
import { UserFormModalComponent } from '../user-form/user-form-modal/user-form-modal.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  items: User[] = [];
  userFormModal: NgbModalRef;

  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) { 
  }

  async ngOnInit() {
    await this.reload();
  }

  async reload() {
    this.items = await this.userService.getAll(); 
  }

  async open(item: User) {
    this.userFormModal = this.modalService.open(UserFormModalComponent);
    this.userFormModal.componentInstance.userData = item;
    this.userFormModal.result.then(x => {
      if(x == "success")
        this.reload();
    });
  }

  closeModal() {
    this.userFormModal?.close();
  }
}
