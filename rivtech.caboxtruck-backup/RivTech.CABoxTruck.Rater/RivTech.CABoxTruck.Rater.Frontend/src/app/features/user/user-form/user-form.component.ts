import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() userData: User;

  frmUser: FormGroup;

  allPermissions = [
    { label: "Read user details", value: 100, checked: false },
    { label: "Manage users", value: 101, checked: false },
    { label: "Read rater pool items", value: 200, checked: false },
    { label: "Manage rater pool items", value: 201, checked: false },
    { label: "Read used raters", value: 300, checked: false },
  ];

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {

    this.frmUser = this.fb.group({
      userId: this.userData?.userId,
      displayName: this.userData?.displayName,
      username: this.userData?.username,
      password: this.userData?.password,
      permissions: this.fb.array([])
    });

    for (let p of this.allPermissions) {
      var formGroup: any = p;
      formGroup.checked = this.userData?.permissions.find(x => x == p.value) != null;
      this.permissions.push(this.fb.group(formGroup));
    }

  }

  async save() {
    let model = this.createModel();
    console.log(model);
    if (this.userData == null) {
      await this.userService.add(model);
    } else {
      await this.userService.update(model);
    }
  }

  get permissions(): FormArray {
    return this.frmUser.get('permissions') as FormArray;
  }

  getPermissionLabel(value: number) {
    return this.allPermissions.find(x => x.value == value)?.label;
  }

  permissionChanged(i, checked) {
  }

  createModel(): User {
    let values = this.frmUser.value;
    return {
      userId: values.userId,
      username: values.username,
      displayName: values.displayName,
      password: values.password,
      permissions: values.permissions.filter(x => x.checked).map(x => x.value)
    }
  }
}
