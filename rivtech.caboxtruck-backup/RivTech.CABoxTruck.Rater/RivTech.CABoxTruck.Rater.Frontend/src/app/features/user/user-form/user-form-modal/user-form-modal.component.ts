import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UiService } from '../../../../services/ui.service';
import NotifUtils from '../../../../utilities/notif-utils';
import { User } from '../../user';
import { UserFormComponent } from '../user-form.component';

@Component({
  selector: 'app-user-form-modal',
  templateUrl: './user-form-modal.component.html',
  styleUrls: ['./user-form-modal.component.css']
})
export class UserFormModalComponent implements OnInit {

  @ViewChild('userForm') userForm : UserFormComponent;

  @Input() userData: User;
  hasModel: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private uiService: UiService
  ) { }

  ngOnInit(): void {
    this.hasModel = this.userData != null;
  }

  async save() {
    try{
      this.uiService.blockUI();
      await this.userForm.save();
      this.activeModal.close("success");
    } catch(error) {
      this.uiService.toastError(error);
    } finally {
      this.uiService.unblockUI();
    }
  }

  deleteUser(item: User) {
    NotifUtils.showWarning(`Are you sure you want to delete user <strong>${item.displayName}</strong>?`, async () => {
      alert('User deleted');
    });
  }

}
