export interface PremiumRaterDumpItem {
    filename: string,
    createdDateUtc: Date,
}