import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { DateUtils } from '../../utilities/date-utils';
import { PremiumRaterDumpItem } from './premium-rater-dump-item';

@Injectable()
export class PremiumRaterDumpService {



  baseUrl: string;

  constructor(
    private httpClient: HttpClient
  ) { 
    this.baseUrl = environment.apiUrl;
  }

  public getItems(filter: any) : Promise<PremiumRaterDumpItem[]> {
    let endpoint = `${this.baseUrl}/api/PremiumRaterDump/Get`;
    let queryParams = `&timezoneOffset=${DateUtils.getTimezoneOffsetInHours()}`

    if(!!filter.date)
      queryParams += `&date=${filter.date}`;
    if(!!filter.recentCount)
      queryParams += `&recentCount=${filter.recentCount}`;
    if(!!filter.submission)
      queryParams += `&submission=${filter.submission}`;
    if(!!filter.option)
      queryParams += `&option=${filter.option}`;
    if(!!filter.timeFrom)
      queryParams += `&timeFrom=${filter.timeFrom}`;
    if(!!filter.timeTo)
      queryParams += `&timeTo=${filter.timeTo}`;


    let url = `${endpoint}?${queryParams}`;
    return this.httpClient.get<PremiumRaterDumpItem[]>(url).toPromise().then(data => data);
  }

  public downloadFile(item: PremiumRaterDumpItem, selectedDateString: string) : Promise<Blob> {
    let url =`${this.baseUrl}/api/PremiumRaterDump/Download?filename=${item.filename}&date=${selectedDateString}`;
    return this.httpClient.get(url, {
        responseType: 'blob'
      }).toPromise().then(data => data);
  }

}
