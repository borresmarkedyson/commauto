import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UiService } from '../../../services/ui.service';
import { PremiumRaterDumpItem } from '../premium-rater-dump-item';
import { PremiumRaterDumpService } from '../premium-rater-dump.service';

@Component({
  selector: 'app-premium-rater-dump-list',
  templateUrl: './premium-rater-dump-list.component.html',
  styleUrls: ['./premium-rater-dump-list.component.css']
})
export class PremiumRaterDumpListComponent implements OnInit {

  @Input() forDashboard: boolean = false;
  @Input() recentCount : number = null;

  selectedDateString: string;
  submissionNumberFilter: string;
  optionFilter: string;
  items: PremiumRaterDumpItem[];

  constructor(
    private premiumRaterDumpService: PremiumRaterDumpService,
    private uiService: UiService,
  ) {
    this.selectedDateString = new Date().toISOString().substring(0, 10);
  }

  async ngOnInit() {
  }

  async filterList(filter: any) {
    if(this.forDashboard) {
      this.items = await this.premiumRaterDumpService.getItems({
        recentCount: this.recentCount
      });
    } else {
      this.items = await this.premiumRaterDumpService.getItems(filter);
    }
  }

  async download(item: PremiumRaterDumpItem) {
    try {
      this.uiService.blockUI();
      let blob = await this.premiumRaterDumpService.downloadFile(item, this.selectedDateString);
      const downloadLink: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
      const url = window.URL.createObjectURL(blob);
      downloadLink.href = url;
      downloadLink.download = `${item.filename}`;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
      URL.revokeObjectURL(url);
    } catch (e) {
      console.log(e);
      this.uiService.toastError("Error occured while downloading premium rater.");
    } finally {
      this.uiService.unblockUI();
    }
  }

}
