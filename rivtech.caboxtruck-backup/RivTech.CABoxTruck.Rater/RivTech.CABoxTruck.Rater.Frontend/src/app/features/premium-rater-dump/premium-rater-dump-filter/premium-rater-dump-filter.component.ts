import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-premium-rater-dump-filter',
  templateUrl: './premium-rater-dump-filter.component.html',
})
export class PremiumRaterDumpFilterComponent implements OnInit {

  @Output() onFilter: EventEmitter<any> = new EventEmitter<any>();

  filterFrm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.filterFrm = this.fb.group({
      'date': new Date().toISOString().substring(0, 10),
      'timeFrom': '',
      'timeTo': '',
      'submission': null,
      'option': null
    });
  }


  ngOnInit(): void {
  }

  filter() {
    let data = this.filterFrm.value;
    this.onFilter.emit(data);
  }

  onTimeFromChange() {
    let values = this.filterFrm.value;
    if (values.timeFrom >= values.timeTo) {
      this.filterFrm.patchValue({
        'timeTo': null
      });
    }
  }

  onTimeToChange() {
    let values = this.filterFrm.value;
    if (values.timeTo <= values.timeFrom) {
      this.filterFrm.patchValue({
        'timeFrom': null
      });
    }
  }

}
