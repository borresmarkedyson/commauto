import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRaterDumpListComponent } from './premium-rater-dump-list/premium-rater-dump-list.component';
import { PremiumRaterDumpService } from './premium-rater-dump.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { PremiumRaterDumpFilterComponent } from './premium-rater-dump-filter/premium-rater-dump-filter.component';


@NgModule({
  declarations: [
    PremiumRaterDumpListComponent,
    PremiumRaterDumpFilterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
  ],
  exports: [
    PremiumRaterDumpListComponent,
    PremiumRaterDumpFilterComponent
  ],
  providers: [
    PremiumRaterDumpService
  ]
})
export class PremiumRaterDumpModule { }
