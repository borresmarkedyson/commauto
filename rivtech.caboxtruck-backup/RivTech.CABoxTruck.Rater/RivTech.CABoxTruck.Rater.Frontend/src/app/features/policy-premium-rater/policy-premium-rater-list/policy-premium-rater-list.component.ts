import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { UiService } from '../../../services/ui.service';
import { PolicyPremiumRaterItem } from '../policy-premium-rater-item';
import { PolicyPremiumRaterService } from '../policy-premium-rater.service';

@Component({
  selector: 'app-policy-premium-rater-list',
  templateUrl: './policy-premium-rater-list.component.html',
  styleUrls: ['./policy-premium-rater-list.component.css']
})
export class PolicyPremiumRaterListComponent implements OnInit {

  @Input() forDashboard: boolean;
  @Input() recentCount: number;

  public items: PolicyPremiumRaterItem[];

  constructor(
    private policyPremiumRaterService: PolicyPremiumRaterService,
    private uiService: UiService,
  ) { }

  ngOnInit(): void {

  }

  filterList(filter?: any): Observable<any> {
    if (this.forDashboard) {
      filter = {
        recentCount: this.recentCount
      }
    }
    return this.policyPremiumRaterService.getItems(filter).pipe(
      tap(data => this.items = data)
    );
  }

  download(item: PolicyPremiumRaterItem) {
    this.uiService.blockUI();

    this.policyPremiumRaterService.downloadFile(item).pipe(
      catchError((e) => {
        this.uiService.toastError("Error occured while downloading premium rater.");
        return of();
      }),
      finalize(() => this.uiService.unblockUI())
    ).subscribe(blob => {
      debugger;
      const downloadLink: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
      const url = window.URL.createObjectURL(blob);
      downloadLink.href = url;
      downloadLink.download = `${item.fileName}`;
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
      URL.revokeObjectURL(url);
    });
  }
}
