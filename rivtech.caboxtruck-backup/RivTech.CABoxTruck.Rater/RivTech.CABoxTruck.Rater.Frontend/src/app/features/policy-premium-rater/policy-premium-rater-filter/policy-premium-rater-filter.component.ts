import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-policy-premium-rater-filter',
  templateUrl: './policy-premium-rater-filter.component.html',
})
export class PolicyPremiumRaterFilterComponent implements OnInit {

  @Output() onFilter: EventEmitter<any> = new EventEmitter<any>();

  filterFrm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.filterFrm = this.fb.group({
      'date': new Date().toISOString().substring(0, 10),
      'submission': null,
      'endorsementNumber': null
    });
  }


  ngOnInit(): void {
  }

  filter() {
    let data = this.filterFrm.value;
    this.onFilter.emit(data);
  }

}
