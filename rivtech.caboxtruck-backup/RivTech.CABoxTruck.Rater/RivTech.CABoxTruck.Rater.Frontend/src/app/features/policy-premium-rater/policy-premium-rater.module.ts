import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PolicyPremiumRaterListComponent } from './policy-premium-rater-list/policy-premium-rater-list.component';
import { MomentModule } from 'ngx-moment';
import { PolicyPremiumRaterService } from './policy-premium-rater.service';
import { PolicyPremiumRaterFilterComponent } from './policy-premium-rater-filter/policy-premium-rater-filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [PolicyPremiumRaterListComponent, PolicyPremiumRaterFilterComponent],
  imports: [
    CommonModule,
    MomentModule,
    ReactiveFormsModule,
  ],
  exports: [
    PolicyPremiumRaterListComponent,
    PolicyPremiumRaterFilterComponent,
  ],
  providers: [
    PolicyPremiumRaterService
  ]
})
export class PolicyPremiumRaterModule { }
