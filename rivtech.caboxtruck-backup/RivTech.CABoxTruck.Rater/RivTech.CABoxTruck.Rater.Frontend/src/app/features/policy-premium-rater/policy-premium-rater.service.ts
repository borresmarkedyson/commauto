import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { DateUtils } from '../../utilities/date-utils';
import { PolicyPremiumRaterItem } from './policy-premium-rater-item';

@Injectable()
export class PolicyPremiumRaterService {

  baseUrl: string;

  constructor(
    private httpClient: HttpClient
  ) { 
    this.baseUrl = environment.apiUrl;
  }

  public getItems(filter: any) : Observable<PolicyPremiumRaterItem[]> {
    let endpoint = `${this.baseUrl}/api/PremiumRaterArchive/Get`;
    let queryParams = `&timezoneOffset=${DateUtils.getTimezoneOffsetInHours()}`

    if(!!filter.date)
      queryParams += `&date=${filter.date}`;
    if(!!filter.recentCount)
      queryParams += `&recentCount=${filter.recentCount}`;
    if(!!filter.submission)
      queryParams += `&submission=${filter.submission}`;
    if(!!filter.endorsementNumber)
      queryParams += `&endorsementNumber=${filter.endorsementNumber}`;

    let url = `${endpoint}?${queryParams}`;
    return this.httpClient.get<PolicyPremiumRaterItem[]>(url);
  }

  public downloadFile(item: PolicyPremiumRaterItem) : Observable<Blob> {
    let url =`${this.baseUrl}/api/PremiumRaterArchive/Download/${item.id}`;
    return this.httpClient.get(url, { responseType: 'blob' });
  }

}
