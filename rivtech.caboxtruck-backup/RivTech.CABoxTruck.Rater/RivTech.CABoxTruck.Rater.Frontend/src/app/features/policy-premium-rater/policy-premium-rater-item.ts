export interface PolicyPremiumRaterItem {
    id: string,
    submission: string,
    endorsementNumber: number,
    fileName: string,
    createdDate: Date,
}