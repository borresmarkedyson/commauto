import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { User } from "../features/user/user";
import { AuthResult } from "../shared/auth-result";

@Injectable()
export class AuthService {

    public static getToken() : string  {
        let user: any = JSON.parse(localStorage.getItem("user"));
        return user?.token;
    } 

    public static getUser() : User {
        return JSON.parse(localStorage.getItem("user"));
    }

    baseUrl: string;

    constructor(private httpClient: HttpClient) {
        this.baseUrl = environment.apiUrl;
    }

    public async login(username: string, password: string): Promise<AuthResult> {
        var url = `${this.baseUrl}/auth`;
        var body = {
            "username": username,
            "password": password
        };
        let result: AuthResult;
        try {
            let response: any = await this.httpClient.post(url, body).toPromise();
            if (response.user === "undefined") {
                throw "User not found.";
            }
            result = {
                success: true,
                data: response
            };
            let user = JSON.stringify(response);
            localStorage.setItem("user", user);
            console.log(response);
        } catch (e) {
            console.log(e);
            result = {
                success: false,
                error: e.error.title
            };
        }

        return new Promise<AuthResult>(resolve => resolve(result));
    }

    public logout() {
        localStorage.removeItem("user");
    }

}
