import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import swal from 'sweetalert2';

@Injectable()
export class UiService {
    

    constructor(
        private toastrService: ToastrService
    ) {

    }

    blockUI(): void {
        swal.fire({
            title: "Please wait",
            allowOutsideClick: false,
            allowEscapeKey: false,
            showConfirmButton: false
        });
        swal.showLoading();
    }

    unblockUI(): void {
        swal.close();
    }

    toastError(error: string) {
        this.toastrService.error(error);
    }

    toastSuccess(message: string) {
        this.toastrService.success(message);
    }
}