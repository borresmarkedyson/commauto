export const commonEnvironment = {
    AuthKey: 'auth',
    UsernameKey: 'uname',
    ClientId: 'Environmental',
    GrantType: {
        Password: "password",
        RefreshToken: "refresh_token"
    },
    allowDump: true
  };