// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { commonEnvironment } from "./environment.common";

export const _settings = { 
  production: false,
  apiUrl: 'http://boxtruckraterapitest.westus2.cloudapp.azure.com:3000'
};

export const environment = Object.assign({}, commonEnvironment, _settings);