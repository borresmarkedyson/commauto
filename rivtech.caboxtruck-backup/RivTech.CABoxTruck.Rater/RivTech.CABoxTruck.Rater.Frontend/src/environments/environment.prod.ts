import { commonEnvironment } from "./environment.common";

export const _settings = {
  production: true,
  apiUrl: 'http://vm-boxtruck-raterapi-prod.westus2.cloudapp.azure.com:3000',
  allowDump: false
};

export const environment = Object.assign({}, commonEnvironment, _settings);
