﻿using System.Collections.Generic;
using System;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using System.Linq;
using System.Threading;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Inbound
{
    public class PremiumRaterService : IPremiumRaterService
    {
        private readonly ILogger<PremiumRaterService> _logger;
        private readonly PremiumRaterPool _premiumRaterPool;
        private readonly IPremiumRaterDumpRepository _premiumRaterDumpRepository;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IStorageService _storageService;
        private readonly IPremiumRaterArchiveRepository _premiumRaterArchiveRepository;
        private readonly RaterSettings _raterSettings;

        public PremiumRaterService(
            ILogger<PremiumRaterService> logger,
            PremiumRaterPool premiumRaterPool,
            IPremiumRaterDumpRepository premiumRaterDumpRepository,
            IServiceScopeFactory serviceScopeFactory,
            IStorageService storageService,
            IPremiumRaterArchiveRepository premiumRaterArchiveRepository,
            RaterSettings raterSettings
        )
        {
            _logger = logger;
            _premiumRaterPool = premiumRaterPool;
            _premiumRaterDumpRepository = premiumRaterDumpRepository;
            _serviceScopeFactory = serviceScopeFactory;
            _storageService = storageService;
            _premiumRaterArchiveRepository = premiumRaterArchiveRepository;
            _raterSettings = raterSettings;
        }

        public IPremiumRater Rate(RateCommand command)
        {
            IPremiumRater premiumRater = (PremiumRater)_premiumRaterPool.GetItemReadyForRating(command.SubmissionNumber);
            if (premiumRater is null)
                throw new RaterException($"Rater of {command.SubmissionNumber} is not yet ready to rate.");
            premiumRater.Rate(command.InputValues, command.OptionId);
            return premiumRater;
        }

        public async Task DumpRaterAsync(IPremiumRater premiumRater)
        {
            if (premiumRater != null)
            {
                await _premiumRaterDumpRepository.CreateAsync(PremiumRaterDump.FromRater(premiumRater));
            }
        }

        public async Task SaveRaterFromIssuanceAsync(IPremiumRater rater)
        {
            if (rater == null)
                return;

            // Get repository from a scope factory since this method will run in background. 
            var scope = _serviceScopeFactory.CreateScope();
            var premiumRaterArchiveRepository = scope.ServiceProvider.GetService<IPremiumRaterArchiveRepository>();
            var archive = PremiumRaterArchive.CreateFromRater(rater);

            // If there's an existing rater archive, remove it and create a new one.
            var storedArchive = await premiumRaterArchiveRepository.GetAsync(rater.SubmissionNumber, rater.EndorsementNumber.Value);
            if (storedArchive != null)
            {
                await _storageService.DeleteAsync(storedArchive.RelativePath);
                await premiumRaterArchiveRepository.RemoveAsync(storedArchive);
            }
            await _storageService.SaveBytesAsync(rater.FileContent.Bytes, archive.RelativePath);
            await premiumRaterArchiveRepository.CreateAsync(archive);
        }

        public IEnumerable<IPremiumRater> GetRaterPoolItems(bool? runningOnly = null)
        {
            runningOnly ??= false;
            return _premiumRaterPool.PoolItems
                ?.OrderBy(x => x.CreatedDate)
                ?.Where(x => runningOnly == false || (runningOnly == true && x.Status.Equals(PremiumRaterStatus.Running)));
        }

        public async Task PreloadRatersAsync(string submissionNumber, DateTime inceptionDate)
        {
            DateTime preloadTime = DateTime.Now;

            _ = _premiumRaterPool.PreloadRaterAsync(submissionNumber, inceptionDate);

            // Check every second if raters for submission are initialized
            // while timeout is not reached yet.
            bool initialized;
            bool timeoutReached;
            do
            {
                var rater = _premiumRaterPool.GetItemReadyForRating(submissionNumber);
                initialized = rater != null;
                if (initialized)
                    break;

                int elapsedSeconds = (int)(DateTime.Now - preloadTime).TotalSeconds;

                timeoutReached = elapsedSeconds > _raterSettings.RaterPreloadTimeout;
                if (timeoutReached)
                    throw new TimeoutException($"Rater preload timeout has been reached.");

                await Task.Delay(1000);
            } while (!initialized && !timeoutReached);
        }


        public async Task UnloadRaterAsync(string submissionNumber)
        {
            await _premiumRaterPool.UnloadRatersBySubmissionAsync(submissionNumber);
        }

        public async Task ForceUnloadAsync(string submissionNumber)
        {
            await _premiumRaterPool.UnloadRatersBySubmissionAsync(submissionNumber);
        }



        public async Task<PremiumRaterArchive[]> GetArchivesAsync(PremiumRaterArchiveFilter filter)
        {
            var items = await _premiumRaterArchiveRepository.GetAsync(filter);
            return items.ToArray();
        }

        public async Task<PremiumRaterArchive> GetArchiveByIdAsync(Guid id)
        {
            return await _premiumRaterArchiveRepository.GetByIdAsync(id);
        }

        public async Task<byte[]> DownloadArchiveFileAsync(Guid id)
        {
            var archive = await _premiumRaterArchiveRepository.GetByIdAsync(id);
            return await _storageService.GetAsync(archive.RelativePath);
        }

        public async Task DeleteArchive(string submissionNumber, int endorsementNumber)
        {
            var archive = await _premiumRaterArchiveRepository.GetAsync(submissionNumber, endorsementNumber);
            if (archive is null)
                return;
            await _premiumRaterArchiveRepository.RemoveAsync(archive);
            await _storageService.DeleteAsync(archive.RelativePath);
        }


        public PremiumRaterDump[] Get(PremiumRaterDumpFilter filter)
        {
            return _premiumRaterDumpRepository.Get(filter).ToArray();
        }
        public FileContent DownloadDumpFile(DateTime date, string filename)
        {
            return _premiumRaterDumpRepository.GetDumpFile(date, filename);
        }

        public async Task ShutdownRaterPoolAsync()
        {
            await _premiumRaterPool.RequestShutdownAsync();
        }

        public async Task RemoveIdleAsync()
        {
            try
            {
                await _premiumRaterPool.RemoveIdleAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while removing idle raters in rater pool.");
            }
        }

        public async Task RemoveSuspendedRaters()
        {
            try
            {
                await _premiumRaterPool.RemoveSuspendedRatersAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occured while removing suspended raters in rater pool.");
            }
        }

        /// <summary>
        /// Suspends rater of a policy.
        /// </summary>
        public void SuspendRater(string submissionNumber)
        {
            var id = new PolicyPremiumRaterId(submissionNumber);
            _premiumRaterPool.SuspendRaters(submissionNumber);
        }

        public async Task ForceCloseStuckUnloaded()
        {
            try
            {
                var unloadedRaters = _premiumRaterPool.PoolItems.Where(x => x.Status == PremiumRaterStatus.Unloaded)?.ToArray();
                for (int i = 0; i < unloadedRaters?.Length; i++)
                {
                    var rater = unloadedRaters[i];
                    await using (rater)
                    {
                        rater.ForceCloseEngine();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error force closing unloaded raters.");
            }
        }
    }


}
