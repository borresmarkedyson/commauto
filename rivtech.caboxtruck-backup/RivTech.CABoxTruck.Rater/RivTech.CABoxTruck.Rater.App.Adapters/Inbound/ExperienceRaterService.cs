﻿using System;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Inbound
{
    public class ExperienceRaterService : IExperienceRaterService
    {
        private readonly ILogger<ExperienceRaterService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IExperienceRaterRepository _experienceRaterRepository;
        private readonly IExperienceRaterFactory _experienceRaterFactory;
        private readonly IExperienceRaterEngineLoader _engineLoader;

        public ExperienceRaterService(
            ILogger<ExperienceRaterService> logger,
            IServiceScopeFactory serviceScopeFactory,
            IExperienceRaterFactory experienceRaterFactory,
            IExperienceRaterRepository experienceRaterRepository,
            IExperienceRaterEngineLoader engineLoader
        )
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
            _experienceRaterFactory = experienceRaterFactory;
            _experienceRaterRepository = experienceRaterRepository;
            _engineLoader = engineLoader;
        }

        public async Task CreateInitialAsync(string submissionNumber, DateTime effectiveDate)
        {
            if (_experienceRaterRepository.Exists(submissionNumber))
                return;
            using ExperienceRater experienceRater = await _experienceRaterFactory
                .CreateFromTemplateAsync(submissionNumber, effectiveDate);
            await experienceRater.LoadEngineAsync(_engineLoader);
            experienceRater.SetRaterInfo();
            await experienceRater.WrapUpAsync();
            await _experienceRaterRepository.CreateAsync(experienceRater);
        }

        public async Task<OutputValues> UploadAsync(string submissionNumber, DateTime effectiveDate, FileContent newContent)
        {
            // Get experience rater and create new if not existing.
            ExperienceRater experienceRater = await _experienceRaterRepository.GetAsync(submissionNumber);
            if (experienceRater is null)
            {
                await CreateInitialAsync(submissionNumber, effectiveDate);
                return await UploadAsync(submissionNumber, effectiveDate, newContent);
            }
            await experienceRater.ChangeContent(newContent, _engineLoader);
            OutputValues outputValues = experienceRater.ReadOutput();
            // Clean up in background.
            _ = Task.Run(async () => await SaveUpdatedRaterAsync(experienceRater));
            return outputValues;
        }

        private async Task SaveUpdatedRaterAsync(ExperienceRater experienceRater)
        {
            try
            {
                // This task will run in a different thread and the injected services will be disposed
                // so get the experienceRaterRepository using ServiceScopeFactory.
                using var scope = _serviceScopeFactory.CreateScope();
                var experienceRaterRepository = scope.ServiceProvider.GetService<IExperienceRaterRepository>();
                await using (experienceRater)
                {
                    // Finalize rater.
                    await experienceRater.WrapUpAsync();
                    // Save updated rater.
                    await experienceRaterRepository.UpdateAsync(experienceRater);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error when finalizing experience rater after changing content.");
            }

        }

        public async Task<ExperienceRater> DownloadAsync(string submissionNumber, DateTime effectiveDate, InputValues inputValues)
        {
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            // Get experience rater.
            using ExperienceRater experienceRater = await _experienceRaterRepository.GetAsync(submissionNumber);
            if (experienceRater is null)
            {
                await CreateInitialAsync(submissionNumber, effectiveDate);
                return await DownloadAsync(submissionNumber, effectiveDate, inputValues);
            }
            watch.Stop();
            Console.WriteLine($"Experience rater fetched: {watch.Elapsed.Seconds}");
            await experienceRater.LoadEngineAsync(_engineLoader);
            experienceRater.MapInput(inputValues);
            await experienceRater.WrapUpAsync();
            return experienceRater;
        }

        public async Task DeleteAsync(string submissionNumber)
        {
            await _experienceRaterRepository.DeleteAsync(submissionNumber);
        }

        public bool Exists(string submissionNumber)
        {
            return _experienceRaterRepository.Exists(submissionNumber);
        }


        public async Task CopyToNewSubmission(string from, string to)
        {
            // Get XP rater from repository.
            using ExperienceRater experienceRater = await _experienceRaterRepository.GetAsync(from);
            // Clone XP rater.
            ExperienceRater newExperienceRater = experienceRater.CloneWithNewSubmission(to);
            // Save cloned xp rater.
            await _experienceRaterRepository.CreateAsync(newExperienceRater);
        }
    }
}
