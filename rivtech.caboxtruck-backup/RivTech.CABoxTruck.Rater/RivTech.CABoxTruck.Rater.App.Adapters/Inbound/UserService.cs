﻿using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Inbound
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher _passwordHasher;

        public UserService(
            IUserRepository userRepository,
            IPasswordHasher passwordHasher
        ) {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
        }

        public void Add(User user)
        {
            if (GetByUsername(user.Username) != null)
                throw new InvalidOperationException("Username already exists.");
            user.HashPassword(_passwordHasher);
            _userRepository.Add(user);
        }

        public User GetByUsername(string username)
        {
            return GetAll().SingleOrDefault(x => x.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetUsers();
        }

        public void Remove(string username)
        {
            _userRepository.Delete(username);
        }

        public void Update(User user)
        {
            var savedUser = _userRepository.Get(user.UserId);
            var userByUsername = GetByUsername(user.Username);
            if (userByUsername != null && savedUser.UserId != userByUsername.UserId)
                throw new InvalidOperationException("Username already exists.");

            savedUser.DisplayName = user.DisplayName;
            savedUser.Username = user.Username;
            savedUser.Permissions = user.Permissions;
            _userRepository.Update(savedUser);
        }

        public void ChangePassword(string username, string newPassword)
        {
            User user = _userRepository.GetByUsername(username);
            user.Password = newPassword;
            user.HashPassword(_passwordHasher);
            _userRepository.Update(user);
        }
    }
}
