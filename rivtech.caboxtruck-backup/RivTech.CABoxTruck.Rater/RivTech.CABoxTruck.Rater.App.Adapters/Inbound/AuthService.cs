﻿using RivTech.CABoxTruck.Rater.App.Domain.Ports.Inbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System.Security;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Inbound
{
    public class AuthService : IAuthService
    {
        private readonly IPasswordHasher _passwordHasher;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IUserRepository _userRepository;

        public AuthService(
            IPasswordHasher passwordHasher,
            IJwtTokenGenerator jwtTokenGenerator,
            IUserRepository userRepository
        )
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _jwtTokenGenerator = jwtTokenGenerator;
        }

        public User Authenticate(User user)
        {
            var savedUser = _userRepository.GetByUsername(user.Username);
            if (savedUser is null)
                return null;

            var hashedPassword = _passwordHasher.Hash(user.Password, savedUser.Salt);
            if (!savedUser.Password.Equals(hashedPassword))
                return null;

            savedUser.Token = _jwtTokenGenerator.Generate(savedUser);

            return savedUser;
        }

    }
}
