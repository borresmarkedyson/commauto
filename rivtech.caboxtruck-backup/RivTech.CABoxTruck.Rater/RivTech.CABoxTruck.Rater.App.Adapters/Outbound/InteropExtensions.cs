﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public static class InteropExtensions
    {
        #region Error value checking 
        /*
         * Please refer to below link regarding this code regio.
         * Link: https://stackoverflow.com/a/2425170
         */

        enum CVErrEnum : Int32
        {
            ErrDiv0 = -2146826281,
            ErrGettingData = -2146826245,
            ErrNA = -2146826246,
            ErrName = -2146826259,
            ErrNull = -2146826288,
            ErrNum = -2146826252,
            ErrRef = -2146826265,
            ErrValue = -2146826273
        }

        public static object SanitizeValue(this object value)
        {
            if(int.TryParse($"{value}", out int intVal)
                && Enum.IsDefined(typeof(CVErrEnum), intVal)
            ) {
                return null;
            }
            return value;
        }

        #endregion

        public static List<Name> AsList(this Names names)
        {
            var nameList = new List<Name>();

            for (int nameIndex = 1; nameIndex <= names.Count; nameIndex++)
                nameList.Add(names.Item(nameIndex));

            return nameList;
        }

        public static string TrimmedName(this Name name)
        {
            string nameText = name.Name;
            if (nameText.Contains("!"))
                nameText = nameText[(nameText.IndexOf("!") + 1)..];
            return nameText;
        }
    }
}
