﻿using Microsoft.Extensions.Logging;
using Microsoft.Office.Interop.Excel;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.ExperienceRating
{
    public class ExperienceRaterEngineLoader : IExperienceRaterEngineLoader
    {
        private readonly ITempFileService _tempFileService;
        private readonly IExcelPlaceholderInputMapper _excelPlaceholderInputMapper;
        private readonly ILogger<ExperienceRaterEngineLoader> _logger;

        public ExperienceRaterEngineLoader(
            ITempFileService tempFileService,
            IExcelPlaceholderInputMapper excelPlaceholderInputMapper,
            ILogger<ExperienceRaterEngineLoader> logger
        )
        {
            _tempFileService = tempFileService;
            _excelPlaceholderInputMapper = excelPlaceholderInputMapper;
            _logger = logger;
        }

        public async Task<IExperienceRaterEngine> LoadAsync(FileContent content)
        {
            Application excelApp = null;
            Workbook workbook = null;
            try
            {
                excelApp = new Application();
                Workbooks workbooks = excelApp.Workbooks;
                string tempPath = await _tempFileService.FromContentAsync(content);
                workbook = workbooks.Open(tempPath);
                IExperienceRaterEngine engine = new InteropExperienceRaterEngine(workbook, tempPath, _excelPlaceholderInputMapper);
                engine.Initialize();
                return engine;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error loading experience rater engine from a file content.");
                if (workbook != null)
                {
                    workbook.Close(0);
                    Marshal.ReleaseComObject(workbook);
                }
                if (excelApp != null)
                {
                    excelApp.Quit();
                    Marshal.ReleaseComObject(excelApp);
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();
                throw;
            }
        }
    }
}
