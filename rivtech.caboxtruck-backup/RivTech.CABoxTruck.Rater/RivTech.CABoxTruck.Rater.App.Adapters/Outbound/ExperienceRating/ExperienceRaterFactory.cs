﻿using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.ExperienceRating
{
    public class ExperienceRaterFactory : IExperienceRaterFactory
    {
        private readonly IRaterTemplateRepository _raterTemplateRepository;

        public ExperienceRaterFactory(
            IRaterTemplateRepository raterTemplateRepository
        )
        {
            _raterTemplateRepository = raterTemplateRepository;
        }

        public async Task<ExperienceRater> CreateFromTemplateAsync(string submissionNumber, DateTime effectiveDate)
        {
            // Get template file based on effectiveDate.
            RaterTemplate raterTemplate = await _raterTemplateRepository
                .GetByEffectivityAsync(effectiveDate, RaterTemplate.TYPE_EXPERIENCE);
            var experienceRater = new ExperienceRater(submissionNumber, raterTemplate.Version, raterTemplate.Content);
            return experienceRater;
        }

    }
}
