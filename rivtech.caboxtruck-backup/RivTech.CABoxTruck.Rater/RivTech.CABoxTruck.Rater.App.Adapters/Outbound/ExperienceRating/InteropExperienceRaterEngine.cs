﻿using Microsoft.Office.Interop.Excel;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.ExperienceRating
{
    public class InteropExperienceRaterEngine : IExperienceRaterEngine
    {
        const string POLICY_SHEET = "Pol_SysInput";
        const string VEHICLE_SHEET = "Veh_SysInput";
        const string DRIVER_SHEET = "Drvr_SysInput";
        const string DEFAULT_SHEET = "Experience Rater_Liab";

        const string RATER_INFO_SHEET = "_RaterInfo";
        const string RATER_INFO_CELL_SUBMISSION = "A1";
        const string RATER_INFO_CELL_VERSION = "A2";

        private readonly IExcelPlaceholderInputMapper _excelPlaceholderInputMapper;

        public InteropExperienceRaterEngine(
            Workbook workbook,
            string location,
            IExcelPlaceholderInputMapper excelPlaceholderInputMapper
        )
        {
            Location = location;
            Workbook = workbook;
            _excelPlaceholderInputMapper = excelPlaceholderInputMapper;
        }

        public string Location { get; }
        public string Extension => Path.GetExtension(Location);
        protected Application ExcelApp => Workbook.Application;
        protected Workbook Workbook { get; private set; }

        public void Initialize()
        {
            ExcelApp.EnableEvents = false;
            ExcelApp.Visible = false;
            ExcelApp.ScreenUpdating = false;
            ExcelApp.DisplayAlerts = false;

            // Set calculation to Manual so that when input values is mapped on each cell,
            // the app won't keep recalculating which may extremely slow down mapping.
            ExcelApp.Calculation = XlCalculation.xlCalculationManual;
        }

        public void SetRaterInfo(ExperienceRater experienceRater)
        {
            Sheets sheets = Workbook.Sheets;
            Worksheet addAfter = sheets[sheets.Count];
            Worksheet worksheet = sheets.Add(After: addAfter);
            worksheet.Name = RATER_INFO_SHEET;

            // Set submission number.
            Range idCell = worksheet.Range[RATER_INFO_CELL_SUBMISSION];
            idCell.Value2 = experienceRater.SubmissionNumber;
            idCell.Locked = true;

            // Set rater version.
            Range versionCell = worksheet.Range[RATER_INFO_CELL_VERSION];
            versionCell.Value2 = experienceRater.Version;
            idCell.Locked = true;

            worksheet.Protect();
            worksheet.Visible = XlSheetVisibility.xlSheetHidden;
        }

        public void MapInput(InputValues inputValues)
        {
            if (Workbook == null)
                throw new ExcelRaterNotInitializedException("Load the excel file before mapping input.");
            _excelPlaceholderInputMapper.Map(inputValues, Workbook);
        }

        public void LockInputs()
        {
            Sheets sheets = Workbook.Sheets;

            Worksheet policyInputSheet = sheets[POLICY_SHEET];
            policyInputSheet.Protect();

            Worksheet vehicleInputSheet = sheets[VEHICLE_SHEET];
            vehicleInputSheet.Protect();

            Worksheet driverInputSheet = sheets[DRIVER_SHEET];
            driverInputSheet.Protect();
        }

        public void UnlockInputs()
        {
            Sheets sheets = Workbook.Sheets;

            Worksheet policyInputSheet = sheets[POLICY_SHEET];
            policyInputSheet.Unprotect();

            Worksheet vehicleInputSheet = sheets[VEHICLE_SHEET];
            vehicleInputSheet.Unprotect();

            Worksheet driverInputSheet = sheets[DRIVER_SHEET];
            driverInputSheet.Unprotect();
        }

        public void Calculate()
        {
            var sw = new Stopwatch();
            sw.Restart();

            Workbook.Application.CalculateFull();

            sw.Stop();
            Console.WriteLine($"Calculated ({sw.ElapsedMilliseconds}ms)");
        }

        public void Validate(ExperienceRater experienceRater)
        {
            Sheets worksheets = Workbook.Worksheets;
            Worksheet raterInfoWorksheet = null;
            try
            {
                raterInfoWorksheet = (Worksheet)worksheets[RATER_INFO_SHEET];
            }
            catch (COMException)
            {
                if (raterInfoWorksheet is null)
                    throw new InvalidExperienceRaterFileException("File is not a valid experience rater.");
            }

            Range versionCell = raterInfoWorksheet.Range[RATER_INFO_CELL_VERSION];
            string raterVersion = versionCell.Value2?.ToString() ?? null;
            bool sameVersion = experienceRater.Version.Equals(raterVersion, StringComparison.OrdinalIgnoreCase);
            if (!sameVersion)
                throw new InvalidExperienceRaterFileException($"The rater version for this submission must be {experienceRater.Version} but the version inside the file has '{raterVersion}'.");
        }

        public OutputValues ReadOutput(ExperienceRater experienceRater)
        {
            // Extract factors from Option sheet. 
            Worksheet sheet = Workbook.Sheets[POLICY_SHEET];
            Range scheduleFactor = sheet.Range["ALScheduleRF"];
            Range experienceFactor = sheet.Range["ALExperienceRF"];

            var values = new Dictionary<string, object>
            {
                ["ALScheduleRF"] = ((object)scheduleFactor.Value2)?.SanitizeValue(),
                ["ALExperienceRF"] = ((object)experienceFactor.Value2)?.SanitizeValue(),
            };

            var hic = new dynamic[5];
            for (int i = 0; i < hic.Length; i++)
            {
                Range powerUnitsAL = sheet.Range[$"HIC_{i}_.NumberOfPowerUnitsAL"];
                Range powerUnitsAPD = sheet.Range[$"HIC_{i}_.NumberOfPowerUnitsPD"];

                hic[i] = new Dictionary<string, object>();
                hic[i]["NumberOfPowerUnitsAL"] = (double?)((object)powerUnitsAL.Value2)?.SanitizeValue();
                hic[i]["NumberOfPowerUnitsAPD"] = (double?)((object)powerUnitsAPD.Value2)?.SanitizeValue();
            }
            values["HIC"] = hic;

            return new OutputValues(values);
        }

        public async Task SaveChangesAsync()
        {
            await Task.Run(() =>
            {
                ExcelApp.Calculation = XlCalculation.xlCalculationAutomatic;
                SetDefaultSheet();
                Workbook.Save();
            });
        }

        private void SetDefaultSheet()
        {
            Sheets worksheets = Workbook.Worksheets;
            Worksheet defaultSheet = (Worksheet)worksheets[DEFAULT_SHEET];
            defaultSheet.Select();
        }

        public async Task<FileContent> EndAsync()
        {
            CloseExcel();
            return await CreateContentFromLocationAsync();
        }

        private async Task<FileContent> CreateContentFromLocationAsync()
        {
            byte[] bytes = await File.ReadAllBytesAsync(Location);
            string extension = Path.GetExtension(Location);
            return new FileContent(bytes, extension);
        }

        public void Dispose()
        {
            Task.Run(() =>
            {
                Console.WriteLine($"Deleting file {Location}.");
                CloseExcel();

                if (File.Exists(Location))
                    File.Delete(Location);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            });
        }

        private void CloseExcel()
        {
            try
            {
                if (Workbook != null)
                {
                    Workbook.Close(0);
                    Marshal.ReleaseComObject(Workbook);
                }
            }
            catch { Console.WriteLine("Error closing workbook."); }

            try
            {
                if (ExcelApp != null)
                {
                    ExcelApp.Quit();
                    Marshal.ReleaseComObject(ExcelApp);
                }
            }
            catch { Console.WriteLine("Error quitting excel application."); }
        }
    }
}
