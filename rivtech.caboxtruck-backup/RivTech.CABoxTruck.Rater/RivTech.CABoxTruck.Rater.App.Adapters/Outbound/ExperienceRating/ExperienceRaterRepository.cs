﻿using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.ExperienceRating
{
    public class ExperienceRaterRepository : IExperienceRaterRepository
    {
        private const string FILENAME_PREFIX = "ExperienceRater_";

        private readonly AppDbContext _dbContext;
        private readonly RaterSettings _raterSettings;

        public ExperienceRaterRepository(
            AppDbContext dbContext, 
            RaterSettings raterSettings
        )
        {
            _dbContext = dbContext;
            _raterSettings = raterSettings;
        }

        public async Task<ExperienceRater> CreateAsync(ExperienceRater experienceRater)
        {
            await _dbContext.ExperienceRater.AddAsync(experienceRater);
            await _dbContext.SaveChangesAsync();
            return await _dbContext.ExperienceRater.FindAsync(experienceRater.SubmissionNumber);
        }

        public async Task UpdateAsync(ExperienceRater experienceRater)
        {
            _dbContext.ExperienceRater.Update(experienceRater);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ExperienceRater> GetAsync(string submissionNumber)
        {
            return await _dbContext.ExperienceRater.FindAsync(submissionNumber);
        }

        public bool Exists(string submissionNumber)
        {
            return _dbContext.ExperienceRater.Find(submissionNumber) != null;
        }

        public async Task DeleteAsync(string submissionNumber)
        {
            ExperienceRater experienceRater = await _dbContext.ExperienceRater.FindAsync(submissionNumber);
            _dbContext.ExperienceRater.Remove(experienceRater);
        }

    }
}
