﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public class AzureStorageService : IStorageService
    {
        private const string CONN_STRING_AZURE = "AzureStorage";
        private const string BLOB_CONTAINER_RATER = "rater";
        private readonly IConfiguration _config;
        private readonly ILogger<AzureStorageService> _logger;


        public AzureStorageService(
            IConfiguration config,
            ILogger<AzureStorageService> logger
        )
        {
            _config = config;
            _logger = logger;
        }

        public async Task<byte[]> GetAsync(string relativePath)
        {
            byte[] returnValue = new byte[0];
            var connectionString = _config.GetConnectionString(CONN_STRING_AZURE);

            var blobService = new BlobServiceClient(connectionString);

            string containerName = GetContainerName(relativePath);
            if (containerName == null)
            {
                return returnValue;
            }

            BlobContainerClient container = blobService.GetBlobContainerClient(containerName);
            BlobClient blob = container.GetBlobClient(relativePath);

            if (blob.Exists().Value)
            {
                var result = await blob.DownloadContentAsync();
                returnValue = result.Value.Content.ToArray();
            }

            return returnValue;
        }

        public async Task DeleteAsync(string relativePath)
        {
            var connectionString = _config.GetConnectionString(CONN_STRING_AZURE);

            var blobService = new BlobServiceClient(connectionString);

            string containerName = GetContainerName(relativePath);
            if (containerName == null)
                return;

            BlobContainerClient container = blobService.GetBlobContainerClient(containerName);
            BlobClient blob = container.GetBlobClient(relativePath);

            if (blob.Exists().Value)
            {
                await blob.DeleteAsync(DeleteSnapshotsOption.IncludeSnapshots);
            }
        }

        public async Task SaveBytesAsync(byte[] bytes, string relativePath)
        {
            var connectionString = _config.GetConnectionString(CONN_STRING_AZURE);

            var blobService = new BlobServiceClient(connectionString);

            string containerName = GetContainerName(relativePath);
            if (containerName == null)
                return;
            BlobContainerClient container = blobService.GetBlobContainerClient(containerName);
            BlobClient blob = container.GetBlobClient(relativePath);
            try
            {

                await blob.UploadAsync(new BinaryData(bytes));
            }
            catch (RequestFailedException e)
            {
                _logger.LogError(e, "Error uploading premium rater to azure storage.");
            }
        }

        private string GetContainerName(string relativePath)
        {
            string[] pathArr = relativePath.Split("\\");

            return pathArr[0] switch
            {
                PremiumRaterArchive.FOLDER_NAME => BLOB_CONTAINER_RATER,
                _ => null,
            };
        }
    }
}
