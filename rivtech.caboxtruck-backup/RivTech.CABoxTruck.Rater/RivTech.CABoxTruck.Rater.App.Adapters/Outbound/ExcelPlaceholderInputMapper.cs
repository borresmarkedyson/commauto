﻿using Microsoft.Extensions.Logging;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.Rater.App.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public interface IExcelPlaceholderInputMapper
    {
        void Map(InputValues inputValues, Workbook workbook);
    }

    public class ExcelPlaceholderInputMapper : IExcelPlaceholderInputMapper
    {
        private  readonly ILogger _logger;

        public ExcelPlaceholderInputMapper(
            ILogger<ExcelPlaceholderInputMapper> logger
        ) {
            _logger = logger;
        }

        public void Map(InputValues inputValues, Workbook workbook)
        {
            Stopwatch sw = new Stopwatch();
            sw.Restart();
            MapValues(workbook, "Pol_SysInput", inputValues);
            sw.Stop();
            Console.WriteLine($"Mapped Pol_SysInput. [{sw.Elapsed.Seconds}]");

            sw.Restart();
            MapListValues(workbook, "Veh_SysInput", inputValues, "Vehicle.VehicleList");
            sw.Stop();
            Console.WriteLine($"Mapped Veh_SysInput. [{sw.Elapsed.Seconds}]");

            sw.Restart();
            MapListValues(workbook, "Drvr_SysInput", inputValues, "Driver.DriverList");
            sw.Stop();
            Console.WriteLine($"Mapped Drvr_SysInput. [{sw.Elapsed.Seconds}]");

        }

        private void MapValues(Workbook workbook, string sheetName, InputValues inputValues)
        {
            Sheets sheets = workbook.Worksheets;
            Worksheet inputSheet = sheets[sheetName];
            Names names = inputSheet.Names;

            Console.WriteLine($"Names count: {names.Count}");

            for (int nameIndex = 1; nameIndex <= names.Count; nameIndex++)
            {
                Name name = names.Item(nameIndex);
                string nameText = GetNameText(name);
                Marshal.ReleaseComObject(name);

                var placeholder = new ExcelPlaceholder(nameText);
                if (!placeholder.IsValid())
                    continue;

                Range inputField = inputSheet.Range[$"{placeholder}"];
                if (inputField == null)
                    continue;

                object val = inputValues.ParseValue(placeholder.Path);
                if (val == null)
                    continue;

                var sw = new Stopwatch();
                sw.Restart();
                Console.WriteLine($"{placeholder}: {val}");
                inputField.Value2 = val;
                sw.Stop();
                //Console.WriteLine($"Assign value of {placeholder.Path} with {val}. [{sw.ElapsedMilliseconds}ms]");
            }
        }

        private void MapListValues(Workbook workbook, string sheetName, InputValues inputValues, string propertyName, int startRow = 2)
        {
            Sheets sheets = workbook.Worksheets;
            Worksheet worksheet = (Worksheet)sheets[sheetName]; ;
            int itemCount = ((JArray)inputValues.ParseValue(propertyName))?.Count ?? 0;

            ClearList(worksheet, startRow);

            Names names = worksheet.Names;
            List<ExcelPlaceholder> rowPlaceholders;

            for (int itemIndex = 0; itemIndex < itemCount; itemIndex++)
            {
                rowPlaceholders = GetRowItemPlaceholders(worksheet, itemIndex);
                foreach (ExcelPlaceholder placeholder in rowPlaceholders)
                {
                    Range cell = null;
                    try
                    {
                        // Map input value to current cell.
                        cell = worksheet.Range[$"{placeholder}"];
                        object value = inputValues.ParseValue(placeholder.Path);
                        cell.Value2 = value;

                        // Create placeholder for below cell if current itemIndex is not last. 
                        bool isLastItemIndex = itemIndex == (itemCount - 1);
                        if (!isLastItemIndex)
                        {
                            Range cellBelow = worksheet.Cells[cell.Row + 1, cell.Column];
                            ExcelPlaceholder cellBelowPlaceholder = placeholder.UpdateIndex(placeholder.Index.Value + 1);
                            names.Add($"{cellBelowPlaceholder}", cellBelow);
                            Marshal.ReleaseComObject(cellBelow);
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, $"Error getting cell of placeholder {placeholder.PlaceholderText}");
                    }
                    finally
                    {
                        if(cell != null) Marshal.ReleaseComObject(cell);
                    }
                }
            }
        }

        private List<ExcelPlaceholder> GetRowItemPlaceholders(Worksheet worksheet, int itemIndex)
        {
            var placeholderList = new List<ExcelPlaceholder>();
            var nameList = worksheet.Names.AsList();

            foreach (Name name in nameList)
            {
                var placeholder = new ExcelPlaceholder(GetNameText(name));
                if (placeholder.IsValid() && placeholder.Index == itemIndex)
                    placeholderList.Add(placeholder);

                Marshal.ReleaseComObject(name);
            }

            return placeholderList;
        }

        private string GetNameText(Name name)
        {
            // Ex. SheetName!SampleName -> SampleName
            string nameText = name.Name;
            if (nameText.Contains("!"))
                nameText = name.Name.Substring(name.Name.IndexOf("!") + 1);
            return nameText;
        }

        private void ClearList(Worksheet worksheet, int startRow)
        {
            // Clear records after the first one.
            List<Name> wsNames = worksheet.Names.AsList();

            for(int i = wsNames.Count - 1; i >= 0; i--)
            {
                Name name = wsNames[i];
                var placeholder = new ExcelPlaceholder(name.TrimmedName());
                if (!placeholder.IsValid() || (placeholder.Index ?? -1) <= 0)
                    continue;

                Range cell = name.RefersToRange;
                cell.ClearContents();
                name.Delete();
            }
        }
    }
}
