﻿using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public class RaterTemplateRepository : IRaterTemplateRepository
    {
        const string TEMPLATE_DIRECTORY = "RaterExcel";
        private readonly AppDbContext _dbContext;

        public RaterTemplateRepository(
            AppDbContext dbContext
        ) {
            _dbContext = dbContext;
        }

        public async Task<RaterTemplate> GetByEffectivityAsync(DateTime effectiveDate, string type)
        {
            if (!(type.Equals(RaterTemplate.TYPE_EXPERIENCE)
                || type.Equals(RaterTemplate.TYPE_PREMIUM)))
                throw new ArgumentException($"{type} is an invalid type for rater template.");

            var raterTemplate = _dbContext.RaterTemplate.ToList()
                    .OrderByDescending(x => x.EffectiveDate)
                    .FirstOrDefault(x => effectiveDate.Date >= x.EffectiveDate.Date && x.Type.Equals(type));
            raterTemplate.Content = await CreateFileContentAsync(raterTemplate);
            return raterTemplate;
        }

        public string GetVersionFromEffectiveDate(DateTime effectiveDate, string type)
        {
            if (!(type.Equals(RaterTemplate.TYPE_EXPERIENCE)
                || type.Equals(RaterTemplate.TYPE_PREMIUM)))
                throw new ArgumentException($"{type} is an invalid type for rater template.");

            RaterTemplate raterTemplate = _dbContext.RaterTemplate.ToList()
                    .OrderByDescending(x => x.EffectiveDate)
                    .FirstOrDefault(x => effectiveDate.Date >= x.EffectiveDate.Date && x.Type.Equals(type));
            return raterTemplate.Version;
        }

        private async Task<FileContent> CreateFileContentAsync(RaterTemplate raterTemplate)
        {
            // Get fullpath of template.
            string path = Path.Combine(TEMPLATE_DIRECTORY, raterTemplate.FilePath);
            // Read bytes from path and create FileContent.
            byte[] content = await File.ReadAllBytesAsync(path);
            return new FileContent(content, Path.GetExtension(path));
        }

    }


};
