﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Archiving;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating
{
    public class PremiumRaterArchiveRepository : IPremiumRaterArchiveRepository
    {
        private readonly AppDbContext _dbContext;

        public PremiumRaterArchiveRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PremiumRaterArchive> CreateAsync(PremiumRaterArchive archive)
        {
            var entity = await _dbContext.PremiumRaterArchive.AddAsync(archive);
            await _dbContext.SaveChangesAsync();
            return entity.Entity;
        }

        public async Task<PremiumRaterArchive> GetAsync(string submissionNumber, int endorsementNumber)
        {
            var entity = await _dbContext.PremiumRaterArchive
                .FirstOrDefaultAsync(x => x.SubmissionNumber == submissionNumber && x.EndorsementNumber == endorsementNumber);
            return entity;
        }

        public async Task<IEnumerable<PremiumRaterArchive>> GetAsync(PremiumRaterArchiveFilter filter)
        {
            var items = _dbContext.PremiumRaterArchive.OrderByDescending(x => x.CreatedDate).AsQueryable();

            if (filter.RecentCount is null)
            {
                items = items.AsEnumerable().Where(x =>
                {
                    var fileOffsetDate = x.CreatedDate.Add(TimeSpan.FromHours(filter.TimezoneOffset.Value));
                    var fitlerTime = fileOffsetDate.AddSeconds(-fileOffsetDate.Second).TimeOfDay;

                    bool submissionFilter = filter.Submission is null || x.SubmissionNumber.Equals(filter.Submission, StringComparison.OrdinalIgnoreCase);

                    return (fileOffsetDate.Date.Equals(filter.Date)) && submissionFilter && (filter.EndorsementNumber is null || x.EndorsementNumber == filter.EndorsementNumber);
                }).AsQueryable();
            }
            else
            {
                items = items.Take(10);
            }

            return await Task.FromResult(items.ToList());
        }

        public async Task<PremiumRaterArchive> GetByIdAsync(Guid id)
        {
            return await _dbContext.PremiumRaterArchive.FindAsync(id);
        }

        public async Task<IEnumerable<PremiumRaterArchive>> GetAsync()
        {
            var items = _dbContext.PremiumRaterArchive.AsNoTracking();
            return await Task.FromResult(items);
        }

        public async Task RemoveAsync(PremiumRaterArchive storedArchive)
        {
            if (storedArchive is null)
                return;
            _dbContext.PremiumRaterArchive.Remove(storedArchive);
            await _dbContext.SaveChangesAsync();
        }
    }
}
