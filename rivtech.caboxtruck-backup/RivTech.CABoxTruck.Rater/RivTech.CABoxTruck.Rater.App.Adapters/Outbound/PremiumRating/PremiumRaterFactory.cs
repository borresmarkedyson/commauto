﻿using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public class PremiumRaterFactory : IPremiumRaterFactory
    {
        private readonly IRaterTemplateRepository _raterTemplateRepository;
        private readonly ITempFileService _tempFileService;
        private readonly IExcelPlaceholderInputMapper _excelPlaceholderInputMapper;

        public PremiumRaterFactory(
            IRaterTemplateRepository raterTemplateRepository,
            ITempFileService tempFileService,
            IExcelPlaceholderInputMapper excelPlaceholderInputMapper
        ) {
            _raterTemplateRepository = raterTemplateRepository;
            _tempFileService = tempFileService;
            _excelPlaceholderInputMapper = excelPlaceholderInputMapper;
        }

        public async Task<IPremiumRater> CreateAsync(
            string submissionNumber,
            DateTime inceptionDate
        ) {
            RaterTemplate template = await _raterTemplateRepository
                .GetByEffectivityAsync(inceptionDate, RaterTemplate.TYPE_PREMIUM);
            string tempPath = await _tempFileService.FromContentAsync(template.Content);

            var engine = new InteropPremiumRaterEngine(
                tempPath,
                _excelPlaceholderInputMapper, 
                ResultReaderConfigs.FromRaterVersion(template.Version));

            return new PremiumRater(submissionNumber, inceptionDate, template.Content, engine);
        }


    }
}
