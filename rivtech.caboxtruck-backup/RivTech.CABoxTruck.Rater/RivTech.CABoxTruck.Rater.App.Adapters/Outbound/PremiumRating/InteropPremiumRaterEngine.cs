﻿using Microsoft.Office.Interop.Excel;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Exceptions;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating
{
    public class InteropPremiumRaterEngine : IPremiumRaterEngine
    {
        private readonly IExcelPlaceholderInputMapper _excelPlaceholderInputMapper;
        private readonly IResultReaderConfig[] _resultReaderConfigs;
        private bool _isWorkbookOpening = false;

        public InteropPremiumRaterEngine(
            string location,
            IExcelPlaceholderInputMapper excelPlaceholderInputMapper,
            IResultReaderConfig[] resultReaderConfigs
        )
        {
            Location = location;
            _excelPlaceholderInputMapper = excelPlaceholderInputMapper;
            _resultReaderConfigs = resultReaderConfigs;
        }

        protected bool EngineStopped { get; private set; }

        public string Location { get; private set; }
        protected Application ExcelApp { get; private set; }
        protected Workbook Workbook { get; private set; }

        public async Task InitializeAsync()
        {
            var success = await Task.Run(() =>
            {
                try
                {

                    ExcelApp = new Application();
                    Workbooks workbooks = ExcelApp.Workbooks;
                    _isWorkbookOpening = true;
                    Workbook = workbooks.Open(Location);
                    _isWorkbookOpening = false;

                    ExcelApp.Visible = false;
                    ExcelApp.ScreenUpdating = false;

                    // Set calculation to Manual so that when input values are mapped on each cell,
                    // the app won't keep recalculating which may extremely slow down mapping.
                    ExcelApp.Calculation = XlCalculation.xlCalculationManual;
                    return true;
                }
                catch (Exception e)
                {
                    _isWorkbookOpening = false;
                    return false;
                }
            });

            if (!success)
            {
                throw new RaterException("Failed to load premium rater file while initializing.");
            }
        }

        public OutputValues GetOutput()
        {
            var sw = new Stopwatch();
            sw.Restart();

            var outputValues = new OutputValues();

            foreach (var config in _resultReaderConfigs)
            {
                var valueReader = ResultValueReader.Create(config);
                Dictionary<string, object> values = valueReader.ReadValues(Workbook);
                outputValues.AddValues(values);
            }

            sw.Stop();
            Console.WriteLine($"Read result: [{sw.ElapsedMilliseconds}ms]");

            return outputValues;
        }

        public void MapInput(InputValues inputValues)
        {
            if (Workbook == null)
                throw new ExcelRaterNotInitializedException("Load the excel file before mapping input.");
            _excelPlaceholderInputMapper.Map(inputValues, Workbook);
        }

        public void Calculate()
        {
            var sw = new Stopwatch();
            sw.Restart();

            ExcelApp.CalculateFull();

            sw.Stop();
            Console.WriteLine($"Calculated ({sw.ElapsedMilliseconds}ms)");
        }

        public async Task SaveChangesAsync()
        {
            await Task.Run(() =>
            {
                var sw = new Stopwatch();
                sw.Restart();
                ExcelApp.Calculation = XlCalculation.xlCalculationAutomatic;
                Workbook.Save();
                sw.Stop();
                Console.WriteLine($"Premium rater engine saved. [{sw.Elapsed.Seconds}]");
            });
        }

        public async Task StopAsync()
        {
            if (EngineStopped)
                return;

            EngineStopped = true;

            await Task.Run(() =>
            {
                try
                {
                    // Wait for workbook to finish opening before closing it.
                    while (_isWorkbookOpening)
                        Thread.Sleep(1000);

                    Workbook?.Close(0);
                    Marshal.ReleaseComObject(Workbook);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForFullGCComplete();
                GC.Collect();
            });
        }

        public async Task<FileContent> ReadFileAsync()
        {
            byte[] bytes = await File.ReadAllBytesAsync(Location);
            string extension = Path.GetExtension(Location);
            return new FileContent(bytes, extension);
        }

        public async ValueTask DisposeAsync()
        {
            await StopAsync();
            Close();
        }

        public void Close()
        {

            if (ExcelApp != null)
            {
                ExcelApp.Quit();
                Marshal.FinalReleaseComObject(ExcelApp);
                ExcelApp = null;
            }
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            try
            {
                // Delete premium rater file.
                Console.WriteLine($"Deleting file {Location}.");
                if (File.Exists(Location))
                    File.Delete(Location);
            }
            catch(Exception e) { }
        }
    }
}
