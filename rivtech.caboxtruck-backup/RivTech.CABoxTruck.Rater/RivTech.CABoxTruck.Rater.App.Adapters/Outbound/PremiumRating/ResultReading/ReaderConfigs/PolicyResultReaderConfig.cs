﻿namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading.ReaderConfigs
{
    internal abstract class PolicyResultReaderConfig : ISingleResultReaderConfig
    {
        public static PolicyResultReaderConfig V1()
        {
            return new V1Config();
        }

        public string SheetName => "Pol_SysInput";
        public abstract string[] PlaceholderNames { get; }

        #region Versions

        private class V1Config : PolicyResultReaderConfig
        {
            public override string[] PlaceholderNames => new string[] {
                "GeneralLiabilityPremium",
                "AccountDriverFactor",
            };
        }

        /// Add new implementation below if there will be new versions. 

        #endregion
    }

}
