﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading.ReaderConfigs
{
    internal abstract class VehicleResultReaderConfig : IListResultReaderConfig
    {
        public static VehicleResultReaderConfig V1()
        {
            return new V1Config();
        }

        public string ValueKeyName => "Vehicle";
        public string SheetName => "Veh_SysInput";
        public string IdColumnName => "VehicleID";
        public int StartRowIndex => 2;
        public int IdColumnIndex => 1;
        public abstract Dictionary<string, string> ColumnMapping { get; }

        #region Versions

        private class V1Config : VehicleResultReaderConfig
        {
            public override Dictionary<string, string> ColumnMapping => new Dictionary<string, string>
            {
                { "Radius", "O" },
                { "RadiusTerritory", "P" },
                { "ALPremium", "Q" },
                { "PDPremium", "R" },
                { "CargoPremium", "S" },
                { "TotalPremium", "T" },
                // { "", "U" },
                { "BIPremium", "V" },
                { "PropDamagePremium", "W" },
                { "MedPayPremium", "X" },
                { "PIPPremium", "Y" },
                // { "", "Z" },
                { "UMUIM_BI_Premium", "AA" },
                { "UMUIM_PD_Premium", "AB" },
                { "CompPremium", "AC" },
                { "FireTheftPremium", "AD" },
                { "CollisionPremium", "AE" },
                { "PerVehiclePremium", "AF" },
            };
        }

        #endregion

    }
}
