﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading.ReaderConfigs
{
    /// <summary>
    /// Result reader config for Vehicle Refrigeration Premium.
    /// </summary>
    internal abstract class VehRefPremResultReaderConfig : ICustomResultReaderConfig
    {
        public static VehRefPremResultReaderConfig V1()
        {
            return new V1Config();
        }

        public string ValueKeyName => "VehicleRef";
        public string SheetName => "VehicleRating_column";
        public string IdColumnName => "VehicleID";
        public int StartRowIndex => 2;
        public int IdColumnIndex => 1;
        public abstract Dictionary<string, string> ColumnMapping { get; }

        #region Versions

        private class V1Config : VehRefPremResultReaderConfig
        {
            public override Dictionary<string, string> ColumnMapping => new Dictionary<string, string>();
        }

        #endregion

    }
}
