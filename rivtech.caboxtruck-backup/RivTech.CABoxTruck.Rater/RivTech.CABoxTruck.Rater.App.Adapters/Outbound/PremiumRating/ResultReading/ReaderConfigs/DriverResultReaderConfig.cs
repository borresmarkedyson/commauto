﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading.ReaderConfigs
{
    internal abstract class DriverResultReaderConfig : IListResultReaderConfig
    {
        public static DriverResultReaderConfig V1()
        {
            return new V1Config();
        }

        public string ValueKeyName => "Driver";
        public string SheetName => "Drvr_SysInput";
        public string IdColumnName => "DriverID";
        public int StartRowIndex => 2;
        public int IdColumnIndex => 1;
        public abstract Dictionary<string, string> ColumnMapping { get; }

        #region Versions 

        private class V1Config : DriverResultReaderConfig
        {
            public override Dictionary<string, string> ColumnMapping => new Dictionary<string, string> {
                {  "FiveYearPoints" , "Q" },
                {  "AgeFactor" , "R" },
                {  "TotalFactor" , "S" },
            };
        }

        #endregion
    }



}
