﻿using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading.ReaderConfigs;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading
{
    public class ResultReaderConfigs
    {
        public static IResultReaderConfig[] FromRaterVersion(string version)
        {
            switch (version)
            {
                case PremiumRater.Version_1:
                case PremiumRater.Version_2:
                // If version 2 of rater exists and has NO changes in result configs, uncomment the line below else create a new case.
                // case PremiumRater.Version_2:
                    return new IResultReaderConfig[] {
                        PolicyResultReaderConfig.V1(),
                        VehicleResultReaderConfig.V1(),
                        VehRefPremResultReaderConfig.V1(),
                        DriverResultReaderConfig.V1()
                    };
                // If version 2 of rater exists and HAS changes in result configs, uncomment the line below else create a new case.
                // case PremiumRater.Version_2:  
                //return new IResultReaderConfig[] {
                //    PolicyResultReaderConfig.V2(),
                //    VehicleResultReaderConfig.V1(),
                //    VehicleRatingResultReaderConfig.V2(),
                //    DriverResultReaderConfig.V1()
                //};
                default:
                    return null;
            }
        }

    }

    /// <summary>
    /// Base interface of result reader configuration.
    /// </summary>
    public interface IResultReaderConfig
    {
        /// <summary>
        /// Name of sheet where results will be read.
        /// </summary>
        string SheetName { get; }
    }

    /// <summary>
    /// Interface for singe-valued result reader configuration.
    /// </summary>
    public interface ISingleResultReaderConfig : IResultReaderConfig
    {
        /// <summary>
        /// Placeholder names of cells where values are stored.
        /// </summary>
        string[] PlaceholderNames { get; }
    }

    /// <summary>
    /// Interface for list-values result reader configuration. 
    /// </summary>
    public interface IListResultReaderConfig : IResultReaderConfig
    {
        /// <summary>
        /// Name of the list result in the final output values. 
        /// </summary>
        string ValueKeyName { get; }
        /// <summary>
        /// Columns where the list values will be read. Keys will be used as name in final output values.
        /// </summary>
        Dictionary<string, string> ColumnMapping { get; }
        /// <summary>
        /// ID name of per item results. 
        /// </summary>
        string IdColumnName { get; }
        /// <summary>
        /// Starting row index of list items. A 1-based index.
        /// </summary>
        int StartRowIndex { get; }
        /// <summary>
        /// Column index where IDs are stored. A 1-based index.
        /// </summary>
        int IdColumnIndex { get; }
    }

    /// <summary>
    /// Interface for list-values result reader configuration. 
    /// </summary>
    public interface ICustomResultReaderConfig : IResultReaderConfig
    {
        /// <summary>
        /// Name of the list result in the final output values. 
        /// </summary>
        string ValueKeyName { get; }
        /// <summary>
        /// Columns where the list values will be read. Keys will be used as name in final output values.
        /// </summary>
        Dictionary<string, string> ColumnMapping { get; }
        /// <summary>
        /// ID name of per item results. 
        /// </summary>
        string IdColumnName { get; }
        /// <summary>
        /// Starting row index of list items. A 1-based index.
        /// </summary>
        int StartRowIndex { get; }
        /// <summary>
        /// Column index where IDs are stored. A 1-based index.
        /// </summary>
        int IdColumnIndex { get; }
    }
}
