﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading
{
    internal abstract class ResultValueReader
    {
        // Returns a ResultValueReader subtype based on the given IResultReaderConfig baseConfig 
        public static ResultValueReader Create(IResultReaderConfig readerConfig)
        {
            if (readerConfig is ISingleResultReaderConfig)
            {
                return new SingleResultValueReader(readerConfig);
            }
            else if (readerConfig is IListResultReaderConfig)
            {
                return new ListResultValueReader(readerConfig);
            }
            else if (readerConfig is ICustomResultReaderConfig)
            {
                return new VehRefPremResultValueReader(readerConfig);
            }
            else throw new NotSupportedException($"{readerConfig?.GetType()} is not supported.");
        }

        protected IResultReaderConfig ReaderConfig { get; }

        public ResultValueReader(IResultReaderConfig readerConfig)
        {
            ReaderConfig = readerConfig;
        }

        public abstract Dictionary<string, object> ReadValues(Workbook workbook);

        #region Hidden implementations 

        class SingleResultValueReader : ResultValueReader
        {
            public SingleResultValueReader(IResultReaderConfig readerConfig) : base(readerConfig) { }

            public override Dictionary<string, object> ReadValues(Workbook workbook)
            {
                var values = new Dictionary<string, object>();
                var resultReaderConfig = ReaderConfig as ISingleResultReaderConfig;

                Sheets sheets = workbook.Worksheets;
                Worksheet sheet = sheets[resultReaderConfig.SheetName];

                foreach (var placeholderName in resultReaderConfig.PlaceholderNames)
                {
                    Range outputField = null;
                    object value;

                    try
                    {
                        outputField = sheet.Range[placeholderName];
                        value = ((object)outputField?.Value2).SanitizeValue();
                    }
                    catch
                    {
                        Console.WriteLine($"Error with {placeholderName}");
                        value = "ERROR";
                    }
                    finally
                    {
                        if (outputField != null)
                            Marshal.ReleaseComObject(outputField);
                    }
                    values.Add(placeholderName, value);
                }

                return values;
            }
        }

        class ListResultValueReader : ResultValueReader
        {
            public ListResultValueReader(IResultReaderConfig readerConfig) : base(readerConfig) { }

            public override Dictionary<string, object> ReadValues(Workbook workbook)
            {
                var results = new List<Dictionary<string, object>>();
                var resultReaderConfig = ReaderConfig as IListResultReaderConfig;
                Sheets sheets = workbook.Worksheets;
                Worksheet sheet = (Worksheet)sheets[resultReaderConfig.SheetName];
                int rowIndex = resultReaderConfig.StartRowIndex;

                Range usedRange = sheet.UsedRange;
                Range idCell;
                do
                {
                    idCell = (Range)usedRange[rowIndex, resultReaderConfig.IdColumnIndex];
                    if (idCell.Value2 is null)
                        break;

                    var itemResult = new Dictionary<string, object>
                    {
                        { resultReaderConfig.IdColumnName, idCell.Value2 }
                    };

                    foreach (KeyValuePair<string, string> kvpColumnMapping in resultReaderConfig.ColumnMapping)
                    {
                        string cellAddress = $"{kvpColumnMapping.Value}{rowIndex}";
                        Range columnValue = sheet.Range[cellAddress];
                        object value;
                        try
                        {
                            value = ((object)columnValue.Value2)?.SanitizeValue();
                        }
                        catch
                        {
                            value = "ERROR";
                        }
                        finally
                        {
                            if (columnValue != null)
                                Marshal.ReleaseComObject(columnValue);
                        }
                        itemResult.Add(kvpColumnMapping.Key, value);
                    }

                    results.Add(itemResult);

                    // Get id cell of next row.
                    rowIndex++;
                }
                while (!string.IsNullOrWhiteSpace($"{idCell?.Value2}"));

                return new Dictionary<string, object> {
                    { resultReaderConfig.ValueKeyName, results }
                };
            }
        }

        /// <summary>
        /// Result value reader implementation for Vehicle Refrigeration Premium.
        /// </summary>
        class VehRefPremResultValueReader : ResultValueReader
        {
            public VehRefPremResultValueReader(IResultReaderConfig readerConfig) : base(readerConfig) { }

            //TODO: Move ReadValues to specific class
            public override Dictionary<string, object> ReadValues(Workbook workbook)
            {
                var results = new List<object>();
                var resultReaderConfig = ReaderConfig as ICustomResultReaderConfig;
                try
                {  
                    var sheets = workbook.Worksheets;
                    var sheet = (Worksheet)sheets[resultReaderConfig.SheetName];
                    var range = sheet.UsedRange;

                    const int refValueRow1 = 1185;
                    const int refValueRow2 = 1243;

                    var lastColumnIndex = 7; //Vehicle starts in Column G
                    for (var i = 7; i < 200; i++)
                    {
                        var refCell1 = (Range)range[refValueRow1, i];
                        var refCell2 = (Range)range[refValueRow2, i];
                        
                        if (refCell1.Value2 > 0)
                        {
                            lastColumnIndex = i;
                        }
                        results.Add(new { Value1 = refCell1.Value2, Value2 = refCell2.Value2 });
                    }
                    results = results.Take(lastColumnIndex - 6).ToList();
                }
                catch (Exception) { }

                return new Dictionary<string, object> {
                    { resultReaderConfig.ValueKeyName, results }
                };
            }
        }

        #endregion
    }

}
