﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating
{
    public class PremiumRaterDumpRepository : IPremiumRaterDumpRepository
    {
        private readonly RaterSettings _raterSettings;
        private readonly ILogger<PremiumRaterDumpRepository> _logger;

        public PremiumRaterDumpRepository(
            IOptions<RaterSettings> raterSettingsOptions,
            ILogger<PremiumRaterDumpRepository> logger
        )
        {
            _raterSettings = raterSettingsOptions.Value;
            _logger = logger;
        }

        public async Task CreateAsync(PremiumRaterDump dump)
        {
            string dumpDirectory = _raterSettings.PremiumRaterDumpDirectory;
            if (string.IsNullOrWhiteSpace(dumpDirectory))
                throw new InvalidOperationException("Premium rater dump directory is not set.");
            string path = Path.Combine(dumpDirectory, dump.Filename);
            await File.WriteAllBytesAsync(path, dump.Content.Bytes);
        }

        public IEnumerable<PremiumRaterDump> Get(PremiumRaterDumpFilter filter)
        {
            string dumpDirectory = _raterSettings.PremiumRaterDumpDirectory;
            var files = GetFiles().OrderByDescending(x => x.CreatedDateUtc).AsEnumerable();

            if (filter.RecentCount is null)
            {
                files = files.Where(x =>
                {
                    var fileOffsetDate = x.CreatedDateUtc.Add(TimeSpan.FromHours(filter.TimezoneOffset.Value));
                    var fitlerTime = fileOffsetDate.AddSeconds(-fileOffsetDate.Second).TimeOfDay;

                    var tsFrom = TimeSpan.ParseExact(filter.TimeFrom ?? "00:00", "hh\\:mm", null);
                    var tsTo = TimeSpan.ParseExact(filter.TimeTo ?? "23:59", "hh\\:mm", null);

                    bool submissionFilter = filter.Submission is null || x.SubmissionNumber.Equals(filter.Submission, StringComparison.OrdinalIgnoreCase);
                    bool timeFromFilter = (filter.TimeFrom is null || tsFrom.TotalSeconds <= fitlerTime.TotalSeconds);
                    bool timeToFilter = (filter.TimeTo is null || tsTo.TotalSeconds >= fitlerTime.TotalSeconds);

                    return (fileOffsetDate.Date.Equals(filter.Date))
                        && submissionFilter
                        && (filter.Option is null || x.OptionId == filter.Option)
                        && timeFromFilter
                        && timeToFilter;
                });
            }
            else
            {
                files = files.Take(10);
            }

            return files.ToList();
        }


        public FileContent GetDumpFile(DateTime localDate, string filename)
        {
            string dumpDirectory = _raterSettings.PremiumRaterDumpDirectory;
            string path = Path.Combine(dumpDirectory, filename);
            byte[] bytes = File.ReadAllBytes(path);
            return new FileContent(bytes, Path.GetExtension(filename));
        }

        private IEnumerable<PremiumRaterDump> GetFiles()
        {
            return Directory.GetFiles(_raterSettings.PremiumRaterDumpDirectory)
                .Select(x =>
                {
                    try
                    {
                        var filename = Path.GetFileName(x);
                        return PremiumRaterDump.FromFilename(filename);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, $"Error reading file {x}.");
                        return null;
                    }
                })
                .Where(x => x != null)
                .OrderByDescending(x => x.CreatedDateUtc);
        }

    }
}
