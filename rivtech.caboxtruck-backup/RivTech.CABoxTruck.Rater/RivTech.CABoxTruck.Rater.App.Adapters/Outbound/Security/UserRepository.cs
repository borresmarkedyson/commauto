﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.Security
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(User user)
        {
            _dbContext.User.Add(user);
            _dbContext.SaveChanges();
        }

        public User GetByUsername(string username)
        {
            return _dbContext.User.AsNoTracking().SingleOrDefault(x => x.Username == username);
        }

        public User Get(int userId)
        {
            return _dbContext.User.AsNoTracking().SingleOrDefault(x => x.UserId == userId);
        }

        public IEnumerable<User> GetUsers()
        {
            return _dbContext.User.AsNoTracking().ToList().Select(x =>
            {
                x.ClearPasswordAndSalt();
                return x;
            }).ToList();
        }

        public void Update(User user)
        {
            _dbContext.User.Update(user);
            _dbContext.SaveChanges();
        }


        public void Delete(string username)
        {
            var user = GetByUsername(username);
            _dbContext.User.Remove(user);
            _dbContext.SaveChanges();
        }
    }
}
