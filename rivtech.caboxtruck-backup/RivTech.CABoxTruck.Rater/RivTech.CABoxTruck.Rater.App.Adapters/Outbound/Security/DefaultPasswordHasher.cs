﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.Security;
using System;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound.Security
{
    public class DefaultPasswordHasher : IPasswordHasher
    {
        public string Hash(string password, string salt)
        {
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Encoding.UTF8.GetBytes(salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}
