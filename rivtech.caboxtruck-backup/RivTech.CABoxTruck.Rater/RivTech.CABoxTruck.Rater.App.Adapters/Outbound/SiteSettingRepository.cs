﻿using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Persistence;
using System.Linq;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public class SiteSettingRepository : ISiteSettingRepository
    {
        private readonly AppDbContext _dbContext;

        public SiteSettingRepository(
            AppDbContext dbContext
        ) {
            _dbContext = dbContext;
        }

        public SiteSetting Get(string key)
        {
            return _dbContext.SiteSetting.FirstOrDefault(x => x.Name.Equals(key));
        }
    }
}
