﻿using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Adapters.Outbound
{
    public class TempFileService : ITempFileService
    {
        private readonly ILogger<TempFileService> _logger;

        public TempFileService(
            ILogger<TempFileService> logger
        )
        {
            _logger = logger;
        }

        public async Task<string> FromContentAsync(FileContent fileContent, string subPath = null)
        {
            string tempPath = GetParentTempPath();
            if (!string.IsNullOrWhiteSpace(subPath))
                tempPath = Path.Combine(tempPath, subPath);
            string fileName = $"{Guid.NewGuid()}_{DateTime.Now:yyyyMMddhhmmss}{fileContent.Extension}";
            tempPath = Path.Combine(tempPath, fileName);
            await File.WriteAllBytesAsync(tempPath, fileContent.Bytes);
            return tempPath;
        }

        private string GetParentTempPath()
        {
            string tempPath = Path.Combine(Directory.GetCurrentDirectory(), "temp");
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            return tempPath;
        }

        /// <summary>
        /// A fallback method in case temp files are not successfully deleted.
        /// </summary>
        public void ClearTempFiles()
        {
            string tempPath = Path.Combine(Directory.GetCurrentDirectory(), "temp");
            var files = Directory.GetFiles(tempPath)?.ToArray() ?? new string[0];
            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    File.Delete(files[i]);
                }
                catch (Exception e)
                {
                    _logger.LogError($"Error deleting temp file {files[i]}.", e);
                }
            }
        }
    }
}
