﻿namespace RaterTemplateUpdateToolNetcore
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dialogRaterOpenForExtraction = new System.Windows.Forms.OpenFileDialog();
            this.btnOpenRaterForExtractionDialog = new System.Windows.Forms.Button();
            this.btnExtractNames = new System.Windows.Forms.Button();
            this.dialogSaveExtractedNames = new System.Windows.Forms.SaveFileDialog();
            this.btnSelectTemplateToUpdate = new System.Windows.Forms.Button();
            this.btnUpdateTemplate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOpenExtract = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dialogOpenTemplateToUpdate = new System.Windows.Forms.OpenFileDialog();
            this.dialogSaveUpdatedTemplate = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dialogRaterOpenForExtraction
            // 
            this.dialogRaterOpenForExtraction.FileName = "openFileDialog1";
            this.dialogRaterOpenForExtraction.FileOk += new System.ComponentModel.CancelEventHandler(this.dialogRaterOpenForExtraction_FileOk);
            // 
            // btnOpenRaterForExtractionDialog
            // 
            this.btnOpenRaterForExtractionDialog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenRaterForExtractionDialog.Location = new System.Drawing.Point(5, 20);
            this.btnOpenRaterForExtractionDialog.Name = "btnOpenRaterForExtractionDialog";
            this.btnOpenRaterForExtractionDialog.Size = new System.Drawing.Size(365, 33);
            this.btnOpenRaterForExtractionDialog.TabIndex = 0;
            this.btnOpenRaterForExtractionDialog.Text = "1. Open rater";
            this.btnOpenRaterForExtractionDialog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpenRaterForExtractionDialog.UseVisualStyleBackColor = true;
            this.btnOpenRaterForExtractionDialog.Click += new System.EventHandler(this.btnOpenRaterForExtractionDialog_Click);
            // 
            // btnExtractNames
            // 
            this.btnExtractNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExtractNames.Location = new System.Drawing.Point(5, 58);
            this.btnExtractNames.Name = "btnExtractNames";
            this.btnExtractNames.Size = new System.Drawing.Size(365, 36);
            this.btnExtractNames.TabIndex = 2;
            this.btnExtractNames.Text = "2. Extract";
            this.btnExtractNames.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExtractNames.UseVisualStyleBackColor = true;
            this.btnExtractNames.Click += new System.EventHandler(this.btnExtractNames_Click);
            // 
            // dialogSaveExtractedNames
            // 
            this.dialogSaveExtractedNames.FileOk += new System.ComponentModel.CancelEventHandler(this.dialogSaveExtractedNames_FileOk);
            // 
            // btnSelectTemplateToUpdate
            // 
            this.btnSelectTemplateToUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectTemplateToUpdate.Location = new System.Drawing.Point(5, 21);
            this.btnSelectTemplateToUpdate.Name = "btnSelectTemplateToUpdate";
            this.btnSelectTemplateToUpdate.Size = new System.Drawing.Size(365, 36);
            this.btnSelectTemplateToUpdate.TabIndex = 4;
            this.btnSelectTemplateToUpdate.Text = "4. Select template to update";
            this.btnSelectTemplateToUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelectTemplateToUpdate.UseVisualStyleBackColor = true;
            this.btnSelectTemplateToUpdate.Click += new System.EventHandler(this.btnSelectTemplateToUpdate_Click);
            // 
            // btnUpdateTemplate
            // 
            this.btnUpdateTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateTemplate.Location = new System.Drawing.Point(5, 63);
            this.btnUpdateTemplate.Name = "btnUpdateTemplate";
            this.btnUpdateTemplate.Size = new System.Drawing.Size(365, 36);
            this.btnUpdateTemplate.TabIndex = 5;
            this.btnUpdateTemplate.Text = "5. Update template";
            this.btnUpdateTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateTemplate.UseVisualStyleBackColor = true;
            this.btnUpdateTemplate.Click += new System.EventHandler(this.btnUpdateTemplate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnOpenExtract);
            this.groupBox1.Controls.Add(this.btnExtractNames);
            this.groupBox1.Controls.Add(this.btnOpenRaterForExtractionDialog);
            this.groupBox1.Location = new System.Drawing.Point(10, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 144);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Extract placeholders(Names in excel) into a csv file.";
            // 
            // btnOpenExtract
            // 
            this.btnOpenExtract.Location = new System.Drawing.Point(5, 100);
            this.btnOpenExtract.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOpenExtract.Name = "btnOpenExtract";
            this.btnOpenExtract.Size = new System.Drawing.Size(365, 36);
            this.btnOpenExtract.TabIndex = 3;
            this.btnOpenExtract.Text = "3. Open extract to edit";
            this.btnOpenExtract.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOpenExtract.UseVisualStyleBackColor = true;
            this.btnOpenExtract.Click += new System.EventHandler(this.btnOpenExtract_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnUpdateTemplate);
            this.groupBox2.Controls.Add(this.btnSelectTemplateToUpdate);
            this.groupBox2.Location = new System.Drawing.Point(10, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 110);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update new template with update placeholders";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 275);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(396, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // dialogOpenTemplateToUpdate
            // 
            this.dialogOpenTemplateToUpdate.FileName = "openFileDialog1";
            // 
            // dialogSaveUpdatedTemplate
            // 
            this.dialogSaveUpdatedTemplate.FileOk += new System.ComponentModel.CancelEventHandler(this.dialogSaveUpdatedTemplate_FileOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 297);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnOpenRaterForExtractionDialog;
        private System.Windows.Forms.OpenFileDialog dialogRaterOpenForExtraction;
        private System.Windows.Forms.Button btnExtractNames;
        private System.Windows.Forms.SaveFileDialog dialogSaveExtractedNames;
        private System.Windows.Forms.Button btnSelectTemplateToUpdate;
        private System.Windows.Forms.Button btnUpdateTemplate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnOpenExtract;
        private System.Windows.Forms.OpenFileDialog dialogOpenTemplateToUpdate;
        private System.Windows.Forms.SaveFileDialog dialogSaveUpdatedTemplate;
    }
}

