﻿using CsvHelper;
using Microsoft.Office.Interop.Excel;
using RivTech.CABoxTruck.Rater.App.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaterTemplateUpdateToolNetcore
{
    public partial class MainForm : Form
    {
        string currentTempFileExtract;
        string currentTempFileUpdated;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenRaterForExtractionDialog_Click(object sender, EventArgs e)
        {
            dialogRaterOpenForExtraction.ShowDialog();
        }

        private void dialogRaterOpenForExtraction_FileOk(object sender, CancelEventArgs e)
        {
            btnOpenRaterForExtractionDialog.Text = $"File: {dialogRaterOpenForExtraction.FileName}";
        }

        private async void btnExtractNames_Click(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(() => ExtractNames());
                dialogSaveExtractedNames.ShowDialog();
                Console.WriteLine($"Saved at {dialogSaveExtractedNames.FileName}");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}\n{ex.StackTrace}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ExtractNames()
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            Workbook workbook = null;

            try
            {
                app.ReferenceStyle = XlReferenceStyle.xlA1;
                Workbooks workbooks = app.Workbooks;
                workbook = workbooks.Open(dialogRaterOpenForExtraction.FileName);
                Sheets sheets = workbook.Worksheets;

                var items = new List<PlaceholderNameItem>();

                // Get Policy system input placeholders.
                Worksheet policyInputSheet = (Worksheet)sheets["Pol_SysInput"];
                Names policyNames = policyInputSheet.Names;
                for (int n = 1; n <= policyNames.Count; n++)
                {
                    Name name = policyNames.Item(n);
                    string nameText = name.Name.Replace("Pol_SysInput!", "");
                    var excelPlaceholder = new ExcelPlaceholder(nameText);
                    if (!excelPlaceholder.IsValid())
                        continue;
                    items.Add(new PlaceholderNameItem
                    {
                        Placeholder = excelPlaceholder.PlaceholderText,
                        CellAddress = name.RefersToLocal.ToString().Replace("=", "")
                    });
                }


                // Get Policy system input placeholders.
                Worksheet vehicleInputSheet = (Worksheet)sheets["Veh_SysInput"];
                Names vehicleNames = vehicleInputSheet.Names;
                for (int n = 1; n <= vehicleNames.Count; n++)
                {
                    Name name = vehicleNames.Item(n);
                    string nameText = name.Name.Replace("Veh_SysInput!", "");
                    var excelPlaceholder = new ExcelPlaceholder(nameText);
                    if (!excelPlaceholder.IsValid())
                        continue;
                    items.Add(new PlaceholderNameItem
                    {
                        Placeholder = excelPlaceholder.PlaceholderText,
                        CellAddress = name.RefersToLocal.ToString().Replace("=", "")
                    });
                }

                // Get Policy system input placeholders.
                Worksheet driverInputSheet = (Worksheet)sheets["Drvr_SysInput"];
                Names driverNames = driverInputSheet.Names;
                for (int n = 1; n <= driverNames.Count; n++)
                {
                    Name name = driverNames.Item(n);
                    string nameText = name.Name.Replace("Drvr_SysInput!", "");
                    var excelPlaceholder = new ExcelPlaceholder(nameText);
                    if (!excelPlaceholder.IsValid())
                        continue;

                    var cellAddress = name.RefersToLocal;
                    items.Add(new PlaceholderNameItem
                    {
                        Placeholder = excelPlaceholder.PlaceholderText,
                        CellAddress = cellAddress.ToString().Replace("=", "")
                    });
                }

                currentTempFileExtract = Path.Combine(Directory.GetCurrentDirectory(), $"temp-file-{DateTime.Now:yyyyMMddHHmmss}.csv");
                using var writer = new StreamWriter(currentTempFileExtract);
                using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);
                csv.WriteRecords(items);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                _ = Task.Run(() =>
                {
                    if (workbook != null)
                    {
                        workbook.Close(0);
                        Marshal.ReleaseComObject(workbook);
                    }
                    if (app != null)
                        app.Quit();
                    GC.Collect();
                });
            }
        }

        private void dialogSaveExtractedNames_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                File.Move(currentTempFileExtract, dialogSaveExtractedNames.FileName, overwrite: true);
                MessageBox.Show("Extraction done.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Extraction error.\n{ex.Message}", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void btnOpenExtract_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo(dialogSaveExtractedNames.FileName)
            {
                UseShellExecute = true
            });
        }

        private void btnSelectTemplateToUpdate_Click(object sender, EventArgs e)
        {
            dialogOpenTemplateToUpdate.ShowDialog();
        }

        private async void btnUpdateTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                btnUpdateTemplate.Enabled = false;
                await Task.Run(() => UpdateTemplate());
                dialogSaveUpdatedTemplate.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Extraction error.\n{ex.Message}", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                btnUpdateTemplate.Enabled = true;
            }
        }

        private void UpdateTemplate()
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            Workbook workbook = null;

            try
            {
                using StreamReader reader = new StreamReader(dialogSaveExtractedNames.FileName);
                using CsvReader csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
                var nameItems = csvReader.GetRecords<PlaceholderNameItem>().ToList();

                string tempFilename = $"RaterUpdate-{DateTime.Now:yyyyMMddHHmmss}{Path.GetExtension(dialogOpenTemplateToUpdate.FileName)}";
                currentTempFileUpdated = Path.Combine(Directory.GetCurrentDirectory(), tempFilename);
                File.Copy(dialogOpenTemplateToUpdate.FileName, currentTempFileUpdated);

                app.ReferenceStyle = XlReferenceStyle.xlA1;
                Workbooks workbooks = app.Workbooks;
                workbook = workbooks.Open(currentTempFileUpdated);
                Sheets sheets = workbook.Worksheets;

                Worksheet policyInputSheet = (Worksheet)sheets["Pol_SysInput"];
                Names policyNames = policyInputSheet.Names;
                foreach(PlaceholderNameItem item in nameItems.Where(x => x.CellAddress.StartsWith("Pol_SysInput!")))
                {
                    policyNames.Add(item.Placeholder, policyInputSheet.Range[item.CellAddress]);
                }

                Worksheet vehicleInputSheet = (Worksheet)sheets["Veh_SysInput"];
                Names vehicleNames = vehicleInputSheet.Names;
                foreach(PlaceholderNameItem item in nameItems.Where(x => x.CellAddress.StartsWith("Veh_SysInput!")))
                {
                    vehicleNames.Add(item.Placeholder, vehicleInputSheet.Range[item.CellAddress]);
                }

                Worksheet driverInputSheet = (Worksheet)sheets["Drvr_SysInput"];
                Names driverNames = driverInputSheet.Names;
                foreach(PlaceholderNameItem item in nameItems.Where(x => x.CellAddress.StartsWith("Drvr_SysInput!")))
                {
                    driverNames.Add(item.Placeholder, driverInputSheet.Range[item.CellAddress]);
                }

                workbook.Save();
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                _ = Task.Run(() =>
                {
                    if (workbook != null)
                    {
                        workbook.Close(0);
                        Marshal.ReleaseComObject(workbook);
                    }
                    if (app != null)
                    {
                        app.Quit();
                        Marshal.ReleaseComObject(app);
                    }
                    GC.Collect();
                });
            }
        }

        private void dialogSaveUpdatedTemplate_FileOk(object sender, CancelEventArgs e)
        {
            File.Move(currentTempFileUpdated, dialogSaveUpdatedTemplate.FileName, overwrite: true);
            MessageBox.Show("Template updated.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
