﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations;

namespace RivTech.CABoxTruck.Rater.App.Persistence
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ExperienceRaterEntityConfig());
            modelBuilder.ApplyConfiguration(new PremiumRaterArchiveEntityConfig());
            modelBuilder.ApplyConfiguration(new RaterTemplateEntityConfig());
            modelBuilder.ApplyConfiguration(new SiteSettingEntityConfig());
            modelBuilder.ApplyConfiguration(new UserEntityConfig());
        }

        #region Tables
        // Arrange alphabetically, thank you.
        public DbSet<ExperienceRater> ExperienceRater { get; set; }
        public DbSet<PremiumRaterArchive> PremiumRaterArchive { get; set; }
        public DbSet<RaterTemplate> RaterTemplate { get; set; }
        public DbSet<SiteSetting> SiteSetting { get; set; }
        public DbSet<User> User { get; set; }
        #endregion
    }
}
