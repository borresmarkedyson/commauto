﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Rater.App.Persistence.Migrations
{
    public partial class PremiumRaterArchive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PremiumRaterArchive",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SubmissionNumber = table.Column<string>(nullable: false),
                    EndorsementNumber = table.Column<int>(nullable: false),
                    InputValues = table.Column<string>(nullable: false),
                    OutputValues = table.Column<string>(nullable: true),
                    RatingElapsed = table.Column<double>(nullable: false),
                    FileName = table.Column<string>(nullable: false),
                    RelativePath = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PremiumRaterArchive", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PremiumRaterArchive");
        }
    }
}
