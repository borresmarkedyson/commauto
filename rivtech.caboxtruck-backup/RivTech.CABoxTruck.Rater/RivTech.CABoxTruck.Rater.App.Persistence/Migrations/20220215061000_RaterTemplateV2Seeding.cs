﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Rater.App.Persistence.Migrations
{
    public partial class RaterTemplateV2Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "RaterTemplate",
                columns: new[] { "Version", "Type", "EffectiveDate", "FilePath" },
                values: new object[] { "v2", "X", new DateTime(2022, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "v2/ExperienceRater.xlsb" });

            migrationBuilder.InsertData(
                table: "RaterTemplate",
                columns: new[] { "Version", "Type", "EffectiveDate", "FilePath" },
                values: new object[] { "v2", "P", new DateTime(2022, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "v2/PremiumRater.xlsb" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RaterTemplate",
                keyColumns: new[] { "Version", "Type" },
                keyValues: new object[] { "v2", "P" });

            migrationBuilder.DeleteData(
                table: "RaterTemplate",
                keyColumns: new[] { "Version", "Type" },
                keyValues: new object[] { "v2", "X" });
        }
    }
}
