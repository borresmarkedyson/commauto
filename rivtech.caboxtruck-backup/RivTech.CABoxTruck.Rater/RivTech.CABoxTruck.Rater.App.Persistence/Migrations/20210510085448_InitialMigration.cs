﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Rater.App.Persistence.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExperienceRater",
                columns: table => new
                {
                    SubmissionNumber = table.Column<string>(nullable: false),
                    Content_Bytes = table.Column<byte[]>(nullable: true),
                    Content_Extension = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperienceRater", x => x.SubmissionNumber);
                });

            migrationBuilder.CreateTable(
                name: "RaterTemplate",
                columns: table => new
                {
                    Version = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    EffectiveDate = table.Column<DateTime>(nullable: false),
                    FilePath = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaterTemplate", x => new { x.Version, x.Type });
                });

            migrationBuilder.CreateTable(
                name: "SiteSetting",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteSetting", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    Permissions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });

            migrationBuilder.InsertData(
                table: "RaterTemplate",
                columns: new[] { "Version", "Type", "EffectiveDate", "FilePath" },
                values: new object[] { "v1", "X", new DateTime(2021, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "v1/ExperienceRater.xlsb" });

            migrationBuilder.InsertData(
                table: "RaterTemplate",
                columns: new[] { "Version", "Type", "EffectiveDate", "FilePath" },
                values: new object[] { "v1", "P", new DateTime(2021, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "v1/PremiumRater.xlsb" });

            migrationBuilder.InsertData(
                table: "SiteSetting",
                columns: new[] { "Name", "Value" },
                values: new object[] { "ApiKey", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExperienceRater");

            migrationBuilder.DropTable(
                name: "RaterTemplate");

            migrationBuilder.DropTable(
                name: "SiteSetting");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
