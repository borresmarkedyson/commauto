﻿using RivTech.CABoxTruck.Rater.App.Domain;
using System;

namespace RivTech.CABoxTruck.Rater.App.Persistence.Seedings
{
    public static class RaterTemplateSeeding
    {
        internal static RaterTemplate[] Get()
        {
            return new RaterTemplate[]
            {
                // V1
                new RaterTemplate("v1", "X", DateTime.Parse("1/1/2021"), "v1/ExperienceRater.xlsb"),
                new RaterTemplate("v1", "P", DateTime.Parse("1/1/2021"), "v1/PremiumRater.xlsb"),
                // V2
                new RaterTemplate("v2", "X", DateTime.Parse("2/15/2022"), "v2/ExperienceRater.xlsb"),
                new RaterTemplate("v2", "P", DateTime.Parse("2/15/2022"), "v2/PremiumRater.xlsb"),
            };
        }
        
    }
}
