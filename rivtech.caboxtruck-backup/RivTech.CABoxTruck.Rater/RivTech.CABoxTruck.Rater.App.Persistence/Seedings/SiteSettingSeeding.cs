﻿using RivTech.CABoxTruck.Rater.App.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Persistence.Seedings
{
    public static class SiteSettingSeeding
    {
        public static SiteSetting[] Get()
        {
            return new SiteSetting[]
            {
                new SiteSetting(SiteSetting.NAME_API_KEY, "") // Manually insert API Key after migration.
            };
        }
    }
}
