﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Rater.App.Domain.Security;
using System;
using System.Linq;

namespace RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations
{
    public class UserEntityConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.UserId);
            builder.Ignore(x => x.Token); // Do not map token.
            builder.Property(e => e.Permissions)
                .HasConversion(
                    modelValue => string.Join(',', modelValue),
                    dbValue => dbValue.Split(',', StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => Convert.ToInt32(x)).ToArray()
                );
        }
    }
}
