﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;

namespace RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations
{
    public class ExperienceRaterEntityConfig : IEntityTypeConfiguration<ExperienceRater>
    {
        public void Configure(EntityTypeBuilder<ExperienceRater> builder)
        {
            builder.HasKey(x => x.SubmissionNumber);
            builder.Property(x => x.Version).IsRequired();
            builder.Property(x => x.DateModified).IsRequired();
            builder.Ignore(x => x.Engine);

            var content = builder.OwnsOne(x => x.Content);
            content.Property(x => x.Extension).IsRequired();
            content.Property(x => x.Bytes).IsRequired();
        }
    }
}
