﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Persistence.Seedings;

namespace RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations
{
    public class RaterTemplateEntityConfig : IEntityTypeConfiguration<RaterTemplate>
    {
        public void Configure(EntityTypeBuilder<RaterTemplate> builder)
        {
            builder.HasKey(x => new { x.Version, x.Type });
            builder.Property(x => x.EffectiveDate).IsRequired();
            builder.Property(x => x.FilePath).IsRequired();
            builder.Ignore(x => x.Content);

            builder.HasData(RaterTemplateSeeding.Get());
        }
    }
}
