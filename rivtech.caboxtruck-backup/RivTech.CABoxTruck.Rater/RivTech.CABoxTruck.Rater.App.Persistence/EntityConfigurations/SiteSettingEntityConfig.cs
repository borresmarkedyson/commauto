﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Persistence.Seedings;

namespace RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations
{
    public class SiteSettingEntityConfig : IEntityTypeConfiguration<SiteSetting>
    {
        public void Configure(EntityTypeBuilder<SiteSetting> builder)
        {
            builder.HasKey(x => x.Name);
            builder.Property(x => x.Value);
            builder.HasData(SiteSettingSeeding.Get());
        }
    }
}
