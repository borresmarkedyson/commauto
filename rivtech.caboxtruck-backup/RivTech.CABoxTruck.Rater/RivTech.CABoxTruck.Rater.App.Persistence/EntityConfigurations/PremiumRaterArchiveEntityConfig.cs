﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.ExperienceRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.ResultArchive;

namespace RivTech.CABoxTruck.Rater.App.Persistence.EntityConfigurations
{
    public class PremiumRaterArchiveEntityConfig : IEntityTypeConfiguration<PremiumRaterArchive>
    {
        public void Configure(EntityTypeBuilder<PremiumRaterArchive> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SubmissionNumber).IsRequired();
            builder.Property(x => x.EndorsementNumber).IsRequired();
            builder.Property(x => x.InputValues).IsRequired();
            builder.Property(x => x.OutputValues);
            builder.Property(x => x.RatingElapsed).IsRequired();
            builder.Property(x => x.FileName).IsRequired();
            builder.Property(x => x.RelativePath).IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();
        }

    }
}
