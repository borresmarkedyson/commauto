﻿using CommAuto.Rating.Domain;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating
{
    public abstract class ExcelRatingMethod : IRatingMethod
    {
        /// <summary>
        /// Values used for rating.
        /// </summary>
        protected Dictionary<string, object> InputValues { get; private set; }

        protected virtual void Initialize() { }

        protected abstract void ValidateInput();

        protected abstract void MapInput();
        protected abstract void MapVehiclesInput();

        protected abstract void RecalculateFormula();

        protected abstract RaterResult GetResult();

        public RaterResult Rate(Dictionary<string, object> inputValues)
        {
            InputValues = inputValues;

            Initialize();

            ValidateInput();

            MapInput();

            MapVehiclesInput();

            RecalculateFormula();

            return GetResult();
        }

        /// <summary>
        /// Searches the value in InputValues based from the given property name. 
        /// </summary>
        /// <returns>Returns emtpy string if not found.</returns>
        protected string SearchValueByPropertyName(Dictionary<string, object> obj, string propertyName)
        {
            string[] propertyHierarchy = propertyName.Trim("{}".ToCharArray()).Split('.');
            string value;
            try
            {
                int level = 0;
                object currentProperty = obj[propertyHierarchy[level]];
                while ((propertyHierarchy.Length - 1) > level)
                {
                    level++;
                    var deserializedProperty = JsonConvert.DeserializeObject<Dictionary<string, object>>(currentProperty.ToString());
                    currentProperty = deserializedProperty[propertyHierarchy[level]];
                }
                value = $"{currentProperty}";
            }
            catch (KeyNotFoundException e)
            {
                // No value was provided for the cell input field.
                value = string.Empty;
            }

            return value;
        }

        protected bool HasPlaceholder(string text)
        {
            if (text == null) return false;
            Regex inputRegex = new Regex(@"^{\s*[a-zA-Z]([a-zA-Z0-9]|[(\s*\.\s*)a-zA-Z0-9])*\s*}", RegexOptions.Singleline);
            return inputRegex.Match(text).Success;
        }

        public virtual void Dispose() { }
    }
}
