﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating.FilePool
{
    public class ExcelRaterFile
    {
        public ExcelRaterFile(string path, FileStream fileStream)
        {
            Path = path;
            FileStream = fileStream;
        }
        public string Path { get; }
        public FileStream FileStream { get;  }
    }
}
