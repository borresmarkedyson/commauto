﻿namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating.FilePool
{
    public interface IExcelRaterFilePoolFactory
    {
        ExcelRaterFilePool Create(string version);
    }
}