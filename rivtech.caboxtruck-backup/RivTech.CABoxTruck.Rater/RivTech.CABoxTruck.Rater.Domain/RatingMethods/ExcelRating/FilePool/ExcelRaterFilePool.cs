﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating.FilePool
{
    public class ExcelRaterFilePool : IDisposable
    {
        /// <summary>
        /// Contains the available excel rater file streams to be used for rating.
        /// </summary>
        private ConcurrentBag<FileStream> FileStreams { get; }

        int MaxPoolSize { get; }
        string FilePath { get; }
        string RaterVersion { get; }
        string PoolDirectory => Path.Combine("rater_pool", RaterVersion);

        public ExcelRaterFilePool(int maxPoolSize, string path, string raterVersion)
        {
            FileStreams = new ConcurrentBag<FileStream>();
            MaxPoolSize = maxPoolSize;
            FilePath = path;
            RaterVersion = raterVersion;
        }

        public void Initialize()
        {
            for (int i = 1; i <= MaxPoolSize; i++)
            {
                string newFileName = $"{Path.GetFileNameWithoutExtension(FilePath)}_pool_{i}{Path.GetExtension(FilePath)}";
                if (!Directory.Exists(PoolDirectory)) {
                    Directory.CreateDirectory(PoolDirectory);
                }
                string newFilePath = Path.Combine(PoolDirectory, newFileName);
                File.Copy(FilePath, newFilePath, true);
                var fs = new FileStream(newFilePath, FileMode.Open, FileAccess.ReadWrite);
                FileStreams.Add(fs);
            }
        }

        public ExcelRaterFile Get()
        {
            return new ExcelRaterFile(
                FilePath,
                FileStreams.TryTake(out FileStream fileStream) ? fileStream : throw new Exception()
            );
        }

        public void Dispose()
        {
            Directory.Delete(PoolDirectory, true);
        }
    }
}
