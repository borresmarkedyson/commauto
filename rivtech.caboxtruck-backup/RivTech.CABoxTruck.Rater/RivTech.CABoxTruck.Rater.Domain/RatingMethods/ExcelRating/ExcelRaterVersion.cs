﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating
{
    public class ExcelRaterVersion
    {
        public const string VERSION_1 = "v1";
        public const string VERSION_2 = "v2";
    }
}
