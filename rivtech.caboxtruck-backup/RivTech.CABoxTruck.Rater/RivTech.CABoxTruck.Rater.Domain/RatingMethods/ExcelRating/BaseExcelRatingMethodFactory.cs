﻿using CommAuto.Rating.Domain.RatingMethods.ExcelRating.FilePool;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain.RatingMethods.ExcelRating
{
    public abstract class BaseExcelRatingMethodFactory
    {
        protected IExcelRaterFilePoolFactory ExcelRaterFilePoolFactory { get; }

        public BaseExcelRatingMethodFactory(
            IExcelRaterFilePoolFactory excelRaterFilePoolFactory
        ) {
            ExcelRaterFilePoolFactory = excelRaterFilePoolFactory;
        }

        public abstract ExcelRatingMethod Create(string version);
    }
}
