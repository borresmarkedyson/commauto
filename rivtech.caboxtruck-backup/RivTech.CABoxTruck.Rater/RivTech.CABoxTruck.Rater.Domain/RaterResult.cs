﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    public class RaterResult
    {
        public string RaterVersion { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public string Error { get; set; }
    }
}
