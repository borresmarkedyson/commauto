﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    /// <summary>
    /// Abstracts the creation BaseRaterEngine object.
    /// </summary>
    public interface IRatingMethodFactory
    {
        /// <summary>
        /// Creates BaseRaterEngine object based on type and version provided.
        /// </summary>
        IRatingMethod Create(RatingMethodType ratingMethod, string version, bool? useDefault = false);
    }
}
