﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    public class VehicleRatingResult
    {
        public int VehicleID { get; set; }
        public double Premium { get; set; }
        public double TotalLiabilityPremium { get; set; }

        public override string ToString()
        {
            return $"ID: {VehicleID}\tTotal Premium: {Premium}\tTotal Liability Premium: {TotalLiabilityPremium:F4}";
        }
    }
}
