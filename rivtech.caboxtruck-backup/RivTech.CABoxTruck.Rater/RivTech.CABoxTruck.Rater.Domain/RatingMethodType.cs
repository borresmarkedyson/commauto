﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    public enum RatingMethodType
    {
        Excel = 1,
        Coded = 2
    }
}
