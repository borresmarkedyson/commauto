﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    public class RaterInput
    {
        public bool UseDefaultMethod { get; set; }
        public int? RatingMethodType { get; set; }
        public string Version { get; set; }

        public Dictionary<string, object> InputValues { get; set; }
    }
}
