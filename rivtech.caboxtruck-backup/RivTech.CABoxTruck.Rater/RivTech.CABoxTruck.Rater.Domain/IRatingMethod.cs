﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommAuto.Rating.Domain
{
    public interface IRatingMethod : IDisposable
    {
        RaterResult Rate(Dictionary<string, object> inputValues);
    }
}
