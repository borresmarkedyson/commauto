﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.Security;
using System;
using System.Security.Cryptography;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Tests.Adapters.Outbound.Security
{
    [TestClass]
    public class DefaultPasswordHasherTest
    {
        DefaultPasswordHasher _sut;

        [TestMethod]
        public void Hash_Test()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] saltBytes = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(saltBytes);
            }
            string salt = Encoding.UTF8.GetString(saltBytes);
            string password = "12345";
            _sut = new DefaultPasswordHasher();
            var hash = _sut.Hash(password, salt);

            Console.WriteLine($"Password:[{password}]\nSalt:[{salt}]\nHashed:[{hash}]");

            Assert.AreEqual(hash, _sut.Hash(password, salt));
        }
    }
}
