﻿using Microsoft.Extensions.Logging;
using Microsoft.Office.Interop.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace RivTech.CABoxTruck.Rater.Tests.Domain
{
#if Development
    [TestClass]
#endif
    public class ExcelPlaceholdeMapperTest
    {
        string _fileCopy;
        string _templateFilename;
        ExcelPlaceholderInputMapper _sut;
        Application _app;
        Workbook _workbook;

        [TestInitialize]
        public void Init()
        {
            var mockLogger = new Mock<ILogger<ExcelPlaceholderInputMapper>>();
            _sut = new ExcelPlaceholderInputMapper(mockLogger.Object);
        }

        private void Load()
        {
            // Check if template is existing.
            string templateFile = Path.Combine(Environment.CurrentDirectory, "Templates", _templateFilename);
            Console.WriteLine(templateFile);
            if (!File.Exists(templateFile))
                throw new FileNotFoundException($"Template '{_templateFilename}' does not exist.");
            string tempPath = Path.Combine(Environment.CurrentDirectory, "temp");
            Directory.CreateDirectory(tempPath);

            // Copy template to a temp copy file.
            _fileCopy = Path.Combine(tempPath, $"{Guid.NewGuid()}.xlsb"); ;
            File.Copy(templateFile, _fileCopy);
            
            // Open file.
            _app = new Application();
            _app.Visible = false;
            _app.ScreenUpdating = false;
            _workbook = _app.Workbooks.Open(_fileCopy);
        }

        [TestMethod]
        public void Map_Test()
        {
            _templateFilename = "ExperienceRater.xlsb";
            Load();
            var jsonInput = $@"
            {{
                ""Broker"": {{
                    ""EffectiveDate"": ""{DateTime.Now:o}"",
                    ""Commission"": 15
                }},
               ""Applicant"": {{
                    ""BusinessDetails"": {{
                        ""UseClass"": ""Courier"",
                        ""AccountCategory"": ""Funeral Services"",
                        ""YearsInBusiness"": 10,
                        ""BusinessType"": ""Sole Properietorship"",
                        ""ZipCode"": ""10001""
                    }}
                }},
                ""Driver"": {{
                    ""UseDriverRatingTab"": ""No"",
                    ""UseAccidentsOrViolationInfo"": ""No"",
                    ""KOUnder25"": ""No"",
                    ""KOAFOrMovingViolations"": ""No"",
                    ""DriverList"": [
                        {{
                            ""Number"": 1,
                            ""DriverID"": ""QQQQ"",
                            ""DateOfBirth"": ""QQQQ"",
                            ""Age"": ""QQQQ"",
                            ""OutOfState"": ""QQQQ"",
                            ""YrsDrivingExperience"": ""QQQQ"",
                            ""YrsCommercialDriverExperience"": ""QQQQ"",
                            ""Majors"": ""QQQQ"",
                            ""Drug"": ""QQQQ"",
                            ""Minors"": ""QQQQ"",
                            ""AtFault"": ""QQQQ"",
                            ""MajorLicenseIssue"": ""QQQQ"",
                            ""NAF"": ""QQQQ"",
                            ""Equipment"": ""QQQQ"",
                            ""Speed"": ""QQQQ"",
                            ""Other"": ""QQQQ""
                        }},
                        {{
                            ""Number"": 2,
                            ""DriverID"": ""QQQQ"",
                            ""DriverID"": ""WWWW"",
                            ""DateOfBirth"": ""WWWW"",
                            ""Age"": ""WWWW"",
                            ""OutOfState"": ""WWWW"",
                            ""YrsDrivingExperience"": ""WWWW"",
                            ""YrsCommercialDriverExperience"": ""WWWW"",
                            ""Majors"": ""WWWW"",
                            ""Drug"": ""WWWW"",
                            ""Minors"": ""WWWW"",
                            ""AtFault"": ""WWWW"",
                            ""MajorLicenseIssue"": ""WWWW"",
                            ""NAF"": ""WWWW"",
                            ""Equipment"": ""WWWW"",
                            ""Speed"": ""WWWW"",
                            ""Other"": ""WWWW""
                        }},
                        {{
                            ""Number"": 3,
                            ""DriverID"": ""EEEE"",
                            ""DateOfBirth"": ""EEEE"",
                            ""Age"": ""EEEE"",
                            ""OutOfState"": ""EEEE"",
                            ""YrsDrivingExperience"": ""EEEE"",
                            ""YrsCommercialDriverExperience"": ""EEEE"",
                            ""Majors"": ""EEEE"",
                            ""Drug"": ""EEEE"",
                            ""Minors"": ""EEEE"",
                            ""AtFault"": ""EEEE"",
                            ""MajorLicenseIssue"": ""EEEE"",
                            ""NAF"": ""EEEE"",
                            ""Equipment"": ""EEEE"",
                            ""Speed"": ""EEEE"",
                            ""Other"": ""EEEE""
                        }}
                    ]
                }},
                ""Vehicle"": {{
                    ""TotalVehicles"": 2,
                    ""VehicleList"":[
                        {{
                            ""Number"": 1,
                            ""VIN"": ""AAA"",
                            ""UseInPDRating"": ""Yes"",
                            ""Year"": 2015,
                            ""VehicleType"": ""Trailer"",
                            ""Description"": ""Tractor"",
                            ""CompDeductible"": ""1,500 DED"",
                            ""CollDeductible"": ""250 DED"",
                            ""CollStateAmount"": 10000,
                            ""IncCargoLimit"": ""Yes"",
                            ""IncRefCargoLimit"": ""Yes"",
                            ""UseClass"": ""Logging"",
                            ""BusinessClass"": ""Auto Dealers"",
                        }},
                        {{
                            ""Number"": 2,
                            ""VIN"": ""BBB"",
                            ""UseInPDRating"": ""Yes"",
                            ""Year"": 2018,
                            ""VehicleType"": ""Tow Truck - one axle"",
                            ""Description"": ""Pickup<= 1/2 Ton 4x4"",
                            ""CompDeductible"": ""1,500 DED"",
                            ""CollDeductible"": ""250 DED"",
                            ""CollStateAmount"": 5000,
                            ""IncCargoLimit"": ""No"",
                            ""IncRefCargoLimit"": ""No"",
                            ""UseClass"": ""Child Care"",
                            ""BusinessClass"": ""Building Materials & Hardware Stores"",
                        }}
                    ]
                }},
                ""RequestedCoverages"": {{
                    ""AutoLiability"": {{
                        ""AutoBodilyInjury"": 1001,
                        ""AutoPropertyDamage"": 1002,
                        ""AutoLiabilityDeductible"": 1003,
                        ""MedicalPaymentsLimit"": 1004,
                        ""PIPLimit"": 1005,
                        ""UMSCLLimit"": 1006,
                        ""UMPDLimit"": 1007,
                        ""UIMSCLLimit"": 1008,
                        ""UIMPDLimit"": 1009,
                        ""UMStacking"": ""N"",
                        ""SymbolsRequested"": ""4,5,6""
                    }},
                    ""PhysicalDamage"": {{
                        ""FireTheftSpecPerils"": 1012,
                        ""COMPDeductible"": 1013,
                        ""COLLDeductible"": 1014
                    }},
                    ""OtherCoverages"": {{
                        ""GLBodilyInjuryLimits"": 1015,
                        ""CargoCollLimit"": 1016,
                        ""RefrigeratedCargoLimits"": 1017
                    }}
                }},
                ""RiskSpecifics"": {{
                    ""DriverInfo"": {{
                        ""TotalDrivers"": 18,
                        ""AnyVehiclesOperatedForPersonalUse"": ""Y"",
                        ""AnnualCostOfHire"": 1018
                    }},
                    ""GeneralLiability"": {{
                        ""IsOperationAtStorageLot"": ""Y"",
                        ""Payroll"": 1019
                    }},
                    ""SafetyDevices"": {{
                        ""SafetyDeviceCategory"": {{
                            ""Cameras"": {{
                                ""CameraDiscount"": ""N""
                            }},
                            ""BrakeWarningSystem"": ""Y"",
                            ""SpeedWarningSystem"": ""Y"",
                            ""MonitoringViaTelematicsDevice"": ""Y"",
                            ""AnyActiveAccidentAvoidanceTechnology"": ""Y"",
                            ""AnyPassiveAccidentAvoidanceTechnology"": ""Y"",
                            ""DistractedDrivingEquipmentDiscount"": ""Y"",
                            ""MileageTrackingEquipment"": ""Y"",
                            ""NavigationOrGPSDataAvailable"": ""Y""
                        }}
                    }},
                    ""DriverHiringCriteria"": {{
                        ""BackgroundCheckIncluded"": {{
                            ""WrittenApplication"": ""No"",
                            ""RoadTest"": ""No"",
                            ""WrittenTest"": ""No"",
                            ""FullMedical"": ""No"",
                            ""DrugTesting"": ""No"",
                            ""CurrentMVR"": ""No"",
                            ""ReferenceChecks"": ""No"",
                            ""CriminalBackgroundCheck"": ""No""
                        }},
                        ""ReportAllDriversToRivington"": ""No"",
                        ""IsDisciplinaryPlanDocumentedForAllDrivers"": ""No"",
                        ""AllDriversProperlyLicensedAndDOTCompliant"": ""No"",
                        ""AllDriversDrivingSimilarVehicleCommerciallyFor2YrsPlus"": ""No"",
                        ""AllDriversHaveAtLeast5YrsUSDrivingExperience"": ""No""
                    }},
                    ""MaintenanceQuestions"": {{
                        ""VehicleMaintenanceProgram"": {{
                            ""ServiceRecordForEachVehicle"": ""Yes"",
                            ""ControlledAndFrequentInspections"": ""Yes"",
                            ""VehicleDailyConditionReports"": ""Yes""
                        }},
                        ""WrittenMaintenanceProgram"": ""Yes"",
                        ""WrittenDriverTrainingProgram"": ""Yes"",
                        ""WrittenSafetyProgram"": ""Yes"",
                        ""WrittenAccidentReportingProcedures"": ""Yes"",
                        ""IsMaintenanceProgramManagedByCompany"": ""Yes"",
                        ""DoYouProvideCompleteMaintenanceOnAllVehicles"": ""Yes"",
                        ""DriverFilesAvailableForReview"": ""Yes"",
                        ""AccidentFilesAvailableForReview"": ""Yes""
                    }},
                    ""DOTMiscellaneous"": {{
                        ""PolicyLevelAccicdents"": 11
                    }},
                    ""DestinationInformation"": {{
                        ""OwnerOperator"": 25,
                        ""Radius0To50Miles"": 25 ,
                        ""Radius50to200Miles"": 25,
                        ""RadiusAbove200Miles"": 50
                    }},
                    ""DOTInformation"": {{
                        ""RiskRequiresDOTNumber"": ""Yes"",
                        ""RiskHasDOTNumber"": ""1235489870"",
                        ""RiskHasInterstateAuthority"": ""No"",
                        ""RiskPlansToStayIntrastate"": ""No"",
                        ""DOTRegistrationDate"": ""2021-02-03"",
                        ""DOTCurrentlyActive"": ""No"",
                        ""ChameleonIssues"": ""Yes"",
                        ""CurrentSaferRating"": ""In Accident"",
                        ""ISSCAB"": ""Yes Info"",
                        ""BasicAlert"": {{
                            ""UnsafeDrivingBasicAlertOrWorse"": ""No"",
                            ""HoursOfServiceBasicAlertOrWorse"": ""No"",
                            ""DriverFitnessBasicAlertOrWorse"": ""No"",
                            ""ControlledSubstanceBasicAlertOrWorse"": ""No"",
                            ""VehicleMaintenanceBasicAlertOrWorse"": ""No"",
                            ""CrashBasicAlertOrWorse"": ""No""
                        }}
                    }}
                }},
                ""HistoricalCoverage"": {{
                    ""HistoricalInsuranceCoverage"": {{
                        ""NumberOfVehicles"": 19
                    }},
                    ""ProjectionsAndHistoricalFigures"": {{
                        ""GrossFleetMileage"": 20
                    }}
                }},
                ""AdditionalInterest"": {{
                    ""NoOfAdditionalInsured"": 1020,
                    ""WaiverOfSubrogation"": 1021,
                    ""PrimaryNonContributoryCoverage"": 1022 
                }}
            }}
            ";
            var deserialized = JsonConvert.DeserializeObject<JObject>(jsonInput);
            var inputValues = new InputValues(deserialized);
            Console.WriteLine(jsonInput);

            try
            {
                _app.Calculation = XlCalculation.xlCalculationManual;
                _sut.Map(inputValues, _workbook);
                _app.CalculateFull();
                _app.Calculation = XlCalculation.xlCalculationAutomatic;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}\n{e.StackTrace}");
            }
            finally
            {
                _workbook?.Save();
                _workbook?.Close(0);
                Marshal.ReleaseComObject(_workbook);
                Marshal.ReleaseComObject(_app);
                GC.Collect();

            }

            Process.Start(new ProcessStartInfo(_fileCopy)
            {
                UseShellExecute = true
            });
        }

        [TestMethod]
        public void Map_ExistingXPRater_Test()
        {
            _templateFilename = "ExisitngExperienceRaterTest.xlsb";
            Load();
            var jsonInput = $@"
            {{
                 ""Driver"": {{
                    ""UseDriverRatingTab"": ""No"",
                    ""UseAccidentsOrViolationInfo"": ""No"",
                    ""KOUnder25"": ""No"",
                    ""KOAFOrMovingViolations"": ""No"",
                    ""DriverList"": [
                        {{
                            ""Number"": 1,
                            ""DriverID"": ""QQQQ"",
                            ""DateOfBirth"": ""QQQQ"",
                            ""Age"": ""QQQQ"",
                            ""OutOfState"": ""QQQQ"",
                            ""YrsDrivingExperience"": ""QQQQ"",
                            ""YrsCommercialDriverExperience"": ""QQQQ"",
                            ""Majors"": ""QQQQ"",
                            ""Drug"": ""QQQQ"",
                            ""Minors"": ""QQQQ"",
                            ""AtFault"": ""QQQQ"",
                            ""MajorLicenseIssue"": ""QQQQ"",
                            ""NAF"": ""QQQQ"",
                            ""Equipment"": ""QQQQ"",
                            ""Speed"": ""QQQQ"",
                            ""Other"": ""QQQQ""
                        }},
                        {{
                            ""Number"": 2,
                            ""DriverID"": ""QQQQ"",
                            ""DriverID"": ""WWWW"",
                            ""DateOfBirth"": ""WWWW"",
                            ""Age"": ""WWWW"",
                            ""OutOfState"": ""WWWW"",
                            ""YrsDrivingExperience"": ""WWWW"",
                            ""YrsCommercialDriverExperience"": ""WWWW"",
                            ""Majors"": ""WWWW"",
                            ""Drug"": ""WWWW"",
                            ""Minors"": ""WWWW"",
                            ""AtFault"": ""WWWW"",
                            ""MajorLicenseIssue"": ""WWWW"",
                            ""NAF"": ""WWWW"",
                            ""Equipment"": ""WWWW"",
                            ""Speed"": ""WWWW"",
                            ""Other"": ""WWWW""
                        }},
                        {{
                            ""Number"": 3,
                            ""DriverID"": ""EEEE"",
                            ""DateOfBirth"": ""EEEE"",
                            ""Age"": ""EEEE"",
                            ""OutOfState"": ""EEEE"",
                            ""YrsDrivingExperience"": ""EEEE"",
                            ""YrsCommercialDriverExperience"": ""EEEE"",
                            ""Majors"": ""EEEE"",
                            ""Drug"": ""EEEE"",
                            ""Minors"": ""EEEE"",
                            ""AtFault"": ""EEEE"",
                            ""MajorLicenseIssue"": ""EEEE"",
                            ""NAF"": ""EEEE"",
                            ""Equipment"": ""EEEE"",
                            ""Speed"": ""EEEE"",
                            ""Other"": ""EEEE""
                        }}, 
                        {{
                            ""Number"": 4,
                            ""DriverID"": ""FFF"",
                            ""DateOfBirth"": ""FFF"",
                            ""Age"": ""FFF"",
                            ""OutOfState"": ""FFF"",
                            ""YrsDrivingExperience"": ""FFF"",
                            ""YrsCommercialDriverExperience"": ""FFF"",
                            ""Majors"": ""EEEE"",
                            ""Drug"": ""EEEE"",
                            ""Minors"": ""EEEE"",
                            ""AtFault"": ""EEEE"",
                            ""MajorLicenseIssue"": ""EEEE"",
                            ""NAF"": ""EEEE"",
                            ""Equipment"": ""EEEE"",
                            ""Speed"": ""EEEE"",
                            ""Other"": ""EEEE""
                        }}
                    ]
                }},
                ""Broker"": {{
                    ""EffectiveDate"": ""{DateTime.Now:o}"",
                    ""Commission"": 15
                }}
            }}";
            var deserialized = JsonConvert.DeserializeObject<JObject>(jsonInput);
            var inputValues = new InputValues(deserialized);

            try
            {
                _app.Calculation = XlCalculation.xlCalculationManual;
                _sut.Map(inputValues, _workbook);
                _app.CalculateFull();
                _app.Calculation = XlCalculation.xlCalculationAutomatic;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}\n{e.StackTrace}");
            }
            finally
            {
                _workbook?.Save();
                _workbook?.Close(0);
                Marshal.ReleaseComObject(_workbook);
                Marshal.ReleaseComObject(_app);
                GC.Collect();
            }

            Process.Start(new ProcessStartInfo(_fileCopy)
            {
                UseShellExecute = true
            });
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}
