﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RivTech.CABoxTruck.Rater.App.Domain;
using System.IO;
using System;
using System.Collections.Immutable;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound;
using Moq;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ResultReading;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;

namespace RivTech.CABoxTruck.Rater.App.Tests.Domain.ExcelRating
{
#if Development
    [TestClass]
#endif
    public class InteropPremiumRaterEngineTest
    {
        InteropPremiumRaterEngine _sut;

        [TestInitialize]
        public void Init()
        {
            var app = new Application();
            var path = Path.Combine(Environment.CurrentDirectory, "Rater_v1.xlsb");
            Workbook workbook = app.Workbooks.Open(path);
            var mockLogger = new Mock<ILogger<ExcelPlaceholderInputMapper>>();
            var excelPlaceholderInputMaper = new ExcelPlaceholderInputMapper(mockLogger.Object);
            _sut = new InteropPremiumRaterEngine(path, excelPlaceholderInputMaper, ResultReaderConfigs.FromRaterVersion(PremiumRater.Version_1));
        }

        //[TestMethod]
        //public void MapInput_ThrowException_WhenNotInitialized()
        //{
        //    var inputValues = new InputValues(new Dictionary<string, object> {
        //        { "TestVal", 3 }
        //    });

        //    Assert.ThrowsException<ExcelRaterNotInitializedException>(() => _sut.MapInput(inputValues));
        //}

        //[TestMethod]
        //public void MapInput_SampleTest()
        //{
        //    // Arrange
        //    var inputValues = new InputValues(new Dictionary<string, object> {
        //        { "TestVal", 3 },
        //        { "TestValB", 4 }
        //    });
        //    _sut.Initialize();

        //    // Act
        //    _sut.MapInput(inputValues);

        //    // Assert
        //    Range B3Cell = _sut.SystemInputWorksheet.UsedRange[3, 2];
        //    Range B4Cell = _sut.SystemInputWorksheet.UsedRange[4, 2];
        //    Assert.AreEqual(3d, Convert.ToDouble(B3Cell.Value2));
        //    Assert.AreEqual(4d, Convert.ToDouble(B4Cell.Value2));
        //}

        //[TestMethod]
        public void GetOutput_SampleTest()
        {
            // Arrange
            var json = @"{
                ""TestVal"": 3,
                ""TestVal2"": 5,
            }";
            var inputValues = new InputValues(JsonConvert.DeserializeObject<JObject>(json));
            _sut.InitializeAsync();
            _sut.MapInput(inputValues);
            _sut.Calculate();

            // Act
            OutputValues outputValues = _sut.GetOutput();

            // Assert
            ImmutableDictionary<string, object> data = outputValues.Data;
            Assert.AreEqual(9d, data["TestOutput"]);
            Assert.AreEqual(7d, data["TestOutput2"]);
        }
    }
}
