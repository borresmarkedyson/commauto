﻿using Castle.Core.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Dumping;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Tests.Adapters.Outbound.PremiumRating
{
#if Development
    [TestClass]
#endif
    public class PremiumRaterDumpRepositoryTest
    {
        PremiumRaterDumpRepository _sut;
        string _dumpTestPath = "test rater dump files";

        [TestInitialize]
        public async Task Init()
        {
            Directory.CreateDirectory(_dumpTestPath);
            var raterSettingsOptions = new Mock<IOptions<RaterSettings>>();
            raterSettingsOptions.Setup(x => x.Value).Returns(new RaterSettings { 
                PremiumRaterDumpDirectory = _dumpTestPath
            });
            var logger = new Mock<ILogger<PremiumRaterDumpRepository>>();
            _sut = new PremiumRaterDumpRepository(raterSettingsOptions.Object, logger.Object);

            var mockPremiumRater = new Mock<IPremiumRater>();
            mockPremiumRater.Setup(x => x.SubmissionNumber).Returns("Subnum1");
            mockPremiumRater.Setup(x => x.FileContent).Returns(new FileContent(new byte[0], ".xlsb"));
            await _sut.CreateAsync(PremiumRaterDump.FromRater(mockPremiumRater.Object));
        }

        [TestCleanup]
        public void Cleanup()
        {
            Directory.Delete(_dumpTestPath, true);
        }

        [TestMethod]
        public void GetDumpFiles()
        {
            var items = _sut.Get(new PremiumRaterDumpFilter { 
                Date = DateTime.Now,
                TimezoneOffset = 8,
                TimeFrom = "20:08"
            }).ToList();
            Assert.AreEqual(1, items.Count);
        }
    }
}
