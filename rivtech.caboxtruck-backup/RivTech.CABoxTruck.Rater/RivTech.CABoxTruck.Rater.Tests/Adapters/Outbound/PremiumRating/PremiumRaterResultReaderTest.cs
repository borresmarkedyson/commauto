﻿using Microsoft.Office.Interop.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Adapters.Outbound.PremiumRating.ReaderConfigs;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Tests.Domain.ExcelRating
{
    [TestClass]
    public class PremiumRaterResultReaderTest
    {
        PremiumRaterResultReader _sut;
        string _fileCopy;
        Workbook _workbook;
        Workbooks _workbooks;
        Application _app;

        [TestInitialize]
        public void Init()
        {
            string tempPath = Path.Combine(Environment.CurrentDirectory, "temp");
            Directory.CreateDirectory(tempPath);

            _fileCopy = Path.Combine(tempPath, $"{Guid.NewGuid()}.xlsb"); ;
            File.Copy(Path.Combine(Environment.CurrentDirectory, "rater_output_test.xlsb"), _fileCopy);
            _app = new Application();
            _workbooks = _app.Workbooks;
            _workbook = _workbooks.Open(_fileCopy);

            _sut = new PremiumRaterResultReader(ResultReaderConfigs.FromRaterVersion(PremiumRater.Version_1));
        }

        [TestMethod]
        public void ReadResults_Test()
        {
            OutputValues result = _sut.ReadResults(_workbook);
            Assert.IsNotNull(result);

            foreach (var key in result.Data.Keys)
            {
                Console.WriteLine($"{key}: {result.Data[key] ?? "null"}\t");
            }

            var vehicleResults = (List<Dictionary<string, object>>)result.Data["Vehicle"];
            foreach (var vehicleItems in vehicleResults)
            {
                foreach (var vehicleData in vehicleItems)
                {
                    Console.Write($"{vehicleData.Key}: {vehicleData.Value}\t");
                }
                Console.WriteLine();
            }
            _workbook.Close(0);
            _workbook = null;
        }

        [TestCleanup]
        public void CleanUp()
        {
            _workbook?.Close(0);
            Task.Run(() =>
            {
                if (_workbook != null)
                    Marshal.ReleaseComObject(_workbook);
                Marshal.ReleaseComObject(_workbooks);
                Marshal.ReleaseComObject(_app);
                GC.Collect();
            });
        }
    }
}
