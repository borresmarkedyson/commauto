﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Rater.Tests.Helpers
{
    public class InputValuesHelper
    {
        public string Create() => @"{
                ""Applicant"": {
                    ""Name"": ""Test App"" 
                },
                ""Program"": {
                    ""Id"": 1,
                    ""Name"": ""Box Truck""
                },
                ""Vehicles"": [
                    {
                        ""ID"": 1,
                        ""VIN"": ""AAA"",
                        ""Year"": 2010,
                        ""Limit1"": 100,
                        ""Limit2"": 100
                    },
                    {
                        ""ID"": 2,
                        ""VIN"": ""BBB"",
                        ""Year"": 2015,
                        ""Limit1"": 300,
                        ""Limit2"": 300
                    },
                    {
                        ""ID"": 3,
                        ""VIN"": ""CCC"",
                        ""Year"": 2018,
                        ""Limit1"": 400,
                        ""Limit2"": 400
                    }
                ],
                ""TestVal"": 2
            }";

    }
}
