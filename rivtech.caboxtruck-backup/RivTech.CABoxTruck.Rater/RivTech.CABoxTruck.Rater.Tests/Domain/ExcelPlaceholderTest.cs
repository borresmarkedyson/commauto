﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RivTech.CABoxTruck.Rater.App.Domain;

namespace RivTech.CABoxTruck.Rater.Tests.Domain.ExcelRating
{
    [TestClass]
    public class ExcelPlaceholderTest
    {

        [TestMethod]
        public void Create_Test()
        {
            // Sample valid placeholders
            Assert.IsTrue(new ExcelPlaceholder("__a__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__a1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__aa__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__aB__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__a.a__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__a1.a__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__a.a1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A.a__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A1.a__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A.a1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A.A__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A1.A__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__A1.A1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab.ab__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab1.ab__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab.ab1__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab.Ab__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab.aB__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Ab.aB__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Aa_0_.Bb__").IsValid()); 
            Assert.IsTrue(new ExcelPlaceholder("__a.b.c__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Aa.Bb.Cc__").IsValid());
            Assert.IsTrue(new ExcelPlaceholder("__Aa_11_.Bb.Cc__").IsValid());

            // Sample invalid placeholders.
            Assert.IsFalse(new ExcelPlaceholder("Ab").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Ab").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("Ab__").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Ab.").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Ab_0_.").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Ab_0__1_.").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Ab_0_.b").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("A__Ab_0_.b__").IsValid());
            Assert.IsFalse(new ExcelPlaceholder("__Aa_0__1_.Bb__").IsValid()); 
            Assert.IsFalse(new ExcelPlaceholder("__Aa_01_.Bb__").IsValid()); 
        }

        [TestMethod]
        public void UpdateIndex_Test()
        {
            var placeholder = new ExcelPlaceholder("__Abc_0_.Abc__");

            ExcelPlaceholder result = placeholder.UpdateIndex(1);

            Assert.AreNotEqual("__Abc_0_.Abc__", $"{result}");
            Assert.AreEqual("__Abc_1_.Abc__", $"{result}");

            placeholder = new ExcelPlaceholder("__Abc_10_.Abc__");
            result = placeholder.UpdateIndex(11);
            Assert.AreEqual("__Abc_11_.Abc__", $"{result}");
        }

        [TestMethod]
        public void Index_Test()
        {
            var placeholder = new ExcelPlaceholder("__Abc_0_.Abc__");
            int? index = placeholder.Index;
            Assert.IsNotNull(index);
            Assert.AreEqual(0, index);

            placeholder = new ExcelPlaceholder("__Abc_10_.Abc__");
            index = placeholder.Index;
            Assert.IsNotNull(index);
            Assert.AreEqual(10, index);

            placeholder = new ExcelPlaceholder("__Abc.Abc__");
            index = placeholder.Index;
            Assert.IsNull(index);
        }

    }
}
