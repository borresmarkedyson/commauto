﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound;
using RivTech.CABoxTruck.Rater.App.Domain.Ports.Outbound.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating.Pooling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Tests.Domain.ExcelRating.PremiumRating.Pooling
{
    [TestClass]
    public class PremiumRaterPoolTest
    {
        PremiumRaterPool _sut;
        Mock<IPremiumRaterFactory> _mockPremiumRaterFactory;
        Mock<IPremiumRaterDumpRepository> _mockPremiumRaterDumpRepository;
        Mock<ILogger<PremiumRaterPool>> _mockLogger;

        [TestInitialize]
        public void Init()
        {
            _mockLogger = new Mock<ILogger<PremiumRaterPool>>();
            _mockPremiumRaterFactory = new Mock<IPremiumRaterFactory>();
            _mockPremiumRaterDumpRepository = new Mock<IPremiumRaterDumpRepository>();

            _mockLogger.Setup(x => x.Log(
                    It.IsAny<LogLevel>(),
                    It.IsAny<EventId>(),
                    It.IsAny<It.IsAnyType>(),
                    It.IsAny<Exception>(),
                    (Func<It.IsAnyType, Exception, string>)It.IsAny<object>())
                ).Callback(new InvocationAction(invocation =>
                {
                    var logLevel = (LogLevel)invocation.Arguments[0]; // The first two will always be whatever is specified in the setup above
                    var eventId = (EventId)invocation.Arguments[1];  // so I'm not sure you would ever want to actually use them
                    var state = invocation.Arguments[2];
                    var exception = (Exception?)invocation.Arguments[3];
                    var formatter = invocation.Arguments[4];

                    var invokeMethod = formatter.GetType().GetMethod("Invoke");
                    var logMessage = (string?)invokeMethod?.Invoke(formatter, new[] { state, exception });
                    Console.WriteLine(logMessage);
                }));
            _sut = new PremiumRaterPool(
                PremiumRaterPoolCollection.INSTANCE,
                _mockLogger.Object,
                _mockPremiumRaterFactory.Object,
                new RaterSettings()
                {

                },
                new Mock<IServiceScopeFactory>().Object
            );
        }

        [TestMethod]
        public async Task Preload_ShouldDoNothing_WhenAlreadyInitializing()
        {
            var id = "1";
            var mockPoolItem = new Mock<IPremiumRater>();
            mockPoolItem.Setup(x => x.SubmissionNumber).Returns(id);
            mockPoolItem.Setup(x => x.Status).Returns(PremiumRaterStatus.Initializing);
            mockPoolItem.Setup(x => x.PreloadAsync()).Returns(Task.Run(() =>
            {
                Console.WriteLine("Loading engine for 10s...");
                Thread.Sleep(3000);
                mockPoolItem.Setup(x => x.Status).Returns(PremiumRaterStatus.Ready);
            }));

            _mockPremiumRaterFactory.Setup(x => x.CreateAsync(It.IsAny<string>(), It.IsAny<DateTime>()))
                .ReturnsAsync(mockPoolItem.Object);

            _ = _sut.PreloadRaterAsync("1", It.IsAny<DateTime>()); // Do not wait to simulate as running background.

            // Rater status must be initializing.
            Assert.AreEqual(PremiumRaterStatus.Initializing, mockPoolItem.Object.Status);

            await _sut.PreloadRaterAsync("1", It.IsAny<DateTime>()); // Do not wait to simulate as running background.

            Thread.Sleep(3100);
            // Rater status must be ready after 3 secs of loading time.

            var item = _sut.GetItemReadyForRating(id);
            Assert.AreEqual(PremiumRaterStatus.Ready, mockPoolItem.Object.Status);
        }

        [TestMethod]
        public async Task WhenRatersAreBeingUnloaded_PreloadRatersAsync_ShouldBeAbleToPreloadNewOnes()
        {
            // Arrange
            var id =  "1";
            var mockPoolItem = new Mock<IPremiumRater>();
            mockPoolItem.Setup(x => x.SubmissionNumber).Returns(id);
            mockPoolItem.Setup(x => x.Status).Returns(PremiumRaterStatus.Initializing);
            mockPoolItem.Setup(x => x.PreloadAsync()).Returns(Task.Run(() =>
            {
                Console.WriteLine("Loading engine for 10s...");
                Thread.Sleep(3000); // Load for 3secs
                mockPoolItem.Setup(x => x.Status).Returns(PremiumRaterStatus.Ready);
            }));
            mockPoolItem.Setup(x => x.PreloadAsync()).Returns(Task.Run(() => {
                _sut.OnPreloaded(mockPoolItem.Object);
            }));
            mockPoolItem.Setup(x => x.UnloadAsync()).Returns(Task.Run(() =>
            {
                Console.WriteLine("Unloading rater...");
                Thread.Sleep(5000); // Unload for 8secs
                mockPoolItem.Setup(x => x.Status).Returns(PremiumRaterStatus.Unloaded);
            }));

            _mockPremiumRaterFactory.Setup(x => x.CreateAsync(It.IsAny<string>(), It.IsAny<DateTime>()))
                .ReturnsAsync(mockPoolItem.Object);

            await _sut.PreloadRaterAsync("1", It.IsAny<DateTime>()); // Do not wait to simulate as running background.

            Assert.AreEqual(1, _sut.PoolItems.Count(x => x.SubmissionNumber.Equals("1")));

            // Act: Preload new raters while unloading the previous ones.
            _ = _sut.UnloadRatersBySubmissionAsync("1");
            Assert.AreEqual(1, _sut.PoolItems.Count(x => x.SubmissionNumber.Equals("1")));
            _ = _sut.PreloadRaterAsync("1", It.IsAny<DateTime>()); // Do not wait to simulate as running background.

            // Assert
            Assert.AreEqual(1, _sut.PoolItems.Count(x => x.SubmissionNumber.Equals("1") && x.Status.Equals(PremiumRaterStatus.Initializing)));
        }
    }
}
