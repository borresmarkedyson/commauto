﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RivTech.CABoxTruck.Rater.App.Domain;
using RivTech.CABoxTruck.Rater.App.Domain.PremiumRating;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Rater.App.Tests.Domain.ExcelRating.PremiumRating
{
    [TestClass]
    public class PremiumRaterTest 
    {
        private Mock<IPremiumRaterEngine> _mockEngine; 

        [TestInitialize]
        public void Init()
        {
            _mockEngine = new Mock<IPremiumRaterEngine>();
        }

        [TestMethod]
        public void GetHashCode_MustMatch_WhenSameSubmissionNumber()
        {
            var premiumRater1 = new PremiumRater("submission1", DateTime.Parse("2/1/2021"), new FileContent(new byte[0], ".xlsb"), _mockEngine.Object);
            var premiumRater2 = new PremiumRater("submission1", DateTime.Parse("2/1/2021"), new FileContent(new byte[0], ".xlsb"), _mockEngine.Object);
            Assert.AreEqual(premiumRater1.GetHashCode(), premiumRater2.GetHashCode());

            var premiumRater3 = new PremiumRater("submission2", DateTime.Parse("2/1/2021"), new FileContent(new byte[0], ".xlsb"), _mockEngine.Object);
            Assert.AreNotEqual(premiumRater1.GetHashCode(), premiumRater3.GetHashCode());
            Assert.AreNotEqual(premiumRater2.GetHashCode(), premiumRater3.GetHashCode());
        }

        [TestMethod]
        public async Task IsIdleForAnHour_MustBeFalse_WhenNot()
        {
            var engine = new Mock<IPremiumRaterEngine>().Object;
            var rater = new PremiumRater("123", DateTime.Now, new FileContent(new byte[0], ".xlsb"), engine);
            Assert.IsFalse(rater.IsIdle(5));
            await rater.PreloadAsync();
            Assert.IsFalse(rater.IsIdle(5));
        }

        [TestMethod]
        public async Task Unload_ShouldPass_WhenStatusInitializing()
        {
            var mockEngine = new Mock<IPremiumRaterEngine>();
            mockEngine.Setup(x => x.InitializeAsync())
                .Callback(() => Thread.Sleep(10000));
            var premiumRater = new PremiumRater("submission1", 
                DateTime.Parse("2/1/2021"), 
                new FileContent(new byte[0], ".xlsb"), 
                mockEngine.Object);
            await premiumRater.PreloadAsync();

            premiumRater.UnloadAsync();

            Assert.AreEqual(PremiumRaterStatus.Unloaded, premiumRater.Status);
        }
    }
}
