﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RivTech.CABoxTruck.Rater.App.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace RivTech.CABoxTruck.Rater.App.Tests.Domain
{
    [TestClass]
    public class InputValuesTest
    {


        [TestMethod]
        public void MyTestMethod()
        {
            var jsonInput = $@"
            {{
               ""Policy"": {{
                    ""InceptionDate"": ""{DateTime.Parse("1/1/2021"):o}"",
                    ""OrganizationType"": ""Sole Properietorship"" 
                }},
                ""Vehicle"": [
                    {{
                        ""VIN"": ""AAA"",
                        ""IncludeInRating"": ""Yes"",
                        ""VehicleYear"": ""1/1/2021""
                    }},
                    {{
                        ""VIN"": ""BBB"",
                        ""IncludeInRating"": ""Yes"",
                        ""VehicleYear"": ""1/1/2021""
                    }}
                ]
            }}
            ";
            var deserialized = JsonConvert.DeserializeObject<JObject>(jsonInput);
            var inputValues = new InputValues(deserialized);

            var inceptionDate = inputValues.ParseValue("Policy.InceptionDate");
            Assert.AreEqual(DateTime.Parse("1/1/2021"), inceptionDate);

            var vehicle1VIN = inputValues.ParseValue("Vehicle_0_.VIN");
            Assert.AreEqual(vehicle1VIN, "AAA");
        }

        [TestMethod]
        public void MustRunUnder250ms()
        {
            var jsonInput = $@"
            {{
                ""Broker"": {{
                    ""EffectiveDate"": ""{DateTime.Now:o}"",
                    ""Commission"": 15
                }},
               ""Applicant"": {{
                    ""BusinessDetails"": {{
                        ""UseClass"": ""Courier"",
                        ""AccountCategory"": ""Funeral Services"",
                        ""YearsInBusiness"": 10,
                        ""BusinessType"": ""Sole Properietorship"",
                        ""ZipCode"": ""10001""
                    }}
                }},
                ""Driver"": {{
                    ""UseDriverRatingTab"": ""No"",
                    ""UseAccidentsOrViolationInfo"": ""No"",
                    ""KOUnder25"": ""No"",
                    ""KOAFOrMovingViolations"": ""No"",
                    ""DriverList"": [
                        {{
                            ""DriverID"": ""QQQQ"",
                            ""DateOfBirth"": ""QQQQ"",
                            ""Age"": ""QQQQ"",
                            ""OutOfState"": ""QQQQ"",
                            ""YrsDrivingExperience"": ""QQQQ"",
                            ""YrsCommercialDriverExperience"": ""QQQQ"",
                            ""Majors"": ""QQQQ"",
                            ""Drug"": ""QQQQ"",
                            ""Minors"": ""QQQQ"",
                            ""AtFault"": ""QQQQ"",
                            ""MajorLicenseIssue"": ""QQQQ"",
                            ""NAF"": ""QQQQ"",
                            ""Equipment"": ""QQQQ"",
                            ""Speed"": ""QQQQ"",
                            ""Other"": ""QQQQ""
                        }},
                        {{
                            ""DriverID"": ""WWWW"",
                            ""DateOfBirth"": ""WWWW"",
                            ""Age"": ""WWWW"",
                            ""OutOfState"": ""WWWW"",
                            ""YrsDrivingExperience"": ""WWWW"",
                            ""YrsCommercialDriverExperience"": ""WWWW"",
                            ""Majors"": ""WWWW"",
                            ""Drug"": ""WWWW"",
                            ""Minors"": ""WWWW"",
                            ""AtFault"": ""WWWW"",
                            ""MajorLicenseIssue"": ""WWWW"",
                            ""NAF"": ""WWWW"",
                            ""Equipment"": ""WWWW"",
                            ""Speed"": ""WWWW"",
                            ""Other"": ""WWWW""
                        }},
                        {{
                            ""DriverID"": ""EEEE"",
                            ""DateOfBirth"": ""EEEE"",
                            ""Age"": ""EEEE"",
                            ""OutOfState"": ""EEEE"",
                            ""YrsDrivingExperience"": ""EEEE"",
                            ""YrsCommercialDriverExperience"": ""EEEE"",
                            ""Majors"": ""EEEE"",
                            ""Drug"": ""EEEE"",
                            ""Minors"": ""EEEE"",
                            ""AtFault"": ""EEEE"",
                            ""MajorLicenseIssue"": ""EEEE"",
                            ""NAF"": ""EEEE"",
                            ""Equipment"": ""EEEE"",
                            ""Speed"": ""EEEE"",
                            ""Other"": ""EEEE""
                        }}
                    ]
                }},
                ""Vehicle"": {{
                    ""TotalVehicles"": 2,
                    ""VehicleList"":[
                        {{
                            ""VIN"": ""AAA"",
                            ""UseInPDRating"": ""Yes"",
                            ""Year"": 2015,
                            ""VehicleType"": ""Trailer"",
                            ""Description"": ""Tractor"",
                            ""CompDeductible"": ""1,500 DED"",
                            ""CollDeductible"": ""250 DED"",
                            ""CollStateAmount"": 10000,
                            ""IncCargoLimit"": ""Yes"",
                            ""IncRefCargoLimit"": ""Yes"",
                            ""UseClass"": ""Logging"",
                            ""BusinessClass"": ""Auto Dealers"",
                        }},
                        {{
                            ""VIN"": ""BBB"",
                            ""UseInPDRating"": ""Yes"",
                            ""Year"": 2018,
                            ""VehicleType"": ""Tow Truck - one axle"",
                            ""Description"": ""Pickup<= 1/2 Ton 4x4"",
                            ""CompDeductible"": ""1,500 DED"",
                            ""CollDeductible"": ""250 DED"",
                            ""CollStateAmount"": 5000,
                            ""IncCargoLimit"": ""No"",
                            ""IncRefCargoLimit"": ""No"",
                            ""UseClass"": ""Child Care"",
                            ""BusinessClass"": ""Building Materials & Hardware Stores"",
                        }}
                    ]
                }},
                ""RequestedCoverages"": {{
                    ""AutoLiability"": {{
                        ""AutoBodilyInjury"": 1001,
                        ""AutoPropertyDamage"": 1002,
                        ""AutoLiabilityDeductible"": 1003,
                        ""MedicalPaymentsLimit"": 1004,
                        ""PIPLimit"": 1005,
                        ""UMSCLLimit"": 1006,
                        ""UMPDLimit"": 1007,
                        ""UIMSCLLimit"": 1008,
                        ""UIMPDLimit"": 1009,
                        ""UMStacking"": ""N"",
                        ""SymbolsRequested"": ""4,5,6""
                    }},
                    ""PhysicalDamage"": {{
                        ""FireTheftSpecPerils"": 1012,
                        ""COMPDeductible"": 1013,
                        ""COLLDeductible"": 1014
                    }},
                    ""OtherCoverages"": {{
                        ""GLBodilyInjuryLimits"": 1015,
                        ""CargoCollLimit"": 1016,
                        ""RefrigeratedCargoLimits"": 1017
                    }}
                }},
                ""RiskSpecifics"": {{
                    ""DriverInfo"": {{
                        ""TotalDrivers"": 18,
                        ""AnyVehiclesOperatedForPersonalUse"": ""Y"",
                        ""AnnualCostOfHire"": 1018
                    }},
                    ""GeneralLiability"": {{
                        ""IsOperationAtStorageLot"": ""Y"",
                        ""Payroll"": 1019
                    }},
                    ""SafetyDevices"": {{
                        ""SafetyDeviceCategory"": {{
                            ""Cameras"": {{
                                ""CameraDiscount"": ""N""
                            }},
                            ""BrakeWarningSystem"": ""Y"",
                            ""SpeedWarningSystem"": ""Y"",
                            ""MonitoringViaTelematicsDevice"": ""Y"",
                            ""AnyActiveAccidentAvoidanceTechnology"": ""Y"",
                            ""AnyPassiveAccidentAvoidanceTechnology"": ""Y"",
                            ""DistractedDrivingEquipmentDiscount"": ""Y"",
                            ""MileageTrackingEquipment"": ""Y"",
                            ""NavigationOrGPSDataAvailable"": ""Y""
                        }}
                    }},
                    ""DriverHiringCriteria"": {{
                        ""BackgroundCheckIncluded"": {{
                            ""WrittenApplication"": ""No"",
                            ""RoadTest"": ""No"",
                            ""WrittenTest"": ""No"",
                            ""FullMedical"": ""No"",
                            ""DrugTesting"": ""No"",
                            ""CurrentMVR"": ""No"",
                            ""ReferenceChecks"": ""No"",
                            ""CriminalBackgroundCheck"": ""No""
                        }},
                        ""ReportAllDriversToRivington"": ""No"",
                        ""IsDisciplinaryPlanDocumentedForAllDrivers"": ""No"",
                        ""AllDriversProperlyLicensedAndDOTCompliant"": ""No"",
                        ""AllDriversDrivingSimilarVehicleCommerciallyFor2YrsPlus"": ""No"",
                        ""AllDriversHaveAtLeast5YrsUSDrivingExperience"": ""No""
                    }},
                    ""MaintenanceQuestions"": {{
                        ""VehicleMaintenanceProgram"": {{
                            ""ServiceRecordForEachVehicle"": ""Yes"",
                            ""ControlledAndFrequentInspections"": ""Yes"",
                            ""VehicleDailyConditionReports"": ""Yes""
                        }},
                        ""WrittenMaintenanceProgram"": ""Yes"",
                        ""WrittenDriverTrainingProgram"": ""Yes"",
                        ""WrittenSafetyProgram"": ""Yes"",
                        ""WrittenAccidentReportingProcedures"": ""Yes"",
                        ""IsMaintenanceProgramManagedByCompany"": ""Yes"",
                        ""DoYouProvideCompleteMaintenanceOnAllVehicles"": ""Yes"",
                        ""DriverFilesAvailableForReview"": ""Yes"",
                        ""AccidentFilesAvailableForReview"": ""Yes""
                    }},
                    ""DOTMiscellaneous"": {{
                        ""PolicyLevelAccicdents"": 11
                    }},
                    ""DestinationInformation"": {{
                        ""OwnerOperator"": 25,
                        ""Radius0To50Miles"": 25 ,
                        ""Radius50to200Miles"": 25,
                        ""RadiusAbove200Miles"": 50
                    }},
                    ""DOTInformation"": {{
                        ""RiskRequiresDOTNumber"": ""Yes"",
                        ""RiskHasDOTNumber"": ""1235489870"",
                        ""RiskHasInterstateAuthority"": ""No"",
                        ""RiskPlansToStayIntrastate"": ""No"",
                        ""DOTRegistrationDate"": ""2021-02-03"",
                        ""DOTCurrentlyActive"": ""No"",
                        ""ChameleonIssues"": ""Yes"",
                        ""CurrentSaferRating"": ""In Accident"",
                        ""ISSCAB"": ""Yes Info"",
                        ""BasicAlert"": {{
                            ""UnsafeDrivingBasicAlertOrWorse"": ""No"",
                            ""HoursOfServiceBasicAlertOrWorse"": ""No"",
                            ""DriverFitnessBasicAlertOrWorse"": ""No"",
                            ""ControlledSubstanceBasicAlertOrWorse"": ""No"",
                            ""VehicleMaintenanceBasicAlertOrWorse"": ""No"",
                            ""CrashBasicAlertOrWorse"": ""No""
                        }}
                    }}
                }},
                ""HistoricalCoverage"": {{
                    ""HistoricalInsuranceCoverage"": {{
                        ""NumberOfVehicles"": 19
                    }},
                    ""ProjectionsAndHistoricalFigures"": {{
                        ""GrossFleetMileage"": 20
                    }}
                }},
                ""AdditionalInterest"": {{
                    ""NoOfAdditionalInsured"": 1020,
                    ""WaiverOfSubrogation"": 1021,
                    ""PrimaryNonContributoryCoverage"": 1022 
                }}
            }}
            ";

            var deserialized = JsonConvert.DeserializeObject<JObject>(jsonInput);
            var inputValues = new InputValues(deserialized);

            var sw = new Stopwatch();
            sw.Restart();
            var result = inputValues.ParseValue("RiskSpecifics.SafetyDevices.SafetyDeviceCategory.Cameras.CameraDiscount");
            sw.Stop();
            Assert.AreEqual("N", result?.ToString());
            Assert.IsTrue(sw.ElapsedMilliseconds < 250);
        }

        [TestMethod]
        public void Parse_Test()
        {
            var jsonInput = $@"
            {{
                ""Vehicle"": [
                    {{ ""VIN"": ""AAA"" }}, 
                    {{ ""VIN"": ""BBB"" }}, 
                    {{ ""VIN"": ""CCC"" }}, 
                    {{ ""VIN"": ""DDD"" }}, 
                    {{ ""VIN"": ""EEE"" }}, 
                    {{ ""VIN"": ""FFF"" }}, 
                    {{ ""VIN"": ""GGG"" }}, 
                    {{ ""VIN"": ""HHH"" }}, 
                    {{ ""VIN"": ""JJJ"" }}, 
                    {{ ""VIN"": ""KKK"" }}, 
                    {{ ""VIN"": ""LLL"" }}, 
                    {{ ""VIN"": ""MMM"" }} 
                ]
            }}
            ";
            var deserialized = JsonConvert.DeserializeObject<JObject>(jsonInput);
            var inputValues = new InputValues(deserialized);

            Assert.AreEqual("AAA", inputValues.ParseValue("Vehicle_0_.VIN"));
            Assert.AreEqual("MMM", inputValues.ParseValue("Vehicle_11_.VIN"));
        }
    }
}
