﻿using DotLiquid;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using DotLiquid.NamingConventions;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Model.ContentTemplateModel;
using System.Text;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using RivTech.CABoxTruck.AZFunction.HtmlContentTemplates;

namespace RivTech.CABoxTruck.AZFunction.Common.ContentGenerator
{
    public class ContentGenerator : IContentGenerator
    {
        private readonly IContentTemplateHandler _contentTemplateHandler;
        private readonly IMapper _mapper;

        public ContentGenerator(IContentTemplateHandler contentTemplateHandler, IMapper mapper)
        {
            _contentTemplateHandler = contentTemplateHandler;
            _mapper = mapper;
        }

        public Stream GenerateContent(ContentGeneratorRequest contentGeneratorRequest, Stream contentStream, ILogger iLogger)
        {
            try
            {
                iLogger.LogInformation("Content generator template.");

                iLogger.LogInformation(JsonConvert.SerializeObject(contentGeneratorRequest));

                if (!string.IsNullOrEmpty(contentGeneratorRequest.TemplateName))
                {
                    string transformedContent = string.Empty;
                    string contentTemplate = string.Empty;

                    // Only request the template if the key has been given
                    if (!string.IsNullOrEmpty(contentGeneratorRequest.TemplateName))
                    {
                        contentTemplate = _contentTemplateHandler.GetContentTemplate(contentGeneratorRequest.TemplateName);
                    }

                    switch ((contentGeneratorRequest.PayloadType).ToLower())
                    {
                        case "payment confirmation":
                            PaymentConfirmationTemplate paymentConfirmationTemplate = JsonConvert.DeserializeObject<PaymentConfirmationTemplate>(contentGeneratorRequest.Payload);
                            paymentConfirmationTemplate.CurrencyLanguage = "en-us";

                            transformedContent = ContentTransformation(contentTemplate, Hash.FromAnonymousObject(paymentConfirmationTemplate));
                            break;
                        case "accounts receivable report email":
                            transformedContent = contentTemplate;
                            break;
                        case "accounts receivable report csv":
                            JsonSerializerSettings settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore,
                                FloatParseHandling = FloatParseHandling.Decimal
                            };
                            List<AccountsReceivableDTO> accountsReceivableDTO = JsonConvert.DeserializeObject<List<AccountsReceivableDTO>>(contentGeneratorRequest.Payload, settings);
                            var accountsReceivables = _mapper.Map<List<AccountsReceivableReportTemplate>>(accountsReceivableDTO);
                            transformedContent = JsonConvert.SerializeObject(accountsReceivables);
                            break;
                        case "agent invoice":
                            iLogger.LogInformation("Payload type: agent invoice");
                            AgentInvoiceTemplate agentInvoiceTemplate = JsonConvert.DeserializeObject<AgentInvoiceTemplate>(contentGeneratorRequest.Payload);
                            iLogger.LogInformation(JsonConvert.SerializeObject(agentInvoiceTemplate));
                            transformedContent = ContentTransformation(contentTemplate, Hash.FromAnonymousObject(agentInvoiceTemplate));
                            iLogger.LogInformation(transformedContent);
                            break;
                    }

                    switch ((contentGeneratorRequest.TemplateType).ToLower())
                    {
                        case "email":
                            contentStream = GenerateStreamFromString(transformedContent);
                            break;
                        case "csv":
                            contentStream = ConvertTemplateContentToCSV(transformedContent);
                            break;
                    }

                    //contentStream.Dispose();
                }
                else
                {
                    iLogger.LogError("Payload is empty.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return contentStream;
        }

        private string ContentTransformation(string templateContent, Hash hashedObject)
        {
            string transformedContent = string.Empty;

            Liquid.UseRubyDateFormat = true;

            // Register filter to use in liquid template
            Template.RegisterFilter(typeof(LiquidIntegerFilter));
            Template.RegisterFilter(typeof(LiquidDateFilter));
            Template.NamingConvention = new RubyNamingConvention();

            Context context = new Context(CultureInfo.CurrentCulture);
            context.Environments.Add(hashedObject);
            RenderParameters renderParameters = RenderParameters.FromContext(context, context.FormatProvider);
            renderParameters.ErrorsOutputMode = ErrorsOutputMode.Rethrow;

            transformedContent = Template.Parse(templateContent).Render(renderParameters);

            foreach (Exception exception in renderParameters.Context.Errors)
            {
                throw exception;
            }

            return transformedContent;
        }

        private Stream GenerateStreamFromString(string content)
        {
            MemoryStream memoryStream = new MemoryStream();

            StreamWriter streamWriter = new StreamWriter(memoryStream);
            streamWriter.WriteLine(content);
            streamWriter.Flush();

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        private Stream ConvertTemplateContentToCSV(string templateContent)
        {
            StringBuilder stringBuilder = new StringBuilder();

            using (DataTable dataTable = JsonConvert.DeserializeObject<DataTable>(templateContent))
            {
                stringBuilder.AppendLine(string.Join(",", dataTable.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList()));

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (i == dataTable.Rows.Count - 1)
                    {
                        stringBuilder.Append(string.Join(",", dataTable.Rows[i].ItemArray));
                    }
                    else
                    {
                        stringBuilder.AppendLine(string.Join(",", dataTable.Rows[i].ItemArray));
                    }
                }
            }

            string content = stringBuilder.ToString().Trim(new Char[] { '{', '}' });

            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(content));

            return memoryStream;
        }
    }
}
