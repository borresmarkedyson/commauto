﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Common.ContentGenerator
{
    public static class LiquidDateFilter
    {
        public static int DateTimeAsSeconds(string value)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);//from 1970/1/1 00:00:00 to now

            DateTime dateValue = DateTime.Parse(value);

            TimeSpan result = dateValue.Subtract(dateTime);

            int seconds = Convert.ToInt32(result.TotalSeconds);

            return seconds;
        }

        public static string SecondsAsDateTime(int totalSeconds)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            DateTime dateTimeResult = dateTime.AddSeconds(totalSeconds);

            string result = dateTimeResult.ToString("s");

            return result;
        }
    }
}