﻿
namespace RivTech.CABoxTruck.AZFunction.Common.ContentGenerator
{
    public static class LiquidIntegerFilter
    {
        public static int AsInteger(string value)
        {
            return int.Parse(value);
        }
    }
}