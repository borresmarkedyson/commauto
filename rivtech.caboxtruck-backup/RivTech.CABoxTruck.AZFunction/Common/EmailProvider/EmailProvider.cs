﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using SendGrid;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using RivTech.CABoxTruck.AZFunction.Services;
using RestSharp;
using System.Threading.Tasks;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Common.EmailProvider
{
    public class EmailProvider : IEmailProvider
    {
        private readonly string _sendgridApiKey;
        private int _retrySendingEmail = 5;
        private int _sleepPeriod = 1;

        public EmailProvider()
        {
            _sendgridApiKey = Environment.GetEnvironmentVariable(BoxtruckConfiguration.SendGridApiKey) ?? "";
        }

        public bool SendEmail(EmailParameters emailParameters, EmailQueueDTO emailQueueDTO = null)
        {
            bool processEmail = true;
            bool sendSuccessful = false;
            int retryCount = 1;
            string[] toAddresses = !string.IsNullOrEmpty(emailParameters.Address) ? emailParameters.Address.Split(';') : new string[] { };
            string[] bccAddresses = !string.IsNullOrEmpty(emailParameters.BccAddress) ? emailParameters.BccAddress.Split(';') : new string[] { };
            string[] CcAddresses = !string.IsNullOrEmpty(emailParameters.CcAddress) ? emailParameters.CcAddress.Split(';') : new string[] { };
            SendGrid.SendGridClient sendgridClient = null;
            EmailQueueDTO existingEmailQueueDTO = null;

            try
            {
                if (emailQueueDTO != null && emailQueueDTO.Id != Guid.Empty)
                {
                    RestSharp.IRestResponse getEmailQueueResponse = BoxTruckService.GetEmailQueue(emailQueueDTO.Id).Result;
                    if (getEmailQueueResponse.IsSuccessful)
                    {
                        existingEmailQueueDTO = JsonTool.DeserializeObject<EmailQueueDTO>(getEmailQueueResponse.Content);
                    }
                }

                if (existingEmailQueueDTO == null)
                {
                    var newEmailQueueDTO = new EmailQueueDTO() { 
                        Address = emailParameters.Address,
                        BccAddress = emailParameters.BccAddress,
                        CcAddress = emailParameters.CcAddress,
                        FromAddress = emailParameters.FromAddress,
                        Subject = emailParameters.Subject,
                        Content = emailParameters.Content,
                        AttachmentFileName = emailParameters.AttachmentFileName,
                        AttachmentUrl = emailParameters.AttachmentUrl,
                        CreatedDate = emailParameters.CreatedDate,
                        UpdatedDate = emailParameters.UpdatedDate,
                        Type = emailParameters.Type
                    };
                    if (emailParameters.Attachment != null && emailParameters.Attachment.Length > 0)
                    {
                        newEmailQueueDTO.Attachment = emailParameters.Attachment.ToArray();
                    }

                    var insertEmailQueueResponse = BoxTruckService.InsertEmailQueue(newEmailQueueDTO).Result;
                    if (insertEmailQueueResponse.IsSuccessful)
                    {
                        existingEmailQueueDTO = JsonTool.DeserializeObject<EmailQueueDTO>(insertEmailQueueResponse.Content);
                    }
                }

                SendGrid.Helpers.Mail.EmailAddress sendgridFromAddress = new SendGrid.Helpers.Mail.EmailAddress(emailParameters.FromAddress, Environment.GetEnvironmentVariable(BoxtruckConfiguration.SenderEmail));
                SendGrid.Helpers.Mail.SendGridMessage sendgridMessage = new SendGrid.Helpers.Mail.SendGridMessage()
                {
                    From = sendgridFromAddress,
                    Subject = emailParameters.Subject,
                    PlainTextContent = emailParameters.Content.TrimStart('"').TrimEnd('"'),
                    HtmlContent = emailParameters.Content.TrimStart('"').TrimEnd('"')
                };

                foreach (string toAddress in toAddresses)
                {
                    sendgridMessage.AddTo(toAddress, "");
                }

                foreach (string bccAddress in bccAddresses)
                {
                    if (!string.IsNullOrEmpty(bccAddress) && !toAddresses.Contains(bccAddress))
                    {
                        sendgridMessage.AddBcc(bccAddress, "");
                    }
                }

                foreach (string CcAddress in CcAddresses)
                {
                    if (!string.IsNullOrEmpty(CcAddress) && !toAddresses.Contains(CcAddress))
                    {
                        sendgridMessage.AddCc(CcAddress, "");
                    }
                }

                if (emailParameters.Attachment != null && emailParameters.AttachmentFileName != null)
                {
                    emailParameters.Attachment.Seek(0, SeekOrigin.Begin);
                    SendGrid.Helpers.Mail.Attachment sendgridAttachment = new SendGrid.Helpers.Mail.Attachment()
                    {
                        Content = Convert.ToBase64String(emailParameters.Attachment.ToArray()),
                        Type = Path.GetExtension(emailParameters.AttachmentFileName) == ".pdf" ? "application/pdf" : "application/octet-stream",
                        Filename = emailParameters.AttachmentFileName
                    };

                    sendgridMessage.AddAttachments(new List<SendGrid.Helpers.Mail.Attachment> { sendgridAttachment });
                }

                // We want to retry sending email with SendGrid Api for at least 5 times
                // before we log any error return back from SendGrid
                while (processEmail && retryCount <= _retrySendingEmail)
                {
                    sendgridClient = new SendGrid.SendGridClient(_sendgridApiKey);

                    System.Threading.Tasks.Task<SendGrid.Response> resp = sendgridClient.SendEmailAsync(sendgridMessage);
                    resp.Wait();

                    if (resp.Exception != null)
                    {
                        if (retryCount == _retrySendingEmail)
                        {
                            throw resp.Exception;
                        }

                        Thread.Sleep(_sleepPeriod);
                    }
                    else
                    {
                        SendGrid.Response result = resp.Result;

                        if (result.StatusCode != System.Net.HttpStatusCode.OK && result.StatusCode != System.Net.HttpStatusCode.Accepted)
                        {
                            if (retryCount == _retrySendingEmail)
                            {
                                throw new Exception("SendGrid error - " + resp.Result.StatusCode.ToString() + ". To Address: " + (string.IsNullOrEmpty(emailParameters.Address) ? "null" : emailParameters.Address) + ", bcc address: " + (string.IsNullOrEmpty(emailParameters.BccAddress) ? "null" : emailParameters.BccAddress)  + ", Cc address: " + (string.IsNullOrEmpty(emailParameters.CcAddress) ? "null" : emailParameters.CcAddress) + ", from address: " + (string.IsNullOrEmpty(emailParameters.FromAddress) ? "null" : emailParameters.FromAddress));
                            }

                            Thread.Sleep(_sleepPeriod);
                        }
                        else
                        {
                            sendSuccessful = true;
                            processEmail = false;

                            if (existingEmailQueueDTO != null && existingEmailQueueDTO.Id != Guid.Empty)
                            {
                                var insertEmailQueueArchiveResponse = BoxTruckService.InsertEmailQueueArchive(emailQueueDTO).Result;
                                var deleteEmailQueueResponse = BoxTruckService.DeleteEmailQueue(emailQueueDTO).Result;
                            }
                        }
                    }

                    retryCount++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error generated from SendEmail function{Environment.NewLine}{ex.Message}", ex);
            }

            return sendSuccessful;
        }

        public static void ReportEmail(ReportDTO reportDTO)
        {
            EmailParameters emailParameters = new EmailParameters()
            {
                Address = Environment.GetEnvironmentVariable("ReceiverEmail") ?? "",
                FromAddress = Environment.GetEnvironmentVariable("SenderEmail") ?? "",
                Content = reportDTO.EmailQueueDTO.Content,
                Subject = reportDTO.EmailQueueDTO.Subject
            };

            var emailProvider = new EmailProvider();
            emailProvider.SendEmail(emailParameters);
        }
    }
}
