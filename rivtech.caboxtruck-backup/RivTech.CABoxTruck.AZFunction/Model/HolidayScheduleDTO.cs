﻿namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class HolidayScheduleDTO
    {
        public int Id { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string HolidayName { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
    }
}