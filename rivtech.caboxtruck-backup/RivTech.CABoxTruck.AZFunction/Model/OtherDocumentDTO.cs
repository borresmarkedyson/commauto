﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class OtherDocumentDTO
    {
        public Guid? Id { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public bool? IsUploaded { get; set; }
        public string Source { get; set; }
        public long? CreatedBy { get; set; }
        public string CreatedByFullName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? CreatedDateProper { get; set; }
        public bool? IsActive { get; set; }
    }
}