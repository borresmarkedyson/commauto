﻿namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class HttpTriggerDTO
    {
        public string JobName { get; set; }
        public string Date { get; set; }
    }
}