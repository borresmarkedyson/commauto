﻿using AutoMapper;
using RivTech.CABoxTruck.AZFunction.Model.ContentTemplateModel;
using System.Globalization;

namespace RivTech.CABoxTruck.AZFunction.Model.DTOMapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AccountsReceivableDTO, AccountsReceivableReportTemplate>()
                .ForMember(d => d.PastDueAmount, opt => opt.MapFrom(src => src.PastDueAmount.HasValue ? "\"$" + src.PastDueAmount.Value.ToString("#,##0.00", CultureInfo.InvariantCulture) + "\"" : string.Empty))
                .ForMember(d => d.CurrentAmountDue, opt => opt.MapFrom(src => src.CurrentAmountDue.HasValue ? "\"$" + src.CurrentAmountDue.Value.ToString("#,##0.00", CultureInfo.InvariantCulture) + "\"" : string.Empty))
                .ForMember(d => d.Inception, opt => opt.MapFrom(src => src.Inception != null ? src.Inception.Value.ToString("MM/dd/yyyy") : string.Empty))
                .ForMember(d => d.PendingCancellation, opt => opt.MapFrom(src => src.PendingCancellation != null ? src.PendingCancellation.Value.ToString("MM/dd/yyyy") :string.Empty))
                .ForMember(d => d.CancellationDate, opt => opt.MapFrom(src => src.CancellationDate != null ? src.CancellationDate.Value.ToString("MM/dd/yyyy") : string.Empty))
                .ForMember(d => d.PastDueDate, opt => opt.MapFrom(src => src.PastDueDate != null ? src.PastDueDate.Value.ToString("MM/dd/yyyy") : string.Empty))
                .ForMember(d => d.CurrentDueDate, opt => opt.MapFrom(src => src.CurrentDueDate != null ? src.CurrentDueDate.Value.ToString("MM/dd/yyyy") : string.Empty));
        }
    }
}
