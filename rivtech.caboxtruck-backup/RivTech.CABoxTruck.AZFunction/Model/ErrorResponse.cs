﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class ErrorResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string ReferenceNumber { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}