﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class AccountsReceivableInputDTO
    {
        public string SortNameBy { get; set; }
        public string ReceivableType { get; set; }
        public string Broker { get; set; }
        public bool PastDueOnly { get; set; }
        public bool NoPaymentsMade { get; set; }
        public DateTime? ValuationDate { get; set; }

    }
}