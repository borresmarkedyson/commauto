﻿namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class ErrorReport
    {
        public string PolicyNumber { get; set; }
        public string Details { get; set; }
    }
}