﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class JobResponseDTO
    {
        public Guid? RiskId { get; set; }
        public string PolicyNumber { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public bool HasError { get; set; }
    }
}