﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class EmailQueueAttachmentDTO
    {
        public Guid? Id { get; set; }
        public Guid? EmailQueueId { get; set; }
        public Guid? KeyId { get; set; }
        public string Source { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsActive { get; set; }

        public OtherDocumentDTO OtherDocument { get; set; }
    }
}