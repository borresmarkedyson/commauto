﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class EmailQueueDTO
    {
        public EmailQueueDTO()
        {
        }

        public EmailQueueDTO(EmailQueueDTO emailQueueDTO)
        {
            Subject = emailQueueDTO.Subject;
            Content = emailQueueDTO.Content;
        }
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string BccAddress { get; set; }
        public string CcAddress { get; set; }
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentUrl { get; set; }
        public byte[] Attachment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Type { get; set; }
        public bool IsSent { get; set; }
    }
}