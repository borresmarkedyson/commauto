﻿using System;
using System.Collections.Generic;
using RivTech.CABoxTruck.AZFunction.Interfaces;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class InvoiceResponseDTO : ITriggerDate
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal CurrentAmountInvoiced { get; set; }
        public decimal TotalAmountDue { get; set; }
        public DateTime CreatedDate { get; set; }
        public ApplicationUserDTO CreatedBy { get; set; }
        public DateTime? VoidDate { get; set; }
        public ApplicationUserDTO VoidedBy { get; set; }
        public List<InvoiceDetailsDTO> InvoiceDetails { get; set; }
        public decimal FutureInstallmentAmount { get; set; }
        public decimal PayoffAmount { get; set; }
        public bool IsRecurringPayment { get; set; }
        public DateTime TriggerDate { get; set; }
        public bool IsUserSelectedDate { get; set; }
        public decimal BrokerCommission { get; set; }
    }
}