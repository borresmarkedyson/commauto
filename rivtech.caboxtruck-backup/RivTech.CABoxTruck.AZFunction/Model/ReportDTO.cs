﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class ReportDTO
    {
        public ReportDTO()
        {
            EmailQueueDTO = new EmailQueueDTO();
            ErrorReport = new List<ErrorReport>();
        }

        public EmailQueueDTO EmailQueueDTO { get; set; }
        public List<ErrorReport> ErrorReport { get; set; }
        public string JobName { get; set; }
    }
}