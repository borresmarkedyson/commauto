﻿namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class ApplicationUserDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}