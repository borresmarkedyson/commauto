﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class TokenIdentityDTO
    {
        public string Token { get; set; }
    }
}
