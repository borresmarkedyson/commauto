﻿using DotLiquid;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class PaymentConfirmationRequest
    {
        public string InsuredName { get; set; }
        public string PolicyNumber { get; set; }
        public string PaymentDate { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public string Underwriter { get; set; }
        public string Email { get; set; }
    }
}
