﻿namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class InvoiceRequestDTO
    {
        public string InvoiceDate { get; set; }
        public string AppId { get; set; }
    }
}