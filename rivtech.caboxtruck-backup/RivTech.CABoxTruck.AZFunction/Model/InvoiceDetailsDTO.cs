﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Model
{
    public class InvoiceDetailsDTO
    {
        public Guid Id { get; set; }
        public AmountSubTypeDTO AmountSubType { get; set; }
        public string AmountSubTypeId { get; set; }
        public decimal InvoicedAmount { get; set; }
    }
}