﻿using DotLiquid;

namespace RivTech.CABoxTruck.AZFunction.Model.ContentTemplateModel
{
    public class AgentInvoiceTemplate : ILiquidizable
    {
        public string BusinessName { get; set; }
        public string PolicyNumber { get; set; }

        public object ToLiquid()
        {
            return Hash.FromAnonymousObject(this);
        }
    }
}
