﻿using DotLiquid;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.Model.ContentTemplateModel
{
    public class AccountsReceivableReportTemplate : ILiquidizable
    {
        [JsonProperty("Broker")]
        public string Broker { get; set; }
        [JsonProperty("Policy Number")]
        public string PolicyNumber { get; set; }
        [JsonProperty("Insured")]
        public string Insured { get; set; }
        [JsonProperty("Insured Phone")]
        public string InsuredPhone { get; set; }
        [JsonProperty("Inception Date")]
        public string Inception { get; set; }
        [JsonProperty("Pending Cancellation")]
        public string PendingCancellation { get; set; }
        [JsonProperty("Cancellation Date")]
        public string CancellationDate { get; set; }
        [JsonProperty("Past Due Date")]
        public string PastDueDate { get; set; }
        [JsonProperty("Past Due Amount")]
        public string PastDueAmount { get; set; }
        [JsonProperty("Due Date")]
        public string CurrentDueDate { get; set; }
        [JsonProperty("Total Amount Due")]
        public string CurrentAmountDue { get; set; }
        [JsonProperty("No Payments")]
        public string NoPayments { get; set; }
        [JsonProperty("Financed")]
        public string Financed { get; set; }

        public object ToLiquid()
        {
            return Hash.FromAnonymousObject(this);
        }
    }
}
