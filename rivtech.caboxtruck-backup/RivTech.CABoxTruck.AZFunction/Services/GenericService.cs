﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Services
{
    public class GenericService
    {
        public async Task<IEnumerable<HolidayScheduleDTO>> GetHolidaySchedule()
        {
            var baseUrl = Environment.GetEnvironmentVariable("GenericServiceApiUrl") ?? "";

            var client = new RestClient(baseUrl);
            var request = new RestRequest
            {
                Resource = "api/HolidaySchedule",
                Method = Method.GET
            };

            request.AddHeader("Content-Type", "application/json");
            var response = await client.ExecuteAsync(request);
            return response.IsSuccessful
                ? JsonTool.DeserializeObject<List<HolidayScheduleDTO>>(response.Content)
                : null;
        }

        public static async Task<IRestResponse> CheckBillingApiConnection()
        {
            var apiUrl = Environment.GetEnvironmentVariable("BillingApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(apiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };
            var boxTruckRequest = new RestRequest
            {
                Resource = "api/BusinessDay?date=2021/10/26",
                RequestFormat = DataFormat.Json,
                Method = Method.GET
            };
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> CheckBoxTruckApiConnection()
        {
            var apiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(apiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };
            var boxTruckRequest = new RestRequest
            {
                Resource = "api/Banner",
                RequestFormat = DataFormat.Json,
                Method = Method.GET
            };
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }
    }
}