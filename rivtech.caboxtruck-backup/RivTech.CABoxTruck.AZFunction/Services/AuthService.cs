﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Services
{
    public class AuthService
    {
        // Need to update credentials per envi
        //private const string TemporaryIdentityCredentials = "{\"username\": \"uwuser@rivtech.com\", \"password\": \"1234p@$$W0Rd!\"}"; // Dev and Test Envi
        private const string TemporaryIdentityCredentials = "{\"username\": \"rivtechadmin\", \"password\": \"$miN@d34!\"}";

        public static string GetToken()
        {
            var apiUrl = Environment.GetEnvironmentVariable("UseridentityApiUrl") ?? "";
            var client = new RestClient(apiUrl);
            var request = new RestRequest
            {
                Resource = "Auth",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", TemporaryIdentityCredentials, ParameterType.RequestBody);

            var response = client.Execute(request);
            var tokenReponse = JsonConvert.DeserializeObject<TokenIdentityDTO>(response.Content);
            return tokenReponse.Token;
        }
    }
}