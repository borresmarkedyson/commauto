﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Services
{
    public class BoxTruckService
    {

        public static async Task<IRestResponse> AutoPolicyCancellation(string invoiceDateRequest)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/RiskBind/AutoPolicyCancellation",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> AutoNoticeCancellation(string invoiceDateRequest)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/RiskBind/AutoNoticeCancellation",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> AutoRescindNoticeCancellation(string invoiceDateRequest)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/RiskBind/AutoRescindNoticeCancellation",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> GetAllEmailQueued(string invoiceDateRequest)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/EmailQueue/GetAllEmailQueued",
                RequestFormat = DataFormat.Json,
                Method = Method.GET
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> InsertEmailQueue(EmailQueueDTO emailQueueDTO)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/EmailQueue/AddEmailQueue",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var emailQueueData = JsonConvert.SerializeObject(emailQueueDTO);

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", DateTime.Now.ToString("yyyy-MM-dd"));
            boxTruckRequest.AddParameter("application/json", emailQueueData, ParameterType.RequestBody);

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> DeleteEmailQueue(EmailQueueDTO emailQueueDTO)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/EmailQueue/DeleteEmailQueue",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var emailQueueData = JsonConvert.SerializeObject(emailQueueDTO);

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", DateTime.Now.ToString("yyyy-MM-dd"));
            boxTruckRequest.AddParameter("application/json", emailQueueData, ParameterType.RequestBody);

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> GetEmailQueue(Guid emailQueueId)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = $"api/EmailQueue/GetEmailQueue/{emailQueueId}",
                RequestFormat = DataFormat.Json,
                Method = Method.GET
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", DateTime.Now.ToString("yyyy-MM-dd"));

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> InsertEmailQueueArchive(EmailQueueDTO emailQueueDTO)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/EmailQueueArchive/AddEmailQueueArchive",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var emailQueueData = JsonConvert.SerializeObject(emailQueueDTO);

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", DateTime.Now.ToString("yyyy-MM-dd"));
            boxTruckRequest.AddParameter("application/json", emailQueueData, ParameterType.RequestBody);

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> GetAccountsReceivableReportData(string invoiceDateRequest, AccountsReceivableInputDTO accountsReceivableInputDTO)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = "api/Reports/SearchAccountsReceivable",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var dataDTO = JsonConvert.SerializeObject(accountsReceivableInputDTO);

            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));
            boxTruckRequest.AddParameter("application/json", dataDTO, ParameterType.RequestBody);

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }

        public static async Task<IRestResponse> ProcessFtpToCovenir(string batchId)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };

            var boxTruckRequest = new RestRequest
            {
                Resource = $"api/FTPClientServer/ftpZipSend?batchId={batchId}",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            boxTruckRequest.AddHeader("Content-Type", "application/json");

            var token = AuthService.GetToken();
            boxTruckRequest.AddHeader("Authorization", string.Concat("Bearer ", token));
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }
    }
}