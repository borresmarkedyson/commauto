﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Services
{
    public class InvoiceService
    {
        public static async Task<IRestResponse> GenerateInvoice(string invoiceDateRequest)
        {
            var billingApiUrl = Environment.GetEnvironmentVariable("BillingApiUrl") ?? "";
            var appId = Environment.GetEnvironmentVariable("AppId");

            var billingClient = new RestClient(billingApiUrl);
            var billingRequest = new RestRequest
            {
                Resource = "api/Invoice",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var invoiceRequest = new InvoiceRequestDTO
            {
                AppId = appId
            };

            var billingSerializePayload = JsonConvert.SerializeObject(invoiceRequest);
            billingRequest.AddHeader("Content-Type", "application/json");
            billingRequest.AddParameter("application/json", billingSerializePayload, ParameterType.RequestBody);
            billingRequest.AddHeader("userdate", invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd"));
            var token = AuthService.GetToken();
            billingRequest.AddHeader("Authorization", string.Concat("Bearer ", token));

            return await billingClient.ExecuteAsync(billingRequest);
        }

        public static async Task<IRestResponse> GenerateInvoiceReport(IEnumerable<InvoiceResponseDTO> currentBatch)
        {
            var boxTruckApiUrl = Environment.GetEnvironmentVariable("BoxTruckApiUrl") ?? "";
            var timeOut = Environment.GetEnvironmentVariable("TimeoutAPI");
            var timeOutApi = string.IsNullOrEmpty(timeOut) ? 5 : double.Parse(timeOut);

            var boxTruckClient = new RestClient(boxTruckApiUrl)
            {
                Timeout = Convert.ToInt32(TimeSpan.FromMinutes(timeOutApi).TotalMilliseconds)
            };
            var boxTruckRequest = new RestRequest
            {
                Resource = "api/InvoiceReport",
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };

            var boxTruckSerializePayload = JsonConvert.SerializeObject(currentBatch);
            boxTruckRequest.AddHeader("Content-Type", "application/json");
            boxTruckRequest.AddParameter("application/json", boxTruckSerializePayload, ParameterType.RequestBody);
            return await boxTruckClient.ExecuteAsync(boxTruckRequest);
        }
    }
}