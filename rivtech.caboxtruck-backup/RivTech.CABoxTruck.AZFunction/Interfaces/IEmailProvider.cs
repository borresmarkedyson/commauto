﻿using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.Interfaces
{
    public interface IEmailProvider
    {
        bool SendEmail(EmailParameters emailParameters, EmailQueueDTO emailQueueDTO = null);
    }
}
