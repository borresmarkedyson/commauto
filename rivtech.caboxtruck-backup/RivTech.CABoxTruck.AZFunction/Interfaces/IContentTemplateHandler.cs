﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Model;
using System;
using System.IO;

namespace RivTech.CABoxTruck.AZFunction.Interfaces
{
    public interface IContentTemplateHandler
    {
        string GetContentTemplate(string templateName);
    }
}
