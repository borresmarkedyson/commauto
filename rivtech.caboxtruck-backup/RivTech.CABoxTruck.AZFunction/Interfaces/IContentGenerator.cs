﻿using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Model;
using System.IO;

namespace RivTech.CABoxTruck.AZFunction.Interfaces
{
    public interface IContentGenerator
    {
        Stream GenerateContent(ContentGeneratorRequest contentGeneratorRequest, Stream contentStream, ILogger iLogger);
    }
}
