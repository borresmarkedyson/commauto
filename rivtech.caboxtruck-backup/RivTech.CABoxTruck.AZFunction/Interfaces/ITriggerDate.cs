﻿using System;

namespace RivTech.CABoxTruck.AZFunction.Interfaces
{
    public interface ITriggerDate
    {
        public DateTime TriggerDate { get; set; }
        public bool IsUserSelectedDate { get; set; }
    }
}