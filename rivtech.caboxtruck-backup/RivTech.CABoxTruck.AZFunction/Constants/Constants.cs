﻿namespace RivTech.CABoxTruck.AZFunction.Constants
{
    public static class JobName
    {
        public const string GenerateInstallmentInvoiceHttp = "GenerateInstallmentInvoiceHttp";
        public const string AutoPolicyCancellationHttp = "AutoPolicyCancellationHttp"; 
        public const string GenerateInstallmentInvoiceOrchestrator = "GenerateInstallmentInvoiceOrchestrator";
        public const string AutoPolicyCancellationOrchestrator = "AutoPolicyCancellationOrchestrator"; 
        public const string GenerateInstallmentInvoiceFunction = "GenerateInstallmentInvoiceFunction";
        public const string AutoPolicyCancellationFunction = "AutoPolicyCancellationFunction"; 
        public const string ContentGeneratorFunction = "ContentGeneratorFunction";
        public const string EmailProviderFunction = "EmailProviderFunction";
        public const string PaymentConfirmationFunction = "PaymentConfirmationFunction";
        public const string NonPayNoticePendingCancellationHttp = "NonPayNoticePendingCancellationHttp";
        public const string NonPayNoticePendingCancellationOrchestrator = "NonPayNoticePendingCancellationOrchestrator";
        public const string NonPayNoticePendingCancellationFunction = "NonPayNoticePendingCancellationFunction";
        public const string RescindNoticePendingCancellationHttp = "RescindNoticePendingCancellationHttp";
        public const string RescindNoticePendingCancellationOrchestrator = "RescindNoticePendingCancellationOrchestrator";
        public const string RescindNoticePendingCancellationFunction = "RescindNoticePendingCancellationFunction";
        public const string SequentialHttp = "SequentialHttp";
        public const string SequentialOrchestrator = "SequentialOrchestrator";
        public const string SendSequentialEmailQueuedFunction = "SendSequentialEmailQueuedFunction";
        public const string SendSequentialEmailQueuedOrchestrator = "SendSequentialEmailQueuedOrchestrator";
        public const string SendSequentialEmailQueuedHttp = "SendSequentialEmailQueuedHttp";
        public const string GenerateAccountsReceivableReportFunction = "GenerateAccountsReceivableReportFunction";
        public const string ProcessFTPFunction = "ProcessFTPFunction";
        public const string ProcessFTPHttp = "ProcessFTPHttp";
        public const string ProcessFTPOrchestrator = "ProcessFTPOrchestrator";
    }

    public static class Disabler
    {
        public const string HttpFunction = "Disable-Http-Function";
        public const string TimerFunction = "Disable-Timer-Function";
        public const string TestFunction = "Disable-Test-Function";
    }

    public static class JobAPI
    {
        public const string BillingInvoice = "Invoice";
        public const string InvoiceReport = "InvoiceReport";
        public const string BoxtruckAutoPolicyCancellation = "AutoPolicyCancellation";
        public const string BoxtruckAutoNoticeCancellation = "AutoNoticeCancellation";
        public const string BoxtruckAutoRescindNoticeCancellation = "AutoRescindNoticeCancellation";
        public const string BoxtruckAutoGetAccountsReceivableReportData = "GetAccountsReceivableReportData";
    }

    public static class LogsJobName
    {
        public const string GenerateInstallmentInvoice = "Generate Installment Invoice Job";
        public const string AutoPolicyCancellation = "Auto Policy Cancellation Job";
        public const string NonPayNoticePendingCancellation = "Non-Pay Notice of Pending Cancellation Job";
        public const string RescindNoticePendingCancellation = "Rescind Notice of Pending Cancellation Job";
        public const string SendSequentialEmailQueued = "Send Sequential Email Queued Job";
        public const string GenerateAccountsReceivableReport = "Generate Accounts Receivable Report Job";
        public const string ProcessFTP = "Process FTP Job";
    }

    public static class GeneralMessage
    {
        public const string NoRecords = "NO RECORD(S) found for ";
        public const string CompletedBatch = "COMPLETED batch";
        public const string FailedBatch = "FAILED batch";
    }

    public static class AzureConstants
    {
        public const string NotApplicable = "Not Applicable";
        public const string BillingApiFailed = "Connection to Billing API Failed";
        public const string BoxTruckApiFailed = "Connection to BoxTruck API Failed";
        public const string ProcessingBatchIds = "Failed in Processing Batch risk Ids. Contact Administrator.";
    }
    public static class CommonConstants
    {
        public const string HttpFunction = "Disable-Http-Function";
        public const string TimerFunction = "Disable-Timer-Function";
        public const string TestFunction = "Disable-Test-Function";
    }

    public static class BoxtruckConfiguration
    {
        public const string SendGridApiKey = "SEND_GRID_API_KEY";
        public const string SenderEmail = "SenderEmail";
        public const string MLSEmail = "MLSEmail";
        public const string RivPartnersEmail = "RivPartnersEmail";
    }
}