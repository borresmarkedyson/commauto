﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.HtmlContentTemplates
{
    public static class PaymentConfirmationEmailTemplate
    {
        public const string PaymentConfirmation = @"
			<html>
			<head>
				<style>
					.companyDetails {
						color: #2F5597;
					}
				</style>
			</head>
			<body>
				<div align='left'>
					<p>
						Hello,
					</p>
					<p>
						Please see below details for recent policy payment:
					</p>
					<p>
						<div>Named Insured: {{InsuredName}}</div>
						<div>Payment Date: {{PaymentDate}}</div>
						<div>Policy Number: {{PolicyNumber}}</div>
						<div>Payment Method: {{PaymentMethod}}</div>
						<div>Amount: {{Amount | currency: CurrencyLanguage}}</div>
						<div>Underwriter: {{Underwriter}}</div>
					</p>
					<p>
						Regards,
					</p>
					<div class='companyDetails'>
						<p>
							<div><strong>Rivington Partners</strong></div>
							<div><i>An Applied Underwriters Company</i></div>
						</p>
						<p>
							877-234-4420 Exp Code 3050
						</p>
					</div>
				</div>
				<!-- StartFooter -->
				<div style='padding: 0px 0px 0px 0px; height: 20px;'>
				</div>
				<!-- EndFooter -->
			</body>
			</html>
        ";
    }
}
