﻿namespace RivTech.CABoxTruck.AZFunction.HtmlContentTemplates
{
    public static class AgentInvoiceEmailTemplate
    {
        public const string AgentInvoiceEmail = @"
            <html>
            <body>

            <p>
            Hello,
            </p>

            <p>
            Please see attached invoice for {{BusinessName}} {{PolicyNumber}}. 
            </p>

            <p>
            Regards, <br/>
            Rivington Commercial Auto<br/>
            </p>

            </body>
            </html>
            ";
    }
}
