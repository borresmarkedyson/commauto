﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.AZFunction.HtmlContentTemplates
{
    public static class AccountsReceivableReportEmailTemplate
	{
        public const string AccountsReceivableReport = @"
			<html>
			<head>
			</head>
			<body>
				<div align='left'>
					<p>
						Hello,
					</p>
					<p>
						Please see attached AR report from TIC Boxtruck.
					</p>
					<p>
						Regards,
					</p>
					<p>
						Rivtech,
					</p>
				</div>
				<!-- StartFooter -->
				<div style='padding: 0px 0px 0px 0px; height: 20px;'>
				</div>
				<!-- EndFooter -->
			</body>
			</html>
        ";
    }
}
