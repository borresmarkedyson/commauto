﻿using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using RivTech.CABoxTruck.AZFunction.Interfaces;

namespace RivTech.CABoxTruck.AZFunction.HtmlContentTemplates
{
    public class ContentTemplateHandler : IContentTemplateHandler
    {
        public string GetContentTemplate(string templateName)
        {
            string contentTemplate = string.Empty;

            switch ((templateName).ToLower())
            {
                case "paymentconfirmationtemplate":
                    contentTemplate = PaymentConfirmationEmailTemplate.PaymentConfirmation;
                    break;
                case "accountsreceivablereportemailtemplate":
                    contentTemplate = AccountsReceivableReportEmailTemplate.AccountsReceivableReport;
                    break;
                case "agentinvoicetemplate":
                    contentTemplate = AgentInvoiceEmailTemplate.AgentInvoiceEmail;
                    break;

            }

            return ReplaceSpaceString(contentTemplate);
        }

        public string ReplaceSpaceString(string content)
        {
            content = content.Replace("\n", "").Replace("\r", "").Replace("\t", "");

            return content;
        }
    }
}
