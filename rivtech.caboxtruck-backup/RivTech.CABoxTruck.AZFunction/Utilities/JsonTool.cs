﻿using System;
using Newtonsoft.Json;

namespace RivTech.CABoxTruck.AZFunction.Utilities
{
    public class JsonTool
    {
        private static JsonSerializerSettings JsonUtcSettings =>
            new JsonSerializerSettings
            {
                DateFormatString = "yyyy-MM-ddTH:mm:ss.fffK",
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };

        public static TDto DeserializeObject<TDto>(string dto) where TDto : class
        {
            try
            {
                return JsonConvert.DeserializeObject<TDto>(dto, JsonUtcSettings);
            }
            catch (Exception)
            {
                return JsonConvert.DeserializeObject<TDto>(dto);
            }
        }
    }
}
