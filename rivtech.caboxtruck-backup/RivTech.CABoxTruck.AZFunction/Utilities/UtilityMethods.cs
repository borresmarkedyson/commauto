﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using RivTech.CABoxTruck.AZFunction.Interfaces;

namespace RivTech.CABoxTruck.AZFunction.Utilities
{
    public static class UtilityMethods
    {
        public static async Task<List<T>> SetTriggerDate<T>(List<T> list, string requestDate)
            where T : class, ITriggerDate
        {
            foreach (var item in list)
            {
                if (!string.IsNullOrWhiteSpace(requestDate))
                {
                    var dateTime = Convert.ToDateTime(requestDate);
                    var timeNow = DateTime.Now;
                    var triggerDate = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day,
                        timeNow.Hour, timeNow.Minute, timeNow.Second, timeNow.Millisecond);

                    item.TriggerDate = triggerDate;
                    item.IsUserSelectedDate = true;
                }
                else
                {
                    item.TriggerDate = DateTime.Now;
                }

                await Task.Delay(10);
            }

            return list;
        }

        public static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public static Stream StringToStream(string src)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(src);
            return new MemoryStream(byteArray);
        }
    }
}