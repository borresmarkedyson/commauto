﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class SendSequentialEmailQueuedFunction
    {
        private readonly IEmailProvider _emailProvider;
        private readonly IHttpClientFactory _httpClientFactory;

        public SendSequentialEmailQueuedFunction(IHttpClientFactory httpClientFactory, IEmailProvider emailProvider)
        {
            _emailProvider = emailProvider;
            _httpClientFactory = httpClientFactory;
        }

        [FunctionName(JobName.SendSequentialEmailQueuedFunction)]
        public async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.SendSequentialEmailQueued} executed at: {DateTime.Now}");

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.SendSequentialEmailQueued },
                JobName = LogsJobName.SendSequentialEmailQueued
            };

            try
            {
                var boxtruckApiResponse = await BoxTruckService.GetAllEmailQueued(invoiceDateRequest);

                if (boxtruckApiResponse.IsSuccessful)
                {

                    var emailQueueDTOs = JsonTool.DeserializeObject<List<EmailQueueDTO>>(boxtruckApiResponse.Content);

                    foreach (var emailQueueDTO in emailQueueDTOs)
                    {
                        EmailParameters emailParameters = new EmailParameters()
                        {
                            Address = emailQueueDTO.Address,
                            FromAddress = emailQueueDTO.FromAddress,
                            BccAddress = emailQueueDTO.BccAddress,
                            CcAddress = emailQueueDTO.CcAddress,
                            Subject = emailQueueDTO.Subject,
                            Content = emailQueueDTO.Content,
                            AttachmentFileName = emailQueueDTO.AttachmentFileName,
                            AttachmentUrl = emailQueueDTO.AttachmentUrl,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now
                        };

                        
                        if(emailQueueDTO.Attachment != null && emailQueueDTO.Attachment.Length > 0)
                        {
                            emailParameters.Attachment = new MemoryStream(emailQueueDTO.Attachment);
                        }
                        // Fetch attachment from URL
                        else if (!String.IsNullOrEmpty(emailQueueDTO.AttachmentUrl))
                        {
                            using (Stream stream = await _httpClientFactory.CreateClient().GetStreamAsync(emailQueueDTO.AttachmentUrl))
                            {
                                emailParameters.Attachment = new MemoryStream();
                                await stream.CopyToAsync(emailParameters.Attachment);
                                emailParameters.Attachment.Seek(0, SeekOrigin.Begin);
                            }
                        }

                        _emailProvider.SendEmail(emailParameters, emailQueueDTO);
                    }

                }
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.SendSequentialEmailQueued}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.SendSequentialEmailQueued} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}