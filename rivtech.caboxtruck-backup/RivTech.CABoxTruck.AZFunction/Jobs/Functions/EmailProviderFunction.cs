using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using System;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using System.Net.Http;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class EmailProviderFunction
    {
        private readonly IEmailProvider _emailProvider;
        private readonly IHttpClientFactory _httpClientFactory;

        public EmailProviderFunction(IHttpClientFactory httpClientFactory, IEmailProvider emailProvider)
        {
            _emailProvider = emailProvider;
            _httpClientFactory = httpClientFactory;
        }

        [FunctionName(JobName.EmailProviderFunction)]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger iLogger)
        {
            iLogger.LogInformation("C# HTTP trigger function processed a request.");

            //string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.name;

            SendEmailRequest sendEmailRequest = JsonConvert.DeserializeObject<SendEmailRequest>(requestBody);

            bool sentSuccess = false;

            try
            {
                iLogger.LogInformation("Start sending email\n");

                // Construct email parameters object based on the request object
                EmailParameters emailParameters = new EmailParameters()
                {
                    Address = sendEmailRequest.ToAddress,
                    FromAddress = sendEmailRequest.FromAddress,
                    BccAddress = sendEmailRequest.BccAddress,
                    CcAddress = sendEmailRequest.CcAddress,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                };

                // Use Subject in the request or fetch from URL
                if (!String.IsNullOrEmpty(sendEmailRequest.Subject))
                {
                    emailParameters.Subject = sendEmailRequest.Subject;
                }

                // Use Content in the request or fetch from URL
                if (!String.IsNullOrEmpty(sendEmailRequest.Content))
                {
                    emailParameters.Content = sendEmailRequest.Content;
                }

                // Fetch attachment from URL
                if (!String.IsNullOrEmpty(sendEmailRequest.AttachmentUrl))
                {
                    using (Stream stream = await _httpClientFactory.CreateClient().GetStreamAsync(sendEmailRequest.AttachmentUrl))
                    {
                        emailParameters.Attachment = new MemoryStream();
                        await stream.CopyToAsync(emailParameters.Attachment);
                        emailParameters.Attachment.Seek(0, SeekOrigin.Begin);
                    }

                    emailParameters.AttachmentFileName = sendEmailRequest.AttachmentFileName;
                    emailParameters.AttachmentUrl = sendEmailRequest.AttachmentUrl;
                }

                sentSuccess = _emailProvider.SendEmail(emailParameters);

                iLogger.LogInformation(sentSuccess.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            return new OkObjectResult(sentSuccess);
        }
    }
}
