﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class ProcessFTPFunction
    {
        [FunctionName(JobName.ProcessFTPFunction)]
        public static async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.ProcessFTP} executed at: {DateTime.Now}");

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.ProcessFTP },
                JobName = LogsJobName.ProcessFTP
            };

            var batchId = !String.IsNullOrWhiteSpace(invoiceDateRequest) ? Convert.ToDateTime(invoiceDateRequest).ToString("MMddyyyy") : null;
            try
            {
                var boxtruckApiResponse = await BoxTruckService.ProcessFtpToCovenir(batchId);

                if (!boxtruckApiResponse.IsSuccessful)
                {
                    var errorResponse = JsonTool.DeserializeObject<ErrorResponse>(boxtruckApiResponse.Content);
                    log.LogError($"FAILED! {JobAPI.BoxtruckAutoPolicyCancellation}. Status: {errorResponse?.StatusCode}, Error Message: {errorResponse?.Message}");

                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.ProcessFTP} failed to run.</p>";

                    reportDTO.ErrorReport.Add(new ErrorReport
                    {
                        PolicyNumber = AzureConstants.NotApplicable,
                        Details = AzureConstants.BoxTruckApiFailed
                    });

                    EmailProvider.ReportEmail(reportDTO);
                    return reportDTO;
                }


                reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Success";
                reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.ProcessFTP} ran successfully.</p>";
                EmailProvider.ReportEmail(reportDTO);
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.ProcessFTP}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.ProcessFTP} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}