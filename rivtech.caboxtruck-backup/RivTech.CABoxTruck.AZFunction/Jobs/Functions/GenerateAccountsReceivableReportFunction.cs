﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class GenerateAccountsReceivableReportFunction
    {
        private readonly IContentGenerator _contentGenerator;

        public GenerateAccountsReceivableReportFunction(IContentGenerator contentGenerator)
        {
            _contentGenerator = contentGenerator;
        }

        [FunctionName(JobName.GenerateAccountsReceivableReportFunction)]
        public async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.GenerateAccountsReceivableReport} executed at: {DateTime.Now}");

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.GenerateAccountsReceivableReport },
                JobName = LogsJobName.GenerateAccountsReceivableReport
            };

            try
            {
                var accountsReceivableRequest = new AccountsReceivableInputDTO() {
                    SortNameBy = "Insured Name",
                    ReceivableType = "All",
                    Broker = "All",
                    PastDueOnly = false,
                    NoPaymentsMade = false
                };

                var boxtruckApiResponse = await BoxTruckService.GetAccountsReceivableReportData(invoiceDateRequest, accountsReceivableRequest);

                if (!boxtruckApiResponse.IsSuccessful)
                {
                    var errorResponse = JsonTool.DeserializeObject<ErrorResponse>(boxtruckApiResponse.Content);
                    log.LogError($"FAILED! {JobAPI.BoxtruckAutoGetAccountsReceivableReportData}. Status: {errorResponse?.StatusCode}, Error Message: {errorResponse?.Message}");

                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.GenerateAccountsReceivableReport} failed to run.</p>";

                    reportDTO.ErrorReport.Add(new ErrorReport
                    {
                        PolicyNumber = AzureConstants.NotApplicable,
                        Details = AzureConstants.BoxTruckApiFailed
                    });

                    EmailProvider.ReportEmail(reportDTO);
                    return reportDTO;
                }

                ContentGeneratorRequest generatecontentRequest = new ContentGeneratorRequest()
                {
                    PayloadType = "Accounts Receivable Report Email",
                    TemplateName = "AccountsReceivableReportEmailTemplate",
                    TemplateType = "Email",
                    DocumentType = "Html"
                };

                Stream contentStream = null;

                contentStream = _contentGenerator.GenerateContent(generatecontentRequest, contentStream, log);

                string content = UtilityMethods.StreamToString(contentStream);

                contentStream.Dispose();

                ContentGeneratorRequest generateAttachmentRequest = new ContentGeneratorRequest()
                {
                    Payload = boxtruckApiResponse.Content,
                    PayloadType = "Accounts Receivable Report CSV",
                    TemplateName = "AccountsReceivableReportTemplate",
                    TemplateType = "CSV",
                    DocumentType = "csv"
                };

                Stream attachmentStream = null;

                attachmentStream = _contentGenerator.GenerateContent(generateAttachmentRequest, attachmentStream, log);

                var newEmailQueueDTO = new EmailQueueDTO()
                {
                    Address = Environment.GetEnvironmentVariable(BoxtruckConfiguration.MLSEmail),
                    FromAddress = Environment.GetEnvironmentVariable(BoxtruckConfiguration.SenderEmail),
                    Subject = string.Format("TIC Boxtruck AR Report {0}", DateTime.Now.ToString("MM/dd/yyyy")),
                    Content = content,
                    AttachmentFileName = string.Format("AccountsReceivable_{0}_{1}_{2}.csv", DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), DateTime.Now.ToString("yyyy")),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    Type = "Accounts Receivable Daily Report",
                };
                if (attachmentStream != null && attachmentStream.Length > 0)
                {
                    MemoryStream ms = new MemoryStream();
                    attachmentStream.CopyTo(ms);
                    newEmailQueueDTO.Attachment = ms.ToArray();
                }

                var insertEmailQueueDailyARResponse = BoxTruckService.InsertEmailQueue(newEmailQueueDTO).Result;

                DateTime parsedDate = DateTime.Parse(invoiceDateRequest) != null ? DateTime.Parse(invoiceDateRequest) : DateTime.Now;

                if (parsedDate.Day == 1)
                {
                    newEmailQueueDTO.Address = Environment.GetEnvironmentVariable(BoxtruckConfiguration.RivPartnersEmail);
                    var insertEmailQueueMonthlyARResponse = BoxTruckService.InsertEmailQueue(newEmailQueueDTO).Result;
                }

                attachmentStream.Dispose();

                reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Success";
                reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.GenerateAccountsReceivableReport} ran successfully.</p>";
                EmailProvider.ReportEmail(reportDTO);
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.GenerateAccountsReceivableReport}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.GenerateAccountsReceivableReport} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}