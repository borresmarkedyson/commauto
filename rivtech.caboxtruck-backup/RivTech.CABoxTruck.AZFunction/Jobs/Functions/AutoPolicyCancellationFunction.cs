﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class AutoPolicyCancellationFunction
    {
        [FunctionName(JobName.AutoPolicyCancellationFunction)]
        public static async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.AutoPolicyCancellation} executed at: {DateTime.Now}");

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.AutoPolicyCancellation },
                JobName = LogsJobName.AutoPolicyCancellation
            };

            try
            {
                var boxtruckApiResponse = await BoxTruckService.AutoPolicyCancellation(invoiceDateRequest);

                if (!boxtruckApiResponse.IsSuccessful)
                {
                    var errorResponse = JsonTool.DeserializeObject<ErrorResponse>(boxtruckApiResponse.Content);
                    log.LogError($"FAILED! {JobAPI.BoxtruckAutoPolicyCancellation}. Status: {errorResponse?.StatusCode}, Error Message: {errorResponse?.Message}");

                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.AutoPolicyCancellation} failed to run.</p>";

                    reportDTO.ErrorReport.Add(new ErrorReport
                    {
                        PolicyNumber = AzureConstants.NotApplicable,
                        Details = AzureConstants.BoxTruckApiFailed
                    });

                    EmailProvider.ReportEmail(reportDTO);
                    return reportDTO;
                }


                reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Success";
                reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.AutoPolicyCancellation} ran successfully.</p>";
                EmailProvider.ReportEmail(reportDTO);
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.AutoPolicyCancellation}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.AutoPolicyCancellation} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}