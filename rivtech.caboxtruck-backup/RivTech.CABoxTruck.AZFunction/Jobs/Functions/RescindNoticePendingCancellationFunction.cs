﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class RescindNoticePendingCancellationFunction
    {
        [FunctionName(JobName.RescindNoticePendingCancellationFunction)]
        public static async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.RescindNoticePendingCancellation} executed at: {DateTime.Now}");

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.RescindNoticePendingCancellation },
                JobName = LogsJobName.RescindNoticePendingCancellation
            };

            try
            {
                var boxtruckApiResponse = await BoxTruckService.AutoRescindNoticeCancellation(invoiceDateRequest);

                if (!boxtruckApiResponse.IsSuccessful)
                {
                    var errorResponse = JsonTool.DeserializeObject<ErrorResponse>(boxtruckApiResponse.Content);
                    log.LogError($"FAILED! {JobAPI.BoxtruckAutoRescindNoticeCancellation}. Status: {errorResponse?.StatusCode}, Error Message: {errorResponse?.Message}");

                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.RescindNoticePendingCancellation} failed to run.</p>";

                    reportDTO.ErrorReport.Add(new ErrorReport
                    {
                        PolicyNumber = AzureConstants.NotApplicable,
                        Details = AzureConstants.BoxTruckApiFailed
                    });

                    EmailProvider.ReportEmail(reportDTO);
                    return reportDTO;
                }

                reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Success";
                reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.RescindNoticePendingCancellation} ran successfully.</p>";
                EmailProvider.ReportEmail(reportDTO);
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.RescindNoticePendingCancellation}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.RescindNoticePendingCancellation} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}