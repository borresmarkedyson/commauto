using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using Microsoft.AspNetCore.Hosting;
using RivTech.CABoxTruck.AZFunction.Utilities;
using System;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class PaymentConfirmationFunction
    {
        private readonly IContentGenerator _contentGenerator;
        private readonly IEmailProvider _emailProvider;

        public PaymentConfirmationFunction(IContentGenerator contentGenerator, IEmailProvider emailProvider)
        {
            _contentGenerator = contentGenerator;
            _emailProvider = emailProvider;
        }

        [FunctionName(JobName.PaymentConfirmationFunction)]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger iLogger, ExecutionContext context)
        {
            iLogger.LogInformation("C# HTTP trigger function processed a request.");

            bool sentSuccess = false;

            //string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.name;

            PaymentConfirmationRequest paymentConfirmationRequest = JsonConvert.DeserializeObject<PaymentConfirmationRequest>(requestBody);

            ContentGeneratorRequest contentGeneratorRequest = new ContentGeneratorRequest()
            {
                Payload = JsonConvert.SerializeObject(paymentConfirmationRequest),
                PayloadType = "Payment Confirmation",
                TemplateName = "PaymentConfirmationTemplate",
                TemplateType = "Email",
                DocumentType = "Html"
            };

            Stream contentStream = null;

            contentStream = _contentGenerator.GenerateContent(contentGeneratorRequest, contentStream, iLogger);

            string content = UtilityMethods.StreamToString(contentStream);

            // Construct email parameters object based on the request object
            EmailParameters emailParameters = new EmailParameters()
            {
                Address = paymentConfirmationRequest.Email,
                FromAddress = Environment.GetEnvironmentVariable("SenderEmail") ?? "",
                Content = content,
                Subject = $"Payment Notification - {paymentConfirmationRequest.InsuredName}-{paymentConfirmationRequest.PolicyNumber}"
            };

            sentSuccess = _emailProvider.SendEmail(emailParameters);

            return new OkObjectResult(sentSuccess);
        }
    }
}
