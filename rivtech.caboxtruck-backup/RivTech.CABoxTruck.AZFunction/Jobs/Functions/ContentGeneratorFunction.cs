using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using Microsoft.AspNetCore.Hosting;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class ContentGeneratorFunction
    {
        private readonly IContentGenerator _contentGenerator;

        public ContentGeneratorFunction(IContentGenerator contentGenerator)
        {
            _contentGenerator = contentGenerator;
        }

        [FunctionName(JobName.ContentGeneratorFunction)]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger iLogger)
        {
            iLogger.LogInformation("C# HTTP trigger function processed a request.");

            //string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.name;

            //Deserialize payload from request body to content template model
            ContentGeneratorRequest contentGeneratorRequest = JsonConvert.DeserializeObject<ContentGeneratorRequest>(requestBody);

            Stream contentStream = null;

            contentStream = _contentGenerator.GenerateContent(contentGeneratorRequest, contentStream, iLogger);

            return new FileStreamResult(contentStream, "Text/html") { FileDownloadName = "Boxtruck.html" };
        }
    }
}
