﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Functions
{
    public class GenerateInstallmentInvoiceFunction
    {
        [FunctionName(JobName.GenerateInstallmentInvoiceFunction)]
        public static async Task<ReportDTO> Run(ILogger log, [ActivityTrigger] string invoiceDateRequest = null)
        {
            log.LogInformation($"{LogsJobName.GenerateInstallmentInvoice} executed at: {DateTime.Now}");

            var size = Environment.GetEnvironmentVariable("BatchSize");
            var batchSize = string.IsNullOrEmpty(size) ? 25 : int.Parse(size);
            var numberOfErrors = 0;

            var reportDTO = new ReportDTO
            {
                EmailQueueDTO = {Subject = LogsJobName.GenerateInstallmentInvoice},
                JobName = LogsJobName.GenerateInstallmentInvoice
            };

            try
            {
                var billingApiResponse = await InvoiceService.GenerateInvoice(invoiceDateRequest);

                if (!billingApiResponse.IsSuccessful)
                {
                    var errorResponse = JsonTool.DeserializeObject<ErrorResponse>(billingApiResponse.Content);
                    log.LogError($"FAILED! {JobAPI.BillingInvoice}. Status: {errorResponse?.StatusCode}, Error Message: {errorResponse?.Message}");

                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.GenerateInstallmentInvoice} failed to run.</p>";

                    reportDTO.ErrorReport.Add(new ErrorReport
                    {
                        PolicyNumber = AzureConstants.NotApplicable,
                        Details = AzureConstants.BillingApiFailed
                    });

                    return reportDTO;
                }

                var invoiceResponse = JsonTool.DeserializeObject<List<InvoiceResponseDTO>>(billingApiResponse.Content);
                if (invoiceResponse != null && invoiceResponse.Any())
                {
                    invoiceResponse = await UtilityMethods.SetTriggerDate(invoiceResponse, invoiceDateRequest);

                    var numberOfBatches = (int) Math.Ceiling((double) invoiceResponse.Count / batchSize);
                    log.LogInformation($"{invoiceResponse.Count} record(s) found and {numberOfBatches} batch of BoxTruck API call, {LogsJobName.GenerateInstallmentInvoice}.");

                    for (var i = 0; i < numberOfBatches; i++)
                        try
                        {
                            var currentBatch = invoiceResponse.Skip(i * batchSize).Take(batchSize).ToList();
                            var boxTruckApiResponse = await InvoiceService.GenerateInvoiceReport(currentBatch);

                            if (!boxTruckApiResponse.IsSuccessful)
                            {
                                log.LogError($"FAILED! {JobAPI.InvoiceReport}, Batch: {i}. Payload: --");
                                log.LogError($"Status: {boxTruckApiResponse.ResponseStatus}, Error Message: {boxTruckApiResponse.StatusDescription}, Exception: {boxTruckApiResponse.Content}");
                                numberOfErrors++;
                                foreach (var record in currentBatch)
                                    reportDTO.ErrorReport.Add(new ErrorReport
                                    {
                                        PolicyNumber = AzureConstants.NotApplicable,
                                        Details = $"{AzureConstants.BoxTruckApiFailed}. RiskId: {record.RiskId}"
                                    });

                                continue;
                            }

                            var jobResponse = JsonTool.DeserializeObject<List<JobResponseDTO>>(boxTruckApiResponse.Content);
                            var messageLog = new List<string>();
                            foreach (var res in jobResponse)
                            {
                                if (res.HasError)
                                {
                                    reportDTO.ErrorReport.Add(new ErrorReport
                                    {
                                        PolicyNumber = res.PolicyNumber,
                                        Details = res.Description
                                    });
                                    numberOfErrors++;
                                }
                                messageLog.Add($"id: {res.RiskId} source: {res.Source} description: {res.Description}");
                            }

                            log.LogInformation($"RESPONSE: {string.Join(", ", messageLog.ToArray())}");
                            log.LogInformation($"{GeneralMessage.CompletedBatch} {i} for {JobAPI.InvoiceReport}.");
                        }
                        catch (Exception e)
                        {
                            log.LogError($"{GeneralMessage.FailedBatch} {i} loop. Exception: {e}");

                            reportDTO.ErrorReport.Add(new ErrorReport
                            {
                                PolicyNumber = AzureConstants.NotApplicable,
                                Details = AzureConstants.ProcessingBatchIds
                            });
                        }
                }
                else
                {
                    log.LogInformation($"{GeneralMessage.NoRecords} {LogsJobName.GenerateInstallmentInvoice}");
                }

                if (numberOfErrors > 0)
                {
                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Failed";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.GenerateInstallmentInvoice} failed to run.</p>";
                }
                else
                {
                    reportDTO.EmailQueueDTO.Subject = $"{reportDTO.EmailQueueDTO.Subject} Success";
                    reportDTO.EmailQueueDTO.Content = $"<p>{LogsJobName.GenerateInstallmentInvoice} ran successfully.</p>";
                }
            }
            catch (Exception e)
            {
                log.LogError($"FAILED! {LogsJobName.GenerateInstallmentInvoice}. Exception: {e}");
            }
            finally
            {
                log.LogInformation($"COMPLETED! {LogsJobName.GenerateInstallmentInvoice} at {DateTime.Now}. Used notice date: {invoiceDateRequest ?? DateTime.Now.ToString("yyyy-MM-dd")}");
            }

            return reportDTO;
        }
    }
}