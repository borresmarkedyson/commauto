using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Triggers
{
    public static class TimerTriggerTest
    {
        [Disable]
        [FunctionName("TimerTriggerTest")]
        public static void Run([TimerTrigger("0 */5 * * * *")] TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");
        }
    }
}