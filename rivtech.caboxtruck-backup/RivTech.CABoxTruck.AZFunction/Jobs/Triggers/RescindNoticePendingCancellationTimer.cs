using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Triggers
{
    public static class RescindNoticePendingCancellationTimer
    {
        [Disable(Disabler.TimerFunction)]
        [FunctionName("RescindNoticePendingCancellationTimer")]
        public static async Task TimerStart([TimerTrigger("0 01 * * *")] TimerInfo timerInfo,
            [DurableClient] IDurableOrchestrationClient starter, ILogger log)
        {

            log.LogInformation($"Current Time: {DateTime.Now:yyyy-MM-dd}");
            
            //TimerTrigger("0 0 5 * * 1-5") schedules at 05:00 AM, Monday through Friday, same as Centuari
            //https://bradymholt.github.io/cron-expression-descriptor/
            //DateTime.Now and trigger time (eg 5AM) follows WEBSITE_TIME_ZONE config value

            var service = new GenericService();
            var holidayList = await service.GetHolidaySchedule();
            var holiday = holidayList.FirstOrDefault(val =>
                val.Day == DateTime.Now.Day && val.Month == DateTime.Now.Month);

            if (holiday != null)
            {
                log.LogWarning($"Job is not executed. {DateTime.Now.ToShortDateString()} is {holiday.HolidayName}");
                return;
            }

            var request = new HttpTriggerDTO
            {
                JobName = "RescindNoticePendingCancellation",
                Date = DateTime.Now.ToString("yyyy-MM-dd")
            };

            log.LogInformation($"Nightly Job orchestrator executed at: {DateTime.Now.ToShortDateString()}");
            var instanceId = await starter.StartNewAsync(JobName.RescindNoticePendingCancellationOrchestrator, request);
            log.LogInformation($"Started orchestration with ID = '{instanceId}'.");
        }
    }
}