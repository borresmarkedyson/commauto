using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Services;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Triggers
{
    public static class HttpTriggerTest
    {
        [Disable(Disabler.TestFunction)]
        [FunctionName("HttpTriggerTest")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string settingsName = req.Query["settingsName"];

            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            settingsName ??= data?.name;

            var settingsValue = "";
            if (!string.IsNullOrEmpty(settingsName))
            {
                settingsValue = Environment.GetEnvironmentVariable(settingsName) ?? "";
            }

            var checkResponse = await GenericService.CheckBillingApiConnection();
            if (checkResponse.IsSuccessful)
            {
                settingsValue += $" - Billing API OK ({checkResponse.Content})";
            }

            var checkResponse2 = await GenericService.CheckBoxTruckApiConnection();
            if (checkResponse2.IsSuccessful)
            {
                settingsValue += $" - BoxTruck API OK \n\n ({checkResponse2.Content})";
            }

            var responseMessage = string.IsNullOrEmpty(settingsName)
                ? "This HTTP triggered function executed successfully. Pass a ?settingsName=Name-Of-Settings in the query string or in the request body for a personalized response."
                : $"Settings Info {{{settingsName}: {settingsValue} }}";

            return new OkObjectResult(responseMessage);
        }

        
    }
}
