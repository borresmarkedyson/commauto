using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;
using RivTech.CABoxTruck.AZFunction.Services;
using RivTech.CABoxTruck.AZFunction.Utilities;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Triggers
{
    public static class GenerateInstallmentInvoiceHttp
    {
        [Disable(Disabler.HttpFunction)]
        [FunctionName(JobName.GenerateInstallmentInvoiceHttp)]
        public static async Task<HttpResponseMessage> HttpStart(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post")]
            HttpRequestMessage req,
            [DurableClient] IDurableOrchestrationClient starter,
            ILogger log)
        {
            var payloadContent = req.Content.ReadAsStringAsync().Result;
            var request = JsonTool.DeserializeObject<HttpTriggerDTO>(payloadContent);

            var service = new GenericService();
            var holidayList = await service.GetHolidaySchedule();

            var dateTime = DateTime.Parse(request.Date);
            var day = dateTime.Day;
            var month = dateTime.Month;

            var holiday = holidayList.FirstOrDefault(val => val.Day == day && val.Month == month);

            if (holiday != null)
            {
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent($"Job is not executed. {dateTime.ToShortDateString()} is {holiday.HolidayName}")
                };
            }

            var instanceId = await starter.StartNewAsync(JobName.GenerateInstallmentInvoiceOrchestrator, request);
            log.LogInformation($"Started orchestration with ID = '{instanceId}'.");
            return starter.CreateCheckStatusResponse(req, instanceId);
        }
    }
}