using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Orchestrators
{
    public static class RescindNoticePendingCancellationOrchestrator
    {
        [FunctionName(JobName.RescindNoticePendingCancellationOrchestrator)]
        public static async Task RunOrchestrator(
            [OrchestrationTrigger] IDurableOrchestrationContext context, ILogger log)
        {
            log = context.CreateReplaySafeLogger(log);

            var inputObject = context.GetInput<HttpTriggerDTO>();

            var rescindNoticePendingCancellation =
                context.CallActivityAsync<ReportDTO>(JobName.RescindNoticePendingCancellationFunction, inputObject?.Date);
            var paralleTasks = new List<Task<ReportDTO>> { rescindNoticePendingCancellation };
            await Task.WhenAll(paralleTasks);

            log.LogInformation($"{JobName.RescindNoticePendingCancellationOrchestrator} completed at: {DateTime.Now}");
        }
    }
}