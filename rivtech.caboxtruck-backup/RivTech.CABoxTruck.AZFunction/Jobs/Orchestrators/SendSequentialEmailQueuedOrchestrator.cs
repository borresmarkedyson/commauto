using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Orchestrators
{
    public static class SendSequentialEmailQueuedOrchestrator
    {
        [FunctionName(JobName.SendSequentialEmailQueuedOrchestrator)]
        public static async Task RunOrchestrator(
            [OrchestrationTrigger] IDurableOrchestrationContext context, ILogger log)
        {
            log = context.CreateReplaySafeLogger(log);

            var inputObject = context.GetInput<HttpTriggerDTO>();

            var sendSequentialEmailQueued = context.CallActivityAsync<ReportDTO>(JobName.SendSequentialEmailQueuedFunction, inputObject?.Date);

            var paralleTasks = new List<Task<ReportDTO>> { sendSequentialEmailQueued };
            await Task.WhenAll(paralleTasks);

            log.LogInformation($"{JobName.SendSequentialEmailQueuedOrchestrator} completed at: {DateTime.Now}");
        }
    }
}