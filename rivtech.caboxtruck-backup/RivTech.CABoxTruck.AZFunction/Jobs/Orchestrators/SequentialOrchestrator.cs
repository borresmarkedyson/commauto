using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Orchestrators
{
    public static class SequentialOrchestrator
    {
        [FunctionName(JobName.SequentialOrchestrator)]
        public static async Task RunOrchestrator(
            [OrchestrationTrigger] IDurableOrchestrationContext context, ILogger log)
        {
            log = context.CreateReplaySafeLogger(log);

            var inputObject = context.GetInput<HttpTriggerDTO>();

            await context.CallActivityAsync<ReportDTO>(JobName.GenerateInstallmentInvoiceFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.RescindNoticePendingCancellationFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.NonPayNoticePendingCancellationFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.AutoPolicyCancellationFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.GenerateAccountsReceivableReportFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.SendSequentialEmailQueuedFunction, inputObject?.Date);

            await context.CallActivityAsync<ReportDTO>(JobName.ProcessFTPFunction, null);

            log.LogInformation($"{JobName.SequentialOrchestrator} completed at: {DateTime.Now}");
        }
    }
}