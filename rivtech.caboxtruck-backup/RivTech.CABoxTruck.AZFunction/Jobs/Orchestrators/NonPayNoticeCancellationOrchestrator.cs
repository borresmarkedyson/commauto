using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;
using Microsoft.Extensions.Logging;
using RivTech.CABoxTruck.AZFunction.Constants;
using RivTech.CABoxTruck.AZFunction.Model;

namespace RivTech.CABoxTruck.AZFunction.Jobs.Orchestrators
{
    public static class NonPayNoticeCancellationOrchestrator
    {
        [FunctionName(JobName.NonPayNoticePendingCancellationOrchestrator)]
        public static async Task RunOrchestrator(
            [OrchestrationTrigger] IDurableOrchestrationContext context, ILogger log)
        {
            log = context.CreateReplaySafeLogger(log);

            var inputObject = context.GetInput<HttpTriggerDTO>();

            var autoPolicyCancellation = context.CallActivityAsync<ReportDTO>(JobName.NonPayNoticePendingCancellationFunction, inputObject?.Date);
            var paralleTasks = new List<Task<ReportDTO>> { autoPolicyCancellation };
            await Task.WhenAll(paralleTasks);

            log.LogInformation($"{JobName.NonPayNoticePendingCancellationOrchestrator} completed at: {DateTime.Now}");
        }
    }
}