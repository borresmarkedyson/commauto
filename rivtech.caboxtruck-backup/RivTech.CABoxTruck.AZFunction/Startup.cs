﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using RivTech.CABoxTruck.AZFunction.Interfaces;
using RivTech.CABoxTruck.AZFunction.Common.ContentGenerator;
using RivTech.CABoxTruck.AZFunction.HtmlContentTemplates;
using RivTech.CABoxTruck.AZFunction.Common.EmailProvider;
using RivTech.CABoxTruck.AZFunction.Model.DTOMapping;
using AutoMapper;

[assembly: FunctionsStartup(typeof(RivTech.CABoxTruck.AZFunction.Startup))]
namespace RivTech.CABoxTruck.AZFunction
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddTransient<IContentGenerator, ContentGenerator>();
            builder.Services.AddTransient<IContentTemplateHandler, ContentTemplateHandler>();
            builder.Services.AddTransient<IEmailProvider, EmailProvider>();
            builder.Services.AddHttpClient();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            builder.Services.AddSingleton(mapper);
        }
    }
}
