﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PaymentProfileConfiguration : IEntityTypeConfiguration<PaymentProfile>
    {
        public void Configure(EntityTypeBuilder<PaymentProfile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(50);
            builder.Property(x => x.PhoneNumber).HasMaxLength(11);
            builder.Property(x => x.Email).IsRequired().HasMaxLength(100);
            builder.Property(x => x.IsRecurringPayment).IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();

            builder.HasOne(x => x.Risk).WithOne().HasForeignKey<PaymentProfile>(x => x.RiskId).IsRequired();
            //builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
        }
    }
}
