﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class NoticeHistoryConfiguration : IEntityTypeConfiguration<NoticeHistory>
    {
        public void Configure(EntityTypeBuilder<NoticeHistory> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.BalanceDueNoticeDate);
            builder.Property(x => x.FlatCancelNopcDate);
            builder.Property(x => x.CancellationEffectiveDate);
            builder.Property(x => x.CancellationProcessDate);
            builder.Property(x => x.NopcNoticeDate);
            builder.HasOne(x => x.Risk).WithOne(x => x.NoticeHistory).HasForeignKey<NoticeHistory>(x => x.RiskId).IsRequired();
        }
    }
}
