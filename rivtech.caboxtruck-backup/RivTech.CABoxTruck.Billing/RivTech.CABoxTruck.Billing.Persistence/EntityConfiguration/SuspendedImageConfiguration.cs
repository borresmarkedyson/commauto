﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedImageConfiguration : IEntityTypeConfiguration<SuspendedImage>
    {
        public void Configure(EntityTypeBuilder<SuspendedImage> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CheckImg).HasMaxLength(20);
            builder.Property(x => x.EnvelopeImg).HasMaxLength(20);
            builder.Property(x => x.InvoiceImg).HasMaxLength(20);
        }
    }
}


