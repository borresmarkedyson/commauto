﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.HasMany(x => x.TransactionDetails);
            builder.Property(x => x.CreatedDate).IsRequired();
        }
    }
}
