﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedCheckConfiguration : IEntityTypeConfiguration<SuspendedCheck>
    {
        public void Configure(EntityTypeBuilder<SuspendedCheck> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Check).HasMaxLength(20);
            builder.Property(x => x.CheckAmount).HasColumnType("decimal(18, 5)");
            builder.Property(x => x.CheckRoutingNum).HasMaxLength(20);
            builder.Property(x => x.CheckAccountNum).HasMaxLength(20);
            builder.Property(x => x.CheckNum).HasMaxLength(20);
        }
    }
}


