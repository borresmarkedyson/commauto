﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.Property(x => x.Amount).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(x => x.EffectiveDate).IsRequired();
            builder.Property(e => e.Reference).HasMaxLength(50);
            builder.Property(e => e.Comment).HasMaxLength(1000);
            builder.Property(x => x.CreatedDate).IsRequired();
            builder.Property(x => x.ClearDate);

            //builder.HasOne(x => x.Risk).WithMany().HasForeignKey(x => x.RiskId).IsRequired();
            builder.HasOne(x => x.InstrumentType).WithMany().HasForeignKey(x => x.InstrumentId).IsRequired();
            //builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
            builder.HasOne(x => x.VoidedBy).WithMany().HasForeignKey(x => x.VoidedById);
            //builder.HasOne(x => x.VoidOfPayment).WithOne().HasForeignKey(x => x.VoidOfPaymentId);
        }
    }
}
