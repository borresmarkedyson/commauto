﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class RivtechApplicationConfiguration : IEntityTypeConfiguration<RivtechApplication>
    {
        public void Configure(EntityTypeBuilder<RivtechApplication> builder)
        {
            builder.Property(o => o.Id)
                .HasMaxLength(10)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Description)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}
