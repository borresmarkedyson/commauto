﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class AuthNetProfileConfiguration : IEntityTypeConfiguration<AuthNetProfile>
    {
        public void Configure(EntityTypeBuilder<AuthNetProfile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CustomerProfileId).IsRequired().HasMaxLength(50);
            builder.Property(x => x.CreatedDate).IsRequired();

            builder.HasOne(x => x.Risk).WithOne().HasForeignKey<AuthNetProfile>(x => x.RiskId).IsRequired();
            //builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
        }
    }
}
