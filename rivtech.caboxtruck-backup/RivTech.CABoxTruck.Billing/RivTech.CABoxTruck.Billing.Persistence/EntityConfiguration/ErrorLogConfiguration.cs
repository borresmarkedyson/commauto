﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.Billing.Data.Entities;

namespace RivTech.Billing.Data.EntitiesConfiguration
{
    public class ErrorLogConfiguration : IEntityTypeConfiguration<ErrorLog>
    {
        public void Configure(EntityTypeBuilder<ErrorLog> builder)
        {
            builder.Property(x => x.Id);
            builder.Property(x => x.UserId);
            builder.Property(x => x.ErrorCode).IsUnicode(false).HasMaxLength(20);
            builder.Property(x => x.ErrorMessage).HasColumnType("varchar(MAX)");
            builder.Property(x => x.JsonMessage).HasColumnType("varchar(MAX)");
            builder.Property(x => x.Action).IsUnicode(false).HasMaxLength(100);
            builder.Property(x => x.Method).IsUnicode(false).HasMaxLength(100);
            builder.Property(x => x.Body).HasColumnType("varchar(MAX)");
            builder.Property(x => x.CreatedDate);
        }
    }
}
