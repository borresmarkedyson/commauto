﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedPaymentConfiguration : IEntityTypeConfiguration<SuspendedPayment>
    {
        public void Configure(EntityTypeBuilder<SuspendedPayment> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ReceiptDate).IsRequired();
            builder.Property(x => x.Time).HasMaxLength(20);
            builder.Property(x => x.PolicyNumber).HasMaxLength(UtilConstant.POLICYNUMBER_LENGTH);
            builder.Property(x => x.PolicyId).HasMaxLength(20);
            builder.Property(x => x.Amount).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(e => e.Comment).HasMaxLength(UtilConstant.STRING_MAXLENGTH);
            builder.Property(e => e.CheckRequested).IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();

            builder.HasOne(x => x.Source).WithMany().HasForeignKey(x => x.SourceId).IsRequired();
            builder.HasOne(x => x.Reason).WithMany().HasForeignKey(x => x.ReasonId);
            builder.HasOne(x => x.Status).WithMany().HasForeignKey(x => x.StatusId).IsRequired();
            builder.HasOne(x => x.Payment).WithMany().HasForeignKey(x => x.PaymentId);
        }
    }
}

