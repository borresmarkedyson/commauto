﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PaymentDetailConfiguration : IEntityTypeConfiguration<PaymentDetail>
    {
        public void Configure(EntityTypeBuilder<PaymentDetail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Amount).HasColumnType("decimal(18, 5)").IsRequired();

            builder.HasOne(x => x.AmountType).WithMany().HasForeignKey(x => x.AmountTypeId).IsRequired();
        }
    }
}
