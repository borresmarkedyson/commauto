﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FirstName).HasMaxLength(50);
            builder.Property(x => x.MiddleName).HasMaxLength(50);
            builder.Property(x => x.LastName).HasMaxLength(50);
            builder.Property(a => a.NameSuffix).HasMaxLength(5);
            builder.Property(x => x.Email).HasMaxLength(100);

            builder.Property(x => x.StreetAddress1).HasMaxLength(200);
            builder.Property(x => x.StreetAddress2).HasMaxLength(200);
            builder.Property(x => x.City).HasMaxLength(100);
            builder.Property(x => x.State).HasMaxLength(5);
            builder.Property(x => x.Zip).HasMaxLength(10);

            builder.Property(u => u.FullName)
                .HasComputedColumnSql("[FirstName] + CASE WHEN [MiddleName] IS NULL THEN '' ELSE ' ' + [MiddleName] END + ' ' + [LastName] + CASE WHEN [NameSuffix] IS NOT NULL THEN ' ' + [NameSuffix] ELSE '' END");
            builder.Property(u => u.StreetAddress)
                .HasComputedColumnSql("[StreetAddress1] + CASE WHEN [StreetAddress2] IS NULL THEN '' ELSE ' ' + [StreetAddress2] END");
        }
    }
}


