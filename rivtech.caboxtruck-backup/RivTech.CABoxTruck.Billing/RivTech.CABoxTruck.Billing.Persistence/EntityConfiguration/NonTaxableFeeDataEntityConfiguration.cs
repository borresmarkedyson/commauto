﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    public class NonTaxableFeeDataEntityConfiguration : IEntityTypeConfiguration<NonTaxableFeeData>
    {
        public void Configure(EntityTypeBuilder<NonTaxableFeeData> builder)
        {
            builder.ToTable("NonTaxableFee");
            builder.HasKey(x => new { x.StateCode, x.FeeKindId, x.EffectiveDate });
            builder.Property(x => x.StateCode).HasColumnType("varchar(3)").HasMaxLength(3); 
            builder.Property(x => x.FeeKindId);
            builder.Property(x => x.EffectiveDate);
        }
    }
}
