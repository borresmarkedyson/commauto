﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    public class FeeKindDataEntityConfiguration : IEntityTypeConfiguration<FeeKindData>
    {
        public void Configure(EntityTypeBuilder<FeeKindData> builder)
        {
            builder.ToTable("FeeKind");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Description);
            builder.Property(x => x.IsActive);
        }
    }
}
