﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedPayeeConfiguration : IEntityTypeConfiguration<SuspendedPayee>
    {
        public void Configure(EntityTypeBuilder<SuspendedPayee> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.Property(x => x.Address).HasMaxLength(60);
            builder.Property(x => x.City).HasMaxLength(40);
            builder.Property(x => x.State).HasMaxLength(40);
            builder.Property(x => x.Zip).HasMaxLength(5);
        }
    }
}
