﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedReturnDetailConfiguration : IEntityTypeConfiguration<SuspendedReturnDetail>
    {
        public void Configure(EntityTypeBuilder<SuspendedReturnDetail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CheckNumber).HasMaxLength(30);
            builder.Property(e => e.CheckCopyLink).HasMaxLength(500);
            builder.Property(x => x.CreatedDate).IsRequired();

            builder.HasOne(x => x.SuspendedPayment).WithOne().HasForeignKey<SuspendedReturnDetail>(x => x.SuspendedPaymentId).IsRequired();
            builder.HasOne(x => x.Payee).WithOne().HasForeignKey<SuspendedReturnDetail>(x => x.PayeeId).IsRequired();
        }
    }
}
