﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PayeeInfoConfiguration : IEntityTypeConfiguration<PayeeInfo>
    {
        public void Configure(EntityTypeBuilder<PayeeInfo> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(100);
            builder.Property(x => x.Address).HasMaxLength(250);
            builder.Property(x => x.City).HasMaxLength(50);
            builder.Property(x => x.State).HasMaxLength(50);
            builder.Property(x => x.ZipCode).HasMaxLength(10);
            builder.Property(x => x.Email).HasMaxLength(UtilConstant.EMAIL_MAXLENGTH);
        }
    }
}
