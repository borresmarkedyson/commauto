﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedPayerConfiguration : IEntityTypeConfiguration<SuspendedPayer>
    {
        public void Configure(EntityTypeBuilder<SuspendedPayer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FirstName).HasMaxLength(50);
            builder.Property(x => x.MiddleInitial).HasMaxLength(1);
            builder.Property(x => x.LastName).HasMaxLength(50);
            builder.Property(x => x.Address1).HasMaxLength(35);
            builder.Property(x => x.Address2).HasMaxLength(35);
            builder.Property(x => x.City).HasMaxLength(30);
            builder.Property(x => x.State).HasMaxLength(2);
            builder.Property(x => x.Zip).HasMaxLength(10);
        }
    }
}


