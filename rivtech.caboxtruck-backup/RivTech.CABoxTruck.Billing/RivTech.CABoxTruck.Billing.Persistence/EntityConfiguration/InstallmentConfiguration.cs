﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class InstallmentConfiguration : IEntityTypeConfiguration<Installment>
    {
        public void Configure(EntityTypeBuilder<Installment> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.Property(x => x.InvoiceOnDate).IsRequired();
            builder.Property(x => x.DueDate).IsRequired();
            builder.Property(x => x.PercentOfOutstanding).HasColumnType("decimal(18, 3)").IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();
        }
    }
}
