﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SharedEntityConfiguration : IEntityTypeConfiguration<SharedEntity>
    {
        public void Configure(EntityTypeBuilder<SharedEntity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(o => o.Id)
                .HasMaxLength(50)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Value)
                .HasMaxLength(50);
        }
    }
}
