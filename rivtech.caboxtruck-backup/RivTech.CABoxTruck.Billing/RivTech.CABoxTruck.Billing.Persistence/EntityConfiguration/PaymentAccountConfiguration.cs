﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PaymentAccountConfiguration : IEntityTypeConfiguration<PaymentAccount>
    {
        public void Configure(EntityTypeBuilder<PaymentAccount> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Description).IsRequired().HasMaxLength(250);
            builder.Property(x => x.IsDefault).IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();
            builder.Property(x => x.UpdatedDate).IsRequired();
            builder.Property(x => x.InsuredAgree).IsRequired();
            builder.Property(x => x.UwAgree).IsRequired();

            builder.HasOne(x => x.InstrumentType).WithMany().HasForeignKey(x => x.InstrumentTypeId).IsRequired();
            builder.HasOne(x => x.Payer).WithMany().HasForeignKey(x => x.PayerId);
            //builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
        }
    }
}
