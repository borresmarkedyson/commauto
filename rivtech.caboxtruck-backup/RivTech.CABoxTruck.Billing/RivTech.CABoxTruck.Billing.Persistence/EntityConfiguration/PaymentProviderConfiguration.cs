﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class PaymentProviderConfiguration : IEntityTypeConfiguration<PaymentProvider>
    {
        public void Configure(EntityTypeBuilder<PaymentProvider> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(o => o.Id).HasMaxLength(10).ValueGeneratedNever().IsRequired();
            builder.Property(o => o.Description).HasMaxLength(50).IsRequired();

            builder.HasOne(x => x.Application).WithMany().HasForeignKey(x => x.ApplicationId).IsRequired();
        }
    }
}

