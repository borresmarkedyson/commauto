﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedStatusHistoryConfiguration : IEntityTypeConfiguration<SuspendedStatusHistory>
    {
        public void Configure(EntityTypeBuilder<SuspendedStatusHistory> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ChangedDate).IsRequired();
            builder.Property(x => x.Comment).HasMaxLength(250);

            builder.HasOne(x => x.Status).WithMany().HasForeignKey(x => x.StatusId).IsRequired().OnDelete(DeleteBehavior.NoAction);
            builder.HasOne(x => x.Payment).WithMany().HasForeignKey(x => x.PaymentId).IsRequired();
        }
    }
}


