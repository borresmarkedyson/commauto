﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class AuthNetAccountConfiguration : IEntityTypeConfiguration<AuthNetAccount>
    {
        public void Configure(EntityTypeBuilder<AuthNetAccount> builder)
        {
            builder.HasKey(x => x.Id);            
            builder.Property(x => x.CustomerPaymentId).IsRequired().HasMaxLength(50);

            builder.HasOne(x => x.PaymentAccount).WithOne()
                .HasForeignKey<AuthNetAccount>(x => x.PaymentAccountId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
