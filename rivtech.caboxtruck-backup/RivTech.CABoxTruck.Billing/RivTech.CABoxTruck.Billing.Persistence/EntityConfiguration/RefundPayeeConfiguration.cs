﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class RefundPayeeConfiguration : IEntityTypeConfiguration<RefundPayee>
    {
        public void Configure(EntityTypeBuilder<RefundPayee> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(UtilConstant.NAME_MAXLENGTH).IsRequired(); //100
            builder.Property(x => x.Street1).HasMaxLength(UtilConstant.STREET1_MAXLENGTH).IsRequired(); //30
            builder.Property(x => x.Street2).HasMaxLength(UtilConstant.STREET2_MAXLENGTH); //30
            builder.Property(x => x.City).HasMaxLength(UtilConstant.CITY_MAXLENGTH).IsRequired(); //40
            builder.Property(x => x.State).HasMaxLength(UtilConstant.STATE_MAXLENGTH).IsRequired(); //40
            builder.Property(x => x.Zip).HasMaxLength(UtilConstant.ZIP_MAXLENGTH).IsRequired(); //5
        }
    }
}
