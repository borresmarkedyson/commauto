﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedDetailConfiguration : IEntityTypeConfiguration<SuspendedDetail>
    {
        public void Configure(EntityTypeBuilder<SuspendedDetail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Number).HasMaxLength(20);
            builder.Property(x => x.EnvelopeNum).HasMaxLength(20);
            builder.Property(x => x.Envelope).HasMaxLength(20);
            builder.Property(x => x.TransactionNum).HasMaxLength(20);
            builder.Property(x => x.LockboxNum).HasMaxLength(20);
            builder.Property(x => x.BatchItem).HasMaxLength(20);
            builder.Property(x => x.InvoicePage).HasMaxLength(20);
            builder.Property(x => x.ClientId).HasMaxLength(16);
            builder.Property(x => x.ClientName).HasMaxLength(45);
            builder.Property(x => x.GroupId).HasMaxLength(16);
            builder.Property(x => x.GroupDescription).HasMaxLength(255);
        }
    }
}


