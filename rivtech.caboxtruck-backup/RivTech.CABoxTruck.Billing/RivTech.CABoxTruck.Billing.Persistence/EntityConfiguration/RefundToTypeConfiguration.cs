﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class RefundToTypeConfiguration : IEntityTypeConfiguration<RefundToType>
    {
        public void Configure(EntityTypeBuilder<RefundToType> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(o => o.Id).HasMaxLength(UtilConstant.ENUMID_MAXLENGTH).ValueGeneratedNever().IsRequired();
            builder.Property(o => o.Description).HasMaxLength(UtilConstant.ENUMDESCRIPTION_MAXLENGTH).IsRequired();
        }
    }
}
