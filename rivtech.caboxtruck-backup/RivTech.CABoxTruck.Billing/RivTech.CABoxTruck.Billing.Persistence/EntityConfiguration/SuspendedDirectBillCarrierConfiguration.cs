﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class SuspendedDirectBillCarrierConfiguration : IEntityTypeConfiguration<SuspendedDirectBillCarrier>
    {
        public void Configure(EntityTypeBuilder<SuspendedDirectBillCarrier> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(o => o.Id).HasMaxLength(10).ValueGeneratedNever().IsRequired();
            builder.Property(o => o.Description).HasMaxLength(50).IsRequired();
        }
    }
}

