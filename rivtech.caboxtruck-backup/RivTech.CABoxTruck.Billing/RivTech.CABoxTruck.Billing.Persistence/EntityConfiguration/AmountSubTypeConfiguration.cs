﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class AmountSubTypeConfiguration : IEntityTypeConfiguration<AmountSubType>
    {
        public void Configure(EntityTypeBuilder<AmountSubType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(o => o.Id)
                .HasMaxLength(30)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Description)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
