﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class ConditionalFeeConfiguration : IEntityTypeConfiguration<ConditionalFee>
    {
        public void Configure(EntityTypeBuilder<ConditionalFee> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(o => o.Id)
                .HasMaxLength(10)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Description)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(x => x.Value).HasColumnType("decimal(18, 2)").IsRequired();
            builder.Property(x => x.IsRate).IsRequired();
            builder.Property(x => x.EffectiveDate).IsRequired();
            builder.Property(x => x.VoidDate);
        }
    }
}
