﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class RiskConfiguration : IEntityTypeConfiguration<Risk>
    {
        public void Configure(EntityTypeBuilder<Risk> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.PolicyNumber).HasMaxLength(30);
            builder.Property(x => x.AlpCommission).HasColumnType("decimal(18, 3)");
            builder.Property(x => x.PdpCommission).HasColumnType("decimal(18, 3)");
        }
    }
}
