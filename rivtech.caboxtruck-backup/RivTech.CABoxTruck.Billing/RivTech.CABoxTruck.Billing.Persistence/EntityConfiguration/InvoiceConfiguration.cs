﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.Property(x => x.InvoiceNumber).HasMaxLength(30).IsRequired();
            builder.Property(x => x.InvoiceDate).IsRequired();
            builder.Property(x => x.DueDate).IsRequired();
            builder.Property(x => x.PreviousBalance).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(x => x.CurrentAmountInvoiced).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(x => x.TotalAmountDue).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(x => x.CreatedDate).IsRequired();
        }
    }
}
