﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Persistence.EntityConfiguration
{
    internal class RefundRequestConfiguration : IEntityTypeConfiguration<RefundRequest>
    {
        public void Configure(EntityTypeBuilder<RefundRequest> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RiskId).IsRequired();
            builder.Property(x => x.Amount).HasColumnType("decimal(18, 5)").IsRequired();
            builder.Property(x => x.CheckNumber).HasMaxLength(UtilConstant.CHECKNUMBER_MAXLENGTH);
            builder.Property(x => x.RefundReason).HasMaxLength(UtilConstant.STRING_MAXLENGTH);
            builder.Property(x => x.CreatedDate).IsRequired();

            builder.HasOne(x => x.Risk).WithMany().HasForeignKey(x => x.RiskId).IsRequired();
            builder.HasOne(x => x.Payment).WithOne().HasForeignKey<RefundRequest>(x => x.PaymentId);
            builder.HasOne(x => x.RefundToType).WithMany().HasForeignKey(x => x.RefundToTypeId).IsRequired();
            builder.HasOne(x => x.RefundPayee).WithOne().HasForeignKey<RefundRequest>(x => x.RefundPayeeId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
        }
    }
}
