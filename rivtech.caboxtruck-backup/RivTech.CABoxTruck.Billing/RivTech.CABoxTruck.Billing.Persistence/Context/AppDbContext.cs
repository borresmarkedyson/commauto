﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Context
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (_currentTransaction != null)
            {
                return null;
            }

            _currentTransaction = await Database.BeginTransactionAsync(cancellationToken);

            return _currentTransaction;
        }

        public async Task<int> CommitTransactionAsync(IDbContextTransaction transaction, CancellationToken cancellationToken = default)
        {
            int result;
            if (transaction == null)
            {
                throw new ArgumentNullException(nameof(transaction));
            }

            if (transaction != _currentTransaction)
            {
                throw new InvalidOperationException($"Transaction {transaction.TransactionId} is not current");
            }

            try
            {
                result = await SaveChangesAsync(cancellationToken);
                await transaction.CommitAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                await RollbackTransaction(cancellationToken);
                if (ex is DbUpdateConcurrencyException)
                {
                    List<string> metaDataNames = new List<string>();
                    var exception = ex as DbUpdateConcurrencyException;
                    foreach (var entry in exception.Entries)
                    {
                        metaDataNames.Add(entry.Metadata.Name);
                    }
                    var names = string.Join(", ", metaDataNames);
                    throw new DbUpdateConcurrencyException(
                        $"Concurrency Error: Data has been updated. Transaction Id: {transaction.TransactionId}. Meta data: {names}");
                }
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    await _currentTransaction.DisposeAsync();
                    _currentTransaction = null;
                }
            }

            return result;
        }

        public async Task RollbackTransaction(CancellationToken cancellationToken = default)
        {
            try
            {
                await _currentTransaction?.RollbackAsync(cancellationToken);
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    await _currentTransaction.DisposeAsync();
                    _currentTransaction = null;
                }
            }
        }

        private IDbContextTransaction _currentTransaction;

        public IDbContextTransaction GetCurrentTransaction() => _currentTransaction;

        public bool HasActiveTransaction => _currentTransaction != null;

        #region enum
        public DbSet<AmountType> AmountType { get; set; }
        public DbSet<AmountSubType> AmountSubType { get; set; }
        public DbSet<TransactionType> TransactionType { get; set; }
        public DbSet<InstallmentType> InstallmentType { get; set; }
        public DbSet<PaymentPlan> PaymentPlan { get; set; }
        public DbSet<InstrumentType> InstrumentType { get; set; }
        public DbSet<PaymentProvider> PaymentProvider { get; set; }
        public DbSet<SuspendedSource> SuspendedSource { get; set; }
        public DbSet<SuspendedReason> SuspendedReason { get; set; }
        public DbSet<SuspendedStatus> SuspendedStatus { get; set; }
        public DbSet<RefundToType> RefundToType { get; set; }
        #endregion

        #region keyless entity
        public DbSet<RefundRequestRisk> RefundRequestRisk { get; set; }
        #endregion
    }
}
