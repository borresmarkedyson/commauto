﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Context
{
    public static class AppDbContextData
    {
        /// <summary>
        /// Initializes the database to contain it's required entities.
        /// </summary>
        public static async Task Initialize(AppDbContext context)
        {
            context.ChangeTracker.DetectChanges();
            await context.SaveChangesAsync();
        }

        public static async Task SeedDataSets(AppDbContext context)
        {
            SeedTable<TransactionType>(context);
            SeedTable<InstallmentType>(context);
            SeedTable<PaymentPlan>(context);
            SeedTable<AmountType>(context);
            SeedTable<AmountSubType>(context);
            SeedTable<InstrumentType>(context);
            SeedTable<RivtechApplication>(context);
            SeedTable<PaymentType>(context);
            SeedTable<PercentDeposit>(context);
            SeedTable<ConditionalFee>(context);
            SeedTable<SuspendedReason>(context);
            SeedTable<SuspendedSource>(context);
            SeedTable<SuspendedStatus>(context);
            SeedTable<RefundToType>(context);
            SeedTable<PolicyStatus>(context);
            SeedTable<SuspendedDirectBillCarrier>(context);

            await context.SaveChangesAsync();
        }

        private static void SeedTable<T>(AppDbContext context) where T : Enumeration, new()
        {
            var valuesToSeed = Enumeration.GetAll<T>();
            foreach (var value in valuesToSeed)
            {
                if (!context.Set<T>().Any(x => x.Id == value.Id))
                {
                    context.Set<T>().Add(value);
                }
            }
        }
    }
}
