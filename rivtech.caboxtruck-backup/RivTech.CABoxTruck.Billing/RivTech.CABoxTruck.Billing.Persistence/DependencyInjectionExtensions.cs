﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using RivTech.CABoxTruck.Billing.Persistence.Repositories;
using System;

namespace RivTech.CABoxTruck.Billing.Persistence
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddPersistenceLayer(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddDbContext<ApplicationWriteDbContext>(options =>
            //    options
            //        .UseSqlServer(configuration.GetConnectionString(PersistenceLayerConstants.WriteDatabaseConnectionStringName))
            //        .EnableSensitiveDataLogging(true)
            //);

            //services.AddScoped<IApplicationWriteDbContext>(provider => provider.GetService<ApplicationWriteDbContext>());
            //services.AddScoped<IApplicationWriteDbFacade, ApplicationWriteDbFacade>();

            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IAuditLogRepository, AuditLogRepository>();
            services.AddScoped<IAppLogRepository, AppLogRepository>();
            services.AddScoped<IAmountSubTypeRepository, AmountSubTypeRepository>();
            services.AddScoped<ITransactionTypeRepository, TransactionTypeRepository>();
            services.AddScoped<IInstallmentTypeRepository, InstallmentTypeRepository>();
            services.AddScoped<IInstallmentRepository, InstallmentRepository>();
            services.AddScoped<IRiskRepository, RiskRepository>();
            services.AddScoped<IPaymentPlanRepository, PaymentPlanRepository>();
            services.AddScoped<IAmountTypeRepository, AmountTypeRepository>();
            services.AddScoped<IInvoiceRepository, InvoiceRepository>();
            services.AddScoped<ISharedEntityRepository, SharedEntityRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IPaymentDetailRepository, PaymentDetailRepository>();
            services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            services.AddScoped<IPaymentProfileRepository, PaymentProfileRepository>();
            services.AddScoped<IPaymentAccountRepository, PaymentAccountRepository>();
            services.AddScoped<IAuthNetProfileRepository, AuthNetProfileRepository>();
            services.AddScoped<IAuthNetAccountRepository, AuthNetAccountRepository>();
            services.AddScoped<ITransactionDetailRepository, TransactionDetailRepository>();
            services.AddTransient<ISuspendedPaymentRepository, SuspendedPaymentRepository>();
            services.AddTransient<ISuspendedStatusHistoryRepository, SuspendedStatusHistoryRepository>();
            services.AddTransient<ISuspendedReturnDetailRepository, SuspendedReturnDetailRepository>();
            services.AddTransient<IRefundRequestRepository, RefundRequestRepository>();
            services.AddTransient<IRefundPayeeRepository, RefundPayeeRepository>();
            services.AddTransient<ISuspendedPayeeRepository, SuspendedPayeeRepository>();
            services.AddTransient<ISuspendedReturnDetailRepository, SuspendedReturnDetailRepository>();
            services.AddTransient<IPolicyStatusRepository, PolicyStatusRepository>();
            services.AddTransient<IErrorLogRepository, ErrorLogRepository>();
            services.AddScoped<INoticeRepository, NoticeRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IStateTaxRepository, StateTaxRepository>();
            services.AddScoped<INonTaxableFeeRepository, NonTaxableFeeRepository>();

            RegisterDatabase(services, configuration);
            return services;
        }

        private static void RegisterDatabase(IServiceCollection services, IConfiguration configuration)
        {
            var databaseSection = configuration.GetSection("ConnectionStrings");
            var connectionString = databaseSection.GetValue<string>("ConBilling");
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(
                    connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        // Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                        sqlOptions.CommandTimeout(60);
                    }));
            services.AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddSqlServer(
                    connectionString,
                    name: "CABoxTruck-check",
                    tags: new[] { "CABoxTruckDB" });
        }
    }
}
