﻿using System;
using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class ApplicationUserRepository : BaseRepository<ApplicationUser, string>, IApplicationUserRepository
    {
        public ApplicationUserRepository(AppDbContext context)
           : base(context)
        {
        }

        public async Task AddUserIfNotExisting(string id, string name)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }

            try
            {
                var userExists = await _context.Set<ApplicationUser>().AnyAsync(x => x.Id == id);
                if (!userExists)
                {
                    var savedUser = new ApplicationUser(id, name);
                    await _context.Set<ApplicationUser>().AddAsync(savedUser);
                    await _context.SaveChangesAsync();
                }
            }
            catch (DbUpdateException e)
            {

            }
        }
    }
}
