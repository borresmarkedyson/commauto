﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities.Logging;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class AuditLogRepository : BaseRepository<AuditLog, long>, IAuditLogRepository
    {
        public AuditLogRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}
