﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class InvoiceRepository : BaseRepository<Invoice, Guid>, IInvoiceRepository
    {
        public InvoiceRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Invoice>> GetAllByRisk(Guid riskId)
        {
            return await _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.InvoiceDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }

        public async Task<Invoice> GetDetails(Guid id)
        {
            return await _context.Set<Invoice>()
                .Include(x => x.InvoiceDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Invoice>> GetAllNotVoided(Guid riskId)
        {
            return await _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId && x.VoidDate == null)
                .Include(x => x.InvoiceDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }

        public async Task<decimal> GetTotalInvoicedAmountAsync(Guid riskId)
        {
            return await _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId && x.VoidDate == null)
                .SumAsync(x => x.CurrentAmountInvoiced);
        }

        public async Task<decimal> GetTotalDueInvoicedAmountAsync(Guid riskId, DateTime dueDate)
        {
            return await _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId && x.VoidDate == null && x.DueDate <= dueDate)
                .SumAsync(x => x.CurrentAmountInvoiced);
        }

        public async Task<Invoice> FindByInvoiceNumber(string invoiceNumber)
        {
            return await _context.Set<Invoice>()
                .Include(x => x.InvoiceDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .FirstOrDefaultAsync(x => x.InvoiceNumber == invoiceNumber);
        }

        public async Task RemoveByRisk(Guid riskId)
        {
            var recordsToRemove = await _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId)
                .ToListAsync();

            _context.Set<Invoice>().RemoveRange(recordsToRemove);

            await _context.SaveChangesAsync();
        }

        public async Task<decimal> GetTotalPastDueAmount(Guid riskId, DateTime dueDate)
        {
            var installments = _context.Set<Installment>().Where(x => x.RiskId == riskId && x.VoidDate == null && x.InvoiceId != null)
                                .Select(x => x.InvoiceId);
                       
            return await _context.Set<Invoice>()
                .Where(x => installments.Contains(x.Id) && x.VoidDate == null && x.DueDate < dueDate)
                .SumAsync(x => x.CurrentAmountInvoiced);
        }

        public Invoice GetCancellationInvoice(Guid riskId)
        {
            var installments = _context.Set<Installment>().Where(x => x.RiskId == riskId && x.VoidDate == null && x.InvoiceId != null)
                                .Select(x => x.InvoiceId);

            return _context.Set<Invoice>()
                .Where(x => x.RiskId == riskId && !installments.Contains(x.Id) && x.VoidDate == null)
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefault();
        }

        public async Task<Invoice> GetLatestInvoiceByNewInvoiceNumber(string newInvoiceNumber)
        {
            return await _context.Set<Invoice>()
                .Include(x => x.InvoiceDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefaultAsync(x => x.InvoiceNumber.Contains(newInvoiceNumber));
        }
    }
}
