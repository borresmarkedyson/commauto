﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class PaymentProfileRepository : BaseRepository<PaymentProfile, Guid>, IPaymentProfileRepository
    {
        public PaymentProfileRepository(AppDbContext context) : base(context)
        {
        }

        public PaymentProfile GetProfileWithDefaultAccount(Guid riskId)
        {
            return _context.Set<PaymentProfile>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.PaymentAccounts.Where(x => x.IsDefault))
                .ThenInclude( x => x.Payer)
                .FirstOrDefault();
        }

        public PaymentProfile GetProfile(Guid riskId)
        {
            return _context.Set<PaymentProfile>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.PaymentAccounts)
                .ThenInclude(x => x.Payer)
                .FirstOrDefault();
        }
    }
}
