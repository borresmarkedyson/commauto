﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class SuspendedStatusHistoryRepository : BaseRepository<SuspendedStatusHistory, Guid>, ISuspendedStatusHistoryRepository
    {
        public SuspendedStatusHistoryRepository(AppDbContext context) : base(context)
        {
        }

        public SuspendedStatusHistory GetFirstPendingData(Guid suspendedPaymentId)
        {
            return _context.Set<SuspendedStatusHistory>()
                .Where(x => x.PaymentId == suspendedPaymentId && x.StatusId == SuspendedStatus.Pending.Id)
                .OrderBy(x => x.ChangedDate)
                .FirstOrDefault();
        }
    }
}


