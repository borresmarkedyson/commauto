﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class TransactionDetailRepository : BaseRepository<TransactionDetail, Guid>, ITransactionDetailRepository
    {
        public TransactionDetailRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<TransactionDetail> GetDetails(Guid id)
        {
            return await _context.Set<TransactionDetail>()
                .Include(x => x.VoidedTransactionDetail)
                .Include(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.Transaction)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<decimal> GetTotalTransactionAmount(Guid riskId)
        {
            return await _context.Set<TransactionDetail>()
                .Include(x => x.Transaction)
                .Where(x => x.Transaction.RiskId == riskId)
                .SumAsync(x => x.Amount);
        }
    }
}
