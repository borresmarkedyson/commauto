﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction, Guid>, ITransactionRepository
    {
        public TransactionRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Transaction>> GetAllByRisk(Guid riskId)
        {
            return await _context.Set<Transaction>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.TransactionType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.VoidedBy)
                .Include(x => x.CreatedBy)
                .ToListAsync();
        }

        public async Task<Transaction> GetDetails(Guid id)
        {
            return await _context.Set<Transaction>()
                .Include(x => x.TransactionType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.VoidedBy)
                .Include(x => x.CreatedBy)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Transaction>> FindByInvoiceId(Guid invoiceId)
        {
            return await _context.Set<Transaction>()
                .Include(x => x.TransactionType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.AmountSubType).ThenInclude(x => x.AmountType)
                .Include(x => x.TransactionDetails).ThenInclude(x => x.VoidedBy)
                .Include(x => x.CreatedBy)
                .Where(x => x.InvoiceId == invoiceId)
                .ToListAsync();
        }

        public async Task RemoveByRisk(Guid riskId)
        {
            var recordsToRemove = await _context.Set<Transaction>()
                .Where(x => x.RiskId == riskId)
                .ToListAsync();

            _context.Set<Transaction>().RemoveRange(recordsToRemove);

            await _context.SaveChangesAsync();
        }
    }
}
