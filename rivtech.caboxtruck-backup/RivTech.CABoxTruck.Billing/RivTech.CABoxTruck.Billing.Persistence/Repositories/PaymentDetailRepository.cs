﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class PaymentDetailRepository : BaseRepository<PaymentDetail, Guid>, IPaymentDetailRepository
    {
        public PaymentDetailRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<PaymentDetail>> GetTotalPerAmountType(Guid? riskId = null, Guid? paymentId = null)
        {
            var paymentDetails = await _context.Set<PaymentDetail>()
                .Where(x => riskId.HasValue? x.Payment.RiskId == riskId : x.Payment.Id == paymentId)
                .Include(x => x.AmountType)
                .ToListAsync();

            return paymentDetails
                .GroupBy(y => y.AmountTypeId)
                .Select(x => new PaymentDetail(x.Sum(x => x.Amount), Enumeration.GetEnumerationById<AmountType>(x.FirstOrDefault().AmountTypeId)))
                .ToList();
        }
    }
}
