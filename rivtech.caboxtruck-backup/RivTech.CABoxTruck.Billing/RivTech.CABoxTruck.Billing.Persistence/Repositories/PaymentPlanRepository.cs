﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class PaymentPlanRepository : BaseRepository<PaymentPlan, string>, IPaymentPlanRepository
    {
        public PaymentPlanRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
