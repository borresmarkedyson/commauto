﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class AuthNetProfileRepository : BaseRepository<AuthNetProfile, Guid>, IAuthNetProfileRepository
    {
        public AuthNetProfileRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
