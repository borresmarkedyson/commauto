﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class PaymentRepository : BaseRepository<Payment, Guid>, IPaymentRepository
    {
        public PaymentRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Payment>> GetAllByRiskId(Guid riskId)
        {
            return await _context.Set<Payment>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.InstrumentType)
                .Include(x => x.PaymentType)
                .Include(x => x.PaymentDetails)
                    .ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }

        public async Task<decimal> GetTotalPaymentAsync(Guid riskId)
        {
            return await _context.Set<Payment>()
                .Where(x => x.RiskId == riskId)
                .SumAsync(x => x.Amount);
        }

        public async Task<Payment> GetDetailsAsync(Guid id)
        {
            return await _context.Set<Payment>()
                .Where(x => x.Id == id)
                .Include(x => x.InstrumentType)
                .Include(x => x.PaymentType)
                .Include(x => x.PaymentDetails)
                    .ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .Include(x => x.PayeeInfo)
                .FirstOrDefaultAsync();
        }

        public async Task<Payment> GetVoidPayment(Guid voidedPaymentId)
        {
            return await _context.Set<Payment>()
                .Where(x => x.VoidOfPaymentId == voidedPaymentId)
                .Include(x => x.InstrumentType)
                .Include(x => x.PaymentType)
                .Include(x => x.PaymentDetails)
                    .ThenInclude(x => x.AmountType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .FirstOrDefaultAsync();
        }

        public async Task<int> GetNsfCount(Guid riskId)
        {
            return await _context.Set<Payment>()
                .CountAsync(x => x.RiskId == riskId && x.PaymentTypeId == PaymentType.NSF.Id);
        }
    }
}