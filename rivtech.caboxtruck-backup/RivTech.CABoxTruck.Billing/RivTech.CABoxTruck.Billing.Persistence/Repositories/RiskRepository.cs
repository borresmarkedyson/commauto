﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class RiskRepository : BaseRepository<Risk, Guid>, IRiskRepository
    {
        public RiskRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Risk> GetDetailsAsync(Guid id)
        {
            return await _context.Set<Risk>()
                .Include(x => x.PaymentPlan)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.InstallmentType)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.CreatedBy)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.VoidedBy)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
        public async Task<Risk> GetDetailsByPolicyNumberAsync(string policyNumber)
        {
            return await _context.Set<Risk>()
                .Include(x => x.PaymentPlan)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.InstallmentType)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.CreatedBy)
                .Include(x => x.InstallmentSchedule).ThenInclude(x => x.VoidedBy)
                .FirstOrDefaultAsync(x => x.PolicyNumber.ToLower() == policyNumber.ToLower());
        }

        public async Task<List<Risk>> GetRisksForCancellation(DateTime requestDate)
        {
            return await _context.Set<Risk>()
                .Where(x => x.PolicyStatusId != PolicyStatus.Cancelled.Id
                        && x.LatestInvoiceDueDate < requestDate).ToListAsync();
        }

        public async Task RemoveById(Guid id)
        {
            var recordToRemove = await _context.Set<Risk>().FirstOrDefaultAsync(x => x.Id == id);
            if (recordToRemove != null)
            {
                _context.Set<Risk>().Remove(recordToRemove);
                await _context.SaveChangesAsync();
            }
        }
    }
}
