﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class SharedEntityRepository : BaseRepository<SharedEntity, string>, ISharedEntityRepository
    {
        public SharedEntityRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
