﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class NoticeRepository : BaseRepository<Risk, Guid>, INoticeRepository
    {
        public NoticeRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<NopcRisk>> GetNopcRisks(DateTime requestDate, string appId)
        {
            var filteredRisks = await (
                from risk in _context.Set<Risk>().Where(x =>
                x.NoticeHistory.NopcNoticeDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == appId)
                select new
                {
                    RiskId = risk.Id,
                    AmountBilled = _context.Set<Invoice>().Where(x => x.RiskId == risk.Id && x.VoidDate == null).Sum(x => x.CurrentAmountInvoiced),
                    AmountPaid = _context.Set<Payment>().Where(x => x.RiskId == risk.Id).Sum(x => x.Amount),
                    TotalTransaction = _context.Set<TransactionDetail>().Where(x => x.Transaction.RiskId == risk.Id
                        && x.Transaction.TransactionTypeId != TransactionType.Cancellation.Id
                        && x.Transaction.TransactionTypeId != TransactionType.Reinstatement.Id).Sum(x => x.Amount),
                    PastDueInvoiceAmount = _context.Set<Invoice>().Where(x => x.RiskId == risk.Id && x.VoidDate == null && x.DueDate < requestDate.Date).Sum(x => x.CurrentAmountInvoiced),
                    EffectiveDate = risk.EffectiveDate,
                    TotalPremium = _context.Set<TransactionDetail>().Where(x => x.Transaction.RiskId == risk.Id && x.AmountSubType.AmountTypeId == AmountType.Premium.Id).Sum(x => x.Amount),
                    PaidPremiums = _context.Set<PaymentDetail>().Where(x => x.Payment.RiskId == risk.Id && x.AmountTypeId == AmountType.Premium.Id).Sum(x => x.Amount),
                }).Where(x => x.AmountPaid > 0)
                .ToListAsync();

            var nopcRisks = (
                from filteredRisk in filteredRisks
                select new NopcRisk
                {
                    RiskId = filteredRisk.RiskId,
                    AmountBilled = filteredRisk.AmountBilled,
                    AmountPaid = filteredRisk.AmountPaid,
                    BalanceDue = filteredRisk.AmountBilled - filteredRisk.AmountPaid,
                    FullAmountDue = filteredRisk.TotalTransaction - filteredRisk.AmountPaid,
                    MinimumAmountDue = filteredRisk.PastDueInvoiceAmount - filteredRisk.AmountPaid,
                    EquityDate = ComputeEquityDate(filteredRisk.EffectiveDate, filteredRisk.TotalPremium, filteredRisk.PaidPremiums)
                }).Where(
                    x => x.EquityDate.HasValue && requestDate.Date >= x.EquityDate.Value.Date.AddDays(-(GetDaysToNextBusinessDate(requestDate) + 14))
                    && x.MinimumAmountDue >= 20
                ).ToList();

            return nopcRisks;
        }

        private static int GetDaysToNextBusinessDate(DateTime date)
        {
            if (date.DayOfWeek == DayOfWeek.Friday)
            {
                return 3;
            }
            else if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }

        private static DateTime? ComputeEquityDate(DateTime effectiveDate, decimal totalPremium, decimal paidPremiums)
        {
            decimal premiumPerDay = totalPremium /
                (decimal)(effectiveDate.Date.AddYears(1) - effectiveDate.Date).TotalDays;

            if (premiumPerDay == 0)
            {
                return null;
            }

            long numberOfPaidDays = (long)(paidPremiums / premiumPerDay);

            try
            {
                DateTime equityDate = effectiveDate.AddDays(numberOfPaidDays);
                DateTime expirationDate = effectiveDate.AddYears(1);

                if (equityDate < expirationDate)
                {
                    return equityDate;
                }
                else
                {
                    return expirationDate;
                }
            }
            catch
            {
                return null;
            }
        }

        public async Task<List<BalanceDueRisk>> GetBalanceDueRisks(DateTime requestDate, string appId)
        {
            var filteredRisks = await (
                from risk in _context.Set<Risk>().Where(x => requestDate.Date >= x.EffectiveDate.Date
                && x.NoticeHistory.BalanceDueNoticeDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == appId)
                select new
                {
                    RiskId = risk.Id,
                    AmountBilled = _context.Set<Invoice>().Where(x => x.RiskId == risk.Id && x.VoidDate == null).Sum(x => x.CurrentAmountInvoiced),
                    AmountPaid = _context.Set<Payment>().Where(x => x.RiskId == risk.Id).Sum(x => x.Amount),
                    InvoiceCount = _context.Set<Invoice>().Count(x => x.RiskId == risk.Id && x.VoidDate == null),
                    TotalTransaction = _context.Set<TransactionDetail>().Where(x => x.Transaction.RiskId == risk.Id
                        && x.Transaction.TransactionTypeId != TransactionType.Cancellation.Id
                        && x.Transaction.TransactionTypeId != TransactionType.Reinstatement.Id).Sum(x => x.Amount),
                    TransferredPaymentCount = _context.Set<Payment>().Count(x => x.RiskId == risk.Id && x.PaymentTypeId == PaymentType.Transfer.Id)
                }).Where(x => x.InvoiceCount == 1 && x.AmountPaid < x.AmountBilled && (x.AmountPaid > 0 || x.TransferredPaymentCount > 0))
                .ToListAsync();

            var balanceDueRisks = (
                from filteredRisk in filteredRisks
                select new BalanceDueRisk
                {
                    RiskId = filteredRisk.RiskId,
                    NoticeDate = requestDate,
                    DepositAmountBilled = filteredRisk.AmountBilled,
                    AmountPaid = filteredRisk.AmountPaid,
                    BalanceDue = filteredRisk.AmountBilled - filteredRisk.AmountPaid,
                    PayoffAmount = filteredRisk.TotalTransaction - filteredRisk.AmountPaid,
                    IsPaymentTransferred = filteredRisk.TransferredPaymentCount > 0 && filteredRisk.AmountPaid == 0
                }).ToList();

            return balanceDueRisks;
        }
    }
}
