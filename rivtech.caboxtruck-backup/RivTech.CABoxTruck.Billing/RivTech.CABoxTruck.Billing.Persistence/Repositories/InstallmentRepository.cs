﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class InstallmentRepository : BaseRepository<Installment, Guid>, IInstallmentRepository
    {
        public InstallmentRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Installment>> GetAllByRisk(Guid riskId)
        {
            return await _context.Set<Installment>()
                .Where(x => x.RiskId == riskId && x.VoidDate == null)
                .Include(x => x.InstallmentType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }

        public async Task<List<Installment>> GetAllIncludingVoid(Guid riskId)
        {
            return await _context.Set<Installment>()
                .Where(x => x.RiskId == riskId)
                .Include(x => x.InstallmentType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }

        public async Task<Installment> FindByInvoiceDate(Guid riskId, DateTime invoiceOnDate)
        {
            return await _context.Set<Installment>()
                .Include(x => x.InstallmentType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .FirstOrDefaultAsync(x => x.RiskId == riskId && x.InvoiceOnDate.Date == invoiceOnDate.Date && x.VoidDate == null && !x.IsInvoiced);
        }

        public async Task<List<Installment>> FindCurrentDayInstallmentsByAppId(DateTime invoiceDate, string appId)
        {
            return await _context.Set<Installment>()
                .Where(x => x.Risk.ApplicationId == appId && !x.IsInvoiced && x.InvoiceOnDate.Date <= invoiceDate.Date && x.InstallmentTypeId == InstallmentType.Installment.Id && x.VoidDate == null)
                .Include(x => x.InstallmentType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .Include(x => x.Risk)
                .ToListAsync();
        }

        public async Task<int> CountAllNotInvoiced(Guid riskId)
        {
            return await _context.Set<Installment>()
                .Where(x => x.RiskId == riskId && !x.IsInvoiced && x.InstallmentTypeId == InstallmentType.Installment.Id && x.VoidDate == null)
                .CountAsync();
        }

        public async Task RemoveByRisk(Guid riskId)
        {
            var recordsToRemove = await _context.Set<Installment>()
                .Where(x => x.RiskId == riskId)
                .ToListAsync();

            _context.Set<Installment>().RemoveRange(recordsToRemove);

            await _context.SaveChangesAsync();
        }

        public async Task<List<Installment>> FindPreviousInstallments(Guid riskId, DateTime requestDate)
        {
            return await _context.Set<Installment>()
                .Where(x => x.RiskId == riskId && !x.IsInvoiced && x.InvoiceOnDate.Date <= requestDate.Date && x.VoidDate == null)
                .Include(x => x.InstallmentType)
                .Include(x => x.CreatedBy)
                .Include(x => x.VoidedBy)
                .ToListAsync();
        }
    }
}
