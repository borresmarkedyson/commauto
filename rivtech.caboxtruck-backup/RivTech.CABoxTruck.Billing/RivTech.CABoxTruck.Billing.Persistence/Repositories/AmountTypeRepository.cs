﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class AmountTypeRepository : BaseRepository<AmountType, string>, IAmountTypeRepository
    {
        public AmountTypeRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
