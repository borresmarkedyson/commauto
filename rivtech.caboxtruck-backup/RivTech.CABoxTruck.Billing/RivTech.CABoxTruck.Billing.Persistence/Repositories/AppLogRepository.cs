﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities.Logging;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class AppLogRepository : BaseRepository<AppLog, long>, IAppLogRepository
    {
        public AppLogRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}
