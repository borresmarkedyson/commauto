﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public abstract class BaseRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        protected readonly AppDbContext _context;

        protected BaseRepository(AppDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            var result = await _context.AddAsync(entity);
            return result.Entity;
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await _context.AddRangeAsync(entities);
        }

        public virtual async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await _context.Set<TEntity>().AnyAsync(expression);
        }

        public virtual async Task<TEntity> FindAsync(TKey key)
        {
            return await _context.FindAsync<TEntity>(key);
        }

        public virtual async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> expression)
        {
            var result = await _context.Set<TEntity>().FirstOrDefaultAsync(expression);
            return result;
        }

        public virtual async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] include)
        {
            return await SetInclude(include).FirstOrDefaultAsync(expression);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await _context.Set<TEntity>().AsNoTracking().Where(expression).ToListAsync();
        }
        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression, params Expression<Func<TEntity, object>>[] include)
        {
            return await SetInclude(include).Where(expression).ToListAsync();
        }

        public TEntity Update(TEntity entity)
        {
            return _context.Update(entity).Entity;
        }

        public virtual TEntity Remove(TEntity entity)
        {
            return _context.Remove(entity).Entity;
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            _context.RemoveRange(entities);
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            int result = 0;
            _context.ChangeTracker.DetectChanges();
            var strategy = _context.Database.CreateExecutionStrategy();
            await strategy.ExecuteAsync(async () =>
            {
                await using var transaction = await _context.BeginTransactionAsync();
                result = await _context.CommitTransactionAsync(transaction);
            });
            return result;
        }

        protected IQueryable<T> SetInclude<T>(params Expression<Func<T, object>>[] includedProperties) where T : BaseEntity<TKey>
        {
            IQueryable<T> query = _context.Set<T>();
            if (includedProperties != null)
            {
                return includedProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            }
            return query;
        }

        public void LogChangeTracker()
        {
            var changes = from e in this._context.ChangeTracker.Entries()
                          where e.State != EntityState.Detached && e.State != EntityState.Unchanged
                          select e;

            foreach (var change in changes)
            {
                Debug.WriteLine($"{change.GetType()} - {change.State}");
            }
        }
    }
}
