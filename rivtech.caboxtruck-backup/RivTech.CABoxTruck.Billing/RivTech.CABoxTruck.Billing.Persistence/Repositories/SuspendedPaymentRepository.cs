﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class SuspendedPaymentRepository : BaseRepository<SuspendedPayment, Guid>, ISuspendedPaymentRepository
    {
        public SuspendedPaymentRepository(AppDbContext context) : base(context)
        {
        }

        public SuspendedPayment GetSuspendedPaymentWithDetail(Guid suspendedPaymentId)
        {
            return _context.Set<SuspendedPayment>()
                .Where(x => x.Id == suspendedPaymentId)
                .Include(p => p.Source)
                .Include(p => p.Reason)
                .Include(p => p.Status)
                .Include(p => p.SuspendedDetail)
                .Include(p => p.SuspendedCheck)
                .Include(p => p.SuspendedImage)
                .Include(p => p.SuspendedPayer)
                .FirstOrDefault();
        }

        public async Task<decimal> GetTotalSuspendedPayment(string policyNumber, string suspendedStatusId)
        {
            return await _context.Set<SuspendedPayment>()
                .Where(x => x.PolicyNumber == policyNumber && x.StatusId == suspendedStatusId)
                .SumAsync(x => x.Amount);
        }
    }
}
