﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class TransactionTypeRepository : BaseRepository<TransactionType, string>, ITransactionTypeRepository
    {
        public TransactionTypeRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
