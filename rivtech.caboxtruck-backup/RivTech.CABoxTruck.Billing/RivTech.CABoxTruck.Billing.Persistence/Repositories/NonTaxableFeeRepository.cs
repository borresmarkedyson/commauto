﻿using Microsoft.EntityFrameworkCore;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class NonTaxableFeeRepository : BaseRepository<NonTaxableFeeData, Guid>, INonTaxableFeeRepository
    {

        public NonTaxableFeeRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<FeeKind>> GetNonTaxableTypesByState(string stateCode, DateTime effectiveDate)
        {
            return await _context.Set<NonTaxableFeeData>()
                .Where(ntf => ntf.StateCode.Equals(stateCode) 
                    && effectiveDate.Date >= ntf.EffectiveDate.Date)
                .Select(ntf => FeeKind.FromId(ntf.FeeKindId)).ToListAsync();

        }
    }
}
