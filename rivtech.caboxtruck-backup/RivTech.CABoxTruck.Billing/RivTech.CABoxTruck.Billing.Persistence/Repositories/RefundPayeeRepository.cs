﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class RefundPayeeRepository : BaseRepository<RefundPayee, Guid>, IRefundPayeeRepository
    {
        public RefundPayeeRepository(AppDbContext context) : base(context)
        {
        }
    }
}

