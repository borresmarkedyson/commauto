﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class InstallmentTypeRepository : BaseRepository<InstallmentType, string>, IInstallmentTypeRepository
    {
        public InstallmentTypeRepository(AppDbContext context)
           : base(context)
        {
        }
    }
}
