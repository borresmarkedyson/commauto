﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.Billing.Data.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class ErrorLogRepository : BaseRepository<ErrorLog, Guid>, IErrorLogRepository
    {
        public ErrorLogRepository(AppDbContext context) : base(context)
        {
        }
    }
}
