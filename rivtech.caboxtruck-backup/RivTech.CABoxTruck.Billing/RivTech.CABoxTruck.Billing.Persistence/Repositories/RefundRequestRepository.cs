﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class RefundRequestRepository : BaseRepository<RefundRequest, Guid>, IRefundRequestRepository
    {
        public RefundRequestRepository(AppDbContext context) : base(context)
        {
        }

        public IEnumerable<RefundRequestRisk> GetRisksWithValidRefundRequest(DateTime date, string appId)
        {
            var query = @"EXEC [dbo].[sp_GetRiskWithValidRefundRequest] @date={0}, @appId={1}";
            return _context.RefundRequestRisk.FromSqlRaw(query, date, appId).ToList();
        }
    }
}

