﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Persistence.Repositories
{
    public class PaymentAccountRepository: BaseRepository<PaymentAccount, Guid>, IPaymentAccountRepository
    {
        public PaymentAccountRepository(AppDbContext context) : base(context)
        {
        }
    }
}
