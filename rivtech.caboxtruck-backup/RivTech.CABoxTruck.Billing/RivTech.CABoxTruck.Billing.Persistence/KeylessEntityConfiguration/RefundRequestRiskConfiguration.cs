﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;

namespace RivTech.CABoxTruck.Billing.Persistence.KeylessEntityConfiguration
{
    internal class RefundRequestRiskConfiguration : IEntityTypeConfiguration<RefundRequestRisk>
    {
        public void Configure(EntityTypeBuilder<RefundRequestRisk> builder)
        {
            builder.HasNoKey().ToView(null);
            builder.Property(x => x.RefundToId).HasMaxLength(10).IsRequired();

            builder.HasOne(x => x.RefundToType).WithMany().HasForeignKey(x => x.RefundToId).IsRequired();
        }
    }
}

