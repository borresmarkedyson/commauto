﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Update_SP_GetRisksWithValidRefundRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(CREATE_FUNCTION_RISKVALIDREFUNDREQUEST);
            migrationBuilder.Sql(UPDATE_STOREDPROCEDURE_GETRISKSWITHVALIDREFUNDREQUEST);
			migrationBuilder.Sql(DROP_FUNCTION_ISREFUNDREQUESTVALID);
		}

        protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.Sql(CREATE_FUNCTION_ISREFUNDREQUESTVALID);
			migrationBuilder.Sql(REVERT_STOREDPROCEDURE_GETRISKSWITHVALIDREFUNDREQUEST);
			migrationBuilder.Sql(DROP_FUNCTION_RISKVALIDREFUNDREQUEST);
		}
		
        const string CREATE_FUNCTION_RISKVALIDREFUNDREQUEST = @"

			CREATE OR ALTER FUNCTION dbo.RiskValidRefundRequest(@date DATE)
			RETURNS TABLE 
			AS
			RETURN
			(
				--RULE: totalPaid > totalTransaction
				--RULE: lastPaymentPostDate is within past 10Days of currentDate

				SELECT Risk.Id, PendingRefundRequest.Amount
				FROM Risk
				CROSS APPLY (select SUM(Payment.Amount) as TotalPayment from Payment where Payment.RiskId = Risk.Id) Payment
				CROSS APPLY (select sum(TransactionDetail.Amount) as TotalWritten from [Transaction]
							inner join TransactionDetail on TransactionDetail.TransactionId = [Transaction].Id
							where [Transaction].RiskId = Risk.Id) Transactions
				CROSS APPLY (select MAX(Payment.CreatedDate) CreatedDate, MAX(Payment.EffectiveDate) PostDate from Payment where Payment.RiskId = Risk.Id) LastPayment
				CROSS APPLY (select MAX(RefundRequest.Amount) Amount from RefundRequest where RefundRequest.RiskId = Risk.Id and RefundRequest.CheckDate IS NULL) PendingRefundRequest
				WHERE Payment.TotalPayment > Transactions.TotalWritten
				AND ( LastPayment.PostDate <= CONVERT(date, @date) AND LastPayment.PostDate >= DATEADD(DAY, -10, CONVERT(date, @date)) )            
			);";

		const string UPDATE_STOREDPROCEDURE_GETRISKSWITHVALIDREFUNDREQUEST = @"
			CREATE or ALTER PROCEDURE [dbo].[sp_GetRiskWithValidRefundRequest] (@date DATE, @appId NVARCHAR(10))
			AS BEGIN
				SELECT ID, AMOUNT INTO #risksValidForRefund FROM dbo.RiskValidRefundRequest(@date);

				DECLARE @riskWithValidRefundRequest TABLE (
					RiskId UNIQUEIDENTIFIER,
					RefundToId NVARCHAR(10)
				);

				--existing valid refundrequest
				INSERT INTO @riskWithValidRefundRequest
				SELECT Risk.Id,
					   RefundRequest.RefundToTypeId
				FROM dbo.Risk
				INNER JOIN dbo.RefundRequest ON RefundRequest.RiskId = Risk.Id
				WHERE RefundRequest.CheckDate IS NULL 
				AND Risk.ApplicationId = @appId
				AND Risk.Id in (select id from #risksValidForRefund)
				AND RefundRequest.Amount = (select Amount from #risksValidForRefund where Id = Risk.Id)

				--delete invalid existing refundrequest
				DELETE RefundRequest
				FROM dbo.RefundRequest 
				inner join dbo.Risk on Risk.Id = RefundRequest.RiskId
				WHERE RefundRequest.CheckDate IS NULL 
				AND Risk.ApplicationId = @appId
				AND RefundRequest.RiskId NOT IN (select RiskId from @riskWithValidRefundRequest )

				--get risk with no refund request, but one can be created
				INSERT INTO @riskWithValidRefundRequest
				SELECT Risk.Id,
						'I'
				FROM dbo.Risk 
				WHERE (SELECT TOP 1 Id FROM dbo.RefundRequest 
						WHERE RefundRequest.RiskId = Risk.Id AND RefundRequest.CheckDate IS NULL) IS NULL		   
				AND Risk.ApplicationId = @appId
				AND Risk.Id in (select id from #risksValidForRefund)

				SELECT * FROM @riskWithValidRefundRequest;
			END;";

		const string DROP_FUNCTION_ISREFUNDREQUESTVALID = @"DROP FUNCTION [dbo].[IsRefundRequestValid];";

		const string CREATE_FUNCTION_ISREFUNDREQUESTVALID = @"
			CREATE OR ALTER FUNCTION dbo.IsRefundRequestValid (@riskId UNIQUEIDENTIFIER, @date DATE)
			RETURNS BIT
			AS
				BEGIN	
					--RULE: totalPaid > totalTransaction
					DECLARE @totalPayment DECIMAL(18,2) = (SELECT sum(amount) FROM dbo.Payment p WHERE RiskId = @riskId);
					IF( @totalPayment IS NULL ) RETURN 0;
					IF( @totalPayment <= (SELECT sum(amount) FROM dbo.[Transaction] t
							INNER JOIN dbo.TransactionDetail td ON td.TransactionId = t.Id
							WHERE RiskId = @riskId) ) RETURN 0;

					--RULE: if has existing request and a new payment was made,
					--      return 0 to recalculate
					DECLARE @lastPaymentCreatedDate DATE = (SELECT TOP 1 CreatedDate 
															FROM dbo.Payment 
															WHERE RiskId = @riskId
															ORDER BY CreatedDate DESC);
					DECLARE @refundRequestDate DATE = (SELECT TOP 1 CreatedDate
														FROM dbo.RefundRequest
														WHERE RiskId = @riskId
														ORDER BY CreatedDate DESC);
					IF( @refundRequestDate IS NOT NULL AND @lastPaymentCreatedDate >= @refundRequestDate)
						RETURN 0;
													 
					--RULE: lastPaymentPostDate is within past 10Days of currentDate
					DECLARE @lastPaymentPostDate DATE = (SELECT TOP 1 EffectiveDate 
															FROM dbo.Payment 
															WHERE RiskId = @riskId
															ORDER BY EffectiveDate DESC);
					IF( @lastPaymentPostDate < DATEADD(DAY, -10, CONVERT(date, @date)) 
						OR @lastPaymentPostDate > CONVERT(date, @date) ) RETURN 0;

					RETURN 1;
				END;";

		const string REVERT_STOREDPROCEDURE_GETRISKSWITHVALIDREFUNDREQUEST = @"
			CREATE or ALTER PROCEDURE [dbo].[sp_GetRiskWithValidRefundRequest] (@date DATE, @appId NVARCHAR(10))
			AS BEGIN
				--get existing valid refundrequest
				DECLARE @riskWithValidRefundRequest TABLE (
					RiskId UNIQUEIDENTIFIER,
					RefundToId NVARCHAR(10)
				);
				INSERT INTO @riskWithValidRefundRequest
				SELECT r.Id,
					   rr.RefundToTypeId
				FROM dbo.Risk r
				INNER JOIN dbo.RefundRequest rr ON rr.RiskId = r.Id
				WHERE rr.CheckDate IS NULL 
				AND r.ApplicationId = @appId
				AND dbo.IsRefundRequestValid(r.Id, @date) = 1;

				--delete invalid existing refundrequest
				DELETE rr
				FROM dbo.RefundRequest rr
				inner join dbo.Risk r on r.Id = rr.RiskId
				WHERE rr.CheckDate IS NULL 
				AND r.ApplicationId = @appId
				AND rr.RiskId NOT IN (select rrValid.RiskId from @riskWithValidRefundRequest rrValid )

				--get risk with no refund request, but one can be created
				--DECLARE @riskToCreatefundRequest  TABLE (
				--	RiskId UNIQUEIDENTIFIER,
				--	RefundToTypeId NVARCHAR(10)
				--);

				INSERT INTO @riskWithValidRefundRequest
				SELECT r.Id,
					   'I'
				FROM dbo.Risk r
				WHERE (SELECT TOP 1 Id FROM dbo.RefundRequest rr
					   WHERE rr.RiskId = r.Id AND rr.CheckDate IS NULL) IS NULL		   
				AND r.ApplicationId = @appId
				AND dbo.IsRefundRequestValid(r.Id, @date) = 1;

				select * from @riskWithValidRefundRequest;
			END;";

		const string DROP_FUNCTION_RISKVALIDREFUNDREQUEST = @"
			DROP FUNCTION [dbo].[RiskValidRefundRequest];";

	}
}
