﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class RemoveAndAddTaxableFeesForNJCA : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[FeeKind] ([Id], [Description], [IsActive])
                    VALUES  ('REINF', 'Reinstatement Fee', 1)
            ");

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate])
                    VALUES  ('NJ', 'REINF', '2021-01-01 00:00:00.0000000'),
                            ('CA', 'REINF', '2021-01-01 00:00:00.0000000')
            ");

            migrationBuilder.Sql("DELETE FROM NonTaxableFee where FeeKindId = 'REINSTAT'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate])
                    VALUES  ('NJ', 'REINSTAT', '2021-01-01 00:00:00.0000000'),
                            ('CA', 'REINSTAT', '2021-01-01 00:00:00.0000000')
            ");

            migrationBuilder.Sql("DELETE FROM NonTaxableFee where FeeKindId = 'REINF'");
            
            migrationBuilder.Sql("DELETE FROM FeeKind where Id = 'REINF'");
        }
    }
}
