﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class PolicyStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NoticeHistoryId",
                table: "Risk");

            migrationBuilder.AddColumn<string>(
                name: "PolicyStatusId",
                table: "Risk",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PolicyStatus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolicyStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Risk_PolicyStatusId",
                table: "Risk",
                column: "PolicyStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_PolicyStatus_PolicyStatusId",
                table: "Risk",
                column: "PolicyStatusId",
                principalTable: "PolicyStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Risk_PolicyStatus_PolicyStatusId",
                table: "Risk");

            migrationBuilder.DropTable(
                name: "PolicyStatus");

            migrationBuilder.DropIndex(
                name: "IX_Risk_PolicyStatusId",
                table: "Risk");

            migrationBuilder.DropColumn(
                name: "PolicyStatusId",
                table: "Risk");

            migrationBuilder.AddColumn<Guid>(
                name: "NoticeHistoryId",
                table: "Risk",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
