﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class NoticeHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "NoticeHistoryId",
                table: "Risk",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "NoticeHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PendingCancellationNoticeDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    NopcNoticeDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BalanceDueNoticeDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RiskId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NoticeHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NoticeHistory_Risk_RiskId",
                        column: x => x.RiskId,
                        principalTable: "Risk",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NoticeHistory_RiskId",
                table: "NoticeHistory",
                column: "RiskId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NoticeHistory");

            migrationBuilder.DropColumn(
                name: "NoticeHistoryId",
                table: "Risk");
        }
    }
}
