﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class RivtechApp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RivtechApplicationId",
                table: "Installment",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RivtechApplication",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RivtechApplication", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Installment_RivtechApplicationId",
                table: "Installment",
                column: "RivtechApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_RivtechApplication_RivtechApplicationId",
                table: "Installment",
                column: "RivtechApplicationId",
                principalTable: "RivtechApplication",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Installment_RivtechApplication_RivtechApplicationId",
                table: "Installment");

            migrationBuilder.DropTable(
                name: "RivtechApplication");

            migrationBuilder.DropIndex(
                name: "IX_Installment_RivtechApplicationId",
                table: "Installment");

            migrationBuilder.DropColumn(
                name: "RivtechApplicationId",
                table: "Installment");
        }
    }
}
