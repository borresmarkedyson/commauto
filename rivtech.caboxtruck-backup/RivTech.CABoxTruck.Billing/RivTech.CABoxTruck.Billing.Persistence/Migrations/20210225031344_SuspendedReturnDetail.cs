﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class SuspendedReturnDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment");

            migrationBuilder.CreateTable(
                name: "SuspendedPayee",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    City = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    State = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedPayee", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedReturnDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SuspendedPaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PayeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CheckNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CheckIssueDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CheckCopyLink = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CheckClearDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CheckEscheatDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedReturnDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedReturnDetail_SuspendedPayee_PayeeId",
                        column: x => x.PayeeId,
                        principalTable: "SuspendedPayee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspendedReturnDetail_SuspendedPayment_SuspendedPaymentId",
                        column: x => x.SuspendedPaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment",
                column: "PayerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedReturnDetail_PayeeId",
                table: "SuspendedReturnDetail",
                column: "PayeeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedReturnDetail_SuspendedPaymentId",
                table: "SuspendedReturnDetail",
                column: "SuspendedPaymentId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuspendedReturnDetail");

            migrationBuilder.DropTable(
                name: "SuspendedPayee");

            migrationBuilder.DropIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment",
                column: "PayerId");
        }
    }
}
