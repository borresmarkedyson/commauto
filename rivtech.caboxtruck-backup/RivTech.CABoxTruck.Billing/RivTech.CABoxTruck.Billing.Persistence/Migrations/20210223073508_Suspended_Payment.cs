﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Suspended_Payment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SuspendedPayer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    City = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    State = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedPayer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedReason",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedReason", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedSource",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedSource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedStatus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedPayment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PolicyNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ReceiptDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PayerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SourceId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    ReasonId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    StatusId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CheckRequested = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedPayment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedPayment_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SuspendedPayment_SuspendedPayer_PayerId",
                        column: x => x.PayerId,
                        principalTable: "SuspendedPayer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspendedPayment_SuspendedReason_ReasonId",
                        column: x => x.ReasonId,
                        principalTable: "SuspendedReason",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspendedPayment_SuspendedSource_SourceId",
                        column: x => x.SourceId,
                        principalTable: "SuspendedSource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspendedPayment_SuspendedStatus_StatusId",
                        column: x => x.StatusId,
                        principalTable: "SuspendedStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedStatusHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StatusId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    ChangedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    ChangedById = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedStatusHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedStatusHistory_ApplicationUser_ChangedById",
                        column: x => x.ChangedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SuspendedStatusHistory_SuspendedPayment_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspendedStatusHistory_SuspendedStatus_StatusId",
                        column: x => x.StatusId,
                        principalTable: "SuspendedStatus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_CreatedById",
                table: "SuspendedPayment",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment",
                column: "PayerId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_ReasonId",
                table: "SuspendedPayment",
                column: "ReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_SourceId",
                table: "SuspendedPayment",
                column: "SourceId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_StatusId",
                table: "SuspendedPayment",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedStatusHistory_ChangedById",
                table: "SuspendedStatusHistory",
                column: "ChangedById");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedStatusHistory_PaymentId",
                table: "SuspendedStatusHistory",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedStatusHistory_StatusId",
                table: "SuspendedStatusHistory",
                column: "StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SuspendedStatusHistory");

            migrationBuilder.DropTable(
                name: "SuspendedPayment");

            migrationBuilder.DropTable(
                name: "SuspendedPayer");

            migrationBuilder.DropTable(
                name: "SuspendedReason");

            migrationBuilder.DropTable(
                name: "SuspendedSource");

            migrationBuilder.DropTable(
                name: "SuspendedStatus");
        }
    }
}
