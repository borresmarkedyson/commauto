﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class AddAmountSubTypeInPaymentDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AmountSubTypeId",
                table: "PaymentDetail",
                type: "nvarchar(30)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDetail_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.DropIndex(
                name: "IX_PaymentDetail_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.DropColumn(
                name: "AmountSubTypeId",
                table: "PaymentDetail");
        }
    }
}
