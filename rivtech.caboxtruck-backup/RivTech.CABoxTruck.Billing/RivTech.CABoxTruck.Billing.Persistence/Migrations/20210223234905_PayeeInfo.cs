﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class PayeeInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PayeeInfoId",
                table: "Payment",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PayeeInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    City = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    State = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ZipCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayeeInfo", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payment_PayeeInfoId",
                table: "Payment",
                column: "PayeeInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_PayeeInfo_PayeeInfoId",
                table: "Payment",
                column: "PayeeInfoId",
                principalTable: "PayeeInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payment_PayeeInfo_PayeeInfoId",
                table: "Payment");

            migrationBuilder.DropTable(
                name: "PayeeInfo");

            migrationBuilder.DropIndex(
                name: "IX_Payment_PayeeInfoId",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "PayeeInfoId",
                table: "Payment");
        }
    }
}
