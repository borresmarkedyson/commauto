﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Add_SuspendedDetail_DirectBillCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClientId",
                table: "SuspendedDetail",
                type: "nvarchar(16)",
                maxLength: 16,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientName",
                table: "SuspendedDetail",
                type: "nvarchar(45)",
                maxLength: 45,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupDescription",
                table: "SuspendedDetail",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "SuspendedDetail",
                type: "nvarchar(16)",
                maxLength: 16,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SettlementDate",
                table: "SuspendedDetail",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "SuspendedDetail");

            migrationBuilder.DropColumn(
                name: "ClientName",
                table: "SuspendedDetail");

            migrationBuilder.DropColumn(
                name: "GroupDescription",
                table: "SuspendedDetail");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "SuspendedDetail");

            migrationBuilder.DropColumn(
                name: "SettlementDate",
                table: "SuspendedDetail");
        }
    }
}
