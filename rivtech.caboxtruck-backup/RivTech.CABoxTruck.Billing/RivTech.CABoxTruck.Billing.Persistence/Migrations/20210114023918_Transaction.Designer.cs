﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RivTech.CABoxTruck.Billing.Persistence.Context;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20210114023918_Transaction")]
    partial class Transaction
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("ApplicationUser");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.Transaction", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("CreatedById")
                        .HasColumnType("nvarchar(50)");

                    b.Property<DateTime>("CreatedDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("EffectiveDate")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("RiskId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("TransactionTypeId")
                        .HasColumnType("nvarchar(10)");

                    b.Property<DateTime>("VoidDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("VoidedById")
                        .HasColumnType("nvarchar(50)");

                    b.Property<Guid?>("VoidedTransactionId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("CreatedById");

                    b.HasIndex("TransactionTypeId");

                    b.HasIndex("VoidedById");

                    b.HasIndex("VoidedTransactionId");

                    b.ToTable("Transaction");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.TransactionDetail", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18,2)");

                    b.Property<string>("AmountSubTypeId")
                        .HasColumnType("nvarchar(10)");

                    b.Property<Guid?>("TransactionId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("AmountSubTypeId");

                    b.HasIndex("TransactionId");

                    b.ToTable("TransactionDetail");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.ValueObjects.AmountSubType", b =>
                {
                    b.Property<string>("Id")
                        .HasMaxLength(10)
                        .HasColumnType("nvarchar(10)");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.ToTable("AmountSubTypes");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.ValueObjects.TransactionType", b =>
                {
                    b.Property<string>("Id")
                        .HasMaxLength(10)
                        .HasColumnType("nvarchar(10)");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<bool>("IsActive")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.ToTable("TransactionTypes");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.Transaction", b =>
                {
                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.Entities.ApplicationUser", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById");

                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.ValueObjects.TransactionType", "TransactionType")
                        .WithMany()
                        .HasForeignKey("TransactionTypeId");

                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.Entities.ApplicationUser", "VoidedBy")
                        .WithMany()
                        .HasForeignKey("VoidedById");

                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.Entities.Transaction", "VoidedTransaction")
                        .WithMany()
                        .HasForeignKey("VoidedTransactionId");

                    b.Navigation("CreatedBy");

                    b.Navigation("TransactionType");

                    b.Navigation("VoidedBy");

                    b.Navigation("VoidedTransaction");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.TransactionDetail", b =>
                {
                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.ValueObjects.AmountSubType", "AmountSubType")
                        .WithMany()
                        .HasForeignKey("AmountSubTypeId");

                    b.HasOne("RivTech.CABoxTruck.Billing.Domain.Entities.Transaction", null)
                        .WithMany("TransactionDetails")
                        .HasForeignKey("TransactionId");

                    b.Navigation("AmountSubType");
                });

            modelBuilder.Entity("RivTech.CABoxTruck.Billing.Domain.Entities.Transaction", b =>
                {
                    b.Navigation("TransactionDetails");
                });
#pragma warning restore 612, 618
        }
    }
}
