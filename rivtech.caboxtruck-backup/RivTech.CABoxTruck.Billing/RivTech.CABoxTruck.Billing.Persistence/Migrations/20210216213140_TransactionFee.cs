﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class TransactionFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransactionFee",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RiskId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AmountSubTypeId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    VoidDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    VoidedById = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionFee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionFee_AmountSubType_AmountSubTypeId",
                        column: x => x.AmountSubTypeId,
                        principalTable: "AmountSubType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransactionFee_ApplicationUser_VoidedById",
                        column: x => x.VoidedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFee_AmountSubTypeId",
                table: "TransactionFee",
                column: "AmountSubTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFee_VoidedById",
                table: "TransactionFee",
                column: "VoidedById");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransactionFee");
        }
    }
}
