﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class DbSets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Installment_InstallmentType_InstallmentTypeId",
                table: "Installment");

            migrationBuilder.DropForeignKey(
                name: "FK_Risk_PaymentPlan_PaymentPlanId",
                table: "Risk");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentPlan",
                table: "PaymentPlan");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstallmentType",
                table: "InstallmentType");

            migrationBuilder.RenameTable(
                name: "PaymentPlan",
                newName: "PaymentPlans");

            migrationBuilder.RenameTable(
                name: "InstallmentType",
                newName: "InstallmentTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentPlans",
                table: "PaymentPlans",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstallmentTypes",
                table: "InstallmentTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_InstallmentTypes_InstallmentTypeId",
                table: "Installment",
                column: "InstallmentTypeId",
                principalTable: "InstallmentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_PaymentPlans_PaymentPlanId",
                table: "Risk",
                column: "PaymentPlanId",
                principalTable: "PaymentPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Installment_InstallmentTypes_InstallmentTypeId",
                table: "Installment");

            migrationBuilder.DropForeignKey(
                name: "FK_Risk_PaymentPlans_PaymentPlanId",
                table: "Risk");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentPlans",
                table: "PaymentPlans");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstallmentTypes",
                table: "InstallmentTypes");

            migrationBuilder.RenameTable(
                name: "PaymentPlans",
                newName: "PaymentPlan");

            migrationBuilder.RenameTable(
                name: "InstallmentTypes",
                newName: "InstallmentType");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentPlan",
                table: "PaymentPlan",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstallmentType",
                table: "InstallmentType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_InstallmentType_InstallmentTypeId",
                table: "Installment",
                column: "InstallmentTypeId",
                principalTable: "InstallmentType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_PaymentPlan_PaymentPlanId",
                table: "Risk",
                column: "PaymentPlanId",
                principalTable: "PaymentPlan",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
