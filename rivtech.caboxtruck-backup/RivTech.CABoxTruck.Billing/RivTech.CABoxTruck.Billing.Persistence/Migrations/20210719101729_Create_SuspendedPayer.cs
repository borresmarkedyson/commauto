﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Create_SuspendedPayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DirectBillCarrierId",
                table: "SuspendedPayment",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SuspendedDirectBillCarrier",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedDirectBillCarrier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedPayer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SuspendedPaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    MiddleInitial = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Address1 = table.Column<string>(type: "nvarchar(35)", maxLength: 35, nullable: true),
                    Address2 = table.Column<string>(type: "nvarchar(35)", maxLength: 35, nullable: true),
                    City = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    State = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedPayer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedPayer_SuspendedPayment_SuspendedPaymentId",
                        column: x => x.SuspendedPaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_DirectBillCarrierId",
                table: "SuspendedPayment",
                column: "DirectBillCarrierId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayer_SuspendedPaymentId",
                table: "SuspendedPayer",
                column: "SuspendedPaymentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SuspendedPayment_SuspendedDirectBillCarrier_DirectBillCarrierId",
                table: "SuspendedPayment",
                column: "DirectBillCarrierId",
                principalTable: "SuspendedDirectBillCarrier",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuspendedPayment_SuspendedDirectBillCarrier_DirectBillCarrierId",
                table: "SuspendedPayment");

            migrationBuilder.DropTable(
                name: "SuspendedDirectBillCarrier");

            migrationBuilder.DropTable(
                name: "SuspendedPayer");

            migrationBuilder.DropIndex(
                name: "IX_SuspendedPayment_DirectBillCarrierId",
                table: "SuspendedPayment");

            migrationBuilder.DropColumn(
                name: "DirectBillCarrierId",
                table: "SuspendedPayment");
        }
    }
}
