﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class LinkTransactionFeeToTransactionDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TransactionFeeId",
                table: "TransactionDetail",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDetail_TransactionFeeId",
                table: "TransactionDetail",
                column: "TransactionFeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_TransactionFee_TransactionFeeId",
                table: "TransactionDetail",
                column: "TransactionFeeId",
                principalTable: "TransactionFee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_TransactionFee_TransactionFeeId",
                table: "TransactionDetail");

            migrationBuilder.DropIndex(
                name: "IX_TransactionDetail_TransactionFeeId",
                table: "TransactionDetail");

            migrationBuilder.DropColumn(
                name: "TransactionFeeId",
                table: "TransactionDetail");
        }
    }
}
