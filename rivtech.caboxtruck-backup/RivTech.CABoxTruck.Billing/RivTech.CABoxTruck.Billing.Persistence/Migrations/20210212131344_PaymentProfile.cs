﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class PaymentProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuthNetProfile",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RiskId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CustomerProfileId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthNetProfile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthNetProfile_Risk_RiskId",
                        column: x => x.RiskId,
                        principalTable: "Risk",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentProfile",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RiskId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(11)", maxLength: 11, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsRecurringPayment = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentProfile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentProfile_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentProfile_Risk_RiskId",
                        column: x => x.RiskId,
                        principalTable: "Risk",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentProvider",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    ApplicationId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentProvider", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentProvider_RivtechApplication_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "RivtechApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentAccount",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaymentProfileId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InstrumentTypeId = table.Column<string>(type: "nvarchar(10)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedById = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentAccount", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentAccount_ApplicationUser_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PaymentAccount_InstrumentType_InstrumentTypeId",
                        column: x => x.InstrumentTypeId,
                        principalTable: "InstrumentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentAccount_PaymentProfile_PaymentProfileId",
                        column: x => x.PaymentProfileId,
                        principalTable: "PaymentProfile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuthNetAccount",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PaymentAccountId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuthNetProfileId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CustomerPaymentId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthNetAccount", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuthNetAccount_AuthNetProfile_AuthNetProfileId",
                        column: x => x.AuthNetProfileId,
                        principalTable: "AuthNetProfile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthNetAccount_PaymentAccount_PaymentAccountId",
                        column: x => x.PaymentAccountId,
                        principalTable: "PaymentAccount",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuthNetAccount_AuthNetProfileId",
                table: "AuthNetAccount",
                column: "AuthNetProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthNetAccount_PaymentAccountId",
                table: "AuthNetAccount",
                column: "PaymentAccountId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuthNetProfile_RiskId",
                table: "AuthNetProfile",
                column: "RiskId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentAccount_CreatedById",
                table: "PaymentAccount",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentAccount_InstrumentTypeId",
                table: "PaymentAccount",
                column: "InstrumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentAccount_PaymentProfileId",
                table: "PaymentAccount",
                column: "PaymentProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfile_CreatedById",
                table: "PaymentProfile",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfile_RiskId",
                table: "PaymentProfile",
                column: "RiskId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProvider_ApplicationId",
                table: "PaymentProvider",
                column: "ApplicationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthNetAccount");

            migrationBuilder.DropTable(
                name: "PaymentProvider");

            migrationBuilder.DropTable(
                name: "AuthNetProfile");

            migrationBuilder.DropTable(
                name: "PaymentAccount");

            migrationBuilder.DropTable(
                name: "PaymentProfile");
        }
    }
}
