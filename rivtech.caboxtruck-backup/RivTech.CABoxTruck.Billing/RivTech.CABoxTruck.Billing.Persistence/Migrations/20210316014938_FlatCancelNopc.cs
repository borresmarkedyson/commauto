﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class FlatCancelNopc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PendingCancellationNoticeDate",
                table: "NoticeHistory",
                newName: "FlatCancelNopcDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FlatCancelNopcDate",
                table: "NoticeHistory",
                newName: "PendingCancellationNoticeDate");
        }
    }
}
