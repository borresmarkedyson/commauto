﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class PercentDepositAndConditionalFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ConditionalFee",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    RivtechApplicationId = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    AmountSubTypeId = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    IsRate = table.Column<bool>(type: "bit", nullable: false),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    VoidDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConditionalFee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConditionalFee_AmountSubType_AmountSubTypeId",
                        column: x => x.AmountSubTypeId,
                        principalTable: "AmountSubType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ConditionalFee_RivtechApplication_RivtechApplicationId",
                        column: x => x.RivtechApplicationId,
                        principalTable: "RivtechApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PercentDeposit",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ApplicationId = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    PaymentPlanId = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PercentDeposit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PercentDeposit_PaymentPlan_PaymentPlanId",
                        column: x => x.PaymentPlanId,
                        principalTable: "PaymentPlan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PercentDeposit_RivtechApplication_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "RivtechApplication",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConditionalFee_AmountSubTypeId",
                table: "ConditionalFee",
                column: "AmountSubTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ConditionalFee_RivtechApplicationId",
                table: "ConditionalFee",
                column: "RivtechApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_PercentDeposit_ApplicationId",
                table: "PercentDeposit",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_PercentDeposit_PaymentPlanId",
                table: "PercentDeposit",
                column: "PaymentPlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConditionalFee");

            migrationBuilder.DropTable(
                name: "PercentDeposit");
        }
    }
}
