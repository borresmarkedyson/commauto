﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class IsFlatCancelInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFlatCancelInvoice",
                table: "Invoice",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFlatCancelInvoice",
                table: "Invoice");
        }
    }
}
