﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class UpdatePremiumServieFeesDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE dbo.AmountSubType 
            SET Description = 'Service Fee - AL'
            WHERE Id = 'ALSF';

            UPDATE dbo.AmountSubType 
            SET Description = 'Service Fee - PD'
            WHERE Id = 'PDSF';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE dbo.AmountSubType 
            SET Description = 'Auto Liability Service Fee'
            WHERE Id = 'ALSF';

            UPDATE dbo.AmountSubType 
            SET Description = 'Physical Damage Service Fee'
            WHERE Id = 'PDSF';
            ");
        }
    }
}
