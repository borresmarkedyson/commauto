﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class RefundRequest_Edit_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "RefundPayee");

            migrationBuilder.AddColumn<string>(
                name: "RefundReason",
                table: "RefundRequest",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Street1",
                table: "RefundPayee",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Street2",
                table: "RefundPayee",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RefundReason",
                table: "RefundRequest");

            migrationBuilder.DropColumn(
                name: "Street1",
                table: "RefundPayee");

            migrationBuilder.DropColumn(
                name: "Street2",
                table: "RefundPayee");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "RefundPayee",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");
        }
    }
}
