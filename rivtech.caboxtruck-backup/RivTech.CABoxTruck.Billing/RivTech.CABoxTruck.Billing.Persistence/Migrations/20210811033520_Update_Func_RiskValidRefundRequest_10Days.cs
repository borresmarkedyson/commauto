﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Update_Func_RiskValidRefundRequest_10Days : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.Sql(UPDATE_FUNCTION_RISKVALIDREFUNDREQUEST_10DAYS);
		}

        protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.Sql(REVERT_FUNCTION_RISKVALIDREFUNDREQUEST);
		}

		const string UPDATE_FUNCTION_RISKVALIDREFUNDREQUEST_10DAYS = @"
			CREATE OR ALTER FUNCTION dbo.RiskValidRefundRequest(@date DATE)
			RETURNS TABLE 
			AS
			RETURN
			(
				--RULE: totalPaid > totalTransaction
				--RULE: lastPaymentPostDate is 10+ days older

				SELECT Risk.Id, Payment.TotalPayment - Transactions.TotalWritten as Amount
				FROM Risk
				CROSS APPLY (select SUM(Payment.Amount) as TotalPayment from Payment where Payment.RiskId = Risk.Id) Payment
				CROSS APPLY (select sum(TransactionDetail.Amount) as TotalWritten from [Transaction]
							inner join TransactionDetail on TransactionDetail.TransactionId = [Transaction].Id
							where [Transaction].RiskId = Risk.Id) Transactions
				CROSS APPLY (select MAX(Payment.CreatedDate) CreatedDate, MAX(Payment.EffectiveDate) PostDate from Payment where Payment.RiskId = Risk.Id) LastPayment
				WHERE Payment.TotalPayment > Transactions.TotalWritten      
				AND ( CONVERT(date, @date) >= DATEADD(DAY, 10, LastPayment.PostDate) )
			);";

		const string REVERT_FUNCTION_RISKVALIDREFUNDREQUEST = @"
			CREATE OR ALTER FUNCTION dbo.RiskValidRefundRequest(@date DATE)
			RETURNS TABLE 
			AS
			RETURN
			(
				--RULE: totalPaid > totalTransaction
				--RULE: lastPaymentPostDate is within past 10Days of currentDate

				SELECT Risk.Id, Payment.TotalPayment - Transactions.TotalWritten as Amount
				FROM Risk
				CROSS APPLY (select SUM(Payment.Amount) as TotalPayment from Payment where Payment.RiskId = Risk.Id) Payment
				CROSS APPLY (select sum(TransactionDetail.Amount) as TotalWritten from [Transaction]
							inner join TransactionDetail on TransactionDetail.TransactionId = [Transaction].Id
							where [Transaction].RiskId = Risk.Id) Transactions
				CROSS APPLY (select MAX(Payment.CreatedDate) CreatedDate, MAX(Payment.EffectiveDate) PostDate from Payment where Payment.RiskId = Risk.Id) LastPayment
				WHERE Payment.TotalPayment > Transactions.TotalWritten
				AND ( LastPayment.PostDate <= CONVERT(date, @date) AND LastPayment.PostDate >= DATEADD(DAY, -10, CONVERT(date, @date)) )            
			);";
	}
}
