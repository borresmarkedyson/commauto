﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class RemoveRequiredAmountSubType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.AlterColumn<string>(
                name: "AmountSubTypeId",
                table: "PaymentDetail",
                type: "nvarchar(30)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.AlterColumn<string>(
                name: "AmountSubTypeId",
                table: "PaymentDetail",
                type: "nvarchar(30)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
