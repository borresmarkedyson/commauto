﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Create_Table_Customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "InsuredAgree",
                table: "PaymentAccount",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                table: "PaymentAccount",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "UwAgree",
                table: "PaymentAccount",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    MiddleName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    NameSuffix = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true, computedColumnSql: "[FirstName] + CASE WHEN [MiddleName] IS NULL THEN '' ELSE ' ' + [MiddleName] END + ' ' + [LastName] + CASE WHEN [NameSuffix] IS NOT NULL THEN ' ' + [NameSuffix] ELSE '' END"),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StreetAddress1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    StreetAddress2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    StreetAddress = table.Column<string>(type: "nvarchar(max)", nullable: true, computedColumnSql: "[StreetAddress1] + CASE WHEN [StreetAddress2] IS NULL THEN '' ELSE ' ' + [StreetAddress2] END"),
                    City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    State = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentAccount_PayerId",
                table: "PaymentAccount",
                column: "PayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentAccount_Customer_PayerId",
                table: "PaymentAccount",
                column: "PayerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentAccount_Customer_PayerId",
                table: "PaymentAccount");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_PaymentAccount_PayerId",
                table: "PaymentAccount");

            migrationBuilder.DropColumn(
                name: "InsuredAgree",
                table: "PaymentAccount");

            migrationBuilder.DropColumn(
                name: "PayerId",
                table: "PaymentAccount");

            migrationBuilder.DropColumn(
                name: "UwAgree",
                table: "PaymentAccount");
        }
    }
}
