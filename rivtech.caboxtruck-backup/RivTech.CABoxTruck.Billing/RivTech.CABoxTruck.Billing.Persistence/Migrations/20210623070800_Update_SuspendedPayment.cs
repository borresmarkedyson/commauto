﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Update_SuspendedPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuspendedPayment_SuspendedPayer_PayerId",
                table: "SuspendedPayment");

            migrationBuilder.DropTable(
                name: "SuspendedPayer");

            migrationBuilder.DropIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment");

            migrationBuilder.DropColumn(
                name: "PayerId",
                table: "SuspendedPayment");

            migrationBuilder.AlterColumn<string>(
                name: "PolicyNumber",
                table: "SuspendedPayment",
                type: "nvarchar(23)",
                maxLength: 23,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldMaxLength: 30,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Comment",
                table: "SuspendedPayment",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PaymentId",
                table: "SuspendedPayment",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PolicyId",
                table: "SuspendedPayment",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Time",
                table: "SuspendedPayment",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SuspendedCheck",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SuspendedPaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Check = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CheckAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CheckRoutingNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CheckAccountNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CheckNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedCheck", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedCheck_SuspendedPayment_SuspendedPaymentId",
                        column: x => x.SuspendedPaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SuspendedPaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TransactionNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LockboxNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Batch = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BatchItem = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Number = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    EnvelopeNum = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Envelope = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    InvoicePage = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedDetail_SuspendedPayment_SuspendedPaymentId",
                        column: x => x.SuspendedPaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuspendedImage",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SuspendedPaymentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CheckImg = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    EnvelopeImg = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    InvoiceImg = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SuspendedImage_SuspendedPayment_SuspendedPaymentId",
                        column: x => x.SuspendedPaymentId,
                        principalTable: "SuspendedPayment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_PaymentId",
                table: "SuspendedPayment",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedCheck_SuspendedPaymentId",
                table: "SuspendedCheck",
                column: "SuspendedPaymentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedDetail_SuspendedPaymentId",
                table: "SuspendedDetail",
                column: "SuspendedPaymentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedImage_SuspendedPaymentId",
                table: "SuspendedImage",
                column: "SuspendedPaymentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SuspendedPayment_Payment_PaymentId",
                table: "SuspendedPayment",
                column: "PaymentId",
                principalTable: "Payment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuspendedPayment_Payment_PaymentId",
                table: "SuspendedPayment");

            migrationBuilder.DropTable(
                name: "SuspendedCheck");

            migrationBuilder.DropTable(
                name: "SuspendedDetail");

            migrationBuilder.DropTable(
                name: "SuspendedImage");

            migrationBuilder.DropIndex(
                name: "IX_SuspendedPayment_PaymentId",
                table: "SuspendedPayment");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "SuspendedPayment");

            migrationBuilder.DropColumn(
                name: "PolicyId",
                table: "SuspendedPayment");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "SuspendedPayment");

            migrationBuilder.AlterColumn<string>(
                name: "PolicyNumber",
                table: "SuspendedPayment",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(23)",
                oldMaxLength: 23,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Comment",
                table: "SuspendedPayment",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PayerId",
                table: "SuspendedPayment",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "SuspendedPayer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    City = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    State = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspendedPayer", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SuspendedPayment_PayerId",
                table: "SuspendedPayment",
                column: "PayerId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SuspendedPayment_SuspendedPayer_PayerId",
                table: "SuspendedPayment",
                column: "PayerId",
                principalTable: "SuspendedPayer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
