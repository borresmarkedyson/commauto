﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class AddCol_Risk_CancellationEffectiveDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CancellationEffectiveDate",
                table: "Risk",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CancellationEffectiveDate",
                table: "Risk");
        }
    }
}
