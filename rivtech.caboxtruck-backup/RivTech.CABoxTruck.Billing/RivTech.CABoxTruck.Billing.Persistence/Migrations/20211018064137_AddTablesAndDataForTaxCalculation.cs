﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class AddTablesAndDataForTaxCalculation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeeDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StateCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FeeKindId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FeeKind",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeeKind", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NonTaxableFee",
                columns: table => new
                {
                    StateCode = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    FeeKindId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NonTaxableFee", x => new { x.StateCode, x.FeeKindId, x.EffectiveDate });
                });

            migrationBuilder.CreateTable(
                name: "StateTax",
                columns: table => new
                {
                    StateCode = table.Column<string>(type: "varchar(3)", maxLength: 3, nullable: false),
                    TaxType = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    TaxSubtype = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EffectiveDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Rate = table.Column<decimal>(type: "decimal(8,3)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateTax", x => new { x.StateCode, x.TaxType, x.TaxSubtype, x.EffectiveDate });
                });

            migrationBuilder.Sql(INSERT_TAX_CALCULATION_DATA);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeeDetails");

            migrationBuilder.DropTable(
                name: "FeeKind");

            migrationBuilder.DropTable(
                name: "NonTaxableFee");

            migrationBuilder.DropTable(
                name: "StateTax");

            migrationBuilder.Sql(DELETE_TAX_CALCULATION_DATA);
        }

        const string INSERT_TAX_CALCULATION_DATA = @"
            SET IDENTITY_INSERT [dbo].[FeeDetails] ON 
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (1, N'NJ', N'RMFAL', N'Service Fee - Multiple Units (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (2, N'NJ', N'RMFAL', N'Service Fee - Single Units (AL)', CAST(200.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (3, N'NJ', N'RMFAL', N'Service Fee - Midterm (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (4, N'NJ', N'RMFPD', N'Service Fee - Multiple Units (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (5, N'NJ', N'RMFPD', N'Service Fee - Single Units (PD)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (6, N'NJ', N'RMFPD', N'Service Fee - Midterm (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (7, N'NJ', N'REINF', N'Reinstatement Fee', CAST(250.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (8, N'CA', N'RMFAL', N'Service Fee - Multiple Units (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (9, N'CA', N'RMFAL', N'Service Fee - Single Units (AL)', CAST(200.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (10, N'CA', N'RMFAL', N'Service Fee - Midterm (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (11, N'CA', N'RMFPD', N'Service Fee - Multiple Units (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (12, N'CA', N'RMFPD', N'Service Fee - Single Units (PD)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (13, N'CA', N'RMFPD', N'Service Fee - Midterm (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (14, N'CA', N'REINF', N'Reinstatement Fee', CAST(250.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (15, N'TX', N'RMFAL', N'Service Fee - Multiple Units (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (16, N'TX', N'RMFAL', N'Service Fee - Single Units (AL)', CAST(200.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (17, N'TX', N'RMFAL', N'Service Fee - Midterm (AL)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (18, N'TX', N'RMFPD', N'Service Fee - Multiple Units (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (19, N'TX', N'RMFPD', N'Service Fee - Single Units (PD)', CAST(100.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (20, N'TX', N'RMFPD', N'Service Fee - Midterm (PD)', CAST(50.00 AS Decimal(12, 2)), 1)
            GO
            INSERT [dbo].[FeeDetails] ([Id], [StateCode], [FeeKindId], [Description], [Amount], [IsActive]) VALUES (21, N'TX', N'REINF', N'Reinstatement Fee', CAST(250.00 AS Decimal(12, 2)), 1)
            GO
            SET IDENTITY_INSERT [dbo].[FeeDetails] OFF
            GO
            INSERT [dbo].[FeeKind] ([Id], [Description], [IsActive]) VALUES (N'INSTF', N'Installment Fee', 1)
            GO
            INSERT [dbo].[FeeKind] ([Id], [Description], [IsActive]) VALUES (N'RMFAL', N'Risk Management Fee (AL)', 1)
            GO
            INSERT [dbo].[FeeKind] ([Id], [Description], [IsActive]) VALUES (N'RMFPD', N'Risk Management Fee (PD)', 1)
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'CA', N'INSTF', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'CA', N'REINSTAT', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'CA', N'RMFAL', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'CA', N'RMFPD', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'NJ', N'INSTF', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'NJ', N'REINSTAT', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'NJ', N'RMFAL', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[NonTaxableFee] ([StateCode], [FeeKindId], [EffectiveDate]) VALUES (N'NJ', N'RMFPD', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2))
            GO
            INSERT [dbo].[StateTax] ([StateCode], [TaxType], [TaxSubtype], [EffectiveDate], [Rate]) VALUES (N'CA', N'SF', N'P', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(0.250 AS Decimal(8, 3)))
            GO
            INSERT [dbo].[StateTax] ([StateCode], [TaxType], [TaxSubtype], [EffectiveDate], [Rate]) VALUES (N'CA', N'SL', N'Main', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(3.000 AS Decimal(8, 3)))
            GO
            INSERT [dbo].[StateTax] ([StateCode], [TaxType], [TaxSubtype], [EffectiveDate], [Rate]) VALUES (N'NJ', N'SL', N'Main', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(5.000 AS Decimal(8, 3)))
            GO
            INSERT [dbo].[StateTax] ([StateCode], [TaxType], [TaxSubtype], [EffectiveDate], [Rate]) VALUES (N'TX', N'SF', N'P', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(0.075 AS Decimal(8, 3)))
            GO
            INSERT [dbo].[StateTax] ([StateCode], [TaxType], [TaxSubtype], [EffectiveDate], [Rate]) VALUES (N'TX', N'SL', N'Main', CAST(N'2021-01-01T00:00:00.0000000' AS DateTime2), CAST(4.850 AS Decimal(8, 3)))
            GO

        ";

        const string DELETE_TAX_CALCULATION_DATA = @"
            delete dbo.FeeDetails;
            delete dbo.FeeKind;
            delete dbo.NonTaxableFee;
            delete dbo.StateTax;
        ";
    }
}
