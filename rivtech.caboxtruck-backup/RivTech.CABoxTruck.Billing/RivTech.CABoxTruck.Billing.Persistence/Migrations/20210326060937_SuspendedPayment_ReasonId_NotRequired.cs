﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class SuspendedPayment_ReasonId_NotRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuspendedPayment_SuspendedReason_ReasonId",
                table: "SuspendedPayment");

            migrationBuilder.AlterColumn<string>(
                name: "ReasonId",
                table: "SuspendedPayment",
                type: "nvarchar(10)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)");

            migrationBuilder.AddForeignKey(
                name: "FK_SuspendedPayment_SuspendedReason_ReasonId",
                table: "SuspendedPayment",
                column: "ReasonId",
                principalTable: "SuspendedReason",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SuspendedPayment_SuspendedReason_ReasonId",
                table: "SuspendedPayment");

            migrationBuilder.AlterColumn<string>(
                name: "ReasonId",
                table: "SuspendedPayment",
                type: "nvarchar(10)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SuspendedPayment_SuspendedReason_ReasonId",
                table: "SuspendedPayment",
                column: "ReasonId",
                principalTable: "SuspendedReason",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
