﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class Rename_EnumTables_Singular : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AmountSubTypes_AmountTypes_AmountTypeId",
                table: "AmountSubTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_Installment_InstallmentTypes_InstallmentTypeId",
                table: "Installment");

            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_AmountSubTypes_AmountSubTypeId",
                table: "InvoiceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_Risk_PaymentPlans_PaymentPlanId",
                table: "Risk");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_TransactionTypes_TransactionTypeId",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_AmountSubTypes_AmountSubTypeId",
                table: "TransactionDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionTypes",
                table: "TransactionTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentPlans",
                table: "PaymentPlans");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstallmentTypes",
                table: "InstallmentTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AmountSubTypes",
                table: "AmountSubTypes");

            migrationBuilder.RenameTable(
                name: "TransactionTypes",
                newName: "TransactionType");

            migrationBuilder.RenameTable(
                name: "PaymentPlans",
                newName: "PaymentPlan");

            migrationBuilder.RenameTable(
                name: "InstallmentTypes",
                newName: "InstallmentType");

            migrationBuilder.RenameTable(
                name: "AmountSubTypes",
                newName: "AmountSubType");

            migrationBuilder.RenameIndex(
                name: "IX_AmountSubTypes_AmountTypeId",
                table: "AmountSubType",
                newName: "IX_AmountSubType_AmountTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionType",
                table: "TransactionType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentPlan",
                table: "PaymentPlan",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstallmentType",
                table: "InstallmentType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AmountSubType",
                table: "AmountSubType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AmountSubType_AmountType_AmountTypeId",
                table: "AmountSubType",
                column: "AmountTypeId",
                principalTable: "AmountType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_InstallmentType_InstallmentTypeId",
                table: "Installment",
                column: "InstallmentTypeId",
                principalTable: "InstallmentType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_AmountSubType_AmountSubTypeId",
                table: "InvoiceDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_PaymentPlan_PaymentPlanId",
                table: "Risk",
                column: "PaymentPlanId",
                principalTable: "PaymentPlan",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_TransactionType_TransactionTypeId",
                table: "Transaction",
                column: "TransactionTypeId",
                principalTable: "TransactionType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_AmountSubType_AmountSubTypeId",
                table: "TransactionDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.DropTable(name: "AmountTypes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AmountSubType_AmountType_AmountTypeId",
                table: "AmountSubType");

            migrationBuilder.DropForeignKey(
                name: "FK_Installment_InstallmentType_InstallmentTypeId",
                table: "Installment");

            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_AmountSubType_AmountSubTypeId",
                table: "InvoiceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_Risk_PaymentPlan_PaymentPlanId",
                table: "Risk");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_TransactionType_TransactionTypeId",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_AmountSubType_AmountSubTypeId",
                table: "TransactionDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionType",
                table: "TransactionType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PaymentPlan",
                table: "PaymentPlan");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstallmentType",
                table: "InstallmentType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AmountSubType",
                table: "AmountSubType");

            migrationBuilder.RenameTable(
                name: "TransactionType",
                newName: "TransactionTypes");

            migrationBuilder.RenameTable(
                name: "PaymentPlan",
                newName: "PaymentPlans");

            migrationBuilder.RenameTable(
                name: "InstallmentType",
                newName: "InstallmentTypes");

            migrationBuilder.RenameTable(
                name: "AmountSubType",
                newName: "AmountSubTypes");

            migrationBuilder.RenameIndex(
                name: "IX_AmountSubType_AmountTypeId",
                table: "AmountSubTypes",
                newName: "IX_AmountSubTypes_AmountTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionTypes",
                table: "TransactionTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PaymentPlans",
                table: "PaymentPlans",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstallmentTypes",
                table: "InstallmentTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AmountSubTypes",
                table: "AmountSubTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AmountSubTypes_AmountType_AmountTypeId",
                table: "AmountSubTypes",
                column: "AmountTypeId",
                principalTable: "AmountType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_InstallmentTypes_InstallmentTypeId",
                table: "Installment",
                column: "InstallmentTypeId",
                principalTable: "InstallmentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_AmountSubTypes_AmountSubTypeId",
                table: "InvoiceDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_PaymentPlans_PaymentPlanId",
                table: "Risk",
                column: "PaymentPlanId",
                principalTable: "PaymentPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_TransactionTypes_TransactionTypeId",
                table: "Transaction",
                column: "TransactionTypeId",
                principalTable: "TransactionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_AmountSubTypes_AmountSubTypeId",
                table: "TransactionDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
