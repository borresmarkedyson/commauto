﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class RemoveAmountSubTypeForeignKeyInPaymentDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.DropIndex(
                name: "IX_PaymentDetail_AmountSubTypeId",
                table: "PaymentDetail");

            migrationBuilder.AlterColumn<string>(
                name: "AmountSubTypeId",
                table: "PaymentDetail",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AmountSubTypeId",
                table: "PaymentDetail",
                type: "nvarchar(30)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDetail_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentDetail_AmountSubType_AmountSubTypeId",
                table: "PaymentDetail",
                column: "AmountSubTypeId",
                principalTable: "AmountSubType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
