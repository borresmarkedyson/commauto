﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class IncreaseAmountSubtypeLength : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(SET_AMOUNT_SUBTYPE_LENGTH_TO_30);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(SET_AMOUNT_SUBTYPE_LENGTH_TO_10);
        }

        const string SET_AMOUNT_SUBTYPE_LENGTH_TO_30 = @"
            DROP INDEX [IX_TransactionDetail_AmountSubTypeId] ON [TransactionDetail];
            DROP INDEX [IX_ConditionalFee_AmountSubTypeId] ON [ConditionalFee];
            DROP INDEX [IX_InvoiceDetail_AmountSubTypeId] ON [InvoiceDetail];	  
						
            ALTER TABLE [TransactionDetail] DROP CONSTRAINT FK_TransactionDetail_AmountSubType_AmountSubTypeId
            ALTER TABLE [ConditionalFee] DROP CONSTRAINT FK_ConditionalFee_AmountSubType_AmountSubTypeId
            ALTER TABLE [InvoiceDetail] DROP CONSTRAINT FK_InvoiceDetail_AmountSubType_AmountSubTypeId
            ALTER TABLE [AmountSubType] DROP CONSTRAINT PK_AmountSubType

            ALTER TABLE [TransactionDetail] ALTER COLUMN [AmountSubTypeId] nvarchar(30) NULL;
            ALTER TABLE [ConditionalFee] ALTER COLUMN [AmountSubTypeId] nvarchar(30) NULL;
            ALTER TABLE [InvoiceDetail] ALTER COLUMN [AmountSubTypeId] nvarchar(30) NULL;
            ALTER TABLE [AmountSubType] ALTER COLUMN [Id] nvarchar(30) NOT NULL;

            ALTER TABLE [AmountSubType] ADD CONSTRAINT PK_AmountSubType PRIMARY KEY (Id);
            ALTER TABLE [TransactionDetail] ADD CONSTRAINT FK_TransactionDetail_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);
            ALTER TABLE [ConditionalFee] ADD CONSTRAINT FK_ConditionalFee_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);
            ALTER TABLE [InvoiceDetail] ADD CONSTRAINT FK_InvoiceDetail_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);

            CREATE INDEX [IX_TransactionDetail_AmountSubTypeId] ON [TransactionDetail] ([AmountSubTypeId]);
            CREATE INDEX [IX_ConditionalFee_AmountSubTypeId] ON [ConditionalFee] ([AmountSubTypeId]);
            CREATE INDEX [IX_InvoiceDetail_AmountSubTypeId] ON [InvoiceDetail] ([AmountSubTypeId]);
        ";

        const string SET_AMOUNT_SUBTYPE_LENGTH_TO_10 = @"
            DROP INDEX [IX_TransactionDetail_AmountSubTypeId] ON [TransactionDetail];
            DROP INDEX [IX_ConditionalFee_AmountSubTypeId] ON [ConditionalFee];
            DROP INDEX [IX_InvoiceDetail_AmountSubTypeId] ON [InvoiceDetail];	  
						
            ALTER TABLE [TransactionDetail] DROP CONSTRAINT FK_TransactionDetail_AmountSubType_AmountSubTypeId
            ALTER TABLE [ConditionalFee] DROP CONSTRAINT FK_ConditionalFee_AmountSubType_AmountSubTypeId
            ALTER TABLE [InvoiceDetail] DROP CONSTRAINT FK_InvoiceDetail_AmountSubType_AmountSubTypeId
            ALTER TABLE [AmountSubType] DROP CONSTRAINT PK_AmountSubType

            ALTER TABLE [TransactionDetail] ALTER COLUMN [AmountSubTypeId] nvarchar(10) NULL;
            ALTER TABLE [ConditionalFee] ALTER COLUMN [AmountSubTypeId] nvarchar(10) NULL;
            ALTER TABLE [InvoiceDetail] ALTER COLUMN [AmountSubTypeId] nvarchar(10) NULL;
            ALTER TABLE [AmountSubType] ALTER COLUMN [Id] nvarchar(10) NOT NULL;

            ALTER TABLE [AmountSubType] ADD CONSTRAINT PK_AmountSubType PRIMARY KEY (Id);
            ALTER TABLE [TransactionDetail] ADD CONSTRAINT FK_TransactionDetail_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);
            ALTER TABLE [ConditionalFee] ADD CONSTRAINT FK_ConditionalFee_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);
            ALTER TABLE [InvoiceDetail] ADD CONSTRAINT FK_InvoiceDetail_AmountSubType_AmountSubTypeId FOREIGN KEY ([AmountSubTypeId]) REFERENCES [AmountSubType](Id);

            CREATE INDEX [IX_TransactionDetail_AmountSubTypeId] ON [TransactionDetail] ([AmountSubTypeId]);
            CREATE INDEX [IX_ConditionalFee_AmountSubTypeId] ON [ConditionalFee] ([AmountSubTypeId]);
            CREATE INDEX [IX_InvoiceDetail_AmountSubTypeId] ON [InvoiceDetail] ([AmountSubTypeId]);
        ";
    }
}
