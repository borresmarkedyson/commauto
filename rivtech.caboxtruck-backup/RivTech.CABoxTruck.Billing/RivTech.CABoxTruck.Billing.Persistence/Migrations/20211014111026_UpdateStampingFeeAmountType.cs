﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class UpdateStampingFeeAmountType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE dbo.AmountSubType 
            SET AmountTypeId = 'T'
            WHERE Id = 'SF'
        ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE dbo.AmountSubType 
            SET AmountTypeId = 'F'
            WHERE Id = 'SF'
        ");
        }
    }
}
