﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class LinkInvoiceToInstallmentAndTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Installment_RivtechApplication_RivtechApplicationId",
                table: "Installment");

            migrationBuilder.DropIndex(
                name: "IX_Installment_RivtechApplicationId",
                table: "Installment");

            migrationBuilder.DropColumn(
                name: "RivtechApplicationId",
                table: "Installment");

            migrationBuilder.AddColumn<Guid>(
                name: "InvoiceId",
                table: "Transaction",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationId",
                table: "Risk",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "InvoiceId",
                table: "Installment",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_InvoiceId",
                table: "Transaction",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Risk_ApplicationId",
                table: "Risk",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Installment_InvoiceId",
                table: "Installment",
                column: "InvoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_Invoice_InvoiceId",
                table: "Installment",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Risk_RivtechApplication_ApplicationId",
                table: "Risk",
                column: "ApplicationId",
                principalTable: "RivtechApplication",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Invoice_InvoiceId",
                table: "Transaction",
                column: "InvoiceId",
                principalTable: "Invoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Installment_Invoice_InvoiceId",
                table: "Installment");

            migrationBuilder.DropForeignKey(
                name: "FK_Risk_RivtechApplication_ApplicationId",
                table: "Risk");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Invoice_InvoiceId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_InvoiceId",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Risk_ApplicationId",
                table: "Risk");

            migrationBuilder.DropIndex(
                name: "IX_Installment_InvoiceId",
                table: "Installment");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "ApplicationId",
                table: "Risk");

            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "Installment");

            migrationBuilder.AddColumn<string>(
                name: "RivtechApplicationId",
                table: "Installment",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Installment_RivtechApplicationId",
                table: "Installment",
                column: "RivtechApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Installment_RivtechApplication_RivtechApplicationId",
                table: "Installment",
                column: "RivtechApplicationId",
                principalTable: "RivtechApplication",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
