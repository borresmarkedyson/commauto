﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class TransactionDetailVoiding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_ApplicationUser_VoidedById",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_Transaction_Transaction_VoidedTransactionId",
                table: "Transaction");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_Transaction_TransactionId",
                table: "TransactionDetail");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_VoidedById",
                table: "Transaction");

            migrationBuilder.DropIndex(
                name: "IX_Transaction_VoidedTransactionId",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "VoidDate",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "VoidedById",
                table: "Transaction");

            migrationBuilder.DropColumn(
                name: "VoidedTransactionId",
                table: "Transaction");

            migrationBuilder.AlterColumn<Guid>(
                name: "TransactionId",
                table: "TransactionDetail",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "VoidDate",
                table: "TransactionDetail",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VoidedById",
                table: "TransactionDetail",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "VoidedTransactionDetailId",
                table: "TransactionDetail",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDetail_VoidedById",
                table: "TransactionDetail",
                column: "VoidedById");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDetail_VoidedTransactionDetailId",
                table: "TransactionDetail",
                column: "VoidedTransactionDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_ApplicationUser_VoidedById",
                table: "TransactionDetail",
                column: "VoidedById",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_Transaction_TransactionId",
                table: "TransactionDetail",
                column: "TransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_TransactionDetail_VoidedTransactionDetailId",
                table: "TransactionDetail",
                column: "VoidedTransactionDetailId",
                principalTable: "TransactionDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_ApplicationUser_VoidedById",
                table: "TransactionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_Transaction_TransactionId",
                table: "TransactionDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDetail_TransactionDetail_VoidedTransactionDetailId",
                table: "TransactionDetail");

            migrationBuilder.DropIndex(
                name: "IX_TransactionDetail_VoidedById",
                table: "TransactionDetail");

            migrationBuilder.DropIndex(
                name: "IX_TransactionDetail_VoidedTransactionDetailId",
                table: "TransactionDetail");

            migrationBuilder.DropColumn(
                name: "VoidDate",
                table: "TransactionDetail");

            migrationBuilder.DropColumn(
                name: "VoidedById",
                table: "TransactionDetail");

            migrationBuilder.DropColumn(
                name: "VoidedTransactionDetailId",
                table: "TransactionDetail");

            migrationBuilder.AlterColumn<Guid>(
                name: "TransactionId",
                table: "TransactionDetail",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<DateTime>(
                name: "VoidDate",
                table: "Transaction",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VoidedById",
                table: "Transaction",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "VoidedTransactionId",
                table: "Transaction",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_VoidedById",
                table: "Transaction",
                column: "VoidedById");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_VoidedTransactionId",
                table: "Transaction",
                column: "VoidedTransactionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_ApplicationUser_VoidedById",
                table: "Transaction",
                column: "VoidedById",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transaction_Transaction_VoidedTransactionId",
                table: "Transaction",
                column: "VoidedTransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDetail_Transaction_TransactionId",
                table: "TransactionDetail",
                column: "TransactionId",
                principalTable: "Transaction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
