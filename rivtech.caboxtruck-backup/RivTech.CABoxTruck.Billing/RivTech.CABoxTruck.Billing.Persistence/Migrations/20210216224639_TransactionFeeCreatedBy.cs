﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RivTech.CABoxTruck.Billing.Persistence.Migrations
{
    public partial class TransactionFeeCreatedBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedById",
                table: "TransactionFee",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionFee_CreatedById",
                table: "TransactionFee",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionFee_ApplicationUser_CreatedById",
                table: "TransactionFee",
                column: "CreatedById",
                principalTable: "ApplicationUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionFee_ApplicationUser_CreatedById",
                table: "TransactionFee");

            migrationBuilder.DropIndex(
                name: "IX_TransactionFee_CreatedById",
                table: "TransactionFee");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "TransactionFee");
        }
    }
}
