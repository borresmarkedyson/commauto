﻿using RivTech.CABoxTruck.Billing.Infrastructure.Encryption;
using System;
using System.IO;

namespace EncryptionKeyGenerator
{
    public class Program
    {
        static void Main(string[] args)
        {
            bool isDirectoryValid = false;
            FileEncryptionService fileEncryptionService = new FileEncryptionService();
            string outputDirectory = string.Empty;
            while (!isDirectoryValid)
            {
                Console.WriteLine("Please enter the folder where keys will be generated:");
                outputDirectory = Console.ReadLine();
                try
                {
                    Directory.CreateDirectory(outputDirectory);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                isDirectoryValid = Directory.Exists(outputDirectory);
            }

            fileEncryptionService.GenerateKeys();
            fileEncryptionService.ExportPublicKey(outputDirectory);
        }
    }
}
