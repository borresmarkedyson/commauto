﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IProfileRequestStrategyFactory
    {
        IProfileRequestStrategy GetRequestStrategy(string appId);
    }
}
