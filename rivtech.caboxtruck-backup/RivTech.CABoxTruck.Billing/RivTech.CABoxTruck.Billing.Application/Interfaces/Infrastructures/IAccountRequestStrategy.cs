﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IAccountRequestStrategy
    {
        dynamic GenerateCreateRequest(PaymentAccountRequestDto accountRequest, dynamic providerPaymentProfile, IMapper mapper);
        dynamic GenerateGetRequest(dynamic providerPaymentProfile, UpdatePaymentAccountDto updateRequest = null);
        dynamic GenerateDeleteRequest(PaymentAccountRequestDto accountRequest, dynamic providerPaymentProfile);
        dynamic GenerateUpdateRequest(UpdatePaymentAccountDto updateRequest, dynamic providerProfile);
    }
}
