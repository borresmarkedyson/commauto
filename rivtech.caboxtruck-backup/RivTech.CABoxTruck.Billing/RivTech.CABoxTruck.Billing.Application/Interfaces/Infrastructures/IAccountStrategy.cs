﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IAccountStrategy
    {
        dynamic CreateAccount(dynamic paymentAccountRequest);
        dynamic GetAccount(dynamic paymentAccountRequest);
        dynamic DeleteAccount(dynamic paymentAccountRequest);
        dynamic UpdateAccount(dynamic request);
    }
}
