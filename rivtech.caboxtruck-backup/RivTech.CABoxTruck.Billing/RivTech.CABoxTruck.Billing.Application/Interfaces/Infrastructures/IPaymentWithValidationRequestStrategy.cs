﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentWithValidationRequestStrategy : IPaymentRequestStrategy
    {
        dynamic CreateAuthorizeAmountRequest(PaymentRequestDto paymentRequest, IMapper mapper);
        dynamic CreateCaptureAuthorizedAmountRequest(decimal amount, string transactionId);
    }
}
