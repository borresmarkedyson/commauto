﻿using RivTech.CABoxTruck.Billing.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IDomainEventDispatcher
    {
        void Dispatch<TEvent>(TEvent domainEvent) where TEvent : BaseDomainEvent; 
        Task DispatchAsync<TEvent>(TEvent domainEvent) where TEvent : BaseDomainEvent; 
    }
}
