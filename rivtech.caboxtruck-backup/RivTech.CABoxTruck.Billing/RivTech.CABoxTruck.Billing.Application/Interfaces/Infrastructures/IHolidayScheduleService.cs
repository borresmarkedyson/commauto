﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IHolidayScheduleService
    {
        public Task<List<HolidayScheduleDto>> GetHolidaySchedule();
    }
}
