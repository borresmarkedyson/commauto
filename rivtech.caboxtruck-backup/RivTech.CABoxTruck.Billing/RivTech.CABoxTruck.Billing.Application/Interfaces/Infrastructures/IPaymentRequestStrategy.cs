﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentRequestStrategy
    {
        dynamic GeneratePostRequest(PaymentRequestDto paymentRequest, IMapper mapper);
        dynamic GeneratePostPaymentProfileRequest(dynamic providerProfile, decimal amount);
    }
}
