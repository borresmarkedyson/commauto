﻿
namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentStrategyFactory
    {
        IPaymentStrategy GetStrategy(string appId);
    }
}
