﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IAccountFactory
    {
        IAccountStrategy GetStrategy(string programId);
    }
}
