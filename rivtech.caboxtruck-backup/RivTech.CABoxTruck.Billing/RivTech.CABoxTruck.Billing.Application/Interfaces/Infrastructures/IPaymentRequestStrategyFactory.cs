﻿
namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentRequestStrategyFactory
    {
        IPaymentRequestStrategy GetRequestStrategy(string appId);
    }
}
