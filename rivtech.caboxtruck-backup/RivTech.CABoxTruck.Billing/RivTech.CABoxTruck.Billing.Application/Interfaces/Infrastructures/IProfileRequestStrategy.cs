﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IProfileRequestStrategy
    {
        dynamic GenerateCreateRequest(PaymentProfileRequestDto paymentRequest, IMapper mapper);
        dynamic GenerateUpdateRequest(PaymentProfileRequestDto profileRequest, dynamic providerPaymentProfile, IMapper mapper);
        dynamic GenerateDeleteRequest(dynamic providerPaymentProfile);
    }
}
