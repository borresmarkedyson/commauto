﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IProfileStrategyFactory
    {
        IProfileStrategy GetStrategy(string appId);
    }
}
