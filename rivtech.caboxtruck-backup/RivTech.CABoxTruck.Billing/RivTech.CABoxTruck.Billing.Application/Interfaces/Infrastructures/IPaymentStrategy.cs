﻿
using RivTech.CABoxTruck.Billing.Application.DTO;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentStrategy
    {
        PaymentProviderResponseDto PostPayment(dynamic paymentRequest);
        string PostPaymentProfile(dynamic paymentRequest);
    }
}
