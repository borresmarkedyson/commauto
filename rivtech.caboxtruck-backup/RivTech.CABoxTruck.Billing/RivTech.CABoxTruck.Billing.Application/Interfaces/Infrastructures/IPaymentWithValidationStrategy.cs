﻿using RivTech.CABoxTruck.Billing.Application.DTO;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentWithValidationStrategy : IPaymentStrategy
    {
        string AuthorizePayment(dynamic paymentRequest);
        PaymentProviderResponseDto CaptureAuthorizedPayment(dynamic paymentRequest);
    }
}
