﻿using System.IO;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IFileEncryptionService
    {
        public void GenerateKeys();
        public void ExportPublicKey(string outputDirectory);
        public void ImportPublicKey(string keyFileName);
        public void ImportPublicKeyFromString(string xmlString);
        public void GetPrivateKey();
        public void EncryptFile(string inputFile, string outputDirectory);
        public void DecryptFile(string encryptedFile, string outputDirectory);
        public void EncryptStream(MemoryStream inputStream, string encryptedFileName);
    }
}
