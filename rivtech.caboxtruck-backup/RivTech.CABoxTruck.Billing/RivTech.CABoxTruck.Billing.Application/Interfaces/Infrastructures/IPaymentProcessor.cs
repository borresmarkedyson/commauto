﻿using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IPaymentProcessor
    {
        Task<PaymentResult> PayAsync(CustomerDetails customer, InvoiceDetails invoice, PaymentCCEFT payment);
        //void Void(Payment payment);
    }
}
