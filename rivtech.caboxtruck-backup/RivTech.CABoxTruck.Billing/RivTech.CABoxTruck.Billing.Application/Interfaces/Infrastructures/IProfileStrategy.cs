﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures
{
    public interface IProfileStrategy
    {
        dynamic CreateProfile(dynamic profileRequest);
        dynamic UpdateProfile(dynamic profileRequest);
        dynamic DeleteProfile(dynamic profileRequest);
    }
}
