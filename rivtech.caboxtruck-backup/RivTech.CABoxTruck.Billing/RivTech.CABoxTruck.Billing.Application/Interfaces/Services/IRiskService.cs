﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IRiskService
    {
        Task<RiskDto> UpdateRisk(UpdateRiskRequestDto request);
        Task<List<Guid>> GetAllRiskForNoticeCancellation(DateTime requestDate);
    }
}
