﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IInstallmentScheduleService
    {
        Task<List<InstallmentDto>> GetAllAsync(Guid riskId);
        Task<List<InstallmentDto>> SaveInstallmentScheduleAsync(Guid riskId, string policyNumber, DateTime startDate, string paymentPlan,
            string appId, bool isPaymentPlanChang, decimal? percentOfOutstanding);
        List<Installment> GenerateInstallmentScheduleWithoutSaving(Guid riskId, DateTime startDate, string paymentPlan,
           string appId, bool isPaymentPlanChange, decimal? percentOfOutstanding);
        Task<List<InstallmentAndInvoiceDto>> GenerateInstallmentScheduleBeforeBind(List<TransactionDetailDto> transactionDetails,
                                DateTime effectiveDate, string stateCode, decimal percentOfOutstanding, string paymentPlan, string appId);
        Task<InstallmentDto> AddEditFutureInstallmentScheduleAsync(Guid riskId, Guid? installmentId, DateTime invoiceDate, DateTime dueDate);
    }
}
