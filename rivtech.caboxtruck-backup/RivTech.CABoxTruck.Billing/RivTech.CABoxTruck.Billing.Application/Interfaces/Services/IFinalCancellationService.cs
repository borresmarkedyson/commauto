﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IFinalCancellationService
    {
        Task<List<FinalCancellationRiskDto>> GetFinalCancellationRisks(string appId);
        Task<FinalCancellationRiskDto> SetFinalCancellation(NoticeRequestDto request);
    }
}
