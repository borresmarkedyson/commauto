﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IAuthNetAccountService
    {
        Task<AuthNetProfileDto> SaveAuthNetAccount(Guid riskId, Guid paymentAccountId, string customerPaymentProfileId);
        Task<AuthNetAccountDto> DeleteAuthNetAccount(Guid paymentAccountId);
    }
}
