﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IInvoiceNumberService
    {
        Task<string> GenerateInvoiceNumber(string appId, string policyNumber);
    }
}
