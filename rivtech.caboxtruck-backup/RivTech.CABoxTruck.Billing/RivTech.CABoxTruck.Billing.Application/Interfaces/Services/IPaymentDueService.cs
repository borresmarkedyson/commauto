﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentDueService
    {
        public Task<PaymentDueRisksDto> GetPaymentDueRisks(string appId);
        public Task<PaymentDueRisksDto> GetPaymentPastDueRisks(string appId);
    }
}
