﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentProfileService
    {
        Task<PaymentProfileDto> UpdateProfile(PaymentProfileRequestDto profileRequest);
        Task<PaymentProfileDto> DeleteProfile(Guid riskId);
        Task<PaymentProfileDto> GetProfile(Guid riskId);
        Task<PaymentProfileDto> CreateProfileOnBind(Guid riskId, BindRequestDto bindRequest);
        Task<PaymentProfileDto> CreateProfileOnPayment(PaymentRequestDto paymentRequest);
        Task<PaymentProfileDto> CreateProfileOnEnroll(PaymentProfileRequestDto profileRequest);
        PaymentProfile GetEnrolledPaymentProfile(Guid riskId);
    }
}
