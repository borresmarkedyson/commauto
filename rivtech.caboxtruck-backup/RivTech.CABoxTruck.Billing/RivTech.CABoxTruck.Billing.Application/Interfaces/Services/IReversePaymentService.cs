﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IReversePaymentService
    {
        Task<FeeInvoiceDto> ReversePayment(ReversePaymentRequestDto request);
    }
}
