﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IDepositAmountService
    {
        Task<decimal> ComputeDeposit(Guid riskId, string appId);
    }
}
