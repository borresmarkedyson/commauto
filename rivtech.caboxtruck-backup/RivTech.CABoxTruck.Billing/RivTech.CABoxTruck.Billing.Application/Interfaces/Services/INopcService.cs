﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface INopcService
    {
        Task<List<NopcRiskDto>> GetNopcRisks(string appId);
        Task<List<NopcRiskDto>> GetNopcRisksToRescind(string appId);
        Task<NopcRiskDto> SetNopc(NoticeRequestDto request);
        Task<NopcRiskDto> RescindNopcRisk(NoticeRequestDto request);
    }
}
