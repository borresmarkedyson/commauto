﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IRefundRequestService
    {
        List<RefundRequestRiskViewDto> GetRiskListWithRefundRequest(string applicationId);
        Task<List<RefundRequestViewDto>> AutoCheckRefundRequest(AutoCheckRefundRequestDto requests);
        Task<RefundReqDto> CreateRefundRequest(CreateRefundRequestDto request);
        Task<RefundReqDto> UpdateRefundRequest(UpdateRefundRequestDto request);
        Task<RefundReqDto> DeleteRefundRequest(Guid refundRequestId);
    }
}
