﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IReinstatementService
    {
        Task<List<ReinstatementRuleView>> GetRisksForReinstatement(GetRisksForReinstatementRequestDto request);
        Task<ReinstatementRuleView> GetRiskForReinstatement(Guid riskId);
        Task<RiskDto> Reinstate(ReinstateRequestDto request);
        Task<RisksTaggedForManualReinstatementDto> TagForManualReinstatement(TagForManualReinstatementRequestDto request);
    }
}
