﻿using AuthorizeNet.Api.Contracts.V1;
using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IAuthNetProfileService
    {
        Task<AuthNetProfileDto> SaveAuthNetProfile(Guid riskId, Guid paymentAccountId, createCustomerProfileResponse response);
        Task<AuthNetProfileDto> DeleteAuthNetProfile(Guid riskId);
    }
}
