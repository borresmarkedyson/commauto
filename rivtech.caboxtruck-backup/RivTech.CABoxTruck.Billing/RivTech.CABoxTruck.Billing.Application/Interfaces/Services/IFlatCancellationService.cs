﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IFlatCancellationService
    {
        Task<List<FlatCancelNopcRiskDto>> GetFlatCancelNopcRisks(string appId);
        Task<FlatCancelNopcRiskDto> SetFlatCancelNopc(NoticeRequestDto request);
    }
}
