﻿using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ICheckNumberService
    {
        public Task<int> GenerateCheckNumber(string appId);
    }
}
