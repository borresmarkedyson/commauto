﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ICancelledRiskPaymentService
    {
        Task<SuspendedPaymentDto> PayToReinstate(PaymentRequestDto paymentRequest);
        Task<PostPaymentViewDto> PayBalance(PaymentRequestDto paymentRequest);
    }
}
