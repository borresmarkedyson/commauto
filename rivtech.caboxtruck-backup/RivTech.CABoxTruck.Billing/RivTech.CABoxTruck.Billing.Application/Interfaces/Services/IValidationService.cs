﻿using FluentValidation.Results;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IValidationService
    {
        void FormatValidationErrorsAndThrow(ValidationResult validationResult);
    }
}
