﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IBillingSummaryService
    {
        Task<List<InstallmentAndInvoiceDto>> GetInstallmentsAndInvoices(Guid riskId);
        Task<BillingSummaryDto> GetBillingSummary(Guid riskId);
    }
}
