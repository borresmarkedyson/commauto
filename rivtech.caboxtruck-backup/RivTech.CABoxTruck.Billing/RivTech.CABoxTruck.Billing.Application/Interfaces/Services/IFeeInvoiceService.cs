﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IFeeInvoiceService
    {
        Task<Invoice> InvoiceFee(Guid riskId, decimal amount, AmountSubType amountSubType, string appId);
    }
}
