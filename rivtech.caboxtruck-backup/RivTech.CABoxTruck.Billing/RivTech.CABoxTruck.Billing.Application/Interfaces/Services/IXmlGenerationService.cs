﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Collections.Generic;
using System.Xml;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IXmlGenerationService
    {
        public XmlDocument GenerateXmlDocument(List<KeyValuePairDto> keyValuePairs, string bodyName);
    }
}
