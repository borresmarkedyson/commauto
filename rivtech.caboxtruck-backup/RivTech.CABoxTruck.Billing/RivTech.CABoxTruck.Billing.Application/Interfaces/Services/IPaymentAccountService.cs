﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentAccountService
    {
        Task<PaymentAccountDto> CreateAccount(PaymentAccountRequestDto accountRequest);
        Task<PaymentAccountDto> UpdateDefaultAccount(UpdateDefaultAccountRequestDto accountRequest);
        Task<PaymentAccountDto> UpdateAccount(UpdatePaymentAccountDto updateRequest);
        Task<PaymentAccountDto> DeleteAccount(PaymentAccountRequestDto accountRequest);
        Task<List<PaymentAccountDto>> GetAccounts(Guid riskId);
        Task<PaymentAccountDto> SetAccountDescription(PaymentAccountDto updatedAccountDto, dynamic providerProfile, PaymentAccountRequestDto accountRequest);
    }
}
