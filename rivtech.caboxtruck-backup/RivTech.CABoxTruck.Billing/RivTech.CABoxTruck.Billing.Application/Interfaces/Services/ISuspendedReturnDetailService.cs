﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ISuspendedReturnDetailService
    {
        Task<SuspendedReturnDetailDto> CreateReturnDetail(ReturnSuspendedPaymentRequestDto request);
        Task<SuspendedReturnDetailDto> DeleteReturnDetail(Guid suspendedPaymentId);
        Task<SuspendedReturnDetailDto> UpdateReturnDetail(UpdateReturnSuspendedPaymentRequestDto request);
    }
}
