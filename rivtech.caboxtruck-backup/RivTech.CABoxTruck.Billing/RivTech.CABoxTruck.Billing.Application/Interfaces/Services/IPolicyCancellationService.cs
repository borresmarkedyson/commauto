﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPolicyCancellationService
    {
        public Task<PolicyCancellationInvoiceDto> ProrateAndCancelPolicy(PolicyCancellationRequestDto request);
        public Task<NopcRiskDto> SetPendingCancellation(PendingCancellationRequestDto request);
    }
}
