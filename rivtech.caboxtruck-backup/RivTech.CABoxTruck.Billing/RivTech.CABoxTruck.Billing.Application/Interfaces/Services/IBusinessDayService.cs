﻿using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IBusinessDayService
    {
        public Task<bool> CheckIfBusinessDay(DateTime date);
    }
}
