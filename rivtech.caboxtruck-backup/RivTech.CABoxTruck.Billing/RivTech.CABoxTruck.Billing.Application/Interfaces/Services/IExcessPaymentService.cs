﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IExcessPaymentService
    {
        Task VerifyExcessPayment(PaymentRequestDto paymentRequest);
    }
}
