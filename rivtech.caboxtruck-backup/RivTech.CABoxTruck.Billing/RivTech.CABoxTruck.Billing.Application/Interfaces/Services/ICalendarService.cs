﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ICalendarService
    {
        DateTime AddBussinessDay(DateTime fromDate, int numberOfBusinessDay);
    }
}
