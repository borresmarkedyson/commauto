﻿using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ISuspendedPaymentViewService
    {
        Task<List<SuspendedPaymentViewDto>> GetAllByStatus(string statusId);
        Task<SuspendedPaymentDetailViewDto> GetDetailView(Guid suspendedPaymentId);
        Task<List<ReturnSuspendedViewDto>> GetReturnedSuspendedPayment();
    }
}
