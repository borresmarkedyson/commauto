﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ITransferPaymentService
    {
        Task<TransferPaymentDto> TransferPayment(TransferPaymentRequestDto request);
        Task<List<TransferPaymentDto>> RewriteTransferPayment(Guid currentRiskId, Guid rewriteRiskId);
    }
}
