﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ISuspendedPaymentService
    {
        Task<SuspendedPaymentDto> CreateSuspendedPayment(CreateSuspendedPaymentRequestDto suspendedPaymentRequest);
        Task<SuspendedPaymentDto> UpdateSuspendedPayment(UpdateSuspendedPaymentRequestDto request);
        Task<SuspendedPaymentDto> PostSuspendedPayment(PostSuspendedPaymentRequestDto request);
        Task<SuspendedPaymentDto> VoidSuspendedPayment(VoidSuspendedPaymentRequestDto request);
        Task<SuspendedPaymentDto> ReturnSuspendedPayment(ReturnSuspendedPaymentRequestDto request);
        Task<SuspendedPaymentDto> ReverseSuspendedPayment(ReverseSuspendedPaymentRequestDto request);
    }
}
