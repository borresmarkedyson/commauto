﻿using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentViewService
    {
        Task<List<PaymentViewDto>> GetPaymentView(Guid riskId);
        Task<PaymentDetailsViewDto> GetPaymentDetailsView(Guid riskId, Guid paymentId);
    }
}
