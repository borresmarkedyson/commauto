﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IProviderPaymentAccountService
    {
        dynamic SendToProviderCreateRequest(PaymentAccountRequestDto accountRequest, dynamic providerProfile);
        Task<dynamic> SaveAccount(PaymentAccountDto paymentAccount, PaymentAccountRequestDto accountRequest, dynamic providerResponse);
        string GetAccountDescription(dynamic providerProfile, PaymentAccountRequestDto accountRequest);
        Task<dynamic> SendToProviderDeleteRequest(PaymentAccountRequestDto accountRequest, dynamic providerProfile);
        Task<dynamic> DeleteProviderAccountData(PaymentAccountRequestDto accountRequest);
        dynamic SendToProviderUpdateRequest(UpdatePaymentAccountDto updateRequest, dynamic providerProfile);
    }
}
