﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ITransactionService
    {
        Task<List<TransactionDto>> GetAllAsync(Guid riskId);
        Task<string> InsertTransactionAsync(Guid riskId, TransactionDto model);
        Task<PremiumFeesAndTaxDto> GetTransactionSummary(Guid riskId);
        Task<Transaction> GenerateTransactionBeforeBind(TransactionDto model, TransactionType transactionType);
    }
}
