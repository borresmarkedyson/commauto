﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IFtpService
    {
        bool DownloadFileDynamicName(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath, string fileNamePattern);
        bool DownloadFile(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath);
        List<string> GetFileNamesInServer(FTPSettingsDto ftpSettings, string fileNamePattern);
        List<string> GetFileNamesInServer(string ftpSettingId);
        string DownloadFile(FTPDownloadRequest request);
    }
}
