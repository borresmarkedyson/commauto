﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IChangePaymentPlanService
    {
        Task<List<ChangePaymentPlanRequiredAmountDto>> ComputePaymentsToChangePaymentPlan(Guid riskId);
        Task<ChangePayPlanInvoiceDto> ChangePaymentPlan(ChangePaymentPlanRequestDto request);
    }
}
