﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IRefundService
    {
        Task<PaymentDto> PostRefund(RefundRequestDto request);
        Task<PaymentDto> SetClearDate(SetClearDateRequestDto request);
        Task<PaymentDto> SetEscheatDate(SetEscheatDateRequestDto request);
    }
}
