﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentDetailService
    {
        Task<List<PaymentDetail>> SavePaymentDetail(List<PaymentDetail> paymentDetails);
        Task<List<PaymentDetail>> CreatePaymentDetail(Guid riskId, PaymentSummaryDto payment);
        Task UpdatePaymentDetail(PaymentDetail paymentDetail);
    }
}
