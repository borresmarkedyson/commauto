﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentBreakdownService
    {
        Task<PaymentRequestDto> ComputeBreakdown(PaymentRequestDto paymentRequest);
    }
}
