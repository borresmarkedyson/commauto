﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentValidationService
    {
        Task ValidateMinimumPayment(Guid riskId, decimal paymentAmount, string appId);
        Task<AllowedPaymentRangeDto> GetAllowedPaymentRange(Guid riskId, string appId);
    }
}
