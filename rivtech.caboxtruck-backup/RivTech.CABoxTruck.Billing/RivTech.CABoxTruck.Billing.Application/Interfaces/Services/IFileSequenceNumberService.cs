﻿using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IFileSequenceNumberService
    {
        public Task<string> GenerateFileSequenceNumber(string appId);
    }
}
