﻿using Microsoft.AspNetCore.Http;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IUserDateService
    {
        Task SetUser(ApplicationUserDto user, HttpRequest request);
        ApplicationUser GetUser();
    }
}
