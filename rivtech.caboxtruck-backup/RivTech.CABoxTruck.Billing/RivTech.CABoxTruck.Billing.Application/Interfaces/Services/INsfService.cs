﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface INsfService
    {
        Task<NsfCountDto> GetNsfCount(Guid riskId);
    }
}
