﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IPaymentService
    {
        Task<PostPaymentViewDto> PostPayment(PaymentRequestDto paymentRequest);
        Task<PostPaymentViewDto> PostBasicPayment(PaymentRequestDto paymentRequest);
        PaymentProviderResponseDto SendToPaymentProvider(PaymentRequestDto paymentRequest);
        Task<PaymentDto> SavePayment(PaymentRequestDto paymentRequest, string referenceNumber, string paymentTypeid = null);
        Task<List<PaymentDto>> GetAllByRiskId(Guid riskId);
        Task<PostPaymentViewDto> ExecutePayment(PaymentRequestDto paymentRequest, string transactionId);
        string AuthorizePayment(Guid riskId, AuthorizePaymentRequestDto paymentRequest);
        Task<PaymentProviderResponseDto> PostPaymentNoSaving(PaymentRequestDto paymentRequest);
        Task<PostPaymentViewDto> PostChangePaymentPlanPayment(PaymentRequestDto paymentRequest);
        Task<PaymentResult> PayWithPostingAsync(
            CustomerDetails customer,
            InvoiceDetails invoice,
            PaymentCCEFT payment,
            string postedBy
        );
        Task RebalanceOverpayment(Guid riskId, string actionSource);
    }
}
