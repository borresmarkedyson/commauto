﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IBalanceDueService
    {
        Task<List<BalanceDueRiskDto>> GetBalanceDueRisks(string appId);
        Task<BalanceDueRiskDto> SetBalanceDue(NoticeRequestDto request);
    }
}
