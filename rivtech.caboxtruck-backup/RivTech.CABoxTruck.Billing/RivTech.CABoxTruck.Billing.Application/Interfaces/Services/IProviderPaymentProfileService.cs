﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IProviderPaymentProfileService
    {
        dynamic SendToProviderCreateRequest(PaymentProfileRequestDto profileRequest);
        Task<dynamic> SaveProfile(PaymentProfileRequestDto profileRequest, PaymentAccountDto paymentAccount, dynamic providerResponse);
        Task<dynamic> GetProfile(Guid paymentProfileId, string appId);
        Task<dynamic> GetProfileWithDefaultAccount(Guid riskId, Guid defaultpaymentAccountId, string appId);
        Task<dynamic> SendToProviderUpdateRequest(PaymentProfileRequestDto profileRequest);
        Task<dynamic> SendToProviderDeleteRequest(Guid riskId, string appId);
    }
}
