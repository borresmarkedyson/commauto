﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ISuspendedStatusHistoryService
    {
        Task CreateSuspendedHistory(SuspendedPayment suspendedPayment);
    }
}
