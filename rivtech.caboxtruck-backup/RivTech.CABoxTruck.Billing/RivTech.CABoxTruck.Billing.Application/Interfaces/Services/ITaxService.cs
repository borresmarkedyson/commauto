﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ITaxService
    {
        Task<ComputeRiskTaxResponseDto> PostCalculateRiskTaxAsync(DateTime effectiveDate, string stateCode, List<TransactionDetailDto> transactionDetails, decimal? installmentCount);
        Task<TaxDetails> CalculateRiskTaxAsync(DateTime effectiveDate, string stateCode, List<TransactionDetailDto> transactionDetails, decimal? installmentCount);

        //Task<IEnumerable<TaxDetailsData>> GetTaxDetails(List<Guid> ids);
    }
}
