﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface ISuspendedLockboxService
    {
        Task<SuspendedPaymentJobViewDto> ImportLockboxFile(PostLockboxSuspendedRequest jobRequest);
    }
}
