﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Services
{
    public interface IInvoiceService
    {
        Task<List<InvoiceDto>> GetAllAsync(Guid riskId);
        Task<InvoiceDto> GenerateInvoiceAsync(Guid riskId, GenerateInvoiceRequestDto model, bool isBind, decimal totalAmountCharged, bool willPayLater = false);
        Task<List<InvoiceNotifView>> GenerateCurrentDayInvoices(GenerateInvoiceRequestDto model);
        Task<string> VoidInvoice(string invoiceNumber, string appId, bool willVoidFees, Guid? invoiceId = null);
        Invoice GenerateInvoiceBeforeBindAsync(decimal percentOfOutstanding, Transaction transaction);
    }
}
