﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ISuspendedPaymentRepository : IRepository<SuspendedPayment, Guid>
    {
        SuspendedPayment GetSuspendedPaymentWithDetail(Guid suspendedPaymentId);
        Task<decimal> GetTotalSuspendedPayment(string policyNumber, string suspendedStatusId);
    }
}
