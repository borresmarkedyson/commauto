﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IRiskRepository : IRepository<Risk, Guid>
    {
        Task<Risk> GetDetailsAsync(Guid id);
        Task RemoveById(Guid id);
        Task<Risk> GetDetailsByPolicyNumberAsync(string policyNumber);
        Task<List<Risk>> GetRisksForCancellation(DateTime requestDate);
    }
}
