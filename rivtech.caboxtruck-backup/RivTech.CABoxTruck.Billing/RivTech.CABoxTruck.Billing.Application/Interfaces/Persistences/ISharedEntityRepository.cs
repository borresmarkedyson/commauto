﻿using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ISharedEntityRepository : IRepository<SharedEntity, string>
    {
    }
}
