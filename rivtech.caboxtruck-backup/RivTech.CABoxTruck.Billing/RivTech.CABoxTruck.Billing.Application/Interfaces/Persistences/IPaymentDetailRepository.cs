﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IPaymentDetailRepository : IRepository<PaymentDetail, Guid>
    {
        public Task<List<PaymentDetail>> GetTotalPerAmountType(Guid? riskId = null, Guid? paymentId = null);
    }
}
