﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IPaymentAccountRepository: IRepository<PaymentAccount, Guid>
    {
    }
}
