﻿using RivTech.Billing.Data.Entities;
using System;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IErrorLogRepository : IRepository<ErrorLog, Guid>
    {
    }
}

