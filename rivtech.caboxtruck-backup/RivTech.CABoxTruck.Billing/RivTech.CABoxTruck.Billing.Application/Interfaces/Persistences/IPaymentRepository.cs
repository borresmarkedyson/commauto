﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IPaymentRepository : IRepository<Payment, Guid>
    {
        Task<IEnumerable<Payment>> GetAllByRiskId(Guid riskId);
        Task<decimal> GetTotalPaymentAsync(Guid riskId);
        Task<Payment> GetDetailsAsync(Guid riskId);
        Task<Payment> GetVoidPayment(Guid voidedPaymentId);
        Task<int> GetNsfCount(Guid riskId);
    }
}
