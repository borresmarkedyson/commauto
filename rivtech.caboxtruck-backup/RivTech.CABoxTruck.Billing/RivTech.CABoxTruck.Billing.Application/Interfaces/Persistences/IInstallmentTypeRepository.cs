﻿using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IInstallmentTypeRepository : IRepository<InstallmentType, string>
    {
    }
}
