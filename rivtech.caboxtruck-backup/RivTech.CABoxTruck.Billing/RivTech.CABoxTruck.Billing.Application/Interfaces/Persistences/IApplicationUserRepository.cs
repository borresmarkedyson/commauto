﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser, string>
    {
        Task AddUserIfNotExisting(string id, string name);
    }
}
