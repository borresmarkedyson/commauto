﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IPaymentProfileRepository: IRepository<PaymentProfile, Guid>
    {
        PaymentProfile GetProfileWithDefaultAccount(Guid riskId);
        PaymentProfile GetProfile(Guid riskId);
    }
}
