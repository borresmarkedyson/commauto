﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IInstallmentRepository : IRepository<Installment, Guid>
    {
        Task<List<Installment>> GetAllByRisk(Guid riskId);
        Task<Installment> FindByInvoiceDate(Guid riskId, DateTime invoiceOnDate);
        Task<List<Installment>> FindCurrentDayInstallmentsByAppId(DateTime invoiceOnDate, string appId);
        Task<int> CountAllNotInvoiced(Guid riskId);
        Task RemoveByRisk(Guid riskId);
        Task<List<Installment>> FindPreviousInstallments(Guid riskId, DateTime requestDate);
        Task<List<Installment>> GetAllIncludingVoid(Guid riskId);
    }
}
