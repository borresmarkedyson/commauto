﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ISuspendedPayeeRepository : IRepository<SuspendedPayee, Guid>
    {
    }
}
