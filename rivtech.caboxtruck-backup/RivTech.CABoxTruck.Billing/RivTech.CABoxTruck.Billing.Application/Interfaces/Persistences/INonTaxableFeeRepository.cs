﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface INonTaxableFeeRepository
    {
        /// <summary>
        /// Returns list of non-taxable FeeType based on given state.
        /// </summary>
        Task<IEnumerable<FeeKind>> GetNonTaxableTypesByState(string stateCode, DateTime effectiveDate);
    }
}
