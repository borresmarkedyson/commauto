﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ITransactionRepository : IRepository<Transaction, Guid>
    {
        Task<List<Transaction>> GetAllByRisk(Guid riskId);
        Task<Transaction> GetDetails(Guid id);
        Task<List<Transaction>> FindByInvoiceId(Guid invoiceId);
        Task RemoveByRisk(Guid riskId);
    }
}
