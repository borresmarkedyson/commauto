﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ITransactionDetailRepository : IRepository<TransactionDetail, Guid>
    {
        Task<TransactionDetail> GetDetails(Guid id);
        Task<decimal> GetTotalTransactionAmount(Guid riskId);
    }
}
