﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface ISuspendedStatusHistoryRepository : IRepository<SuspendedStatusHistory, Guid>
    {
        SuspendedStatusHistory GetFirstPendingData(Guid suspendedPaymentId);
    }
}

