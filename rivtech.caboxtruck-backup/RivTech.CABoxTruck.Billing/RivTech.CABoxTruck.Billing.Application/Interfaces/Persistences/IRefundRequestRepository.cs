﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IRefundRequestRepository : IRepository<RefundRequest, Guid>
    {
        public IEnumerable<RefundRequestRisk> GetRisksWithValidRefundRequest(DateTime date, string appId);
    }
}

