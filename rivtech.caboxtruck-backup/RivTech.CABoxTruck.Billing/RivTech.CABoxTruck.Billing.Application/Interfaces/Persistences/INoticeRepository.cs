﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface INoticeRepository : IRepository<Risk, Guid>
    {
        Task<List<NopcRisk>> GetNopcRisks(DateTime requestDate, string appId);
        Task<List<BalanceDueRisk>> GetBalanceDueRisks(DateTime requestDate, string appId);
    }
}