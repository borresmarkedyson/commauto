﻿using RivTech.CABoxTruck.Billing.Domain.Entities.Logging;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IAppLogRepository : IRepository<AppLog, long>
    {
    }
}
