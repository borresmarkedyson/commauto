﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences
{
    public interface IInvoiceRepository : IRepository<Invoice, Guid>
    {
        Task<List<Invoice>> GetAllByRisk(Guid riskId);
        Task<Invoice> GetDetails(Guid id);
        Task<List<Invoice>> GetAllNotVoided(Guid riskId);
        Task<decimal> GetTotalInvoicedAmountAsync(Guid riskId);
        Task<decimal> GetTotalDueInvoicedAmountAsync(Guid riskId, DateTime dueDate);
        Task<Invoice> FindByInvoiceNumber(string invoiceNumber);
        Task RemoveByRisk(Guid riskId);
        Task<decimal> GetTotalPastDueAmount(Guid riskId, DateTime dueDate);
        Invoice GetCancellationInvoice(Guid riskId);
        Task<Invoice> GetLatestInvoiceByNewInvoiceNumber(string newInvoiceNumber);
        
    }
}
