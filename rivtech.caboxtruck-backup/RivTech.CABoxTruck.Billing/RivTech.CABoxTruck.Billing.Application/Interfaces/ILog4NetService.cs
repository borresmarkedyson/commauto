﻿using RivTech.CABoxTruck.Billing.Application.DTO.Logging;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Interfaces
{
    public interface ILog4NetService
    {
        public Task Info(AuditLogDto auditLog);
        public Task Error(ErrorLogDto errorLog);
        public Task Error(Exception exception, [CallerMemberName] string callerMemberName = "");
        public Task Error(string errorMessage, string jsonMessage, [CallerMemberName] string callerMemberName = "");
    }
}
