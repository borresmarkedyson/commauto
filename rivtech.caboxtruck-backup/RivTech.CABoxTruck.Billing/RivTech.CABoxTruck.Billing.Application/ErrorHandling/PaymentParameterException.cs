﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling
{
    public class PaymentParameterException : Exception
    {
        public PaymentParameterException(string msg = "Invalid parameter.") : base(msg)
        {
        }
    }
}
