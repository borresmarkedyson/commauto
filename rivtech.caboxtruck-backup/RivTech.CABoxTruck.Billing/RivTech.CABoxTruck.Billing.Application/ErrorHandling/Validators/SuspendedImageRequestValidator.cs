﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class SuspendedImageRequestValidator : AbstractValidator<SuspendedImageDto>
    {
        public SuspendedImageRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.CheckImg), () =>
            {
                RuleFor(x => x.CheckImg)
                    .MaximumLength(20).WithMessage(ExceptionMessages.CHECKIMG_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.EnvelopeImg), () =>
            {
                RuleFor(x => x.EnvelopeImg)
                    .MaximumLength(20).WithMessage(ExceptionMessages.ENVELOPEIMG_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.InvoiceImg), () =>
            {
                RuleFor(x => x.InvoiceImg)
                    .MaximumLength(20).WithMessage(ExceptionMessages.INVOICEIMG_TOOLONG);
            });
        }
    }
}






