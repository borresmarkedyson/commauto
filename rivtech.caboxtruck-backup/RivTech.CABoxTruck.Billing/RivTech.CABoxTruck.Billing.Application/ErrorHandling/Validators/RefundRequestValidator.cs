﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class RefundRequestValidator : AbstractValidator<RefundRequestDto>
    {
        public RefundRequestValidator()
        {

            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.InstrumentId)
                .Must(x => Enumeration.GetAllId<InstrumentType>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

            RuleFor(x => x.Amount)
                .GreaterThan(0).WithMessage(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_TOTAL_AMOUNT);

            RuleFor(x => x.Comments)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);

            RuleFor(x => x.PayeeInfo).SetValidator(new PayeeInfoValidator());
        }
    }
}
