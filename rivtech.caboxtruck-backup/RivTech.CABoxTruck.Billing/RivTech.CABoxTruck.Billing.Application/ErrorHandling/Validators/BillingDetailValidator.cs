﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class BillingDetailValidator : AbstractValidator<BillingDetailDto>
    {
        public BillingDetailValidator()
        {
            //RuleFor(x => new { x.InsuredAgree, x.UwAgree })
            //   .Must(x => x.InsuredAgree && x.UwAgree)
            //   //.Must(x => (x.InsuredAgree && !x.UwAgree) || (!x.InsuredAgree && x.UwAgree)) 
            //   .WithMessage(ExceptionMessages.PAYMENT_IAGREE_INVALID);

            RuleFor(x => x.BillingAddress)
                .NotEmpty().WithMessage(ExceptionMessages.BILLING_ADDRESS_REQUIRED)
                .SetValidator(new BillingAddressValidator());

            RuleFor(x => x.BillingAddress.Email)
                .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
                .MaximumLength(UtilConstant.EMAIL_MAXLENGTH).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
                .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);
        }
    }
}

