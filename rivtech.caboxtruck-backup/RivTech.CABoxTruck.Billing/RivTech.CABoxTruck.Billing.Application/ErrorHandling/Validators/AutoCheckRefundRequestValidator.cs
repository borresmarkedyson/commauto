﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class AutoCheckRefundRequestValidator : AbstractValidator<AutoCheckRefundRequestDto>
    {
        public AutoCheckRefundRequestValidator()
        {
            RuleForEach(x => x.RefundPayeeRequest).SetValidator(new RefundPayeeRequestValidator());
        }
    }

    public class RefundPayeeRequestValidator : AbstractValidator<RefundPayeeRequestDto>
    {
        public RefundPayeeRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.RefundPayee)
                .NotEmpty().WithMessage(ExceptionMessages.REFUNDPAYEE_REQUIRED);

            When(x => x.RefundPayee != null, () =>
            {
                RuleFor(x => x.RefundPayee).SetValidator(new RefundPayeeValidator());
            });
        }
    }
}



