﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class RewriteRequestValidator : AbstractValidator<RewriteRequestDto>
    {
        public RewriteRequestValidator()
        {
            RuleFor(x => x.CurrentRiskId)
                .NotEmpty() .WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.RewriteRiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.BindRequest).NotEmpty().WithMessage(ExceptionMessages.BINDREQUEST_REQUIRED);
            RuleFor(x => x.BindRequest).SetValidator(new BindRequestValidator());

            RuleFor(x => x.CancelRequest).NotEmpty().WithMessage(ExceptionMessages.CANCELLATIONREQUEST_REQUIRED);
            RuleFor(x => x.CancelRequest).SetValidator(new PolicyCancellationRequestValidator());
        }
    }
}

