﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PostLockboxSuspendedRequestValidator : AbstractValidator<PostLockboxSuspendedRequest>
    {
        public PostLockboxSuspendedRequestValidator()
        {
            RuleFor(x => x).SetValidator(new JobRequestValidator());
        }
    }
}
