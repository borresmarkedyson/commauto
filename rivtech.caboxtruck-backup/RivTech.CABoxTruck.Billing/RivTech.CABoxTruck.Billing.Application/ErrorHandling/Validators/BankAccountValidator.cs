﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class BankAccountValidator : AbstractValidator<BankAccountDto>
    {
        public BankAccountValidator()
        {
            List<string> accountType = new List<string>() { "checking", "savings", "businessChecking", "unknown" };
            RuleFor(x => x.AccountType)
                .NotEmpty().WithMessage("Account type is required")
                .Must(x => accountType.Contains(x)).WithMessage("Invalid account type");

            RuleFor(x => x.RoutingNumber)
                .NotEmpty() .WithMessage("Routing number is required")
                .MaximumLength(9).WithMessage("Routing number is too long");

            RuleFor(x => x.AccountNumber)
                .NotEmpty().WithMessage("Account number is required")
                .MaximumLength(17).WithMessage("Account number is too long");

            RuleFor(x => x.NameOnAccount)
                .NotEmpty() .WithMessage("Account name is required")
                .MaximumLength(22).WithMessage("Account name is too long");

            RuleFor(x => x.BankName)
                .MaximumLength(50).WithMessage("Bank name is too long");
        }
    }
}
