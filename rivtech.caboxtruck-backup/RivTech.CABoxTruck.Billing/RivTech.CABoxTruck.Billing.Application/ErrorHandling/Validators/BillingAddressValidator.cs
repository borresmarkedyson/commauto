﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class BillingAddressValidator : AbstractValidator<BillingAddressDto>
    {
        public BillingAddressValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("First name is required")
                .MaximumLength(50).WithMessage("First name is too long");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("Last name is required")
                .MaximumLength(50).WithMessage("Last name is too long");

            RuleFor(x => x.Address)
                .NotEmpty().WithMessage("Address is required")
                .MaximumLength(60).WithMessage("Address is too long");

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("City is required")
                .MaximumLength(40).WithMessage("City is too long");

            RuleFor(x => x.Zip)
                .NotEmpty().WithMessage("Zip is required")
                .MaximumLength(20).WithMessage("Zip too long");

            //RuleFor(x => x.Email)
            //    .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
            //    .MaximumLength(UtilConstant.EMAIL_MAXLENGTH).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
            //    .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);
        }
    }
}
