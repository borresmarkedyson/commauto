﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PaymentToReinstateRequestValidator : AbstractValidator<PaymentRequestDto>
    {
        public PaymentToReinstateRequestValidator()
        {
            RuleFor(x => x).SetValidator(new PaymentRequestValidator());

            RuleFor(x => x.BillingDetail.BillingAddress.State)
                .NotEmpty().WithMessage(ExceptionMessages.STATE_REQUIRED)
                .MaximumLength(40).WithMessage(ExceptionMessages.STATE_TOOLONG)
                .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.STATE_INVALID);
        }
    }
}


