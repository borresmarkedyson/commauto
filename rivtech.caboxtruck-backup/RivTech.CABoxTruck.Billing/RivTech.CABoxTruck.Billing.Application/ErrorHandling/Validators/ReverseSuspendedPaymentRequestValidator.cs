﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ReverseSuspendedPaymentRequestValidator : AbstractValidator<ReverseSuspendedPaymentRequestDto>
    {
        public ReverseSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.Comment)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);
        }
    }
}


