﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class GetRisksForReinstatementRequestValidator : AbstractValidator<GetRisksForReinstatementRequestDto>
    {
        public GetRisksForReinstatementRequestValidator()
        {
            RuleFor(x => x).SetValidator(new JobRequestValidator());
        }
    }
}
