﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PolicyCancellationRequestValidator : AbstractValidator<PolicyCancellationRequestDto>
    {
        public PolicyCancellationRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.TransactionDetails)
                 .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED);

            When(x => x.TransactionDetails != null, () =>
            {
                RuleForEach(x => x.TransactionDetails)
                    .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED)
                    .SetValidator(new TransactionDetailValidator());
            });

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);

        }
    }
}
