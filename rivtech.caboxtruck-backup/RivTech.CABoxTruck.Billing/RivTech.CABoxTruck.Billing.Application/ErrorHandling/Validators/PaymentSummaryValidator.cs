﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PaymentSummaryValidator : AbstractValidator<PaymentSummaryDto>
    {
        public PaymentSummaryValidator()
        {
            When(x => x.InstrumentId == InstrumentType.Adjustment.Id ||
            x.InstrumentId == InstrumentType.WriteOff.Id, () =>
           {
               RuleFor(x => x.Amount)
                   .Must((x, amount) => x.PremiumAmount + x.FeeAmount + x.TaxAmount == amount)
                   .WithMessage(ExceptionMessages.TOTAL_AMOUNT_MUST_EQUAL_SUM);
           });

            RuleFor(x => x.Amount)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_TOTAL_AMOUNT);

            RuleFor(x => x.PremiumAmount)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_PREMIUM_AMOUNT);

            RuleFor(x => x.FeeAmount)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_FEE_AMOUNT);

            RuleFor(x => x.TaxAmount)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_TAX_AMOUNT);

            RuleFor(x => x.InstrumentId)
                .Must(x => Enumeration.GetAllId<InstrumentType>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

            RuleFor(x => x.Comment)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);

            When(x => x.InstrumentId != InstrumentType.RecurringCreditCard.Id &&
                      x.InstrumentId != InstrumentType.RecurringECheck.Id, () =>
            {
                RuleFor(x => x.IsRecurringPayment)
                    .Empty()
                    .Equal(false)
                    .WithMessage(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            });

            When(x => x.IsRecurringPayment, () =>
            {
                RuleFor(x => x.InstrumentId)
                    .Must(x => x == InstrumentType.RecurringCreditCard.Id || x == InstrumentType.RecurringECheck.Id)
                    .WithMessage(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);

                RuleFor(x => x.Email)
                    .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
                    .MaximumLength(100).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
                    .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);
            });
        }
    }
}
