﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdateRefundRequestValidator : AbstractValidator<UpdateRefundRequestDto>
    {
        public UpdateRefundRequestValidator()
        {
            RuleFor(x => x.RefundRequestId)
                .NotEmpty().WithMessage(ExceptionMessages.REFUNDREQUESTID_REQUIRED);
            
            RuleFor(x => x.Amount)
                .GreaterThan(0).WithMessage(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO);

            RuleFor(x => x.RefundToTypeId)
                .NotEmpty().WithMessage(ExceptionMessages.REFUNDTOTYPE_REQUIRED)
                .Must(x => Enumeration.GetEnumerationById<RefundToType>(x).IsValid()).WithMessage(ExceptionMessages.REFUNDTOTYPE_INVALID);

            RuleFor(x => x.RefundPayee).SetValidator(new RefundPayeeValidator());
        }
    }
}


