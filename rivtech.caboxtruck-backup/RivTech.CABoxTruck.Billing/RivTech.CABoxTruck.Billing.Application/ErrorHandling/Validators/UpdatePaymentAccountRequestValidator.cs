﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Globalization;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdatePaymentAccountRequestValidator : AbstractValidator<UpdatePaymentAccountDto>
    {
        public UpdatePaymentAccountRequestValidator()
        {
            RuleFor(x => x.Id)
               .NotEmpty().WithMessage(ExceptionMessages.INVALID);

            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.AppId)
                .NotEmpty()
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
                .MaximumLength(UtilConstant.EMAIL_MAXLENGTH).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
                .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);

            //RuleFor(x => x.ExpirationDate)
            //    .NotEmpty().WithMessage(ExceptionMessages.EXPIRATIONDATE_REQUIRED)
            //    .Must(expirationDate => DateTime.TryParseExact(expirationDate, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out _) == true)
            //    .WithMessage(ExceptionMessages.EXPIRATIONDATE_INVALID);
        }
    }
}

