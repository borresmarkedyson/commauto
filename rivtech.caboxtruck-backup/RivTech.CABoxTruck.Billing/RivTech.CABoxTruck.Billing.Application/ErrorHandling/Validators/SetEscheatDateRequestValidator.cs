﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class SetEscheatDateRequestValidator : AbstractValidator<SetEscheatDateRequestDto>
    {
        public SetEscheatDateRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.PaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.PAYMENT_ID_REQUIRED);
        }
    }
}
