﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class VoidSuspendedPaymentRequestValidator : AbstractValidator<VoidSuspendedPaymentRequestDto>
    {
        public VoidSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.Comment)
                .NotEmpty().WithMessage(ExceptionMessages.COMMENT_REQUIRED)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);
        }
    }
}

