﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ReversePaymentRequestValidator : AbstractValidator<ReversePaymentRequestDto>
    {
        public ReversePaymentRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.PaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.PAYMENT_ID_REQUIRED);

            RuleFor(x => x.ReversalTypeId)
                .Must(x => Enumeration.GetAllId<PaymentType>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_REVERSAL_TYPE);

            RuleFor(x => x.ReversalTypeId)
                .NotEqual(PaymentType.Regular.Id)
                .WithMessage(ExceptionMessages.INVALID_REVERSAL_TYPE);

            RuleFor(x => x.Comment)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
