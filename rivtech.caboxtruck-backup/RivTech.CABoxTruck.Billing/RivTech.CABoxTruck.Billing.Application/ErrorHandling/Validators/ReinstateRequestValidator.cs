﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ReinstateRequestValidator : AbstractValidator<ReinstateRequestDto>
    {
        public ReinstateRequestValidator()
        {
            RuleFor(x => x.RiskId)
               .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.TransactionDetails)
                .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED);

            When(x => x.TransactionDetails != null, () =>
            {
                RuleForEach(x => x.TransactionDetails)
                    .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED)
                    .SetValidator(new TransactionDetailValidator());
            });
        }
    }
}
