﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class VoidFeeRequestValidator : AbstractValidator<VoidFeeRequestDto>
    {
        public VoidFeeRequestValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithMessage(ExceptionMessages.FEE_ID_REQUIRED);
            RuleFor(x => x.RiskId).NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);
        }
    }
}
