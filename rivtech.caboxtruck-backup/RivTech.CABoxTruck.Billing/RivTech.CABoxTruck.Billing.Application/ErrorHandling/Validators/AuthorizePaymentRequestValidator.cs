﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class AuthorizePaymentRequestValidator : AbstractValidator<AuthorizePaymentRequestDto>
    {
        public AuthorizePaymentRequestValidator()
        {
            When(x => !x.WillPayLater && x.PaymentPlan != PaymentPlan.PremiumFinanced.Id, () =>
            {
                RuleFor(x => x.EffectiveDate)
                    .NotEmpty().WithMessage(ExceptionMessages.EFFECTIVE_DATE_REQUIRED);

                RuleFor(x => x.InstrumentId)
                    .Must(x => Enumeration.GetAllId<InstrumentType>().Contains(x))
                    .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

                RuleFor(x => x.PaymentPlan)
                    .Must(x => Enumeration.GetAllId<PaymentPlan>().Contains(x))
                    .WithMessage(ExceptionMessages.INVALID_PAYMENT_PLAN);

                When(x => x.InstrumentId == InstrumentType.CreditCard.Id ||
                    x.InstrumentId == InstrumentType.RecurringCreditCard.Id ||
                    x.InstrumentId == InstrumentType.RecurringECheck.Id, () =>
                {
                    RuleFor(x => x.BillingDetail)
                        .NotEmpty().WithMessage(ExceptionMessages.BILLING_DETAILS_REQUIRED);

                    When(x => x.BillingDetail != null, () =>
                    {
                        RuleFor(x => x.BillingDetail.BillingAddress)
                        .NotEmpty().WithMessage(ExceptionMessages.BILLING_ADDRESS_REQUIRED)
                        .SetValidator(new BillingAddressValidator());
                    });
                });

                When(x => x.InstrumentId == InstrumentType.CreditCard.Id && x.BillingDetail != null, () =>
                {
                    RuleFor(x => x.BillingDetail.CreditCard)
                        .SetValidator(new CreditCardValidator());
                });


                When(x => x.InstrumentId == InstrumentType.EFT.Id && x.BillingDetail != null, () =>
                {
                    RuleFor(x => x.BillingDetail.BankAccount)
                    .SetValidator(new BankAccountValidator());
                });

                RuleFor(x => x.TransactionDetails)
                    .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED);

                When(x => x.TransactionDetails != null, () =>
                {
                    RuleForEach(x => x.TransactionDetails)
                        .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED)
                        .SetValidator(new TransactionDetailValidator());
                });

                RuleFor(x => x.AppId)
                    .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                    .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            });
        }
    }
}
