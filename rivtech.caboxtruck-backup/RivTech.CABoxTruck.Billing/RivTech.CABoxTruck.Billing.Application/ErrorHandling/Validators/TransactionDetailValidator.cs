﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class TransactionDetailValidator : AbstractValidator<TransactionDetailDto>
    {
        public TransactionDetailValidator()
        {
            When(x => x.AmountSubType != null, () =>
            {
                RuleFor(x => x.AmountSubType.Id)
                .Must(x => Enumeration.GetAllId<AmountSubType>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);
            });

            RuleFor(x => x.AmountSubType).NotEmpty().WithMessage(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);
        }
    }
}
