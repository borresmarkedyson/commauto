﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class SuspendedCheckRequestValidator : AbstractValidator<SuspendedCheckDto>
    {
        public SuspendedCheckRequestValidator()
        {
            When(x => x.CheckAmount != null, () =>
            {
                RuleFor(x => x.CheckAmount)
                   .ScalePrecision(5, 18).WithMessage(ExceptionMessages.CHECKAMOUNT_INVALID);
            });

            When(x => !string.IsNullOrWhiteSpace(x.Check), () =>
            {
                RuleFor(x => x.Check)
                    .MaximumLength(20).WithMessage(ExceptionMessages.CHECK_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.CheckRoutingNum), () =>
            {
                RuleFor(x => x.CheckRoutingNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.CHECKROUTINGNUM_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.CheckAccountNum), () =>
            {
                RuleFor(x => x.CheckAccountNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.CHECKACCOUNTNUM_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.CheckNum), () =>
            {
                RuleFor(x => x.CheckNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.CHECKNUM_TOOLONG);
            });
        }
    }
}





