﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PostRefundRequestValidator : AbstractValidator<PostRefundRequestDto>
    {
        public PostRefundRequestValidator()
        {
            RuleFor(x => x.EncryptionPublicKey).
                NotEmpty().WithMessage(ExceptionMessages.ENCRYPTION_PUBLIC_KEY_REQUIRED);

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
