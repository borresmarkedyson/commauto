﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class DirectBillRequestValidator : AbstractValidator<DirectBillRequestDto>
    {
        public DirectBillRequestValidator()
        {
            RuleFor(x => x).SetValidator(new JobRequestValidator());
        }
    }
}

