﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PostSuspendedPaymentRequestValidator : AbstractValidator<PostSuspendedPaymentRequestDto>
    {
        public PostSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.AppId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
