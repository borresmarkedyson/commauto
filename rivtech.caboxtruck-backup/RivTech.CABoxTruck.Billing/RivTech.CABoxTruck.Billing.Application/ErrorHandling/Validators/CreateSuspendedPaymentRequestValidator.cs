﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class CreateSuspendedPaymentRequestValidator : AbstractValidator<CreateSuspendedPaymentRequestDto>
    {
        public CreateSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.ReceiptDate)
                .NotEmpty().WithMessage(ExceptionMessages.RECEIPTDATE_REQUIRED);

            RuleFor(x => x.Amount)
                .GreaterThan(0).WithMessage(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_AMOUNT);

            When(x => !string.IsNullOrEmpty(x.PolicyNumber), () =>
            {
                RuleFor(x => x.PolicyNumber)
                    .MaximumLength(UtilConstant.POLICYNUMBER_LENGTH).WithMessage(ExceptionMessages.POLICYNUMBER_TOOLONG)
                    .Matches(RegexPatterns.POLICYNUMBER).WithMessage(ExceptionMessages.POLICYNUMBER_INVALID);
            });

            When(x => x.ReasonId != null, () =>
            {
                RuleFor(x => x.ReasonId)
                     .Must(x => SuspendedReason.IsValid(x)).WithMessage(ExceptionMessages.SUSPENDED_REASON_INVALID);
            });

            RuleFor(x => x.Comment)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);
                      
            When(x => x.SuspendedDetail != null, () =>
            {
                RuleFor(x => x.SuspendedDetail).SetValidator(new SuspendedDetailRequestValidator());
            });

            When(x => x.SuspendedCheck != null, () =>
            {
                RuleFor(x => x.SuspendedCheck).SetValidator(new SuspendedCheckRequestValidator());
            });

            When(x => x.SuspendedImage != null, () =>
            {
                RuleFor(x => x.SuspendedImage).SetValidator(new SuspendedImageRequestValidator());
            });
            
            When(x => x.SuspendedPayer != null, () =>
            {
                RuleFor(x => x.SuspendedPayer).SetValidator(new SuspendedPayerRequestValidator());
            });
        }
    }
}


