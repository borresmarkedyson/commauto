﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class BindRequestValidator : AbstractValidator<BindRequestDto>
    {
        public BindRequestValidator()
        {
            RuleFor(x => x.EffectiveDate)
                .NotEmpty().WithMessage(ExceptionMessages.EFFECTIVE_DATE_REQUIRED);

            RuleFor(x => x.PaymentPlan)
                .Must(x => Enumeration.GetAllId<PaymentPlan>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_PAYMENT_PLAN);

            When(x => x.PaymentPlan == PaymentPlan.FullPay.Id || x.PaymentPlan == PaymentPlan.PremiumFinanced.Id, () =>
            {
                RuleFor(x => x.IsRecurringPayment)
                    .Equal(false)
                    .WithMessage(ExceptionMessages.RECURRINGPAYMENT_PAYMENTPLANCONFLICT);
            });

            //When(x => x.WillPayLater && !x.IsRewrite, () =>
            //{
            //    RuleFor(x => x.PaymentPlan)
            //        .NotEqual(x => PaymentPlan.EightPay.Id)
            //        .WithMessage(ExceptionMessages.PAY_LATER_NOT_ALLOWED_FOR_EIGHT_PAY);
            //});

            When(x => !x.WillPayLater && x.PaymentPlan != PaymentPlan.PremiumFinanced.Id, () =>
            {
                RuleFor(x => x.InstrumentId)
                    .Must(x => Enumeration.GetAllId<InstrumentType>().Contains(x))
                    .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

                When(x => x.InstrumentId == InstrumentType.CreditCard.Id ||
                    x.InstrumentId == InstrumentType.EFT.Id ||
                    x.InstrumentId == InstrumentType.RecurringCreditCard.Id ||
                    x.InstrumentId == InstrumentType.RecurringECheck.Id, () =>
                {
                    RuleFor(x => x.BillingDetail)
                        .NotEmpty().WithMessage(ExceptionMessages.BILLING_DETAILS_REQUIRED);

                    RuleFor(x => x.TransactionId)
                    .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_ID_REQUIRED);

                    When(x => x.BillingDetail != null, () =>
                    {
                        RuleFor(x => x.BillingDetail)
                            .SetValidator(new BillingDetailValidator());
                    });
                });

                When(x => (x.InstrumentId == InstrumentType.CreditCard.Id || x.InstrumentId == InstrumentType.RecurringCreditCard.Id) &&
                            x.BillingDetail != null, () =>
                {
                    RuleFor(x => x.BillingDetail.CreditCard)
                        .SetValidator(new CreditCardValidator());
                });


                When(x => (x.InstrumentId == InstrumentType.EFT.Id || x.InstrumentId == InstrumentType.RecurringECheck.Id) &&
                            x.BillingDetail != null, () =>
                {
                    RuleFor(x => x.BillingDetail.BankAccount)
                    .SetValidator(new BankAccountValidator());
                });
            });

            When(x => x.InstrumentId != InstrumentType.RecurringCreditCard.Id &&
                      x.InstrumentId != InstrumentType.RecurringECheck.Id, () =>
            {
                RuleFor(x => x.IsRecurringPayment)
                    .Empty()
                    .Equal(false)
                    .WithMessage(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            });

            When(x => x.IsRecurringPayment, () =>
            {
                RuleFor(x => x.InstrumentId)
                    .Must(x => x == InstrumentType.RecurringCreditCard.Id || x == InstrumentType.RecurringECheck.Id)
                    .WithMessage(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);

                RuleFor(x => x.Email)
                    .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
                    .MaximumLength(100).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
                    .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);
            });

            RuleFor(x => x.TransactionDetails)
                 .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED);

            When(x => x.TransactionDetails != null, () =>
            {
                RuleForEach(x => x.TransactionDetails)
                    .NotEmpty().WithMessage(ExceptionMessages.TRANSACTION_DETAILS_REQUIRED)
                    .SetValidator(new TransactionDetailValidator());
            });

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
