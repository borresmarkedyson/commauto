﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class SuspendedPayerRequestValidator : AbstractValidator<SuspendedPayerDto>
    {
        public SuspendedPayerRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.FirstName), () =>
            {
                RuleFor(x => x.FirstName)
                    .MaximumLength(50).WithMessage(ExceptionMessages.FIRSTNAME_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.MiddleInitial), () =>
            {
                RuleFor(x => x.MiddleInitial)
                    .MaximumLength(1).WithMessage(ExceptionMessages.MIDDLEINITIAL_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.LastName), () =>
            {
                RuleFor(x => x.LastName)
                    .MaximumLength(50).WithMessage(ExceptionMessages.LASTNAME_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.Address1), () =>
            {
                RuleFor(x => x.Address1)
                    .MaximumLength(35).WithMessage(ExceptionMessages.ADDRESS_TOO_LONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.Address2), () =>
            {
                RuleFor(x => x.Address2)
                    .MaximumLength(35).WithMessage(ExceptionMessages.ADDRESS_TOO_LONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.City), () =>
            {
                RuleFor(x => x.City)
                    .MaximumLength(30).WithMessage(ExceptionMessages.CITY_TOO_LONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.State), () =>
            {
                RuleFor(x => x.State)
                    .MaximumLength(2).WithMessage(ExceptionMessages.STATE_TOO_LONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.Zip), () =>
            {
                RuleFor(x => x.Zip)
                    .MaximumLength(10).WithMessage(ExceptionMessages.ZIP_CODE_TOO_LONG);
            });
        }
    }
}



