﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdateRiskRequestValidator : AbstractValidator<UpdateRiskRequestDto>
    {
        public UpdateRiskRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.PolicyNumber)
                .NotEmpty().WithMessage(ExceptionMessages.POLICYNUMBER_REQUIRED)
                .MaximumLength(UtilConstant.POLICYNUMBER_LENGTH).WithMessage(ExceptionMessages.POLICYNUMBER_TOOLONG)
                .Matches(RegexPatterns.POLICYNUMBER).WithMessage(ExceptionMessages.POLICYNUMBER_INVALID);
        }
    }
}

