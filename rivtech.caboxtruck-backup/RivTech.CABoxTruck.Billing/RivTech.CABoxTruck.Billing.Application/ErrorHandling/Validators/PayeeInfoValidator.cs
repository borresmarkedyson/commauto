﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PayeeInfoValidator : AbstractValidator<PayeeInfoDto>
    {
        public PayeeInfoValidator()
        {
            RuleFor(x => x.Name)
                .MaximumLength(100).WithMessage(ExceptionMessages.NAME_TOO_LONG);

            RuleFor(x => x.Address)
                .MaximumLength(250).WithMessage(ExceptionMessages.ADDRESS_TOO_LONG);

            RuleFor(x => x.City)
                .MaximumLength(50).WithMessage(ExceptionMessages.CITY_TOO_LONG);

            RuleFor(x => x.State)
                .MaximumLength(50).WithMessage(ExceptionMessages.STATE_TOO_LONG);

            RuleFor(x => x.ZipCode)
                .MaximumLength(10).WithMessage(ExceptionMessages.ZIP_CODE_TOO_LONG);
        }
    }
}
