﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class CreatePaymentAccountRequestValidator : AbstractValidator<PaymentAccountRequestDto>
    {
        public CreatePaymentAccountRequestValidator()
        {
            RuleFor(x => x)
                .SetValidator(new PaymentAccountRequestValidator());

            RuleFor(x => x.InstrumentTypeId)
                .Must(x => x == InstrumentType.RecurringCreditCard.Id || x == InstrumentType.RecurringECheck.Id)
                .WithMessage(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);

            RuleFor(x => x.BillingDetail)
               .NotEmpty().WithMessage(ExceptionMessages.BILLING_DETAILS_REQUIRED);         

            When(x => x.BillingDetail != null, () =>
            {
                RuleFor(x => x.BillingDetail)
                    .SetValidator(new BillingDetailValidator());

                When(x => x.InstrumentTypeId == InstrumentType.RecurringCreditCard.Id, () =>
                {
                    RuleFor(x => x.BillingDetail.CreditCard)
                        .SetValidator(new CreditCardValidator());
                });

                When(x => x.InstrumentTypeId == InstrumentType.RecurringECheck.Id, () =>
                {
                    RuleFor(x => x.BillingDetail.BankAccount)
                        .SetValidator(new BankAccountValidator());
                });
            });   
        }
    }
}
