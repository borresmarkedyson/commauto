﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ChangePaymentPlanPaymentValidator : AbstractValidator<PaymentRequestDto>
    {
        public ChangePaymentPlanPaymentValidator()
        {
            RuleFor(x => x).SetValidator(new PaymentRequestValidator());

            RuleFor(x => x.PaymentSummary.Amount)
                .NotEqual(0).WithMessage(ExceptionMessages.TOTAL_AMOUNT_ZERO);

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
