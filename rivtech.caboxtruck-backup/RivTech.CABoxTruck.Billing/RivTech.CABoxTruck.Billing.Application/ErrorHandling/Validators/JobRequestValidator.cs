﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class JobRequestValidator : AbstractValidator<JobRequestDto>
    {
        public JobRequestValidator()
        {
            RuleFor(x => x.ApplicationId)
                .NotEmpty().WithMessage(ExceptionMessages.RIVTECHAPPLICATIONID_REQUIRED)
                .Must(x => RivtechApplication.IsValid(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
