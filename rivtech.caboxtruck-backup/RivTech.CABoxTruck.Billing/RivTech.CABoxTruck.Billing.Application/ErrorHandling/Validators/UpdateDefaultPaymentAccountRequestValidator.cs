﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdateDefaultPaymentAccountRequestValidator : AbstractValidator<UpdateDefaultAccountRequestDto>
    {
        public UpdateDefaultPaymentAccountRequestValidator()
        {
            RuleFor(x => x.Id)
               .NotEmpty().WithMessage(ExceptionMessages.INVALID);

            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.IsDefault)
                .Must(x => x == false || x == true)
                .WithMessage(ExceptionMessages.INVALID);

            RuleFor(x => x.AppId)
                .NotEmpty()
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
