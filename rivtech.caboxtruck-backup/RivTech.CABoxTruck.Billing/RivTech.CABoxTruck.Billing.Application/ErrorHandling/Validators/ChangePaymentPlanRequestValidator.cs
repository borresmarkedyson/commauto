﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ChangePaymentPlanRequestValidator : AbstractValidator<ChangePaymentPlanRequestDto>
    {
        public ChangePaymentPlanRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.NewPaymentPlanId)
                .Must(x => Enumeration.GetAllId<PaymentPlan>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_PAYMENT_PLAN);

            RuleFor(x => x.PaymentDetails)
                 .NotEmpty().WithMessage(ExceptionMessages.PAYMENT_DETAILS_REQUIRED);

            When(x => x.PaymentDetails != null, () =>
            {
                When(x => x.PaymentDetails.TotalAmount > 0, () =>
                {
                    RuleFor(x => x.InstrumentId)
                        .Must(x => Enumeration.GetAllId<InstrumentType>().Contains(x))
                        .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

                    When(x => x.InstrumentId == InstrumentType.CreditCard.Id, () =>
                    {
                        RuleFor(x => x.BillingDetail)
                            .NotEmpty().WithMessage(ExceptionMessages.BILLING_DETAILS_REQUIRED);

                        When(x => x.BillingDetail != null, () =>
                        {
                            RuleFor(x => x.BillingDetail.BillingAddress)
                                .NotEmpty().WithMessage(ExceptionMessages.BILLING_ADDRESS_REQUIRED)
                                .SetValidator(new BillingAddressValidator());
                        });
                    });

                    When(x => x.InstrumentId == InstrumentType.CreditCard.Id && x.BillingDetail != null, () =>
                    {
                        RuleFor(x => x.BillingDetail.CreditCard)
                            .SetValidator(new CreditCardValidator());
                    });


                    When(x => x.InstrumentId == InstrumentType.EFT.Id && x.BillingDetail != null, () =>
                    {
                        RuleFor(x => x.BillingDetail.BankAccount)
                         .SetValidator(new BankAccountValidator());
                    });
                });

                RuleFor(x => x.PaymentDetails.TotalAmount)
                    .GreaterThanOrEqualTo(0).WithMessage(ExceptionMessages.TOTAL_AMOUNT_LESS_THAN_ZERO)
                    .Must((x, amount) => x.PaymentDetails.PremiumAmount + x.PaymentDetails.FeeAmount + x.PaymentDetails.TaxAmount == amount)
                    .WithMessage(ExceptionMessages.TOTAL_AMOUNT_MUST_EQUAL_SUM)
                    .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_TOTAL_AMOUNT);

                RuleFor(x => x.PaymentDetails.PremiumAmount)
                    .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_PREMIUM_AMOUNT);

                RuleFor(x => x.PaymentDetails.FeeAmount)
                    .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_FEE_AMOUNT);

                RuleFor(x => x.PaymentDetails.TaxAmount)
                    .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_TAX_AMOUNT);
            });
        }
    }
}
