﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class TransferPaymentRequestValidator : AbstractValidator<TransferPaymentRequestDto>
    {
        public TransferPaymentRequestValidator()
        {
            RuleFor(x => x.FromRiskId)
                .NotEmpty().WithMessage(ExceptionMessages.SOURCE_RISK_ID_REQUIRED);

            RuleFor(x => x.ToPolicyNumber)
                .NotEmpty().WithMessage(ExceptionMessages.DESTINATION_POLICY_NUMBER_REQUIRED)
                .NotEqual(x => x.FromPolicyNumber).WithMessage(ExceptionMessages.DESTINATION_RISK_ID_MUST_NOT_EQUAL_SOURCE);

            RuleFor(x => x.PaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.PAYMENT_ID_REQUIRED);
        }
    }
}
