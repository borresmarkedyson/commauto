﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class TagForManualReinstatementRequestValidator : AbstractValidator<TagForManualReinstatementRequestDto>
    {
        public TagForManualReinstatementRequestValidator()
        {
            RuleFor(x => x.RiskIds)
               .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);
        }
    }
}
