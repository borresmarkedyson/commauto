﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class AddTransactionFeeRequestValidator : AbstractValidator<AddTransactionFeeRequestDto>
    {
        public AddTransactionFeeRequestValidator()
        {
            RuleFor(x => x.AddDate).
                NotEmpty().WithMessage(ExceptionMessages.INVALID_DATE);

            RuleFor(x => x.AmountSubTypeId)
                .Must(x => Enumeration.GetAllId<AmountSubType>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);

            RuleFor(x => x.RiskId).NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.Amount)
                .GreaterThan(0).WithMessage(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO)
                .ScalePrecision(5, 18).WithMessage(ExceptionMessages.INVALID_AMOUNT);

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
