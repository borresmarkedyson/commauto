﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class BasicPaymentRequestValidator : AbstractValidator<PaymentRequestDto>
    {
        public BasicPaymentRequestValidator()
        {
            RuleFor(x => x).SetValidator(new PaymentRequestValidator());

            When(x => x.PaymentSummary.InstrumentId == InstrumentType.Adjustment.Id
                || x.PaymentSummary.InstrumentId == InstrumentType.WriteOff.Id, () =>
            {
                When(x => x.PaymentSummary.InstrumentId == InstrumentType.WriteOff.Id, () =>
                {
                    RuleFor(x => x.PaymentSummary.Amount)
                        .NotEqual(0).WithMessage(ExceptionMessages.TOTAL_AMOUNT_ZERO);

                });

            }).Otherwise(() =>
            {
                RuleFor(x => x.PaymentSummary.Amount)
                    .GreaterThan(0).WithMessage(ExceptionMessages.TOTAL_AMOUNT_LESSTHANOREQUALZERO);

                RuleFor(x => x.PaymentSummary.PremiumAmount)
                    .GreaterThanOrEqualTo(0).WithMessage(ExceptionMessages.PREMIUM_AMOUNT_LESS_THAN_ZERO);

                RuleFor(x => x.PaymentSummary.FeeAmount)
                    .GreaterThanOrEqualTo(0).WithMessage(ExceptionMessages.FEE_AMOUNT_LESS_THAN_ZERO);

                RuleFor(x => x.PaymentSummary.TaxAmount)
                    .GreaterThanOrEqualTo(0).WithMessage(ExceptionMessages.TAX_AMOUNT_LESS_THAN_ZERO);
            });

            RuleFor(x => x.AppId)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x))
                .WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
        }
    }
}
