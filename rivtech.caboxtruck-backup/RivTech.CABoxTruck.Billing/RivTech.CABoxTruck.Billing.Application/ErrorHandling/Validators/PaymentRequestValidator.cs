﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PaymentRequestValidator : AbstractValidator<PaymentRequestDto>
    {
        public PaymentRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.PaymentSummary)
                .NotEmpty().WithMessage(ExceptionMessages.PAYMENTSUMMARY_REQUIRED)
                .SetValidator(new PaymentSummaryValidator());

            When(x => x.PaymentSummary.InstrumentId == InstrumentType.CreditCard.Id ||
                  x.PaymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id ||
                  x.PaymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id, () =>
            {
                RuleFor(x => x.BillingDetail)
                    .NotEmpty().WithMessage(ExceptionMessages.BILLING_DETAILS_REQUIRED);

                When(x => x.BillingDetail != null, () =>
                {
                    RuleFor(x => x.BillingDetail.BillingAddress)
                        .NotEmpty().WithMessage(ExceptionMessages.BILLING_ADDRESS_REQUIRED)
                        .SetValidator(new BillingAddressValidator());
                });
            });

            When(x => (x.PaymentSummary.InstrumentId == InstrumentType.CreditCard.Id ||
                       x.PaymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id) &&
                       x.BillingDetail != null, () =>
            {
                RuleFor(x => x.BillingDetail.CreditCard)
                    .SetValidator(new CreditCardValidator());
            });


            When(x => (x.PaymentSummary.InstrumentId == InstrumentType.EFT.Id ||
                       x.PaymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id) &&
                       x.BillingDetail != null, () =>
            {
                RuleFor(x => x.BillingDetail.BankAccount)
                 .SetValidator(new BankAccountValidator());
            });
        }
    }
}
