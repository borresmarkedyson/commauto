﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdateSuspendedPaymentRequestValidator : AbstractValidator<UpdateSuspendedPaymentRequestDto>
    {
        public UpdateSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty()
                .WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.ReasonId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_REASON_REQUIRED)
                .Must(x => SuspendedReason.IsValid(x)).WithMessage(ExceptionMessages.SUSPENDED_REASON_INVALID);
        }
    }
}


