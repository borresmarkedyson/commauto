﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class CreateRefundRequestValidator : AbstractValidator<CreateRefundRequestDto>
    {
        public CreateRefundRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.RISK_ID_REQUIRED);

            RuleFor(x => x.Amount)
                //.NotEmpty().WithMessage(ExceptionMessages.AMOUNT_REQUIRED)
                .GreaterThan(0).WithMessage(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO);

            RuleFor(x => x.RefundToTypeId)
                .NotEmpty().WithMessage(ExceptionMessages.REFUNDTOTYPE_REQUIRED)
                .Must(x => Enumeration.GetEnumerationById<RefundToType>(x).IsValid()).WithMessage(ExceptionMessages.REFUNDTOTYPE_INVALID);

            RuleFor(x => x.RefundPayee).SetValidator(new RefundPayeeValidator());
        }
    }
}

