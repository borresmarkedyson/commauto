﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class PostPaymentAllEnrolledAccountRequestValidator : AbstractValidator<PostPaymentAllEnrolledAccountRequest>
    {
        public PostPaymentAllEnrolledAccountRequestValidator()
        {
            RuleFor(x => x).SetValidator(new JobRequestValidator());
        }
    }
}
