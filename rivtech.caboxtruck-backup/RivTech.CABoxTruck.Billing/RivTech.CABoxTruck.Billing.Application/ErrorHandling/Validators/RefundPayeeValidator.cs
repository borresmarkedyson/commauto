﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class RefundPayeeValidator : AbstractValidator<RefundPayeeDto>
    {
        public RefundPayeeValidator()
        {
            RuleFor(x => x)
                .NotEmpty().WithMessage(ExceptionMessages.REFUNDPAYEE_REQUIRED);

            When(x => x != null, () =>
            {
                RuleFor(x => x.Name)
                    .NotEmpty().WithMessage(ExceptionMessages.NAME_REQUIRED)
                    .MaximumLength(UtilConstant.NAME_MAXLENGTH).WithMessage(ExceptionMessages.NAME_TOOLONG)
                    .Matches(RegexPatterns.NAME).WithMessage(ExceptionMessages.NAME_INVALID);

                RuleFor(x => x.Street1)
                    .NotEmpty().WithMessage(ExceptionMessages.STREET1_REQUIRED)
                    .MaximumLength(UtilConstant.STREET1_MAXLENGTH).WithMessage(ExceptionMessages.STREET1_TOOLONG);
                //.Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.STREET1_INVALID);

                RuleFor(x => x.Street2)
                    .MaximumLength(UtilConstant.STREET2_MAXLENGTH).WithMessage(ExceptionMessages.STREET2_TOOLONG);
                    //.Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.STREET2_INVALID);

                RuleFor(x => x.City)
                    .NotEmpty().WithMessage(ExceptionMessages.CITY_REQUIRED)
                    .MaximumLength(UtilConstant.CITY_MAXLENGTH).WithMessage(ExceptionMessages.CITY_TOOLONG)
                    .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.CITY_INVALID);

                RuleFor(x => x.State)
                    .NotEmpty().WithMessage(ExceptionMessages.STATE_REQUIRED)
                    .MaximumLength(UtilConstant.STATE_MAXLENGTH).WithMessage(ExceptionMessages.STATE_TOOLONG)
                    .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.STATE_INVALID);

                RuleFor(x => x.Zip)
                    .NotEmpty().WithMessage(ExceptionMessages.ZIP_REQUIRED)
                    .MaximumLength(UtilConstant.ZIP_MAXLENGTH).WithMessage(ExceptionMessages.ZIP_TOOLONG)
                    .Matches(RegexPatterns.ZIP).WithMessage(ExceptionMessages.ZIP_INVALID);
            });
        }
    }
}




