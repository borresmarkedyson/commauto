﻿using CreditCardValidator;
using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Globalization;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class CreditCardValidator : AbstractValidator<CreditCardDto>
    {
        public CreditCardValidator()
        {
            RuleFor(x => x.CardCode)
                .NotEmpty().WithMessage(ExceptionMessages.CVV_REQUIRED)
                .Length(3, 4).WithMessage(ExceptionMessages.CVV_TOOLONG);

            RuleFor(x => x.CardNumber)
                .NotEmpty().WithMessage(ExceptionMessages.CARDNUMBER_REQUIRED)
                .Length(13, 16).WithMessage(ExceptionMessages.CARDNUMBER_TOOLONG);

            RuleFor(x => x.ExpirationDate)
                .NotEmpty().WithMessage(ExceptionMessages.EXPIRATIONDATE_REQUIRED)
                .Must(expirationDate => DateTime.TryParseExact(expirationDate, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out _) == true)
                .WithMessage(ExceptionMessages.EXPIRATIONDATE_INVALID);
        }
    }
}
