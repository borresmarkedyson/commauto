﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class ReturnSuspendedPaymentRequestValidator : AbstractValidator<ReturnSuspendedPaymentRequestDto>
    {
        public ReturnSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.Payee)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_PAYEE_REQUIRED);

            When(x => x.Payee != null, () =>
            {
                RuleFor(x => x.Payee.Name)
                    .NotEmpty().WithMessage(ExceptionMessages.NAME_REQUIRED)
                    .MaximumLength(100).WithMessage(ExceptionMessages.NAME_TOOLONG);
                    //.Matches(RegexPatterns.NAME).WithMessage(ExceptionMessages.NAME_INVALID);

                RuleFor(x => x.Payee.Address)
                    .NotEmpty().WithMessage(ExceptionMessages.ADDRESS_REQUIRED)
                    .MaximumLength(60).WithMessage(ExceptionMessages.ADDRESS_TOOLONG)
                    .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.ADDRESS_INVALID);

                RuleFor(x => x.Payee.City)
                    .NotEmpty().WithMessage(ExceptionMessages.CITY_REQUIRED)
                    .MaximumLength(40).WithMessage(ExceptionMessages.CITY_TOOLONG)
                    .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.CITY_INVALID);

                RuleFor(x => x.Payee.State)
                    .NotEmpty().WithMessage(ExceptionMessages.STATE_REQUIRED)
                    .MaximumLength(40).WithMessage(ExceptionMessages.STATE_TOOLONG)
                    .Matches(RegexPatterns.ALPHANUMERIC_SPACE).WithMessage(ExceptionMessages.STATE_INVALID);

                RuleFor(x => x.Payee.Zip)
                    .NotEmpty().WithMessage(ExceptionMessages.ZIP_REQUIRED)
                    .MaximumLength(5).WithMessage(ExceptionMessages.ZIP_TOOLONG)
                    .Matches(RegexPatterns.ZIP).WithMessage(ExceptionMessages.ZIP_INVALID);
            });

            RuleFor(x => x.Comment)
                .MaximumLength(1000).WithMessage(ExceptionMessages.COMMENT_TOO_LONG);
        }
    }
}



