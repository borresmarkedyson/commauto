﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class SuspendedDetailRequestValidator : AbstractValidator<SuspendedDetailDto>
    {
        public SuspendedDetailRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.Number), () =>
            {
                RuleFor(x => x.Number)
                    .MaximumLength(20).WithMessage(ExceptionMessages.NUMBER_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.EnvelopeNum), () =>
            {
                RuleFor(x => x.EnvelopeNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.ENVELOPENUM_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.Envelope), () =>
            {
                RuleFor(x => x.Envelope)
                    .MaximumLength(20).WithMessage(ExceptionMessages.ENVELOPE_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.TransactionNum), () =>
            {
                RuleFor(x => x.TransactionNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.TRANSACTIONNUM_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.LockboxNum), () =>
            {
                RuleFor(x => x.LockboxNum)
                    .MaximumLength(20).WithMessage(ExceptionMessages.LOCKBOXNUM_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.BatchItem), () =>
            {
                RuleFor(x => x.BatchItem)
                    .MaximumLength(20).WithMessage(ExceptionMessages.BATCHITEM_TOOLONG);
            });


            When(x => !string.IsNullOrWhiteSpace(x.InvoicePage), () =>
            {
                RuleFor(x => x.InvoicePage)
                    .MaximumLength(20).WithMessage(ExceptionMessages.INVOICEPAGE_TOOLONG);
            });


            When(x => !string.IsNullOrWhiteSpace(x.ClientId), () =>
            {
                RuleFor(x => x.ClientId)
                    .MaximumLength(16).WithMessage(ExceptionMessages.CLIENTID_TOOLONG);
            });

            When(x => !string.IsNullOrWhiteSpace(x.ClientName), () =>
            {
                RuleFor(x => x.ClientName)
                    .MaximumLength(45).WithMessage(ExceptionMessages.CLIENTNAME_TOOLONG);
            });
            When(x => !string.IsNullOrWhiteSpace(x.GroupId), () =>
            {
                RuleFor(x => x.GroupId)
                    .MaximumLength(16).WithMessage(ExceptionMessages.GROUPID_TOOLONG);
            });
            When(x => !string.IsNullOrWhiteSpace(x.GroupDescription), () =>
            {
                RuleFor(x => x.GroupDescription)
                    .MaximumLength(255).WithMessage(ExceptionMessages.GROUPDESCRIPTION_TOOLONG);
            });
        }
    }
}




