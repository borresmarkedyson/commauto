﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class UpdateReturnSuspendedPaymentRequestValidator : AbstractValidator<UpdateReturnSuspendedPaymentRequestDto>
    {

        public UpdateReturnSuspendedPaymentRequestValidator()
        {
            RuleFor(x => x.SuspendedPaymentId)
                .NotEmpty().WithMessage(ExceptionMessages.SUSPENDED_ID_REQUIRED);

            RuleFor(x => x.CheckNumber)
                .MaximumLength(30).WithMessage(ExceptionMessages.CHECKNUMBER_TOOLONG);
        }
    }
}




