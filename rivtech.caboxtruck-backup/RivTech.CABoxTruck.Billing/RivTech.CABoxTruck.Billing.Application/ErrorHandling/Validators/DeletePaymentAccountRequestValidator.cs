﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class DeletePaymentAccountRequestValidator : AbstractValidator<PaymentAccountRequestDto>
    {
        public DeletePaymentAccountRequestValidator()
        {
            RuleFor(x => x).SetValidator(new PaymentAccountRequestValidator());

            RuleFor(x => x.Id)
               .NotEmpty().WithMessage(ExceptionMessages.INVALID);

            RuleFor(x => x.InstrumentTypeId)
                .Must(x => x == InstrumentType.RecurringCreditCard.Id || x == InstrumentType.RecurringECheck.Id)
                .WithMessage(ExceptionMessages.INVALID_INSTRUMENT_TYPE);

            RuleFor(x => x.IsDefault)
                .Must(x => x == false)
                .WithMessage(ExceptionMessages.PAYMENTACCOUNT_CANNOT_DELETE);
        }
    }
}
