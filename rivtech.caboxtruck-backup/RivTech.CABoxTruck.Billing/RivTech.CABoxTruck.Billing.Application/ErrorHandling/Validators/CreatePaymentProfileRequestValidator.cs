﻿using FluentValidation;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators
{
    public class CreatePaymentProfileRequestValidator : AbstractValidator<PaymentProfileRequestDto>
    {
        public CreatePaymentProfileRequestValidator()
        {
            RuleFor(x => x.RiskId)
                .NotEmpty().WithMessage(ExceptionMessages.INVALID_RISK_ID);

            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage(ExceptionMessages.FIRSTNAME_REQUIRED)
                .MaximumLength(50).WithMessage(ExceptionMessages.FIRSTNAME_TOOLONG)
                .Matches(RegexPatterns.NAME).WithMessage(ExceptionMessages.FIRSTNAME_INVALID);

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage(ExceptionMessages.LASTNAME_REQUIRED)
                .MaximumLength(50).WithMessage(ExceptionMessages.LASTNAME_TOOLONG)
                .Matches(RegexPatterns.NAME).WithMessage(ExceptionMessages.LASTNAME_INVALID);

            RuleFor(x => x.PhoneNumber)
                .Matches(RegexPatterns.PHONENUMBER).WithMessage(ExceptionMessages.PHONE_INVALID);

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage(ExceptionMessages.EMAIL_REQUIRED)
                .MaximumLength(100).WithMessage(ExceptionMessages.EMAIL_TOOLONG)
                .Matches(RegexPatterns.EMAIL).WithMessage(ExceptionMessages.EMAIL_INVALID);

            RuleFor(x => x.IsRecurringPayment)
                .Equal(true).WithMessage(ExceptionMessages.ISRECURRINGPAYMENT_INVALID);

            RuleFor(x => x.AppId)
                .NotEmpty().WithMessage(ExceptionMessages.RIVTECHAPPLICATIONID_REQUIRED)
                .Must(x => Enumeration.GetAllId<RivtechApplication>().Contains(x)).WithMessage(ExceptionMessages.INVALID_RIVTECH_APPLICATION);

            RuleFor(x => x.PaymentAccount)
                .NotEmpty().WithMessage(ExceptionMessages.PAYMENTACCOUNT_REQUIRED);

            When(x => x.PaymentAccount != null, () =>
            {
                RuleFor(x => x.PaymentAccount)
                    .SetValidator(new CreatePaymentAccountRequestValidator());
            });
        }
    }
}


