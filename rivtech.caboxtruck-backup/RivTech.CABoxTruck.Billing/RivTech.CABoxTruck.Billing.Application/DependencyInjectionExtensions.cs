﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Application.Services;
using RivTech.CABoxTruck.Billing.Application.Services.Logging;

namespace RivTech.CABoxTruck.Billing.Application
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<IInstallmentScheduleService, InstallmentScheduleService>();
            services.AddTransient<IInvoiceService, InvoiceService>();
            services.AddTransient<IInvoiceNumberService, InvoiceNumberService>();
            services.AddScoped<ILog4NetService, Log4NetService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPaymentDetailService, PaymentDetailService>();
            services.AddTransient<IPaymentViewService, PaymentViewService>();
            services.AddTransient<IPaymentProfileService, PaymentProfileService>();
            services.AddTransient<IPaymentAccountService, PaymentAccountService>();
            services.AddTransient<IProviderPaymentProfileService, ProviderPaymentProfileService>();
            services.AddTransient<IProviderPaymentAccountService, ProviderPaymentAccountService>();
            services.AddTransient<IAuthNetProfileService, AuthNetProfileService>();
            services.AddTransient<IAuthNetAccountService, AuthNetAccountService>();
            services.AddTransient<IBindService, BindService>();
            services.AddTransient<IExcessPaymentService, ExcessPaymentService>();
            services.AddTransient<IBillingSummaryService, BillingSummaryService>();
            services.AddTransient<IReversePaymentService, ReversePaymentService>();
            services.AddTransient<IPaymentBreakdownService, PaymentBreakdownService>();
            services.AddTransient<ITransferPaymentService, TransferPaymentService>();
            services.AddTransient<IRefundService, RefundService>();
            services.AddTransient<ITransactionFeeService, TransactionFeeService>();
            services.AddTransient<IValidationService, ValidationService>();
            services.AddTransient<ISuspendedPaymentService, SuspendedPaymentService>();
            services.AddTransient<ISuspendedStatusHistoryService, SuspendedStatusHistoryService>();
            services.AddTransient<ISuspendedPaymentViewService, SuspendedPaymentViewService>();
            services.AddTransient<IChangePaymentPlanService, ChangePaymentPlanService>();
            services.AddTransient<ISuspendedReturnDetailService, SuspendedReturnDetailService>();
            services.AddTransient<IPaymentSubscriptionService, PaymentSubscriptionService>();
            services.AddTransient<ICalendarService, CalendarService>();
            services.AddTransient<IFlatCancellationService, FlatCancellationService>();
            services.AddTransient<INsfService, NsfService>();
            services.AddTransient<IFinalCancellationService, FinalCancellationService>();
            services.AddTransient<IBalanceDueService, BalanceDueService>();
            services.AddTransient<INopcService, NopcService>();
            services.AddTransient<IEndorsementService, EndorsementService>();
            services.AddTransient<ISuspendedLockboxService, SuspendedLockboxService>();
            services.AddTransient<IFtpService, FtpService>();
            services.AddTransient<IRewriteService, RewriteService>();
            services.AddTransient<IRefundRequestService, RefundRequestService>();
            services.AddTransient<IPaymentValidationService, PaymentValidationService>();
            services.AddTransient<IRefundRequestTransmittalService, RefundRequestTransmittalService>();
            services.AddTransient<IXmlGenerationService, XmlGenerationService>();
            services.AddTransient<IBusinessDayService, BusinessDayService>();
            services.AddTransient<IFileSequenceNumberService, FileSequenceNumberService>();
            services.AddTransient<IPolicyCancellationService, PolicyCancellationService>();
            services.AddTransient<IRiskService, RiskService>();
            services.AddTransient<IReinstatementService, ReinstatementService>();
            services.AddTransient<ICancelledRiskPaymentService, CancelledRiskPaymentService>();
            services.AddTransient<ICheckNumberService, CheckNumberService>();
            services.AddTransient<IPaymentDueService, PaymentDueService>();
            services.AddTransient<IFeeInvoiceService, FeeInvoiceService>();
            services.AddScoped<IUserDateService, UserDateService>();
            services.AddTransient<IDepositAmountService, DepositAmountService>();
            services.AddTransient<ISuspendedDirectBillService, SuspendedDirectBillService>();
            services.AddTransient<ITaxService, TaxService>();


            SetAutoMapper(services);
            return services;
        }

        private static void SetAutoMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
