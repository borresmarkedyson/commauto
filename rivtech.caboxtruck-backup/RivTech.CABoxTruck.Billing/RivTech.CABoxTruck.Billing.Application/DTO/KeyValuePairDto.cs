﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class KeyValuePairDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
