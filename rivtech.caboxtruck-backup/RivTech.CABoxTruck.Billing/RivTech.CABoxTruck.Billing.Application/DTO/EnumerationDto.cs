﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class EnumerationDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
