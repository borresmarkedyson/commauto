﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PayeeInfoDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
    }
}
