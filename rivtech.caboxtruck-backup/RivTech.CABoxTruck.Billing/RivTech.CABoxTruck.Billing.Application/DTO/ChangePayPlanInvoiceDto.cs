﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class ChangePayPlanInvoiceDto : InvoiceDto
    {
        public string PaymentMethod { get; set; }
        public string AccountNumber { get; set; }
        public string PaymentPlan { get; set; }
        public string Reference { get; set; }
        public decimal PaymentAmount { get; set; }
        public string EmailAddress { get; set; }
        public bool IsRecurringPayment { get; set; }
    }
}
