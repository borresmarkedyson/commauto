﻿using AuthorizeNet.Api.Contracts.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PaymentProviderResponseDto
    {
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string ReferenceId { get; set; }

        public PaymentProviderResponseDto(transactionResponse response)
        {
            AccountType = response.accountType;
            AccountNumber = response.accountNumber;
            ReferenceId = response.transId;
        }
    }
}
