﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class TransactionTaxDto
    {
        public Guid Id { get; set; }
        public DateTime AddDate { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public DateTime? VoidDate { get; set; }
    }
}
