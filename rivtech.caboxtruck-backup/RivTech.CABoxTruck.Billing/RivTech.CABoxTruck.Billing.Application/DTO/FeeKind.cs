﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class FeeKind : Enumeration
    {
        public static readonly FeeKind RiskManagementFeeAL = new FeeKind("RMFAL", "Risk Management Fee (AL)");
        public static readonly FeeKind RiskManagementFeePD = new FeeKind("RMFPD", "Risk Management Fee (PD)");
        public static readonly FeeKind InstallmentFee = new FeeKind("INSTF", "Installment Fee");
        public static readonly FeeKind ReinstatementFee = new FeeKind("REINF", "Reinstatement Fee");

        public static FeeKind FromId(string feeTypeId)
        {
            if (feeTypeId.Equals(RiskManagementFeeAL.Id)) return RiskManagementFeeAL;
            if (feeTypeId.Equals(RiskManagementFeePD.Id)) return RiskManagementFeePD;
            if (feeTypeId.Equals(InstallmentFee.Id)) return InstallmentFee;
            if (feeTypeId.Equals(ReinstatementFee.Id)) return ReinstatementFee;
            return null;
        }

        private FeeKind(string id, string description) : base(id, description)
        {
        }

    }

    public class FeeDetails
    {
        public static string MultipleALServiceFee = "Service Fee - Multiple Units (AL)";
        public static string SingleALServiceFee = "Service Fee - Single Units (AL)";
        public static string MidtermALServiceFee = "Service Fee - Midterm (AL)";
        public static string MultiplePDServiceFee = "Service Fee - Multiple Units (PD)";
        public static string SinglePDServiceFee = "Service Fee - Single Units (PD)";
        public static string MidtermPDServiceFee = "Service Fee - Midterm (PD)";
        public static string ReinstatementFee = "Reinstatement Fee";

        public string StateCode { get; set; }
        public string FeeKindId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public bool IsActive { get; set; }

        public FeeDetails(string _stateCode, string _feeKindId, string _description, decimal _amount, bool _isActive)
        {
            StateCode = _stateCode;
            FeeKindId = _feeKindId;
            Description = _description;
            Amount = _amount;
            IsActive = _isActive;
        }
    }
}
