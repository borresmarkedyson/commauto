﻿
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    /// <summary>
    /// An Entity(Agregate Root) for tax computation and its breakdown.
    /// </summary>
    public class TaxDetails
    {
        private readonly List<TaxDetailsBreakdown> _taxDetailsBreakdowns = new List<TaxDetailsBreakdown>();

        public Guid Id { get; private set; }
        public RiskPremiumFees PremiumFees { get; }
        public StateTax StateTax { get; }
        public IEnumerable<FeeKind> NonTaxableFees { get; }

        public IEnumerable<TaxDetailsBreakdown> TaxDetailsBreakdowns => _taxDetailsBreakdowns.AsReadOnly();
        public List<TransactionDetailDto> TransactionDetails { get; set; } = new List<TransactionDetailDto>();

        public decimal TotalSLTax => TaxDetailsBreakdowns
                                    .Where(x => x.TaxAmountTypeId == TaxType.SurplusLine.Id)
                                    .Sum(x => x.Amount);
        public decimal TotalStampingFee => TaxDetailsBreakdowns
                                    .FirstOrDefault(x => x.TaxAmountTypeId == TaxType.StampingFee.Id)
                                    ?.Amount ?? 0;

        public decimal TotalTax => TaxDetailsBreakdowns.Sum(x => x.Amount);

        public TaxDetails(RiskPremiumFees premiumFees, StateTax stateTax, IEnumerable<FeeKind> nonTaxableFees)
        {
            PremiumFees = premiumFees;
            StateTax = stateTax;
            NonTaxableFees = nonTaxableFees;
        }

        public TaxDetailsBreakdown GetAmountByType(string taxTypeId, string taxSubtypeId = null)
        {
            return TaxDetailsBreakdowns.FirstOrDefault(x => x.TaxAmountTypeId == taxTypeId && x.TaxAmountSubtypeId == taxSubtypeId);
        }

        public void Calculate()
        {
            decimal totalTaxableAmount = PremiumFees.Amounts
                // Where amount is Premium or Taxable fee.
                .Where(amount => amount is IPremium
                    || (amount is Fee fee && !NonTaxableFees.Any(ntf => ntf.Equals(fee.Kind))))
                // Total of amount value.
                .Sum(amount => amount.Value);

            foreach (SurplusLineTax slTax in StateTax.SLTaxes)
            {
                decimal amount = slTax.MultiplyRate(totalTaxableAmount);

                if (amount != 0)
                {
                    _taxDetailsBreakdowns.Add(TaxDetailsBreakdown.FromSLTax(TaxType.SurplusLine.Id, slTax.Kind.Id, amount));
                    TransactionDetails.Add(new TransactionDetailDto
                    {
                        Amount = amount,
                        AmountSubType = new AmountSubTypeDto
                        {
                            Id = AmountSubType.SurplusLinesTax.Id,
                            AmountType = new EnumerationDto { Id = AmountType.Tax.Id, Description = AmountType.Tax.Description },
                            Description = AmountSubType.SurplusLinesTax.Description
                        }
                    });
                }
            }

            if (StateTax.StampingFeeTax != null)
            {
                decimal amount = StateTax.StampingFeeTax.CalculateTaxAmount(totalTaxableAmount);

                if (amount != 0)
                {
                    _taxDetailsBreakdowns.Add(TaxDetailsBreakdown.FromStampingFee(TaxType.StampingFee.Id, amount));
                    TransactionDetails.Add(new TransactionDetailDto
                    {
                        Amount = amount,
                        AmountSubType = new AmountSubTypeDto
                        {
                            Id = AmountSubType.StampingFee.Id,
                            AmountType = new EnumerationDto { Id = AmountType.Tax.Id, Description = AmountType.Tax.Description },
                            Description = AmountSubType.SurplusLinesTax.Description
                        }
                    });
                }
            }
        }
    }
}
