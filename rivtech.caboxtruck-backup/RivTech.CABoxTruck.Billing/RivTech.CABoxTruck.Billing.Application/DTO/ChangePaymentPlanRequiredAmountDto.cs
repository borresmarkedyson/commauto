﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class ChangePaymentPlanRequiredAmountDto
    {
        public string PaymentPlanId { get; set; }
        public decimal AmountToPay { get; set; }
        public ChangePaymentPlanPaymentDetailsDto Details { get; set; }
    }
}
