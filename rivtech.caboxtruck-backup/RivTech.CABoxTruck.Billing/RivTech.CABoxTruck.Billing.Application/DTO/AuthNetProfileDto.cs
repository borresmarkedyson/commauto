﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class AuthNetProfileDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string CustomerProfileId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }

        public List<AuthNetAccountDto> AuthNetAccount { get; set; }
    }
}
