﻿
namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public interface IPremium : IAmount
    {
        string Description { get; }
    }
}