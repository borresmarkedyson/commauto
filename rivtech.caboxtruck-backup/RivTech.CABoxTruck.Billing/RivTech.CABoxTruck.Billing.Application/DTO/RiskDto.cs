﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class RiskDto
    {
        public string PaymentPlanId { get; set; }
        public EnumerationDto PaymentPlan { get; set; }
        public string ApplicationId { get; set; }
        public EnumerationDto Application { get; set; }
        public List<InstallmentDto> InstallmentSchedule { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Guid NoticeHistoryId { get; set; }
        public DateTime LatestInvoiceDueDate { get; set; }
        public decimal? AlpCommission { get; private set; }
        public decimal? PdpCommission { get; private set; }
    }
}
