﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class TransactionDetailDto
    {
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public AmountSubTypeDto AmountSubType { get; set; }
    }
}
