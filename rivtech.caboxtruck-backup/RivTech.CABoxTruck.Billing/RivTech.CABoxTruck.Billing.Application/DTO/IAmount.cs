﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public interface IAmount
    {
        decimal Value { get; }
    }
}
