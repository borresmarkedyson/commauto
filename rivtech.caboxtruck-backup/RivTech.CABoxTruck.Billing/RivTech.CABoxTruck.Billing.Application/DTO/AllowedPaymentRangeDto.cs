﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class AllowedPaymentRangeDto
    {
        public decimal MinimumPaymentAmount { get; set; }
        public decimal MaximumPaymentAmount { get; set; }
        public bool IsAccountEnrolled { get; set; }
    }
}
