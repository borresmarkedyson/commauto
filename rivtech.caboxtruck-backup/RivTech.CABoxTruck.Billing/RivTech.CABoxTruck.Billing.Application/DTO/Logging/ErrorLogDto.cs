﻿using System;
namespace RivTech.CABoxTruck.Billing.Application.DTO.Logging
{
    public class ErrorLogDto
    {
        public Guid Id { get; set; }
        public long UserId { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string JsonMessage { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
        public string Body { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

