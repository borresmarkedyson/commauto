﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Logging
{
    public class AuditLogDto
    {
        public long AuditLogID { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public long Key { get; set; }
        public string AuditType { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
    }
}
