﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Logging
{
    public class AppLogDto
    {
        public long Id { get; set; }
        public long ReferrenceID { get; set; }
        public string ClientID { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public string StatusCode { get; set; }
        public string JsonMessage { get; set; }
        public long? KeyID { get; set; }
        public string TableName { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
