﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Logging;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.Billing.Data.Entities;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.KeylessEntities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using static RivTech.CABoxTruck.Billing.Domain.Utils.AppSettings.DirectBillSettings;
using static RivTech.CABoxTruck.Billing.Domain.Utils.AppSettings.LockboxSettings;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Transaction, TransactionDto>();
            CreateMap<TransactionDetail, TransactionDetailDto>();
            CreateMap<Installment, InstallmentDto>();
            CreateMap<Payment, PaymentDto>().ReverseMap();
            CreateMap<Payment, TransferPaymentDto>();
            CreateMap<PaymentDetail, PaymentDetailDto>().ReverseMap();
            CreateMap<Invoice, InvoiceDto>().ReverseMap();
            CreateMap<InvoiceDetail, InvoiceDetailDto>().ReverseMap();
            CreateMap<InstallmentAndInvoice, InstallmentAndInvoiceDto>();
            CreateMap<BillingSummary, BillingSummaryDto>();
            CreateMap<PaymentAccount, PaymentAccountDto>().ReverseMap();
            CreateMap<PaymentProfile, PaymentProfileDto>();
            CreateMap<AuthNetProfile, AuthNetProfileDto>();
            CreateMap<AuthNetAccount, AuthNetAccountDto>().ReverseMap();
            CreateMap<Installment, InstallmentDto>();
            CreateMap<InvoiceDetail, InvoiceDetailDto>();
            CreateMap<PayeeInfo, PayeeInfoDto>().ReverseMap();
            CreateMap<ApplicationUser, ApplicationUserDto>().ReverseMap();
            CreateMap<SuspendedPayment, SuspendedPaymentDto>().ReverseMap();
            CreateMap<SuspendedPayee, SuspendedPayeeDto>().ReverseMap();
            CreateMap<SuspendedReturnDetail, SuspendedReturnDetailDto>().ReverseMap();
            CreateMap<RefundRequest, RefundReqDto>().ReverseMap();
            CreateMap<RefundPayee, RefundPayeeDto>().ReverseMap();
            CreateMap<Risk, RiskDto>().ReverseMap();
            CreateMap<ErrorLog, ErrorLogDto>().ReverseMap();
            CreateMap<Invoice, ChangePayPlanInvoiceDto>();
            CreateMap<Invoice, PolicyCancellationInvoiceDto>();
            CreateMap<SuspendedDetail, SuspendedDetailDto>().ReverseMap();
            CreateMap<SuspendedCheck, SuspendedCheckDto>().ReverseMap();
            CreateMap<SuspendedImage, SuspendedImageDto>().ReverseMap();
            CreateMap<SuspendedPayer, SuspendedPayerDto>().ReverseMap();
            CreateMap<Customer, CustomerDto>().ReverseMap();
            CreateMap<FtpSettingLB, FTPSettingsDto>().ReverseMap();
            CreateMap<FtpSettingDB, FTPSettingsDto>().ReverseMap();

            #region value object
            CreateMap<TransactionType, EnumerationDto>();
            CreateMap<AmountType, EnumerationDto>().ReverseMap();
            CreateMap<AmountSubType, AmountSubTypeDto>();
            CreateMap<InstallmentType, EnumerationDto>();
            CreateMap<InstrumentType, EnumerationDto>().ReverseMap();
            CreateMap<StateTax, StateTaxData>().ReverseMap();
            CreateMap<FeeKind, FeeKindData>().ReverseMap();
            #endregion

            #region view model
            CreateMap<InvoiceDto, InvoiceNotifView>().ReverseMap();
            CreateMap<InvoiceDto, BindViewDto>().ReverseMap();
            CreateMap<BindViewDto, BindRequestDto>().ReverseMap();
            CreateMap<RefundRequestRiskViewDto, RefundRequestRisk>().ReverseMap();

            CreateMap<Payment, PaymentViewDto>()
                .ForMember(dest => dest.PostDate, opt => opt.MapFrom(src => src.EffectiveDate));

            CreateMap<SuspendedPayment, SuspendedPaymentViewDto>()
                .ForMember(dest => dest.Reason, opt => opt.MapFrom(src => src.Reason.Description))
                .ForMember(dest => dest.ReceiptDate, opt => opt.MapFrom(src => src.ReceiptDate.ToString("yyyy'-'MM'-'dd'T'") + src.CreatedDate.ToString("HH':'mm':'ss'.'fff'Z'")));

            CreateMap<SuspendedReturnDetail, ReturnSuspendedViewDto>()
                .ForMember(dest => dest.Payee, opt => opt.MapFrom(src => src.Payee.Name))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.SuspendedPayment.Amount))
                .ForMember(dest => dest.ReturnDate, opt => opt.MapFrom(src => src.CreatedDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")))
                .ForMember(dest => dest.CheckIssueDate, opt => opt.MapFrom(src => src.CheckIssueDate.HasValue ?
                    src.CheckIssueDate.Value.ToString("MM/dd/yyyy") : null))
                .ForMember(dest => dest.CheckClearDate, opt => opt.MapFrom(src => src.CheckClearDate.HasValue ?
                    src.CheckClearDate.Value.ToString("MM/dd/yyyy") : null))
                .ForMember(dest => dest.CheckEscheatDate, opt => opt.MapFrom(src => src.CheckEscheatDate.HasValue ?
                    src.CheckEscheatDate.Value.ToString("MM/dd/yyyy") : null));

            CreateMap<SuspendedPayment, SuspendedPaymentDetailViewDto>()
                .ForMember(dest => dest.TransactionNum, opt => opt.MapFrom(src => src.SuspendedDetail.TransactionNum))
                .ForMember(dest => dest.LockboxNum, opt => opt.MapFrom(src => src.SuspendedDetail.LockboxNum))
                .ForMember(dest => dest.ReceiptDate, opt => opt.MapFrom(src => src.ReceiptDate.ToString("MM/dd/yyyy")))
                .ForMember(dest => dest.Batch, opt => opt.MapFrom(src => src.SuspendedDetail.Batch))
                .ForMember(dest => dest.BatchItem, opt => opt.MapFrom(src => src.SuspendedDetail.BatchItem))
                .ForMember(dest => dest.CheckAmount, opt => opt.MapFrom(src => src.SuspendedCheck.CheckAmount))
                .ForMember(dest => dest.RoutingNum, opt => opt.MapFrom(src => src.SuspendedCheck.CheckRoutingNum))
                .ForMember(dest => dest.AccountNum, opt => opt.MapFrom(src => src.SuspendedCheck.CheckAccountNum))
                .ForMember(dest => dest.CheckNum, opt => opt.MapFrom(src => src.SuspendedCheck.CheckNum))
                .ForMember(dest => dest.EnvelopeImg, opt => opt.MapFrom(src => src.SuspendedImage.CheckImg))
                .ForMember(dest => dest.InvoiceImg, opt => opt.MapFrom(src => src.SuspendedImage.InvoiceImg))
                .ForMember(dest => dest.CheckImg, opt => opt.MapFrom(src => src.SuspendedImage.CheckImg))
                .ForMember(dest => dest.Reason, opt => opt.MapFrom(src => src.Reason.Description))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Description))
                .ForMember(dest => dest.Payer, opt => opt.MapFrom(src => src.SuspendedPayer));

            CreateMap<SuspendedReturnDetail, ReturnSuspendedDetailViewDto>()
                .ForMember(dest => dest.Payee, opt => opt.MapFrom(src => src.Payee.Name))
                .ForMember(dest => dest.ReturnDate, opt => opt.MapFrom(src => src.CreatedDate.ToString("MM/dd/yyyy")))
                .ForMember(dest => dest.CheckIssueDate, opt => opt.MapFrom(src => src.CheckIssueDate.HasValue ?
                    src.CheckIssueDate.Value.ToString("MM/dd/yyyy") : null))
                .ForMember(dest => dest.CheckClearDate, opt => opt.MapFrom(src => src.CheckClearDate.HasValue ?
                    src.CheckClearDate.Value.ToString("MM/dd/yyyy") : null))
                .ForMember(dest => dest.CheckEscheatDate, opt => opt.MapFrom(src => src.CheckEscheatDate.HasValue ?
                    src.CheckEscheatDate.Value.ToString("MM/dd/yyyy") : null));

            CreateMap<RefundReqDto, RefundRequestViewDto>()
                .ForMember(dest => dest.RefundAmount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(dest => dest.RefundToType, opt => opt.MapFrom(src =>
                    Enumeration.GetEnumerationById<RefundToType>(src.RefundToTypeId).Description))
                .ForMember(dest => dest.RefundPayeeName, opt => opt.MapFrom(src => src.RefundPayee.Name))
                .ForMember(dest => dest.RefundPayeeAddress, opt => opt.MapFrom(src => string.IsNullOrWhiteSpace(src.RefundPayee.Street2) ?
                    $"{src.RefundPayee.Street1} {src.RefundPayee.City} {src.RefundPayee.State} {src.RefundPayee.Zip}" :
                    $"{src.RefundPayee.Street1} {src.RefundPayee.Street2} {src.RefundPayee.City} {src.RefundPayee.State} {src.RefundPayee.Zip}"));

            CreateMap<PaymentDto, RecurringPaymentViewDto>()
                .ForMember(dest => dest.InstrumentType, opt => opt.MapFrom(src => Enumeration.GetEnumerationById<InstrumentType>(src.InstrumentId).Description))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")));
            #endregion

            #region request
            CreateMap<PaymentProfile, PaymentProfileRequestDto>().ReverseMap();
            CreateMap<PaymentAccount, PaymentAccountRequestDto>().ReverseMap()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.InstrumentTypeId));
            CreateMap<SuspendedPayee, SuspendedPayeeRequestDto>().ReverseMap();
            CreateMap<SuspendedReturnDetail, UpdateReturnSuspendedPaymentRequestDto>().ReverseMap();
            CreateMap<RefundReqDto, CreateRefundRequestDto>().ReverseMap();
            CreateMap<RefundRequest, UpdateRefundRequestDto>().ReverseMap();
            CreateMap<Risk, UpdateRiskRequestDto>().ReverseMap();

            CreateMap<PayeeInfo, BillingAddressDto>().ReverseMap()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.Zip));
            CreateMap<PaymentProfileRequestDto, BindRequestDto>().ReverseMap()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.BillingDetail.BillingAddress.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.BillingDetail.BillingAddress.LastName));
            CreateMap<PaymentAccountRequestDto, BindRequestDto>().ReverseMap()
                .ForMember(dest => dest.InstrumentTypeId, opt => opt.MapFrom(src => src.InstrumentId))
                .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => true));

            CreateMap<PaymentProfileRequestDto, PaymentRequestDto>().ReverseMap()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.BillingDetail.BillingAddress.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.BillingDetail.BillingAddress.LastName))
                .ForMember(dest => dest.IsRecurringPayment, opt => opt.MapFrom(src => src.PaymentSummary.IsRecurringPayment))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.PaymentSummary.Email));
            CreateMap<PaymentAccountRequestDto, PaymentRequestDto>().ReverseMap()
                .ForMember(dest => dest.InstrumentTypeId, opt => opt.MapFrom(src => src.PaymentSummary.InstrumentId))
                .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => true));

            CreateMap<UpdateRefundRequestDto, RefundReqDto>().ReverseMap()
                .ForMember(dest => dest.RefundRequestId, opt => opt.MapFrom(src => src.Id));

            CreateMap<CreateSuspendedPaymentRequestDto, PaymentRequestDto>().ReverseMap()
                .ForMember(dest => dest.ReceiptDate, opt => opt.MapFrom(src => src.PaymentSummary.EffectiveDate))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.PaymentSummary.Amount))
                .ForMember(dest => dest.ReasonId, opt => opt.MapFrom(src => SuspendedReason.ReinstatementCriteriaNotMet.Id))
                .ForMember(dest => dest.SourceId, opt => opt.MapFrom(src => SuspendedSource.Internal.Id));

            CreateMap<TransactionDto, ReinstateRequestDto>().ReverseMap()
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(src => TransactionType.Reinstatement));

            CreateMap<PaymentAccountDto, PaymentAccountRequestDto>().ReverseMap()
                .ForMember(dest => dest.Payer, opt => opt.MapFrom(src => src.BillingDetail.BillingAddress))
                .ForMember(dest => dest.InsuredAgree, opt => opt.MapFrom(src => src.BillingDetail.InsuredAgree))
                .ForMember(dest => dest.UwAgree, opt => opt.MapFrom(src => src.BillingDetail.UwAgree));

            CreateMap<CustomerDto, BillingAddressDto>().ReverseMap()
                .ForMember(dest => dest.StreetAddress1, opt => opt.MapFrom(src => src.Address));

            CreateMap<BillingAddressDto, Customer>()
                .ForMember(dest => dest.StreetAddress1, opt => opt.MapFrom(src => src.Address));

            CreateMap<Customer, BillingAddressDto>()
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.StreetAddress));

            CreateMap<TaxDetails, TaxDetailsDto>().ReverseMap();
            CreateMap<TaxDetailsBreakdown, TaxDetailsBreakdownDto>().ReverseMap();

            #endregion

            #region third-party
            CreateMap<customerAddressType, BillingAddressDto>().ReverseMap();
            CreateMap<bankAccountType, BankAccountDto>().ReverseMap();
            CreateMap<creditCardType, CreditCardDto>().ReverseMap();
            CreateMap<customerProfileExType, PaymentProfileRequestDto>().ReverseMap();
            CreateMap<PaymentSummaryCCEFT, PaymentSummaryCCEFTDto>().ReverseMap();
            #endregion
        }
    }
}
