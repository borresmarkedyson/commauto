﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedCheckDto
    {
        public Guid Id { get; set; }
        public Guid SuspendedPaymentId { get; set; }
        public string Check { get; set; }
        public decimal? CheckAmount { get; set; }
        public string CheckRoutingNum { get; set; }
        public string CheckAccountNum { get; set; }
        public string CheckNum { get; set; }
    }
}
