﻿using System.Xml.Serialization;

namespace RivTech.CABoxTruck.Billing.Application.DTO.CheckTransmittal
{
    public class DetailRecord
    {
        [XmlElement(ElementName = "Issuing_Carrier_Name")]
        public string IssuingCarrierName { get; set; }
        [XmlElement(ElementName = "Check_Type")]
        public string CheckType { get; set; }
        [XmlElement(ElementName = "Delivery_Type")]
        public string DeliveryType { get; set; }
        [XmlElement(ElementName = "Bank_Id")]
        public int BankId { get; set; }
        [XmlElement(ElementName = "Check_No")]
        public int CheckNo { get; set; }
        [XmlElement(ElementName = "Check_Date")]
        public string CheckDate { get; set; }
        [XmlElement(ElementName = "Policy_Number")]
        public string PolicyNumber { get; set; }
        [XmlElement(ElementName = "Policy_Effective_Date")]
        public string PolicyEffectiveDate { get; set; }
        [XmlElement(ElementName = "Payee_Name")]
        public string InsuredName { get; set; }
        [XmlElement(ElementName = "Payee_Street1")]
        public string InsuredStreet1 { get; set; }
        [XmlElement(ElementName = "Payee_Street2")]
        public string InsuredStreet2 { get; set; }
        [XmlElement(ElementName = "Payee_City")]
        public string InsuredCity { get; set; }
        [XmlElement(ElementName = "Payee_State")]
        public string InsuredState { get; set; }
        [XmlElement(ElementName = "Payee_Zip")]
        public string InsuredZip { get; set; }
        [XmlElement(ElementName = "Refund_Reason")]
        public string RefundReason { get; set; }
        [XmlElement(ElementName = "Refund_Reason_Is_Blank_Flag")]
        public int RefundReasonIsBlankFlag { get; set; }
        [XmlElement(ElementName = "Refund_Date")]
        public string RefundDate { get; set; }
        [XmlElement(ElementName = "Total_Premium")]
        public decimal TotalPremium { get; set; }
        [XmlElement(ElementName = "Total_Paid")]
        public decimal TotalPaid { get; set; }
        [XmlElement(ElementName = "Refund_Amount")]
        public decimal RefundAmount { get; set; }
        [XmlElement(ElementName = "Refund_Amount_Is_Zero_Flag")]
        public int RefundAmountIsZeroFlag { get; set; }
        [XmlElement(ElementName = "Check_Message")]
        public string CheckMessage { get; set; }
        [XmlElement(ElementName = "Check_Message_Is_Blank_Flag")]
        public int CheckMessageIsBlankFlag { get; set; }
    }
}
