﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RivTech.CABoxTruck.Billing.Application.DTO.CheckTransmittal
{
    [XmlRoot(ElementName = "Checks", Namespace = "http://www.applieduw.com/policy_checks")]
    public class PolicyChecks
    {
        [XmlElement(ElementName = "File_Date", Namespace = "")]
        public string FileDate { get; set; }
        [XmlElement(ElementName = "File_Time", Namespace = "")]
        public string FileTime { get; set; }
        [XmlElement(ElementName = "File_Sequence_Number", Namespace = "")]
        public int FileSequenceNumber { get; set; }
        [XmlElement(ElementName = "Header_Record_Count", Namespace = "")]
        public int HeaderRecordCount { get; set; }
        [XmlElement(ElementName = "Total_Check_Count", Namespace = "")]
        public int TotalCheckCount { get; set; }
        [XmlElement(ElementName = "Total_Check_Amount", Namespace = "")]
        public decimal TotalCheckAmount { get; set; }
        [XmlElement(ElementName = "Check_Number_Hash", Namespace = "")]
        public decimal CheckNumberHash { get; set; }
        [XmlElement(ElementName = "Check_Group", Namespace = "")]
        public List<GroupHeaderRecord> GroupHeaderRecords { get; set; }

        [XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation = "http://www.applieduw.com/policy_checks/policy_checks.xsd";

        [XmlAttribute("File_Name")]
        public string FileName { get; set; }
    }
}
