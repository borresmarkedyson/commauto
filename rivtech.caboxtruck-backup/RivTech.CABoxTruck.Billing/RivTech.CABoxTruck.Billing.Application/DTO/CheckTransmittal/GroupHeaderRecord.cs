﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RivTech.CABoxTruck.Billing.Application.DTO.CheckTransmittal
{
    public class GroupHeaderRecord
    {
        [XmlElement(ElementName = "Issuing_Carrier_Name")]
        public string IssuingCarrierName { get; set; }
        [XmlElement(ElementName = "Check_Type")]
        public string CheckType { get; set; }
        [XmlElement(ElementName = "Delivery_Type")]
        public string DeliveryType { get; set; }
        [XmlElement(ElementName = "Bank_Id")]
        public int BankId { get; set; }
        [XmlElement(ElementName = "Total_Check_Count")]
        public int TotalCheckCount { get; set; }
        [XmlElement(ElementName = "Total_Check_Amount")]
        public decimal TotalCheckAmount { get; set; }
        [XmlElement(ElementName = "Check_Number_Hash")]
        public decimal CheckNumberHash { get; set; }
        [XmlElement(ElementName = "Check")]
        public List<DetailRecord> DetailRecords { get; set; }
    }
}
