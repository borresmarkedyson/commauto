﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class InstallmentDto
    {
        public Guid Id { get; set; }
        public DateTime InvoiceOnDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PercentOfOutstanding { get; set; }
        public bool IsInvoiced { get; set; }
        public DateTime CreatedDate { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
        public DateTime? VoidDate { get; set; }
        public ApplicationUserDto VoidedBy { get; set; }
        public EnumerationDto InstallmentType { get; set; }
    }
}
