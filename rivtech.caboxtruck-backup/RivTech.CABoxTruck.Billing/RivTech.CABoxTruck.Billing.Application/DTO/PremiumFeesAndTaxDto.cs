﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PremiumFeesAndTaxDto
    {
        public decimal Premium { get; set; }
        public decimal Fees { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
    }
}
