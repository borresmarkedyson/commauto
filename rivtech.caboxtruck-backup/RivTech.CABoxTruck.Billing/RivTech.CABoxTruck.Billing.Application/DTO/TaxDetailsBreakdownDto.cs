﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    /// <summary>
    /// An Entity(Agregate Root) for tax computation and its breakdown.
    /// </summary>
    public class TaxDetailsBreakdownDto
    {
        public Guid TaxDetailsId { get; set; }
        public string TaxAmountTypeId { get; set; }
        public string TaxAmountSubtypeId { get; set; }
        public decimal Amount { get; set; }
    }
}
