﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedImageDto
    {
        public Guid Id { get; set; }
        public Guid SuspendedPaymentId { get; set; }
        public string CheckImg { get; set; }
        public string EnvelopeImg { get; set; }
        public string InvoiceImg { get; set; }
    }
}
