﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class RefundReqDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CheckDate { get; set; }
        public string CheckNumber { get; set; }
        public Guid? PaymentId { get; set; }
        public string RefundToTypeId { get; set; }
        public Guid RefundPayeeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }

        public PaymentDto Payment { get; set; }
        public EnumerationDto RefundToType { get; set; }
        public RefundPayeeDto RefundPayee { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
    }
}
