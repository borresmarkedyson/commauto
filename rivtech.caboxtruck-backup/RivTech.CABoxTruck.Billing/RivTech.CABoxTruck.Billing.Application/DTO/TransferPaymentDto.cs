﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class TransferPaymentDto
    {
        public Guid Id { get; set; }
        public Guid TransferToPaymentId { get; set; }
        public Guid RiskId { get; set; }
        public decimal Amount { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string InstrumentId { get; set; }
        public string Reference { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
        public Guid? VoidOfPaymentId { get; set; }
        public string VoidedById { get; set; }
        public DateTime? VoidDate { get; set; }

        public EnumerationDto InstrumentType { get; private set; }
        public ApplicationUserDto CreatedBy { get; private set; }
        public ApplicationUserDto VoidedBy { get; private set; }
        public List<PaymentDetailDto> PaymentDetails { get; private set; }

        public TransferPaymentDto()
        {
            PaymentDetails = new List<PaymentDetailDto>();
        }
    }
}
