﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class InvoiceDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal CurrentAmountInvoiced { get; set; }
        public decimal TotalAmountDue { get; set; }
        public DateTime CreatedDate { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
        public DateTime? VoidDate { get; set; }
        public ApplicationUserDto VoidedBy { get; set; }
        public List<InvoiceDetailDto> InvoiceDetails { get; set; }
        public decimal FutureInstallmentAmount { get; set; }
        public decimal PayoffAmount { get; set; }
        public decimal BrokerCommission { get; set; }
    }
}
