﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class TransactionDto
    {
        public Guid Id { get; set; }
        public EnumerationDto TransactionType { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
        public Guid? VoidedTransactionId { get; set; }
        public DateTime? VoidDate { get; set; }
        public ApplicationUserDto VoidedBy { get; set; }
        public string AppId { get; set; }

        public TransactionDto() { }

        public TransactionDto(BindRequestDto request)
        {
            AppId = request.AppId;
            EffectiveDate = request.EffectiveDate;
            TransactionDetails = GetNonZeroTransactionDetails(request.TransactionDetails);
            TransactionType = new EnumerationDto
            {
                Id = Domain.ValueObjects.TransactionType.New.Id
            };
        }

        public TransactionDto(List<TransactionDetailDto> transactionDetails, string appId, DateTime effectiveDate)
        {
            AppId = appId;
            EffectiveDate = effectiveDate;
            TransactionDetails = GetNonZeroTransactionDetails(transactionDetails);
            TransactionType = new EnumerationDto
            {
                Id = Domain.ValueObjects.TransactionType.New.Id
            };
        }

        private List<TransactionDetailDto> GetNonZeroTransactionDetails(List<TransactionDetailDto> transactionDetails)
        {
            List<TransactionDetailDto> nonZeroTransactionDetails = new List<TransactionDetailDto>();
            foreach (var transactionDetail in transactionDetails)
            {
                if (transactionDetail.Amount != 0)
                {
                    nonZeroTransactionDetails.Add(transactionDetail);
                }
            }
            return nonZeroTransactionDetails;
        }

        public TransactionDto(EndorsementRequestDto request, DateTime effectiveDate)
        {
            AppId = request.AppId;
            EffectiveDate = effectiveDate;
            TransactionDetails = request.TransactionDetails;
            TransactionType = new EnumerationDto
            {
                Id = Domain.ValueObjects.TransactionType.Endorsement.Id
            };
        }

        public TransactionDto(PolicyCancellationRequestDto request, DateTime effectiveDate)
        {
            AppId = request.AppId;
            EffectiveDate = effectiveDate;
            TransactionDetails = request.TransactionDetails;
            TransactionType = new EnumerationDto
            {
                Id = Domain.ValueObjects.TransactionType.Cancellation.Id
            };
        }
    }
}
