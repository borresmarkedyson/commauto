﻿using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PostRefundResponseDto
    {
        public List<FailedRiskViewDto> FailedRisk { get; set; }
    }
}
