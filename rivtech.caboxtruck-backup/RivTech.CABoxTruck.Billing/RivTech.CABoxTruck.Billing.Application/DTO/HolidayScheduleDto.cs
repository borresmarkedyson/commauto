﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class HolidayScheduleDto
    {
        public int Id { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string HolidayName { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
    }
}
