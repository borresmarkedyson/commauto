﻿
using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public abstract class Tax
    {
        public Tax(decimal rate)
        {
            Rate = rate;
        }

        public decimal Rate { get; }
    }

    public class TaxType : Enumeration
    {
        public static readonly TaxType SurplusLine = new TaxType("SL", "Surplus Line");
        public static readonly TaxType StampingFee = new TaxType("SF", "Stamping Fee");

        private TaxType(string id, string desc) : base(id, desc) { }
    }

}
