﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class PaymentViewDto
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime PostDate { get; set; }
        public string PaymentType { get; set; }
        public string Method { get; set; }
        public string Reference { get; set; }
        public string Comment { get; set; }
        public DateTime? ClearDate { get; set; }
        public DateTime? EscheatDate { get; set; }
        public string ReversalDate { get; set; }
    }
}
