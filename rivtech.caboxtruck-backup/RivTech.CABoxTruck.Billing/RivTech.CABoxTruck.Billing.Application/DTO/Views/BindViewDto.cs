﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class BindViewDto : InvoiceDto
    {
        public bool IsRecurringPayment { get; set; }
        public string Email { get; set; }
        public List<InstallmentAndInvoiceDto> InstallmentSchedule { get; set; }
        public PostPaymentViewDto Payment { get; set; }
    }
}
