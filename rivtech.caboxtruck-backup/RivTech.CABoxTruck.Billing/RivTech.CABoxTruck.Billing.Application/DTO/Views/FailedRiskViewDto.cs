﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class FailedRiskViewDto
    {
        public string RiskId { get; set; }
        public string PolicyNumber { get; set; }
        public string ErrorMessage { get; set; }
    }
}
