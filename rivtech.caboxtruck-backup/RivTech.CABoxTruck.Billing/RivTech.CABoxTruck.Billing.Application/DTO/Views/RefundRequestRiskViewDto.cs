﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class RefundRequestRiskViewDto
    {
        public Guid RiskId { get; set; }
        public string RefundToId { get; set; }
    }
}
