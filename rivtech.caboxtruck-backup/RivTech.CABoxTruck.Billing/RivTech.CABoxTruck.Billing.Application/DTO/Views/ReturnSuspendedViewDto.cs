﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class ReturnSuspendedViewDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public string ReturnDate { get; set; }
        public string Payee { get; set; }
        public decimal Amount { get; set; }
        public string CheckIssueDate { get; set; }
        public string CheckNumber { get; set; }
        public string CheckClearDate { get; set; }
        public string CheckEscheatDate { get; set; }
    }
}
