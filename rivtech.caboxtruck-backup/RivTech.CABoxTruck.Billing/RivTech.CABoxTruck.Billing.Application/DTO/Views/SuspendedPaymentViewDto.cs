﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class SuspendedPaymentViewDto
    {
        public Guid Id { get; set; }
        public string ReceiptDate { get; set; }
        public string PolicyNumber { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }
        public string StatusId { get; set; }
    }
}
