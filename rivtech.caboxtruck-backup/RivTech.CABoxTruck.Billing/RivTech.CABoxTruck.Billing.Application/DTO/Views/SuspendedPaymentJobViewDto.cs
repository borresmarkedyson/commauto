﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class SuspendedPaymentJobViewDto
    {
        public List<SuspendedPaymentDto> SuspendedPayment { get; set; }
        public List<FailedRiskViewDto> FailedRisk { get; set; }
    }
}
