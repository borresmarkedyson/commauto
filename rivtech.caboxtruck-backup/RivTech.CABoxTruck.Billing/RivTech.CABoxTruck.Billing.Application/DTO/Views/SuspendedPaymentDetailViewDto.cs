﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class SuspendedPaymentDetailViewDto
    {
        public Guid Id { get; set; }
        public string TransactionNum { get; set; }
        public string LockboxNum { get; set; }
        public string ReceiptDate { get; set; }
        public string Batch { get; set; }
        public string BatchItem { get; set; }
        public decimal? CheckAmount { get; set; }
        public string RoutingNum { get; set; }
        public string AccountNum { get; set; }
        public string CheckNum { get; set; }
        public string CheckImg { get; set; }
        public string EnvelopeImg { get; set; }
        public string InvoiceImg { get; set; }
        public string PolicyNumber { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public string DirectBillCarrierId { get; set; }
        public SuspendedPayerDto Payer { get; set; }
        public ReturnSuspendedDetailViewDto ReturnDetail { get; set; }
    }

    public class ReturnSuspendedDetailViewDto
    {
        public Guid Id { get; set; }
        public string ReturnDate { get; set; }
        public string Payee { get; set; }
        public string CheckIssueDate { get; set; }
        public string CheckNumber { get; set; }
        public string CheckClearDate { get; set; }
        public string CheckEscheatDate { get; set; }
    }
}

