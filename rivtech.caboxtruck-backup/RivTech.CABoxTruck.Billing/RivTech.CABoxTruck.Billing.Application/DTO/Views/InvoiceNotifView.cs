﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class InvoiceNotifView: InvoiceDto
    {
        public bool IsRecurringPayment { get; set; }
    }
}
