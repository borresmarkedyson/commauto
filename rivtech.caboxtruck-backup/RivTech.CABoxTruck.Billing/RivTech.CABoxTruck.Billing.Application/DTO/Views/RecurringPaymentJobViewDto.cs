﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class RecurringPaymentJobViewDto
    {
        public List<RecurringPaymentViewDto> RecurringPaymentView { get; set; }
        public List<FailedRiskViewDto> FailedRisk { get; set; }
    }
}
