﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class RecurringPaymentViewDto
    {
        public Guid RiskId { get; set; }
        public decimal Amount { get; set; }
        public string AccountNumber { get; set; }
        public string Reference { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PaymentPlan { get; set; }
        public string InstrumentType { get; set; }
        public string Email { get; set; }

        public void SetDetail(string paymentPlanId, PaymentAccount defaultAccount)
        {
            AccountNumber = defaultAccount.Description;
            PaymentPlan = Enumeration.GetEnumerationById<PaymentPlan>(paymentPlanId).Description;
            Email = defaultAccount.Payer.Email;
        }
    }
}

