﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class RewriteViewDto
    {
        public BindViewDto Bind { get; set; }
        public PolicyCancellationInvoiceDto Cancellation { get; set; }
        public List<TransferPaymentDto> TransferredPayment { get; set; }
    }
}
