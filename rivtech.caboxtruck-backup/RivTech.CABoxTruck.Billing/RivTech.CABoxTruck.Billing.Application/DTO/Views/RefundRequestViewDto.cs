﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class RefundRequestViewDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public decimal RefundAmount { get; set; }
        public decimal OverpaymentAmount { get; set; }
        public decimal TotalTransactionAmount { get; set; }
        public decimal TotalPaymentAmount { get; set; }
        public string RefundToType { get; set; }
        public string RefundPayeeName { get; set; }
        public string RefundPayeeAddress { get; set; }
        public bool IsChecked { get; set; }
    }
}
