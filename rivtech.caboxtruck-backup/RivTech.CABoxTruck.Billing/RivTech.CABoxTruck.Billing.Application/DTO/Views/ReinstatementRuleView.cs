﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class ReinstatementRuleView
    {
        public Guid RiskId { get; set; }
        public List<Guid> SuspendedPaymentId { get; set; }
        public decimal TotalSuspendedPaymentAmount { get; set; }
        public decimal TotalPastDueAmount { get; set; }
        public decimal TotalPaymentAmount { get; set; }
    }
}
