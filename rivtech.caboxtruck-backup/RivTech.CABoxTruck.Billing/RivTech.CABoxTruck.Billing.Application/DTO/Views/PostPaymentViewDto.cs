﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Views
{
    public class PostPaymentViewDto
    {
        public Guid RiskId { get; set; }
        public string Instrument { get; set; }
        public string Account { get; set; }
        public string PaymentPlan { get; set; }
        public string CreatedDate { get; set; }
        public PaymentDto Payment { get; set; }
        public bool IsRecurringPayment { get; set; }

        public PostPaymentViewDto(PaymentDto payment, PaymentProviderResponseDto response, Risk risk, PaymentProfileDto profile = null )
        {
            RiskId = risk.Id;
            Instrument = Enumeration.GetEnumerationById<InstrumentType>(payment.InstrumentId).Description;
            PaymentPlan = Enumeration.GetEnumerationById<PaymentPlan>(risk.PaymentPlanId).Description;
            CreatedDate = payment.CreatedDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            Payment = payment;
            IsRecurringPayment = profile == null ? false : profile.IsRecurringPayment;

            if (response == null) return;
            Account = payment.InstrumentId == InstrumentType.RecurringECheck.Id || payment.InstrumentId == InstrumentType.EFT.Id ?
                $"Bank Account {response.AccountNumber}" :
                $"{response.AccountType} {response.AccountNumber}";
        }
    }
}
