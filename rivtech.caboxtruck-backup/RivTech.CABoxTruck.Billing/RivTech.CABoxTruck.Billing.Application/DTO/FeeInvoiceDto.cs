﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class FeeInvoiceDto
    {
        public Guid TransactionId { get; set; }
        public Guid? PaymentId { get; set; }
        public InvoiceDto Invoice { get; set; }
    }
}
