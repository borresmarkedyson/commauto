﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PaymentAccountDto
    {
        public Guid Id { get; set; }
        public Guid PaymentProfileId { get; set; }
        public string InstrumentTypeId { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public Guid PayerId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
        public bool InsuredAgree { get; set; }
        public bool UwAgree { get; set; }

        public EnumerationDto InstrumentType { get; set; }
        public CustomerDto Payer { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
    }
}
