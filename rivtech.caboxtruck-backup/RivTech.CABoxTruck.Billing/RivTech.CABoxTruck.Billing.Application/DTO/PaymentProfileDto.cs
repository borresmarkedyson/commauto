﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PaymentProfileDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsRecurringPayment { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }

        public List<PaymentAccountDto> PaymentAccounts { get; set; }
        public ApplicationUserDto CreatedBy { get; private set; }

        public PaymentProfileDto()
        {
            PaymentAccounts = new List<PaymentAccountDto>();
        }
    }
}
