﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class AuthNetAccountDto
    {
        public Guid Id { get; set; }
        public Guid PaymentAccountId { get; private set; }
        public Guid AuthNetProfileId { get; private set; }
        public string CustomerPaymentId { get; private set; }

        public PaymentAccountDto PaymentAccount { get; private set; }
    }
}
