﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class NsfCountDto
    {
        public int NsfCount { get; set; }
    }
}
