﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class FinalCancellationRiskDto
    {
        public Guid RiskId { get; set; }
        public decimal BalanceDue { get; set; }
        public DateTime RequestDate { get; set; }
    }
}
