﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class ChangePaymentPlanPaymentDetailsDto
    {
        public decimal TotalAmount { get; set; }
        public decimal PremiumAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal TaxAmount { get; set; }
    }
}
