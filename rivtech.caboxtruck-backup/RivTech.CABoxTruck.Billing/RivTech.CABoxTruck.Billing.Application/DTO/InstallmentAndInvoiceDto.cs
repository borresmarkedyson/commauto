﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class InstallmentAndInvoiceDto
    {
        public Guid? InvoiceId { get; set; }
        public Guid? InstallmentId { get; set; }
        public string Status { get; set; }
        public string InstallmentType { get; set; }
        public decimal Premium { get; set; }
        public decimal Tax { get; set; }
        public decimal Fee { get; set; }
        public decimal TotalBilled { get; set; }
        public decimal Balance { get; set; }
        public decimal TotalDue { get; set; }
        public DateTime BillDate { get; set; }
        public DateTime DueDate { get; set; }
        public string InvoiceNumber { get; set; }
        public bool IsFlatCancelInvoice { get; set; }
        public decimal FutureInstallmentFee { get; set; }
        public decimal TotalDueWithoutFutureInstallmentFee { get; set; }

    }
}
