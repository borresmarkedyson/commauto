﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class FlatCancelNopcRiskDto
    {
        public Guid RiskId { get; set; }
        public DateTime NoticeDate { get; set; }
        public decimal AmountBilled { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal BalanceDue { get; set; }
        public decimal FullAmountDue { get; set; }
        public decimal MinimumAmountDue { get; set; }
        public DateTime CancellationDate { get; set; }
    }
}
