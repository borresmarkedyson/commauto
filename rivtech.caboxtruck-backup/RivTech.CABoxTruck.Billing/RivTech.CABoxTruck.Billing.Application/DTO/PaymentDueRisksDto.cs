﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PaymentDueRisksDto
    {
        public List<PaymentDueRiskDto> PaymentDueRisks { get; set; }
        public int Count { get; set; }
    }
}
