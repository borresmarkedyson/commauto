﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class ApplicationUserDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime UserDate { get; set; }
    }
}
