﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class InvoiceDetailDto
    {
        public Guid Id { get; set; }
        public AmountSubTypeDto AmountSubType { get; set; }
        public string AmountSubTypeId { get; set; }
        public decimal InvoicedAmount { get; set; }
    }
}
