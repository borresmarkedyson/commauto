﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PaymentDueRiskDto
    {
        public Guid RiskId { get; set; }
    }
}
