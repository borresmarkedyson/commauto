﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedPaymentDto
    {
        public Guid Id { get; set; }
        public DateTime ReceiptDate { get; set; }
        public string Time { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyId { get; set; }
        public decimal Amount { get; set; }
        public string SourceId { get; set; }
        public string ReasonId { get; set; }
        public string StatusId { get; set; }
        public string Comment { get; set; }
        public bool CheckRequested { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
        public Guid? PaymentId { get; set; }
        public string DirectBillCarrierId { get; set; }

        public SuspendedDetailDto SuspendedDetail { get; set; }
        public SuspendedCheckDto SuspendedCheck { get; set; }
        public SuspendedImageDto SuspendedImage { get; set; }
        public SuspendedPayerDto SuspendedPayer { get; set; }
        public EnumerationDto Source { get; set; }
        public EnumerationDto Reason { get; set; }
        public EnumerationDto Status { get; set; }
        public ApplicationUserDto CreatedBy { get; set; }
        public PaymentDto Payment { get; set; }
        public EnumerationDto DirectBillCarrier { get; set; }
    }
}
