﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedDetailDto
    {
        public Guid Id { get; set; }
        public Guid SuspendedPaymentId { get; set; }
        public string TransactionNum { get; set; }
        public string LockboxNum { get; set; }
        public string Batch { get; set; }
        public string BatchItem { get; set; }
        public string Number { get; set; }
        public string EnvelopeNum { get; set; }
        public string Envelope { get; set; }
        public string InvoicePage { get; set; }
        public string ClientId { get; set; }
        public string ClientName{ get; set; }
        public DateTime? SettlementDate { get; set; }
        public string GroupId { get; set; }
        public string GroupDescription { get; set; }
    }
}
