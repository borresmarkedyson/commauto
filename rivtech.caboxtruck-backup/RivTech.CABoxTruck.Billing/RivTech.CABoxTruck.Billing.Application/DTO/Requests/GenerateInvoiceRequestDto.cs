﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class GenerateInvoiceRequestDto
    {
        public string AppId { get; set; }
        public DateTime? CurrentDateTime { get; set; }

        public GenerateInvoiceRequestDto() { }

        public GenerateInvoiceRequestDto(string appId, DateTime? currentDateTime)
        {
            AppId = appId;
            CurrentDateTime = currentDateTime;
        }
    }
}
