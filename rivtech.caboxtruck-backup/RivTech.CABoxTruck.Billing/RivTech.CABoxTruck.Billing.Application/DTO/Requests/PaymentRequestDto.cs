﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PaymentRequestDto
    {
        public string AppId { get; set; }
        public Guid RiskId { get; set; }
        public PaymentSummaryDto PaymentSummary { get; set; }
        public BillingDetailDto BillingDetail { get; set; }
        public PaymentSummaryCCEFTDto PaymentPortalRequest { get; set; }

        public PaymentRequestDto() { }

        public PaymentRequestDto(Guid riskId, InvoiceDto invoice, BindRequestDto request, DateTime requestDate)
        {
            RiskId = riskId;
            BillingDetail = request.BillingDetail;
            PaymentSummary = new PaymentSummaryDto(invoice, request, requestDate);
            AppId = request.AppId;
        }

        public PaymentRequestDto(Guid riskId, decimal percentDeposit, BindRequestDto request, DateTime requestDate)
        {
            RiskId = riskId;
            BillingDetail = request.BillingDetail;
            PaymentSummary = new PaymentSummaryDto(request, percentDeposit, requestDate);
            AppId = request.AppId;
        }

        public PaymentRequestDto(Guid riskId, decimal percentDeposit, AuthorizePaymentRequestDto request, DateTime requestDate)
        {
            RiskId = riskId;
            BillingDetail = request.BillingDetail;
            PaymentSummary = new PaymentSummaryDto(request, percentDeposit, requestDate);
            AppId = request.AppId;
        }

        public PaymentRequestDto(Guid riskId, ChangePaymentPlanRequestDto request, string appId, DateTime requestDate)
        {
            RiskId = riskId;
            BillingDetail = request.BillingDetail;
            PaymentSummary = new PaymentSummaryDto(request, requestDate);
            AppId = appId;
        }

        public PaymentRequestDto(SuspendedPaymentDto suspendedPayment, PostSuspendedPaymentRequestDto request)
        {
            RiskId = request.RiskId;
            PaymentSummary = new PaymentSummaryDto(suspendedPayment);
            AppId = request.AppId;

            var payer = suspendedPayment.SuspendedPayer;
            if (payer == null) return;
            BillingDetail = new BillingDetailDto()
            {
                BillingAddress = new BillingAddressDto()
                {
                    FirstName = payer.FirstName,
                    LastName = payer.LastName,
                    Address = $"{payer.Address1} {payer.Address2}",
                    City = payer.City,
                    State = payer.State,
                    Zip = payer.Zip,
                }
            };
        }

        public PaymentRequestDto(Guid riskId, decimal dueBalance, DateTime requestDate)
        {
            RiskId = riskId;
            PaymentSummary = new PaymentSummaryDto(dueBalance, requestDate);
        }
    }
}
