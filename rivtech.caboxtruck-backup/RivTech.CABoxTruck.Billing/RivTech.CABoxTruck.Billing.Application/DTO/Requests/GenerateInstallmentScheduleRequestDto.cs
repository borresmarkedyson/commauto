﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class GenerateInstallmentScheduleRequestDto
    {
        public DateTime StartingDueDate { get; set; }
        public string PaymentPlan { get; set; }
        public string AppId { get; set; }
        public string PolicyNumber { get; set; }
    }
}
