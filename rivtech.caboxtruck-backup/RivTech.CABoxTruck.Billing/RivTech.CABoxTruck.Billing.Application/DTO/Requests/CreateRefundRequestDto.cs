﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class CreateRefundRequestDto
    {
        public Guid RiskId { get; set; }
        public decimal Amount { get; set; }
        public string RefundToTypeId { get; set; }
        public RefundPayeeDto RefundPayee { get; set; }
    }
}
