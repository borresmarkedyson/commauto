﻿namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PostRefundRequestDto
    {
        public string EncryptionPublicKey { get; set; }
        public string AppId { get; set; }
    }
}
