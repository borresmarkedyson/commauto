﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdateRiskRequestDto
    {
        public Guid RiskId { get; set; }
        public string PolicyNumber { get; set; }
    }
}
