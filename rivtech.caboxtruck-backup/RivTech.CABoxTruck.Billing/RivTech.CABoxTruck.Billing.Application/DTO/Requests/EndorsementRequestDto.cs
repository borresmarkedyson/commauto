﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class EndorsementRequestDto
    {
        public Guid RiskId { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public string AppId { get; set; }
        public DateTime EndorsementEffectiveDate { get; set; }
    }
}
