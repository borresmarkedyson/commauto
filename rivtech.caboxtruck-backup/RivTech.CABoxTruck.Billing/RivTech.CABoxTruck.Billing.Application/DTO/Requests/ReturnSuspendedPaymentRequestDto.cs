﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ReturnSuspendedPaymentRequestDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public SuspendedPayeeRequestDto Payee { get; set; }
        public string Comment { get; set; }
    }

    public class SuspendedPayeeRequestDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
