﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class CreateSuspendedPaymentRequestDto
    {
        public DateTime ReceiptDate { get; set; }
        public string Time { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyId { get; set; }
        public decimal Amount { get; set; }
        public string SourceId { get; set; }
        public string ReasonId { get; set; }
        public string Comment { get; set; }
        public string DirectBillCarrierId { get; set; }

        public SuspendedDetailDto SuspendedDetail { get; set; }
        public SuspendedCheckDto SuspendedCheck { get; set; }
        public SuspendedImageDto SuspendedImage { get; set; }
        public SuspendedPayerDto SuspendedPayer { get; set; }
    }
}
