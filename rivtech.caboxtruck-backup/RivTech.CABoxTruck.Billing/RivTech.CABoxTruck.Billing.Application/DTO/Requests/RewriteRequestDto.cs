﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class RewriteRequestDto
    {
        public Guid CurrentRiskId { get; set; }
        public Guid RewriteRiskId { get; set; }
        public BindRequestDto BindRequest { get; set; }
        public PolicyCancellationRequestDto CancelRequest { get; set; }
    }
}
