﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ComputeRiskTaxResponseDto
    {
        public TaxDetailsDto TaxDetails { get; set; }
    }
}
