﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class TagForManualReinstatementRequestDto
    {
        public List<Guid> RiskIds { get; set; }
    }
}
