﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PaymentSummaryDto
    {
        public DateTime EffectiveDate { get; set; }
        public string InstrumentId { get; set; }
        public decimal Amount { get; set; }
        public decimal PremiumAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal PremiumFeeAmount { get; set; }
        public decimal PremiumFeeTaxAmount { get; set; }
        public decimal TransactionFeeAmount { get; set; }
        public decimal TransactionTaxAmount { get; set; }
        public string Comment { get; set; }
        public bool IsRecurringPayment { get; set; }
        public string Email { get; set; }
        public decimal TotalALPremiumAmount { get; set; }
        public decimal TotalPDPremiumAmount { get; set; }
        public decimal ALCommissionAmount { get; set; }
        public decimal PDCommissionAmount { get; set; }
        public decimal PolicyOverpayment { get; set; }


        public PaymentSummaryDto() { }

        public PaymentSummaryDto(InvoiceDto invoice, BindRequestDto request, DateTime requestDate)
        {
            decimal totalPremium = 0;
            decimal totalPremiumFees = 0;
            decimal totalTransactionFees = 0;
            decimal totalTaxes = 0;
            decimal totalTransactionTaxes = 0;

            foreach (var invoiceDetail in invoice.InvoiceDetails)
            {
                if (invoiceDetail.AmountSubType.AmountType.Id == AmountType.Premium.Id)
                {
                    totalPremium += invoiceDetail.InvoicedAmount;
                }
                else if (invoiceDetail.AmountSubType.AmountType.Id == AmountType.PremiumFee.Id)
                {
                    totalPremiumFees += invoiceDetail.InvoicedAmount;
                }
                else if (invoiceDetail.AmountSubType.AmountType.Id == AmountType.TransactionFee.Id)
                {
                    totalTransactionFees += invoiceDetail.InvoicedAmount;
                }
                else if (invoiceDetail.AmountSubType.AmountType.Id == AmountType.Tax.Id)
                {
                    totalTaxes += invoiceDetail.InvoicedAmount;
                }
                else if (invoiceDetail.AmountSubType.AmountType.Id == AmountType.TransactionTax.Id)
                {
                    totalTransactionTaxes += invoiceDetail.InvoicedAmount;
                }
            }

            EffectiveDate = requestDate.Date;
            InstrumentId = request.InstrumentId;
            Amount = invoice.CurrentAmountInvoiced;
            PremiumAmount = totalPremium;
            PremiumFeeAmount = totalPremiumFees;
            TransactionFeeAmount = totalTransactionFees;
            TransactionTaxAmount = totalTransactionTaxes;
            FeeAmount = totalPremiumFees + totalTransactionFees;
            TaxAmount = totalTaxes + totalTransactionTaxes;
            Comment = "Payment";
        }

        public PaymentSummaryDto(BindRequestDto request, decimal percentDeposit, DateTime requestDate)
        {
            decimal totalPremium = 0;
            decimal totalPremiumFees = 0;
            decimal totalPremiumTaxes = 0;
            decimal totalTransactionFees = 0;
            decimal totalTransactionTaxes = 0;
            foreach (var transactionDetail in request.TransactionDetails)
            {
                if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.Premium.Id)
                {
                    totalPremium += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.PremiumFee.Id)
                {
                    totalPremiumFees += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.TransactionFee.Id)
                {
                    totalTransactionFees += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.Tax.Id)
                {
                    totalPremiumTaxes += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.TransactionTax.Id)
                {
                    totalTransactionTaxes += transactionDetail.Amount;
                }
            }

            EffectiveDate = requestDate.Date;
            InstrumentId = request.InstrumentId;
            PremiumAmount = totalPremium * percentDeposit;
            PremiumFeeAmount = totalPremiumFees;
            TransactionFeeAmount = totalTransactionFees;
            FeeAmount = totalPremiumFees + totalTransactionFees;
            TaxAmount = totalPremiumTaxes + totalTransactionTaxes;
            Amount = PremiumAmount + FeeAmount + TaxAmount;
            Comment = "Payment";
            IsRecurringPayment = request.IsRecurringPayment;
            Email = request.Email;

            if (request.TotalAmountCharged != Amount)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_AMOUNT_CHARGED_MISMATCH);
            }
        }

        public PaymentSummaryDto(AuthorizePaymentRequestDto request, decimal percentDeposit, DateTime requestDate)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            foreach (var transactionDetail in request.TransactionDetails)
            {
                if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.Premium.Id)
                {
                    totalPremium += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.PremiumFee.Id
                    || Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.TransactionFee.Id)
                {
                    totalFees += transactionDetail.Amount;
                }
                else if (Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.Tax.Id
                    || Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubType.Id).AmountTypeId == AmountType.TransactionTax.Id)
                {
                    tax += transactionDetail.Amount;
                }
            }

            EffectiveDate = requestDate.Date;
            InstrumentId = request.InstrumentId;
            PremiumAmount = totalPremium * percentDeposit;
            FeeAmount = totalFees;
            TaxAmount = tax;
            Amount = PremiumAmount + FeeAmount + TaxAmount;
            Comment = "Payment";
            IsRecurringPayment = request.IsRecurringPayment;
            Email = request.Email;

            if (request.TotalAmountCharged != Amount)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_AMOUNT_CHARGED_MISMATCH);
            }
        }

        public PaymentSummaryDto(ChangePaymentPlanRequestDto request, DateTime requestDate)
        {
            EffectiveDate = requestDate.Date;
            InstrumentId = request.InstrumentId;
            Amount = request.PaymentDetails.TotalAmount;
            PremiumAmount = request.PaymentDetails.PremiumAmount;
            FeeAmount = request.PaymentDetails.FeeAmount;
            TaxAmount = request.PaymentDetails.TaxAmount;
            Comment = "Bill Plan Change Payment";
        }

        public PaymentSummaryDto(SuspendedPaymentDto suspendedPayment)
        {
            EffectiveDate = suspendedPayment.ReceiptDate;
            InstrumentId = suspendedPayment.SourceId;
            Amount = suspendedPayment.Amount;
            PremiumAmount = suspendedPayment.Amount;
            FeeAmount = 0;
            TaxAmount = 0;
            Comment = suspendedPayment.Comment;
        }

        public PaymentSummaryDto(decimal dueBalance, DateTime requestDate)
        {
            EffectiveDate = requestDate.Date;
            Amount = dueBalance;
            PremiumAmount = dueBalance;
            FeeAmount = 0;
            TaxAmount = 0;
        }
    }
}
