﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class RefundRequestDto
    {
        public Guid RiskId { get; set; }
        public string InstrumentId { get; set; }
        public decimal Amount { get; set; }
        public string Comments { get; set; }
        public PayeeInfoDto PayeeInfo { get; set; }
        public string CheckNumber { get; set; }
    }
}
