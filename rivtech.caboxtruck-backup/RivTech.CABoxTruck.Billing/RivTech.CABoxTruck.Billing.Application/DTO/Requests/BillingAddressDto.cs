﻿
namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class BillingAddressDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
    }
}
