﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ChargePaymentAccountRequestDto
    {
        public Guid RiskId { get; set; }
        public decimal Amount { get; set; }
    }
}
