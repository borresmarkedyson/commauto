﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdateDefaultAccountRequestDto
    {
        public Guid Id { get; set; }
        public string AppId { get; set; }
        public Guid RiskId { get; set; }
        public bool IsDefault { get; set; }
    }
}
