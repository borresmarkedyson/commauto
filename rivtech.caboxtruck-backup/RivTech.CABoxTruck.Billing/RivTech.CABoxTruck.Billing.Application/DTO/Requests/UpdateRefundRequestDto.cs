﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdateRefundRequestDto
    {
        public Guid RefundRequestId { get; set; }
        public decimal Amount { get; set; }
        public string RefundToTypeId { get; set; }
        public RefundPayeeDto RefundPayee { get; set; }
    }
}

