﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class VoidSuspendedPaymentRequestDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public string Comment { get; set; }
    }
}
