﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class AddTransactionFeeRequestDto
    {
        public DateTime AddDate { get; set; }
        public Guid RiskId { get; set; }
        public string AmountSubTypeId { get; set; }
        public decimal Amount { get; set; }
        public string AppId { get; set; }
        public string Description { get; set; }
    }
}
