﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PaymentAccountRequestDto
    {
        public Guid Id { get; set; }
        public string AppId { get; set; }
        public Guid RiskId { get; set; }
        public string InstrumentTypeId { get; set; }
        public bool IsDefault { get; set; }
        public BillingDetailDto BillingDetail { get; set; }
    }
}
