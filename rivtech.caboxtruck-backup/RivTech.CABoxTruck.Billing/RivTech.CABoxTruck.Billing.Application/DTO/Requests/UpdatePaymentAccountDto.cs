﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdatePaymentAccountDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string Email { get; set; }
        //public string ExpirationDate { get; set; }
        public string AppId { get; set; }
    }
}
