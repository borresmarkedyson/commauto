﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdateSuspendedPaymentRequestDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public string ReasonId { get; set; }
    }
}

