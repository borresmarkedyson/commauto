﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class SetClearDateRequestDto
    {
        public Guid RiskId { get; set; }
        public Guid PaymentId { get; set; }
        public DateTime? ClearDate { get; set; }
    }
}
