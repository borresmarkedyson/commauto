﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PostSuspendedPaymentRequestDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public Guid RiskId { get; set; }
        public string AppId { get; set; }
    }
}
