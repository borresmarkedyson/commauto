﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PolicyCancellationRequestDto
    {
        public Guid RiskId { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public bool IsFlatCancel { get; set; }
        public string AppId { get; set; }
        public bool IsReasonNonPayment { get; set; }
        public bool IsManualCancellation { get; set; }
    }
}
