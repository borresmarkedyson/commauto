﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class InstallmentInvoiceBeforeBindRequestDto
    {
        public DateTime EffectiveDate { get; set; }
        public string PaymentPlan { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public string AppId { get; set; }
        public decimal PercentOfOutstanding { get; set; }
        public string StateCode { get; set; }
    }
}
