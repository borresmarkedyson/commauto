﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class UpdateReturnSuspendedPaymentRequestDto
    {
        public Guid SuspendedPaymentId { get; set; }
        public DateTime? CheckIssueDate { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? CheckClearDate { get; set; }
        public DateTime? CheckEscheatDate { get; set; }
    }
}
