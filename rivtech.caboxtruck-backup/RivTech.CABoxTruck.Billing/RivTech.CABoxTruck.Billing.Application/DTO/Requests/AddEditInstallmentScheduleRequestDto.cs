﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class AddEditInstallmentScheduleRequestDto
    {
        public Guid? InstallmentId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
    }
}
