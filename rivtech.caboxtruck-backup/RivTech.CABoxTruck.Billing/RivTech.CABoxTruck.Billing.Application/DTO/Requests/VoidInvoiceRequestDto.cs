﻿namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class VoidInvoiceRequestDto
    {
        public string InvoiceNumber { get; set; }
        public string AppId { get; set; }
    }
}
