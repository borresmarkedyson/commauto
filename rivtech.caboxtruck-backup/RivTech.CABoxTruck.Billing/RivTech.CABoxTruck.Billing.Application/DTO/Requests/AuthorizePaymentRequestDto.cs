﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class AuthorizePaymentRequestDto
    {
        public DateTime EffectiveDate { get; set; }
        public string InstrumentId { get; set; }
        public string PaymentPlan { get; set; }
        public BillingDetailDto BillingDetail { get; set; } = new BillingDetailDto();
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public string AppId { get; set; }
        public decimal TotalAmountCharged { get; set; }
        public bool WillPayLater { get; set; }
        public bool IsRecurringPayment { get; set; }
        public string Email { get; set; }
    }
}
