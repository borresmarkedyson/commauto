﻿
namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class CreditCardDto
    {
        public string CardCode { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string CreditCardTypeId { get; set; }
    }
}
