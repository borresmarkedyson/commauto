﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ComputeRiskTaxRequestDto
    {
        public DateTime EffectiveDate { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public string StateCode { get; set; }
        public decimal? InstallmentCount { get; set; }
    }
}
