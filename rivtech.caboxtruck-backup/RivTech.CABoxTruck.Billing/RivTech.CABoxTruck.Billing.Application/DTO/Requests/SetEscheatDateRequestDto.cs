﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class SetEscheatDateRequestDto
    {
        public Guid RiskId { get; set; }
        public Guid PaymentId { get; set; }
        public DateTime? EscheatDate { get; set; }
        public string Comments { get; set; }
    }
}
