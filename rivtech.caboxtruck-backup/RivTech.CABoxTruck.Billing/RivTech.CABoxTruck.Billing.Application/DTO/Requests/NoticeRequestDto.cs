﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class NoticeRequestDto
    {
        public string AppId { get; set; }
        public Guid RiskId { get; set; }
    }
}
