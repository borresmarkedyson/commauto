﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class TransferPaymentRequestDto
    {
        public Guid FromRiskId { get; set; }
        public Guid ToRiskId { get; set; }
        public string FromPolicyNumber { get; set; }
        public string ToPolicyNumber { get; set; }
        public Guid PaymentId { get; set; }
        public string Comments { get; set; }
        public bool IsSuspendedPaymentPostJob { get; set; }
    }
}
