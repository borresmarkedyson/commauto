﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ChangePaymentPlanRequestDto
    {
        public Guid RiskId { get; set; }
        public string InstrumentId { get; set; }
        public string NewPaymentPlanId { get; set; }
        public BillingDetailDto BillingDetail { get; set; }
        public ChangePaymentPlanPaymentDetailsDto PaymentDetails { get; set; }
        public string EmailAddress { get; set; }
        public string AppId { get; set; }
    }
}
