﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PendingCancellationRequestDto
    {
        public Guid RiskId { get; set; }
        public DateTime CancellationEffectiveDate { get; set; }
        public string AppId { get; set; }
        public bool IsReasonNonPayment { get; set; }
    }
}
