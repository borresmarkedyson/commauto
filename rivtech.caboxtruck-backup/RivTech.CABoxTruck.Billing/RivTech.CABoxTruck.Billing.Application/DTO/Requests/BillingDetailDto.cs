﻿
namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class BillingDetailDto
    {
        public CreditCardDto CreditCard { get; set; }
        public BillingAddressDto BillingAddress { get; set; } = new BillingAddressDto();
        public BankAccountDto BankAccount { get; set; }
        public bool InsuredAgree { get; set; } = true; //always true, ui does not send this value now
        public bool UwAgree { get; set; } = true; //always true, ui does not send this value now
    }
}
