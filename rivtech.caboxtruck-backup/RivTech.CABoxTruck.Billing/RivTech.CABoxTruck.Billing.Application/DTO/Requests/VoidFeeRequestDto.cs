﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class VoidFeeRequestDto
    {
        public Guid Id { get; set; }
        public Guid RiskId { get; set; }
        public string AppId { get; set; }

        public VoidFeeRequestDto() { }

        public VoidFeeRequestDto(Guid riskId, Guid feeId, string appId)
        {
            RiskId = riskId;
            Id = feeId;
            AppId = appId;
        }
    }
}
