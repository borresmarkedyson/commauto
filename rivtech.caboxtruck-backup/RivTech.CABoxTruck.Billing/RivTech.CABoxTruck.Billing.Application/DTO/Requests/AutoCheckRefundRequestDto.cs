﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class AutoCheckRefundRequestDto
    {
        public List<RefundPayeeRequestDto> RefundPayeeRequest { get; set; }
    }

    public class RefundPayeeRequestDto
    {
        public Guid RiskId { get; set; }
        public RefundPayeeDto RefundPayee { get; set; }
    }
}
