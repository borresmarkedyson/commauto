﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class PaymentProfileRequestDto
    {
        public string AppId { get; set; }
        public Guid RiskId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsRecurringPayment { get; set; }
        public PaymentAccountRequestDto PaymentAccount { get; set; }
    }
}
