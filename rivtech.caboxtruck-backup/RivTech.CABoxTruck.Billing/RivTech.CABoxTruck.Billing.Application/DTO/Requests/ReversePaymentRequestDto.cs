﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ReversePaymentRequestDto
    {
        public Guid RiskId { get; set; }
        public Guid PaymentId { get; set; }
        public string ReversalTypeId { get; set; }
        public string Comment { get; set; }
        public string AppId { get; set; }
    }
}
