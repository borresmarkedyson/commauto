﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class FTPDownloadRequest
    {
        public string FTPSettingId { get; set; }
        public string FileName { get; set; }
        public string DLfileExtension { get; set; }
    }
}
