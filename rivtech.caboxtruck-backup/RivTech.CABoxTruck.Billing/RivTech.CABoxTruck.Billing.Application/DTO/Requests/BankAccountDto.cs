﻿
namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class BankAccountDto
    {
        public string Payer { get; set; }
        public string AccountType { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string NameOnAccount { get; set; }
        public string BankName { get; set; }
    }
}
