﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class ReinstateRequestDto
    {
        public Guid RiskId { get; set; }
        public List<TransactionDetailDto> TransactionDetails { get; set; }
        public string AppId { get; set; }
    }
}
