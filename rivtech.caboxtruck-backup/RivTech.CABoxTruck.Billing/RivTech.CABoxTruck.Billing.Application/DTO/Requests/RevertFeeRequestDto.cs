﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO.Requests
{
    public class RevertFeeRequestDto
    {
        public Guid TransactionId { get; set; }
        public Guid? InvoiceId { get; set; }
        public Guid? PaymentId { get; set; }
        public string AppId { get; set; }
    }
}
