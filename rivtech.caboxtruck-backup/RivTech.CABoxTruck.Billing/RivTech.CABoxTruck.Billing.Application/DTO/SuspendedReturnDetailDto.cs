﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedReturnDetailDto
    {
        public Guid Id { get; set; }
        public Guid SuspendedPaymentId { get; set; }
        public string PayeeId { get; set; }
        public string CheckNumber { get; set; }
        public string CheckIssueDate { get; set; }
        public string CheckCopyLink { get; set; }
        public DateTime? CheckClearDate { get; set; }
        public DateTime? CheckEscheatDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
    }
}
