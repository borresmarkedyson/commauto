﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    /// <summary>
    /// An Entity(Agregate Root) for tax computation and its breakdown.
    /// </summary>
    public class TaxDetailsDto
    {
        public IEnumerable<TaxDetailsBreakdownDto> TaxDetailsBreakdowns { get; set; }
        public decimal TotalSLTax { get; set; }
        public decimal TotalStampingFee { get; set; }
        public decimal TotalTax { get; set; }
    }
}
