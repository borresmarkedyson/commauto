﻿using System;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class SuspendedPayerDto
    {
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
