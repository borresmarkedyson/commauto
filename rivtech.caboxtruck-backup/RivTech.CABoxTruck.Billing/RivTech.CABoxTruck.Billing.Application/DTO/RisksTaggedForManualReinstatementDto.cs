﻿using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class RisksTaggedForManualReinstatementDto
    {
        public List<Guid> RiskIds { get; set; }
    }
}
