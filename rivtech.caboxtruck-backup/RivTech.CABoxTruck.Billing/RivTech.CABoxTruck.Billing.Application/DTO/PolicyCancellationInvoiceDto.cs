﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class PolicyCancellationInvoiceDto : InvoiceDto
    {
        public decimal TotalPaid { get; set; }
        public decimal MinimumAmountDue { get; set; }
        public decimal FullAmountDue { get; set; }
        public bool IsNsf { get; set; }
    }
}
