﻿namespace RivTech.CABoxTruck.Billing.Application.DTO
{
    public class AmountSubTypeDto
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public EnumerationDto AmountType { get; set; }
    }
}
