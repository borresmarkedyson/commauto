﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class BalanceDueService : IBalanceDueService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;
        private readonly INoticeRepository _noticeRepository;
        private readonly IBillingSummaryService _billingSummaryService;

        public BalanceDueService(
            IRiskRepository riskRepository,
            IPaymentRepository paymentRepository,
            IInvoiceRepository invoiceRepository,
            IValidationService validationService,
            IUserDateService userDateService,
            INoticeRepository noticeRepository,
            IBillingSummaryService billingSummaryService
        )
        {
            _riskRepository = riskRepository;
            _paymentRepository = paymentRepository;
            _invoiceRepository = invoiceRepository;
            _validationService = validationService;
            _userDateService = userDateService;
            _noticeRepository = noticeRepository;
            _billingSummaryService = billingSummaryService;
        }

        public async Task<List<BalanceDueRiskDto>> GetBalanceDueRisks(string appId)
        {
            List<BalanceDueRiskDto> balanceDueRisks = new List<BalanceDueRiskDto>();
            DateTime requestDate = _userDateService.GetUser().UserDate;
            var balanceDueRisksFromDb = await _noticeRepository.GetBalanceDueRisks(requestDate, appId);
            foreach (var risk in balanceDueRisksFromDb)
            {
                var riskFromDb = await _riskRepository.FindAsync(x => x.Id == risk.RiskId, x => x.NoticeHistory);
                if (riskFromDb.NoticeHistory == null)
                {
                    riskFromDb.InitializeNoticeHistory();
                }
                riskFromDb.NoticeHistory.BalanceDueNoticeDate = requestDate;

                balanceDueRisks.Add(new BalanceDueRiskDto
                {
                    RiskId = risk.RiskId,
                    NoticeDate = risk.NoticeDate,
                    DepositAmountBilled = risk.DepositAmountBilled,
                    AmountPaid = risk.AmountPaid,
                    BalanceDue = risk.BalanceDue,
                    PayoffAmount = risk.PayoffAmount,
                    IsPaymentTransferred = risk.IsPaymentTransferred
                });
            }

            await _riskRepository.SaveChangesAsync();
            return balanceDueRisks;
        }

        public async Task<BalanceDueRiskDto> SetBalanceDue(NoticeRequestDto request)
        {
            var validator = new NoticeRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            DateTime requestDate = _userDateService.GetUser().UserDate;

            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == request.RiskId && requestDate.Date >= x.EffectiveDate.Date
                && x.NoticeHistory.BalanceDueNoticeDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == request.AppId, x => x.NoticeHistory);

            if (riskFromDb == null)
            {
                return null;
            }

            var generatedInvoices = await _invoiceRepository.GetAllNotVoided(request.RiskId);
            if (generatedInvoices.Count != 1)
            {
                return null;
            }

            var totalPayment = await _paymentRepository.GetTotalPaymentAsync(request.RiskId);
            var totalInvoiced = await _invoiceRepository.GetTotalInvoicedAmountAsync(request.RiskId);
            if (totalPayment > 0 && totalPayment < totalInvoiced)
            {
                if (riskFromDb.NoticeHistory == null)
                {
                    riskFromDb.InitializeNoticeHistory();
                }
                riskFromDb.NoticeHistory.BalanceDueNoticeDate = requestDate;
                await _riskRepository.SaveChangesAsync();
                return new BalanceDueRiskDto
                {
                    RiskId = request.RiskId,
                    NoticeDate = requestDate,
                    DepositAmountBilled = totalInvoiced,
                    AmountPaid = totalPayment,
                    BalanceDue = totalInvoiced - totalPayment
                };
            }
            else
            {
                return null;
            }
        }
    }
}
