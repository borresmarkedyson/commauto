﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class InvoiceService : IInvoiceService
    {
        private decimal totalAmountToInvoice = 0;
        private decimal totalPremium = 0;
        private decimal initialTotalPremium = 0;
        private decimal totalFees = 0;
        private decimal totalTax = 0;
        private decimal invoicedPremium = 0;
        private decimal invoicedFees = 0;
        private decimal invoicedTax = 0;
        private decimal previousBalance = 0;
        private Transaction installmentFeeTransaction = null;

        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceNumberService _invoiceNumberService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ITransactionFeeService _transactionFeeService;
        private readonly IRiskRepository _riskRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IMapper _mapper;
        private readonly IPaymentProfileService _paymentProfileService;
        private readonly IUserDateService _userDateService;
        private readonly IStateTaxRepository _stateTaxRepository;
        private readonly INonTaxableFeeRepository _nonTaxableFeeRepository;

        public InvoiceService(
            IInvoiceRepository invoiceRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceNumberService invoiceNumberService,
            ITransactionRepository transactionRepository,
            IApplicationUserRepository applicationUserRepository,
            IPaymentRepository paymentRepository,
            ITransactionFeeService transactionFeeService,
            IRiskRepository riskRepository,
            IBillingSummaryService billingSummaryService,
            IMapper mapper,
            IPaymentProfileService paymentProfileService,
            IUserDateService userDateService,
            IStateTaxRepository stateTaxRepository,
            INonTaxableFeeRepository nonTaxableFeeRepository
        )
        {
            _invoiceRepository = invoiceRepository;
            _installmentRepository = installmentRepository;
            _invoiceNumberService = invoiceNumberService;
            _transactionRepository = transactionRepository;
            _applicationUserRepository = applicationUserRepository;
            _paymentRepository = paymentRepository;
            _transactionFeeService = transactionFeeService;
            _riskRepository = riskRepository;
            _billingSummaryService = billingSummaryService;
            _mapper = mapper;
            _paymentProfileService = paymentProfileService;
            _userDateService = userDateService;
            _stateTaxRepository = stateTaxRepository;
            _nonTaxableFeeRepository = nonTaxableFeeRepository;
        }

        public async Task<List<InvoiceDto>> GetAllAsync(Guid riskId)
        {
            var result = await _invoiceRepository.GetAllByRisk(riskId);
            var invoices = _mapper.Map<List<InvoiceDto>>(result);

            var billingSummary = await _billingSummaryService.GetBillingSummary(riskId);

            if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description, StringComparison.OrdinalIgnoreCase))
            {
                foreach (var invoice in invoices)
                {
                    ComputePremiumFinancedBrokerCommission(billingSummary, invoice);
                }
            }

            return invoices;
        }

        public async Task<InvoiceDto> GenerateInvoiceAsync(Guid riskId, GenerateInvoiceRequestDto model, bool isBind,
            decimal totalAmountCharged, bool willPayLater = false)
        {
            DateTime currentDate;
            if (isBind)
            {
                currentDate = model.CurrentDateTime.HasValue ? model.CurrentDateTime.Value : _userDateService.GetUser().UserDate.Date;
            }
            else
            {
                currentDate = _userDateService.GetUser().UserDate.Date;
            }

            var installmentFromDb = await _installmentRepository.FindByInvoiceDate(riskId, currentDate);
            if (installmentFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.INSTALLMENT_WITH_INVOICE_DATE_NOT_FOUND);
            }
            else if (installmentFromDb.IsInvoiced)
            {
                throw new ParameterException(ExceptionMessages.INSTALLMENT_ALREADY_INVOICED);
            }

            DateTime dueDate;
            if (isBind)
            {
                previousBalance = 0;
                dueDate = currentDate;
            }
            else
            {
                previousBalance = await ComputePreviousBalance(riskId);
                dueDate = currentDate.AddDays(15);
            }

            List<InvoiceDetail> invoiceDetails = await GetInvoiceDetails(riskId, installmentFromDb, model.AppId, currentDate);

            if (isBind && !willPayLater && totalAmountCharged != totalAmountToInvoice)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_AMOUNT_CHARGED_MISMATCH);
            }

            Invoice invoice = new Invoice(riskId, await _invoiceNumberService.GenerateInvoiceNumber(model.AppId, installmentFromDb.Risk.PolicyNumber), currentDate,
                dueDate, previousBalance, totalAmountToInvoice, invoiceDetails, _userDateService.GetUser());

            await _invoiceRepository.AddAsync(invoice);
            await _invoiceRepository.SaveChangesAsync();

            installmentFromDb.SetAsInvoiced(invoice.Id);
            await _installmentRepository.SaveChangesAsync();
            var invoiceFromDb = await _invoiceRepository.GetDetails(invoice.Id);

            var riskFromDb = await _riskRepository.FindAsync(riskId);
            riskFromDb.SetLatestInvoiceDueDate(dueDate);
            await _riskRepository.SaveChangesAsync();

            await UpdateInvoicedFees(riskId, currentDate);

            var invoiceDto = _mapper.Map<InvoiceDto>(invoiceFromDb);

            var billingSummary = await _billingSummaryService.GetBillingSummary(riskId);

            invoiceDto = ComputePremiumFinancedBrokerCommission(billingSummary, invoiceDto);

            invoiceDto.PayoffAmount = billingSummary.PayoffAmount;

            return invoiceDto;
        }

        private async Task<decimal> ComputePreviousBalance(Guid riskId)
        {
            decimal previousPaymentAmount = await _paymentRepository.GetTotalPaymentAsync(riskId);
            decimal previousInvoicedAmount = await _invoiceRepository.GetTotalInvoicedAmountAsync(riskId);

            return previousInvoicedAmount - previousPaymentAmount;
        }

        private async Task<decimal> ComputePremiumBalance(Guid riskId)
        {
            var previousPayments = await _paymentRepository.GetAllByRiskId(riskId);
            var previousInvoices = await _invoiceRepository.GetAllNotVoided(riskId);

            decimal previousPremiumPayment = 0;
            decimal previousPremiumInvoice = 0;

            foreach (var payment in previousPayments)
            {
                if (payment?.PaymentDetails != null)
                {
                    foreach (var paymentDetail in payment.PaymentDetails)
                    {
                        if (paymentDetail?.AmountTypeId == AmountType.Premium.Id)
                        {
                            previousPremiumPayment += paymentDetail.Amount;
                        }
                    }
                }
            }

            foreach (var invoice in previousInvoices)
            {
                if (invoice?.InvoiceDetails != null)
                {
                    foreach (var invoiceDetail in invoice.InvoiceDetails)
                    {
                        if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                        {
                            previousPremiumInvoice += invoiceDetail.InvoicedAmount;
                        }
                    }
                }
            }

            return previousPremiumInvoice - previousPremiumPayment;
        }

        private async Task<List<InvoiceDetail>> GetInvoiceDetails(Guid riskId, Installment installment, string appId, DateTime invoiceDate)
        {
            List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();
            var transactionsFromDb = await _transactionRepository.GetAllByRisk(riskId);
            if (transactionsFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.NO_TRANSACTIONS_FOR_RISK);
            }
            installmentFeeTransaction = null;

            ComputeTotalPremiumsAndFees(invoiceDetails, transactionsFromDb, invoiceDate);

            List<InvoiceDetail> previousInvoiceDetails = await GetPreviousInvoiceDetails(installment.RiskId);
            SubractInvoicedAmount(invoiceDetails, previousInvoiceDetails);

            await SetInvoicedPremiums(installment, invoiceDetails);

            if (installment.InstallmentType.Id == InstallmentType.Installment.Id)
            {
                await AddInstallmentFee(installment, invoiceDetails, appId, invoiceDate, previousInvoiceDetails);
            }

            if (installment.InstallmentType.Id == InstallmentType.Installment.Id ||
            installment.InstallmentType.Id == InstallmentType.Endorsement.Id)
            {
                int remainingInstallmentCount = await _installmentRepository.CountAllNotInvoiced(installment.RiskId);
                if (remainingInstallmentCount == 0)
                {
                    remainingInstallmentCount = 1;
                }
                totalAmountToInvoice = (((totalPremium - invoicedPremium) / remainingInstallmentCount)
                    + (totalFees - invoicedFees) + (totalTax - invoicedTax));
            }
            else if (installment.InstallmentType.Id == InstallmentType.Deposit.Id)
            {
                totalAmountToInvoice = (((totalPremium - invoicedPremium) * installment.PercentOfOutstanding)
                    + (totalFees - invoicedFees) + (totalTax - invoicedTax));
            }

            return invoiceDetails;
        }

        private async Task SetInvoicedPremiums(Installment installment, List<InvoiceDetail> invoiceDetails)
        {
            if (installment.InstallmentTypeId == InstallmentType.Deposit.Id)
            {
                foreach (var invoiceDetail in invoiceDetails)
                {
                    AmountSubType amountSubType = Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail.AmountSubTypeId);
                    if (amountSubType?.AmountTypeId == AmountType.Premium.Id)
                    {
                        invoiceDetail.SetAmount((invoiceDetail.InvoicedAmount * installment.PercentOfOutstanding));
                    }
                }
            }
            else if (installment.InstallmentTypeId == InstallmentType.Installment.Id)
            {
                int remainingInstallmentCount = await _installmentRepository.CountAllNotInvoiced(installment.RiskId);
                if (remainingInstallmentCount == 0)
                {
                    remainingInstallmentCount = 1;
                }

                foreach (var invoiceDetail in invoiceDetails)
                {
                    AmountSubType amountSubType = Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail.AmountSubTypeId);
                    if (amountSubType?.AmountTypeId == AmountType.Premium.Id)
                    {
                        invoiceDetail.SetAmount((invoiceDetail.InvoicedAmount / remainingInstallmentCount));
                    }
                }
            }
        }

        private void SubractInvoicedAmount(List<InvoiceDetail> invoiceDetails, List<InvoiceDetail> previousInvoiceDetails)
        {
            invoicedPremium = 0;
            invoicedFees = 0;
            invoicedTax = 0;
            foreach (var invoiceDetail in invoiceDetails)
            {
                if (previousInvoiceDetails.Any(x => x.AmountSubTypeId == invoiceDetail.AmountSubTypeId))
                {
                    var previousInvoiceDetail = previousInvoiceDetails.First(x => x.AmountSubTypeId == invoiceDetail.AmountSubTypeId);
                    invoiceDetail.AddAmount(-previousInvoiceDetail.InvoicedAmount);

                    AmountSubType amountSubType = Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail.AmountSubTypeId);
                    if (amountSubType?.AmountTypeId == AmountType.Premium.Id)
                    {
                        invoicedPremium += previousInvoiceDetail.InvoicedAmount;
                    }
                    else if (amountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                        || amountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        invoicedFees += previousInvoiceDetail.InvoicedAmount;
                    }
                    else if (amountSubType?.AmountTypeId == AmountType.Tax.Id
                        || amountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        invoicedTax += previousInvoiceDetail.InvoicedAmount;
                    }
                }
            }

            invoiceDetails.RemoveAll(x => x.InvoicedAmount == 0);
        }

        private void ComputeTotalPremiumsAndFees(List<InvoiceDetail> invoiceDetails, List<Transaction> transactionsFromDb, DateTime invoiceDate)
        {
            initialTotalPremium = 0;
            totalPremium = 0;
            totalFees = 0;
            totalTax = 0;


            foreach (var transaction in transactionsFromDb.Where(t => string.Equals(t.TransactionTypeId, TransactionType.New.Id, StringComparison.OrdinalIgnoreCase)))
            {
                if (transaction?.TransactionDetails != null)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                        {
                            initialTotalPremium += transactionDetail.Amount;
                        }
                    }
                }
            }

            foreach (var transaction in transactionsFromDb)
            {
                if (transaction != null && transaction.TransactionDetails != null)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                        {
                            totalPremium += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                            && invoiceDate.Date >= transaction.EffectiveDate.Date)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType != null && transactionDetail.AmountSubType.AmountTypeId == AmountType.Tax.Id)
                        {
                            totalTax += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else
                        {
                            continue;
                        }

                        if (invoiceDetails.Any(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId))
                        {
                            var invoiceDetail = invoiceDetails.First(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId);
                            invoiceDetail.AddAmount(transactionDetail.Amount);
                        }
                        else
                        {
                            invoiceDetails.Add(new InvoiceDetail(transactionDetail.Amount, transactionDetail.AmountSubType));
                        }
                    }
                }
            }
        }

        private async Task<bool> ShouldAddInstallmentFee(Installment installment)
        {
            int remainingInstallmentCount = await _installmentRepository.CountAllNotInvoiced(installment.RiskId);
            if (remainingInstallmentCount == 0)
            {
                remainingInstallmentCount = 1;
            }
            decimal premiumAmountToInvoice = ((totalPremium - invoicedPremium) / remainingInstallmentCount);
            decimal premiumBalance = await ComputePremiumBalance(installment.RiskId);

            return (premiumAmountToInvoice + premiumBalance > 0);
        }

        public async Task<List<InvoiceNotifView>> GenerateCurrentDayInvoices(GenerateInvoiceRequestDto model)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            if (model.CurrentDateTime.HasValue)
            {
                currentDate = model.CurrentDateTime.Value;
            }

            var currentDayInvoices = new List<InvoiceNotifView>();
            var currentDayInstallments = await _installmentRepository.FindCurrentDayInstallmentsByAppId(currentDate, model.AppId);
            if (currentDayInstallments == null)
            {
                return currentDayInvoices;
            }

            foreach (var installment in currentDayInstallments)
            {
                if (installment.IsInvoiced || installment.IsSkipped || installment.VoidDate != null || installment.Risk.PolicyStatusId == PolicyStatus.Cancelled.Id)
                {
                    continue;
                }
                previousBalance = await ComputePreviousBalance(installment.RiskId);

                List<InvoiceDetail> invoiceDetails = await GetInvoiceDetails(installment.RiskId, installment, model.AppId, currentDate);

                Invoice invoice = new Invoice(installment.RiskId, await _invoiceNumberService.GenerateInvoiceNumber(model.AppId, installment.Risk.PolicyNumber), currentDate,
                    installment.DueDate, previousBalance, totalAmountToInvoice, invoiceDetails, _userDateService.GetUser());

                await _invoiceRepository.AddAsync(invoice);
                await _invoiceRepository.SaveChangesAsync();

                installment.SetAsInvoiced(invoice.Id);
                if (installmentFeeTransaction != null)
                {
                    installmentFeeTransaction.SetInvoiceId(invoice.Id);
                    await _transactionRepository.SaveChangesAsync();
                }
                await _installmentRepository.SaveChangesAsync();
                var invoiceFromDb = await _invoiceRepository.GetDetails(invoice.Id);

                var riskFromDb = await _riskRepository.FindAsync(installment.RiskId);
                riskFromDb.SetLatestInvoiceDueDate(installment.DueDate);
                await _riskRepository.SaveChangesAsync();

                await UpdateInvoicedFees(installment.RiskId, currentDate);

                var invoiceDto = _mapper.Map<InvoiceDto>(invoiceFromDb);

                var billingSummary = await _billingSummaryService.GetBillingSummary(installment.RiskId);

                invoiceDto = ComputePremiumFinancedBrokerCommission(billingSummary, invoiceDto);

                invoiceDto.PayoffAmount = billingSummary.PayoffAmount;

                var invoiceNotifView = _mapper.Map<InvoiceNotifView>(invoiceDto);
                invoiceNotifView.IsRecurringPayment = _paymentProfileService.GetEnrolledPaymentProfile(invoice.RiskId) != null;
                currentDayInvoices.Add(invoiceNotifView);
            }

            return currentDayInvoices;
        }

        public InvoiceDto ComputePremiumFinancedBrokerCommission(BillingSummaryDto billingSummary, InvoiceDto invoice)
        {
            //Compute premium commission for Premium Financed payment plan
            if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description, StringComparison.OrdinalIgnoreCase))
            {
                decimal totalALPremium = 0;
                decimal totalPDPremium = 0;

                foreach (var invoiceDetail in invoice.InvoiceDetails)
                {
                    if (invoiceDetail?.AmountSubType?.AmountType.Id == AmountType.Premium.Id
                        && (invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.AdditionalInsured.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.WaiverOfSubrogation.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PrimaryNoncontributory.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumShortRate.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumShortRate.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumShortRate.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumShortRate.Id))
                    {
                        totalALPremium += invoiceDetail.InvoicedAmount;
                    }
                    else if (invoiceDetail?.AmountSubType?.AmountType.Id == AmountType.Premium.Id
                       && (invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.TIEndst.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.HPDEndst.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremium.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumAdjustment.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumShortRate.Id
                            || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumShortRate.Id))
                    {
                        totalPDPremium += invoiceDetail.InvoicedAmount;
                    }
                }

                decimal brokerCommission = (totalALPremium * billingSummary.ALCommission) + (totalPDPremium * billingSummary.PDCommission);

                invoice.BrokerCommission = brokerCommission;
            }

            return invoice;
        }

        private async Task<List<InvoiceDetail>> GetPreviousInvoiceDetails(Guid riskId)
        {
            List<InvoiceDetail> previousInvoiceDetails = new List<InvoiceDetail>();
            var previousInvoices = await _invoiceRepository.GetAllNotVoided(riskId);
            if (previousInvoices == null)
            {
                return previousInvoiceDetails;
            }

            foreach (var invoice in previousInvoices)
            {
                if (invoice.InvoiceDetails != null)
                {
                    foreach (var previousInvoiceDetail in invoice.InvoiceDetails)
                    {
                        if (previousInvoiceDetails.Any(x => x.AmountSubTypeId == previousInvoiceDetail.AmountSubTypeId))
                        {
                            var invoiceDetail = previousInvoiceDetails.First(x => x.AmountSubTypeId == previousInvoiceDetail.AmountSubTypeId);
                            invoiceDetail.AddAmount(previousInvoiceDetail.InvoicedAmount);
                        }
                        else
                        {
                            previousInvoiceDetails.Add(new InvoiceDetail(previousInvoiceDetail.InvoicedAmount, previousInvoiceDetail.AmountSubType));
                        }
                    }
                }
            }
            return previousInvoiceDetails;
        }

        private async Task AddInstallmentFee(Installment installment, List<InvoiceDetail> invoiceDetails,
            string appId, DateTime invoiceDate, List<InvoiceDetail> previousInvoiceDetails)
        {
            StateTax stateTaxDto = _stateTaxRepository.Get(installment.Risk.StateCode, installment.Risk.EffectiveDate);
            IEnumerable<FeeKind> feeKindDto = _nonTaxableFeeRepository.GetNonTaxableTypesByState(installment.Risk.StateCode, installment.Risk.EffectiveDate).Result;
            bool hasTaxableInstallmentFee = !feeKindDto.Any(ntf => ntf.Id.Equals(FeeKind.InstallmentFee.Id));
            decimal taxRate = hasTaxableInstallmentFee ? stateTaxDto.TotalRate : 0;

            int remainingInstallmentCount = await _installmentRepository.CountAllNotInvoiced(installment.RiskId);
            if (remainingInstallmentCount == 0)
            {
                remainingInstallmentCount = 1;
            }
            decimal previousInstallmentFeeTotal = previousInvoiceDetails
                                                    .Where(i => string.Equals(i.AmountSubTypeId, AmountSubType.InstallmentFee.Id, StringComparison.OrdinalIgnoreCase)).Sum(s => s.InvoicedAmount);

            InstallmentFee installmentFee = new InstallmentFee(initialTotalPremium, invoicedPremium, remainingInstallmentCount, taxRate, previousInstallmentFeeTotal);

            if (await ShouldAddInstallmentFee(installment))
            {
                var transactionDetails = new List<TransactionDetail>();
                if(installmentFee.Fee > 0)
                {
                    transactionDetails.Add(new TransactionDetail(installmentFee.Fee, null, AmountSubType.InstallmentFee));
                }

                if (installmentFee.Tax > 0)
                {
                    transactionDetails.Add(new TransactionDetail(installmentFee.Tax, null, AmountSubType.InstallmentTax));
                }

                installmentFeeTransaction = await _transactionRepository.AddAsync(new Transaction(installment.RiskId,
                    TransactionType.Fee, transactionDetails, invoiceDate, _userDateService.GetUser(), null));
                await _transactionRepository.SaveChangesAsync();

                if (installmentFee.Fee > 0)
                {
                    invoiceDetails.Add(new InvoiceDetail(installmentFee.Fee, AmountSubType.InstallmentFee));
                }

                if (installmentFee.Tax > 0)
                {
                    invoiceDetails.Add(new InvoiceDetail(installmentFee.Tax, AmountSubType.InstallmentTax));
                }

                totalFees += installmentFee.Fee;
                totalTax += installmentFee.Tax;
            }
        }

        public async Task<string> VoidInvoice(string invoiceNumber, string appId, bool willVoidFees, Guid? invoiceId = null)
        {
            Invoice invoiceFromDb;
            if (invoiceId.HasValue)
            {
                invoiceFromDb = await _invoiceRepository.GetDetails(invoiceId.Value);
            }
            else
            {
                invoiceFromDb = await _invoiceRepository.FindByInvoiceNumber(invoiceNumber);
            }

            if (invoiceFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_INVOICE_NUMBER);
            }
            else if (invoiceFromDb.VoidDate != null)
            {
                throw new ParameterException(ExceptionMessages.INVOICE_ALREADY_VOID);
            }

            invoiceFromDb.SetAsVoid(_userDateService.GetUser());
            await _invoiceRepository.SaveChangesAsync();

            if (willVoidFees)
            {
                List<Transaction> transactionsFromDb = await _transactionRepository.FindByInvoiceId(invoiceFromDb.Id);
                foreach (var transactionFromDb in transactionsFromDb)
                {
                    if (transactionFromDb != null)
                    {
                        if (transactionFromDb.TransactionDetails != null)
                        {
                            foreach (var transactionDetail in transactionFromDb.TransactionDetails)
                            {
                                await _transactionFeeService.VoidTransactionFee(new VoidFeeRequestDto(transactionFromDb.RiskId, transactionDetail.Id, appId));
                            }
                        }
                    }
                }
            }


            return invoiceFromDb.Id.ToString();
        }

        private async Task UpdateInvoicedFees(Guid riskId, DateTime invoiceDate)
        {
            var transactionsFromDb = await _transactionRepository.GetAllByRisk(riskId);
            foreach (var transaction in transactionsFromDb)
            {
                if (transaction.TransactionDetails == null)
                {
                    continue;
                }
                foreach (var transactionDetail in transaction.TransactionDetails)
                {
                    if ((transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                        || transactionDetail.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                        && transactionDetail.InvoiceDate == null
                        && transaction.EffectiveDate.Date <= invoiceDate.Date)
                    {
                        transactionDetail.SetInvoiceDate(invoiceDate);
                    }
                }
            }
            await _transactionRepository.SaveChangesAsync();
        }

        public Invoice GenerateInvoiceBeforeBindAsync(decimal percentOfOutstanding, Transaction transaction)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate.Date;

            (List<InvoiceDetail>, decimal) invoiceDetails = GetInvoiceDetailsBeforeBind(percentOfOutstanding, transaction, currentDate);

            Invoice invoice = new Invoice(Guid.Empty, null, currentDate,
                currentDate, previousBalance, invoiceDetails.Item2, invoiceDetails.Item1, _userDateService.GetUser());

            return invoice;
        }
        private (List<InvoiceDetail>, decimal) GetInvoiceDetailsBeforeBind(decimal percentOfOutstanding, Transaction transaction, DateTime invoiceDate)
        {
            List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();

            List<Transaction> transactions = new List<Transaction> { transaction };

            ComputeTotalPremiumsAndFeesBeforeBind(invoiceDetails, transactions, invoiceDate);

            SetInvoicedPremiumsBeforeBind(invoiceDetails, percentOfOutstanding);

            decimal totalInvoicedAmount = (((totalPremium - invoicedPremium) * percentOfOutstanding)
                + (totalFees - invoicedFees) + (totalTax - invoicedTax));

            return (invoiceDetails, totalInvoicedAmount);
        }

        private void SetInvoicedPremiumsBeforeBind(List<InvoiceDetail> invoiceDetails, decimal percentOfOutstanding)
        {
            foreach (var invoiceDetail in invoiceDetails)
            {
                AmountSubType amountSubType = Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail.AmountSubTypeId);
                if (amountSubType?.AmountTypeId == AmountType.Premium.Id)
                {
                    invoiceDetail.SetAmount((invoiceDetail.InvoicedAmount * percentOfOutstanding));
                }
            }
        }

        public void ComputeTotalPremiumsAndFeesBeforeBind(List<InvoiceDetail> invoiceDetails, List<Transaction> transactionsFromDb, DateTime invoiceDate)
        {
            totalPremium = 0;
            totalFees = 0;
            totalTax = 0;
            foreach (var transaction in transactionsFromDb)
            {
                if (transaction != null && transaction.TransactionDetails != null)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail?.AmountTypeId == AmountType.Premium.Id)
                        {
                            totalPremium += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountTypeId == AmountType.PremiumFee.Id)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id
                            && invoiceDate.Date >= transaction.EffectiveDate.Date)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountTypeId == AmountType.Tax.Id)
                        {
                            totalTax += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountTypeId == AmountType.TransactionTax.Id)
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else
                        {
                            continue;
                        }

                        invoiceDetails.Add(new InvoiceDetail(transactionDetail.Amount, Enumeration.GetEnumerationById<AmountSubType>(transactionDetail.AmountSubTypeId)));
                    }
                }
            }
        }
    }
}
