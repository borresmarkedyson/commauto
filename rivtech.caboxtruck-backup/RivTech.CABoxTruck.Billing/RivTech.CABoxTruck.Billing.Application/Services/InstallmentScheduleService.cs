﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.Factory.InstallmentSchedules;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class InstallmentScheduleService : IInstallmentScheduleService
    {

        private readonly IInstallmentRepository _installmentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IMapper _mapper;
        private readonly IUserDateService _userDateService;
        private readonly ITaxService _taxService;
        private readonly ITransactionService _transactionService;
        private readonly IInvoiceService _invoiceService;
        private readonly IStateTaxRepository _stateTaxRepository;
        private readonly INonTaxableFeeRepository _nonTaxableFeeRepository;

        public InstallmentScheduleService(
            ITransactionService transactionService,
            IInstallmentRepository installmentRepository,
            IRiskRepository riskRepository,
            IMapper mapper,
            IUserDateService userDateService,
            ITaxService taxService,
            IInvoiceService invoiceService,
            IStateTaxRepository stateTaxRepository,
            INonTaxableFeeRepository nonTaxableFeeRepository
        )
        {
            _installmentRepository = installmentRepository;
            _riskRepository = riskRepository;
            _mapper = mapper;
            _userDateService = userDateService;
            _taxService = taxService;
            _transactionService = transactionService;
            _invoiceService = invoiceService;
            _stateTaxRepository = stateTaxRepository;
            _nonTaxableFeeRepository = nonTaxableFeeRepository;
        }

        private async Task<List<InstallmentDto>> GenerateInstallmentSchedule(Guid riskId, DateTime startDate, string paymentPlan,
           string appId, bool isPaymentPlanChange, decimal? percentOfOutstanding)
        {
            var installmentSchedules = GenerateInstallmentScheduleWithoutSaving(riskId, startDate, paymentPlan, appId, isPaymentPlanChange, percentOfOutstanding);

            await _installmentRepository.AddRangeAsync(installmentSchedules);
            await _installmentRepository.SaveChangesAsync();
            return _mapper.Map<List<InstallmentDto>>(installmentSchedules);
        }

        public List<Installment> GenerateInstallmentScheduleWithoutSaving(Guid riskId, DateTime startDate, string paymentPlan,
           string appId, bool isPaymentPlanChange, decimal? percentOfOutstanding)
        {
            InstallmentScheduleFactory factory;
            if (paymentPlan == PaymentPlan.PremiumFinanced.Id)
            {
                factory = new PremiumFinancedInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding);
            }
            else if (paymentPlan == PaymentPlan.FullPay.Id)
            {
                factory = new FullPayInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding);
            }
            else if (paymentPlan == PaymentPlan.TwoPay.Id)
            {
                factory = new TwoPayInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding, PaymentPlan.PayCounts[paymentPlan]);
            }
            else if (paymentPlan == PaymentPlan.ThreePay.Id || paymentPlan == PaymentPlan.FourPay.Id)
            {
                factory = new NinetyDaysInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding, PaymentPlan.PayCounts[paymentPlan]);
            }
            else if (paymentPlan == PaymentPlan.FivePay.Id || paymentPlan == PaymentPlan.SixPay.Id || paymentPlan == PaymentPlan.SevenPay.Id)
            {
                factory = new SixtyDaysInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding, PaymentPlan.PayCounts[paymentPlan]);
            }
            else if (paymentPlan == PaymentPlan.EightPay.Id || paymentPlan == PaymentPlan.NinePay.Id || paymentPlan == PaymentPlan.TenPay.Id || paymentPlan == PaymentPlan.TenPayRenewal.Id || paymentPlan == PaymentPlan.ElevenPay.Id)
            {
                factory = new MonthlyInstallmentScheduleFactory(riskId, startDate, appId, _userDateService.GetUser(), isPaymentPlanChange, percentOfOutstanding, PaymentPlan.PayCounts[paymentPlan]);
            }
            else
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_PLAN);
            }

            var installmentSchedules = factory.GetInstallmentSchedule();
            return (List<Installment>)installmentSchedules;
        }

        public async Task<List<InstallmentDto>> GetAllAsync(Guid riskId)
        {
            var result = await _installmentRepository.GetAllByRisk(riskId);
            return _mapper.Map<List<InstallmentDto>>(result);
        }

        public async Task<List<InstallmentDto>> SaveInstallmentScheduleAsync(Guid riskId, string policyNumber, DateTime startDate, string paymentPlan,
            string appId, bool isPaymentPlanChange, decimal? percentOfOutstanding)
        {
            var riskFromDb = await _riskRepository.GetDetailsAsync(riskId);
            if (riskFromDb == null)
            {
                riskFromDb = new Risk { Id = riskId };
                riskFromDb.SetPolicyNumber(policyNumber);
                riskFromDb.InitializeNoticeHistory();
                await _riskRepository.AddAsync(riskFromDb);
                await _riskRepository.SaveChangesAsync();
            }

            if (riskFromDb.PaymentPlan?.Id == paymentPlan)
            {
                return _mapper.Map<List<InstallmentDto>>(await _installmentRepository.GetAllByRisk(riskId));
            }
            else if (riskFromDb.PaymentPlan == null)
            {
                var installmentSchedule = await GenerateInstallmentSchedule(riskId, startDate, paymentPlan, appId, isPaymentPlanChange, percentOfOutstanding);
                riskFromDb.SetPaymentPlanId(paymentPlan);
                riskFromDb.SetApplicationId(appId);
                riskFromDb.SetEffectiveDate(startDate);
                await _riskRepository.SaveChangesAsync();
                return installmentSchedule;
            }
            else
            {
                await VoidCurrentInstallmentSchedule(riskId);
                var installmentSchedule = await GenerateInstallmentSchedule(riskId, startDate, paymentPlan, appId, isPaymentPlanChange, percentOfOutstanding);
                riskFromDb.SetPaymentPlanId(paymentPlan);
                riskFromDb.SetApplicationId(appId);
                riskFromDb.SetEffectiveDate(startDate);
                await _riskRepository.SaveChangesAsync();
                return installmentSchedule;
            }
        }

        private async Task VoidCurrentInstallmentSchedule(Guid riskId)
        {
            var currentInstallmentSchedule = await _installmentRepository.GetAllByRisk(riskId);

            foreach (var installment in currentInstallmentSchedule)
            {
                installment.SetAsVoid(_userDateService.GetUser());
            }

            await _installmentRepository.SaveChangesAsync();
        }
        public async Task<List<InstallmentAndInvoiceDto>> GenerateInstallmentScheduleBeforeBind(List<TransactionDetailDto> transactionDetails,
                                DateTime effectiveDate, string stateCode, decimal percentOfOutstanding, string paymentPlan, string appId)
        {
            transactionDetails = transactionDetails.Where(t => t.AmountSubType.Id != AmountSubType.StampingFee.Id
                                                && t.AmountSubType.Id != AmountSubType.SurplusLinesTax.Id
                                                && t.AmountSubType.Id != AmountSubType.InstallmentFee.Id).ToList();

            TaxDetails taxDetails = await _taxService.CalculateRiskTaxAsync(effectiveDate, stateCode, transactionDetails, null);

            transactionDetails = taxDetails.TransactionDetails;

            TransactionDto transactionDto = new TransactionDto(transactionDetails, appId, effectiveDate);

            Transaction transaction = await _transactionService.GenerateTransactionBeforeBind(transactionDto, Enumeration.GetEnumerationById<TransactionType>(transactionDto.TransactionType.Id));

            List<Installment> installments = GenerateInstallmentScheduleWithoutSaving(Guid.Empty, effectiveDate, paymentPlan, appId, false, percentOfOutstanding);

            Invoice invoice = _invoiceService.GenerateInvoiceBeforeBindAsync(percentOfOutstanding, transaction);

            StateTax stateTaxDto = _stateTaxRepository.Get(stateCode, effectiveDate);
            IEnumerable<FeeKind> feeKindDto = _nonTaxableFeeRepository.GetNonTaxableTypesByState(stateCode, effectiveDate).Result;
            bool hasTaxableInstallmentFee = !feeKindDto.Any(ntf => ntf.Id.Equals(FeeKind.InstallmentFee.Id));
            decimal installmentFeeTaxRate = hasTaxableInstallmentFee ? stateTaxDto.TotalRate : 0;

            List<InstallmentAndInvoice> installmentAndInvoices = GenerateInstallmentAndInvoices(installments, invoice, transaction, installmentFeeTaxRate);

            return _mapper.Map<List<InstallmentAndInvoiceDto>>(installmentAndInvoices);
        }

        private List<InstallmentAndInvoice> GenerateInstallmentAndInvoices(List<Installment> installments, Invoice invoice, Transaction transaction, decimal installmentFeeTaxRate)
        {
            List<InstallmentAndInvoice> installmentAndInvoices = new List<InstallmentAndInvoice>();
            bool isPreviousInstallmentInvoiced = false;
            
            DateTime previousInvoiceDate = installments[0].InvoiceOnDate;

            decimal previousTotalDue = 0;
            foreach (var installment in installments)
            {
                PremiumFeeAndTax billed = ComputePremiumsFeesAndTaxesFromInvoice(invoice);

                if (installment != null && string.Equals(installment.InstallmentTypeId, InstallmentType.Deposit.Id, StringComparison.OrdinalIgnoreCase) && invoice != null)
                {
                    var installmentAndInvoice = new InstallmentAndInvoice
                    {
                        Status = installment.VoidDate == null ? "Billed" : "Void",
                        InstallmentType = InstallmentType.Deposit.Description,
                        Premium = billed.Premium,
                        Fee = billed.Fees,
                        Tax = billed.Tax,
                        Balance = invoice.PreviousBalance,
                        BillDate = invoice.InvoiceDate,
                        DueDate = invoice.DueDate,
                        InvoiceNumber = invoice.InvoiceNumber,
                        TotalBilled = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals(),
                        TotalDue = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals() + invoice.PreviousBalance.RoundTo2Decimals()
                    };

                    installmentAndInvoices.Add(installmentAndInvoice);
                    isPreviousInstallmentInvoiced = true;
                }
                else if (installment != null && string.Equals(installment.InstallmentTypeId, InstallmentType.Installment.Id, StringComparison.OrdinalIgnoreCase))
                {
                    PremiumFeeAndTax premiumFeeAndTaxFromTransactions = ComputeActivePremiumsFeesAndTaxesFromTransactions(transaction, previousInvoiceDate, installment.InvoiceOnDate, isPreviousInstallmentInvoiced);
                    (PremiumFeeAndTax, decimal) premiumFeeAndTaxFromPreviousInvoices = ComputePremiumsFeesAndTaxesFromPreviousInvoices(invoice);

                    int countInstallmentsNotInvoiced = CountInstallmentsNotInvoiced(installments);

                    decimal premium = ((premiumFeeAndTaxFromTransactions.Premium - premiumFeeAndTaxFromPreviousInvoices.Item1.Premium) / countInstallmentsNotInvoiced);
                    decimal fee = isPreviousInstallmentInvoiced ? premiumFeeAndTaxFromTransactions.Fees - premiumFeeAndTaxFromPreviousInvoices.Item1.Fees : 0;
                    decimal tax = isPreviousInstallmentInvoiced ? premiumFeeAndTaxFromTransactions.Tax - premiumFeeAndTaxFromPreviousInvoices.Item1.Tax : 0;
                    decimal balance = 0;

                    if (isPreviousInstallmentInvoiced)
                    {
                        balance = billed.TotalAmount;
                    }
                    else if (previousTotalDue < 0)
                    {
                        balance = previousTotalDue;
                    }
                    else
                    {
                        balance = 0;
                    }

                    decimal totalDue = premium.RoundTo2Decimals() + fee.RoundTo2Decimals() + tax.RoundTo2Decimals() + balance.RoundTo2Decimals();
                    InstallmentFee installmentFee = new InstallmentFee(premiumFeeAndTaxFromTransactions.Premium, premiumFeeAndTaxFromPreviousInvoices.Item1.Premium, countInstallmentsNotInvoiced, installmentFeeTaxRate, premiumFeeAndTaxFromPreviousInvoices.Item2);
                    if (totalDue > 0.10m)
                    {
                        fee += installmentFee.Fee;
                        tax += installmentFee.Tax;
                    }

                    installmentAndInvoices.Add(new InstallmentAndInvoice
                    {
                        Status = "Future",
                        InstallmentType = InstallmentType.Installment.Description,
                        Premium = premium,
                        Fee = fee,
                        Tax = tax,
                        Balance = balance,
                        BillDate = installment.InvoiceOnDate,
                        DueDate = installment.DueDate,
                        InvoiceNumber = null,
                        FutureInstallmentFee = totalDue > 0 ? installmentFee.Fee : 0,
                        TotalBilled = premium.RoundTo2Decimals() + fee.RoundTo2Decimals() + tax.RoundTo2Decimals(),
                        TotalDue = premium.RoundTo2Decimals() + fee.RoundTo2Decimals() + tax.RoundTo2Decimals() + balance.RoundTo2Decimals()
                    });

                    previousTotalDue = premium.RoundTo2Decimals() + fee.RoundTo2Decimals() + tax.RoundTo2Decimals() + balance.RoundTo2Decimals();

                    isPreviousInstallmentInvoiced = false;
                    previousInvoiceDate = installment.InvoiceOnDate;
                }
            }

            installmentAndInvoices = installmentAndInvoices.OrderBy(x => x.InvoiceNumber == null)
                .ThenBy(x => x.InvoiceNumber).ThenBy(x => x.BillDate).ToList();

            return installmentAndInvoices;
        }

        private PremiumFeeAndTax ComputePremiumsFeesAndTaxesFromInvoice(Invoice invoice)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            if (invoice?.InvoiceDetails != null)
            {
                foreach (var invoiceDetail in invoice.InvoiceDetails)
                {
                    if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.Premium.Id)
                    {
                        totalPremium += invoiceDetail.InvoicedAmount;
                    }
                    else if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.PremiumFee.Id
                        || Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        totalFees += invoiceDetail.InvoicedAmount;
                    }
                    else if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.Tax.Id
                        || Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        tax += invoiceDetail.InvoicedAmount;
                    }
                }
            }
            return new PremiumFeeAndTax(totalPremium, totalFees, tax);
        }
        private PremiumFeeAndTax ComputeActivePremiumsFeesAndTaxesFromTransactions(Transaction transaction, DateTime previousInvoiceDate, DateTime currentInvoiceDate, bool isPreviousInstallmentInvoiced)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            if (transaction.TransactionDetails != null)
            {
                foreach (var transactionDetail in transaction.TransactionDetails)
                {
                    if (transactionDetail?.AmountTypeId == AmountType.Premium.Id)
                    {
                        totalPremium += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.PremiumFee.Id)
                    {
                        totalFees += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id
                        && isPreviousInstallmentInvoiced
                        && previousInvoiceDate.Date >= transaction.EffectiveDate.Date
                        && transactionDetail.InvoiceDate == null)
                    {
                        totalFees += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id
                        && currentInvoiceDate.Date >= transaction.EffectiveDate.Date
                        && previousInvoiceDate.Date < transaction.EffectiveDate.Date
                        && transactionDetail.InvoiceDate == null)
                    {
                        totalFees += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id
                        && transactionDetail.InvoiceDate != null)
                    {
                        totalFees += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.Tax.Id
                        || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        tax += transactionDetail.Amount;
                    }
                }
            }
            return new PremiumFeeAndTax(totalPremium, totalFees, tax);
        }

        private int CountInstallmentsNotInvoiced(List<Installment> installments)
        {
            return installments == null ? 0 : installments.Count() - 1;
        }

        private (PremiumFeeAndTax, decimal) ComputePremiumsFeesAndTaxesFromPreviousInvoices(Invoice invoice)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            decimal totalInstallmentFee = 0;

            if (invoice != null)
            {
                if (invoice?.InvoiceDetails != null)
                {
                    foreach (var invoiceDetail in invoice.InvoiceDetails)
                    {
                        if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.Premium.Id)
                        {
                            totalPremium += invoiceDetail.InvoicedAmount;
                        }
                        else if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.PremiumFee.Id
                            || Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.TransactionFee.Id)
                        {
                            totalFees += invoiceDetail.InvoicedAmount;

                            if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.TransactionFee.Id
                                   || invoiceDetail?.AmountSubType?.Id == AmountSubType.InstallmentFee.Id)
                            {
                                totalInstallmentFee += invoiceDetail.InvoicedAmount;
                            }
                        }
                        else if (Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.Tax.Id
                            || Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail?.AmountSubTypeId).AmountTypeId == AmountType.TransactionTax.Id)
                        {
                            tax += invoiceDetail.InvoicedAmount;
                        }
                    }
                }
            }
            return (new PremiumFeeAndTax(totalPremium, totalFees, tax), totalInstallmentFee);
        }

        public async Task<InstallmentDto> AddEditFutureInstallmentScheduleAsync(Guid riskId, Guid? installmentId, DateTime invoiceDate, DateTime dueDate)
        {
            if (!installmentId.HasValue)
            {
                var installments = await _installmentRepository.GetAllByRisk(riskId);

                decimal percentOfOutstanding = installments[0].PercentOfOutstanding;

                CustomInstallmentScheduleFactory customInstallmentSchedule = new CustomInstallmentScheduleFactory(riskId, invoiceDate, dueDate, _userDateService.GetUser(), percentOfOutstanding, InstallmentType.Installment);

                var installment = customInstallmentSchedule.GetInstallment();

                await _installmentRepository.AddAsync(installment);
                await _installmentRepository.SaveChangesAsync();

                return _mapper.Map<InstallmentDto>(installment);
            }
            else
            {
                Installment installment = await _installmentRepository.FindAsync(installmentId.Value);
                installment.SetInvoiceOnDate(invoiceDate);
                installment.SetDueDate(dueDate);
                await _installmentRepository.SaveChangesAsync();

                return _mapper.Map<InstallmentDto>(installment);
            }
        }
    }
}
