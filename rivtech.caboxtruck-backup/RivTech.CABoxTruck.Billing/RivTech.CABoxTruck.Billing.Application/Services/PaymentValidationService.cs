﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentValidationService : IPaymentValidationService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IPaymentProfileRepository _paymentProfileRepository;
        private readonly IDepositAmountService _depositAmountService;

        public PaymentValidationService(
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IBillingSummaryService billingSummaryService,
            IPaymentProfileRepository paymentProfileRepository,
            IDepositAmountService depositAmountService
        )
        {
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _billingSummaryService = billingSummaryService;
            _paymentProfileRepository = paymentProfileRepository;
            _depositAmountService = depositAmountService;
        }

        public async Task ValidateMinimumPayment(Guid riskId, decimal paymentAmount, string appId)
        {
            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == riskId, x => x.NoticeHistory);
            if (riskFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            if (riskFromDb.PaymentPlanId == PaymentPlan.PremiumFinanced.Id)
            {
                return;
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(riskId);
            decimal minimumPaymentAmount = 0;

            var isNopcActive = riskFromDb.NoticeHistory?.NopcNoticeDate != null;

            if (riskFromDb.PolicyStatusId == PolicyStatus.Cancelled.Id)
            {
                if (billingSummary.Paid <= 0)
                {
                    minimumPaymentAmount = await _depositAmountService.ComputeDeposit(riskId, appId);
                    if (minimumPaymentAmount > paymentAmount)
                    {
                        throw new ParameterException(ExceptionMessages.MINIMUM_PAYMENT_MUST_BE_DEPOSIT);
                    }
                }
                else
                {
                    minimumPaymentAmount = billingSummary.PastDueAmountBeforeCancellation;
                    if (minimumPaymentAmount > paymentAmount)
                    {
                        throw new ParameterException(ExceptionMessages.MINIMUM_PAYMENT_MUST_BE_PAST_DUE);
                    }
                }
            }
            else if (isNopcActive)
            {
                int nsfCount = await _paymentRepository.GetNsfCount(riskId);
                if (nsfCount > 2)
                {
                    minimumPaymentAmount = billingSummary.PayoffAmount;
                    if (minimumPaymentAmount > paymentAmount)
                    {
                        throw new ParameterException(ExceptionMessages.PAYMENT_MUST_BE_POLICY_PAYOFF);
                    }
                }
                else
                {
                    minimumPaymentAmount = billingSummary.PastDueAmount > 0 ?
                        billingSummary.PastDueAmount : 0;
                    if (minimumPaymentAmount > paymentAmount)
                    {
                        throw new ParameterException(ExceptionMessages.MINIMUM_PAYMENT_MUST_BE_PAST_DUE);
                    }
                }
            }
            else
            {
                if (billingSummary.Paid > 0)
                {
                    minimumPaymentAmount = billingSummary.PastDueAmount > 0 ?
                        billingSummary.PastDueAmount : 0;
                    if (minimumPaymentAmount > paymentAmount)
                    {
                        throw new ParameterException(ExceptionMessages.MINIMUM_PAYMENT_MUST_BE_PAST_DUE);
                    }
                }
            }
        }

        public async Task<AllowedPaymentRangeDto> GetAllowedPaymentRange(Guid riskId, string appId)
        {
            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == riskId, x => x.NoticeHistory);
            if (riskFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(riskId);
            decimal minimumPaymentAmount = 0;

            var isNopcActive = riskFromDb.NoticeHistory?.NopcNoticeDate != null;

            if (riskFromDb.PolicyStatusId == PolicyStatus.Cancelled.Id)
            {
                if (billingSummary.Paid <= 0)
                {
                    minimumPaymentAmount = await _depositAmountService.ComputeDeposit(riskId, appId);
                }
                else
                {
                    minimumPaymentAmount = billingSummary.PastDueAmountBeforeCancellation;
                }
            }
            else if (isNopcActive)
            {
                int nsfCount = await _paymentRepository.GetNsfCount(riskId);
                if (nsfCount > 2)
                {
                    minimumPaymentAmount = billingSummary.PayoffAmount;
                }
                else
                {
                    minimumPaymentAmount = billingSummary.PastDueAmount > 0 ?
                        billingSummary.PastDueAmount : 0;
                }
            }
            else
            {
                if (billingSummary.Paid > 0)
                {
                    minimumPaymentAmount = billingSummary.PastDueAmount > 0 ?
                        billingSummary.PastDueAmount : 0;
                }
                else
                {
                    minimumPaymentAmount = await _depositAmountService.ComputeDeposit(riskId, appId);
                }
            }

            var paymentProfile = await _paymentProfileRepository.FindAsync(x => x.RiskId == riskId);

            return new AllowedPaymentRangeDto
            {
                MinimumPaymentAmount = minimumPaymentAmount > 0 ? minimumPaymentAmount.RoundTo2Decimals() : 0,
                MaximumPaymentAmount = billingSummary.PayoffAmount.RoundTo2Decimals(),
                IsAccountEnrolled = paymentProfile != null && paymentProfile.IsRecurringPayment
            };
        }
    }
}
