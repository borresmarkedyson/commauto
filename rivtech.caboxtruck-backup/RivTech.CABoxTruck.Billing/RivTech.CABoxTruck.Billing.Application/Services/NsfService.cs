﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class NsfService : INsfService
    {
        private readonly IPaymentRepository _paymentRepository;

        public NsfService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public async Task<NsfCountDto> GetNsfCount(Guid riskId)
        {
            return new NsfCountDto
            {
                NsfCount = await _paymentRepository.GetNsfCount(riskId)
            };
        }
    }
}
