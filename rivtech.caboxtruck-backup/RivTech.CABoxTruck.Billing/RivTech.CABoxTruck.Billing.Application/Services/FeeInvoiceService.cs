﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class FeeInvoiceService : IFeeInvoiceService
    {
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoiceNumberService _invoiceNumberService;
        private readonly IUserDateService _userDateService;
        private readonly IRiskRepository _riskRepository;

        public FeeInvoiceService(
            IInstallmentRepository installmentRepository,
            IPaymentRepository paymentRepository,
            IInvoiceRepository invoiceRepository,
            IInvoiceNumberService invoiceNumberService,
            IUserDateService userDateService,
            IRiskRepository riskRepository
        )
        {
            _installmentRepository = installmentRepository;
            _paymentRepository = paymentRepository;
            _invoiceRepository = invoiceRepository;
            _invoiceNumberService = invoiceNumberService;
            _userDateService = userDateService;
            _riskRepository = riskRepository;
        }

        public async Task<Invoice> InvoiceFee(Guid riskId, decimal amount, AmountSubType amountSubType, string appId)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            var feeInstallment = new Installment(riskId, currentDate, currentDate, 0, InstallmentType.Installment, _userDateService.GetUser());
            decimal previousBalance = await ComputePreviousBalance(riskId);
            List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();
            invoiceDetails.Add(new InvoiceDetail(amount, amountSubType));

            var riskFromDb = await _riskRepository.FindAsync(riskId);
            DateTime invoiceDueDate = currentDate;
            if (riskFromDb.EffectiveDate > currentDate)
            {
                invoiceDueDate = riskFromDb.EffectiveDate;
            }

            Invoice invoice = new Invoice(riskId, await _invoiceNumberService.GenerateInvoiceNumber(appId, riskFromDb.PolicyNumber), currentDate,
                        invoiceDueDate, previousBalance, amount, invoiceDetails, _userDateService.GetUser());
            await _invoiceRepository.AddAsync(invoice);
            await _invoiceRepository.SaveChangesAsync();

            feeInstallment.SetAsInvoiced(invoice.Id);

            await _installmentRepository.AddAsync(feeInstallment);
            await _installmentRepository.SaveChangesAsync();

            return invoice;
        }

        private async Task<decimal> ComputePreviousBalance(Guid riskId)
        {
            decimal previousPaymentAmount = await _paymentRepository.GetTotalPaymentAsync(riskId);
            decimal previousInvoicedAmount = await _invoiceRepository.GetTotalInvoicedAmountAsync(riskId);

            return previousInvoicedAmount - previousPaymentAmount;
        }
    }
}
