﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class AuthNetAccountService: IAuthNetAccountService
    {
        private readonly IMapper _mapper;
        private readonly IAuthNetAccountRepository _authNetAccountRepository;
        private readonly IAuthNetProfileRepository _authNetProfileRepository;

        public AuthNetAccountService(
            IMapper mapper,
            IAuthNetAccountRepository authNetAccountRepository,
            IAuthNetProfileRepository authNetProfileRepository
        )
        {
            _mapper = mapper;
            _authNetAccountRepository = authNetAccountRepository;
            _authNetProfileRepository = authNetProfileRepository;
        }

        public async Task<AuthNetProfileDto> SaveAuthNetAccount(Guid riskId, Guid paymentAccountId, string customerPaymentProfileId)
        {
            var authNetProfile = await _authNetProfileRepository.FindAsync(x => x.RiskId == riskId);
            var authNetAccount = await _authNetAccountRepository.AddAsync(
                new AuthNetAccount(
                    paymentAccountId,
                    authNetProfile.Id,
                    customerPaymentProfileId
                ));
            await _authNetAccountRepository.SaveChangesAsync();

            var authNetProfileDto = _mapper.Map<AuthNetProfileDto>(authNetProfile);
            authNetProfileDto.AuthNetAccount = new List<AuthNetAccountDto>() { _mapper.Map<AuthNetAccountDto>(authNetAccount) };
            return authNetProfileDto;
        }

        public async Task<AuthNetAccountDto> DeleteAuthNetAccount(Guid paymentAccountId)
        {
            var account = await _authNetAccountRepository.FindAsync(x => x.PaymentAccountId == paymentAccountId);
            var deletedAccount = _authNetAccountRepository.Remove(account);
            await _authNetAccountRepository.SaveChangesAsync();
            return _mapper.Map<AuthNetAccountDto>(deletedAccount);
        }     
    }
}
