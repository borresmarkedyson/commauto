﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedPaymentViewService : ISuspendedPaymentViewService
    {
        private readonly IMapper _mapper;
        private readonly ISuspendedPaymentRepository _suspendedPaymentRepository;
        private readonly ISuspendedReturnDetailRepository _suspendedReturnDetailRepository;
        private readonly ISuspendedStatusHistoryRepository _suspendedStatusHistoryRepository;

        public SuspendedPaymentViewService(
            IMapper mapper,
            ISuspendedPaymentRepository suspendedPaymentRepository,
            ISuspendedReturnDetailRepository suspendedReturnDetailRepository,
            ISuspendedStatusHistoryRepository suspendedStatusHistoryRepository
        )
        {
            _mapper = mapper;
            _suspendedPaymentRepository = suspendedPaymentRepository;
            _suspendedReturnDetailRepository = suspendedReturnDetailRepository;
            _suspendedStatusHistoryRepository = suspendedStatusHistoryRepository;
        }

        public async Task<List<SuspendedPaymentViewDto>> GetAllByStatus(string statusId)
        {
            if (!Enumeration.GetAllId<SuspendedStatus>().Contains(statusId))
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_STATUS_INVALID);
            }

            var result = await _suspendedPaymentRepository.GetAsync(p => p.StatusId == statusId, p => p.Source, p => p.Reason, p => p.Status);
            var resultView = _mapper.Map<List<SuspendedPaymentViewDto>>(result);

            if (statusId == SuspendedStatus.Pending.Id)
            {
                foreach (var view in resultView)
                {
                    var history = _suspendedStatusHistoryRepository.GetFirstPendingData(view.Id);
                    view.Comment = history.Comment;
                }
            }            
            return resultView;
        }

        public async Task<List<ReturnSuspendedViewDto>> GetReturnedSuspendedPayment()
        {
            var resultList = new List<ReturnSuspendedViewDto>();
            var suspendedPayments = await _suspendedPaymentRepository.GetAsync(p => p.StatusId == SuspendedStatus.Returned.Id);

            foreach (var suspendedPayment in suspendedPayments)
            {
                var returnDetails = await _suspendedReturnDetailRepository.FindAsync(d => d.SuspendedPaymentId == suspendedPayment.Id,
                    d => d.Payee, d => d.SuspendedPayment);
                resultList.Add(_mapper.Map<ReturnSuspendedViewDto>(returnDetails));
            };

            return resultList;
        }

        public async Task<SuspendedPaymentDetailViewDto> GetDetailView(Guid suspendedPaymentId)
        {
            var result = _suspendedPaymentRepository.GetSuspendedPaymentWithDetail(suspendedPaymentId);
            var pendingHistory = _suspendedStatusHistoryRepository.GetFirstPendingData(suspendedPaymentId);
            var resultView = _mapper.Map<SuspendedPaymentDetailViewDto>(result);
            resultView.Comment = pendingHistory.Comment;

            if (result.StatusId == SuspendedStatus.Returned.Id)
            {
                var returnDetails = await _suspendedReturnDetailRepository.FindAsync(d => d.SuspendedPaymentId == suspendedPaymentId,
                    d => d.Payee);                
                resultView.ReturnDetail = _mapper.Map<ReturnSuspendedDetailViewDto>(returnDetails);
            }

            return resultView;
        }
    }
}
