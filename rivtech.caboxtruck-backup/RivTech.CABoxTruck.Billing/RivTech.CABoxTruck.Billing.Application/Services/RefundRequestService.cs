﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class RefundRequestService : IRefundRequestService
    {
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;
        private readonly IRefundRequestRepository _refundRequestRepository;
        private readonly IRefundPayeeRepository _refundPayeeRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ITransactionDetailRepository _transactionDetailRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IUserDateService _userDateService;

        public RefundRequestService(
            IMapper mapper,
            IValidationService validationService,
            IRefundRequestRepository refundRequestRepository,
            IRefundPayeeRepository refundPayeeRepository,
            IPaymentRepository paymentRepository,
            ITransactionDetailRepository transactionDetailRepository,
            IRiskRepository riskRepository,
            IUserDateService userDateService
        )
        {
            _mapper = mapper;
            _validationService = validationService;
            _refundRequestRepository = refundRequestRepository;
            _refundPayeeRepository = refundPayeeRepository;
            _paymentRepository = paymentRepository;
            _transactionDetailRepository = transactionDetailRepository;
            _riskRepository = riskRepository;
            _userDateService = userDateService;
        }

        public List<RefundRequestRiskViewDto> GetRiskListWithRefundRequest(string applicationId)
        {
            var result = _refundRequestRepository.GetRisksWithValidRefundRequest(_userDateService.GetUser().UserDate.Date, applicationId);
            return _mapper.Map<List<RefundRequestRiskViewDto>>(result);
        }

        public async Task<List<RefundRequestViewDto>> AutoCheckRefundRequest(AutoCheckRefundRequestDto requests)
        {
            var validationResult = new AutoCheckRefundRequestValidator().Validate(requests);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var refundRequestViews = new List<RefundRequestViewDto>();
            decimal totalPaymentAmount;
            decimal totalTransactionAmount;
            bool isExistingData;
            foreach (var request in requests.RefundPayeeRequest)
            {
                isExistingData = true;
                totalPaymentAmount = await _paymentRepository.GetTotalPaymentAsync(request.RiskId);
                totalTransactionAmount = await _transactionDetailRepository.GetTotalTransactionAmount(request.RiskId);

                var refundRequest = await _refundRequestRepository.FindAsync(x => x.RiskId == request.RiskId && x.CheckDate == null, x => x.RefundPayee);
                var refundRequestDto = _mapper.Map<RefundReqDto>(refundRequest);
                if (refundRequestDto == null) //if no existing unchecked refundrequest
                {
                    //create refund request on the fly
                    refundRequestDto = new RefundReqDto()
                    {
                        RiskId = request.RiskId,
                        Amount = UtilConstant.REFUNDREQUEST_OVERPAYMENT_DEFAULTAMOUNT,
                        RefundToTypeId = RefundToType.Insured.Id,
                        CreatedDate = DateTime.Now,
                        RefundPayee = request.RefundPayee
                    };
                    isExistingData = false;

                    //RULE: FOR AUTOCHECK overpayment <= 2500, save data on db
                    if (totalPaymentAmount - totalTransactionAmount <= UtilConstant.REFUNDREQUEST_OVERPAYMENT_MAXAMOUNT)
                    {
                        refundRequestDto.Amount = totalPaymentAmount - totalTransactionAmount;
                        refundRequestDto = await CreateRefundRequestData(refundRequestDto);
                        isExistingData = true;
                    }
                }
                else
                {
                    refundRequestDto.RefundPayee = request.RefundPayee;
                    refundRequestDto = await UpdateRefundRequest(_mapper.Map<UpdateRefundRequestDto>(refundRequestDto));
                }

                //create refundrequest view
                var refundRequestView = _mapper.Map<RefundRequestViewDto>(refundRequestDto);
                refundRequestView.OverpaymentAmount = totalPaymentAmount - totalTransactionAmount;
                refundRequestView.TotalTransactionAmount = totalTransactionAmount;
                refundRequestView.TotalPaymentAmount = totalPaymentAmount;
                refundRequestView.IsChecked = isExistingData;

                refundRequestViews.Add(refundRequestView);
            }
            return refundRequestViews;
        }

        private async Task<RefundReqDto> CreateRefundRequestData(RefundReqDto request)
        {
            var refundRequest = await _refundRequestRepository.AddAsync(
                new RefundRequest(
                    request.RiskId,
                    request.Amount,
                    Enumeration.GetEnumerationById<RefundToType>(request.RefundToTypeId),
                    _mapper.Map<RefundPayee>(request.RefundPayee),
                    _userDateService.GetUser()
                ));
            await _refundRequestRepository.SaveChangesAsync();
            return _mapper.Map<RefundReqDto>(refundRequest);
        }

        public async Task<RefundReqDto> CreateRefundRequest(CreateRefundRequestDto request)
        {
            var validationResult = new CreateRefundRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await CreateRefundRequestData(_mapper.Map<RefundReqDto>(request));
        }

        public async Task<RefundReqDto> UpdateRefundRequest(UpdateRefundRequestDto request)
        {
            var validationResult = new UpdateRefundRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await UpdateRefundRequestData(request);
        }

        private async Task<RefundReqDto> UpdateRefundRequestData(UpdateRefundRequestDto request)
        {
            var refundRequest = await _refundRequestRepository.FindAsync(x => x.Id == request.RefundRequestId, x => x.RefundPayee);
            if (refundRequest == null)
            {
                throw new ParameterException(ExceptionMessages.REFUNDREQUESTID_INVALID);
            }

            request.RefundPayee.Id = refundRequest.RefundPayeeId;
            _mapper.Map(request, refundRequest);

            var updatedRefundRequest = _refundRequestRepository.Update(refundRequest);
            await _refundRequestRepository.SaveChangesAsync();

            return _mapper.Map<RefundReqDto>(updatedRefundRequest);
        }

        public async Task<RefundReqDto> DeleteRefundRequest(Guid refundRequestId)
        {
            var refundRequest = await _refundRequestRepository.FindAsync(x => x.Id == refundRequestId, x => x.RefundPayee);
            if (refundRequest == null)
            {
                throw new ParameterException(ExceptionMessages.REFUNDREQUESTID_INVALID);
            }

            var deletedPaymentAccount = _refundRequestRepository.Remove(refundRequest);
            await _refundRequestRepository.SaveChangesAsync();

            _refundPayeeRepository.Remove(refundRequest.RefundPayee);
            await _refundPayeeRepository.SaveChangesAsync();

            return _mapper.Map<RefundReqDto>(deletedPaymentAccount);
        }
    }
}
