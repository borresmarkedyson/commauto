﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.IO;
using System.Net;
using System.Collections.Generic;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using System;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Threading.Tasks;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using System.Linq;
using AutoMapper;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedDirectBillService : ISuspendedDirectBillService
    {
        private readonly IFtpService _ftpService;
        private readonly IValidationService _validationService;
        private readonly ISuspendedPaymentService _suspendedPaymentService;
        private readonly IUserDateService _userDateService;
        private readonly ILog4NetService _logger;
        private readonly IMapper _mapper;

        public SuspendedDirectBillService(
            IFtpService ftpService,
            IValidationService validationService,
            ISuspendedPaymentService suspendedPaymentService,
            IUserDateService userDateService,
            ILog4NetService logger,
            IMapper mapper
        )
        {
            _ftpService = ftpService;
            _validationService = validationService;
            _suspendedPaymentService = suspendedPaymentService;
            _userDateService = userDateService;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<SuspendedPaymentJobViewDto> ImportDirectBillFile(DirectBillRequestDto jobRequest)
        {
            //validate job request
            var validationResult = new DirectBillRequestValidator().Validate(jobRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var directbillSuspendedPayments = GetPaymentFromDirectBill(SuspendedDirectBillCarrier.CentauriSpecialtyInsuranceCompany.Id);
            directbillSuspendedPayments.AddRange(GetPaymentFromDirectBill(SuspendedDirectBillCarrier.CentauriNationalInsuranceCompany.Id));
            
            var error = new List<FailedRiskViewDto>();
            var createdSuspendedPayments = new List<SuspendedPaymentDto>();
            foreach (var dbpayment in directbillSuspendedPayments)
            {
                try
                {
                    createdSuspendedPayments.Add(await _suspendedPaymentService.CreateSuspendedPayment(dbpayment));
                }
                catch (Exception e)
                {
                    error.Add(new FailedRiskViewDto()
                    {
                        RiskId = dbpayment.PolicyId,
                        PolicyNumber = dbpayment.PolicyNumber,
                        ErrorMessage = $"{dbpayment.DirectBillCarrierId}: {e.Message}"
                    });
                    await _logger.Error($"{dbpayment.DirectBillCarrierId} {dbpayment.PolicyId} {dbpayment.PolicyNumber}: {e.Message}", e.ToString());
                }
            }

            return new SuspendedPaymentJobViewDto()
            {
                SuspendedPayment = createdSuspendedPayments,
                FailedRisk = error
            };
        }

        private List<CreateSuspendedPaymentRequestDto> GetPaymentFromDirectBill(string directBillCarrierId)
        {
            //create ftp request
            var currentDate = _userDateService.GetUser().UserDate.ToString("yyyyMMdd");
            var directBillSettings = Service.AppSettings.DirectBill;
            var fileName = directBillCarrierId == SuspendedDirectBillCarrier.CentauriSpecialtyInsuranceCompany.Id ?
                directBillSettings.FTP.FileNameCSIC : directBillSettings.FTP.FileNameCNIC;
            directBillSettings.FTP.FileName = $"{fileName}{currentDate}.txt";
            var dlFilePath = $"{directBillSettings.DLFileDirectory}{directBillSettings.FTP.FileName}";

            //download file
            if (!_ftpService.DownloadFile(_mapper.Map<FTPSettingsDto>(directBillSettings.FTP), directBillSettings.DLFileDirectory, dlFilePath)) return new List<CreateSuspendedPaymentRequestDto>();

            var lockboxSuspendedPayments = new List<CreateSuspendedPaymentRequestDto>();
            string[] lines = File.ReadAllLines(dlFilePath);
            foreach (string line in lines)
            {
                string[] col = line.Split(',');                

                var suspendedPayment = new CreateSuspendedPaymentRequestDto()
                {
                    SuspendedDetail = new SuspendedDetailDto()
                    {
                        ClientId = FormatString(col[0]),
                        ClientName = FormatString(col[1]),
                        GroupId = FormatString(col[6]),
                        GroupDescription = FormatString(col[7]),
                        TransactionNum = FormatString(col[8])
                    },
                    SuspendedPayer = new SuspendedPayerDto()
                    {
                        FirstName = FormatString(col[9]),
                        MiddleInitial = FormatString(col[10]),
                        LastName = FormatString(col[11]),
                        Address1 = FormatString(col[12]),
                        Address2 = FormatString(col[13]),
                        City = FormatString(col[14]),
                        State = FormatString(col[15]),
                        Zip = FormatString(col[16]),
                    },
                    PolicyNumber = FormatString(col[2]),
                    DirectBillCarrierId = directBillCarrierId,
                    SourceId = SuspendedSource.DirectBill.Id
                };
                if (DateTime.TryParse(FormatString(col[4]), out DateTime date)) suspendedPayment.ReceiptDate = date;
                if (DateTime.TryParse(FormatString(col[5]), out date)) suspendedPayment.SuspendedDetail.SettlementDate = date;
                if (decimal.TryParse(FormatString(col[3]), out decimal amount)) suspendedPayment.Amount = amount;

                lockboxSuspendedPayments.Add(suspendedPayment);
            }
            return lockboxSuspendedPayments;
        }

        private string FormatString(string str)
        {
            return str.TrimStart('"').TrimEnd('"');
        }
    }
}

