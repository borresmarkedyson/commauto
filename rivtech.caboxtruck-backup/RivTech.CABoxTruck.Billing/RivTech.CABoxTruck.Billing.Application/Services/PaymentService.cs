﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DomainEvents;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.Factory.Payments;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IMapper _mapper;
        private readonly IPaymentDetailService _paymentDetailService;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IPaymentStrategyFactory _paymentStrategyFactory;
        private readonly IPaymentRequestStrategyFactory _paymentRequestStrategyFactory;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IValidationService _validationService;
        private readonly IPaymentValidationService _paymentValidationService;
        private readonly IPaymentBreakdownService _paymentBreakdownService;
        private readonly IPaymentProfileService _paymentProfileService;
        private readonly IExcessPaymentService _excessPaymentService;
        private readonly IRiskRepository _riskRepository;
        private readonly IUserDateService _userDateService;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        IPaymentProcessor _paymentProcessor;
        IDomainEventDispatcher _domainEventDispatcher;

        public PaymentService(
            IMapper mapper,
            IPaymentDetailService paymentDetailService,
            IPaymentRepository paymentRepository,
            IPaymentStrategyFactory paymentStrategyFactory,
            IPaymentRequestStrategyFactory paymentRequestStrategyFactory,
            IApplicationUserRepository applicationUserRepository,
            IValidationService validationService,
            IPaymentBreakdownService paymentBreakdownService,
            IPaymentProfileService paymentProfileService,
            IExcessPaymentService excessPaymentService,
            IPaymentValidationService paymentValidationService,
            IRiskRepository riskRepository,
            IPaymentProcessor paymentProcessor,
            IUserDateService userDateService,
            IDomainEventDispatcher domainEventDispatcher,
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository
        )
        {
            _mapper = mapper;
            _paymentDetailService = paymentDetailService;
            _paymentRepository = paymentRepository;
            _paymentStrategyFactory = paymentStrategyFactory;
            _paymentRequestStrategyFactory = paymentRequestStrategyFactory;
            _applicationUserRepository = applicationUserRepository;
            _validationService = validationService;
            _paymentValidationService = paymentValidationService;
            _paymentBreakdownService = paymentBreakdownService;
            _paymentProfileService = paymentProfileService;
            _excessPaymentService = excessPaymentService;
            _riskRepository = riskRepository;
            _userDateService = userDateService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentProcessor = paymentProcessor
                ?? throw new ArgumentNullException(nameof(paymentProcessor));
            _domainEventDispatcher = domainEventDispatcher
                ?? throw new ArgumentNullException(nameof(domainEventDispatcher));
        }

        public async Task<PostPaymentViewDto> PostPayment(PaymentRequestDto paymentRequest)
        {
            paymentRequest = await _paymentBreakdownService.ComputeBreakdown(paymentRequest);
            if (paymentRequest.PaymentSummary.IsRecurringPayment)
            {
                await _paymentProfileService.CreateProfileOnPayment(paymentRequest);
            }

            try
            {
                return await PostBasicPayment(paymentRequest);
            }
            catch (Exception ex)
            {
                if (paymentRequest.PaymentSummary.IsRecurringPayment)
                {
                    await _paymentProfileService.DeleteProfile(paymentRequest.RiskId);
                }

                throw ex;
            }
        }

        public async Task<PostPaymentViewDto> PostBasicPayment(PaymentRequestDto paymentRequest)
        {
            var validator = new BasicPaymentRequestValidator();
            var validationResult = validator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            if (paymentRequest.PaymentSummary.InstrumentId != InstrumentType.Adjustment.Id
                && paymentRequest.PaymentSummary.InstrumentId != InstrumentType.WriteOff.Id
                && paymentRequest.PaymentSummary.InstrumentId != InstrumentType.Transferred.Id)
            {
                await _paymentValidationService.ValidateMinimumPayment(paymentRequest.RiskId, paymentRequest.PaymentSummary.Amount, paymentRequest.AppId);
            }

            await _excessPaymentService.VerifyExcessPayment(paymentRequest);

            var paymentResult = await SendToPNC(paymentRequest);
            var referenceNumber = paymentResult != null ? paymentResult.ReferenceNumber : null;
            var payment = await SavePayment(paymentRequest, referenceNumber);
            var risk = await _riskRepository.FindAsync(paymentRequest.RiskId);
            var profile = await _paymentProfileService.GetProfile(paymentRequest.RiskId);

            await RebalanceOverpayment(paymentRequest.RiskId, "Payment");
            return new PostPaymentViewDto(payment, null, risk, profile);
        }

        public PaymentProviderResponseDto SendToPaymentProvider(PaymentRequestDto paymentRequest)
        {
            var paymentSummary = paymentRequest.PaymentSummary;

            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    var paymentReqStrategy = _paymentRequestStrategyFactory.GetRequestStrategy(paymentRequest.AppId);
                    var paymentStrategy = _paymentStrategyFactory.GetStrategy(paymentRequest.AppId);
                    var paymentReq = paymentReqStrategy.GeneratePostRequest(paymentRequest, _mapper);
                    return paymentStrategy.PostPayment(paymentReq);
                case var _ when paymentSummary.InstrumentId == InstrumentType.Cash.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Check.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Adjustment.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.WriteOff.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Transferred.Id:
                    return null;
                default:
                    throw new ParameterException(ExceptionMessages.INVALID_INSTRUMENT_TYPE);
            }
        }

        public async Task<PaymentDto> SavePayment(PaymentRequestDto paymentRequest, string referenceNumber, string paymentTypeId = null)
        {
            var paymentSummary = paymentRequest.PaymentSummary;

            var paymentDetails = await _paymentDetailService.CreatePaymentDetail(paymentRequest.RiskId, paymentSummary);
            var payment = await _paymentRepository.AddAsync(
                new Payment(
                    paymentRequest.RiskId,
                    paymentSummary.Amount,
                    paymentSummary.EffectiveDate,
                    referenceNumber,
                    Enumeration.GetEnumerationById<InstrumentType>(paymentSummary.InstrumentId),
                    paymentDetails,
                    paymentSummary.Comment,
                    _userDateService.GetUser(),
                    paymentTypeId
                ));
            var payeeInfo = _mapper.Map<PayeeInfo>(paymentRequest.BillingDetail?.BillingAddress);
            if (payeeInfo != null)
            {
                payeeInfo.Email = paymentRequest.PaymentSummary?.Email;
                payment.SetPayeeInfo(payeeInfo);
            }

            await _paymentRepository.SaveChangesAsync();
            return _mapper.Map<PaymentDto>(payment);
        }

        public async Task<List<PaymentDto>> GetAllByRiskId(Guid riskId)
        {
            var result = await _paymentRepository.GetAllByRiskId(riskId);
            return _mapper.Map<List<PaymentDto>>(result.ToList());
        }

        public async Task RebalanceOverpayment(Guid riskId, string actionSource)
        {
            var payments = await _paymentRepository.GetAllByRiskId(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;

            foreach (var payment in payments)
            {
                foreach (var transactionDetail in payment.PaymentDetails)
                {
                    if (transactionDetail?.AmountTypeId == AmountType.Premium.Id)
                    {
                        premiumAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.PremiumFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.Tax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                }
            }
            var transactionPremiumFeeAndTax = await ComputePremiumsFeesAndTaxesFromTransactions(riskId);

            var overpayments = payments.Where(y => y.VoidDate == null).SelectMany(p => p.PaymentDetails).Where(x => x.AmountTypeId == AmountType.Overpayment.Id);

            foreach (PaymentDetail overPayment in overpayments.ToList())
            {
                decimal overpaymentAmount = overPayment.Amount;

                if (overpaymentAmount > 0)
                {
                    decimal totalOverpayment = 0;

                    var paymentDetails = new List<PaymentDetail>();

                    decimal amount = 0;

                    if (feeAmount < transactionPremiumFeeAndTax.Fees && overpaymentAmount > 0)
                    {
                        amount = transactionPremiumFeeAndTax.Fees - feeAmount;

                        if (overpaymentAmount >= amount)
                        {
                            overpaymentAmount -= amount;
                            paymentDetails.Add(new PaymentDetail(amount, AmountType.PremiumFee, overPayment.PaymentId));

                            totalOverpayment += amount;
                            feeAmount += amount;
                        }
                        else
                        {
                            paymentDetails.Add(new PaymentDetail(overpaymentAmount, AmountType.PremiumFee, overPayment.PaymentId));

                            totalOverpayment += overpaymentAmount;
                            feeAmount += overpaymentAmount;

                            overpaymentAmount = 0;
                        }

                    }

                    if (taxAmount < transactionPremiumFeeAndTax.Tax && overpaymentAmount > 0)
                    {
                        amount = 0;
                        amount = transactionPremiumFeeAndTax.Tax - taxAmount;

                        if (overpaymentAmount >= amount)
                        {
                            overpaymentAmount -= amount;
                            paymentDetails.Add(new PaymentDetail(amount, AmountType.Tax, overPayment.PaymentId));

                            totalOverpayment += amount;
                            taxAmount += amount;
                        }
                        else
                        {
                            paymentDetails.Add(new PaymentDetail(overpaymentAmount, AmountType.Tax, overPayment.PaymentId));

                            totalOverpayment += overpaymentAmount;
                            taxAmount += overpaymentAmount;

                            overpaymentAmount = 0;
                        }

                    }

                    if (premiumAmount < transactionPremiumFeeAndTax.Premium && overpaymentAmount > 0)
                    {
                        amount = 0;
                        amount = transactionPremiumFeeAndTax.Premium - premiumAmount;

                        BillingSummary billingSummary = new BillingSummary(
                        await _riskRepository.GetDetailsAsync(riskId),
                        await _transactionRepository.GetAllByRisk(riskId),
                        await _installmentRepository.GetAllByRisk(riskId),
                        await _invoiceRepository.GetAllByRisk(riskId),
                        (List<Payment>)await _paymentRepository.GetAllByRiskId(riskId),
                        _userDateService.GetUser().UserDate);

                        var totalALPremiumBilled = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Billed;
                        var totalPDPremiumBilled = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Billed;

                        var totalALPremiumWritten = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Written;
                        var totalPDPremiumWritten = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Written;

                        var totalALPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Paid;
                        var totalPDPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Paid;

                        var totalPremiumBilled = totalALPremiumBilled + totalPDPremiumBilled;
                        var totalPremiumWritten = totalALPremiumWritten + totalPDPremiumWritten;
                        var totalPremiumPaid = totalALPremiumPaid + totalPDPremiumPaid;

                        var isPremiumPaymentOverBilled = (amount + totalPremiumPaid) > totalPremiumBilled && totalALPremiumWritten > totalALPremiumBilled;

                        var totalALPremiumPercentage = (isPremiumPaymentOverBilled ? totalALPremiumWritten / totalPremiumWritten : totalALPremiumBilled / totalPremiumBilled);
                        var totalPDPremiumPercentage = (isPremiumPaymentOverBilled ? totalPDPremiumWritten / totalPremiumWritten : totalPDPremiumBilled / totalPremiumBilled);

                        decimal totalALPremiumAmount = 0;
                        decimal totalPDPremiumAmount = 0;

                        decimal aLCommissionAmount = 0;
                        decimal pDCommissionAmount = 0;

                        if (isPremiumPaymentOverBilled)
                        {
                            var premiumPayments = payments.Where(y => y.VoidDate == null).SelectMany(p => p.PaymentDetails).Where(x => x.AmountTypeId == AmountType.Premium.Id || x.AmountTypeId == AmountType.Commission.Id);
                            decimal totalPremiumPayment = 0;

                            foreach (var premiumPayment in premiumPayments)
                            {
                                if (premiumPayment != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(premiumPayment.AmountSubTypeId))
                                    {
                                        totalPremiumPayment += premiumPayment.Amount;
                                        paymentDetails.Add(new PaymentDetail(-premiumPayment.Amount, Enumeration.GetEnumerationById<AmountType>(premiumPayment.AmountTypeId), Enumeration.GetEnumerationById<AmountSubType>(premiumPayment.AmountSubTypeId), premiumPayment.PaymentId));
                                    }
                                    else
                                    {
                                        totalPremiumPayment += premiumPayment.Amount;
                                        paymentDetails.Add(new PaymentDetail(-premiumPayment.Amount, Enumeration.GetEnumerationById<AmountType>(premiumPayment.AmountTypeId), premiumPayment.PaymentId));
                                    }
                                }
                            }

                            var groupedPremiumPayments = payments.Where(y => y.VoidDate == null).SelectMany(p => p.PaymentDetails).Where(x => x.AmountTypeId == AmountType.Premium.Id || x.AmountTypeId == AmountType.Commission.Id).GroupBy(
                                                    p => p.PaymentId,
                                                    p => p.Amount,
                                                    (key, g) => new { PaymentId = key, Amount = g.Sum() });

                            foreach (var groupedPremiumPayment in groupedPremiumPayments)
                            {
                                if (groupedPremiumPayment != null)
                                {
                                    totalALPremiumAmount = totalALPremiumPercentage * groupedPremiumPayment.Amount;
                                    totalPDPremiumAmount = totalPDPremiumPercentage * groupedPremiumPayment.Amount;

                                    aLCommissionAmount = 0;
                                    pDCommissionAmount = 0;

                                    if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                                    {
                                        aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                                        pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                                        totalALPremiumAmount -= aLCommissionAmount;
                                        totalPDPremiumAmount -= pDCommissionAmount;
                                    }

                                    if (totalALPremiumAmount > 0)
                                    {
                                        paymentDetails.Add(new PaymentDetail(totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium, groupedPremiumPayment.PaymentId));
                                    }

                                    if (totalPDPremiumAmount > 0)
                                    {
                                        paymentDetails.Add(new PaymentDetail(totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium, groupedPremiumPayment.PaymentId));
                                    }

                                    if (aLCommissionAmount > 0)
                                    {
                                        paymentDetails.Add(new PaymentDetail(aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission, groupedPremiumPayment.PaymentId));
                                    }

                                    if (pDCommissionAmount > 0)
                                    {
                                        paymentDetails.Add(new PaymentDetail(pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission, groupedPremiumPayment.PaymentId));
                                    }
                                }
                            }
                        }

                        if (overpaymentAmount >= amount)
                        {
                            overpaymentAmount -= amount;

                            totalALPremiumAmount = totalALPremiumPercentage * amount;
                            totalPDPremiumAmount = totalPDPremiumPercentage * amount;
                            premiumAmount += amount;

                            aLCommissionAmount = 0;
                            pDCommissionAmount = 0;

                            if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                            {
                                aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                                pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                                totalALPremiumAmount -= aLCommissionAmount;
                                totalPDPremiumAmount -= pDCommissionAmount;
                            }

                            if (totalALPremiumAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium, overPayment.PaymentId));
                            }

                            if (totalPDPremiumAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium, overPayment.PaymentId));
                            }

                            if (aLCommissionAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission, overPayment.PaymentId));
                            }

                            if (pDCommissionAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission, overPayment.PaymentId));
                            }

                            totalOverpayment += amount;
                        }
                        else
                        {
                            totalALPremiumAmount = totalALPremiumPercentage * overpaymentAmount;
                            totalPDPremiumAmount = totalPDPremiumPercentage * overpaymentAmount;
                            premiumAmount += overpaymentAmount;

                            aLCommissionAmount = 0;
                            pDCommissionAmount = 0;

                            if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                            {
                                aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                                pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                                totalALPremiumAmount -= aLCommissionAmount;
                                totalPDPremiumAmount -= pDCommissionAmount;
                            }

                            if (totalALPremiumAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium, overPayment.PaymentId));
                            }

                            if (totalPDPremiumAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium, overPayment.PaymentId));
                            }

                            if (aLCommissionAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission, overPayment.PaymentId));
                            }

                            if (pDCommissionAmount > 0)
                            {
                                paymentDetails.Add(new PaymentDetail(pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission, overPayment.PaymentId));
                            }

                            totalOverpayment += overpaymentAmount;

                            overpaymentAmount = 0;
                        }

                    }

                    if (totalOverpayment > 0)
                    {

                        decimal newOverpaymentAmount = overPayment.Amount - totalOverpayment;
                        overPayment.SetPaymentDetailAmount(newOverpaymentAmount);

                        await _paymentDetailService.UpdatePaymentDetail(overPayment);

                        await _paymentDetailService.SavePaymentDetail(paymentDetails);
                    }
                }
            }
        }

        private async Task<PremiumFeeAndTax> ComputePremiumsFeesAndTaxesFromTransactions(Guid riskId)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;


           var _transactions = await _transactionRepository.GetAllByRisk(riskId);

            if (_transactions != null)
            {
                foreach (var transaction in _transactions)
                {
                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                            {
                                totalPremium += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                            {
                                totalFees += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                            {
                                tax += transactionDetail.Amount;
                            }
                        }
                    }
                }
            }
            return new PremiumFeeAndTax(totalPremium, totalFees, tax);
        }

        public string AuthorizePayment(Guid riskId, AuthorizePaymentRequestDto request)
        {
            var validationResult = new AuthorizePaymentRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            if (request.WillPayLater || request.PaymentPlan == PaymentPlan.PremiumFinanced.Id)
            {
                return string.Empty;
            }

            if (request.InstrumentId != InstrumentType.CreditCard.Id &&
                request.InstrumentId != InstrumentType.EFT.Id &&
                request.InstrumentId != InstrumentType.RecurringCreditCard.Id &&
                request.InstrumentId != InstrumentType.RecurringECheck.Id)
            {
                return string.Empty;
            }

            request.BillingDetail.BillingAddress.Email = request.Email;
            DateTime currentDate = _userDateService.GetUser().UserDate;
            PercentDeposit percentDeposit = PercentDeposit.GetByPaymentPlanAndAppId(request.PaymentPlan, request.AppId);
            var paymentRequest = new PaymentRequestDto(riskId, percentDeposit.Value, request, currentDate);

            var paymentRequestValidator = new BasicPaymentRequestValidator();
            validationResult = paymentRequestValidator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var paymentSummary = paymentRequest.PaymentSummary;

            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    IPaymentWithValidationRequestStrategy paymentReqStrategy =
                        (IPaymentWithValidationRequestStrategy)_paymentRequestStrategyFactory.GetRequestStrategy(paymentRequest.AppId);
                    IPaymentWithValidationStrategy paymentStrategy =
                        (IPaymentWithValidationStrategy)_paymentStrategyFactory.GetStrategy(paymentRequest.AppId);
                    var paymentReq = paymentReqStrategy.CreateAuthorizeAmountRequest(paymentRequest, _mapper);
                    return paymentStrategy.AuthorizePayment(paymentReq);
                case var _ when paymentSummary.InstrumentId == InstrumentType.Cash.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Check.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Adjustment.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.WriteOff.Id:
                    return string.Empty;
                default:
                    throw new ParameterException(ExceptionMessages.INVALID_INSTALLMENT_TYPE);
            }
        }

        private PaymentProviderResponseDto CaptureAuthorizedPayment(PaymentRequestDto paymentRequest, string transactionId)
        {
            var paymentSummary = paymentRequest.PaymentSummary;

            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    IPaymentWithValidationRequestStrategy paymentReqStrategy =
                        (IPaymentWithValidationRequestStrategy)_paymentRequestStrategyFactory.GetRequestStrategy(paymentRequest.AppId);
                    IPaymentWithValidationStrategy paymentStrategy =
                        (IPaymentWithValidationStrategy)_paymentStrategyFactory.GetStrategy(paymentRequest.AppId);
                    var paymentReq = paymentReqStrategy.CreateCaptureAuthorizedAmountRequest(paymentRequest.PaymentSummary.Amount, transactionId);
                    return paymentStrategy.CaptureAuthorizedPayment(paymentReq);
                case var _ when paymentSummary.InstrumentId == InstrumentType.Cash.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Check.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Adjustment.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.WriteOff.Id:
                    return null;
                default:
                    throw new ParameterException(ExceptionMessages.INVALID_INSTALLMENT_TYPE);
            }
        }

        public async Task<PostPaymentViewDto> ExecutePayment(PaymentRequestDto paymentRequest, string transactionId)
        {
            var validator = new BasicPaymentRequestValidator();
            var validationResult = validator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            string referenceNumber = null;
            PaymentProviderResponseDto providerResponse = null;
            if (!string.IsNullOrWhiteSpace(transactionId))
            {
                providerResponse = CaptureAuthorizedPayment(paymentRequest, transactionId);
                referenceNumber = providerResponse.ReferenceId;
            }
            var payment = await SavePayment(paymentRequest, referenceNumber);

            return new PostPaymentViewDto(payment, providerResponse, await _riskRepository.FindAsync(paymentRequest.RiskId));
        }

        public async Task<PaymentProviderResponseDto> PostPaymentNoSaving(PaymentRequestDto paymentRequest)
        {
            paymentRequest = await _paymentBreakdownService.ComputeBreakdown(paymentRequest);
            if (paymentRequest.PaymentSummary.IsRecurringPayment)
            {
                await _paymentProfileService.CreateProfileOnPayment(paymentRequest);
            }

            var validationResult = new BasicPaymentRequestValidator().Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            await _excessPaymentService.VerifyExcessPayment(paymentRequest);
            return SendToPaymentProvider(paymentRequest);
        }

        public async Task<PostPaymentViewDto> PostChangePaymentPlanPayment(PaymentRequestDto paymentRequest)
        {
            paymentRequest = await _paymentBreakdownService.ComputeBreakdown(paymentRequest);
            var validator = new ChangePaymentPlanPaymentValidator();
            var validationResult = validator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            await _excessPaymentService.VerifyExcessPayment(paymentRequest);

            var providerResponse = SendToPaymentProvider(paymentRequest);
            var referenceNumber = providerResponse == null ? null : providerResponse.ReferenceId;
            var payment = await SavePayment(paymentRequest, referenceNumber);
            var risk = await _riskRepository.FindAsync(paymentRequest.RiskId);
            return new PostPaymentViewDto(payment, providerResponse, risk);
        }

        public async Task<PaymentResult> PayWithPostingAsync(
            CustomerDetails customer,
            InvoiceDetails invoice,
            PaymentCCEFT payment,
            string postedBy
        )
        {
            PaymentResult result = null;
            try
            {
                result = await _paymentProcessor.PayAsync(customer, invoice, payment);

                if (!result.Success && result.Errors.Length > 0)
                {
                    throw new Exception(result.Message == null ? result.Errors[0] : result.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public string ReducePolicyNumber(string policyNumber)
        {
            string prefix = policyNumber.Substring(3, 3);
            string state = policyNumber.Substring(7, 2);
            string effectiveYr = policyNumber.Substring(12, 1);
            string clientNumber = policyNumber.Substring(15, 10);
            string reducedPolicyNumber = $"{prefix}{state}{effectiveYr}{clientNumber}";



            return reducedPolicyNumber;
        }

        public async Task<PaymentResult> SendToPNC(PaymentRequestDto paymentRequest)
        {
            var paymentSummary = paymentRequest.PaymentSummary;

            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    var paymentSummaryCCEFT = _mapper.Map<PaymentSummaryCCEFT>(paymentRequest.PaymentPortalRequest);

                    var customer = new CustomerDetailsFactory().Create(paymentSummaryCCEFT);

                    string reducedPolicyNumber = !string.IsNullOrWhiteSpace(paymentSummaryCCEFT.PolicyNumber) ? ReducePolicyNumber(paymentSummaryCCEFT.PolicyNumber) : paymentSummaryCCEFT.PolicyNumber;

                    var invoice = new InvoiceDetails(paymentSummaryCCEFT.Amount, reducedPolicyNumber);
                    var payment = new PaymentFactory().Create(paymentSummaryCCEFT);

                    //CustomerDetails customer = new CustomerDetails(
                    //"Jon", "Snow", "jon.snow@got.com",
                    //new Address("Riverside Street", "Winterfell", "CA", "123456789"));
                    //InvoiceDetails invoice = new InvoiceDetails(10, "1234567");
                    //PaymentCCEFT payment = new CreditCardPayment(CardType.Visa, "4001143077274115", "123", 2022, 06);

                    //CustomerDetails customer = new CustomerDetails(
                    //"Jon",
                    //"Snow",
                    //"jon.snow@got.com",
                    //new Address("Riverside Street", "Winterfell", "CA", "123456789"));
                    //InvoiceDetails invoice = new InvoiceDetails(10, "1234567");
                    //PaymentCCEFT payment = new AchPayment("051000017", "091000019", AchType.Savings,
                    //    AchUsageType.Personal);

                    paymentRequest.PaymentPortalRequest.PostedBy = "susercommautobilling";

                    var result = await PayWithPostingAsync(
                        customer,
                        invoice,
                        payment,
                        paymentRequest.PaymentPortalRequest.PostedBy);

                    return result;
                case var _ when paymentSummary.InstrumentId == InstrumentType.Cash.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Check.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Adjustment.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.WriteOff.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.Transferred.Id:
                    return null;
                default:
                    throw new ParameterException(ExceptionMessages.INVALID_INSTRUMENT_TYPE);
            }
        }
    }
}