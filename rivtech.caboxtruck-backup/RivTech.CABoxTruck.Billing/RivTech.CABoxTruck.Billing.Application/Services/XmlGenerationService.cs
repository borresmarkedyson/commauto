﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Collections.Generic;
using System.Xml;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class XmlGenerationService : IXmlGenerationService
    {
        public XmlDocument GenerateXmlDocument(List<KeyValuePairDto> keyValuePairs, string bodyName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDocument.DocumentElement;
            xmlDocument.InsertBefore(xmlDeclaration, root);

            XmlElement body = xmlDocument.CreateElement(bodyName);
            xmlDocument.AppendChild(body);

            if (keyValuePairs == null)
            {
                return xmlDocument;
            }

            foreach (var keyValuePair in keyValuePairs)
            {
                XmlElement element = xmlDocument.CreateElement(keyValuePair.Key);
                XmlText text = xmlDocument.CreateTextNode(keyValuePair.Value);
                element.AppendChild(text);
                body.AppendChild(element);
            }

            return xmlDocument;
        }
    }
}
