﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class InvoiceNumberService : IInvoiceNumberService
    {

        private readonly IInvoiceRepository _invoiceRepository;
        private readonly ISharedEntityRepository _sharedEntityRepository;

        private const string CENTAURI_PREFIX = "CENT-";
        private const string BOXTRUCK_PREFIX = "I";

        private const int YEAR_LENGTH = 2;
        private const string STARTING_SEQUENCE_NUMBER = "000001";

        private const string BOXTRUCK_STARTING_SEQUENCE_NUMBER = "0000";

        public InvoiceNumberService(IInvoiceRepository invoiceRepository, ISharedEntityRepository sharedEntityRepository)
        {
            _invoiceRepository = invoiceRepository;
            _sharedEntityRepository = sharedEntityRepository;
        }
        public async Task<string> GenerateInvoiceNumber(string appId, string policyNumber)
        {
            if (appId == RivtechApplication.Centauri.Id)
            {
                SharedEntity centauriInvoiceNumber = await _sharedEntityRepository.FindAsync(SharedEntity.CentauriInvoiceNumber.Id);
                if (centauriInvoiceNumber == null)
                {
                    centauriInvoiceNumber = SharedEntity.CentauriInvoiceNumber;
                    await _sharedEntityRepository.AddAsync(centauriInvoiceNumber);
                    await _sharedEntityRepository.SaveChangesAsync();
                }
                string nextInvoiceNumber = GetNextInvoiceNumber(centauriInvoiceNumber.Value, CENTAURI_PREFIX);
                centauriInvoiceNumber.Value = nextInvoiceNumber;
                await _sharedEntityRepository.SaveChangesAsync();
                return nextInvoiceNumber;
            }
            else if (appId == RivtechApplication.Boxtruck.Id)
            {
                string newPolicyNumber = BOXTRUCK_PREFIX + policyNumber.Substring(1);

                var result = await _invoiceRepository.GetLatestInvoiceByNewInvoiceNumber(newPolicyNumber);
                
                string nextInvoiceNumber = GetBoxtruckNextInvoiceNumber(result, policyNumber);

                return nextInvoiceNumber;
            }
            else
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
        }

        private string GetNextInvoiceNumber(string currentInvoiceNumber, string prefix)
        {
            string currentYear = DateTime.Now.Year.ToString();
            string last2DigitsOfCurrentYear = currentYear.Substring(currentYear.Length - YEAR_LENGTH);
            if (currentInvoiceNumber == null)
            {
                return prefix + last2DigitsOfCurrentYear + STARTING_SEQUENCE_NUMBER;
            }

            string invoiceYear = currentInvoiceNumber.Substring(CENTAURI_PREFIX.Length, YEAR_LENGTH);
            string sequenceNumber = currentInvoiceNumber.Substring(CENTAURI_PREFIX.Length + YEAR_LENGTH);

            if (invoiceYear != last2DigitsOfCurrentYear)
            {
                return prefix + last2DigitsOfCurrentYear + STARTING_SEQUENCE_NUMBER;
            }
            else
            {
                int sequenceNumberInt = int.Parse(sequenceNumber);
                string nextSequenceNumber = (sequenceNumberInt + 1).ToString("D" + STARTING_SEQUENCE_NUMBER.Length.ToString());

                return prefix + last2DigitsOfCurrentYear + nextSequenceNumber;
            }
        }

        private string GetBoxtruckNextInvoiceNumber(Invoice invoice, string policyNumber)
        {
            int sequenceNumberInt = int.Parse(invoice == null ? BOXTRUCK_STARTING_SEQUENCE_NUMBER : invoice.InvoiceNumber.Substring(invoice.InvoiceNumber.LastIndexOf('_') + 1));

            string nextSequenceNumber = (sequenceNumberInt + 1).ToString("D" + BOXTRUCK_STARTING_SEQUENCE_NUMBER.Length.ToString());

            string nextInvoiceNumber = string.Format("{0}_{1}",BOXTRUCK_PREFIX + policyNumber.Substring(1), nextSequenceNumber);
            
            return nextInvoiceNumber;
        }
    }
}
