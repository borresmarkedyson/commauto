﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ExcessPaymentService : IExcessPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly ITransactionRepository _transactionRepository;

        public ExcessPaymentService(
            IPaymentRepository paymentRepository,
            ITransactionRepository transactionRepository
        )
        {
            _paymentRepository = paymentRepository;
            _transactionRepository = transactionRepository;
        }

        public async Task VerifyExcessPayment(PaymentRequestDto paymentRequest)
        {
            var paidAmount = await GetPaidAmounts(paymentRequest.RiskId);
            var transactionAmounts = await GetTransactionAmounts(paymentRequest.RiskId);


            if (!string.Equals(paymentRequest.PaymentSummary.InstrumentId, InstrumentType.Check.Id)
                && !string.Equals(paymentRequest.PaymentSummary.InstrumentId, InstrumentType.Cash.Id)
                && (paidAmount.TotalAmount + paymentRequest.PaymentSummary.Amount).RoundTo2Decimals() > transactionAmounts.TotalAmount)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
            }
            else if ((paidAmount.Premium + paymentRequest.PaymentSummary.PremiumAmount).RoundTo2Decimals() > transactionAmounts.Premium)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_PREMIUM_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
            }
            else if ((paymentRequest.PaymentSummary.FeeAmount > 0 && paymentRequest.PaymentSummary.TaxAmount > 0) && (paidAmount.Fees + paymentRequest.PaymentSummary.FeeAmount) + (paidAmount.Tax + paymentRequest.PaymentSummary.TaxAmount) > (transactionAmounts.Fees + transactionAmounts.Tax))
            {
                throw new ParameterException(ExceptionMessages.TOTAL_FEE_TAX_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
            }
            else if (paidAmount.Fees + paymentRequest.PaymentSummary.FeeAmount > transactionAmounts.Fees)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_FEE_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
            }
            else if (paidAmount.Tax + paymentRequest.PaymentSummary.TaxAmount > transactionAmounts.Tax)
            {
                throw new ParameterException(ExceptionMessages.TOTAL_TAX_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
            }
            else if (paidAmount.Premium + paymentRequest.PaymentSummary.PremiumAmount < 0
                || paidAmount.Fees + paymentRequest.PaymentSummary.FeeAmount < 0
                || paidAmount.Tax + paymentRequest.PaymentSummary.TaxAmount < 0)
            {
                throw new ParameterException(ExceptionMessages.NEGATIVE_TOTAL_PAYMENT_NOT_ALLOWED);
            }
        }
        private async Task<PremiumFeeAndTax> GetPaidAmounts(Guid riskId)
        {
            var payments = await _paymentRepository.GetAllByRiskId(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;
            foreach (var payment in payments)
            {
                foreach (var transactionDetail in payment.PaymentDetails)
                {
                    if (transactionDetail?.AmountTypeId == AmountType.Premium.Id)
                    {
                        premiumAmount += transactionDetail.Amount;
                    }
                    if (transactionDetail?.AmountTypeId == AmountType.Commission.Id)
                    {
                        premiumAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.PremiumFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.Tax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                }
            }

            return new PremiumFeeAndTax(premiumAmount, feeAmount, taxAmount);
        }

        private async Task<PremiumFeeAndTax> GetTransactionAmounts(Guid riskId)
        {
            var transactions = await _transactionRepository.GetAllByRisk(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;
            foreach (var transaction in transactions)
            {
                if (transaction.TransactionTypeId == TransactionType.Cancellation.Id ||
                    transaction.TransactionTypeId == TransactionType.Reinstatement.Id)
                {
                    continue;
                }

                foreach (var transactionDetail in transaction.TransactionDetails)
                {
                    if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                    {
                        premiumAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                }
            }

            return new PremiumFeeAndTax(premiumAmount, feeAmount, taxAmount);
        }
    }
}
