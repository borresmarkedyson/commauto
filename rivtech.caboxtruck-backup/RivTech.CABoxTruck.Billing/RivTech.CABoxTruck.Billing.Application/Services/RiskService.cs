﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class RiskService: IRiskService
    {
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;
        private readonly IRiskRepository _riskRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;

        public RiskService(
            IMapper mapper,
            IValidationService validationService,
            IRiskRepository riskRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository
        )
        {
            _mapper = mapper;
            _validationService = validationService;
            _riskRepository = riskRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
        }

        public async Task<RiskDto> UpdateRisk(UpdateRiskRequestDto request)
        {
            var validationResult = new UpdateRiskRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await UpdateRiskData(request);
        }

        private async Task<RiskDto> UpdateRiskData(UpdateRiskRequestDto request)
        {
            var risk = await _riskRepository.FindAsync(request.RiskId);
            if (risk == null) throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            _mapper.Map(request, risk);

            var updatedRisk = _riskRepository.Update(risk);
            await _riskRepository.SaveChangesAsync();

            return _mapper.Map<RiskDto>(updatedRisk);
        }

        public async Task<List<Guid>> GetAllRiskForNoticeCancellation(DateTime requestDate)
        {
            List<Guid> returnList = new List<Guid>();
            var risks = await _riskRepository.GetRisksForCancellation(requestDate);
            var riskIds = risks.Select(x => x.Id).Distinct().ToList();
            foreach (var riskId in riskIds)
            {
                var invoices = await _invoiceRepository.GetAllNotVoided(riskId);
                var pastDueAmount = invoices == null ? 0 : invoices.Where(x => x.DueDate.Date < requestDate).Sum(x => x.CurrentAmountInvoiced);
                if (pastDueAmount > 0)
                {
                    var payments = (List<Payment>)await _paymentRepository.GetAllByRiskId(riskId);
                    var amountPaid = payments.Count() == 0 ? 0 : payments.Sum(x => x.Amount);
                    if ((pastDueAmount - amountPaid) > 0)
                    {
                        returnList.Add(riskId);
                    }
                }
            }

            return returnList;
        }
    }
}
