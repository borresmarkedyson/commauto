﻿using AutoMapper;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.Application.DTO.Logging;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.Billing.Data.Entities;
using RivTech.CABoxTruck.Billing.Domain.Entities.Logging;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services.Logging
{
    public class Log4NetService : ILog4NetService
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IAppLogRepository _appLogRepository;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IErrorLogRepository _errorLogRepository;
        private readonly IMapper _mapper;

        public Log4NetService(
            IMapper mapper,
            IAuditLogRepository auditLogRepository,
            IAppLogRepository appLogRepository,
            IErrorLogRepository errorLogRepository
        )
        {
            // Load configuration
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            _auditLogRepository = auditLogRepository;
            _appLogRepository = appLogRepository;
            _errorLogRepository = errorLogRepository;
            _mapper = mapper;
        }

        public async Task Info(AuditLogDto auditLog)
        {
            log.Info(JsonConvert.SerializeObject(auditLog));
            var data = _mapper.Map<AuditLog>(auditLog);
            await _auditLogRepository.AddAsync(data);
            await _auditLogRepository.SaveChangesAsync();
        }

        public async Task Error(ErrorLogDto errorLog)
        {
            log.Error(JsonConvert.SerializeObject(errorLog));
            var data = _mapper.Map<ErrorLog>(errorLog);
            await _errorLogRepository.AddAsync(data);
            await _errorLogRepository.SaveChangesAsync();
        }

        public async Task Error(string errorMessage, string jsonMessage, [CallerMemberName] string callerMemberName = "")
        {
            ErrorLogDto errLog = new ErrorLogDto
            {
                UserId = 0,
                ErrorMessage = errorMessage,
                ErrorCode = null,
                JsonMessage = jsonMessage,
                Action = callerMemberName,
                Method = null,
                CreatedDate = DateTime.UtcNow
            };

            log.Error(JsonConvert.SerializeObject(errLog));
            var data = _mapper.Map<ErrorLog>(errLog);

            await _errorLogRepository.AddAsync(data);
            await _errorLogRepository.SaveChangesAsync();
        }

        public async Task Error(Exception exception, [CallerMemberName] string callerMemberName = "")
        {
            int errorCode = GetErrorCode(exception);

            ErrorLogDto errLog = new ErrorLogDto
            {
                UserId = 0,
                ErrorMessage = exception.Message,
                ErrorCode = errorCode != 0 ? errorCode.ToString() : null,
                JsonMessage = exception.ToString(),
                Action = callerMemberName,
                Method = null,
                CreatedDate = DateTime.Now
            };

            log.Error(JsonConvert.SerializeObject(errLog));
            var data = _mapper.Map<ErrorLog>(errLog);

            await _errorLogRepository.AddAsync(data);
            await _errorLogRepository.SaveChangesAsync();
        }

        private int GetErrorCode(Exception ex)
        {
            int errorCode = 0;

            if (!(ex is Win32Exception w32ex)) w32ex = ex.InnerException as Win32Exception;
            if (w32ex != null) errorCode = w32ex.ErrorCode;

            return errorCode;
        }
    }
}
