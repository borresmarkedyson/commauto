﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ReversePaymentService : IReversePaymentService
    {

        private readonly IPaymentRepository _paymentRepository;
        private readonly IValidationService _validationService;
        private readonly ITransactionFeeService _transactionFeeService;
        private readonly IUserDateService _userDateService;
        private readonly IPaymentService _paymentService;
         

        public ReversePaymentService(
            IPaymentRepository paymentRepository,
            IValidationService validationService,
            ITransactionFeeService transactionFeeService,
            IUserDateService userDateService,
            IPaymentService paymentService
        )
        {
            _paymentRepository = paymentRepository;
            _validationService = validationService;
            _transactionFeeService = transactionFeeService;
            _userDateService = userDateService;
            _paymentService = paymentService;
        }

        public async Task<FeeInvoiceDto> ReversePayment(ReversePaymentRequestDto request)
        {
            var validator = new ReversePaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var paymentToVoid = await _paymentRepository.GetDetailsAsync(request.PaymentId);
            if (paymentToVoid == null || paymentToVoid.RiskId != request.RiskId)
            {
                throw new ParameterException(ExceptionMessages.INVALID_REQUEST);
            }

            if (paymentToVoid.VoidDate != null)
            {
                throw new ParameterException(ExceptionMessages.PAYMENT_REVERSAL_ALREADY_APPLIED);
            }

            var totalPayment = await _paymentRepository.GetTotalPaymentAsync(request.RiskId);
            if (totalPayment - paymentToVoid.Amount < 0)
            {
                throw new ParameterException(ExceptionMessages.NEGATIVE_TOTAL_PAYMENT_NOT_ALLOWED);
            }

            Payment paymentReversal = Payment.CreateVoidPayment(_userDateService.GetUser(), paymentToVoid,
                Enumeration.GetEnumerationById<PaymentType>(request.ReversalTypeId), request.Comment);
            await _paymentRepository.AddAsync(paymentReversal);
            await _paymentRepository.SaveChangesAsync();

            await _paymentService.RebalanceOverpayment(request.RiskId, "Void");

            if (request.ReversalTypeId == PaymentType.NSF.Id)
            {
                //TODO: Use service to get NSF Fee per state
                decimal nsfFee = 15;
                var invoiceDto = await _transactionFeeService.AddTransactionFee(
                    new AddTransactionFeeRequestDto
                    {
                        Amount = nsfFee,
                        AmountSubTypeId = AmountSubType.NSFFee.Id,
                        RiskId = request.RiskId,
                        AddDate = _userDateService.GetUser().UserDate,
                        AppId = request.AppId
                    }, true);
                invoiceDto.PaymentId = paymentReversal.Id;
                return invoiceDto;
            }

            return null;
        }
    }
}
