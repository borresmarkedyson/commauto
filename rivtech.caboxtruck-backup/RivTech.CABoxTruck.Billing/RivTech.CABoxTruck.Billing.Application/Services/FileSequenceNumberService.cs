﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class FileSequenceNumberService : IFileSequenceNumberService
    {
        private readonly ISharedEntityRepository _sharedEntityRepository;

        public FileSequenceNumberService(ISharedEntityRepository sharedEntityRepository)
        {
            _sharedEntityRepository = sharedEntityRepository;
        }

        public async Task<string> GenerateFileSequenceNumber(string appId)
        {
            if (appId == RivtechApplication.Centauri.Id)
            {
                SharedEntity centauriFileSequenceNumber = await _sharedEntityRepository.FindAsync(SharedEntity.CentauriFileSequenceNumber.Id);
                SharedEntity centauriFileSequenceNumberGeneratedDate = await _sharedEntityRepository.FindAsync(SharedEntity.CentauriLastFileSequenceNumberGeneration.Id);
                if (centauriFileSequenceNumber == null)
                {
                    centauriFileSequenceNumber = SharedEntity.CentauriFileSequenceNumber;
                    await _sharedEntityRepository.AddAsync(centauriFileSequenceNumber);
                    await _sharedEntityRepository.SaveChangesAsync();
                }

                if (centauriFileSequenceNumberGeneratedDate == null)
                {
                    centauriFileSequenceNumberGeneratedDate = SharedEntity.CentauriLastFileSequenceNumberGeneration;
                    await _sharedEntityRepository.AddAsync(centauriFileSequenceNumberGeneratedDate);
                    await _sharedEntityRepository.SaveChangesAsync();
                }
                string nextSequenceNumber = GetNextSequenceNumber(centauriFileSequenceNumber.Value, centauriFileSequenceNumberGeneratedDate.Value);
                centauriFileSequenceNumber.Value = nextSequenceNumber;
                centauriFileSequenceNumberGeneratedDate.Value = DateTime.Now.ToString();
                await _sharedEntityRepository.SaveChangesAsync();
                return nextSequenceNumber;
            }
            else
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
        }

        private string GetNextSequenceNumber(string currentSequenceNumber, string lastGenerationDateString)
        {
            if (currentSequenceNumber == null || lastGenerationDateString == null)
            {
                return "1";
            }
            else
            {
                int sequenceNumberInt = int.Parse(currentSequenceNumber);
                DateTime lastGenerationDate = DateTime.Parse(lastGenerationDateString);
                if (lastGenerationDate.Date != DateTime.Now.Date)
                {
                    return "1";
                }
                else
                {
                    return (sequenceNumberInt + 1).ToString();
                }
            }
        }
    }
}
