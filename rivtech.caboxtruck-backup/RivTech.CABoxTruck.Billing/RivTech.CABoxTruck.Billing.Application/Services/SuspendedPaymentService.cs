﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedPaymentService : ISuspendedPaymentService
    {
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;
        private readonly ISuspendedPaymentRepository _suspendedPaymentRepository;
        private readonly ISuspendedStatusHistoryService _suspendedStatusHistoryService;
        private readonly IPaymentBreakdownService _paymentBreakdownService;
        private readonly IPaymentService _paymentService;
        private readonly IRiskRepository _riskRepository;
        private readonly ISuspendedReturnDetailService _suspendedReturnDetailService;
        private readonly IUserDateService _userDateService;

        public SuspendedPaymentService(
            IMapper mapper,
            IValidationService validationService,
            ISuspendedPaymentRepository suspendedPaymentRepository,
            ISuspendedStatusHistoryService suspendedStatusHistoryService,
            IPaymentBreakdownService paymentBreakdownService,
            IPaymentService paymentService,
            IRiskRepository riskRepository,
            ISuspendedReturnDetailService suspendedReturnDetailService,
            IUserDateService userDateService
        )
        {
            _mapper = mapper;
            _validationService = validationService;
            _suspendedPaymentRepository = suspendedPaymentRepository;
            _suspendedStatusHistoryService = suspendedStatusHistoryService;
            _paymentBreakdownService = paymentBreakdownService;
            _paymentService = paymentService;
            _riskRepository = riskRepository;
            _suspendedReturnDetailService = suspendedReturnDetailService;
            _userDateService = userDateService;
        }

        public async Task<SuspendedPaymentDto> CreateSuspendedPayment(CreateSuspendedPaymentRequestDto suspendedPaymentRequest)
        {
            var validator = new CreateSuspendedPaymentRequestValidator();
            var validationResult = validator.Validate(suspendedPaymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await CreateSuspendedPaymentData(suspendedPaymentRequest);
        }

        private async Task<SuspendedPaymentDto> CreateSuspendedPaymentData(CreateSuspendedPaymentRequestDto request)
        {
            var suspendedPayment = await _suspendedPaymentRepository.AddAsync(
                new SuspendedPayment(
                    request.ReceiptDate,
                    request.Time,
                    request.PolicyNumber,
                    request.PolicyId,
                    request.Amount,
                    request.Comment,
                    _userDateService.GetUser(),
                    Enumeration.GetEnumerationById<SuspendedSource>(request.SourceId),
                    Enumeration.GetEnumerationById<SuspendedReason>(request.ReasonId),
                    SuspendedStatus.Pending,
                    _mapper.Map<SuspendedDetail>(request.SuspendedDetail),
                    _mapper.Map<SuspendedCheck>(request.SuspendedCheck),
                    _mapper.Map<SuspendedImage>(request.SuspendedImage),
                    _mapper.Map<SuspendedPayer>(request.SuspendedPayer),
                    Enumeration.GetEnumerationById<SuspendedDirectBillCarrier>(request.DirectBillCarrierId)
                ));
            await _suspendedPaymentRepository.SaveChangesAsync();
            await _suspendedStatusHistoryService.CreateSuspendedHistory(suspendedPayment);

            return _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
        }

        public async Task<SuspendedPaymentDto> UpdateSuspendedPayment(UpdateSuspendedPaymentRequestDto request)
        {
            var validationResult = new UpdateSuspendedPaymentRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await UpdateSuspendedPaymentData(request);
        }

        private async Task<SuspendedPaymentDto> UpdateSuspendedPaymentData(UpdateSuspendedPaymentRequestDto request)
        {
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(x => x.Id == request.SuspendedPaymentId);
            if (suspendedPayment == null)
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_ID_INVALID);
            }

            if (suspendedPayment.StatusId != SuspendedStatus.Pending.Id)
            {
                throw new ParameterException(ExceptionMessages.UPDATE_SUSPENDEDPAYMENT_STATUSCONFLICT);
            }

            var suspendedPaymentDto = _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
            suspendedPaymentDto.ReasonId = request.ReasonId;
            _mapper.Map(suspendedPaymentDto, suspendedPayment);
            var updatedSuspendedPayment = _suspendedPaymentRepository.Update(suspendedPayment);
            await _suspendedPaymentRepository.SaveChangesAsync();

            return _mapper.Map<SuspendedPaymentDto>(updatedSuspendedPayment);
        }

        public async Task<SuspendedPaymentDto> PostSuspendedPayment(PostSuspendedPaymentRequestDto request)
        {
            //validate request
            var risk = await _riskRepository.FindAsync(x => x.Id == request.RiskId);
            if (risk == null) throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            request.AppId = risk.ApplicationId;
            var validationResult = new PostSuspendedPaymentRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            //prepare updated suspended payment data
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(p => p.Id == request.SuspendedPaymentId, p => p.SuspendedDetail, p => p.SuspendedPayer);
            var suspendedPaymentDto = _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
            if (suspendedPaymentDto.StatusId != SuspendedStatus.Pending.Id) throw new ParameterException(ExceptionMessages.POST_SUSPENDEDPAYMENT_STATUSCONFLICT);

            //post suspended payment process
            var paymentRequest = await CreatePaymentRequest(suspendedPaymentDto, request);
            var paymentCreated = await _paymentService.SavePayment(paymentRequest, suspendedPaymentDto.SuspendedDetail?.TransactionNum, PaymentType.SuspendedPayment.Id);

            suspendedPaymentDto.StatusId = SuspendedStatus.Posted.Id;
            suspendedPaymentDto.PaymentId = paymentCreated.Id;
            return await UpdateSuspendedPayment(suspendedPaymentDto);
        }

        private async Task<PaymentRequestDto> CreatePaymentRequest(SuspendedPaymentDto suspendedPayment, PostSuspendedPaymentRequestDto request)
        {
            var paymentRequest = new PaymentRequestDto(suspendedPayment, request);
            paymentRequest = await _paymentBreakdownService.ComputeBreakdown(paymentRequest);

            var validationResult = new PaymentRequestValidator().Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);
            return paymentRequest;
        }

        private async Task<SuspendedPaymentDto> UpdateSuspendedPayment(SuspendedPaymentDto updateData)
        {
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(x => x.Id == updateData.Id);
            _mapper.Map(updateData, suspendedPayment);

            var updatedSuspendedPayment = _suspendedPaymentRepository.Update(suspendedPayment);
            await _suspendedPaymentRepository.SaveChangesAsync();
            await _suspendedStatusHistoryService.CreateSuspendedHistory(updatedSuspendedPayment);

            return _mapper.Map<SuspendedPaymentDto>(updatedSuspendedPayment);
        }

        public async Task<SuspendedPaymentDto> VoidSuspendedPayment(VoidSuspendedPaymentRequestDto request)
        {
            //validate request
            var validator = new VoidSuspendedPaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            //prepare updated suspended payment data
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(p => p.Id == request.SuspendedPaymentId);
            var suspendedPaymentDto = _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
            if (suspendedPaymentDto.StatusId != SuspendedStatus.Pending.Id)
            {
                throw new ParameterException(ExceptionMessages.VOID_SUSPENDEDPAYMENT_STATUSCONFLICT);
            }

            suspendedPaymentDto.StatusId = SuspendedStatus.Voided.Id;
            suspendedPaymentDto.Comment = request.Comment;

            //void suspended payment process
            return await UpdateSuspendedPayment(suspendedPaymentDto);
        }

        public async Task<SuspendedPaymentDto> ReturnSuspendedPayment(ReturnSuspendedPaymentRequestDto request)
        {
            //validate request
            var validator = new ReturnSuspendedPaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            //prepare updated suspended payment data
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(p => p.Id == request.SuspendedPaymentId);
            var suspendedPaymentDto = _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
            if (suspendedPaymentDto.StatusId != SuspendedStatus.Pending.Id)
            {
                throw new ParameterException(ExceptionMessages.RETURN_SUSPENDEDPAYMENT_STATUSCONFLICT);
            }

            suspendedPaymentDto.StatusId = SuspendedStatus.Returned.Id;
            suspendedPaymentDto.Comment = request.Comment;

            //void suspended payment process
            await _suspendedReturnDetailService.CreateReturnDetail(request);
            return await UpdateSuspendedPayment(suspendedPaymentDto);
        }

        public async Task<SuspendedPaymentDto> ReverseSuspendedPayment(ReverseSuspendedPaymentRequestDto request)
        {
            //validate request
            var validator = new ReverseSuspendedPaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            //prepare updated suspended payment data
            var suspendedPayment = await _suspendedPaymentRepository.FindAsync(p => p.Id == request.SuspendedPaymentId);
            var suspendedPaymentDto = _mapper.Map<SuspendedPaymentDto>(suspendedPayment);
            if (suspendedPaymentDto.StatusId != SuspendedStatus.Returned.Id)
            {
                throw new ParameterException(ExceptionMessages.REVERSE_SUSPENDEDPAYMENT_STATUSCONFLICT);
            }

            suspendedPaymentDto.StatusId = SuspendedStatus.Pending.Id;
            suspendedPaymentDto.Comment = request.Comment;

            //reverse suspended payment process
            await _suspendedReturnDetailService.DeleteReturnDetail(request.SuspendedPaymentId);
            return await UpdateSuspendedPayment(suspendedPaymentDto);
        }
    }
}
