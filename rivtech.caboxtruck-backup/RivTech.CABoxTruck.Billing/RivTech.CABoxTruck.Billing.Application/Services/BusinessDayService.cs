﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class BusinessDayService : IBusinessDayService
    {
        private readonly IHolidayScheduleService _holidayScheduleService;

        public BusinessDayService(IHolidayScheduleService holidayScheduleService)
        {
            _holidayScheduleService = holidayScheduleService;
        }

        public async Task<bool> CheckIfBusinessDay(DateTime date)
        {
            if (date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday)
            {
                return false;
            }

            var holidayList = await _holidayScheduleService.GetHolidaySchedule();

            var holiday = holidayList.FirstOrDefault(val => val.Day == date.Day && val.Month == date.Month);

            if (holiday != null)
            {
                return false;
            }

            return true;
        }
    }
}
