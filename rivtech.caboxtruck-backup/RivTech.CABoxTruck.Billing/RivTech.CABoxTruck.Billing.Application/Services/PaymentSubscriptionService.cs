﻿using AutoMapper;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentSubscriptionService : IPaymentSubscriptionService
    {
        private readonly IMapper _mapper;
        private readonly IRiskRepository _riskRepository;
        private readonly IProviderPaymentProfileService _providerPaymentProfileService;
        private readonly IPaymentProfileRepository _paymentProfileRepository;
        private readonly IPaymentStrategyFactory _paymentStrategyFactory;
        private readonly IPaymentRequestStrategyFactory _paymentRequestStrategyFactory;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentService _paymentService;
        private readonly IPaymentBreakdownService _paymentBreakdownService;
        private readonly IValidationService _validationService;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ICalendarService _calendarService;
        private readonly IUserDateService _userDateService;
        private readonly ILog4NetService _logger;

        public PaymentSubscriptionService(
            IMapper mapper,
            IProviderPaymentProfileService providerPaymentProfileService,
            IRiskRepository riskRepository,
            IPaymentProfileRepository paymentProfileRepository,
            IPaymentStrategyFactory paymentStrategyFactory,
            IPaymentRequestStrategyFactory paymentRequestStrategyFactory,
            IInvoiceRepository invoiceRepository,
            IPaymentService paymentService,
            IPaymentBreakdownService paymentBreakdownService,
            IValidationService validationService,
            IPaymentRepository paymentRepository,
            ICalendarService calendarService,
            IUserDateService userDateService,
            ILog4NetService logger
        )
        {
            _mapper = mapper;
            _riskRepository = riskRepository;
            _providerPaymentProfileService = providerPaymentProfileService;
            _paymentProfileRepository = paymentProfileRepository;
            _paymentStrategyFactory = paymentStrategyFactory;
            _paymentRequestStrategyFactory = paymentRequestStrategyFactory;
            _invoiceRepository = invoiceRepository;
            _paymentService = paymentService;
            _paymentBreakdownService = paymentBreakdownService;
            _validationService = validationService;
            _paymentRepository = paymentRepository;
            _calendarService = calendarService;
            _userDateService = userDateService;
            _logger = logger;
        }

        public async Task<RecurringPaymentJobViewDto> PostPaymentAllRisk(PostPaymentAllEnrolledAccountRequest request)
        {
            //validate request
            var validationResult = new PostPaymentAllEnrolledAccountRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var payments = new List<RecurringPaymentViewDto>();
            var error = new List<FailedRiskViewDto>();
            foreach (var risk in _riskRepository.GetAsync(x => x.ApplicationId == request.ApplicationId).Result.ToList())
            {
                try
                {
                    var payment = await PostPaymentSingleRisk(risk.Id);
                    if (payment != null)
                    {
                        payments.Add(payment);
                    }
                }
                catch (Exception e)
                {
                    error.Add(new FailedRiskViewDto()
                    {
                        RiskId = risk.Id.ToString(),
                        PolicyNumber = risk.PolicyNumber,
                        ErrorMessage = $"{e.Message}"
                    });
                    await _logger.Error($"{risk.Id} {risk.PolicyNumber}: {e.Message}", e.ToString());
                }
            };

            return new RecurringPaymentJobViewDto()
            {
               RecurringPaymentView = payments,
               FailedRisk = error
            };
        }

        public async Task<RecurringPaymentViewDto> PostPaymentSingleRisk(Guid riskId)
        {
            var risk = await _riskRepository.FindAsync(riskId);
            var invoiceDueDate = _userDateService.GetUser().UserDate.Date;

            //RULE: is enrolled to auto pay
            var profile = _paymentProfileRepository.GetProfileWithDefaultAccount(risk.Id);
            if (profile == null || !profile.IsRecurringPayment || !profile.PaymentAccounts.Any())
            {
                return null;
            }

            //RULE: has due invoice
            var dueBalance = await GetDueBalanceAmount(risk.Id, _calendarService.AddBussinessDay(invoiceDueDate, UtilConstant.AUTOPAY_DAYSBEFOREDUEDATE));
            if (dueBalance <= 0)
            {
                return null;
            }

            var payment = await CreatePayment(risk, dueBalance);
            var paymentView = _mapper.Map<RecurringPaymentViewDto>(payment);
            paymentView.SetDetail(risk.PaymentPlanId, profile.PaymentAccounts[0]);
            return paymentView;
        }

        private async Task<PaymentDto> CreatePayment(Risk risk, decimal dueBalance)
        {
            var paymentRequest = await CreatePaymentRequest(risk.Id, dueBalance, risk.ApplicationId);
            var referenceNumber = await SendToProviderChargeAccountRequest(risk.Id, dueBalance);
            return await _paymentService.SavePayment(paymentRequest, referenceNumber);
        }

        private async Task<decimal> GetDueBalanceAmount(Guid riskId, DateTime dueDate)
        {
            var totalPaid = await _paymentRepository.GetTotalPaymentAsync(riskId);
            var totalInvoiceDue = await _invoiceRepository.GetTotalDueInvoicedAmountAsync(riskId, dueDate);

            return totalInvoiceDue - totalPaid;
        }

        private async Task<PaymentRequestDto> CreatePaymentRequest(Guid riskId, decimal dueBalance, string appId)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            var paymentRequest = new PaymentRequestDto(riskId, dueBalance, currentDate);
            paymentRequest.PaymentSummary.InstrumentId = InstrumentType.Cash.Id; //set to cash to pass validation 
            paymentRequest.AppId = appId;
            paymentRequest = await _paymentBreakdownService.ComputeBreakdown(paymentRequest);

            var validator = new PaymentRequestValidator();
            var validationResult = validator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var profile = _paymentProfileRepository.GetProfileWithDefaultAccount(riskId);
            paymentRequest.PaymentSummary.InstrumentId = profile.PaymentAccounts[0].InstrumentTypeId; //set back correct instrument
            paymentRequest.BillingDetail = new BillingDetailDto();
            paymentRequest.BillingDetail.BillingAddress = _mapper.Map<BillingAddressDto>(profile.PaymentAccounts[0].Payer);
            paymentRequest.PaymentSummary.Email = paymentRequest.BillingDetail.BillingAddress.Email; 
            return paymentRequest;
        }

        public async Task<string> SendToProviderChargeAccountRequest(Guid riskId, decimal amount)
        {
            var risk = await _riskRepository.FindAsync(riskId);
            var profile = _paymentProfileRepository.GetProfileWithDefaultAccount(riskId);
            var providerProfile = await _providerPaymentProfileService.GetProfileWithDefaultAccount(riskId, profile.PaymentAccounts[0].Id, risk.ApplicationId);

            var paymentStrategy = _paymentStrategyFactory.GetStrategy(risk.ApplicationId);
            var paymentRequestStrategy = _paymentRequestStrategyFactory.GetRequestStrategy(risk.ApplicationId);

            var providerProfileReq = paymentRequestStrategy.GeneratePostPaymentProfileRequest(providerProfile, amount);
            return paymentStrategy.PostPaymentProfile(providerProfileReq);
        }
    }
}
