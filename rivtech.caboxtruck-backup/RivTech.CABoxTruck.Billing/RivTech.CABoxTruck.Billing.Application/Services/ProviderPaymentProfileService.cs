﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    //class for managing payment provider(authorize.net, pnc, etc) profile response
    public class ProviderPaymentProfileService : IProviderPaymentProfileService
    {
        private readonly IMapper _mapper;
        private readonly IProfileStrategyFactory _profileStrategyFactory;
        private readonly IProfileRequestStrategyFactory _profileRequestStrategyFactory;
        private readonly IAuthNetProfileService _authNetProfileService;
        private readonly IAuthNetProfileRepository _authNetProfileRepository;

        public ProviderPaymentProfileService(
            IMapper mapper,
            IProfileStrategyFactory profileStrategyFactory,
            IProfileRequestStrategyFactory profileRequestStrategyFactory,
            IAuthNetProfileService authNetProfileService,
            IAuthNetProfileRepository authNetProfileRepository
        )
        {
            _mapper = mapper;
            _profileStrategyFactory = profileStrategyFactory;
            _profileRequestStrategyFactory = profileRequestStrategyFactory;
            _authNetProfileService = authNetProfileService;
            _authNetProfileRepository = authNetProfileRepository;
        }

        public dynamic SendToProviderCreateRequest(PaymentProfileRequestDto profileRequest)
        {
            var profileStrategy = _profileStrategyFactory.GetStrategy(profileRequest.AppId);
            var profileRequestStrategy = _profileRequestStrategyFactory.GetRequestStrategy(profileRequest.AppId);

            var providerProfileReq = profileRequestStrategy.GenerateCreateRequest(profileRequest, _mapper);
            return profileStrategy.CreateProfile(providerProfileReq);
        }

        //always return payment provider profile DTO
        public async Task<dynamic> SaveProfile(PaymentProfileRequestDto profileRequest, PaymentAccountDto paymentAccount, dynamic providerResponse)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(profileRequest.AppId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return await _authNetProfileService.SaveAuthNetProfile(profileRequest.RiskId, paymentAccount.Id, (createCustomerProfileResponse)providerResponse);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }

        //always return payment provider profile DTO
        public async Task<dynamic> GetProfile(Guid riskId, string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    var authNetProfile = await _authNetProfileRepository.FindAsync(p => p.RiskId == riskId, p => p.AuthNetAccount);
                    return _mapper.Map<AuthNetProfileDto>(authNetProfile);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }

        //always return payment provider profile DTO
        public async Task<dynamic> GetProfileWithDefaultAccount(Guid riskId, Guid defaultpaymentAccountId, string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    var authNetProfile = await _authNetProfileRepository.FindAsync(x => x.RiskId == riskId,
                        p => p.AuthNetAccount.Where(x => x.PaymentAccountId == defaultpaymentAccountId));
                    return _mapper.Map<AuthNetProfileDto>(authNetProfile);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }

        public async Task<dynamic> SendToProviderUpdateRequest(PaymentProfileRequestDto profileRequest)
        {
            var profileStrategy = _profileStrategyFactory.GetStrategy(profileRequest.AppId);
            var profileRequestStrategy = _profileRequestStrategyFactory.GetRequestStrategy(profileRequest.AppId);

            var providerProfile = await GetProfile(profileRequest.RiskId, profileRequest.AppId);
            if (providerProfile == null)
            {
                return null;
            }

            var providerProfileReq = profileRequestStrategy.GenerateUpdateRequest(profileRequest, providerProfile, _mapper);
            return profileStrategy.UpdateProfile(providerProfileReq);
        }

        public async Task<dynamic> SendToProviderDeleteRequest(Guid riskId, string appId)
        {
            var providerProfile = await GetProfile(riskId, appId);
            if (providerProfile == null)
            {
                return null;
            }

            var profileStrategy = _profileStrategyFactory.GetStrategy(appId);
            var profileRequestStrategy = _profileRequestStrategyFactory.GetRequestStrategy(appId);

            var providerProfileReq = profileRequestStrategy.GenerateDeleteRequest(providerProfile);
            profileStrategy.DeleteProfile(providerProfileReq);

            return await DeleteProviderProfileData(riskId, appId);
        }

        private async Task<dynamic> DeleteProviderProfileData(Guid riskId, string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return await _authNetProfileService.DeleteAuthNetProfile(riskId);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }
    }
}
