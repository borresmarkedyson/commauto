﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class CancelledRiskPaymentService : ICancelledRiskPaymentService
    {
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;
        private readonly IPaymentService _paymentService;
        private readonly ISuspendedPaymentService _suspendedPaymentService;
        private readonly IRiskRepository _riskRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IUserDateService _userDateService;


        public CancelledRiskPaymentService(
            IMapper mapper,
            IValidationService validationService,
            IPaymentService paymentService,
            ISuspendedPaymentService suspendedPaymentService,
            IRiskRepository riskRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IUserDateService userDateService
        )
        {
            _mapper = mapper;
            _validationService = validationService;
            _paymentService = paymentService;
            _suspendedPaymentService = suspendedPaymentService;
            _riskRepository = riskRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _userDateService = userDateService;
        }

        public async Task<SuspendedPaymentDto> PayToReinstate(PaymentRequestDto paymentRequest)
        {
            //validate request
            var validationResult = new PaymentToReinstateRequestValidator().Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(paymentRequest.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }
            //RULE: paymentAmount >= required amount to reinstate(totalPastDueAmount)
            var totalPastDueAmount = await _invoiceRepository.GetTotalPastDueAmount(risk.Id, _userDateService.GetUser().UserDate.Date) -
                                     await _paymentRepository.GetTotalPaymentAsync(risk.Id);
            if (paymentRequest.PaymentSummary.Amount < totalPastDueAmount)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_CANCELLEDRISKPAYMENT_LESSTHANTOTALDUE);
            }

            //create payment
            var providerResponse = await _paymentService.PostPaymentNoSaving(paymentRequest);
            var suspendedPaymentRequest = _mapper.Map<CreateSuspendedPaymentRequestDto>(paymentRequest);
            suspendedPaymentRequest.PolicyNumber = _riskRepository.FindAsync(paymentRequest.RiskId).Result.PolicyNumber;
            suspendedPaymentRequest.Comment = providerResponse?.ReferenceId;

            //create data on suspense
            return await _suspendedPaymentService.CreateSuspendedPayment(suspendedPaymentRequest);
        }

        public async Task<PostPaymentViewDto> PayBalance(PaymentRequestDto paymentRequest)
        {
            //validate request
            var risk = await _riskRepository.FindAsync(paymentRequest.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }
            //RULE: paymentAmount <= balanceAmount
            var totalBalanceAmount = await _invoiceRepository.GetTotalInvoicedAmountAsync(risk.Id) -
                                     await _paymentRepository.GetTotalPaymentAsync(risk.Id);
            if (paymentRequest.PaymentSummary.Amount > totalBalanceAmount)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_CANCELLEDRISKPAYMENT_GREATERTHANBALANCE);
            }

            var providerResponse = await _paymentService.PostPaymentNoSaving(paymentRequest);
            var payment = await _paymentService.SavePayment(paymentRequest, providerResponse?.ReferenceId);
            return new PostPaymentViewDto(payment, providerResponse, risk);
        }
    }
}
