﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentProfileService : IPaymentProfileService
    {
        private readonly IMapper _mapper;
        private readonly IPaymentProfileRepository _paymentProfileRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IProviderPaymentProfileService _providerPaymentProfileService;
        private readonly IValidationService _validationService;
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IUserDateService _userDateService;
        private readonly ICustomerRepository _customerRepository;

        public PaymentProfileService(
            IMapper mapper,
            IPaymentProfileRepository paymentProfileRepository,
            IRiskRepository riskRepository,
            IProviderPaymentProfileService providerPaymentProfileService,
            IValidationService validationService,
            IPaymentAccountService paymentAccountService,
            IApplicationUserRepository applicationUserRepository,
            IUserDateService userDateService,
            ICustomerRepository customerRepository
        )
        {
            _mapper = mapper;
            _paymentProfileRepository = paymentProfileRepository;
            _riskRepository = riskRepository;
            _providerPaymentProfileService = providerPaymentProfileService;
            _validationService = validationService;
            _paymentAccountService = paymentAccountService;
            _applicationUserRepository = applicationUserRepository;
            _userDateService = userDateService;
            _customerRepository = customerRepository;
        }

        private async Task<PaymentProfileDto> CreateProfile(PaymentProfileRequestDto profileRequest)
        {
            var risk = await _riskRepository.FindAsync(x => x.Id == profileRequest.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            if (risk.PaymentPlanId == PaymentPlan.FullPay.Id || risk.PaymentPlanId == PaymentPlan.PremiumFinanced.Id)
            {
                throw new ParameterException(ExceptionMessages.RECURRINGPAYMENT_PAYMENTPLANCONFLICT);
            }

            profileRequest.AppId = risk.ApplicationId;
            profileRequest.PaymentAccount.AppId = risk.ApplicationId;
            profileRequest.PaymentAccount.BillingDetail.InsuredAgree = true;
            profileRequest.PaymentAccount.BillingDetail.UwAgree = true;

            //validate request
            var validationResult = new CreatePaymentProfileRequestValidator().Validate(profileRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);
            if (!profileRequest.PaymentAccount.IsDefault)
            {
                throw new ParameterException(ExceptionMessages.PAYMENTACCOUNTDEFAULT_REQUIRED);
            }

            //create provider profile + payment profile
            var response = _providerPaymentProfileService.SendToProviderCreateRequest(profileRequest);
            var paymentProfile = await CreateProfileData(profileRequest);
            var providerProfile = await _providerPaymentProfileService.SaveProfile(profileRequest, paymentProfile.PaymentAccounts.FirstOrDefault(), response);
            paymentProfile.PaymentAccounts[0] = await _paymentAccountService.SetAccountDescription(paymentProfile.PaymentAccounts.FirstOrDefault(), providerProfile, profileRequest.PaymentAccount);
            return paymentProfile;
        }

        public async Task<PaymentProfileDto> UpdateProfile(PaymentProfileRequestDto profileRequest)
        {
            //validate request
            var risk = await _riskRepository.FindAsync(x => x.Id == profileRequest.RiskId);
            profileRequest.AppId = risk.ApplicationId;
            var validator = new UpdatePaymentProfileRequestValidator();
            var validationResult = validator.Validate(profileRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            await _providerPaymentProfileService.SendToProviderUpdateRequest(profileRequest);
            return await UpdateProfileData(profileRequest);
        }

        public async Task<PaymentProfileDto> DeleteProfile(Guid riskId)
        {
            var risk = await _riskRepository.FindAsync(x => x.Id == riskId);
            var profile = await _paymentProfileRepository.FindAsync(x => x.RiskId == riskId);
            if (profile == null)
            {
                return null;
            }

            await _providerPaymentProfileService.SendToProviderDeleteRequest(risk.Id, risk.ApplicationId);
            return await DeleteProfileData(riskId);
        }

        public async Task<PaymentProfileDto> GetProfile(Guid riskId)
        {
            var profile = await _paymentProfileRepository.FindAsync(x => x.RiskId == riskId, x => x.PaymentAccounts);
            return _mapper.Map<PaymentProfileDto>(profile);
        }

        private async Task<PaymentProfileDto> CreateProfileData(PaymentProfileRequestDto profileRequest)
        {
            var paymentProfile = await _paymentProfileRepository.FindAsync(x => x.RiskId == profileRequest.RiskId);
            if (paymentProfile != null)
            {
                throw new ParameterException(ExceptionMessages.PAYMENTPROFILE_ALREADYEXIST);
            }
            var paymentAccountDto = _mapper.Map<PaymentAccountDto>(profileRequest.PaymentAccount);
            paymentAccountDto.Description = profileRequest.PaymentAccount.InstrumentTypeId;
            paymentAccountDto.CreatedDate = _userDateService.GetUser().UserDate;
            paymentAccountDto.UpdatedDate = paymentAccountDto.CreatedDate;

            paymentProfile = await _paymentProfileRepository.AddAsync(
                new PaymentProfile(
                    profileRequest.RiskId,
                    profileRequest.FirstName,
                    profileRequest.LastName,
                    profileRequest.PhoneNumber,
                    profileRequest.Email,
                    profileRequest.IsRecurringPayment,
                    _userDateService.GetUser(),
                    _mapper.Map<PaymentAccount>(paymentAccountDto)
                ));
            await _paymentProfileRepository.SaveChangesAsync();
            return _mapper.Map<PaymentProfileDto>(paymentProfile);
        }

        private async Task<PaymentProfileDto> UpdateProfileData(PaymentProfileRequestDto profileRequest)
        {
            var profile = await _paymentProfileRepository.FindAsync(x => x.RiskId == profileRequest.RiskId);
            _mapper.Map(profileRequest, profile);

            var updatedProfile = _paymentProfileRepository.Update(profile);
            await _paymentProfileRepository.SaveChangesAsync();
            return _mapper.Map<PaymentProfileDto>(updatedProfile);
        }

        private async Task<PaymentProfileDto> DeleteProfileData(Guid riskId)
        {
            var profile = _paymentProfileRepository.GetProfile(riskId);

            var deleteProfile = _paymentProfileRepository.Remove(profile);
            await _paymentProfileRepository.SaveChangesAsync();

            _customerRepository.RemoveRange(profile.PaymentAccounts.Select(x => x.Payer));
            await _customerRepository.SaveChangesAsync();

            return _mapper.Map<PaymentProfileDto>(deleteProfile);
        }

        public async Task<PaymentProfileDto> CreateProfileOnBind(Guid riskId, BindRequestDto bindRequest)
        {
            if (bindRequest.InstrumentId != InstrumentType.RecurringCreditCard.Id &&
                bindRequest.InstrumentId != InstrumentType.RecurringECheck.Id)
            {
                throw new ParameterException(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            }

            //create profile request
            var profileRequest = _mapper.Map<PaymentProfileRequestDto>(bindRequest);
            profileRequest.RiskId = riskId;
            profileRequest.PaymentAccount = _mapper.Map<PaymentAccountRequestDto>(bindRequest);
            profileRequest.PaymentAccount.RiskId = riskId;

            //create profile
            return await CreateProfile(profileRequest);
        }

        public async Task<PaymentProfileDto> CreateProfileOnPayment(PaymentRequestDto paymentRequest)
        {
            if (paymentRequest.PaymentSummary.InstrumentId != InstrumentType.RecurringCreditCard.Id &&
                paymentRequest.PaymentSummary.InstrumentId != InstrumentType.RecurringECheck.Id)
            {
                throw new ParameterException(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            }

            //create profile request
            var profileRequest = _mapper.Map<PaymentProfileRequestDto>(paymentRequest);
            profileRequest.PaymentAccount = _mapper.Map<PaymentAccountRequestDto>(paymentRequest);

            //create profile
            return await CreateProfile(profileRequest);
        }

        public async Task<PaymentProfileDto> CreateProfileOnEnroll(PaymentProfileRequestDto profileRequest)
        {
            if (profileRequest.PaymentAccount == null)
            {
                throw new ParameterException(ExceptionMessages.PAYMENTACCOUNT_REQUIRED);
            }

            profileRequest.PaymentAccount.RiskId = profileRequest.RiskId;

            //create profile
            return await CreateProfile(profileRequest);
        }

        public PaymentProfile GetEnrolledPaymentProfile(Guid riskId)
        {
            var profile = _paymentProfileRepository.GetProfileWithDefaultAccount(riskId);
            if (profile == null || !profile.IsRecurringPayment || !profile.PaymentAccounts.Any())
            {
                return null;
            }

            return profile;
        }
    }
}
