﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentViewService : IPaymentViewService
    {
        private readonly IMapper _mapper;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ISuspendedPaymentRepository _suspendedPaymentRepository;
        private readonly ISuspendedPaymentViewService _suspendedPaymentViewService;

        public PaymentViewService(
            IMapper mapper,
            IPaymentRepository paymentRepository,
            ISuspendedPaymentRepository suspendedPaymentRepository,
            ISuspendedPaymentViewService suspendedPaymentViewService
        )
        {
            _mapper = mapper;
            _paymentRepository = paymentRepository;
            _suspendedPaymentRepository = suspendedPaymentRepository;
            _suspendedPaymentViewService = suspendedPaymentViewService;
        }

        public async Task<List<PaymentViewDto>> GetPaymentView(Guid riskId)
        {
            var payments = await _paymentRepository.GetAllByRiskId(riskId);
            var paymentViews = new List<PaymentViewDto>();

            payments.ToList().ForEach(payment =>
            {
                if (payment.PaymentTypeId == PaymentType.Regular.Id ||
                    payment.PaymentTypeId == PaymentType.Refund.Id ||
                    payment.PaymentTypeId == PaymentType.SuspendedPayment.Id ||
                    (payment.PaymentTypeId == PaymentType.Transfer.Id && (payment.VoidOfPaymentId == null || payment.VoidDate == null)))
                {
                    var paymentView = _mapper.Map<PaymentViewDto>(payment);

                    paymentView.Method = payment.InstrumentType.Description;

                    if (payment.InstrumentId == InstrumentType.Adjustment.Id ||
                        payment.InstrumentId == InstrumentType.WriteOff.Id)
                    {
                        paymentView.PaymentType = payment.InstrumentType.Description;
                    }
                    else if (payment.PaymentTypeId == PaymentType.Refund.Id)
                    {
                        paymentView.PaymentType = PaymentType.Refund.Description;
                        paymentView.Amount *= -1;
                    }
                    else
                    {
                        paymentView.PaymentType = "Payment";
                    }

                    if (payment.PaymentTypeId == PaymentType.Transfer.Id)
                    {
                        paymentView.PaymentType = payment.InstrumentType.Description;
                        paymentView.Method = payment.PaymentType.Description;
                    }

                    paymentViews.Add(paymentView);
                }
            });

            payments.ToList().ForEach(payment =>
            {
                if (payment.PaymentTypeId != PaymentType.Regular.Id && payment.VoidDate != null)
                {
                    var voidedPayment = paymentViews.FirstOrDefault(x => x.Id == payment.VoidOfPaymentId);
                    if (voidedPayment != null)
                    {
                        voidedPayment.ReversalDate = $"{payment.VoidDate?.ToString("MM/dd/yyyy")} ({payment.PaymentType.Description})";
                        voidedPayment.Comment = payment.Comment;
                    }
                }
            });

            return paymentViews;
        }

        public async Task<PaymentDetailsViewDto> GetPaymentDetailsView(Guid riskId, Guid paymentId)
        {
            var paymentFromDb = await _paymentRepository.GetDetailsAsync(paymentId);
            if (paymentFromDb == null || paymentFromDb.RiskId != riskId)
            {
                throw new ParameterException(ExceptionMessages.INVALID_REQUEST);
            }

            var voidPayment = await _paymentRepository.GetVoidPayment(paymentId);

            decimal premium = 0;
            decimal Fee = 0;
            decimal tax = 0;
            decimal commission = 0;

            paymentFromDb.PaymentDetails.ForEach(paymentDetail =>
            {
                var amountTypeId = paymentDetail.AmountTypeId;
                switch (amountTypeId)
                {
                    case var _ when amountTypeId == AmountType.Premium.Id:
                        premium += paymentDetail.Amount;
                        break;
                    case var _ when amountTypeId == AmountType.PremiumFee.Id:
                        Fee += paymentDetail.Amount;
                        break;
                    case var _ when amountTypeId == AmountType.TransactionFee.Id:
                        Fee += paymentDetail.Amount;
                        break;
                    case var _ when amountTypeId == AmountType.Tax.Id:
                        tax += paymentDetail.Amount;
                        break;
                    case var _ when amountTypeId == AmountType.TransactionTax.Id:
                        tax += paymentDetail.Amount;
                        break;
                    case var _ when amountTypeId == AmountType.Commission.Id:
                        commission += paymentDetail.Amount;
                        break;
                }
            });

            var paymentDetails = new PaymentDetailsViewDto
            {
                PaymentId = paymentFromDb.Id,
                PostDate = paymentFromDb.EffectiveDate,
                PostedBy = paymentFromDb.CreatedBy?.Username,
                Amount = paymentFromDb.PaymentTypeId == PaymentType.Refund.Id ? -paymentFromDb.Amount : paymentFromDb.Amount,
                Premium = paymentFromDb.PaymentTypeId == PaymentType.Refund.Id ? -(premium + commission) : (premium + commission),
                Fee = paymentFromDb.PaymentTypeId == PaymentType.Refund.Id ? -(Fee) : (Fee),
                Tax = paymentFromDb.PaymentTypeId == PaymentType.Refund.Id ? -tax : tax,
                PaymentMethod = paymentFromDb.InstrumentType.Description,
                Reference = paymentFromDb.Reference,
                ReversalType = voidPayment?.PaymentType != null ? voidPayment.PaymentType.Description : null,
                ReversalDate = voidPayment != null ? voidPayment.VoidDate : null,
                ReversalProcessedBy = voidPayment != null ? voidPayment.VoidedBy?.Username : null,
                Comments = voidPayment != null ? voidPayment.Comment : paymentFromDb.Comment,
                ClearDate = paymentFromDb.ClearDate,
                EscheatDate = paymentFromDb.EscheatDate
            };

            if (paymentFromDb.PaymentTypeId == PaymentType.SuspendedPayment.Id) {
                var suspendedPayment = await _suspendedPaymentRepository.FindAsync(x => x.PaymentId == paymentFromDb.Id);
                paymentDetails.SuspendedPaymentDetail = await _suspendedPaymentViewService.GetDetailView(suspendedPayment.Id);
            }

            return paymentDetails;
        }
    }
}
