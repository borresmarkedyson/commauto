﻿using Azure.Storage.Blobs;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.CheckTransmittal;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class RefundRequestTransmittalService : IRefundRequestTransmittalService
    {
        private readonly List<FailedRiskViewDto> failedRisks = new List<FailedRiskViewDto>();
        private readonly IFileEncryptionService _fileEncryptionService;
        private readonly IRefundRequestRepository _refundRequestRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ITransactionDetailRepository _transactionDetailRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IRefundService _refundService;
        private readonly IFileSequenceNumberService _fileSequenceNumberService;
        private readonly IValidationService _validationService;
        private readonly ICheckNumberService _checkNumberService;

        public RefundRequestTransmittalService(
            IFileEncryptionService fileEncryptionService,
            IRefundRequestRepository refundRequestRepository,
            IPaymentRepository paymentRepository,
            ITransactionDetailRepository transactionDetailRepository,
            IRiskRepository riskRepository,
            IRefundService refundService,
            IFileSequenceNumberService fileSequenceNumberService,
            IValidationService validationService,
            ICheckNumberService checkNumberService
        )
        {
            _fileEncryptionService = fileEncryptionService;
            _refundRequestRepository = refundRequestRepository;
            _paymentRepository = paymentRepository;
            _transactionDetailRepository = transactionDetailRepository;
            _riskRepository = riskRepository;
            _refundService = refundService;
            _fileSequenceNumberService = fileSequenceNumberService;
            _validationService = validationService;
            _checkNumberService = checkNumberService;
        }
        public async Task<PostRefundResponseDto> SendRefundRequestViaFtp(PostRefundRequestDto request)
        {
            var validator = new PostRefundRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                DateTime generationDateTime = DateTime.UtcNow;
                var refundRequestSettings = Service.AppSettings.RefundRequest;
                string fileSequenceNumber = await _fileSequenceNumberService.GenerateFileSequenceNumber(request.AppId);
                string fileName = @$"{ refundRequestSettings.FTP.FileName}_{ generationDateTime.ToString("yyyMMdd_HHmmssfff")}_{ fileSequenceNumber}";

                XmlWriterSettings xws = new XmlWriterSettings();
                xws.Encoding = Encoding.GetEncoding("ISO-8859-1");
                xws.NewLineChars = Environment.NewLine;
                xws.ConformanceLevel = ConformanceLevel.Document;
                xws.Indent = true;

                List<DetailRecordWithRisk> validRefunds = await GetValidRefundRequests(request.AppId, generationDateTime);
                List<DetailRecordWithRisk> successfulRefunds = await PostRefundsToSystem(validRefunds);

                if (successfulRefunds.Count > 0)
                {
                    using (XmlWriter xw = XmlWriter.Create(memoryStream, xws))
                    {
                        PolicyChecks policyChecks = GeneratePolicyChecks(successfulRefunds, fileSequenceNumber, generationDateTime);
                        XmlDocument doc = GenerateRefundRequestXml(policyChecks, fileName);
                        doc.WriteTo(new XmlWriterFullEndElement(xw));
                    }

                    memoryStream.Position = 0;
                    BlobClient blobClient = new BlobClient(
                        connectionString: Service.AppSettings.AzureStorages.CentauriNew.ConnectionString,
                        blobContainerName: Service.AppSettings.RefundRequest.AzureStorageDirectory,
                        blobName: @$"{fileName}.xml");
                    await blobClient.UploadAsync(memoryStream);

                    memoryStream.Position = 0;
                    _fileEncryptionService.ImportPublicKeyFromString(request.EncryptionPublicKey);

                    if (!Directory.Exists(refundRequestSettings.FilePath))
                    {
                        Directory.CreateDirectory(refundRequestSettings.FilePath);
                    }

                    string localEncryptedFile = $@"{refundRequestSettings.FilePath}{refundRequestSettings.EncryptedFileName}.bin";

                    _fileEncryptionService.EncryptStream(memoryStream, localEncryptedFile);

                    using (var client = new WebClient())
                    {
                        client.Credentials = new NetworkCredential(refundRequestSettings.FTP.Username, refundRequestSettings.FTP.Password);
                        client.UploadFile(
                            @$"ftp://{refundRequestSettings.FTP.Host}{refundRequestSettings.FTP.FilePath}{fileName}.bin",
                            WebRequestMethods.Ftp.UploadFile,
                            localEncryptedFile);
                    }

                    File.Delete(localEncryptedFile);
                }
                return new PostRefundResponseDto
                {
                    FailedRisk = failedRisks
                };
            }
        }

        private XmlDocument GenerateRefundRequestXml(PolicyChecks policyChecks, string fileName)
        {
            policyChecks.FileName = fileName;

            XmlSerializerNamespaces nameSpace = new XmlSerializerNamespaces();
            nameSpace.Add("au", "http://www.applieduw.com/policy_checks");
            nameSpace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            XmlSerializer serializer = new XmlSerializer(typeof(PolicyChecks));
            XmlDocument xmlDocument;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, policyChecks, nameSpace);

                memoryStream.Position = 0;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreWhitespace = true;

                using (var xtr = XmlReader.Create(memoryStream, settings))
                {
                    xmlDocument = new XmlDocument();
                    xmlDocument.Load(xtr);
                }
            }

            return xmlDocument;
        }

        private async Task<List<DetailRecordWithRisk>> GetValidRefundRequests(string appId, DateTime generationDateTime)
        {
            List<DetailRecordWithRisk> detailRecordsWithRisk = new List<DetailRecordWithRisk>();
            var refundRequestsFromDb = await _refundRequestRepository.GetAsync(x => x.CheckDate == null, x => x.RefundPayee);

            GroupHeaderRecord nationalInsuranceGroupHeaderRecord = new GroupHeaderRecord
            {
                IssuingCarrierName = "Centauri National Insurance Company",
                CheckType = "Policy Refund",
                DeliveryType = "Normal",
                BankId = 1,
                DetailRecords = new List<DetailRecord>()
            };

            GroupHeaderRecord specialtyInsuranceGroupHeaderRecord = new GroupHeaderRecord
            {
                IssuingCarrierName = "Centauri Specialty Insurance Company",
                CheckType = "Policy Refund",
                DeliveryType = "Normal",
                BankId = 1,
                DetailRecords = new List<DetailRecord>()
            };

            foreach (var refundRequest in refundRequestsFromDb)
            {
                var totalPaidAmount = await _paymentRepository.GetTotalPaymentAsync(refundRequest.RiskId);
                var totalTransactionAmount = await _transactionDetailRepository.GetTotalTransactionAmount(refundRequest.RiskId);
                if (totalPaidAmount <= totalTransactionAmount || refundRequest.Amount <= 0
                    || totalPaidAmount <= 0 || refundRequest.Amount > totalPaidAmount)
                {
                    continue;
                }

                var riskFromDb = await _riskRepository.GetDetailsAsync(refundRequest.RiskId);

                var detailRecord = new DetailRecord
                {
                    CheckType = "Policy Refund",
                    DeliveryType = "Normal",
                    BankId = 1,
                    CheckNo = await _checkNumberService.GenerateCheckNumber(appId),
                    CheckDate = generationDateTime.ToString("yyyy-MM-dd"),
                    PolicyNumber = riskFromDb.PolicyNumber,
                    PolicyEffectiveDate = riskFromDb.EffectiveDate.ToString("yyyy-MM-dd"),
                    InsuredName = refundRequest.RefundPayee?.Name,
                    InsuredStreet1 = refundRequest.RefundPayee?.Street1,
                    InsuredStreet2 = string.Empty,
                    InsuredCity = refundRequest.RefundPayee?.City,
                    InsuredState = refundRequest.RefundPayee?.State,
                    InsuredZip = refundRequest.RefundPayee?.Zip,
                    RefundReason = "Overpayment of premium.",
                    RefundReasonIsBlankFlag = 0,
                    RefundDate = generationDateTime.ToString("yyyy-MM-dd"),
                    TotalPremium = totalTransactionAmount,
                    TotalPaid = totalPaidAmount,
                    RefundAmount = refundRequest.Amount,
                    RefundAmountIsZeroFlag = refundRequest.Amount == 0 ? 1 : 0,
                    CheckMessage = string.Empty,
                    CheckMessageIsBlankFlag = 1
                };

                //TODO: Set group header issuing carrier name based on state

                detailRecord.IssuingCarrierName = specialtyInsuranceGroupHeaderRecord.IssuingCarrierName;
                specialtyInsuranceGroupHeaderRecord.DetailRecords.Add(detailRecord);

                detailRecordsWithRisk.Add(new DetailRecordWithRisk
                {
                    RefundRequestId = refundRequest.Id,
                    RiskId = refundRequest.RiskId,
                    DetailRecord = detailRecord
                });
            }

            return detailRecordsWithRisk;
        }

        private PolicyChecks GeneratePolicyChecks(List<DetailRecordWithRisk> successfulRefunds, string fileSequenceNumber, DateTime generationDateTime)
        {
            PolicyChecks policyChecks = new PolicyChecks
            {
                FileDate = generationDateTime.ToString("yyyy-MM-dd"),
                FileTime = generationDateTime.ToString("HH:mm:ss.fff"),
                FileSequenceNumber = int.Parse(fileSequenceNumber),
                GroupHeaderRecords = new List<GroupHeaderRecord>()
            };

            int totalCheckCount = 0;
            decimal totalCheckAmount = 0;
            int checkNumberHash = 0;

            GroupHeaderRecord nationalInsuranceGroupHeaderRecord = new GroupHeaderRecord
            {
                IssuingCarrierName = "Centauri National Insurance Company",
                CheckType = "Policy Refund",
                DeliveryType = "Normal",
                BankId = 1,
                DetailRecords = new List<DetailRecord>()
            };

            GroupHeaderRecord specialtyInsuranceGroupHeaderRecord = new GroupHeaderRecord
            {
                IssuingCarrierName = "Centauri Specialty Insurance Company",
                CheckType = "Policy Refund",
                DeliveryType = "Normal",
                BankId = 1,
                DetailRecords = new List<DetailRecord>()
            };

            foreach (var detailRecordWithRisk in successfulRefunds)
            {

                specialtyInsuranceGroupHeaderRecord.DetailRecords.Add(detailRecordWithRisk.DetailRecord);

                totalCheckCount++;
                totalCheckAmount += detailRecordWithRisk.DetailRecord.RefundAmount;
                checkNumberHash += detailRecordWithRisk.DetailRecord.CheckNo;
            }

            nationalInsuranceGroupHeaderRecord.TotalCheckCount = nationalInsuranceGroupHeaderRecord.DetailRecords.Count;
            nationalInsuranceGroupHeaderRecord.TotalCheckAmount = nationalInsuranceGroupHeaderRecord.DetailRecords.Sum(x => x.RefundAmount);
            nationalInsuranceGroupHeaderRecord.CheckNumberHash = nationalInsuranceGroupHeaderRecord.DetailRecords.Sum(x => x.CheckNo);

            specialtyInsuranceGroupHeaderRecord.TotalCheckCount = specialtyInsuranceGroupHeaderRecord.DetailRecords.Count;
            specialtyInsuranceGroupHeaderRecord.TotalCheckAmount = specialtyInsuranceGroupHeaderRecord.DetailRecords.Sum(x => x.RefundAmount);
            specialtyInsuranceGroupHeaderRecord.CheckNumberHash = specialtyInsuranceGroupHeaderRecord.DetailRecords.Sum(x => x.CheckNo);

            if (nationalInsuranceGroupHeaderRecord.TotalCheckCount > 0)
            {
                policyChecks.GroupHeaderRecords.Add(nationalInsuranceGroupHeaderRecord);
            }
            if (specialtyInsuranceGroupHeaderRecord.TotalCheckCount > 0)
            {
                policyChecks.GroupHeaderRecords.Add(specialtyInsuranceGroupHeaderRecord);
            }

            policyChecks.TotalCheckCount = totalCheckCount;
            policyChecks.TotalCheckAmount = totalCheckAmount;
            policyChecks.CheckNumberHash = checkNumberHash;
            policyChecks.HeaderRecordCount = policyChecks.GroupHeaderRecords.Count;

            return policyChecks;
        }

        private async Task<List<DetailRecordWithRisk>> PostRefundsToSystem(List<DetailRecordWithRisk> detailRecordsWithRisk)
        {
            List<DetailRecordWithRisk> successfulRefunds = new List<DetailRecordWithRisk>();

            foreach (var detailRecordWithRisk in detailRecordsWithRisk)
            {
                try
                {
                    DetailRecord detailRecord = detailRecordWithRisk.DetailRecord;
                    var payment = await _refundService.PostRefund(new RefundRequestDto
                    {
                        RiskId = detailRecordWithRisk.RiskId,
                        Amount = detailRecord.RefundAmount,
                        InstrumentId = InstrumentType.Check.Id,
                        Comments = "Generated from Job",
                        CheckNumber = detailRecord.CheckNo.ToString(),
                        PayeeInfo = new PayeeInfoDto
                        {
                            Name = detailRecord.InsuredName,
                            Address = detailRecord.InsuredStreet1,
                            City = detailRecord.InsuredCity,
                            State = detailRecord.InsuredState,
                            ZipCode = detailRecord.InsuredZip
                        }
                    });

                    var refundRequestFromDb = await _refundRequestRepository.FindAsync(detailRecordWithRisk.RefundRequestId);
                    if (refundRequestFromDb != null)
                    {
                        refundRequestFromDb.SetCheckDetails(
                            detailRecordWithRisk.DetailRecord.CheckNo.ToString(),
                            DateTime.ParseExact(detailRecordWithRisk.DetailRecord.CheckDate, "yyyy-MM-dd",
                                CultureInfo.InvariantCulture, DateTimeStyles.None),
                            payment.Id);
                        await _refundRequestRepository.SaveChangesAsync();
                    }
                    successfulRefunds.Add(detailRecordWithRisk);
                }
                catch (Exception ex)
                {
                    failedRisks.Add(new FailedRiskViewDto
                    {
                        RiskId = detailRecordWithRisk.RiskId.ToString(),
                        PolicyNumber = detailRecordWithRisk.DetailRecord?.PolicyNumber,
                        ErrorMessage = ex.Message
                    });
                }
            }

            return successfulRefunds;
        }

        private class DetailRecordWithRisk
        {
            public Guid RiskId { get; set; }
            public Guid RefundRequestId { get; set; }
            public DetailRecord DetailRecord { get; set; }
        }

    }
}
