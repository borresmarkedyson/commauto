﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class BindService : IBindService
    {
        private readonly ITransactionService _transactionService;
        private readonly IInstallmentScheduleService _installmentScheduleService;
        private readonly IInvoiceService _invoiceService;
        private readonly IPaymentService _paymentService;
        private readonly IValidationService _validationService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IRiskRepository _riskRepository;
        private readonly IPaymentProfileService _paymentProfileService;
        private readonly IMapper _mapper;
        private readonly IUserDateService _userDateService;
        private readonly ITaxService _taxService;

        public BindService(
            ITransactionService transactionService,
            IInstallmentScheduleService installmentScheduleService,
            IInvoiceService invoiceService,
            IPaymentService paymentService,
            IValidationService validationService,
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IBillingSummaryService billingSummaryService,
            IInvoiceRepository invoiceRepository,
            IRiskRepository riskRepository,
            IPaymentProfileService paymentProfileService,
            IPaymentSubscriptionService paymentSubscriptionService,
            IMapper mapper,
            IUserDateService userDateService,
            ITaxService taxService
        )
        {
            _transactionService = transactionService;
            _installmentScheduleService = installmentScheduleService;
            _invoiceService = invoiceService;
            _paymentService = paymentService;
            _validationService = validationService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _billingSummaryService = billingSummaryService;
            _riskRepository = riskRepository;
            _paymentProfileService = paymentProfileService;
            _mapper = mapper;
            _userDateService = userDateService;
            _taxService = taxService;
        }

        public async Task<BindViewDto> Bind(Guid riskId, BindRequestDto request)
        {
            if (request.BillingDetail?.BillingAddress != null)
            {
                request.BillingDetail.BillingAddress.Email = request.Email;
            }

            var validationResult = new BindRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(riskId);
            if (risk != null)
            {
                throw new ParameterException(ExceptionMessages.RISK_ALREADY_BINDED);
            }

            try
            {
                DateTime currentDate = _userDateService.GetUser().UserDate;

                request.TransactionDetails = request.TransactionDetails.Where(t => t.AmountSubType.Id != AmountSubType.StampingFee.Id 
                                                && t.AmountSubType.Id != AmountSubType.SurplusLinesTax.Id
                                                && t.AmountSubType.Id != AmountSubType.InstallmentFee.Id).ToList();

                TaxDetails taxDetails = await _taxService.CalculateRiskTaxAsync(request.EffectiveDate, request.StateCode, request.TransactionDetails, null);

                request.TransactionDetails = taxDetails.TransactionDetails;

                await _transactionService.InsertTransactionAsync(riskId, new TransactionDto(request));
                await _installmentScheduleService.SaveInstallmentScheduleAsync(riskId, request.PolicyNumber, request.EffectiveDate,
                    request.PaymentPlan, request.AppId, false, request.PercentOfOutstanding);
                InvoiceDto invoice = await _invoiceService.GenerateInvoiceAsync(riskId, new GenerateInvoiceRequestDto(request.AppId, request.EffectiveDate),
                    true, request.TotalAmountCharged, request.WillPayLater);

                var riskFromDb = await _riskRepository.FindAsync(riskId);
                if (riskFromDb != null)
                {
                    riskFromDb.SetStateCode(request.StateCode);
                    riskFromDb.SetPolicyStatus(PolicyStatus.InForce);

                    if (string.Equals(request.PaymentPlan, PaymentPlan.PremiumFinanced.Id, StringComparison.OrdinalIgnoreCase))
                    {
                        if (request.AlpCommission.HasValue)
                        {
                            riskFromDb.SetAlpCommission(request.AlpCommission.Value);
                        }

                        if (request.PdpCommission.HasValue)
                        {
                            riskFromDb.SetPdpCommission(request.PdpCommission.Value);
                        }
                    }

                    await _riskRepository.SaveChangesAsync();
                }

                var installmentAndInvoices = await _billingSummaryService.GetInstallmentsAndInvoices(riskId);
                if (installmentAndInvoices.Count <= 1)
                {
                    invoice.FutureInstallmentAmount = 0;
                }
                else
                {
                    invoice.FutureInstallmentAmount = installmentAndInvoices[1].Premium;
                }
                var bindView = _mapper.Map<BindViewDto>(invoice);

                if (!request.WillPayLater && request.PaymentPlan != PaymentPlan.PremiumFinanced.Id)
                {
                    PercentDeposit percentDeposit = PercentDeposit.GetByPaymentPlanAndAppId(request.PaymentPlan, request.AppId);
                    bindView.Payment = await _paymentService.ExecutePayment(new PaymentRequestDto(riskId, percentDeposit.Value, request, currentDate), request.TransactionId);

                    invoice.PayoffAmount -= invoice.TotalAmountDue;
                }

                if (request.IsRecurringPayment)
                {
                    await _paymentProfileService.CreateProfileOnBind(riskId, request);

                    _mapper.Map(request, bindView);
                    bindView.InstallmentSchedule = await _billingSummaryService.GetInstallmentsAndInvoices(riskId);
                }

                return bindView;
            }
            catch (Exception ex)
            {
                await RollbackDbChanges(riskId);
                throw ex;
            }
        }

        private async Task RollbackDbChanges(Guid riskId)
        {
            await _transactionRepository.RemoveByRisk(riskId);
            await _installmentRepository.RemoveByRisk(riskId);
            await _invoiceRepository.RemoveByRisk(riskId);
            await _paymentProfileService.DeleteProfile(riskId);
            await _riskRepository.RemoveById(riskId);
        }
    }
}
