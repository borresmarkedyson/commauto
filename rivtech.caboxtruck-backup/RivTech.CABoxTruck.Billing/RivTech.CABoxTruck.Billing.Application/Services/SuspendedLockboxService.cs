﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Data;
using System.Collections.Generic;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using System;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Threading.Tasks;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Linq;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Constants;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedLockboxService : ISuspendedLockboxService
    {
        private readonly IFtpService _ftpService;
        private readonly IValidationService _validationService;
        private readonly ISuspendedPaymentService _suspendedPaymentService;
        private readonly IUserDateService _userDateService;
        private readonly ILog4NetService _logger;
        private readonly IMapper _mapper;

        public SuspendedLockboxService(
            IFtpService ftpService,
            IValidationService validationService,
            ISuspendedPaymentService suspendedPaymentService,
            IUserDateService userDateService,
            ILog4NetService logger,
            IMapper mapper
        )
        {
            _ftpService = ftpService;
            _validationService = validationService;
            _suspendedPaymentService = suspendedPaymentService;
            _userDateService = userDateService;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<SuspendedPaymentJobViewDto> ImportLockboxFile(PostLockboxSuspendedRequest jobRequest)
        {
            //validate job request
            var validationResult = new PostLockboxSuspendedRequestValidator().Validate(jobRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);
           
            //read lockbox files
            int fileCounter = 1;
            var lockboxSuspendedPayments = new List<CreateSuspendedPaymentRequestDto>();
            foreach (var fileName in GetLockboxFileNames())
            {
                lockboxSuspendedPayments.AddRange(GetPaymentFromLockbox(fileName, fileCounter));
                fileCounter++;
            }

            var error = new List<FailedRiskViewDto>();
            var createdSuspendedPayments = new List<SuspendedPaymentDto>();
            foreach (var lbpayment in lockboxSuspendedPayments)
            {
                try
                {
                    createdSuspendedPayments.Add(await _suspendedPaymentService.CreateSuspendedPayment(lbpayment));
                }
                catch(Exception e)
                {
                    error.Add(new FailedRiskViewDto()
                    {
                        RiskId = lbpayment.PolicyId,
                        PolicyNumber = lbpayment.PolicyNumber,
                        ErrorMessage = e.Message
                    });
                    await _logger.Error($"[{lbpayment.PolicyId}] [{lbpayment.PolicyNumber}]: {e.Message}", e.ToString());
                }
            }

            return new SuspendedPaymentJobViewDto()
            {
                SuspendedPayment = createdSuspendedPayments,
                FailedRisk = error
            };
        }

        private List<string> GetLockboxFileNames()
        {
            //create ftp request
            var lockboxSettings = Service.AppSettings.Lockbox;
            var lbFileNamePattern = UtilConstant.LOCKBOX_FILENAME_REGEXPATTERN.Replace("[date]", _userDateService.GetUser().UserDate.ToString("yyyyMMdd"));
            return _ftpService.GetFileNamesInServer(_mapper.Map<FTPSettingsDto>(lockboxSettings.FTP), lbFileNamePattern);
        }

        public List<CreateSuspendedPaymentRequestDto> GetPaymentFromLockbox(string fileName, int fileCounter)
        {
            //create ftp request
            var lockboxSettings = Service.AppSettings.Lockbox;
            lockboxSettings.FTP.FileName = fileName;
            var currentDate = _userDateService.GetUser().UserDate.ToString("yyyyMMdd");
            var dlFileName = lockboxSettings.FTP.FileName.Replace(".csv", "").Replace("/outbound/", "");
            var dlFilePath = $"{lockboxSettings.DLFileDirectory}{dlFileName}_{currentDate}_{fileCounter}.csv";
            var lbFileNamePattern = UtilConstant.LOCKBOX_FILENAME_REGEXPATTERN.Replace("[date]", currentDate);

            //download file
            if (!_ftpService.DownloadFile(_mapper.Map<FTPSettingsDto>(lockboxSettings.FTP), lockboxSettings.DLFileDirectory, dlFilePath))
                return new List<CreateSuspendedPaymentRequestDto>();

            //read data from csv
            var csvTable = new DataTable();
            string[] carrierDetail;
            using (var reader = new StreamReader(File.OpenRead(dlFilePath)))
            {
                reader.ReadLine(); // skip one line
                carrierDetail = reader.ReadLine().Split(',').Select(sValue => sValue.Trim('\"')).ToArray();
                csvTable.Load(new CsvReader(reader, true));
            }

            //remove empty rows
            csvTable = csvTable.Rows.Cast<DataRow>()
                .Where(row => !row.ItemArray.All(field => string.IsNullOrWhiteSpace(field as string)))
                .CopyToDataTable();

            //map data to createSuspendedPaymentRequestDto model
            var lockboxSuspendedPayments = new List<CreateSuspendedPaymentRequestDto>();
            var carrierId = carrierDetail[1].Equals(SuspendedDirectBillCarrier.CentauriNationalInsuranceCompany.Description) ?
                SuspendedDirectBillCarrier.CentauriNationalInsuranceCompany.Id : SuspendedDirectBillCarrier.CentauriSpecialtyInsuranceCompany.Id;
            for (int i = 0; i < csvTable.Rows.Count; i++)
            {
                var suspendedPayment = new CreateSuspendedPaymentRequestDto()
                {
                    SuspendedDetail = new SuspendedDetailDto()
                    {
                        Number = csvTable.Rows[i][0].ToString(),
                        EnvelopeNum = csvTable.Rows[i][1].ToString(),
                        Envelope = csvTable.Rows[i][2].ToString(),
                        TransactionNum = csvTable.Rows[i][3].ToString(),
                        LockboxNum = csvTable.Rows[i][4].ToString(),
                        Batch = csvTable.Rows[i][7].ToString(),
                        BatchItem = csvTable.Rows[i][8].ToString(),
                        InvoicePage = csvTable.Rows[i][18].ToString(),
                    },
                    SuspendedCheck = new SuspendedCheckDto()
                    {
                        Check = csvTable.Rows[i][9].ToString(),
                        CheckRoutingNum = csvTable.Rows[i][11].ToString(),
                        CheckAccountNum = csvTable.Rows[i][12].ToString(),
                        CheckNum = csvTable.Rows[i][13].ToString()
                    },
                    SuspendedImage = new SuspendedImageDto
                    {
                        CheckImg = csvTable.Rows[i][19].ToString(),
                        EnvelopeImg = csvTable.Rows[i][20].ToString(),
                        InvoiceImg = csvTable.Rows[i][21].ToString(),
                    },
                    Time = csvTable.Rows[i][6].ToString(),
                    PolicyNumber = csvTable.Rows[i][14].ToString(),
                    PolicyId = csvTable.Rows[i][15].ToString(),
                    DirectBillCarrierId = carrierId,
                    SourceId = SuspendedSource.Lockbox.Id
                };

                DateTime date;
                if (DateTime.TryParse(csvTable.Rows[i][5].ToString(), out date)) suspendedPayment.ReceiptDate = date;

                decimal amount;
                if (decimal.TryParse(csvTable.Rows[i][10].ToString().Replace("$",""), out amount)) suspendedPayment.SuspendedCheck.CheckAmount = amount;
                if (decimal.TryParse(csvTable.Rows[i][17].ToString().Replace("$", ""), out amount)) suspendedPayment.Amount = amount;

                lockboxSuspendedPayments.Add(suspendedPayment);
            }

            return lockboxSuspendedPayments;
        }
    }
}
