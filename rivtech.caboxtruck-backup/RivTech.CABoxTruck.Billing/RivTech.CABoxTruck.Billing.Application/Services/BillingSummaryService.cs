﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class BillingSummaryService : IBillingSummaryService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IMapper _mapper;
        private readonly IUserDateService _userDateService;
        private readonly IStateTaxRepository _stateTaxRepository;
        private readonly INonTaxableFeeRepository _nonTaxableFeeRepository;

        public BillingSummaryService(
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IMapper mapper,
            IUserDateService userDateService,
            IStateTaxRepository stateTaxRepository,
            INonTaxableFeeRepository nonTaxableFeeRepository
        )
        {
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _mapper = mapper;
            _userDateService = userDateService;
            _stateTaxRepository = stateTaxRepository;
            _nonTaxableFeeRepository = nonTaxableFeeRepository;
        }

        public async Task<List<InstallmentAndInvoiceDto>> GetInstallmentsAndInvoices(Guid riskId)
        {
            Risk risk = await _riskRepository.GetDetailsAsync(riskId);
            List<Installment> installments = await _installmentRepository.GetAllIncludingVoid(riskId);
            StateTax stateTaxDto = _stateTaxRepository.Get(risk.StateCode, risk.EffectiveDate);
            IEnumerable<FeeKind> feeKindDto = _nonTaxableFeeRepository.GetNonTaxableTypesByState(risk.StateCode, risk.EffectiveDate).Result;
            bool hasTaxableInstallmentFee = !feeKindDto.Any(ntf => ntf.Id.Equals(FeeKind.InstallmentFee.Id));
            decimal taxRate = hasTaxableInstallmentFee ? stateTaxDto.TotalRate : 0;

            BillingSummary billingSummary = new BillingSummary(
                risk,
                await _transactionRepository.GetAllByRisk(riskId),
                installments,
                await _invoiceRepository.GetAllByRisk(riskId),
                (List<Payment>)await _paymentRepository.GetAllByRiskId(riskId),
                _userDateService.GetUser().UserDate,
                taxRate);

            return _mapper.Map<List<InstallmentAndInvoiceDto>>(billingSummary.InstallmentAndInvoices);
        }

        public async Task<BillingSummaryDto> GetBillingSummary(Guid riskId)
        {
            BillingSummary billingSummary = new BillingSummary(
                await _riskRepository.GetDetailsAsync(riskId),
                await _transactionRepository.GetAllByRisk(riskId),
                await _installmentRepository.GetAllByRisk(riskId),
                await _invoiceRepository.GetAllByRisk(riskId),
                (List<Payment>)await _paymentRepository.GetAllByRiskId(riskId),
                _userDateService.GetUser().UserDate);

            return _mapper.Map<BillingSummaryDto>(billingSummary);
        }
    }
}
