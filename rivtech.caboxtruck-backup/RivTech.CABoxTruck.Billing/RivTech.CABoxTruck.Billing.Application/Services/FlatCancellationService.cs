﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class FlatCancellationService : IFlatCancellationService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;

        public FlatCancellationService(
            IRiskRepository riskRepository,
            IPaymentRepository paymentRepository,
            IBillingSummaryService billingSummaryService,
            IValidationService validationService,
            IUserDateService userDateService
        )
        {
            _riskRepository = riskRepository;
            _paymentRepository = paymentRepository;
            _billingSummaryService = billingSummaryService;
            _validationService = validationService;
            _userDateService = userDateService;
        }

        public async Task<List<FlatCancelNopcRiskDto>> GetFlatCancelNopcRisks(string appId)
        {
            DateTime requestDate = _userDateService.GetUser().UserDate;
            List<FlatCancelNopcRiskDto> flatCancelNopcRisks = new List<FlatCancelNopcRiskDto>();
            var nearDueDateRisks = await _riskRepository.GetAsync(x => x.NoticeHistory.FlatCancelNopcDate == null
                && requestDate.Date >= x.EffectiveDate.Date.AddDays(5) && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == appId, x => x.NoticeHistory);
            foreach (var risk in nearDueDateRisks)
            {
                var totalPayment = await _paymentRepository.GetTotalPaymentAsync(risk.Id);
                if (totalPayment == 0)
                {
                    var billingSummary = await _billingSummaryService.GetBillingSummary(risk.Id);

                    flatCancelNopcRisks.Add(new FlatCancelNopcRiskDto
                    {
                        RiskId = risk.Id,
                        NoticeDate = requestDate,
                        AmountBilled = billingSummary.Billed,
                        AmountPaid = billingSummary.Paid,
                        BalanceDue = billingSummary.Balance,
                        CancellationDate = risk.EffectiveDate,
                        FullAmountDue = billingSummary.PayoffAmount,
                        MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0
                    });
                    if (risk.NoticeHistory == null)
                    {
                        risk.InitializeNoticeHistory();
                    }
                    risk.NoticeHistory.FlatCancelNopcDate = requestDate;
                    risk.SetIsPendingCancellationForNonPayment(true);
                }
            }
            await _riskRepository.SaveChangesAsync();
            return flatCancelNopcRisks;
        }

        public async Task<FlatCancelNopcRiskDto> SetFlatCancelNopc(NoticeRequestDto request)
        {
            var validator = new NoticeRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            DateTime requestDate = _userDateService.GetUser().UserDate;

            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == request.RiskId && x.NoticeHistory.FlatCancelNopcDate == null
               && requestDate.Date >= x.EffectiveDate.Date.AddDays(5) && x.PolicyStatusId != PolicyStatus.Cancelled.Id
               && x.ApplicationId == request.AppId, x => x.NoticeHistory);

            if (riskFromDb == null)
            {
                return null;
            }

            var totalPayment = await _paymentRepository.GetTotalPaymentAsync(request.RiskId);
            if (totalPayment != 0)
            {
                return null;
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);

            if (riskFromDb.NoticeHistory == null)
            {
                riskFromDb.InitializeNoticeHistory();
            }
            riskFromDb.NoticeHistory.FlatCancelNopcDate = requestDate;
            riskFromDb.SetIsPendingCancellationForNonPayment(true);
            await _riskRepository.SaveChangesAsync();

            return new FlatCancelNopcRiskDto
            {
                RiskId = request.RiskId,
                NoticeDate = requestDate,
                AmountBilled = billingSummary.Billed,
                AmountPaid = billingSummary.Paid,
                BalanceDue = billingSummary.Balance,
                CancellationDate = riskFromDb.EffectiveDate,
                FullAmountDue = billingSummary.PayoffAmount,
                MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0
            };
        }
    }
}
