﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ApplicationUserService
    {
        private readonly IHttpContextAccessor _context;

        public ApplicationUserService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Guid? GetUserId()
        {
            var userId = _context.HttpContext.User.Claims
                       .First(i => i.Type == ClaimTypes.NameIdentifier).Value;
            if (string.IsNullOrEmpty(userId))
            {
                return null;
            }
            else
            {
                return new Guid(userId);
            }
        }
    }
}
