﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class DepositAmountService : IDepositAmountService
    {

        private readonly IRiskRepository _riskRepository;
        private readonly IInstallmentScheduleService _installmentScheduleService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IUserDateService _userDateService;

        public DepositAmountService(
            IRiskRepository riskRepository,
            IInstallmentScheduleService installmentScheduleService,
            ITransactionRepository transactionRepository,
            IUserDateService userDateService
        )
        {
            _riskRepository = riskRepository;
            _installmentScheduleService = installmentScheduleService;
            _transactionRepository = transactionRepository;
            _userDateService = userDateService;
        }

        public async Task<decimal> ComputeDeposit(Guid riskId, string appId)
        {
            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == riskId);
            if (riskFromDb == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            PremiumFeeAndTax transactionAmounts = await GetTransactionAmounts(riskId, riskFromDb.EffectiveDate);
            var installments = _installmentScheduleService.GenerateInstallmentScheduleWithoutSaving(riskId, riskFromDb.EffectiveDate, riskFromDb.PaymentPlanId, appId, false, null);

            var depositInstallment = installments.FirstOrDefault(x => x.InstallmentTypeId == InstallmentType.Deposit.Id);

            decimal premiumAmount = transactionAmounts.Premium * depositInstallment.PercentOfOutstanding;
            decimal feeAmount = transactionAmounts.Fees;
            decimal taxAmount = transactionAmounts.Tax;

            return premiumAmount + feeAmount + taxAmount;
        }


        private async Task<PremiumFeeAndTax> GetTransactionAmounts(Guid riskId, DateTime? effectiveDate = null)
        {
            if (effectiveDate == null)
            {
                effectiveDate = _userDateService.GetUser().UserDate.Date;
            }

            var transactions = await _transactionRepository.GetAllByRisk(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;
            foreach (var transaction in transactions)
            {
                if (transaction != null && transaction.TransactionDetails != null && effectiveDate >= transaction.EffectiveDate.Date
                    && transaction.TransactionTypeId != TransactionType.Cancellation.Id
                    && transaction.TransactionTypeId != TransactionType.Reinstatement.Id)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                        {
                            premiumAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                        {
                            feeAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                        {
                            feeAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id)
                        {
                            taxAmount += transactionDetail.Amount;
                        }
                    }
                }
            }

            return new PremiumFeeAndTax(premiumAmount, feeAmount, taxAmount);
        }
    }
}
