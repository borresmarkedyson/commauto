﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IAmountSubTypeRepository _amountSubTypeRepository;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IUserDateService _userDateService;
        private readonly IMapper _mapper;

        public TransactionService(
            ITransactionRepository transactionRepository,
            IAmountSubTypeRepository amountSubTypeRepository,
            IApplicationUserRepository applicationUserRepository,
            IUserDateService userDateService,
            IMapper mapper)
        {
            _transactionRepository = transactionRepository;
            _amountSubTypeRepository = amountSubTypeRepository;
            _applicationUserRepository = applicationUserRepository;
            _userDateService = userDateService;
            _mapper = mapper;
        }

        public async Task<List<TransactionDto>> GetAllAsync(Guid riskId)
        {
            var result = await _transactionRepository.GetAllByRisk(riskId);
            return _mapper.Map<List<TransactionDto>>(result);
        }

        public async Task<string> InsertTransactionAsync(Guid riskId, TransactionDto model)
        {
            if (!TransactionType.IsValid(model.TransactionType.Id))
            {
                throw new ParameterException(ExceptionMessages.INVALID_TRANSACTION_TYPE);
            }

            return await InsertTransaction(riskId, model, Enumeration.GetEnumerationById<TransactionType>(model.TransactionType.Id));
        }

        private async Task<string> InsertTransaction(Guid riskId, TransactionDto model, TransactionType transactionType)
        {
            List<TransactionDetail> transactionDetails = new List<TransactionDetail>();
            if (model.TransactionDetails != null)
            {
                foreach (var transactionDetailDto in model.TransactionDetails)
                {
                    if (transactionDetailDto != null)
                    {
                        var amountSubTypeFromDb = await _amountSubTypeRepository.FindAsync(transactionDetailDto.AmountSubType.Id);

                        transactionDetails.Add(new TransactionDetail(transactionDetailDto.Amount, transactionDetailDto.Description, amountSubTypeFromDb));
                    }
                }
            }

            var transaction = new Transaction(riskId, transactionType, transactionDetails, model.EffectiveDate, _userDateService.GetUser(), null);

            var result = await _transactionRepository.AddAsync(transaction);
            await _transactionRepository.SaveChangesAsync();

            return result.Id.ToString();
        }

        public async Task<PremiumFeesAndTaxDto> GetTransactionSummary(Guid riskId)
        {
            var transactions = await _transactionRepository.GetAllByRisk(riskId);

            decimal premiums = transactions.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Premium.Id).Sum(z => z.Amount));
            decimal premiumFees = transactions.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.Amount));
            decimal transactionFees = transactions.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.Amount));
            decimal taxes = transactions.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Tax.Id).Sum(z => z.Amount));
            decimal transactionTaxes = transactions.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.Amount));

            return new PremiumFeesAndTaxDto
            {
                Premium = premiums,
                Fees = premiumFees + transactionFees,
                Tax = taxes + transactionTaxes,
                Total = premiums + premiumFees + transactionFees + taxes
            };
        }

        public async Task<Transaction> GenerateTransactionBeforeBind(TransactionDto model, TransactionType transactionType)
        {
            List<TransactionDetail> transactionDetails = new List<TransactionDetail>();
            if (model.TransactionDetails != null)
            {
                foreach (var transactionDetailDto in model.TransactionDetails)
                {
                    if (transactionDetailDto != null)
                    {
                        var amountSubTypeFromDb = await _amountSubTypeRepository.FindAsync(transactionDetailDto.AmountSubType.Id);

                        transactionDetails.Add(new TransactionDetail(transactionDetailDto.Amount, transactionDetailDto.Description, amountSubTypeFromDb));
                    }
                }
            }

            var transaction = new Transaction(Guid.Empty, transactionType, transactionDetails, model.EffectiveDate, _userDateService.GetUser(), null);

            return transaction;
        }
    }
}
