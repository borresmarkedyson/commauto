﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ReinstatementService : IReinstatementService
    {
        private readonly IMapper _mapper;
        private readonly IRiskRepository _riskRepository;
        private readonly ISuspendedPaymentRepository _suspendedPaymentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IValidationService _validationService;
        private readonly ITransactionService _transactionService;
        private readonly IInvoiceService _invoiceService;
        private readonly IUserDateService _userDateService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly ITaxService _taxService;

        public ReinstatementService(
            IMapper mapper,
            IRiskRepository riskRepository,
            ISuspendedPaymentRepository suspendedPaymentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IValidationService validationService,
            ITransactionService transactionService,
            IInvoiceService invoiceService,
            IUserDateService userDateService,
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            ITaxService taxService
        )
        {
            _mapper = mapper;
            _riskRepository = riskRepository;
            _suspendedPaymentRepository = suspendedPaymentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _validationService = validationService;
            _transactionService = transactionService;
            _invoiceService = invoiceService;
            _userDateService = userDateService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _taxService = taxService;
        }

        public async Task<List<ReinstatementRuleView>> GetRisksForReinstatement(GetRisksForReinstatementRequestDto request)
        {
            //validate job request
            var validationResult = new JobRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var reinstatementRuleViews = new List<ReinstatementRuleView>();
            var risks = await _riskRepository.GetAsync(x => x.PolicyStatusId == PolicyStatus.Cancelled.Id
                && x.CancellationEffectiveDate <= _userDateService.GetUser().UserDate.Date
                && x.ApplicationId == request.ApplicationId && x.CanAutoReinstate);

            foreach (var risk in risks)
            {
                var reinstatementRuleView = await GetRiskForReinstatement(risk.Id);
                if (reinstatementRuleView != null)
                {
                    reinstatementRuleViews.Add(reinstatementRuleView);
                }
            }
            return reinstatementRuleViews;
        }

        public async Task<ReinstatementRuleView> GetRiskForReinstatement(Guid riskId)
        {
            //validate request
            var risk = await _riskRepository.FindAsync(riskId);
            if (risk?.PolicyStatusId != PolicyStatus.Cancelled.Id || risk.CancellationEffectiveDate > _userDateService.GetUser().UserDate.Date)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            var suspendedPayment = await _suspendedPaymentRepository.GetAsync(x => x.PolicyNumber == risk.PolicyNumber &&
                                    x.StatusId == SuspendedStatus.Pending.Id);

            //RULE: suspendedPaymentAmount >= pastDueAmount
            var totalPaymentAmount = await _paymentRepository.GetTotalPaymentAsync(risk.Id);
            var totalPastDueAmount = await _invoiceRepository.GetTotalPastDueAmount(risk.Id, _userDateService.GetUser().UserDate.Date) - totalPaymentAmount;
            var totalSuspendedPaymentAmount = await _suspendedPaymentRepository.GetTotalSuspendedPayment(risk.PolicyNumber, SuspendedStatus.Pending.Id);
            if (totalSuspendedPaymentAmount < totalPastDueAmount)
            {
                return null;
            }

            return new ReinstatementRuleView()
            {
                RiskId = risk.Id,
                TotalSuspendedPaymentAmount = totalSuspendedPaymentAmount,
                TotalPastDueAmount = totalPastDueAmount,
                TotalPaymentAmount = totalPaymentAmount,
                SuspendedPaymentId = suspendedPayment.Select(x => x.Id).ToList()
            };
        }

        public async Task<RiskDto> Reinstate(ReinstateRequestDto request)
        {
            //validate job request
            var validationResult = new ReinstateRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);
            var risk = await _riskRepository.FindAsync(x => x.Id == request.RiskId, x => x.NoticeHistory, x => x.PolicyStatus);
            if (risk?.PolicyStatusId != PolicyStatus.Cancelled.Id)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            string transactionId = string.Empty;
            InvoiceDto lastInvoiceCopy = null;
            Risk updatedRisk = null;

            //get current risk status in case rollback is needed
            PolicyStatus currentPolicyStatus = risk.PolicyStatus;
            bool isPendingCancellationForNonPayment = risk.IsPendingCancellationForNonPayment;
            bool canAutoReinstate = risk.CanAutoReinstate;
            DateTime? cancellationEffectiveDate = risk.CancellationEffectiveDate;

            //get notice history values in case rollback is needed
            NoticeHistory currentNoticeHistory = risk.NoticeHistory;

            try
            {
                //insert pro rated transaction
                var transaction = _mapper.Map<TransactionDto>(request);
                transaction.EffectiveDate = _userDateService.GetUser().UserDate.Date;
                transaction.AppId = risk.ApplicationId;



                request.TransactionDetails = request.TransactionDetails.Where(t => t.AmountSubType.Id != AmountSubType.StampingFee.Id
                                                    && t.AmountSubType.Id != AmountSubType.SurplusLinesTax.Id
                                                    && t.AmountSubType.Id != AmountSubType.InstallmentFee.Id).ToList();

                TaxDetails taxDetails = await _taxService.CalculateRiskTaxAsync(transaction.EffectiveDate, risk.StateCode, request.TransactionDetails, null);

                transaction.TransactionDetails = taxDetails.TransactionDetails;

                transactionId = await _transactionService.InsertTransactionAsync(request.RiskId, transaction);

                //void cancellation premium due invoice
                var lastInvoice = _invoiceRepository.GetCancellationInvoice(request.RiskId);
                lastInvoiceCopy = _mapper.Map<InvoiceDto>(lastInvoice);
                await _invoiceService.VoidInvoice(lastInvoice.InvoiceNumber, request.AppId, true, lastInvoice.Id);

                //update of riskstatus
                risk.SetPolicyStatus(PolicyStatus.InForce);
                risk.SetIsPendingCancellationForNonPayment(false);
                risk.SetCanAutoReinstate(true);
                risk.SetCancellationEffectiveDate(null);

                //reset notice history
                if (risk.NoticeHistory == null)
                {
                    risk.InitializeNoticeHistory();
                }

                risk.NoticeHistory.FlatCancelNopcDate = null;
                risk.NoticeHistory.NopcNoticeDate = null;
                risk.NoticeHistory.CancellationEffectiveDate = null;
                risk.NoticeHistory.CancellationProcessDate = null;
                risk.NoticeHistory.BalanceDueNoticeDate = null;

                updatedRisk = _riskRepository.Update(risk);
                await _riskRepository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                try
                {
                    await RollbackReinstatement(request.RiskId, transactionId, lastInvoiceCopy, currentPolicyStatus,
                        isPendingCancellationForNonPayment, canAutoReinstate, cancellationEffectiveDate, currentNoticeHistory);
                }
                catch (Exception) { }
                throw;
            }

            return _mapper.Map<RiskDto>(updatedRisk);
        }

        public async Task<RisksTaggedForManualReinstatementDto> TagForManualReinstatement(TagForManualReinstatementRequestDto request)
        {
            //validate request
            var validationResult = new TagForManualReinstatementRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risksTaggedForManualReinstatement = new List<Guid>();
            foreach (var riskId in request.RiskIds)
            {
                var riskFromDb = await _riskRepository.FindAsync(riskId);
                if (riskFromDb != null)
                {
                    riskFromDb.SetCanAutoReinstate(false);
                    risksTaggedForManualReinstatement.Add(riskId);
                }
            }
            await _riskRepository.SaveChangesAsync();

            return new RisksTaggedForManualReinstatementDto
            {
                RiskIds = risksTaggedForManualReinstatement
            };
        }

        private async Task RollbackReinstatement(Guid riskId, string transactionId, InvoiceDto lastInvoiceCopy,
            PolicyStatus currentPolicyStatus, bool isPendingCancellationForNonPayment, bool canAutoReinstate,
            DateTime? cancellationEffectiveDate, NoticeHistory currentNoticeHistory)
        {
            var risk = await _riskRepository.FindAsync(x => x.Id == riskId, x => x.NoticeHistory);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            // rollback risk changes
            risk.SetPolicyStatus(currentPolicyStatus);
            risk.SetIsPendingCancellationForNonPayment(isPendingCancellationForNonPayment);
            risk.SetCanAutoReinstate(canAutoReinstate);
            risk.SetCancellationEffectiveDate(cancellationEffectiveDate);

            if (risk.NoticeHistory == null)
            {
                risk.InitializeNoticeHistory();
            }
            risk.NoticeHistory.FlatCancelNopcDate = currentNoticeHistory.FlatCancelNopcDate;
            risk.NoticeHistory.NopcNoticeDate = currentNoticeHistory.NopcNoticeDate;
            risk.NoticeHistory.CancellationEffectiveDate = currentNoticeHistory.CancellationEffectiveDate;
            risk.NoticeHistory.CancellationProcessDate = currentNoticeHistory.CancellationProcessDate;
            risk.NoticeHistory.BalanceDueNoticeDate = currentNoticeHistory.BalanceDueNoticeDate;
            await _riskRepository.SaveChangesAsync();

            // rollback voiding of invoice
            if (lastInvoiceCopy != null && lastInvoiceCopy.VoidDate == null)
            {
                var invoiceFromDb = await _invoiceRepository.FindAsync(lastInvoiceCopy.Id);
                if (invoiceFromDb != null)
                {
                    invoiceFromDb.ResetVoidStatus();
                    await _invoiceRepository.SaveChangesAsync();
                }
            }

            // rollback insertion of transactions
            if (!string.IsNullOrEmpty(transactionId))
            {
                var transactionFromDb = await _transactionRepository.FindAsync(Guid.Parse(transactionId));
                if (transactionFromDb != null)
                {
                    _transactionRepository.Remove(transactionFromDb);
                    await _transactionRepository.SaveChangesAsync();
                }
            }
        }
    }
}
