﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class RefundService : IRefundService
    {
        private readonly IMapper _mapper;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;


        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IRiskRepository _riskRepository;

        public RefundService(
            IMapper mapper,
            IPaymentRepository paymentRepository,
            IApplicationUserRepository applicationUserRepository,
            IValidationService validationService,
            IUserDateService userDateService,

            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IRiskRepository riskRepository
        )
        {
            _mapper = mapper;
            _paymentRepository = paymentRepository;
            _applicationUserRepository = applicationUserRepository;
            _validationService = validationService;
            _userDateService = userDateService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _riskRepository = riskRepository;
        }

        public async Task<PaymentDto> PostRefund(RefundRequestDto request)
        {
            var validator = new RefundRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);


            BillingSummary billingSummary = new BillingSummary(
                await _riskRepository.GetDetailsAsync(request.RiskId),
                await _transactionRepository.GetAllByRisk(request.RiskId),
                await _installmentRepository.GetAllByRisk(request.RiskId),
                await _invoiceRepository.GetAllByRisk(request.RiskId),
                (List<Payment>)await _paymentRepository.GetAllByRiskId(request.RiskId),
                _userDateService.GetUser().UserDate);

            if (billingSummary.Paid - request.Amount < 0)
            {
                throw new ParameterException(ExceptionMessages.NEGATIVE_TOTAL_PAYMENT_NOT_ALLOWED);
            }

            var refundAmount = request.Amount;

            var paymentDetails = new List<PaymentDetail>();

            var totalPolicyOverpayment = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PolicyOverpayment.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;

            if (refundAmount > 0 && totalPolicyOverpayment > 0)
            {
                refundAmount -= totalPolicyOverpayment;

                if (refundAmount < 0)
                {
                    if (totalPolicyOverpayment > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-(totalPolicyOverpayment + refundAmount), AmountType.Overpayment, AmountSubType.PolicyOverpayment));
                    }
                }
                else
                {
                    if (totalPolicyOverpayment > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalPolicyOverpayment, AmountType.Overpayment, AmountSubType.PolicyOverpayment));
                    }
                }
            }

            var totalALPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;
            var totalPDPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;


            if (refundAmount > 0 && (totalALPremiumPaid > 0 || totalPDPremiumPaid > 0))
            {
                refundAmount -= (totalALPremiumPaid + totalPDPremiumPaid).RoundTo2Decimals();

                if (refundAmount < 0)
                {
                    var totalALPremiumBilledPercentage = totalALPremiumPaid / (totalALPremiumPaid + totalPDPremiumPaid);
                    var totalPDPremiumBilledPercentage = totalPDPremiumPaid / (totalALPremiumPaid + totalPDPremiumPaid);

                    var totalALPremiumAmount = totalALPremiumBilledPercentage * (request.Amount - totalPolicyOverpayment);
                    var totalPDPremiumAmount = totalPDPremiumBilledPercentage * (request.Amount - totalPolicyOverpayment);


                    if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                    {
                        var aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                        var pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                        if (aLCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(-aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission));
                        }

                        if (pDCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(-pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission));
                        }

                        totalALPremiumAmount -= aLCommissionAmount;
                        totalPDPremiumAmount -= pDCommissionAmount;
                    }

                    if (totalALPremiumAmount > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium));
                    }

                    if (totalPDPremiumAmount > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium));
                    }
                }
                else
                {
                    var totalALPremiumBilledPercentage = totalALPremiumPaid / billingSummary.Paid;
                    var totalPDPremiumBilledPercentage = totalPDPremiumPaid / billingSummary.Paid;

                    var totalALPremiumAmount = totalALPremiumBilledPercentage * billingSummary.Paid;
                    var totalPDPremiumAmount = totalPDPremiumBilledPercentage * billingSummary.Paid;


                    if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                    {
                        var aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                        var pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                        if (aLCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(-aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission));
                        }

                        if (pDCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(-pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission));
                        }

                        totalALPremiumAmount -= aLCommissionAmount;
                        totalPDPremiumAmount -= pDCommissionAmount;
                    }

                    if (totalALPremiumAmount > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium));
                    }

                    if (totalPDPremiumAmount > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium));
                    }
                }
            }

            var totalTaxPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.Tax.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;

            if (refundAmount > 0 && totalTaxPaid > 0)
            {
                refundAmount -= totalTaxPaid;

                if (refundAmount < 0)
                {
                    if (totalTaxPaid > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-(totalTaxPaid + refundAmount), AmountType.Tax));
                    }
                }
                else
                {
                    if (totalTaxPaid > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalTaxPaid, AmountType.Tax));
                    }
                }
            }

            var totalFeePaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.PremiumFee.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;

            if (refundAmount > 0 && totalFeePaid > 0)
            {
                refundAmount -= totalFeePaid;

                if (refundAmount < 0)
                {
                    if (totalFeePaid > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-(totalFeePaid + refundAmount), AmountType.PremiumFee));
                    }
                }
                else
                {
                    if (totalFeePaid > 0)
                    {
                        paymentDetails.Add(new PaymentDetail(-totalFeePaid, AmountType.PremiumFee));
                    }
                }
            }

            var payment = await _paymentRepository.AddAsync(
                new Payment(
                    request.RiskId,
                    -request.Amount,
                    _userDateService.GetUser().UserDate.Date,
                    request.CheckNumber,
                    InstrumentType.Refund,
                    paymentDetails,
                    request.Comments,
                    _userDateService.GetUser()
                ));

            payment.SetPaymentType(PaymentType.Refund);
            payment.SetPayeeInfo(_mapper.Map<PayeeInfoDto, PayeeInfo>(request.PayeeInfo));

            await _paymentRepository.SaveChangesAsync();
            return _mapper.Map<PaymentDto>(payment);
        }

        public async Task<PaymentDto> SetClearDate(SetClearDateRequestDto request)
        {
            var validator = new SetClearDateRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var paymentFromDb = await _paymentRepository.FindAsync(x => x.Id == request.PaymentId, x => x.PaymentDetails);
            if (paymentFromDb?.RiskId != request.RiskId)
            {
                throw new ParameterException(ExceptionMessages.INVALID_REQUEST);
            }

            paymentFromDb.SetClearDate(request.ClearDate);
            await _paymentRepository.SaveChangesAsync();

            return _mapper.Map<PaymentDto>(paymentFromDb);
        }

        public async Task<PaymentDto> SetEscheatDate(SetEscheatDateRequestDto request)
        {
            var validator = new SetEscheatDateRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var paymentFromDb = await _paymentRepository.FindAsync(x => x.Id == request.PaymentId, x => x.PaymentDetails);
            if (paymentFromDb?.RiskId != request.RiskId)
            {
                throw new ParameterException(ExceptionMessages.INVALID_REQUEST);
            }

            paymentFromDb.SetEscheatDate(request.EscheatDate, request.Comments);
            await _paymentRepository.SaveChangesAsync();

            return _mapper.Map<PaymentDto>(paymentFromDb);
        }
    }
}
