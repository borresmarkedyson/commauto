﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentDetailService : IPaymentDetailService
    {
        private readonly IPaymentDetailRepository _paymentDetailRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;

        public PaymentDetailService(IPaymentDetailRepository paymentDetailRepository,
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IValidationService validationService,
            IUserDateService userDateService)
        {
            _paymentDetailRepository = paymentDetailRepository;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _validationService = validationService;
            _userDateService = userDateService;
        }

        public async Task<List<PaymentDetail>> SavePaymentDetail(List<PaymentDetail> paymentDetails)
        {
            await _paymentDetailRepository.AddRangeAsync(paymentDetails);
            await _paymentDetailRepository.SaveChangesAsync();
            return paymentDetails;
        }

        public async Task UpdatePaymentDetail(PaymentDetail paymentDetail)
        {
            _paymentDetailRepository.Update(paymentDetail);
            await _paymentDetailRepository.SaveChangesAsync();
        }

        public async Task<List<PaymentDetail>> CreatePaymentDetail(Guid riskId, PaymentSummaryDto paymentSummary)
        {
            var amountTypes = Enumeration.GetAll<AmountType>();
            var paymentDetails = new List<PaymentDetail>();
            decimal amount = 0;

            var getAllPayments = await _paymentRepository.GetAllByRiskId(riskId);

            BillingSummary billingSummary = new BillingSummary(
                await _riskRepository.GetDetailsAsync(riskId),
                await _transactionRepository.GetAllByRisk(riskId),
                await _installmentRepository.GetAllByRisk(riskId),
                await _invoiceRepository.GetAllByRisk(riskId),
                (List<Payment>)getAllPayments,
                _userDateService.GetUser().UserDate);

            var totalALPremiumBilled = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed;
            var totalPDPremiumBilled = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed;

            var totalALPremiumWritten = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                                .Written;
            var totalPDPremiumWritten = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written;

            var totalALPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;
            var totalPDPremiumPaid = billingSummary.BillingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;

            var totalPremiumBilled = totalALPremiumBilled + totalPDPremiumBilled;
            var totalPremiumWritten = totalALPremiumWritten + totalPDPremiumWritten;
            var totalPremiumPaid = totalALPremiumPaid + totalPDPremiumPaid;

            var isPremiumPaymentOverBilled = (amount + totalPremiumPaid) > totalPremiumBilled && totalALPremiumWritten > totalALPremiumBilled;

            var totalALPremiumPercentage = (isPremiumPaymentOverBilled ? totalALPremiumWritten / totalPremiumWritten : totalALPremiumBilled / totalPremiumBilled);
            var totalPDPremiumPercentage = (isPremiumPaymentOverBilled ? totalPDPremiumWritten / totalPremiumWritten : totalPDPremiumBilled / totalPremiumBilled);

            if (isPremiumPaymentOverBilled)
            {
                List<Payment> payments = getAllPayments.ToList();
                var adjustedPaymentDetails = new List<PaymentDetail>();

                var premiumPayments = payments.Where(y => y.VoidDate == null).SelectMany(p => p.PaymentDetails).Where(x => x.AmountTypeId == AmountType.Premium.Id || x.AmountTypeId == AmountType.Commission.Id);
                decimal totalPremiumPayment = 0;

                foreach (var premiumPayment in premiumPayments)
                {
                    if (premiumPayment != null)
                    {
                        if (!string.IsNullOrWhiteSpace(premiumPayment.AmountSubTypeId))
                        {
                            totalPremiumPayment += premiumPayment.Amount;
                            adjustedPaymentDetails.Add(new PaymentDetail(-premiumPayment.Amount, Enumeration.GetEnumerationById<AmountType>(premiumPayment.AmountTypeId), Enumeration.GetEnumerationById<AmountSubType>(premiumPayment.AmountSubTypeId), premiumPayment.PaymentId));
                        }
                        else
                        {
                            totalPremiumPayment += premiumPayment.Amount;
                            adjustedPaymentDetails.Add(new PaymentDetail(-premiumPayment.Amount, Enumeration.GetEnumerationById<AmountType>(premiumPayment.AmountTypeId), premiumPayment.PaymentId));
                        }
                    }
                }

                var groupedPremiumPayments = payments.Where(y => y.VoidDate == null).SelectMany(p => p.PaymentDetails).Where(x => x.AmountTypeId == AmountType.Premium.Id || x.AmountTypeId == AmountType.Commission.Id).GroupBy(
                                        p => p.PaymentId,
                                        p => p.Amount,
                                        (key, g) => new { PaymentId = key, Amount = g.Sum() });

                foreach (var groupedPremiumPayment in groupedPremiumPayments)
                {
                    if (groupedPremiumPayment != null)
                    {
                        decimal totalALPremiumAmount = totalALPremiumPercentage * groupedPremiumPayment.Amount;
                        decimal totalPDPremiumAmount = totalPDPremiumPercentage * groupedPremiumPayment.Amount;

                        decimal aLCommissionAmount = 0;
                        decimal pDCommissionAmount = 0;

                        if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
                        {
                            aLCommissionAmount = totalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                            pDCommissionAmount = totalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                            totalALPremiumAmount -= aLCommissionAmount;
                            totalPDPremiumAmount -= pDCommissionAmount;
                        }

                        if (totalALPremiumAmount > 0)
                        {
                            adjustedPaymentDetails.Add(new PaymentDetail(totalALPremiumAmount, AmountType.Premium, AmountSubType.AutoLiabilityPremium, groupedPremiumPayment.PaymentId));
                        }

                        if (totalPDPremiumAmount > 0)
                        {
                            adjustedPaymentDetails.Add(new PaymentDetail(totalPDPremiumAmount, AmountType.Premium, AmountSubType.PhysicalDamagePremium, groupedPremiumPayment.PaymentId));
                        }

                        if (aLCommissionAmount > 0)
                        {
                            adjustedPaymentDetails.Add(new PaymentDetail(aLCommissionAmount, AmountType.Commission, AmountSubType.ALCommission, groupedPremiumPayment.PaymentId));
                        }

                        if (pDCommissionAmount > 0)
                        {
                            adjustedPaymentDetails.Add(new PaymentDetail(pDCommissionAmount, AmountType.Commission, AmountSubType.PDCommission, groupedPremiumPayment.PaymentId));
                        }
                    }
                }
                if (adjustedPaymentDetails.Count() > 0)
                {
                    await SavePaymentDetail(adjustedPaymentDetails);
                }
            }

            paymentSummary.TotalALPremiumAmount = totalALPremiumPercentage * paymentSummary.PremiumAmount;
            paymentSummary.TotalPDPremiumAmount = totalPDPremiumPercentage * paymentSummary.PremiumAmount;

            if (string.Equals(billingSummary.PaymentPlan, PaymentPlan.PremiumFinanced.Description))
            {
                paymentSummary.ALCommissionAmount = paymentSummary.TotalALPremiumAmount * (billingSummary.ALCommission.HasValue ? billingSummary.ALCommission.Value : 0m);
                paymentSummary.PDCommissionAmount = paymentSummary.TotalPDPremiumAmount * (billingSummary.PDCommission.HasValue ? billingSummary.PDCommission.Value : 0m);

                paymentSummary.TotalALPremiumAmount -= paymentSummary.ALCommissionAmount;
                paymentSummary.TotalPDPremiumAmount -= paymentSummary.PDCommissionAmount;
            }

            foreach (var type in amountTypes)
            {
                switch (type.Id)
                {
                    case var _ when type.Id == AmountType.Premium.Id:
                        if (paymentSummary.TotalALPremiumAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(paymentSummary.TotalALPremiumAmount, type, AmountSubType.AutoLiabilityPremium));
                        }

                        if (paymentSummary.TotalPDPremiumAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(paymentSummary.TotalPDPremiumAmount, type, AmountSubType.PhysicalDamagePremium));
                        }
                        break;
                    case var _ when type.Id == AmountType.PremiumFee.Id:
                        amount = string.Equals(paymentSummary.InstrumentId, InstrumentType.WriteOff.Id) ? paymentSummary.FeeAmount : paymentSummary.PremiumFeeAmount;
                        if (amount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(amount, type));
                        }
                        break;
                    case var _ when type.Id == AmountType.Tax.Id:
                        amount = string.Equals(paymentSummary.InstrumentId, InstrumentType.WriteOff.Id) ? paymentSummary.TaxAmount : paymentSummary.PremiumFeeTaxAmount;
                        if (amount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(amount, type));
                        }
                        break;
                    case var _ when type.Id == AmountType.TransactionFee.Id:
                        amount = paymentSummary.TransactionFeeAmount;
                        if (amount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(amount, type));
                        }
                        break;
                    case var _ when type.Id == AmountType.TransactionTax.Id:
                        amount = paymentSummary.TransactionTaxAmount;
                        if (amount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(amount, type));
                        }
                        break;
                    case var _ when type.Id == AmountType.Commission.Id:
                        if (paymentSummary.ALCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(paymentSummary.ALCommissionAmount, type, AmountSubType.ALCommission));
                        }

                        if (paymentSummary.PDCommissionAmount > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(paymentSummary.PDCommissionAmount, type, AmountSubType.PDCommission));
                        }
                        break;
                    case var _ when type.Id == AmountType.Overpayment.Id:
                        if (paymentSummary.PolicyOverpayment > 0)
                        {
                            paymentDetails.Add(new PaymentDetail(paymentSummary.PolicyOverpayment, type, AmountSubType.PolicyOverpayment));
                        }
                        break;
                    default:
                        throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
                }
            }

            return paymentDetails;
        }
    }
}
