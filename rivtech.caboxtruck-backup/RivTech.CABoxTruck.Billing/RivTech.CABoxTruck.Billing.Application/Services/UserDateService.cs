﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class UserDateService : IUserDateService
    {
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IMapper _mapper;

        public UserDateService(
            IApplicationUserRepository applicationUserRepository,
            IMapper mapper
        )
        {
            _applicationUserRepository = applicationUserRepository;
            _mapper = mapper;
        }

        private readonly ApplicationUser user = new ApplicationUser();

        public async Task SetUser(ApplicationUserDto requestUser, HttpRequest request)
        {
            await _applicationUserRepository.AddUserIfNotExisting(requestUser.Id, requestUser.UserName);

            if (request.Headers.TryGetValue("userdate", out var headerValues))
            {
                requestUser.UserDate = DateTime.Parse(headerValues.FirstOrDefault());
            }

            _mapper.Map(requestUser, user);
        }

        public ApplicationUser GetUser()
        {
            user.UserDate = user.UserDate == DateTime.MinValue ? DateTime.Now : user.UserDate.Date.Add(DateTime.Now.TimeOfDay);
            return user;
        }
    }
}