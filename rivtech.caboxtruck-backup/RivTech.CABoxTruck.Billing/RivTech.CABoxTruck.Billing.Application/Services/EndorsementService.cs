﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class EndorsementService : IEndorsementService
    {
        private readonly ITransactionService _transactionService;
        private readonly IInstallmentScheduleService _installmentScheduleService;
        private readonly IInvoiceService _invoiceService;
        private readonly IPaymentService _paymentService;
        private readonly IValidationService _validationService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IUserDateService _userDateService;
        private readonly ITaxService _taxService;

        public EndorsementService(
            ITransactionService transactionService,
            IInstallmentScheduleService installmentScheduleService,
            IInvoiceService invoiceService,
            IPaymentService paymentService,
            IValidationService validationService,
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IRiskRepository riskRepository,
            IApplicationUserRepository applicationUserRepository,
            IUserDateService userDateService,
            ITaxService taxService
        )
        {
            _transactionService = transactionService;
            _installmentScheduleService = installmentScheduleService;
            _invoiceService = invoiceService;
            _paymentService = paymentService;
            _validationService = validationService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _riskRepository = riskRepository;
            _applicationUserRepository = applicationUserRepository;
            _userDateService = userDateService;
            _taxService = taxService;
        }

        public async Task<InvoiceDto> AddEndorsement(EndorsementRequestDto request)
        {
            var validator = new EndorsementRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(request.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            request.TransactionDetails = request.TransactionDetails.Where(t => t.AmountSubType.Id != AmountSubType.StampingFee.Id
                                                && t.AmountSubType.Id != AmountSubType.SurplusLinesTax.Id
                                                && t.AmountSubType.Id != AmountSubType.InstallmentFee.Id).ToList();

            TaxDetails taxDetails = await _taxService.CalculateRiskTaxAsync(request.EndorsementEffectiveDate, risk.StateCode, request.TransactionDetails, null);

            request.TransactionDetails = taxDetails.TransactionDetails;

            await _transactionService.InsertTransactionAsync(request.RiskId, new TransactionDto(request, request.EndorsementEffectiveDate));

            int remainingInstallmentCount = await _installmentRepository.CountAllNotInvoiced(request.RiskId);
            decimal totalEndorsementTransaction = request.TransactionDetails.Sum(x => x.Amount);

            await _paymentService.RebalanceOverpayment(request.RiskId, "Endorsement");

            if (remainingInstallmentCount == 0 && totalEndorsementTransaction != 0)
            {
                DateTime currentDate = _userDateService.GetUser().UserDate;
                await _installmentRepository.AddAsync(new Installment(request.RiskId, currentDate, currentDate, 0, InstallmentType.Endorsement, _userDateService.GetUser()));
                await _installmentRepository.SaveChangesAsync();

                InvoiceDto invoice = await _invoiceService.GenerateInvoiceAsync(request.RiskId, new GenerateInvoiceRequestDto(request.AppId, null), false, 0);
                return invoice;
            }
            else
            {
                return null;
            }
        }
    }
}
