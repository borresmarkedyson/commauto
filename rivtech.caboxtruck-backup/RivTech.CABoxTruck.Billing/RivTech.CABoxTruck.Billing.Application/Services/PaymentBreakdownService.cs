﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentBreakdownService : IPaymentBreakdownService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;

        public PaymentBreakdownService(
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IValidationService validationService,
            IUserDateService userDateService
        )
        {
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _validationService = validationService;
            _userDateService = userDateService;
        }

        public async Task<PaymentRequestDto> ComputeBreakdown(PaymentRequestDto paymentRequest)
        {
            var validator = new BasicPaymentRequestValidator();
            var validationResult = validator.Validate(paymentRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            if (paymentRequest.PaymentSummary.InstrumentId == InstrumentType.Adjustment.Id ||
                paymentRequest.PaymentSummary.InstrumentId == InstrumentType.WriteOff.Id)
            {
                return paymentRequest;
            }

            BillingSummary billingSummary = new BillingSummary(
                await _riskRepository.GetDetailsAsync(paymentRequest.RiskId),
                await _transactionRepository.GetAllByRisk(paymentRequest.RiskId),
                await _installmentRepository.GetAllByRisk(paymentRequest.RiskId),
                await _invoiceRepository.GetAllByRisk(paymentRequest.RiskId),
                (List<Payment>)await _paymentRepository.GetAllByRiskId(paymentRequest.RiskId),
                _userDateService.GetUser().UserDate);

            var remainingBalance = billingSummary.RemainingBalance;

            return new PaymentRequestDto
            {
                AppId = paymentRequest.AppId,
                RiskId = paymentRequest.RiskId,
                BillingDetail = paymentRequest.BillingDetail,
                PaymentSummary = SetBreakdown(paymentRequest.PaymentSummary, remainingBalance.Premium,
                    remainingBalance.PremiumFee, remainingBalance.TransactionFee, remainingBalance.Tax,
                    remainingBalance.TransactionTax, billingSummary.UnpaidTransactions),
                PaymentPortalRequest = paymentRequest.PaymentPortalRequest
            };
        }

        private PaymentSummaryDto SetBreakdown(PaymentSummaryDto paymentSummary, decimal premiumBalance,
            decimal premiumFeeBalance, decimal transactionFeeBalance, decimal taxBalance,
            decimal transactionTaxBalance, PremiumFeeAndTax unpaidTransactions)
        {
            if (premiumBalance < 0)
            {
                premiumBalance = 0;
            }
            if (premiumFeeBalance < 0)
            {
                premiumFeeBalance = 0;
            }
            if (transactionFeeBalance < 0)
            {
                transactionFeeBalance = 0;
            }
            if (taxBalance < 0)
            {
                taxBalance = 0;
            }
            if (transactionTaxBalance < 0)
            {
                transactionTaxBalance = 0;
            }

            decimal premium;
            decimal premiumFee;
            decimal premiumFeeTax;
            decimal transactionFee;
            decimal transactionTax;
            decimal amount = paymentSummary.Amount;

            decimal policyOverpayment = 0;

            if ((string.Equals(paymentSummary.InstrumentId, InstrumentType.Check.Id)
                || string.Equals(paymentSummary.InstrumentId, InstrumentType.Cash.Id))
                && amount > (premiumBalance + premiumFeeBalance + taxBalance + transactionFeeBalance + transactionTaxBalance))
            {
                policyOverpayment = amount - premiumBalance - premiumFeeBalance - taxBalance - transactionFeeBalance - transactionTaxBalance;
                premiumFee = premiumFeeBalance;
                premiumFeeTax = taxBalance;
                premium = premiumBalance;
                transactionFee = transactionFeeBalance;
                transactionTax = transactionTaxBalance;
            }
            else if (amount >= (premiumBalance + premiumFeeBalance + taxBalance + transactionFeeBalance + transactionTaxBalance)
                && (premiumFeeBalance + taxBalance) <= 0 && (transactionFeeBalance + transactionTaxBalance) <= 0)
            {
                premiumFee = premiumFeeBalance;
                premiumFeeTax = taxBalance;
                premium = amount - premiumFeeBalance - taxBalance - transactionFeeBalance - transactionTaxBalance;
                transactionFee = transactionFeeBalance;
                transactionTax = transactionTaxBalance;
            }
            else if ((amount >= (premiumFeeBalance + transactionFeeBalance + taxBalance)))
            {
                premiumFee = premiumFeeBalance;
                premiumFeeTax = taxBalance;
                premium = 0;
                transactionFee = transactionFeeBalance;
                transactionTax = amount - premiumFeeBalance - taxBalance - transactionFeeBalance;
            }
            else if (amount >= (premiumFeeBalance + transactionFeeBalance))
            {
                premiumFee = premiumFeeBalance;
                premiumFeeTax = amount - premiumFeeBalance - transactionFeeBalance;
                premium = 0;
                transactionFee = transactionFeeBalance;
                transactionTax = 0;
            }
            else if (amount >= premiumFeeBalance)
            {
                premiumFee = premiumFeeBalance;
                premiumFeeTax = 0;
                premium = 0;
                transactionFee = amount - premiumFeeBalance;
                transactionTax = 0;
            }
            else
            {
                premiumFee = amount;
                premiumFeeTax = 0;
                premium = 0;
                transactionFee = 0;
                transactionTax = 0;
            }

            paymentSummary.PremiumAmount = premium;
            paymentSummary.PremiumFeeAmount = premiumFee;
            paymentSummary.TransactionFeeAmount = transactionFee;
            paymentSummary.PremiumFeeTaxAmount = premiumFeeTax;
            paymentSummary.TransactionTaxAmount = transactionTax;
            paymentSummary.PolicyOverpayment = policyOverpayment;

            if (paymentSummary.PremiumAmount > unpaidTransactions.Premium)
            {
                paymentSummary.TransactionFeeAmount += (paymentSummary.PremiumAmount - unpaidTransactions.Premium);
                paymentSummary.PremiumAmount = unpaidTransactions.Premium;
            }
            if (paymentSummary.TransactionFeeAmount > unpaidTransactions.TransactionFee)
            {
                paymentSummary.TransactionTaxAmount += (paymentSummary.TransactionFeeAmount - unpaidTransactions.TransactionFee);
                paymentSummary.TransactionFeeAmount = unpaidTransactions.TransactionFee;
            }
            if (paymentSummary.TransactionTaxAmount > unpaidTransactions.TransactionTax)
            {
                paymentSummary.PremiumAmount += (paymentSummary.TransactionTaxAmount - unpaidTransactions.TransactionTax);
                paymentSummary.TransactionTaxAmount = unpaidTransactions.TransactionTax;
            }
            if (paymentSummary.PremiumFeeAmount > unpaidTransactions.PremiumFee)
            {
                paymentSummary.PremiumFeeTaxAmount += (paymentSummary.PremiumFeeAmount - unpaidTransactions.PremiumFee);
                paymentSummary.PremiumFeeAmount = unpaidTransactions.PremiumFee;
            }
            if (paymentSummary.PremiumFeeTaxAmount > unpaidTransactions.Tax)
            {
                paymentSummary.PremiumAmount += (paymentSummary.PremiumFeeTaxAmount - unpaidTransactions.Tax);
                paymentSummary.PremiumFeeTaxAmount = unpaidTransactions.Tax;
            }

            if (paymentSummary.PremiumAmount > unpaidTransactions.Premium)
            {
                paymentSummary.PolicyOverpayment += (paymentSummary.PremiumAmount - unpaidTransactions.Premium);
                paymentSummary.PremiumAmount = unpaidTransactions.Premium;
            }

            paymentSummary.FeeAmount = paymentSummary.PremiumFeeAmount + paymentSummary.TransactionFeeAmount;
            paymentSummary.TaxAmount = paymentSummary.PremiumFeeTaxAmount + paymentSummary.TransactionTaxAmount;

            return paymentSummary;
        }
    }
}
