﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class AuthNetProfileService : IAuthNetProfileService
    {
        private readonly IMapper _mapper;
        private readonly IAuthNetProfileRepository _authNetProfileRepository;
        private readonly IUserDateService _userDateService;

        public AuthNetProfileService(
            IMapper mapper,
            IAuthNetProfileRepository authNetProfileRepository,
            IUserDateService userDateService
        )
        {
            _mapper = mapper;
            _authNetProfileRepository = authNetProfileRepository;
            _userDateService = userDateService;
        }


        public async Task<AuthNetProfileDto> SaveAuthNetProfile(Guid riskId, Guid paymentAccountId, createCustomerProfileResponse response)
        {
            var authNetAccount = new List<AuthNetAccount>()
            {
                new AuthNetAccount( paymentAccountId, response.customerPaymentProfileIdList[0])
            };

            var authNetProfile = await _authNetProfileRepository.AddAsync(
                new AuthNetProfile(
                    riskId,
                    response.customerProfileId,
                    _userDateService.GetUser(),
                    authNetAccount
                ));

            await _authNetProfileRepository.SaveChangesAsync();
            return _mapper.Map<AuthNetProfileDto>(authNetProfile);
        }

        public async Task<AuthNetProfileDto> DeleteAuthNetProfile(Guid riskId)
        {
            var profile = await _authNetProfileRepository.FindAsync(p => p.RiskId == riskId, p => p.AuthNetAccount);

            var deletedProfile = _authNetProfileRepository.Remove(profile);
            await _authNetProfileRepository.SaveChangesAsync();
            return _mapper.Map<AuthNetProfileDto>(deletedProfile);
        }
    }
}
