﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class CheckNumberService : ICheckNumberService
    {
        private readonly ISharedEntityRepository _sharedEntityRepository;

        public CheckNumberService(ISharedEntityRepository sharedEntityRepository)
        {
            _sharedEntityRepository = sharedEntityRepository;
        }

        public async Task<int> GenerateCheckNumber(string appId)
        {
            if (appId == RivtechApplication.Centauri.Id)
            {
                SharedEntity centauriCheckNumber = await _sharedEntityRepository.FindAsync(SharedEntity.CentauriCheckNumber.Id);
                if (centauriCheckNumber == null)
                {
                    centauriCheckNumber = SharedEntity.CentauriCheckNumber;
                    await _sharedEntityRepository.AddAsync(centauriCheckNumber);
                    await _sharedEntityRepository.SaveChangesAsync();
                }

                int nextCheckNumber = GetNextCheckNumber(centauriCheckNumber.Value);
                centauriCheckNumber.Value = nextCheckNumber.ToString();
                await _sharedEntityRepository.SaveChangesAsync();
                return nextCheckNumber;
            }
            else
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
        }

        private int GetNextCheckNumber(string currentSequenceNumber)
        {
            if (currentSequenceNumber == null)
            {
                return 20000001;
            }
            else
            {
                int sequenceNumberInt = int.Parse(currentSequenceNumber);
                return sequenceNumberInt + 1;
            }
        }
    }
}
