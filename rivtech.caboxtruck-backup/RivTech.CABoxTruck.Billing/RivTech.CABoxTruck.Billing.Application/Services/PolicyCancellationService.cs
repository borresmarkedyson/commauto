﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PolicyCancellationService : IPolicyCancellationService
    {
        private readonly ITransactionService _transactionService;
        private readonly IValidationService _validationService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IInvoiceNumberService _invoiceNumberService;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IMapper _mapper;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IUserDateService _userDateService;
        private readonly INsfService _nsfService;
        private readonly ITaxService _taxService;

        public PolicyCancellationService(
            ITransactionService transactionService,
            IValidationService validationService,
            ITransactionRepository transactionRepository,
            IInvoiceRepository invoiceRepository,
            IRiskRepository riskRepository,
            IInvoiceNumberService invoiceNumberService,
            IPaymentRepository paymentRepository,
            IMapper mapper,
            IBillingSummaryService billingSummaryService,
            IUserDateService userDateService,
            INsfService nsfService,
            ITaxService taxService
        )
        {
            _transactionService = transactionService;
            _validationService = validationService;
            _transactionRepository = transactionRepository;
            _invoiceRepository = invoiceRepository;
            _riskRepository = riskRepository;
            _invoiceNumberService = invoiceNumberService;
            _paymentRepository = paymentRepository;
            _mapper = mapper;
            _billingSummaryService = billingSummaryService;
            _userDateService = userDateService;
            _nsfService = nsfService;
            _taxService = taxService;
        }

        public async Task<PolicyCancellationInvoiceDto> ProrateAndCancelPolicy(PolicyCancellationRequestDto request)
        {
            var validator = new PolicyCancellationRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(request.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            if (risk.PolicyStatusId == PolicyStatus.Cancelled.Id)
            {
                throw new ParameterException(ExceptionMessages.POLICY_ALREADY_CANCELLED);
            }

            DateTime currentDate = _userDateService.GetUser().UserDate;

            var billingSummaryBeforeCancellation = await _billingSummaryService.GetBillingSummary(request.RiskId);
            decimal payoffAmountBeforeCancellation = billingSummaryBeforeCancellation.PayoffAmount;

            request.TransactionDetails = request.TransactionDetails.Where(t => t.AmountSubType.Id != AmountSubType.StampingFee.Id
                                                && t.AmountSubType.Id != AmountSubType.SurplusLinesTax.Id
                                                && t.AmountSubType.Id != AmountSubType.InstallmentFee.Id).ToList();

            TaxDetails taxDetails = await _taxService.CalculateRiskTaxAsync(billingSummaryBeforeCancellation.EffectiveDate.Value, risk.StateCode, request.TransactionDetails, null);

            request.TransactionDetails = taxDetails.TransactionDetails;

            await _transactionService.InsertTransactionAsync(request.RiskId, new TransactionDto(request, currentDate));

            var invoiceDetails = await GetInvoiceDetails(request.RiskId);

            DateTime invoiceDate = currentDate;

            Invoice invoice = new Invoice(request.RiskId, await _invoiceNumberService.GenerateInvoiceNumber(request.AppId, risk.PolicyNumber), invoiceDate,
                invoiceDate, await ComputePreviousBalance(request.RiskId), invoiceDetails.Sum(x => x.InvoicedAmount), invoiceDetails, _userDateService.GetUser());

            invoice.SetIsFlatCancelInvoice(request.IsFlatCancel);

            await _invoiceRepository.AddAsync(invoice);
            await _invoiceRepository.SaveChangesAsync();

            var riskFromDb = await _riskRepository.FindAsync(request.RiskId);
            riskFromDb.SetLatestInvoiceDueDate(invoiceDate);
            riskFromDb.SetPolicyStatus(PolicyStatus.Cancelled);
            riskFromDb.SetCancellationEffectiveDate(_userDateService.GetUser().UserDate.Date);

            if (request.IsManualCancellation && request.IsReasonNonPayment)
            {
                riskFromDb.SetCanAutoReinstate(true);
            }
            else if (!request.IsManualCancellation && riskFromDb.IsPendingCancellationForNonPayment)
            {
                riskFromDb.SetCanAutoReinstate(true);
            }
            else
            {
                riskFromDb.SetCanAutoReinstate(false);
            }

            riskFromDb.SetIsPendingCancellationForNonPayment(false);
            await _riskRepository.SaveChangesAsync();

            var nsfCountDto = await _nsfService.GetNsfCount(request.RiskId);

            var invoiceDto = _mapper.Map<PolicyCancellationInvoiceDto>(invoice);

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);
            invoiceDto.PayoffAmount = billingSummary.PayoffAmount;
            invoiceDto.TotalPaid = billingSummary.Paid;
            invoiceDto.MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0;
            invoiceDto.FullAmountDue = payoffAmountBeforeCancellation;
            invoiceDto.IsNsf = nsfCountDto.NsfCount > 0;

            return invoiceDto;
        }

        private async Task<List<InvoiceDetail>> GetTotalTransactions(Guid riskId)
        {
            List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();
            var transactionsFromDb = await _transactionRepository.GetAllByRisk(riskId);
            foreach (var transaction in transactionsFromDb)
            {
                if (transaction.TransactionTypeId == TransactionType.Void.Id)
                {
                    continue;
                }
                if (transaction.TransactionDetails != null)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail.VoidDate != null)
                        {
                            continue;
                        }

                        if (invoiceDetails.Any(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId))
                        {
                            var invoiceDetail = invoiceDetails.First(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId);
                            invoiceDetail.AddAmount(transactionDetail.Amount);
                        }
                        else
                        {
                            invoiceDetails.Add(new InvoiceDetail(transactionDetail.Amount, transactionDetail.AmountSubType));
                        }
                    }
                }
            }

            return invoiceDetails;
        }

        private async Task<List<InvoiceDetail>> GetPreviousInvoiceDetails(Guid riskId)
        {
            List<InvoiceDetail> previousInvoiceDetails = new List<InvoiceDetail>();
            var previousInvoices = await _invoiceRepository.GetAllNotVoided(riskId);
            if (previousInvoices == null)
            {
                return previousInvoiceDetails;
            }

            foreach (var invoice in previousInvoices)
            {
                if (invoice.InvoiceDetails != null)
                {
                    foreach (var previousInvoiceDetail in invoice.InvoiceDetails)
                    {
                        if (previousInvoiceDetails.Any(x => x.AmountSubTypeId == previousInvoiceDetail.AmountSubTypeId))
                        {
                            var invoiceDetail = previousInvoiceDetails.First(x => x.AmountSubTypeId == previousInvoiceDetail.AmountSubTypeId);
                            invoiceDetail.AddAmount(previousInvoiceDetail.InvoicedAmount);
                        }
                        else
                        {
                            previousInvoiceDetails.Add(new InvoiceDetail(previousInvoiceDetail.InvoicedAmount, previousInvoiceDetail.AmountSubType));
                        }
                    }
                }
            }
            return previousInvoiceDetails;
        }

        private void SubtractPreviousInvoices(List<InvoiceDetail> invoiceDetails, List<InvoiceDetail> previousInvoiceDetails)
        {
            foreach (var invoiceDetail in invoiceDetails)
            {
                if (previousInvoiceDetails.Any(x => x.AmountSubTypeId == invoiceDetail.AmountSubTypeId))
                {
                    var previousInvoiceDetail = previousInvoiceDetails.First(x => x.AmountSubTypeId == invoiceDetail.AmountSubTypeId);
                    invoiceDetail.AddAmount(-previousInvoiceDetail.InvoicedAmount);
                }
            }

            invoiceDetails.RemoveAll(x => x.InvoicedAmount == 0);
        }

        private async Task<List<InvoiceDetail>> GetInvoiceDetails(Guid riskId)
        {
            var invoiceDetails = await GetTotalTransactions(riskId);
            var previousInvoiceDetails = await GetPreviousInvoiceDetails(riskId);
            SubtractPreviousInvoices(invoiceDetails, previousInvoiceDetails);

            return invoiceDetails;
        }

        private async Task<decimal> ComputePreviousBalance(Guid riskId)
        {
            decimal previousPaymentAmount = await _paymentRepository.GetTotalPaymentAsync(riskId);
            decimal previousInvoicedAmount = await _invoiceRepository.GetTotalInvoicedAmountAsync(riskId);

            return previousInvoicedAmount - previousPaymentAmount;
        }

        public async Task<NopcRiskDto> SetPendingCancellation(PendingCancellationRequestDto request)
        {
            var validator = new PendingCancellationRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(x => x.Id == request.RiskId, x => x.NoticeHistory);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            if (risk.PolicyStatusId == PolicyStatus.Cancelled.Id)
            {
                throw new ParameterException(ExceptionMessages.POLICY_ALREADY_CANCELLED);
            }

            DateTime currentDateTime = _userDateService.GetUser().UserDate;

            if (risk.NoticeHistory == null)
            {
                risk.InitializeNoticeHistory();
            }
            risk.NoticeHistory.NopcNoticeDate = currentDateTime;
            risk.NoticeHistory.CancellationEffectiveDate = request.CancellationEffectiveDate;
            risk.SetIsPendingCancellationForNonPayment(request.IsReasonNonPayment);
            await _riskRepository.SaveChangesAsync();

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);

            return new NopcRiskDto
            {
                RiskId = risk.Id,
                NoticeDate = currentDateTime,
                AmountBilled = billingSummary.Billed,
                AmountPaid = billingSummary.Paid,
                BalanceDue = billingSummary.Balance,
                MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0,
                FullAmountDue = billingSummary.PayoffAmount,
                CancellationDate = request.CancellationEffectiveDate
            };
        }
    }
}
