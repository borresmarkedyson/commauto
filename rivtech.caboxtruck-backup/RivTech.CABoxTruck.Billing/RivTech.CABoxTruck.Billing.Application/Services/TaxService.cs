﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class TaxService : ITaxService
    {
        private readonly IMapper _mapper;
        private readonly IStateTaxRepository _stateTaxRepository;
        private readonly INonTaxableFeeRepository _nonTaxableFeeRepository;

        public TaxService(
            IMapper mapper,
            IStateTaxRepository stateTaxRepository,
            INonTaxableFeeRepository nonTaxableFeeRepository
        )
        {
            _mapper = mapper;
            _stateTaxRepository = stateTaxRepository;
            _nonTaxableFeeRepository = nonTaxableFeeRepository;
        }

        public async Task<ComputeRiskTaxResponseDto> PostCalculateRiskTaxAsync(DateTime effectiveDate, string stateCode, List<TransactionDetailDto> transactionDetails, decimal? installmentCount)
        {
            TaxDetails taxDetails = await CalculateRiskTaxAsync(effectiveDate, stateCode, transactionDetails, installmentCount);
            ComputeRiskTaxResponseDto response = new ComputeRiskTaxResponseDto(); 
            var taxDetailsDto = _mapper.Map<TaxDetailsDto>(taxDetails);
            response.TaxDetails = taxDetailsDto;

            return response;
        }

        public async Task<TaxDetails> CalculateRiskTaxAsync(DateTime effectiveDate, string stateCode, List<TransactionDetailDto> transactionDetails, decimal? installmentCount)
        {
            if (effectiveDate == null)
                throw new InvalidOperationException("Cannot calculate tax without effective date.");

            TaxDetails taxDetails = null;

            StateTax stateTax = _stateTaxRepository.Get(stateCode, effectiveDate);

            List<TransactionDetailDto> sumTransactionDetails = new List<TransactionDetailDto>();

            sumTransactionDetails.Add(new TransactionDetailDto {
                Amount = transactionDetails.Where(t => AmountSubType.ALPremiumGroup.ContainsKey(t.AmountSubType.Id))
                                                   .Sum(s => s.Amount),
                                                            AmountSubType = new AmountSubTypeDto { 
                                                                Id = AmountSubType.AutoLiabilityPremium.Id, 
                                                                Description = AmountSubType.AutoLiabilityPremium.Description,          
                                                                AmountType = new EnumerationDto { 
                                                                    Id = AmountType.Premium.Id, 
                                                                    Description = AmountType.Premium.Description 
                                                                }
                                                            }
            });

            sumTransactionDetails.Add(new TransactionDetailDto
            {
                Amount = transactionDetails.Where(t => AmountSubType.PDPremiumGroup.ContainsKey(t.AmountSubType.Id))
                                                   .Sum(s => s.Amount),
                AmountSubType = new AmountSubTypeDto
                {
                    Id = AmountSubType.PhysicalDamagePremium.Id,
                    Description = AmountSubType.PhysicalDamagePremium.Description,
                    AmountType = new EnumerationDto
                    {
                        Id = AmountType.Premium.Id,
                        Description = AmountType.Premium.Description
                    }
                }
            });

            sumTransactionDetails.Add(new TransactionDetailDto
            {
                Amount = transactionDetails.Where(t => string.Equals(t.AmountSubType.Id, AmountSubType.ALSFee.Id))
                                                   .Sum(s => s.Amount),
                AmountSubType = new AmountSubTypeDto
                {
                    Id = AmountSubType.ALSFee.Id,
                    Description = AmountSubType.ALSFee.Description,
                    AmountType = new EnumerationDto
                    {
                        Id = AmountType.PremiumFee.Id,
                        Description = AmountType.PremiumFee.Description
                    }
                }
            });

            sumTransactionDetails.Add(new TransactionDetailDto
            {
                Amount = transactionDetails.Where(t => string.Equals(t.AmountSubType.Id, AmountSubType.PDSFee.Id))
                                                   .Sum(s => s.Amount),
                AmountSubType = new AmountSubTypeDto
                {
                    Id = AmountSubType.PDSFee.Id,
                    Description = AmountSubType.PDSFee.Description,
                    AmountType = new EnumerationDto
                    {
                        Id = AmountType.PremiumFee.Id,
                        Description = AmountType.PremiumFee.Description
                    }
                }
            });

            sumTransactionDetails.Add(new TransactionDetailDto
            {
                Amount = transactionDetails.Where(t => string.Equals(t.AmountSubType.Id, AmountSubType.InstallmentFee.Id))
                                                   .Sum(s => s.Amount),
                AmountSubType = new AmountSubTypeDto
                {
                    Id = AmountSubType.InstallmentFee.Id,
                    Description = AmountSubType.InstallmentFee.Description,
                    AmountType = new EnumerationDto
                    {
                        Id = AmountType.TransactionFee.Id,
                        Description = AmountType.TransactionFee.Description
                    }
                }
            });

            sumTransactionDetails.Add(new TransactionDetailDto
            {
                Amount = transactionDetails.Where(t => string.Equals(t.AmountSubType.Id, AmountSubType.ReinstatementFee.Id))
                                                   .Sum(s => s.Amount),
                AmountSubType = new AmountSubTypeDto
                {
                    Id = AmountSubType.ReinstatementFee.Id,
                    Description = AmountSubType.ReinstatementFee.Description,
                    AmountType = new EnumerationDto
                    {
                        Id = AmountType.PremiumFee.Id,
                        Description = AmountType.PremiumFee.Description
                    }
                }
            });

            try
            {
                var riskPremiumFees = CreateFromTransactionDetails(sumTransactionDetails, installmentCount);

                taxDetails = await Create(riskPremiumFees, stateTax, effectiveDate);

                taxDetails.Calculate();

                if (taxDetails != null && taxDetails.TransactionDetails != null && taxDetails.TransactionDetails.Count() > 0)
                {
                    transactionDetails.AddRange(taxDetails.TransactionDetails);
                    taxDetails.TransactionDetails = transactionDetails;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}");
            }

            return taxDetails;

        }
        public async Task<TaxDetails> Create(RiskPremiumFees premiumFees, StateTax stateTax, DateTime policyEffectiveDate)
        {
            IEnumerable<FeeKind> nonTaxableFees = await _nonTaxableFeeRepository
                .GetNonTaxableTypesByState(stateTax.StateCode, policyEffectiveDate);

            return new TaxDetails(premiumFees, stateTax, nonTaxableFees);
        }

        public RiskPremiumFees CreateFromTransactionDetails(List<TransactionDetailDto> sumTransactionDetails, decimal? installmentCount)
        {
            var riskPremiumFees = new RiskPremiumFees();

            foreach (TransactionDetailDto transactionDetail in sumTransactionDetails)
            {
                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.AutoLiabilityPremium.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new RegularPremium(RegularPremiumKind.AutoLiability, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.PhysicalDamagePremium.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new RegularPremium(RegularPremiumKind.PhysicalDamage, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.CargoPremium.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new RegularPremium(RegularPremiumKind.Cargo, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.GeneralLiabilityPremium.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new AdditionalPremium(AdditionalPremiumKind.GeneralLiability, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.ALSFee.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new DTO.Fee(FeeKind.RiskManagementFeeAL, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.PDSFee.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new DTO.Fee(FeeKind.RiskManagementFeePD, transactionDetail.Amount));

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.InstallmentFee.Id, StringComparison.OrdinalIgnoreCase) && (installmentCount ?? 0) > 0)
                {
                    decimal totalInstallmentFee = transactionDetail.Amount * installmentCount.Value;
                    riskPremiumFees.Add(new DTO.Fee(FeeKind.InstallmentFee, totalInstallmentFee));
                }

                if (string.Equals(transactionDetail.AmountSubType.Id, AmountSubType.ReinstatementFee.Id, StringComparison.OrdinalIgnoreCase))
                    riskPremiumFees.Add(new DTO.Fee(FeeKind.ReinstatementFee, transactionDetail.Amount));

            }

            return riskPremiumFees;
        }
    }
}
