﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class NopcService : INopcService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;
        private readonly INoticeRepository _noticeRepository;

        public NopcService(
            IRiskRepository riskRepository,
            IInvoiceRepository invoiceRepository,
            IBillingSummaryService billingSummaryService,
            IPaymentRepository paymentRepository,
            IValidationService validationService,
            IUserDateService userDateService,
            INoticeRepository noticeRepository
        )
        {
            _riskRepository = riskRepository;
            _invoiceRepository = invoiceRepository;
            _billingSummaryService = billingSummaryService;
            _paymentRepository = paymentRepository;
            _validationService = validationService;
            _userDateService = userDateService;
            _noticeRepository = noticeRepository;
        }

        public async Task<List<NopcRiskDto>> GetNopcRisks(string appId)
        {
            List<NopcRiskDto> nopcRisks = new List<NopcRiskDto>();
            DateTime requestDate = _userDateService.GetUser().UserDate;

            var nopcRisksFromDb = await _noticeRepository.GetNopcRisks(requestDate, appId);
            foreach (var risk in nopcRisksFromDb)
            {
                nopcRisks.Add(new NopcRiskDto
                {
                    RiskId = risk.RiskId,
                    NoticeDate = requestDate,
                    AmountBilled = risk.AmountBilled,
                    AmountPaid = risk.AmountPaid,
                    BalanceDue = risk.BalanceDue,
                    FullAmountDue = risk.FullAmountDue,
                    MinimumAmountDue = risk.MinimumAmountDue,
                    CancellationDate = requestDate.Date.AddDays(15)
                });

                var riskFromDb = await _riskRepository.FindAsync(x => x.Id == risk.RiskId, x => x.NoticeHistory);
                if (riskFromDb.NoticeHistory == null)
                {
                    riskFromDb.InitializeNoticeHistory();
                }
                riskFromDb.NoticeHistory.NopcNoticeDate = requestDate;
                riskFromDb.NoticeHistory.CancellationEffectiveDate = requestDate.Date.AddDays(15);
                riskFromDb.SetIsPendingCancellationForNonPayment(true);
            }

            await _riskRepository.SaveChangesAsync();
            return nopcRisks;
        }

        private int GetDaysToNextBusinessDate(DateTime date)
        {
            if (date.DayOfWeek == DayOfWeek.Friday)
            {
                return 3;
            }
            else if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }

        public async Task<List<NopcRiskDto>> GetNopcRisksToRescind(string appId)
        {
            List<NopcRiskDto> nopcRisksToRescind = new List<NopcRiskDto>();
            DateTime requestDate = _userDateService.GetUser().UserDate;

            var nopcRisks = await _riskRepository.GetAsync(x => x.NoticeHistory.CancellationEffectiveDate != null
                && x.NoticeHistory.CancellationProcessDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == appId, x => x.NoticeHistory);

            foreach (var risk in nopcRisks)
            {

                var billingSummary = await _billingSummaryService.GetBillingSummary(risk.Id);
                var nsfCount = await _paymentRepository.GetNsfCount(risk.Id);

                if (nsfCount > 2 && billingSummary.Paid < billingSummary.TotalPremium)
                {
                    continue;
                }

                if (billingSummary.PastDueAmount <= 0)
                {
                    nopcRisksToRescind.Add(new NopcRiskDto
                    {
                        RiskId = risk.Id,
                        NoticeDate = requestDate,
                        AmountBilled = billingSummary.Billed,
                        AmountPaid = billingSummary.Paid,
                        BalanceDue = billingSummary.Balance,
                        FullAmountDue = billingSummary.PayoffAmount,
                        MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0,
                        CancellationDate = risk.NoticeHistory.CancellationEffectiveDate.Value.Date
                    });

                    if (risk.NoticeHistory == null)
                    {
                        risk.InitializeNoticeHistory();
                    }
                    risk.NoticeHistory.NopcNoticeDate = null;
                    risk.NoticeHistory.CancellationEffectiveDate = null;
                    risk.SetIsPendingCancellationForNonPayment(false);
                }
            }
            await _riskRepository.SaveChangesAsync();
            return nopcRisksToRescind;
        }

        public async Task<NopcRiskDto> SetNopc(NoticeRequestDto request)
        {
            var validator = new NoticeRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            DateTime requestDate = _userDateService.GetUser().UserDate;

            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == request.RiskId
                && x.NoticeHistory.NopcNoticeDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == request.AppId, x => x.NoticeHistory);

            if (riskFromDb == null)
            {
                return null;
            }
            var generatedInvoices = await _invoiceRepository.GetAllNotVoided(request.RiskId);
            if (generatedInvoices.Count <= 1)
            {
                return null;
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);
            if (billingSummary.EquityDate == null || billingSummary.Paid <= 0)
            {
                return null;
            }

            if (billingSummary.PastDueAmount >= 20 &&
                requestDate.Date >= billingSummary.EquityDate.Value.Date.AddDays(-(GetDaysToNextBusinessDate(requestDate) + 14)))
            {
                if (riskFromDb.NoticeHistory == null)
                {
                    riskFromDb.InitializeNoticeHistory();
                }
                riskFromDb.NoticeHistory.NopcNoticeDate = requestDate;
                riskFromDb.NoticeHistory.CancellationEffectiveDate = billingSummary.EquityDate.Value.Date;
                riskFromDb.SetIsPendingCancellationForNonPayment(true);
                await _riskRepository.SaveChangesAsync();

                return new NopcRiskDto
                {
                    RiskId = request.RiskId,
                    NoticeDate = requestDate,
                    AmountBilled = billingSummary.Billed,
                    AmountPaid = billingSummary.Paid,
                    BalanceDue = billingSummary.Balance,
                    FullAmountDue = billingSummary.PayoffAmount,
                    MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0,
                    CancellationDate = billingSummary.EquityDate.Value.Date
                };
            }
            else
            {
                return null;
            }
        }

        public async Task<NopcRiskDto> RescindNopcRisk(NoticeRequestDto request)
        {
            var validator = new NoticeRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            DateTime requestDate = _userDateService.GetUser().UserDate;

            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == request.RiskId && x.NoticeHistory.CancellationEffectiveDate != null
                && x.NoticeHistory.CancellationProcessDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == request.AppId, x => x.NoticeHistory);

            if (riskFromDb == null)
            {
                return null;
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);
            var nsfCount = await _paymentRepository.GetNsfCount(request.RiskId);

            if (nsfCount > 2 && billingSummary.Paid < billingSummary.TotalPremium)
            {
                return null;
            }

            if (billingSummary.PastDueAmount <= 0)
            {
                if (riskFromDb.NoticeHistory == null)
                {
                    riskFromDb.InitializeNoticeHistory();
                }
                riskFromDb.NoticeHistory.NopcNoticeDate = null;
                riskFromDb.NoticeHistory.CancellationEffectiveDate = null;
                riskFromDb.SetIsPendingCancellationForNonPayment(false);
                await _riskRepository.SaveChangesAsync();
                return new NopcRiskDto
                {
                    RiskId = request.RiskId,
                    NoticeDate = requestDate,
                    AmountBilled = billingSummary.Billed,
                    AmountPaid = billingSummary.Paid,
                    BalanceDue = billingSummary.Balance,
                    FullAmountDue = billingSummary.PayoffAmount,
                    MinimumAmountDue = billingSummary.AmountNeededToReinstate > 0 ? billingSummary.AmountNeededToReinstate : 0
                };
            }
            else
            {
                return null;
            }
        }
    }
}
