﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedReturnDetailService: ISuspendedReturnDetailService
    {
        private readonly IMapper _mapper;
        private readonly IValidationService _validationService;
        private readonly ISuspendedReturnDetailRepository _suspendedReturnDetailRepository;
        private readonly ISuspendedPayeeRepository _suspendedPayeeRepository;
        private readonly IUserDateService _userDateService;

        public SuspendedReturnDetailService(
            IMapper mapper,
            IValidationService validationService,
            ISuspendedReturnDetailRepository suspendedReturnDetailRepository,
            ISuspendedPayeeRepository suspendedPayeeRepository,
            IUserDateService userDateService
        )
        {
            _mapper = mapper;
            _validationService = validationService;
            _suspendedReturnDetailRepository = suspendedReturnDetailRepository;
            _suspendedPayeeRepository = suspendedPayeeRepository;
            _userDateService = userDateService;
    }

        public async Task<SuspendedReturnDetailDto> CreateReturnDetail(ReturnSuspendedPaymentRequestDto request)
        {
            var suspendedPayee = _mapper.Map<SuspendedPayee>(request.Payee);
            var suspendedReturnDetail = await _suspendedReturnDetailRepository.AddAsync(
                new SuspendedReturnDetail(
                    request.SuspendedPaymentId,
                    suspendedPayee,
                    _userDateService.GetUser()
                ));
            await _suspendedReturnDetailRepository.SaveChangesAsync();

            return _mapper.Map<SuspendedReturnDetailDto>(suspendedReturnDetail);
        }

        public async Task<SuspendedReturnDetailDto> DeleteReturnDetail(Guid suspendedPaymentId)
        {
            var suspendedReturnDetail = await _suspendedReturnDetailRepository.FindAsync(x => x.SuspendedPaymentId == suspendedPaymentId, x => x.Payee);
            var deletedReturnDetail = _suspendedReturnDetailRepository.Remove(suspendedReturnDetail);
            await _suspendedReturnDetailRepository.SaveChangesAsync();

            _suspendedPayeeRepository.Remove(suspendedReturnDetail.Payee);
            await _suspendedPayeeRepository.SaveChangesAsync();

            return _mapper.Map<SuspendedReturnDetailDto>(deletedReturnDetail);
        }

        public async Task<SuspendedReturnDetailDto> UpdateReturnDetail(UpdateReturnSuspendedPaymentRequestDto request)
        {
            //validate request
            var validator = new UpdateReturnSuspendedPaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            //updated detail process
            return await UpdateReturnDetailData(request);
        }

        private async Task<SuspendedReturnDetailDto> UpdateReturnDetailData(UpdateReturnSuspendedPaymentRequestDto request)
        {
            var suspendedReturnDetail = await _suspendedReturnDetailRepository.FindAsync(d => d.SuspendedPaymentId == request.SuspendedPaymentId);
            if (suspendedReturnDetail == null) throw new ParameterException(ExceptionMessages.RETURN_SUSPENDEDPAYMENT_UPDATEDETAIL_STATUSCONFLICT);
            
            _mapper.Map(request, suspendedReturnDetail);

            var updatedDetail = _suspendedReturnDetailRepository.Update(suspendedReturnDetail);
            await _suspendedReturnDetailRepository.SaveChangesAsync();

            return _mapper.Map<SuspendedReturnDetailDto>(updatedDetail);
        }
    }
}
