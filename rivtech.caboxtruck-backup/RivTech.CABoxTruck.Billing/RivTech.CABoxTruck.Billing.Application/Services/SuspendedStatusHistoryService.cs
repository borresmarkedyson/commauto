﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class SuspendedStatusHistoryService : ISuspendedStatusHistoryService
    {
        private readonly ISuspendedStatusHistoryRepository _suspendedStatusHistoryRepository;
        private readonly IUserDateService _userDateService;


        public SuspendedStatusHistoryService(
            ISuspendedStatusHistoryRepository suspendedStatusHistoryRepository,
            IUserDateService userDateService
        )
        {
            _suspendedStatusHistoryRepository = suspendedStatusHistoryRepository;
            _userDateService = userDateService;
        }

        public async Task CreateSuspendedHistory(SuspendedPayment suspendedPayment)
        {
            await _suspendedStatusHistoryRepository.AddAsync(
                new SuspendedStatusHistory(suspendedPayment, _userDateService.GetUser())
            );
            await _suspendedStatusHistoryRepository.SaveChangesAsync();
        }
    }
}
