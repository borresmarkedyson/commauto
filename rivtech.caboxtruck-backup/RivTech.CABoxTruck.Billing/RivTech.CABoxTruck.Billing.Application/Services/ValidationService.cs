﻿using FluentValidation.Results;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ValidationService : IValidationService
    {
        public void FormatValidationErrorsAndThrow(ValidationResult validationResult)
        {
            if (validationResult.IsValid)
            {
                return;
            }

            List<string> errorMessages = new List<string>();
            foreach (var error in validationResult.Errors)
            {
                if (!errorMessages.Contains(error.ErrorMessage))
                {
                    errorMessages.Add(error.ErrorMessage);
                }
            }

            string exceptionMessage;
            if (errorMessages.Count == 1)
            {
                exceptionMessage = errorMessages[0];
            }
            else
            {
                exceptionMessage = "Data validation has failed due to the following:\r\n";
                foreach (var errorMessage in errorMessages)
                {
                    exceptionMessage += $"{errorMessage}\r\n";
                }
            }
            throw new ParameterException(exceptionMessage);
        }
    }
}
