﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.IO;
using System.Net;
using System;
using RivTech.CABoxTruck.Billing.Application.DTO;
using Renci.SshNet;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class FtpService: IFtpService
    {
        private readonly IMapper _mapper;
        public FtpService( IMapper mapper )
        {
            _mapper = mapper;
        }

        public bool DownloadFileDynamicName(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath, string fileNamePattern)
        {
            if (ftpSettings.IsSsh) return DownloadFileDynamicNameSFtp(ftpSettings, dlFileDirectory, dlFilePath, fileNamePattern);
            else return DownloadFileDynamicNameFtp(ftpSettings, dlFileDirectory, dlFilePath, fileNamePattern);
        }

        private bool DownloadFileDynamicNameFtp(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath, string fileNamePattern)
        {
            var regex = new Regex(fileNamePattern, RegexOptions.IgnoreCase);
            var request = (FtpWebRequest)WebRequest.Create($"ftp://{ftpSettings.BaseUrl}{ftpSettings.FilePath}");
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(ftpSettings.Username, ftpSettings.Password);

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (Stream respStream = response.GetResponseStream())
            {
                try
                {
                    StreamReader reader = new StreamReader(respStream);
                    //Read each file name from the response
                    for (string fname = reader.ReadLine(); fname != null; fname = reader.ReadLine())
                    {                        
                        if (regex.IsMatch(fname))
                        {
                            ftpSettings.FileName = fname;
                            return DownloadFileFtp(ftpSettings, dlFileDirectory, dlFilePath);
                        }
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
            return false;
        }

        private bool DownloadFileDynamicNameSFtp(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath, string fileNamePattern)
        {
            var regex = new Regex(fileNamePattern, RegexOptions.IgnoreCase);
            using var sftp = new SftpClient(ftpSettings.BaseUrl, ftpSettings.Port, ftpSettings.Username, ftpSettings.Password);
            try
            {
                sftp.Connect();
                sftp.ChangeDirectory("/");
                foreach (string fname in sftp.ListDirectory(ftpSettings.Directory).Select(s => s.FullName).ToList())
                {
                    if (regex.IsMatch(fname.Replace("/outbound/", "")))
                    {
                        ftpSettings.FileName = fname;
                        DownloadFileSftp(ftpSettings, dlFileDirectory, dlFilePath);
                    }
                }
                sftp.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DownloadFile(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath)
        {
            if (ftpSettings.IsSsh) return DownloadFileSftp(ftpSettings, dlFileDirectory, dlFilePath);
            else return DownloadFileFtp(ftpSettings, dlFileDirectory, dlFilePath);
        }

        private bool DownloadFileFtp(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath)
        {
            var request = (FtpWebRequest)WebRequest.Create($"ftp://{ftpSettings.BaseUrl}{ftpSettings.FilePath}{ftpSettings.FileName}");
            request.Credentials = new NetworkCredential(ftpSettings.Username, ftpSettings.Password);

            Directory.CreateDirectory(dlFileDirectory);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                using (Stream fileStream = File.Create(dlFilePath))
                {
                    responseStream.CopyTo(fileStream);
                }
                response.Close();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                response.Close();
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable) return false;
                throw new Exception(ex.Message);
            }
        }

        private bool DownloadFileSftp(FTPSettingsDto ftpSettings, string dlFileDirectory, string dlFilePath)
        {
            using var sftp = new SftpClient(ftpSettings.BaseUrl, ftpSettings.Port, ftpSettings.Username, ftpSettings.Password);
            try
            {
                Directory.CreateDirectory(dlFileDirectory);
                sftp.Connect();
                using (var file = File.OpenWrite(dlFilePath))
                {
                    sftp.DownloadFile(ftpSettings.FileName, file);
                }
                sftp.Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetFileNamesInServer(FTPSettingsDto ftpSettings, string fileNamePattern)
        {
            if (ftpSettings.IsSsh) return GetFileNamesInServerSftp(ftpSettings, fileNamePattern);
            else return GetFileNamesInServerFtp(ftpSettings, fileNamePattern);            
        }

        private List<string> GetFileNamesInServerFtp(FTPSettingsDto ftpSettings, string fileNamePattern)
        {
            var fileNames = new List<string>();
            var regex = new Regex(fileNamePattern, RegexOptions.IgnoreCase);
            var request = (FtpWebRequest)WebRequest.Create($"ftp://{ftpSettings.BaseUrl}{ftpSettings.FilePath}");
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(ftpSettings.Username, ftpSettings.Password);

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            using (Stream respStream = response.GetResponseStream())
            {
                try
                {
                    StreamReader reader = new StreamReader(respStream);
                    //Read each file name from the response
                    for (string fname = reader.ReadLine(); fname != null; fname = reader.ReadLine())
                    {
                        if (string.IsNullOrEmpty(fileNamePattern) || regex.IsMatch(fname))
                            fileNames.Add(fname);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return fileNames;
        }

        private List<string> GetFileNamesInServerSftp(FTPSettingsDto ftpSettings, string fileNamePattern)
        {
            var fileNames = new List<string>();
            var regex = new Regex(fileNamePattern, RegexOptions.IgnoreCase);
            using var sftp = new SftpClient(ftpSettings.BaseUrl, ftpSettings.Port, ftpSettings.Username, ftpSettings.Password);
            try
            {
                sftp.Connect();
                sftp.ChangeDirectory("/");
                foreach (string fname in sftp.ListDirectory(ftpSettings.Directory).Select(s => s.FullName).ToList())
                {
                    if (string.IsNullOrEmpty(fileNamePattern) || regex.IsMatch(fname.Replace("/outbound/", "")))
                        fileNames.Add(fname);
                }
                sftp.Disconnect();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fileNames;
        }

        public List<string> GetFileNamesInServer(string ftpSettingId)
        {
            FTPSettingsDto ftpSettings = new FTPSettingsDto();
            switch (ftpSettingId)
            {
                case var _ when ftpSettingId == SuspendedSource.Lockbox.Id:
                    ftpSettings = _mapper.Map<FTPSettingsDto>(Service.AppSettings.Lockbox.FTP);
                    break;
                case var _ when ftpSettingId == SuspendedSource.DirectBill.Id:
                    ftpSettings = _mapper.Map<FTPSettingsDto>(Service.AppSettings.DirectBill.FTP);
                    break;
                default:
                    break;
            }

            return GetFileNamesInServer(ftpSettings, "");
        }

        public string DownloadFile(FTPDownloadRequest request)
        {
            FTPSettingsDto ftpSettings;
            string dlFileDirectory;

            switch (request.FTPSettingId)
            {
                case var _ when request.FTPSettingId == SuspendedSource.Lockbox.Id:
                    dlFileDirectory = Service.AppSettings.Lockbox.DLFileDirectory;
                    ftpSettings = _mapper.Map<FTPSettingsDto>(Service.AppSettings.Lockbox.FTP);
                    break;
                case var _ when request.FTPSettingId == SuspendedSource.DirectBill.Id:
                    dlFileDirectory = Service.AppSettings.DirectBill.DLFileDirectory;
                    ftpSettings = _mapper.Map<FTPSettingsDto>(Service.AppSettings.DirectBill.FTP);
                    break;
                default:
                    return $"Invalid ftp settings Id.";
            }
            ftpSettings.FileName = $"{ftpSettings.FilePath}{request.FileName}";
            var dlFilePath = $"{dlFileDirectory}{request.FileName.Replace(request.DLfileExtension,"")}{request.DLfileExtension}";

            if (DownloadFile(ftpSettings, dlFileDirectory, dlFilePath)) return $"File downloaded: {dlFilePath}";
            return $"Download failed.";
        }
    }
}
