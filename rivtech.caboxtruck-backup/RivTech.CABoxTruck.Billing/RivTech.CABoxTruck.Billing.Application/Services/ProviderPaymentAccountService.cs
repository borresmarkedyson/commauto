﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    //class for managing payment provider(authorize.net, pnc, etc) account response
    public class ProviderPaymentAccountService : IProviderPaymentAccountService
    {
        private readonly IMapper _mapper;
        private readonly IAccountFactory _accountFactory;
        private readonly IAccountRequestFactory _accountRequestFactory;
        private readonly IAuthNetAccountService _authNetAccountService;

        public ProviderPaymentAccountService(
            IMapper mapper,
            IAccountFactory accountFactory,
            IAccountRequestFactory accountRequestFactory,
            IAuthNetAccountService authNetAccountService
        )
        {
            _mapper = mapper;
            _accountFactory = accountFactory;
            _accountRequestFactory = accountRequestFactory;
            _authNetAccountService = authNetAccountService;
        }
        
        public dynamic SendToProviderCreateRequest(PaymentAccountRequestDto accountRequest, dynamic providerProfile)
        {
            var accountStrategy = _accountFactory.GetStrategy(accountRequest.AppId);
            var accountRequestStrategy = _accountRequestFactory.GetRequestStrategy(accountRequest.AppId);

            var accountReq = accountRequestStrategy.GenerateCreateRequest(accountRequest, providerProfile, _mapper);
            return accountStrategy.CreateAccount(accountReq);
        }

        public async Task<dynamic> SaveAccount(PaymentAccountDto paymentAccount, PaymentAccountRequestDto accountRequest, dynamic providerResponse)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(accountRequest.AppId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    var response = (createCustomerPaymentProfileResponse)providerResponse;
                    return await _authNetAccountService.SaveAuthNetAccount(accountRequest.RiskId, paymentAccount.Id, response.customerPaymentProfileId);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }

        public string GetAccountDescription(dynamic providerProfile, PaymentAccountRequestDto accountRequest)
        {
            var accountStrategy = _accountFactory.GetStrategy(accountRequest.AppId);
            var accountRequestStrategy = _accountRequestFactory.GetRequestStrategy(accountRequest.AppId);

            var getAccountRequest = accountRequestStrategy.GenerateGetRequest(providerProfile);
            var providerAccountResponse = accountStrategy.GetAccount(getAccountRequest);

            var paymentProviderId = PaymentProvider.GetByAppId(accountRequest.AppId).Id;

            if(accountRequest.InstrumentTypeId == InstrumentType.RecurringCreditCard.Id)
            {
                switch (paymentProviderId)
                {
                    case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                        var response = providerAccountResponse.paymentProfile.payment.Item as creditCardMaskedType;
                        return $"{response.cardType} {response.cardNumber}";
                    default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
                }
            }

            if (accountRequest.InstrumentTypeId == InstrumentType.RecurringECheck.Id)
            {
                switch (paymentProviderId)
                {
                    case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                        var response = providerAccountResponse.paymentProfile.payment.Item as bankAccountMaskedType;
                        return $"{response.bankName} {response.accountNumber}";
                    default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
                }
            }

            throw new ArgumentException(ExceptionMessages.INVALID_INSTRUMENT_TYPE);
        }

        public async Task<dynamic> SendToProviderDeleteRequest(PaymentAccountRequestDto accountRequest, dynamic providerProfile)
        {
            var accountStrategy = _accountFactory.GetStrategy(accountRequest.AppId);
            var accountRequestStrategy = _accountRequestFactory.GetRequestStrategy(accountRequest.AppId);
            //delete provider account data on provider side
            var accountReq = accountRequestStrategy.GenerateDeleteRequest(accountRequest, providerProfile);
            accountStrategy.DeleteAccount(accountReq);
            //delete provider account data on billing side
            return await DeleteProviderAccountData(accountRequest);
        }

        public async Task<dynamic> DeleteProviderAccountData(PaymentAccountRequestDto accountRequest)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(accountRequest.AppId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return await _authNetAccountService.DeleteAuthNetAccount(accountRequest.Id);
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }

        public dynamic SendToProviderUpdateRequest(UpdatePaymentAccountDto updateRequest, dynamic providerProfile)
        {
            var accountStrategy = _accountFactory.GetStrategy(updateRequest.AppId);
            var accountRequestStrategy = _accountRequestFactory.GetRequestStrategy(updateRequest.AppId);
            //get account data from provider
            var getRequest = accountRequestStrategy.GenerateGetRequest(providerProfile, updateRequest);
            var providerAccount = accountStrategy.GetAccount(getRequest);
            //update provider account data on provider side
            var accountReq = accountRequestStrategy.GenerateUpdateRequest(updateRequest, providerAccount);
            return accountStrategy.UpdateAccount(accountReq);
        }
    }
}
