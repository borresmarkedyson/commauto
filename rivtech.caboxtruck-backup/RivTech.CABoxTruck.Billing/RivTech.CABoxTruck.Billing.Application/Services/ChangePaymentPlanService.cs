﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class ChangePaymentPlanService : IChangePaymentPlanService
    {
        private const decimal FULL_PAY_DISCOUNT = 20;
        private List<Transaction> installmentFeeTransactions = new List<Transaction>();

        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IInstallmentScheduleService _installmentScheduleService;
        private readonly IInvoiceService _invoiceService;
        private readonly IPaymentService _paymentService;
        private readonly IInvoiceNumberService _invoiceNumberService;
        private readonly IValidationService _validationService;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IMapper _mapper;
        private readonly IUserDateService _userDateService;
        private readonly IPaymentProfileService _paymentProfileService;

        public ChangePaymentPlanService(
            ITransactionRepository transactionRepository,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IInstallmentScheduleService installmentScheduleService,
            IInvoiceService invoiceService,
            IPaymentService paymentService,
            IInvoiceNumberService invoiceNumberService,
            IValidationService validationService,
            IBillingSummaryService billingSummaryService,
            IMapper mapper,
            IUserDateService userDateService,
            IPaymentProfileService paymentProfileService
        )
        {
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _installmentScheduleService = installmentScheduleService;
            _invoiceService = invoiceService;
            _paymentService = paymentService;
            _invoiceNumberService = invoiceNumberService;
            _validationService = validationService;
            _billingSummaryService = billingSummaryService;
            _mapper = mapper;
            _userDateService = userDateService;
            _paymentProfileService = paymentProfileService;
        }

        public async Task<ChangePayPlanInvoiceDto> ChangePaymentPlan(ChangePaymentPlanRequestDto request)
        {
            var validator = new ChangePaymentPlanRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var risk = await _riskRepository.FindAsync(request.RiskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            if (risk.PaymentPlanId == request.NewPaymentPlanId)
            {
                throw new ParameterException(ExceptionMessages.PAYMENT_PLANS_THE_SAME);
            }

            DateTime currentDate = _userDateService.GetUser().UserDate;

            PostPaymentViewDto paymentDetails = null;
            if (request.PaymentDetails.TotalAmount > 0)
            {
                paymentDetails = await _paymentService.PostChangePaymentPlanPayment(new PaymentRequestDto(request.RiskId, request, risk.ApplicationId, currentDate));
            }

            await VoidInstallmentsAndInvoices(request.RiskId, request.AppId);
            //TODO: uncomment when there are new business rules for states other than Florida
            //await AddSubtractFullPayDiscount(request.RiskId, risk.PaymentPlanId, request.NewPaymentPlanId, user);
            await _installmentScheduleService.SaveInstallmentScheduleAsync(request.RiskId, risk.PolicyNumber, risk.EffectiveDate, request.NewPaymentPlanId, risk.ApplicationId, false, null);
            return await GenerateChangePaymentPlanInvoice(request, risk.ApplicationId, paymentDetails, risk.EffectiveDate);
        }

        private async Task VoidInstallmentsAndInvoices(Guid riskId, string appId)
        {
            var invoices = await _invoiceRepository.GetAllNotVoided(riskId);
            foreach (var invoice in invoices)
            {
                await _invoiceService.VoidInvoice(invoice.InvoiceNumber, appId, false);
            }

            var installmentSchedules = await _installmentRepository.GetAllByRisk(riskId);
            installmentSchedules.ForEach(x => x.SetAsVoid(_userDateService.GetUser()));
        }

        public async Task<List<ChangePaymentPlanRequiredAmountDto>> ComputePaymentsToChangePaymentPlan(Guid riskId)
        {
            var risk = await _riskRepository.FindAsync(riskId);
            if (risk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }
            DateTime effectiveDate = risk.EffectiveDate;
            string appId = risk.ApplicationId;
            string currentPaymentPlanId = risk.PaymentPlanId;

            PremiumFeeAndTax transactionAmounts = await GetTransactionAmounts(riskId);
            PremiumFeeAndTax paidAmounts = await GetPaidAmounts(riskId);

            var changePaymentPlanAmounts = new List<ChangePaymentPlanRequiredAmountDto>();

            var paymentPlanIds = PaymentPlan.GetPaymentPlanIds();
            paymentPlanIds.Remove(currentPaymentPlanId);
            foreach (var paymentPlanId in paymentPlanIds)
            {
                if (paymentPlanId == PaymentPlan.PremiumFinanced.Id)
                {
                    changePaymentPlanAmounts.Add(
                    new ChangePaymentPlanRequiredAmountDto
                    {
                        PaymentPlanId = paymentPlanId,
                        AmountToPay = 0,
                        Details = new ChangePaymentPlanPaymentDetailsDto
                        {
                            TotalAmount = 0,
                            PremiumAmount = 0,
                            FeeAmount = 0,
                            TaxAmount = 0
                        }
                    });
                    continue;
                }

                // require payment of deposit when payment plan is 8-pay
                if (paymentPlanId == PaymentPlan.EightPay.Id)
                {
                    transactionAmounts = await GetTransactionAmounts(riskId, effectiveDate);
                }

                var installments = _installmentScheduleService.GenerateInstallmentScheduleWithoutSaving(riskId, effectiveDate, paymentPlanId, appId, false, null);

                decimal premiumAmount = 0;
                decimal feeAmount = 0;
                decimal taxAmount = 0;
                decimal totalPremium = transactionAmounts.Premium;
                //TODO: uncomment when there are new business rules for states other than Florida
                //if (paymentPlanId == PaymentPlan.FullPay.Id && currentPaymentPlanId != PaymentPlan.FullPay.Id)
                //{
                //    totalPremium -= FULL_PAY_DISCOUNT;
                //}
                //else if (paymentPlanId != PaymentPlan.FullPay.Id && currentPaymentPlanId == PaymentPlan.FullPay.Id)
                //{
                //    totalPremium += FULL_PAY_DISCOUNT;
                //}

                foreach (var installment in installments)
                {
                    if (installment.InvoiceOnDate.Date > _userDateService.GetUser().UserDate.Date
                        && installment.InstallmentTypeId != InstallmentType.Deposit.Id)
                    {
                        break;
                    }

                    if (installment.InstallmentTypeId == InstallmentType.Deposit.Id)
                    {
                        premiumAmount += totalPremium * installment.PercentOfOutstanding;
                        feeAmount += transactionAmounts.Fees;
                        taxAmount += transactionAmounts.Tax;
                    }
                    else if (installment.InstallmentTypeId == InstallmentType.Installment.Id)
                    {
                        premiumAmount += (totalPremium * (1 - installment.PercentOfOutstanding)) / (installments.Count - 1);
                    }
                }

                decimal premiumToPay = premiumAmount - paidAmounts.Premium;
                decimal feeToPay = feeAmount - paidAmounts.Fees;
                decimal taxToPay = taxAmount - paidAmounts.Tax;
                decimal totalAmountToPay = premiumToPay + feeToPay + taxToPay;

                if (totalAmountToPay <= 0)
                {
                    totalAmountToPay = 0;
                    premiumToPay = 0;
                    feeToPay = 0;
                    taxToPay = 0;
                }

                changePaymentPlanAmounts.Add(
                    new ChangePaymentPlanRequiredAmountDto
                    {
                        PaymentPlanId = paymentPlanId,
                        AmountToPay = totalAmountToPay,
                        Details = new ChangePaymentPlanPaymentDetailsDto
                        {
                            TotalAmount = totalAmountToPay,
                            PremiumAmount = premiumToPay,
                            FeeAmount = feeToPay,
                            TaxAmount = taxToPay
                        }
                    });
            }
            return changePaymentPlanAmounts;
        }

        private async Task AddSubtractFullPayDiscount(Guid riskId, string currentPaymentPlanId, string newPaymentPlanId)
        {
            decimal amountToAdd;
            if (currentPaymentPlanId == PaymentPlan.FullPay.Id && newPaymentPlanId != PaymentPlan.FullPay.Id)
            {
                amountToAdd = FULL_PAY_DISCOUNT;
            }
            else if (currentPaymentPlanId != PaymentPlan.FullPay.Id && newPaymentPlanId == PaymentPlan.FullPay.Id)
            {
                amountToAdd = -FULL_PAY_DISCOUNT;
            }
            else
            {
                return;
            }

            var transactionDetails = new List<TransactionDetail> { new TransactionDetail(amountToAdd, null, AmountSubType.FullPayDiscount) };

            await _transactionRepository.AddAsync(new Transaction(riskId, TransactionType.BillPlanChange, transactionDetails, _userDateService.GetUser().UserDate, _userDateService.GetUser(), null));
            await _transactionRepository.SaveChangesAsync();
        }

        private async Task<PremiumFeeAndTax> GetTransactionAmounts(Guid riskId, DateTime? effectiveDate = null)
        {
            if (effectiveDate == null)
            {
                effectiveDate = _userDateService.GetUser().UserDate.Date;
            }

            var transactions = await _transactionRepository.GetAllByRisk(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;
            foreach (var transaction in transactions)
            {
                if (transaction != null && transaction.TransactionDetails != null && effectiveDate >= transaction.EffectiveDate.Date)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                        {
                            premiumAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                        {
                            feeAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                        {
                            feeAmount += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id)
                        {
                            taxAmount += transactionDetail.Amount;
                        }
                    }
                }
            }

            return new PremiumFeeAndTax(premiumAmount, feeAmount, taxAmount);
        }

        private async Task<PremiumFeeAndTax> GetPaidAmounts(Guid riskId)
        {
            var payments = await _paymentRepository.GetAllByRiskId(riskId);

            decimal premiumAmount = 0;
            decimal feeAmount = 0;
            decimal taxAmount = 0;
            foreach (var payment in payments)
            {
                foreach (var transactionDetail in payment.PaymentDetails)
                {
                    if (transactionDetail?.AmountTypeId == AmountType.Premium.Id)
                    {
                        premiumAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.PremiumFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        feeAmount += transactionDetail.Amount;
                    }
                    else if (transactionDetail?.AmountTypeId == AmountType.Tax.Id)
                    {
                        taxAmount += transactionDetail.Amount;
                    }
                }
            }

            return new PremiumFeeAndTax(premiumAmount, feeAmount, taxAmount);
        }

        public async Task<ChangePayPlanInvoiceDto> GenerateChangePaymentPlanInvoice(ChangePaymentPlanRequestDto request, string appId,
            PostPaymentViewDto paymentDetails, DateTime effectiveDate)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            Guid riskId = request.RiskId;
            var risk = await _riskRepository.FindAsync(request.RiskId);

            PremiumFeeAndTax paidAmounts = await GetPaidAmounts(riskId);
            InvoiceSummary totalInvoiceSummary;

            // include deposit invoice when payment plan is 8-pay or effective date is future date
            if (request.NewPaymentPlanId == PaymentPlan.EightPay.Id
                || risk.EffectiveDate.Date > currentDate.Date)
            {
                totalInvoiceSummary = await GetTotalInvoiceDetails(riskId, risk.EffectiveDate);
            }
            else
            {
                totalInvoiceSummary = await GetTotalInvoiceDetails(riskId);
            }

            var installments = await _installmentRepository.GetAllByRisk(riskId);

            decimal premiumAmount = 0;
            decimal premiumFeeAmount = 0;
            decimal transactionFeeAmount = 0;
            decimal taxAmount = 0;

            foreach (var installment in installments)
            {
                if (installment.InvoiceOnDate.Date > currentDate.Date &&
                    installment.InstallmentTypeId != InstallmentType.Deposit.Id)
                {
                    break;
                }

                if (installment.InstallmentTypeId == InstallmentType.Deposit.Id)
                {
                    premiumAmount += totalInvoiceSummary.PremiumAmount * installment.PercentOfOutstanding;
                    premiumFeeAmount += totalInvoiceSummary.FeeAmount;
                    taxAmount += totalInvoiceSummary.TaxAmount;
                }
                else if (installment.InstallmentTypeId == InstallmentType.Installment.Id)
                {
                    premiumAmount += (totalInvoiceSummary.PremiumAmount * (1 - installment.PercentOfOutstanding)) / (installments.Count - 1);
                }
            }

            foreach (var invoiceDetail in totalInvoiceSummary.Details)
            {
                if ((Enumeration.GetEnumerationById<AmountSubType>(invoiceDetail.AmountSubTypeId)).AmountTypeId == AmountType.Premium.Id)
                {
                    invoiceDetail.SetAmount(invoiceDetail.InvoicedAmount * premiumAmount / totalInvoiceSummary.PremiumAmount);
                }
            }

            decimal totalAmountToInvoice = premiumAmount + premiumFeeAmount + taxAmount + transactionFeeAmount;

            //require payment only if effective date is not a future date or the payment plan is 8-pay
            if (totalAmountToInvoice - paidAmounts.TotalAmount > 0 && request.NewPaymentPlanId != PaymentPlan.PremiumFinanced.Id
                && (risk.EffectiveDate.Date <= currentDate.Date || request.NewPaymentPlanId == PaymentPlan.EightPay.Id))
            {
                throw new ParameterException(ExceptionMessages.INSUFFICIENT_AMOUNT_TO_CHANGE_PAYMENT_PLAN);
            }

            DateTime invoiceDueDate = currentDate;

            if (effectiveDate > currentDate.Date)
            {
                invoiceDueDate = effectiveDate;
            }

            Invoice invoice = new Invoice(riskId, await _invoiceNumberService.GenerateInvoiceNumber(appId, risk.PolicyNumber), currentDate,
                invoiceDueDate, -paidAmounts.TotalAmount, totalAmountToInvoice, totalInvoiceSummary.Details, _userDateService.GetUser());

            await _invoiceRepository.AddAsync(invoice);
            await _invoiceRepository.SaveChangesAsync();

            foreach (var installmentFeeTransaction in installmentFeeTransactions)
            {
                if (installmentFeeTransaction != null)
                {
                    installmentFeeTransaction.SetInvoiceId(invoice.Id);
                    await _transactionRepository.SaveChangesAsync();
                }
            }

            var previousInstallments = await _installmentRepository.FindPreviousInstallments(riskId, currentDate);
            previousInstallments.ForEach(x => x.SetAsSkipped());
            if (request.NewPaymentPlanId == PaymentPlan.EightPay.Id)
            {
                installments.Where(x => x.InstallmentTypeId == InstallmentType.Deposit.Id).FirstOrDefault().SetAsSkipped();
            }

            var changePaymentPlanInstallment = new Installment(riskId, currentDate, currentDate, 0, InstallmentType.BillPlanChange, _userDateService.GetUser());
            changePaymentPlanInstallment.SetAsInvoiced(invoice.Id);

            await _installmentRepository.AddAsync(changePaymentPlanInstallment);
            await _installmentRepository.SaveChangesAsync();
            var invoiceFromDb = await _invoiceRepository.GetDetails(invoice.Id);

            await UpdateInvoicedFees(riskId, currentDate);

            var invoiceDto = _mapper.Map<ChangePayPlanInvoiceDto>(invoiceFromDb);

            var billingSummary = await _billingSummaryService.GetBillingSummary(riskId);
            invoiceDto.PayoffAmount = billingSummary.PayoffAmount;

            if (request.InstrumentId != null)
            {
                invoiceDto.PaymentMethod = paymentDetails?.Instrument;
                invoiceDto.AccountNumber = paymentDetails?.Account;
            }
            invoiceDto.PaymentPlan = Enumeration.GetEnumerationById<PaymentPlan>(request.NewPaymentPlanId).Description;
            invoiceDto.Reference = paymentDetails?.Payment?.Reference;
            invoiceDto.PaymentAmount = paymentDetails?.Payment != null ? paymentDetails.Payment.Amount : 0;
            invoiceDto.EmailAddress = request.EmailAddress;
            invoiceDto.IsRecurringPayment = _paymentProfileService.GetEnrolledPaymentProfile(riskId) != null;

            return invoiceDto;
        }

        private async Task<InvoiceSummary> GetTotalInvoiceDetails(Guid riskId, DateTime? transactionEffectiveDate = null)
        {
            if (transactionEffectiveDate == null)
            {
                transactionEffectiveDate = _userDateService.GetUser().UserDate.Date;
            }

            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal totalTax = 0;

            List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();
            var transactionsFromDb = await _transactionRepository.GetAllByRisk(riskId);
            foreach (var transaction in transactionsFromDb)
            {
                if (transaction.TransactionTypeId == TransactionType.Void.Id)
                {
                    continue;
                }
                if (transaction != null && transaction.TransactionDetails != null && transactionEffectiveDate >= transaction.EffectiveDate.Date)
                {
                    foreach (var transactionDetail in transaction.TransactionDetails)
                    {
                        if (transactionDetail.VoidDate != null)
                        {
                            continue;
                        }
                        if (transactionDetail?.AmountSubType != null && transactionDetail.AmountSubType.AmountTypeId == AmountType.Premium.Id)
                        {
                            totalPremium += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType != null && (transactionDetail.AmountSubType.AmountTypeId == AmountType.PremiumFee.Id
                            || transactionDetail.AmountSubType.AmountTypeId == AmountType.TransactionFee.Id))
                        {
                            totalFees += transactionDetail.Amount;
                        }
                        else if (transactionDetail?.AmountSubType != null && transactionDetail.AmountSubType.AmountTypeId == AmountType.Tax.Id)
                        {
                            totalTax += transactionDetail.Amount;
                        }

                        if (invoiceDetails.Any(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId))
                        {
                            var invoiceDetail = invoiceDetails.First(x => x.AmountSubTypeId == transactionDetail.AmountSubTypeId);
                            invoiceDetail.AddAmount(transactionDetail.Amount);
                        }
                        else
                        {
                            invoiceDetails.Add(new InvoiceDetail(transactionDetail.Amount, transactionDetail.AmountSubType));
                        }
                    }
                }
            }

            invoiceDetails.RemoveAll(x => x.InvoicedAmount == 0);

            return new InvoiceSummary
            {
                PremiumAmount = totalPremium,
                FeeAmount = totalFees,
                TaxAmount = totalTax,
                TotalAmount = totalPremium + totalFees + totalTax,
                Details = invoiceDetails
            };
        }

        private async Task UpdateInvoicedFees(Guid riskId, DateTime invoiceDate)
        {
            var transactionsFromDb = await _transactionRepository.GetAllByRisk(riskId);
            foreach (var transaction in transactionsFromDb)
            {
                if (transaction.TransactionDetails == null)
                {
                    continue;
                }
                foreach (var transactionDetail in transaction.TransactionDetails)
                {
                    if ((transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                        || transactionDetail.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                        && transactionDetail.InvoiceDate == null
                        && transaction.EffectiveDate.Date <= invoiceDate.Date)
                    {
                        transactionDetail.SetInvoiceDate(invoiceDate);
                    }
                }
            }
            await _transactionRepository.SaveChangesAsync();
        }
    }
}
