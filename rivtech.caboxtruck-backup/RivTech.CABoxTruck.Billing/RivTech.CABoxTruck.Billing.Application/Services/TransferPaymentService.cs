﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class TransferPaymentService : ITransferPaymentService
    {

        private readonly IMapper _mapper;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IRiskRepository _riskRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IInstallmentRepository _installmentRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentService _paymentService;

        public TransferPaymentService(
            IMapper mapper,
            IPaymentRepository paymentRepository,
            IRiskRepository riskRepository,
            IBillingSummaryService billingSummaryService,
            IValidationService validationService,
            IUserDateService userDateService,
            IInstallmentRepository installmentRepository,
            IInvoiceRepository invoiceRepository,
            IPaymentService paymentService,
            ITransactionRepository transactionRepository
        )
        {
            _mapper = mapper;
            _paymentRepository = paymentRepository;
            _riskRepository = riskRepository;
            _billingSummaryService = billingSummaryService;
            _validationService = validationService;
            _userDateService = userDateService;
            _transactionRepository = transactionRepository;
            _installmentRepository = installmentRepository;
            _invoiceRepository = invoiceRepository;
            _paymentService = paymentService;
        }

        public async Task<TransferPaymentDto> TransferPayment(TransferPaymentRequestDto request)
        {
            var validator = new TransferPaymentRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var paymentToVoid = await _paymentRepository.GetDetailsAsync(request.PaymentId);
            var destinationRisk = await _riskRepository.GetDetailsByPolicyNumberAsync(request.ToPolicyNumber);
            if (paymentToVoid == null || paymentToVoid.RiskId != request.FromRiskId || destinationRisk == null)
            {
                throw new ParameterException(ExceptionMessages.INVALID_REQUEST);
            }

            BillingSummary destinationRiskBillingSummary = new BillingSummary(
                await _riskRepository.GetDetailsAsync(destinationRisk.Id),
                await _transactionRepository.GetAllByRisk(destinationRisk.Id),
                await _installmentRepository.GetAllByRisk(destinationRisk.Id),
                await _invoiceRepository.GetAllByRisk(destinationRisk.Id),
                (List<Payment>)await _paymentRepository.GetAllByRiskId(destinationRisk.Id),
                _userDateService.GetUser().UserDate);

            //RULE: allow overpayment if transfer via suspendedpayment post job
            if (!request.IsSuspendedPaymentPostJob)
            {
                if (destinationRiskBillingSummary.Paid + paymentToVoid.Amount > destinationRiskBillingSummary.TotalPremium)
                {
                    throw new ParameterException(ExceptionMessages.TOTAL_PREMIUM_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION);
                }
            }            

            Payment paymentReversal = Payment.CreateVoidPayment(_userDateService.GetUser(), paymentToVoid, PaymentType.Transfer, request.Comments);
            var savedPaymentReversal = await _paymentRepository.AddAsync(paymentReversal);
            await _paymentRepository.SaveChangesAsync();

            PaymentRequestDto paymentRequest = new PaymentRequestDto() { 
                AppId = "CABT",
                RiskId = destinationRisk.Id,
                PaymentSummary = new PaymentSummaryDto() { 
                    EffectiveDate = paymentToVoid.EffectiveDate,
                    InstrumentId = paymentToVoid.InstrumentId,
                    Amount = paymentToVoid.Amount,
                    Comment = request.Comments
                },
                BillingDetail = new BillingDetailDto() { 
                    BillingAddress = new BillingAddressDto() { 
                        Address = paymentToVoid.PayeeInfo?.Address,
                        City = paymentToVoid.PayeeInfo?.City,
                        State = paymentToVoid.PayeeInfo?.State,
                        Zip = paymentToVoid.PayeeInfo?.ZipCode,
                    },
                    InsuredAgree = true,
                    UwAgree = true
                }
                
            };

            var transferPaymentDto = _mapper.Map<TransferPaymentDto>(savedPaymentReversal);

            if (paymentToVoid.InstrumentId == InstrumentType.WriteOff.Id)
            {
                Payment transferPayment = Payment.CreateTransferPayment(_userDateService.GetUser(), paymentToVoid, destinationRisk.Id, request.Comments);
                await _paymentRepository.AddAsync(transferPayment);
                await _paymentRepository.SaveChangesAsync();

                transferPaymentDto.TransferToPaymentId = transferPayment.Id;
            }
            else
            {
                PostPaymentViewDto postPaymentViewDto = await _paymentService.PostPayment(paymentRequest);

                transferPaymentDto.TransferToPaymentId = postPaymentViewDto.Payment.Id;
            }

            return transferPaymentDto;
        }

        public async Task<List<TransferPaymentDto>> RewriteTransferPayment(Guid currentRiskId, Guid rewriteRiskId)
        {
            var payments = await _paymentRepository.GetAsync(x => x.RiskId == currentRiskId);
            if (payments == null)
            {
                return null;
            }

            var transferredPayment = new List<TransferPaymentDto>();
            var transferRequest = new TransferPaymentRequestDto()
            {
                FromRiskId = currentRiskId,
                ToRiskId = rewriteRiskId
            };

            foreach (var payment in payments)
            {
                transferRequest.PaymentId = payment.Id;
                transferRequest.Comments = payment.Comment;
                transferredPayment.Add(await TransferPayment(transferRequest));
            }

            return transferredPayment;
        }

        #region rewrite transfer exclude fee
        //public async Task<PaymentDto> RewriteTransferPayment(Guid currentRiskId, Guid rewriteRiskId, ApplicationUserDto user)
        //{
        //    var paymentList = new List<Payment>();
        //    var risk = await _riskRepository.FindAsync(currentRiskId);

        //    var payments = _paymentRepository.GetAllByRiskId(currentRiskId).Result.Where(x => x.VoidDate == null);
        //    if (!payments.Any()) return null;

        //    //on old risk, transfer payments
        //    foreach (var payment in payments)
        //    {
        //        if (payment.PaymentDetails.Any(x => x.AmountTypeId == AmountType.TransactionFee.Id))
        //        {   //if transactionFee is paid, void payment > create payment breakdown + transfer nonTF
        //            paymentList.Add(Payment.CreateVoidPayment(user.Id, payment, PaymentType.Void, payment.Comment));
        //            paymentList.AddRange(await CreatePaymentBreakdwon(payment, user.Id));
        //        }
        //        else paymentList.Add(Payment.CreateVoidPayment(user.Id, payment, PaymentType.Transfer, payment.Comment));
        //    }
        //    await _paymentRepository.AddRangeAsync(paymentList);

        //    //on new risk, create one payment (paymentAmount == oldRiskTotalPaymentAmount)
        //    var transferredPayment = await CreateRewriteTransferPayment(currentRiskId, rewriteRiskId, user);
        //    await _paymentRepository.AddAsync(transferredPayment);

        //    await _paymentRepository.SaveChangesAsync();
        //    return _mapper.Map<PaymentDto>(transferredPayment);
        //}
        //private async Task<List<Payment>> CreatePaymentBreakdwon(Payment payment, string userId)
        //{
        //    var paymentDetails = await _paymentDetailRepository.GetTotalPerAmountType(null, payment.Id);

        //    //create paymentWithNoTransactionFee
        //    var paymentNoTF = new Payment(
        //        payment.RiskId,
        //        paymentDetails.Where(x => x.AmountTypeId != AmountType.TransactionFee.Id).Sum(x => x.Amount),
        //        payment.EffectiveDate,
        //        payment.Reference,
        //        Enumeration.GetEnumerationById<InstrumentType>(payment.InstrumentId),
        //        paymentDetails.Where(x => x.AmountTypeId != AmountType.TransactionFee.Id).ToList(),
        //        payment.Comment,
        //        payment.CreatedById,
        //        payment.PaymentTypeId
        //    );
        //    //create transfer for paymentWithNoTransactionFee
        //    var transferPaymentNoTF = Payment.CreateVoidPayment(userId, paymentNoTF, PaymentType.Transfer, paymentNoTF.Comment);

        //    //create paymentWithOnlyTransactionFee
        //    var paymentTF = new Payment(
        //        payment.RiskId,
        //        paymentDetails.Where(x => x.AmountTypeId == AmountType.TransactionFee.Id).Sum(x => x.Amount),
        //        payment.EffectiveDate,
        //        payment.Reference,
        //        Enumeration.GetEnumerationById<InstrumentType>(payment.InstrumentId),
        //        paymentDetails.Where(x => x.AmountTypeId == AmountType.TransactionFee.Id).ToList(),
        //        payment.Comment,
        //        payment.CreatedById,
        //        payment.PaymentTypeId
        //    );

        //    return new List<Payment>() { paymentNoTF, transferPaymentNoTF, paymentTF };
        //}

        //private async Task<Payment> CreateRewriteTransferPayment(Guid currentRiskId, Guid rewriteRiskId, ApplicationUserDto user)
        //{
        //    var paymentDetails = await _paymentDetailRepository.GetTotalPerAmountType(currentRiskId);
        //    //exclude transaction fee
        //    paymentDetails.RemoveAll(x => x.AmountTypeId == AmountType.TransactionFee.Id);

        //    return new Payment(
        //        rewriteRiskId,
        //        paymentDetails.Sum(x => x.Amount),
        //        DateTime.Now.Date,
        //        null,
        //        InstrumentType.Transferred,
        //        paymentDetails,
        //        null,
        //        user.Id,
        //        PaymentType.Transfer.Id
        //    );
        //}
        #endregion               

        #region rewrite transfer excess payment
        //private async Task<PaymentDto> TransferExcessPayment(Guid currentRiskId, Guid rewriteRiskId, ApplicationUserDto user)
        //{
        //    var newRisk = await _riskRepository.FindAsync(rewriteRiskId);

        //    var overPayment = await _paymentRepository.GetTotalPaymentAsync(currentRiskId) -
        //                      await _transactionDetailRepository.GetTotalTransactionAmount(currentRiskId);
        //    if (overPayment <= 0) return null;

        //    var paymentRequest = new PaymentRequestDto() {
        //        AppId = newRisk.ApplicationId,
        //        RiskId = newRisk.Id,
        //        PaymentSummary = new PaymentSummaryDto()
        //        {
        //            EffectiveDate = DateTime.Now.Date,
        //            InstrumentId = InstrumentType.Transferred.Id,
        //            Amount = overPayment,
        //            IsRecurringPayment = false
        //        }
        //    };
        //    //var payment = await _paymentService.PostPayment(paymentRequest, user);
        //    payment.RiskId = currentRiskId;

        //    var paymentReversal = Payment.CreateNegativePayment(user.Id, _mapper.Map<Payment>(payment.Payment), PaymentType.Transfer, null);
        //    await _paymentRepository.AddAsync(paymentReversal);
        //    await _paymentRepository.SaveChangesAsync();

        //    return payment.Payment;
        //}
        #endregion
    }
}
