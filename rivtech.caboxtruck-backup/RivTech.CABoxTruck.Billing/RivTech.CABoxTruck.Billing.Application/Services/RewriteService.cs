﻿using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class RewriteService : IRewriteService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IBindService _bindService;
        private readonly IPolicyCancellationService _cancellationService;
        private readonly IValidationService _validationService;
        private readonly ITransferPaymentService _transferPaymentService;

        public RewriteService(
            IRiskRepository riskRepository,
            IBindService bindService,
            IPolicyCancellationService cancellationService,
            IValidationService validationService,
            ITransferPaymentService transferPaymentService
        )
        {
            _riskRepository = riskRepository;
            _bindService = bindService;
            _cancellationService = cancellationService;
            _validationService = validationService;
            _transferPaymentService = transferPaymentService;
        }

        public async Task<RewriteViewDto> Rewrite(RewriteRequestDto request)
        {
            request = SetRequestFieldValues(request);
            var validationResult = new RewriteRequestValidator().Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);
            var currentRisk = await _riskRepository.FindAsync(request.CurrentRiskId);
            if (currentRisk?.PolicyStatusId != PolicyStatus.InForce.Id)
            {
                throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            }

            var rewriteResult = new RewriteViewDto();
            rewriteResult.Bind = await _bindService.Bind(request.RewriteRiskId, request.BindRequest);
            rewriteResult.Cancellation = await _cancellationService.ProrateAndCancelPolicy(request.CancelRequest);
            rewriteResult.TransferredPayment = await _transferPaymentService.RewriteTransferPayment(request.CurrentRiskId, request.RewriteRiskId);
            return rewriteResult;
        }

        private RewriteRequestDto SetRequestFieldValues(RewriteRequestDto request)
        {
            request.BindRequest.WillPayLater = true;
            request.BindRequest.IsRewrite = true;
            request.CancelRequest.RiskId = request.CurrentRiskId;
            request.CancelRequest.IsManualCancellation = false;
            request.CancelRequest.IsReasonNonPayment = false;

            return request;
        }
    }
}
