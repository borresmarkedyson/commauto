﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class CalendarService: ICalendarService
    {
        public CalendarService()
        {
        }

        public DateTime AddBussinessDay(DateTime date, int numberOfBusinessDay)
        {
            if (numberOfBusinessDay == 0) return date;
            var returnDate = date.Date;

            int i = numberOfBusinessDay > 0 ? 1 : -1;
            do
            {
                returnDate = returnDate.AddDays(numberOfBusinessDay > 0 ? 1 : -1);
                if (!(IsHoliday(returnDate) || IsWeekEnd(returnDate)))
                {
                    i = numberOfBusinessDay > 0
                        ? i += 1 // if count is positve, add days 
                        : i -= 1; // if count is negative, subtract days
                }
            } while ( (numberOfBusinessDay > 0 && i <= numberOfBusinessDay) ||
                      (numberOfBusinessDay < 0 && i >= numberOfBusinessDay) );

            return returnDate.Date;
        }

        public bool IsHoliday(DateTime date)
        {
            return Holiday.GetByDate(date) != null ? true : false;
        }

        public bool IsWeekEnd(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday
                || date.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}
