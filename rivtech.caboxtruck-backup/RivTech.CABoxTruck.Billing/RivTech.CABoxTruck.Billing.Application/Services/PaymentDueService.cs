﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentDueService : IPaymentDueService
    {

        private readonly IRiskRepository _riskRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IUserDateService _userDateService;

        public PaymentDueService(
            IRiskRepository riskRepository,
            IBillingSummaryService billingSummaryService,
            IUserDateService userDateService
        )
        {
            _riskRepository = riskRepository;
            _billingSummaryService = billingSummaryService;
            _userDateService = userDateService;
        }

        public async Task<PaymentDueRisksDto> GetPaymentDueRisks(string appId)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            List<PaymentDueRiskDto> paymentDueRisks = new List<PaymentDueRiskDto>();

            var paymentDueRisksFromDb = await _riskRepository.GetAsync(x => currentDate.Date == x.LatestInvoiceDueDate.Date
                && x.NoticeHistory.NopcNoticeDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id && x.ApplicationId == appId, x => x.NoticeHistory);

            foreach (var risk in paymentDueRisksFromDb)
            {
                var billingSummary = await _billingSummaryService.GetBillingSummary(risk.Id);
                if (billingSummary.Balance > 0)
                {
                    paymentDueRisks.Add(new PaymentDueRiskDto
                    {
                        RiskId = risk.Id
                    });
                }
            }
            return new PaymentDueRisksDto
            {
                PaymentDueRisks = paymentDueRisks,
                Count = paymentDueRisks.Count
            };
        }

        public async Task<PaymentDueRisksDto> GetPaymentPastDueRisks(string appId)
        {
            DateTime currentDate = _userDateService.GetUser().UserDate;
            List<PaymentDueRiskDto> pastDueRisks = new List<PaymentDueRiskDto>();

            var pastDueRisksFromDb = await _riskRepository.GetAsync(x => currentDate.Date > x.LatestInvoiceDueDate.Date
                && x.NoticeHistory.NopcNoticeDate != null && x.PolicyStatusId != PolicyStatus.Cancelled.Id && x.ApplicationId == appId, x => x.NoticeHistory);

            foreach (var risk in pastDueRisksFromDb)
            {
                var billingSummary = await _billingSummaryService.GetBillingSummary(risk.Id);
                if (billingSummary.PastDueAmount > 0)
                {
                    pastDueRisks.Add(new PaymentDueRiskDto
                    {
                        RiskId = risk.Id
                    });
                }
            }
            return new PaymentDueRisksDto
            {
                PaymentDueRisks = pastDueRisks,
                Count = pastDueRisks.Count
            };
        }
    }
}
