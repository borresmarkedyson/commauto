﻿using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class PaymentAccountService : IPaymentAccountService
    {
        private readonly IMapper _mapper;
        private readonly IPaymentAccountRepository _paymentAccountRepository;
        private readonly IPaymentProfileRepository _paymentProfileRepository;
        private readonly IProviderPaymentAccountService _providerPaymentAccountService;
        private readonly IProviderPaymentProfileService _providerPaymentProfileService;
        private readonly IRiskRepository _riskRepository;
        private readonly IValidationService _validationService;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IUserDateService _userDateService;
        private readonly ICustomerRepository _customerRepository;

        public PaymentAccountService(
            IMapper mapper,
            IPaymentAccountRepository paymentAccountRepository,
            IPaymentProfileRepository paymentProfileRepository,
            IProviderPaymentAccountService providerPaymentAccountService,
            IProviderPaymentProfileService providerPaymentProfileService,
            IRiskRepository riskRepository,
            IValidationService validationService,
            IApplicationUserRepository applicationUserRepository,
            IUserDateService userDateService,
            ICustomerRepository customerRepository
        )
        {
            _mapper = mapper;
            _paymentAccountRepository = paymentAccountRepository;
            _paymentProfileRepository = paymentProfileRepository;
            _providerPaymentAccountService = providerPaymentAccountService;
            _providerPaymentProfileService = providerPaymentProfileService;
            _riskRepository = riskRepository;
            _validationService = validationService;
            _applicationUserRepository = applicationUserRepository;
            _userDateService = userDateService;
            _customerRepository = customerRepository;
        }

        public async Task<PaymentAccountDto> CreateAccount(PaymentAccountRequestDto accountRequest)
        {
            var risk = await _riskRepository.FindAsync(accountRequest.RiskId);
            accountRequest.AppId = risk.ApplicationId;
            accountRequest.BillingDetail.InsuredAgree = true;
            accountRequest.BillingDetail.UwAgree = true;
            var validator = new CreatePaymentAccountRequestValidator();
            var validationResult = validator.Validate(accountRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            dynamic providerProfile = await _providerPaymentProfileService.GetProfile(accountRequest.RiskId, accountRequest.AppId);
            if (providerProfile == null)
            {
                throw new ParameterException(ExceptionMessages.PAYMENTPROFILE_DOESNTEXIST);
            }

            var response = _providerPaymentAccountService.SendToProviderCreateRequest(accountRequest, providerProfile);
            var paymentAccount = await CreateAccountData(accountRequest);
            providerProfile = await _providerPaymentAccountService.SaveAccount(paymentAccount, accountRequest, response);

            return await SetAccountDescription(paymentAccount, providerProfile, accountRequest);
        }

        private async Task<PaymentAccountDto> CreateAccountData(PaymentAccountRequestDto accountRequest)
        {
            var profile = await _paymentProfileRepository.FindAsync(p => p.RiskId == accountRequest.RiskId, p => p.PaymentAccounts);

            var paymentAccount = await _paymentAccountRepository.AddAsync(
                new PaymentAccount(
                    profile.Id,
                    accountRequest.InstrumentTypeId,
                    accountRequest.IsDefault,
                    accountRequest.BillingDetail.InsuredAgree,
                    accountRequest.BillingDetail.UwAgree,
                    _userDateService.GetUser(),
                    Enumeration.GetEnumerationById<InstrumentType>(accountRequest.InstrumentTypeId),
                    _mapper.Map<Customer>(accountRequest.BillingDetail.BillingAddress)
                ));
            await _paymentAccountRepository.SaveChangesAsync();

            if (paymentAccount.IsDefault)
            {
                var defaultAccount = profile.PaymentAccounts.Find(p => p.Id != paymentAccount.Id && p.IsDefault);
                if (defaultAccount != null)
                {
                    var defaultAccountDto = _mapper.Map<PaymentAccountDto>(defaultAccount);
                    defaultAccountDto.IsDefault = false;
                    _mapper.Map(defaultAccountDto, defaultAccount);

                    _paymentAccountRepository.Update(defaultAccount);
                    await _paymentAccountRepository.SaveChangesAsync();
                }
            }

            return _mapper.Map<PaymentAccountDto>(paymentAccount);
        }

        public async Task<PaymentAccountDto> SetAccountDescription(PaymentAccountDto updatedAccountDto, dynamic providerProfile, PaymentAccountRequestDto accountRequest)
        {
            updatedAccountDto.Description = _providerPaymentAccountService.GetAccountDescription(providerProfile, accountRequest);

            var updatedAccount = await _paymentAccountRepository.FindAsync(updatedAccountDto.Id);
            _mapper.Map(updatedAccountDto, updatedAccount);

            updatedAccount = _paymentAccountRepository.Update(updatedAccount);
            await _paymentAccountRepository.SaveChangesAsync();
            return _mapper.Map<PaymentAccountDto>(updatedAccount);
        }

        public async Task<PaymentAccountDto> UpdateDefaultAccount(UpdateDefaultAccountRequestDto accountRequest)
        {
            var risk = await _riskRepository.FindAsync(accountRequest.RiskId);
            accountRequest.AppId = risk.ApplicationId;

            var validationResult = new UpdateDefaultPaymentAccountRequestValidator().Validate(accountRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await UpdateDefaultAccountData(accountRequest);
        }

        private async Task<PaymentAccountDto> UpdateDefaultAccountData(UpdateDefaultAccountRequestDto accountRequest)
        {
            var account = await _paymentAccountRepository.FindAsync(accountRequest.Id);
            if (account.IsDefault == accountRequest.IsDefault)
            {
                return _mapper.Map<PaymentAccountDto>(account);
            }

            if (account.IsDefault && !accountRequest.IsDefault)
            {
                throw new ParameterException(ExceptionMessages.UPDATE_PAYMENTACCOUNT_DEFAULTACCOUNT);
            }

            //set current default account to isdefault = false
            var defaultAccount = await _paymentAccountRepository.FindAsync(p => p.PaymentProfileId == account.PaymentProfileId && p.IsDefault);
            var defaultAccountDto = _mapper.Map<PaymentAccountDto>(defaultAccount);
            defaultAccountDto.IsDefault = false;
            _mapper.Map(defaultAccountDto, defaultAccount);
            _paymentAccountRepository.Update(defaultAccount);
            await _paymentAccountRepository.SaveChangesAsync();

            //set new default account 
            var accountDto = _mapper.Map<PaymentAccountDto>(account);
            accountDto.IsDefault = accountRequest.IsDefault;
            _mapper.Map(accountDto, account);
            var updatedAccount = _paymentAccountRepository.Update(account);
            await _paymentAccountRepository.SaveChangesAsync();

            return _mapper.Map<PaymentAccountDto>(updatedAccount);
        }

        public async Task<PaymentAccountDto> UpdateAccount(UpdatePaymentAccountDto updateRequest)
        {
            var risk = await _riskRepository.FindAsync(updateRequest.RiskId);
            if (risk == null) throw new ParameterException(ExceptionMessages.INVALID_RISK_ID);
            updateRequest.AppId = risk.ApplicationId;

            var validationResult = new UpdatePaymentAccountRequestValidator().Validate(updateRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            return await UpdateAccountData(updateRequest);
        }

        private async Task<PaymentAccountDto> UpdateAccountData(UpdatePaymentAccountDto updateRequest)
        {
            //set current default account to isdefault = false
            var account = await _paymentAccountRepository.FindAsync(x => x.Id == updateRequest.Id, x => x.Payer);
            account.Payer.SetEmail(updateRequest.Email);
            account.SetUpdatedDate(_userDateService.GetUser().UserDate);

            var updatedAccount = _paymentAccountRepository.Update(account);
            await _paymentAccountRepository.SaveChangesAsync();
            return _mapper.Map<PaymentAccountDto>(updatedAccount);

            #region update provider
            //var profile = await _paymentProfileRepository.FindAsync(x => x.RiskId == updateRequest.RiskId, x => x.PaymentAccounts.Where(x => x.Id == updateRequest.Id));
            //if (profile == null || profile?.PaymentAccounts == null || profile?.PaymentAccounts[0]?.InstrumentTypeId != InstrumentType.RecurringCreditCard.Id)
            //    throw new ParameterException(ExceptionMessages.PAYMENTACCOUNTID_INVALID);

            //dynamic providerProfile = await _providerPaymentProfileService.GetProfile(updateRequest.RiskId, updateRequest.AppId);
            //if (providerProfile == null) throw new ParameterException(ExceptionMessages.PAYMENTPROFILE_DOESNTEXIST);

            //_providerPaymentAccountService.SendToProviderUpdateRequest(updateRequest, providerProfile);
            //profile.PaymentAccounts[0].SetUpdatedDate(_userDateService.GetUser().UserDate);
            //var updatedAccount = _paymentAccountRepository.Update(profile.PaymentAccounts[0]);
            //await _paymentAccountRepository.SaveChangesAsync();
            //return _mapper.Map<PaymentAccountDto>(updatedAccount);
            #endregion
        }

        public async Task<PaymentAccountDto> DeleteAccount(PaymentAccountRequestDto accountRequest)
        {
            //populate account request
            var risk = await _riskRepository.FindAsync(accountRequest.RiskId);
            var account = await _paymentAccountRepository.FindAsync(x => x.Id == accountRequest.Id, x => x.Payer);
            accountRequest.AppId = risk.ApplicationId;
            _mapper.Map(await _paymentAccountRepository.FindAsync(accountRequest.Id), accountRequest);

            var validator = new DeletePaymentAccountRequestValidator();
            var validationResult = validator.Validate(accountRequest);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            var providerProfile = await _providerPaymentProfileService.GetProfile(accountRequest.RiskId, accountRequest.AppId);
            await _providerPaymentAccountService.SendToProviderDeleteRequest(accountRequest, providerProfile);
            return await DeleteAccountData(account);
        }

        private async Task<PaymentAccountDto> DeleteAccountData(PaymentAccount account)
        {
            var deletedPaymentAccount = _paymentAccountRepository.Remove(account);
            await _paymentAccountRepository.SaveChangesAsync();

            _customerRepository.Remove(account.Payer);
            await _customerRepository.SaveChangesAsync();
            return _mapper.Map<PaymentAccountDto>(deletedPaymentAccount);
        }

        public async Task<List<PaymentAccountDto>> GetAccounts(Guid riskId)
        {
            var profile = await _paymentProfileRepository.FindAsync(x => x.RiskId == riskId, x => x.PaymentAccounts);
            return _mapper.Map<List<PaymentAccountDto>>(profile.PaymentAccounts);
        }
    }
}
