﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling.Validators;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Persistences;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.Services
{
    public class FinalCancellationService : IFinalCancellationService
    {
        private readonly IRiskRepository _riskRepository;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IValidationService _validationService;
        private readonly IUserDateService _userDateService;

        public FinalCancellationService(
            IRiskRepository riskRepository,
            IBillingSummaryService billingSummaryService,
            IValidationService validationService,
            IUserDateService userDateService
        )
        {
            _riskRepository = riskRepository;
            _billingSummaryService = billingSummaryService;
            _validationService = validationService;
            _userDateService = userDateService;
        }

        public async Task<List<FinalCancellationRiskDto>> GetFinalCancellationRisks(string appId)
        {
            DateTime requestDate = _userDateService.GetUser().UserDate;
            List<FinalCancellationRiskDto> finalCancellationRisks = new List<FinalCancellationRiskDto>();
            var finalCancellationRisksFromDb = await _riskRepository.GetAsync(x => x.NoticeHistory.CancellationEffectiveDate != null
                && requestDate.Date >= x.NoticeHistory.CancellationEffectiveDate.Value.Date
                && x.NoticeHistory.CancellationProcessDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == appId, x => x.NoticeHistory);
            foreach (var risk in finalCancellationRisksFromDb)
            {
                var billingSummary = await _billingSummaryService.GetBillingSummary(risk.Id);
                finalCancellationRisks.Add(new FinalCancellationRiskDto
                {
                    RiskId = risk.Id,
                    BalanceDue = billingSummary.Balance,
                    RequestDate = requestDate
                });
                if (risk.NoticeHistory == null)
                {
                    risk.InitializeNoticeHistory();
                }
                risk.NoticeHistory.CancellationProcessDate = requestDate;
            }
            await _riskRepository.SaveChangesAsync();
            return finalCancellationRisks;
        }

        public async Task<FinalCancellationRiskDto> SetFinalCancellation(NoticeRequestDto request)
        {
            var validator = new NoticeRequestValidator();
            var validationResult = validator.Validate(request);
            _validationService.FormatValidationErrorsAndThrow(validationResult);

            DateTime requestDate = _userDateService.GetUser().UserDate;

            var riskFromDb = await _riskRepository.FindAsync(x => x.Id == request.RiskId && x.NoticeHistory.CancellationEffectiveDate != null
                && requestDate.Date >= x.NoticeHistory.CancellationEffectiveDate.Value.Date
                && x.NoticeHistory.CancellationProcessDate == null && x.PolicyStatusId != PolicyStatus.Cancelled.Id
                && x.ApplicationId == request.AppId, x => x.NoticeHistory);

            if (riskFromDb == null)
            {
                return null;
            }

            var billingSummary = await _billingSummaryService.GetBillingSummary(request.RiskId);
            if (riskFromDb.NoticeHistory == null)
            {
                riskFromDb.InitializeNoticeHistory();
            }
            riskFromDb.NoticeHistory.CancellationProcessDate = requestDate;

            await _riskRepository.SaveChangesAsync();
            return new FinalCancellationRiskDto
            {
                RiskId = request.RiskId,
                BalanceDue = billingSummary.Balance,
                RequestDate = requestDate
            };
        }
    }
}
