﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.DomainEvents
{
    public class DomainEventDispatcher : IDomainEventDispatcher
    {
        private ServiceProvider _provider;

        public DomainEventDispatcher(ServiceProvider provider)
        {
            _provider = provider;
        }

        public void Dispatch<TEvent>(TEvent domainEvent) where TEvent : BaseDomainEvent
        {
            var services = _provider.GetServices<BaseDomainEventHandler<TEvent>>();
            foreach(BaseDomainEventHandler<TEvent> s in services)
            {
                s.Handle(domainEvent);
            }
        }

        public async Task DispatchAsync<TEvent>(TEvent domainEvent) where TEvent : BaseDomainEvent
        {
            var services = _provider.GetServices<BaseDomainEventHandler<TEvent>>();
            foreach (BaseDomainEventHandler<TEvent> s in services)
            {
                await s.HandleAsync(domainEvent);
            }
        }
    }
}
