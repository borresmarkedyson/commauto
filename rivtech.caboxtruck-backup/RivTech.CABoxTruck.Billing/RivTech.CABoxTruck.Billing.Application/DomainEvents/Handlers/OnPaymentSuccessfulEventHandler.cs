﻿using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Application.DomainEvents.Handlers
{
    public class OnPaymentSuccessfulEventHandler : BaseDomainEventHandler<OnPaymentSuccesful>
    {
        private IHttpClientFactory _httpClientFactory;

        public OnPaymentSuccessfulEventHandler(
            IHttpClientFactory httpClientFactory
        ) {
            _httpClientFactory = httpClientFactory;
        }

        public override async Task HandleAsync(OnPaymentSuccesful domainEvent)
        {
            try
            {
                using (var client = _httpClientFactory.CreateClient(UtilConstant.HTTP_CLIENT_COMM_AUTO_BILLING))
                {
                    var url = "api/paymentposting/GetPaymentAmountDistribution?"
                                + "&policyDetailID=" + domainEvent.PolicyDetailID
                                + "&paymentAmount=" + domainEvent.Invoice.AmountDue;
                    var response = await client.GetAsync(url);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                    var stringJson = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(stringJson);
                    json.Payer = domainEvent.Customer.Payer;
                    json.PaymentDate = domainEvent.DateTime;
                    json.PostedBy = domainEvent.PostedBy;
                    json.CheckNumber = domainEvent.ReferenceNumber;
                    json.Comment = domainEvent.ReferenceNumber;
                    json.PaymentInstrumentTypeId = int.Parse(PaymentInstrumentType.FromPayment(domainEvent.Payment).Id);
                    json.Email = domainEvent.Payment.Email;
                    stringJson = JsonConvert.SerializeObject(json);
                    var stringContent = new StringContent(stringJson, Encoding.UTF8, "application/json");

                    response = await client.PostAsync("api/payment", stringContent);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.ReasonPhrase);
                    }
                }
            }
            catch(Exception e)
            {
            }
        }
   }
}
