﻿using RivTech.CABoxTruck.Billing.Infrastructure.Encryption;
using System;
using System.IO;

namespace FileDecryptor
{
    class Program
    {
        static void Main(string[] args)
        {
            FileEncryptionService fileEncryptionService = new FileEncryptionService();

            string encryptedFilePath = GetEncryptedFile();
            string publicKeyFilePath = GetPublicKeyFile();
            string outputDirectory = GetOutputDirectory();
            fileEncryptionService.ImportPublicKey(publicKeyFilePath);
            fileEncryptionService.GetPrivateKey();
            fileEncryptionService.DecryptFile(encryptedFilePath, outputDirectory);
        }

        private static string GetEncryptedFile()
        {
            while (true)
            {
                Console.WriteLine("Please enter the path of the encrypted file:");
                string fileName = Console.ReadLine();

                if (File.Exists(fileName) && fileName.EndsWith("bin"))
                {
                    return fileName;
                }
                else
                {
                    Console.WriteLine("Invalid file!");
                }
            }
        }

        private static string GetPublicKeyFile()
        {
            while (true)
            {
                Console.WriteLine("Please enter the path of the public key:");
                string fileName = Console.ReadLine();

                if (File.Exists(fileName) && fileName.EndsWith("txt"))
                {
                    return fileName;
                }
                else
                {
                    Console.WriteLine("Invalid file!");
                }
            }
        }

        private static string GetOutputDirectory()
        {
            while (true)
            {
                Console.WriteLine("Please enter the folder where the decrypted file will be generated:");
                string outputDirectory = Console.ReadLine();

                try
                {
                    Directory.CreateDirectory(outputDirectory);

                    if (Directory.Exists(outputDirectory))
                    {
                        return outputDirectory;
                    }
                    else
                    {
                        Console.WriteLine("Invalid folder!");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
