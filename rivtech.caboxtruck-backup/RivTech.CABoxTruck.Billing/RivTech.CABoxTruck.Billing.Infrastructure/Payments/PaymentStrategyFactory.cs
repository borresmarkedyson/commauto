﻿using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments
{
    public class PaymentStrategyFactory : IPaymentStrategyFactory
    {
        private readonly IPaymentStrategy _authNetPaymentStrategy = new AuthNetPaymentStrategy();

        public IPaymentStrategy GetStrategy(string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return _authNetPaymentStrategy;
                default: throw new PaymentParameterException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }
    }
}

