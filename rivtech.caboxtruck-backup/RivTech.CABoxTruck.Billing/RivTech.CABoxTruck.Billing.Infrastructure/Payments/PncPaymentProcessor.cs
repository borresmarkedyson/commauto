﻿using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc.ResponseModel;
using RivTech.CABoxTruck.Billing.Infrastructure.Security;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments
{
    public class PncPaymentProcessor : IPaymentProcessor
    {
        private IHttpClientFactory _httpClientFactory;

        public PncPaymentProcessor(
            IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory
                                 ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }

        public async Task<PaymentResult> PayAsync(
            CustomerDetails customer,
            InvoiceDetails invoice,
            PaymentCCEFT payment
        )
        {
            HttpClient httpClient = _httpClientFactory.CreateClient(
                ApiConstants.PNC_HTTP_CLIENT);
            string jsonContent = PaymentRequestCreator
                .Generate(customer, invoice, payment);
            var content = new StringContent(
                jsonContent,
                Encoding.UTF8,
                "application/json");
            HttpResponseMessage response = await httpClient
                .PutAsync("api/Payment", content);

            if (!response.IsSuccessStatusCode)
            {
                var maskContent = JsonMasker.Mask(new[] { "CcAccountNumber", "CcCVV", "AchRoutingNumber", "AchAccountNumber" }, jsonContent);
                return new PaymentResult(payment)
                {
                    Success = false,
                    Message = "(HTTP) " + response.ReasonPhrase
                };
            }

            var responseResult = await response.Content.ReadAsAsync<PncResponse>();

            if (!responseResult.Success)
            {
                var maskContent =  JsonMasker.Mask(new[] { "CcAccountNumber", "CcCVV", "AchRoutingNumber", "AchAccountNumber" }, jsonContent);
                return new PaymentResult(payment)
                {
                    Success = false,
                    Message = responseResult.Message,
                    Errors = responseResult.Errors.ToList().Select(x => FormatError(x)).ToArray()
                };
            }

            return new PaymentResult(payment)
            {
                Success = true,
                Message = "Payment Successful.",
                ReferenceNumber = responseResult.Result.Payments.First().TransactionID.ToString()
            };

        }

        private string FormatError(string error)
        {
            try
            {
                if (error.Contains("Response Code Declined", StringComparison.OrdinalIgnoreCase))
                    return "Payment Declined";

                var property = error.Substring(0, error.IndexOf(":"));
                property = property.Substring(property.LastIndexOf(".") + 1);
                property = ReplacePropertyName(property);
                CultureInfo.CurrentCulture.TextInfo.ToTitleCase(property.ToLower());
                var message = error.Substring(error.IndexOf(":") + 1) + ".";
                return $"{property}: {message}";
            }
            catch(Exception e)
            {
                return error;
            }
        }

        private string ReplacePropertyName(string prop)
        {
            switch (prop.ToLower())
            {
                case "ccaccountnumber": return "Credit Card Number";
                case "cccvv": return "CVV";
                case "zip": return "Zip Code";
                default: return prop;
            }
        }

    }

}
