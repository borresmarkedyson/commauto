﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Infrastructure.Exceptions;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments
{
    public class AuthNetPaymentStrategy : IPaymentWithValidationStrategy
    {
        private PaymentProviderResponseDto ExecuteTransaction(dynamic paymentRequest)
        {
            try
            {
                var controller = new createTransactionController(paymentRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    if (errorresponse != null)
                    {
                        throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                    }
                    else
                    {
                        throw new AuthNetException(ExceptionMessages.PAYMENT_GATEWAY_ERROR);
                    }
                }

                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.errors != null)
                    {
                        throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetTransactionResponseMessages(response.transactionResponse.errors.ToList())));
                    }
                    return new PaymentProviderResponseDto(response.transactionResponse);
                }
                else
                {
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetTransactionResponseMessages(response.transactionResponse.errors.ToList())));
                    }
                    else
                    {
                        throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));
                    }
                }
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        private void ThrowErr()
        {
            throw new SocketException();
        }

        public PaymentProviderResponseDto PostPayment(dynamic paymentRequest)
        {
            return ExecuteTransaction(paymentRequest);
        }

        public string AuthorizePayment(dynamic paymentRequest)
        {
            return ExecuteTransaction(paymentRequest).ReferenceId;
        }

        public PaymentProviderResponseDto CaptureAuthorizedPayment(dynamic paymentRequest)
        {
            return ExecuteTransaction(paymentRequest);
        }

        public string PostPaymentProfile(dynamic paymentRequest)
        {
            try
            {
                var controller = new createTransactionController((createTransactionRequest)paymentRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response.transactionResponse.transId;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }
    }
}
