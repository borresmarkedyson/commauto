﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.Infrastructure.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public class PncCachedTokenProvider : ITokenProvider
    {
        TokenProviderSettings _settings;
        IMemoryCache _memoryCache;
        IHttpClientFactory _httpClientFactory;
        private string _isoDate;

        public PncCachedTokenProvider(
            TokenProviderSettings tokenRequestSettings,
            IHttpClientFactory httpClientFactory,
            IMemoryCache memoryCache
        )
        {
            _memoryCache = memoryCache
                ?? throw new ArgumentNullException(nameof(memoryCache));
            _httpClientFactory = httpClientFactory
                ?? throw new ArgumentNullException(nameof(httpClientFactory));

            _settings = tokenRequestSettings;
        }

        private string Date => DateTime.Now.Date.AddDays(1).AddMinutes(-5).ToString("O");

        public string GetToken()
        {
            if (!_memoryCache.TryGetValue(
                ApiConstants.PNC_TOKEN_CACHE_KEY,
                out string tokenCache)
            )
            {
                // fetch the value from the source
                tokenCache = GenerateToken();

                // store in the cache
                _memoryCache.Set(
                    ApiConstants.PNC_TOKEN_CACHE_KEY,
                    tokenCache,
                    new MemoryCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromHours(1)));
            }

            return tokenCache;
        }
        private string GenerateToken()
        {
            var request = new AuthRequest
            {
                APIKey = new Guid(_settings.ApiKey),
                CreationDate = Date,
                grant_type = "client_credentials",
                Role = _settings.Role,
                Signature = Signature
            };

            var jsonRequest = JsonConvert.SerializeObject(request);
            var content = new StringContent(
                jsonRequest.ToString(),
                Encoding.UTF8,
                "application/json");
            var httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri(_settings.BaseUrl);
            HttpResponseMessage httpResponse = httpClient
                .PostAsync("Api/Auth/AccessToken", content).Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            if (!httpResponse.IsSuccessStatusCode)
                throw new Exception(stringResponse);

            var response = httpResponse.Content.ReadAsAsync<AuthResponse>().Result;

            // store in the cache
            _memoryCache.Set(
                ApiConstants.PNC_TOKEN_CACHE_KEY,
                response.access_token,
                new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromHours(1)));

            return response.access_token;
        }
        private string Signature
        {
            get
            {
                var keyPair = (AsymmetricCipherKeyPair)new PemReader(
                    new StringReader(_settings.PrivateKey),
                    new PasswordFinder(_settings.PassPhrase)).ReadObject();
                var rsaParams = DotNetUtilities.ToRSAParameters(
                    (RsaPrivateCrtKeyParameters)keyPair.Private);
                var csp = new RSACryptoServiceProvider();
                csp.ImportParameters(rsaParams);

                var messageText = string.Concat(_settings.Secret, Date);
                var bMessage = Encoding.UTF8.GetBytes(messageText);
                var signature = csp.SignData(bMessage, _settings.HashAlgorithm);
                return Convert.ToBase64String(signature);
            }
        }
    }
}
