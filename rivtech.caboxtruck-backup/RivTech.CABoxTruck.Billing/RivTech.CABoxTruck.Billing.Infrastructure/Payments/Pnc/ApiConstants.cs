﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public class ApiConstants
    {
        public static readonly string PNC_HTTP_POST_PAYMENT_PROCESS = "processPaymentUrl";
        public static readonly string PNC_TOKEN_CACHE_KEY = "PNC_TOKEN_CACHE_KEY";
        public static readonly string PNC_HTTP_CLIENT = "HTTP_CLIENT_PNC";
      
    }
}
