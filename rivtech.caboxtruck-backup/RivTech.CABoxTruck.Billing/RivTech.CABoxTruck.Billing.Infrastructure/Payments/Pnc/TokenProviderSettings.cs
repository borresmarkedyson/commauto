﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public class TokenProviderSettings
    {
        public string PrivateKey { get; set; }
        public string ApiKey { get; set; }
        public string Role { get; set; }
        public string Secret { get; set; }
        public string BaseUrl { get; set; } 
        public string HashAlgorithm { get; set; }
        public string PassPhrase { get; set; }
    }
}
