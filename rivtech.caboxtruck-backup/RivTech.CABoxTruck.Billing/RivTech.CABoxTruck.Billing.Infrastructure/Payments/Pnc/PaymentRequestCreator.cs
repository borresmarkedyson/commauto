﻿using System;
using System.Dynamic;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments
{
    internal class PaymentRequestCreator
    {
        internal static string Generate(CustomerDetails customer, InvoiceDetails invoice, PaymentCCEFT payment)
        {
            dynamic json = new ExpandoObject();
            json.PaymentDate = DateTime.UtcNow.ToString("O");
            json.CheckForDuplicate = false;
            json.Customer = new
            {
                CompanyName = customer.Payer,
                customer.Email,
                //customer.FirstName,
                //customer.LastName,
                //MiddleName = "Middlename"
            };
            json.InvoicePayments = new dynamic[] {
                new
                {
                    Amount = invoice.AmountDue,
                    //invoice.AmountDue,
                    Invoice = new
                    {
                        ReferenceNumber = invoice.ReferenceNumber,
                        //AmountDue = invoice.AmountDue,
                        //PaymentsPostedAsOfDate = payment.DueDate.ToString("O"),
                        //Status = "Active"
                    }
                }
            };

            if (payment is CreditCardPayment)
            {
                var cc = payment as CreditCardPayment;
                json.PaymentAccount = new
                {
                    AccountType = "CreditCard",
                    CcAccountNumber = cc.Number,
                    Address = customer.Address.Street,
                    City = customer.Address.City,
                    State = customer.Address.State,
                    Zip = customer.Address.Zipcode,
                    CcCardType = cc.CardType.ToString(),
                    CcCVV = cc.Cvv,
                    CcExpirationDate = cc.ExpirationDate.ToString("MM/yyyy"),
                    Name = customer.Payer,
                    //CcRememberMe = true,
                    //SingleUse = true,
                };
            }
            else if(payment is AchPayment)
            {
                var ft = payment as AchPayment;
                json.PaymentAccount = new
                {
                    AccountType = "ACH",
                    AchRoutingNumber = ft.RoutingNumber,
                    AchAccountNumber = ft.AccountNumber,
                    AchType = ft.Type,
                    AchUsageType = ft.UsageType,
                    Name = customer.Payer,
                    //AchBankName = ft.BankName,
                    //AchRememberMe = true,
                    //SingleUse = true,
                };
            }
            else
            {
                throw new InvalidOperationException("Invalid type of payment.");
            }
            return JsonConvert.SerializeObject(json);
        }
    }
}