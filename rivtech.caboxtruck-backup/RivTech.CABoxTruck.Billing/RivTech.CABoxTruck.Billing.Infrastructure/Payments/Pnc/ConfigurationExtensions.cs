﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public static class ConfigurationExtensions
    {
        const string PNC_SECTION = "Pnc";
        public static string GetPncSettings(this IConfiguration config, string key)
        {
            return config.GetSection(PNC_SECTION)[key];
        }
    }
}
