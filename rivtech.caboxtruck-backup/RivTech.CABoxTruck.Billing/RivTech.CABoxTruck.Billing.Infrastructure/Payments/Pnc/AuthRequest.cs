﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public class AuthRequest
    {
        public Guid APIKey { get; set; }
        public string CreationDate { get; set; }
        public string Signature { get; set; }
        public string grant_type { get; set; }
        public string Role { get; set; }
    }
}
