﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc.ResponseModel
{
    internal class PncResponse
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public string[] Errors { get; set; }
        public PncResponseResult Result { get; set; }
    }
}
