﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc.ResponseModel
{
    public class PncReponseResultPayment
    {
        public long TransactionID { get; set; }
    }
}
