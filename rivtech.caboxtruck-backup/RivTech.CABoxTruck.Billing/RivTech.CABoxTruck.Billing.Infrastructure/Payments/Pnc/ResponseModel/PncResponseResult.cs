﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc.ResponseModel
{
    public class PncResponseResult
    {
        public PncReponseResultPayment[] Payments { get; set; }
    }
}
