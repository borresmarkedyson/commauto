﻿namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public interface ITokenProvider
    {
        string GetToken();
    }
}