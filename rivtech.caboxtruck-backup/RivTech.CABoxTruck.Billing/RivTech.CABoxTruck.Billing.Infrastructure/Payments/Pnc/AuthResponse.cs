﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc
{
    public class AuthResponse
    {
        // contains the access token granted to the client
        public string access_token { get; set; }
        // contains the token type, will always be "Bearer"
        public string token_type { get; set; }
        public string Message { get; set; }
    }
}
