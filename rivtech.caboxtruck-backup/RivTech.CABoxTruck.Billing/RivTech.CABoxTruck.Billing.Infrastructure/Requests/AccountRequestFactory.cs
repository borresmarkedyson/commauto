﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AccountRequestFactory : IAccountRequestFactory
    {
        private readonly IAccountRequestStrategy _authNetAccountRequestStrategy = new AuthNetAccountRequestStrategy();

        public IAccountRequestStrategy GetRequestStrategy(string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id: 
                    return _authNetAccountRequestStrategy;
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }
    }
}
