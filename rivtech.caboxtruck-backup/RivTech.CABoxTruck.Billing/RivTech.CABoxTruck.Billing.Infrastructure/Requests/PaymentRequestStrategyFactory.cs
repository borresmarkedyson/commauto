﻿using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class PaymentRequestStrategyFactory : IPaymentRequestStrategyFactory
    {
        private readonly IPaymentRequestStrategy _authNetPaymentRequestStrategy = new AuthNetPaymentRequestStrategy();

        public IPaymentRequestStrategy GetRequestStrategy(string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return _authNetPaymentRequestStrategy;
                default: throw new PaymentParameterException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }
    }
}
