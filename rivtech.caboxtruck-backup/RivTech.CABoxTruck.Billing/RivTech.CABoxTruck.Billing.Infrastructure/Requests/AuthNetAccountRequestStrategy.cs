﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AuthNetAccountRequestStrategy : IAccountRequestStrategy
    {
        public dynamic GenerateCreateRequest(PaymentAccountRequestDto accountRequest,dynamic providerPaymentProfile, IMapper mapper)
        {
            var profile = (AuthNetProfileDto)providerPaymentProfile;
            var paymentAccount = new paymentType();
            switch (accountRequest.InstrumentTypeId)
            {
                case var _ when accountRequest.InstrumentTypeId == InstrumentType.RecurringCreditCard.Id:
                    paymentAccount.Item = mapper.Map<creditCardType>(accountRequest.BillingDetail.CreditCard);
                    break;
                case var _ when accountRequest.InstrumentTypeId == InstrumentType.RecurringECheck.Id:
                    var bankAccount = mapper.Map<bankAccountType>(accountRequest.BillingDetail.BankAccount);
                    bankAccount.echeckType = echeckTypeEnum.WEB;
                    paymentAccount.Item = bankAccount;
                    break;
                default: throw new ParameterException(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            };

            var paymentProfile = new customerPaymentProfileType()
            {
                billTo = mapper.Map<customerAddressType>(accountRequest.BillingDetail.BillingAddress),
                payment = paymentAccount
            };
            
            var validationMode = (validationModeEnum)Enum.Parse(typeof(validationModeEnum), Service.AppSettings.AuthorizeNet.ValidationMode);
            return new createCustomerPaymentProfileRequest
            {
                customerProfileId = profile.CustomerProfileId,
                paymentProfile = paymentProfile,
                validationMode = validationMode
            };
        }

        public dynamic GenerateGetRequest(dynamic providerPaymentProfile, UpdatePaymentAccountDto updateRequest = null)
        {
            var profile = (AuthNetProfileDto)providerPaymentProfile;

            return new getCustomerPaymentProfileRequest()
            {
                customerProfileId = profile.CustomerProfileId,
                customerPaymentProfileId = updateRequest == null ? profile.AuthNetAccount[0].CustomerPaymentId :
                    profile.AuthNetAccount.Where(x => x.PaymentAccountId == updateRequest.Id).FirstOrDefault().CustomerPaymentId
            };
        }

        public dynamic GenerateDeleteRequest(PaymentAccountRequestDto accountRequest, dynamic providerPaymentProfile)
        {
            var profile = (AuthNetProfileDto)providerPaymentProfile;
            var account = profile.AuthNetAccount.Where(a => a.PaymentAccountId == accountRequest.Id).FirstOrDefault();

            return new deleteCustomerPaymentProfileRequest
            {
                customerProfileId = profile.CustomerProfileId,
                customerPaymentProfileId = account.CustomerPaymentId
            };
        }

        public dynamic GenerateUpdateRequest(UpdatePaymentAccountDto updateRequest, dynamic providerProfile)
        {
            var profile = (getCustomerPaymentProfileResponse)providerProfile;
            var profileCC = (creditCardMaskedType)profile.paymentProfile.payment.Item;
            var creditCard = new creditCardType
            {
                cardNumber = profileCC.cardNumber,
                //expirationDate = updateRequest.ExpirationDate
            };

            return new updateCustomerPaymentProfileRequest
            {
                customerProfileId = profile.paymentProfile.customerProfileId,
                paymentProfile = new customerPaymentProfileExType
                {
                    payment = new paymentType { Item = creditCard },
                    customerPaymentProfileId = profile.paymentProfile.customerPaymentProfileId
                }
            };
        }
    }
}
