﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AuthNetPaymentRequestStrategy : IPaymentWithValidationRequestStrategy
    {
        public dynamic GeneratePostRequest(PaymentRequestDto paymentRequest, IMapper mapper)
        {
            var paymentSummary = paymentRequest.PaymentSummary;
            var billingDetail = paymentRequest.BillingDetail;

            var transactionRequest = new transactionRequestType()
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
                amount = paymentSummary.Amount,
                billTo = mapper.Map<customerAddressType>(billingDetail.BillingAddress)
            };

            var paymentType = new paymentType();
            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                    paymentType.Item = mapper.Map<creditCardType>(billingDetail.CreditCard);
                    break;
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    var bankAccount = mapper.Map<bankAccountType>(billingDetail.BankAccount);
                    bankAccount.echeckType = echeckTypeEnum.WEB;
                    paymentType.Item = bankAccount;
                    break;
            };
            transactionRequest.payment = paymentType;

            return new createTransactionRequest { transactionRequest = transactionRequest };
        }

        public dynamic CreateAuthorizeAmountRequest(PaymentRequestDto paymentRequest, IMapper mapper)
        {
            var paymentSummary = paymentRequest.PaymentSummary;
            var billingDetail = paymentRequest.BillingDetail;

            var transactionRequest = new transactionRequestType()
            {
                transactionType = transactionTypeEnum.authOnlyTransaction.ToString(),
                amount = paymentSummary.Amount,
                billTo = mapper.Map<customerAddressType>(billingDetail.BillingAddress)
            };

            var paymentType = new paymentType();
            switch (paymentSummary.InstrumentId)
            {
                case var _ when paymentSummary.InstrumentId == InstrumentType.CreditCard.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringCreditCard.Id:
                    paymentType.Item = mapper.Map<creditCardType>(billingDetail.CreditCard);
                    break;
                case var _ when paymentSummary.InstrumentId == InstrumentType.EFT.Id:
                case var _ when paymentSummary.InstrumentId == InstrumentType.RecurringECheck.Id:
                    var bankAccount = mapper.Map<bankAccountType>(billingDetail.BankAccount);
                    bankAccount.echeckType = echeckTypeEnum.WEB;
                    paymentType.Item = bankAccount;
                    break;
            };
            transactionRequest.payment = paymentType;

            return new createTransactionRequest { transactionRequest = transactionRequest };
        }

        public dynamic CreateCaptureAuthorizedAmountRequest(decimal amount, string transactionId)
        {
            var transactionRequest = new transactionRequestType()
            {
                transactionType = transactionTypeEnum.priorAuthCaptureTransaction.ToString(),
                amount = amount,
                refTransId = transactionId
            };

            return new createTransactionRequest { transactionRequest = transactionRequest };
        }

        public dynamic GeneratePostPaymentProfileRequest(dynamic providerProfile, decimal amount)
        {
            var profile = (AuthNetProfileDto)providerProfile;

            var profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = profile.CustomerProfileId;
            profileToCharge.paymentProfile = new paymentProfile { 
                paymentProfileId = profile.AuthNetAccount[0].CustomerPaymentId
            };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
                amount = amount,
                profile = profileToCharge
            };

            return new createTransactionRequest { transactionRequest = transactionRequest };
        }
    }
}
