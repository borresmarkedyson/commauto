﻿using AuthorizeNet.Api.Contracts.V1;
using AutoMapper;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AuthNetProfileRequestStrategy : IProfileRequestStrategy
    {
        public dynamic GenerateCreateRequest(PaymentProfileRequestDto profileRequest, IMapper mapper)
        {
            var paymentAccountRequest = profileRequest.PaymentAccount;

            var paymentAccount = new paymentType();
            switch (paymentAccountRequest.InstrumentTypeId)
            {
                case var _ when paymentAccountRequest.InstrumentTypeId == InstrumentType.RecurringCreditCard.Id:
                    paymentAccount.Item = mapper.Map<creditCardType>(paymentAccountRequest.BillingDetail.CreditCard);
                    break;
                case var _ when paymentAccountRequest.InstrumentTypeId == InstrumentType.RecurringECheck.Id:
                    var bankAccount = mapper.Map<bankAccountType>(paymentAccountRequest.BillingDetail.BankAccount);
                    bankAccount.echeckType = echeckTypeEnum.WEB;
                    paymentAccount.Item = bankAccount;
                    break;
                default: throw new ParameterException(ExceptionMessages.ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT);
            };

            var paymentProfileList = new List<customerPaymentProfileType>();
            paymentProfileList.Add( new customerPaymentProfileType() {
                billTo = mapper.Map<customerAddressType>(paymentAccountRequest.BillingDetail.BillingAddress),
                payment = paymentAccount
            });

            var profile = new customerProfileType() {
                description = profileRequest.RiskId.ToString(),
                email = profileRequest.Email,
                paymentProfiles = paymentProfileList.ToArray()
            };

            var validationMode = (validationModeEnum)Enum.Parse(typeof(validationModeEnum), Service.AppSettings.AuthorizeNet.ValidationMode); 
            return new createCustomerProfileRequest { profile = profile, validationMode = validationMode };
        }

        public dynamic GenerateUpdateRequest(PaymentProfileRequestDto profileRequest, dynamic providerPaymentProfile, IMapper mapper)
        {
            var providerProfile = (AuthNetProfileDto)providerPaymentProfile;
            var profile = mapper.Map<customerProfileExType>(profileRequest);
            profile.description = providerProfile.ToString();
            profile.customerProfileId = providerProfile.CustomerProfileId;

            return new updateCustomerProfileRequest() { profile = profile };
        }

        public dynamic GenerateDeleteRequest(dynamic providerPaymentProfile)
        {
            var providerProfile = (AuthNetProfileDto)providerPaymentProfile;

            return new deleteCustomerProfileRequest
            {
                customerProfileId = providerProfile.CustomerProfileId
            };
        }
    }
}
