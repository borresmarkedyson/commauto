﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using System;
using System.IO;
using System.Security.Cryptography;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Encryption
{
    public class FileEncryptionService : IFileEncryptionService
    {
        CspParameters cspp = new CspParameters();
        RSACryptoServiceProvider rsa;

        private const string PUBLIC_KEY_FILE_NAME = "RsaPublicKey.txt";
        private const string CONTAINER_NAME = "Key01";

        public void GenerateKeys()
        {
            cspp.KeyContainerName = CONTAINER_NAME;
            cspp.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(cspp);
            rsa.PersistKeyInCsp = true;
        }

        public void ExportPublicKey(string outputDirectory)
        {
            Directory.CreateDirectory(outputDirectory);
            StreamWriter sw = new StreamWriter(@$"{outputDirectory}\{PUBLIC_KEY_FILE_NAME}", false);
            sw.Write(rsa.ToXmlString(false));
            sw.Close();
        }

        public void ImportPublicKey(string keyFileName)
        {
            StreamReader sr = new StreamReader(keyFileName);
            cspp.KeyContainerName = CONTAINER_NAME;
            cspp.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(cspp);
            string keytxt = sr.ReadToEnd();
            rsa.FromXmlString(keytxt);
            rsa.PersistKeyInCsp = true;
            sr.Close();
        }

        public void ImportPublicKeyFromString(string xmlString)
        {
            cspp.KeyContainerName = CONTAINER_NAME;
            cspp.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(cspp);
            rsa.FromXmlString(xmlString);
            rsa.PersistKeyInCsp = true;
        }

        public void GetPrivateKey()
        {
            cspp.KeyContainerName = CONTAINER_NAME;
            cspp.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(cspp);
            rsa.PersistKeyInCsp = true;
        }

        public void EncryptFile(string inputFile, string outputDirectory)
        {
            if (!File.Exists(inputFile))
            {
                throw new Exception("File does not exist");
            }
            else if (!inputFile.EndsWith("xml"))
            {
                throw new Exception("Only xml files are accepted for encryption");
            }

            Aes aes = Aes.Create();
            ICryptoTransform transform = aes.CreateEncryptor();

            byte[] keyEncrypted = rsa.Encrypt(aes.Key, false);

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            int lKey = keyEncrypted.Length;
            LenK = BitConverter.GetBytes(lKey);
            int lIV = aes.IV.Length;
            LenIV = BitConverter.GetBytes(lIV);

            int startFileName = inputFile.LastIndexOf("\\") + 1;
            string encryptedFile = $@"{outputDirectory}/{inputFile.Substring(startFileName, inputFile.LastIndexOf(".") - startFileName)}.enc";

            using (FileStream outputFileStream = new FileStream(encryptedFile, FileMode.Create))
            {
                outputFileStream.Write(LenK, 0, 4);
                outputFileStream.Write(LenIV, 0, 4);
                outputFileStream.Write(keyEncrypted, 0, lKey);
                outputFileStream.Write(aes.IV, 0, lIV);

                using (CryptoStream outStreamEncrypted = new CryptoStream(outputFileStream, transform, CryptoStreamMode.Write))
                {
                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = aes.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];
                    int bytesRead = 0;

                    using (FileStream inFs = new FileStream(inputFile, FileMode.Open))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamEncrypted.Write(data, 0, count);
                            bytesRead += blockSizeBytes;
                        }
                        while (count > 0);
                        inFs.Close();
                    }
                    outStreamEncrypted.FlushFinalBlock();
                    outStreamEncrypted.Close();
                }
                outputFileStream.Close();
            }
        }

        public void EncryptStream(MemoryStream inputStream, string encryptedFileName)
        {
            Aes aes = Aes.Create();
            ICryptoTransform transform = aes.CreateEncryptor();

            byte[] keyEncrypted = rsa.Encrypt(aes.Key, false);

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            int lKey = keyEncrypted.Length;
            LenK = BitConverter.GetBytes(lKey);
            int lIV = aes.IV.Length;
            LenIV = BitConverter.GetBytes(lIV);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(LenK);
                Array.Reverse(LenIV);
            }

            using (FileStream outputFileStream = new FileStream(encryptedFileName, FileMode.Create))
            {
                outputFileStream.Write(LenK, 0, 4);
                outputFileStream.Write(LenIV, 0, 4);
                outputFileStream.Write(keyEncrypted, 0, lKey);
                outputFileStream.Write(aes.IV, 0, lIV);

                using (CryptoStream outStreamEncrypted = new CryptoStream(outputFileStream, transform, CryptoStreamMode.Write))
                {
                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = aes.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];
                    int bytesRead = 0;

                    do
                    {
                        count = inputStream.Read(data, 0, blockSizeBytes);
                        offset += count;
                        outStreamEncrypted.Write(data, 0, count);
                        bytesRead += blockSizeBytes;
                    }
                    while (count > 0);
                    inputStream.Close();

                    outStreamEncrypted.FlushFinalBlock();
                    outStreamEncrypted.Close();
                }
                outputFileStream.Close();
            }
        }


        public void DecryptFile(string encryptedFile, string outputDirectory)
        {
            Aes aes = Aes.Create();

            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            string encryptedFileName = Path.GetFileName(encryptedFile);

            string decryptedFile = @$"{outputDirectory}\{encryptedFileName.Substring(0, encryptedFileName.LastIndexOf("."))}.xml";

            using (FileStream inputFileStream = new FileStream(encryptedFile, FileMode.Open))
            {
                byte[] buf = new byte[4];
                int c;

                inputFileStream.Read(LenK, 0, LenK.Length);
                inputFileStream.Read(LenIV, 0, LenIV.Length);

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(LenK);
                    Array.Reverse(LenIV);
                }

                int lenK = BitConverter.ToInt32(LenK, 0);
                int lenIV = BitConverter.ToInt32(LenIV, 0);

                int startC = lenK + lenIV + 8;
                int lenC = (int)inputFileStream.Length - startC;

                byte[] KeyEncrypted = new byte[lenK];
                byte[] IV = new byte[lenIV];

                inputFileStream.Read(KeyEncrypted, 0, lenK);
                inputFileStream.Read(IV, 0, lenIV);
                Directory.CreateDirectory(outputDirectory);

                byte[] KeyDecrypted = rsa.Decrypt(KeyEncrypted, false);

                ICryptoTransform transform = aes.CreateDecryptor(KeyDecrypted, IV);

                using (FileStream outputFileStream = new FileStream(decryptedFile, FileMode.Create))
                {
                    int count = 0;
                    int offset = 0;

                    int blockSizeBytes = aes.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];
                    inputFileStream.Seek(startC, SeekOrigin.Begin);
                    using (CryptoStream outStreamDecrypted = new CryptoStream(outputFileStream, transform, CryptoStreamMode.Write))
                    {
                        do
                        {
                            count = inputFileStream.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamDecrypted.Write(data, 0, count);
                        }
                        while (count > 0);

                        outStreamDecrypted.FlushFinalBlock();
                        outStreamDecrypted.Close();
                    }
                    outputFileStream.Close();
                }
                inputFileStream.Close();
            }
        }
    }
}
