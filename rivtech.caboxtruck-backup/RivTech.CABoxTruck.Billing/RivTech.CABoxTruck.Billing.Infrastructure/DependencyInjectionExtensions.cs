﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Infrastructure.Encryption;
using RivTech.CABoxTruck.Billing.Infrastructure.Generic;
using RivTech.CABoxTruck.Billing.Infrastructure.PaymentProfiles;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc;
using RivTech.CABoxTruck.Billing.Infrastructure.Requests;

namespace RivTech.CABoxTruck.Billing.Infrastructure
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IPaymentStrategyFactory, PaymentStrategyFactory>();
            services.AddTransient<IPaymentStrategy, AuthNetPaymentStrategy>();
            services.AddTransient<IPaymentRequestStrategyFactory, PaymentRequestStrategyFactory>();
            services.AddTransient<IPaymentRequestStrategy, AuthNetPaymentRequestStrategy>();

            services.AddTransient<IProfileStrategyFactory, ProfileStrategyFactory>();
            services.AddTransient<IProfileStrategy, AuthNetProfileStrategy>();
            services.AddTransient<IProfileRequestStrategyFactory, ProfileRequestStrategyFactory>();
            services.AddTransient<IProfileRequestStrategy, AuthNetProfileRequestStrategy>();

            services.AddTransient<IAccountFactory, AccountFactory>();
            services.AddTransient<IAccountStrategy, AuthNetAccountStrategy>();
            services.AddTransient<IAccountRequestFactory, AccountRequestFactory>();
            services.AddTransient<IAccountRequestStrategy, AuthNetAccountRequestStrategy>();

            services.AddTransient<IFileEncryptionService, FileEncryptionService>();
            services.AddTransient<IHolidayScheduleService, HolidayScheduleService>();

            services.AddScoped<ITokenProvider, PncCachedTokenProvider>();
            services.AddScoped<IPaymentProcessor, PncPaymentProcessor>();

            SetAutoMapper(services);
            ConnectAuthorizeNet(configuration);
            return services;
        }

        private static void SetAutoMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void ConnectAuthorizeNet(IConfiguration configuration)
        {
            var authorizeNetSection = configuration.GetSection("AuthorizeNet");
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.createEnvironment(
                authorizeNetSection.GetValue<string>("BaseUrl"),
                authorizeNetSection.GetValue<string>("XMLbasedUrl"));
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = authorizeNetSection.GetValue<string>("ApiLoginID"),
                ItemElementName = ItemChoiceType.transactionKey,
                Item = authorizeNetSection.GetValue<string>("ApiTransactionKey"),
            };
        }
    }
}
