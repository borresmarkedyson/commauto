﻿using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Security
{
    public static class JsonMasker
    {
        public static string Mask(string[] Properties, string toMaskJsonContent)
        {
            var maskedContent = toMaskJsonContent;
            foreach (var property in Properties)
            {
                maskedContent = new Regex($"(?<={property}\":\")(.*?)(?=\")").Replace(maskedContent, "****");
            }
            return maskedContent;
        }
    }
}
