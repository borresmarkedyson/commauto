﻿using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AccountFactory : IAccountFactory
    {
        private readonly IAccountStrategy _authNetAccountStrategy = new AuthNetAccountStrategy();

        public IAccountStrategy GetStrategy(string appId)
        {
            var paymentProviderId = PaymentProvider.GetByAppId(appId).Id;
            switch (paymentProviderId)
            {
                case var _ when paymentProviderId == PaymentProvider.AuthorizeNet.Id:
                    return _authNetAccountStrategy;
                default: throw new ArgumentException(ExceptionMessages.NO_PAYMENT_PROVIDER);
            }
        }
    }
}
