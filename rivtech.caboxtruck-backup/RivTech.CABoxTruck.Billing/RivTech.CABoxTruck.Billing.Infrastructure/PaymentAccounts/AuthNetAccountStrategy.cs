﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Infrastructure.Exceptions;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Requests
{
    public class AuthNetAccountStrategy : IAccountStrategy
    {
        public dynamic CreateAccount(dynamic paymentAccountRequest)
        {
            try
            {
                var controller = new createCustomerPaymentProfileController(paymentAccountRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        public dynamic GetAccount(dynamic paymentAccountRequest)
        {
            try
            {
                var controller = new getCustomerPaymentProfileController(paymentAccountRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));
           
                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        public dynamic DeleteAccount(dynamic paymentAccountRequest)
        {
            try
            {
                var controller = new deleteCustomerPaymentProfileController(paymentAccountRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        public dynamic UpdateAccount(dynamic request)
        {
            try
            {
                var controller = new updateCustomerPaymentProfileController(request);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }
    }
}
