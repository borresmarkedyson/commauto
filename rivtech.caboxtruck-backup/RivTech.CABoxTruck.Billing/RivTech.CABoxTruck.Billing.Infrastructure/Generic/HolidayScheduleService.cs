﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using RestSharp;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Generic
{
    public class HolidayScheduleService : IHolidayScheduleService
    {
        private const string HOLIDAY_SCHEDULE_KEY = nameof(HOLIDAY_SCHEDULE_KEY);

        private readonly IMemoryCache _cache;

        public HolidayScheduleService(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task<List<HolidayScheduleDto>> GetHolidaySchedule()
        {

            if (_cache.TryGetValue(HOLIDAY_SCHEDULE_KEY, out List<HolidayScheduleDto> cachedHolidaySchedule))
            {
                return cachedHolidaySchedule;
            }

            var genericServiceSettings = Service.AppSettings.GenericService;

            var client = new RestClient(genericServiceSettings.Url);
            var request = new RestRequest
            {
                Resource = "api/HolidaySchedule",
                Method = Method.GET
            };

            request.AddHeader("Content-Type", "application/json");
            var response = await client.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                var holidaySchedule = JsonConvert.DeserializeObject<List<HolidayScheduleDto>>(response.Content);
                _cache.Set(HOLIDAY_SCHEDULE_KEY, holidaySchedule, DateTime.Now.Date.AddDays(1));
                return holidaySchedule;
            }

            return null;
        }
    }
}
