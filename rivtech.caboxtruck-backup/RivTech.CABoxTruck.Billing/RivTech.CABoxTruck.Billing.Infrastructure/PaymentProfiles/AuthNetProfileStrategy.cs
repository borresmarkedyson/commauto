﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Infrastructure.Exceptions;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace RivTech.CABoxTruck.Billing.Infrastructure.PaymentProfiles
{
    public class AuthNetProfileStrategy : IProfileStrategy
    {
        public dynamic CreateProfile(dynamic profileRequest)
        {
            try
            {
                var controller = new createCustomerProfileController((createCustomerProfileRequest)profileRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        public dynamic UpdateProfile(dynamic profileRequest)
        {
            try
            {
                var controller = new updateCustomerProfileController((updateCustomerProfileRequest)profileRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }

        public dynamic DeleteProfile(dynamic profileRequest)
        {
            try
            {
                var controller = new deleteCustomerProfileController((deleteCustomerProfileRequest)profileRequest);
                controller.Execute();

                var response = controller.GetApiResponse();
                if (response == null)
                {
                    var errorresponse = controller.GetErrorResponse();
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(errorresponse.messages.message.ToList())));
                }

                if (response.messages.resultCode != messageTypeEnum.Ok)
                    throw new AuthNetException(string.Join(":", AuthNetErrorMessages.GetErrorResponseMessages(response.messages.message.ToList())));

                return response;
            }
            catch (WebException error) when (error.InnerException is SocketException)
            {
                throw new AuthNetException(AuthNetErrorMessages.GetConnectionResponseMessage(error.InnerException as SocketException));
            }
        }
    }
}
