﻿using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Exceptions
{
    public class AuthNetException : PaymentThirdPartyException
    {
        public AuthNetException() { }

        public AuthNetException(string message) : base(message) { }
    }
}
