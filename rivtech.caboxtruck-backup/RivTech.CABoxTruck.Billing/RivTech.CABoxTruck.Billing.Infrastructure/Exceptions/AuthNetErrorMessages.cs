﻿using AuthorizeNet.Api.Contracts.V1;
using System.Collections.Generic;
using System.Net.Sockets;

namespace RivTech.CABoxTruck.Billing.Infrastructure.Exceptions
{

	public class AuthNetErrorMessages
	{
		public const string ROUTINGNUMBER_INVALID_ERRCODE = "9";
		public const string ROUTINGNUMBER_INVALID_MSG = "Invalid routing number";

		public const string XMLParseErrorLength = "E00003L";
		public const string LengthMessage = "# must be at least #-# characters.";

		private static readonly Dictionary<string, string> ErrorMessages = new Dictionary<string, string>
		{
			{ ROUTINGNUMBER_INVALID_ERRCODE, ROUTINGNUMBER_INVALID_MSG },
			{ XMLParseErrorLength, LengthMessage }
		};

		public const int SERVERDOWN_ERRCODE = 10060;
		public const string SERVERDOWN_MSG = "Connection attempt to authorize.net failed.";

		private static readonly Dictionary<int, string> ConnectionErrorMessages = new Dictionary<int, string>
		{
			{ SERVERDOWN_ERRCODE, SERVERDOWN_MSG }
		};

		public static List<string> GetErrorResponseMessages(List<messagesTypeMessage> errors)
		{
			var errorMessages = new List<string>();

			foreach (var err in errors)
			{
				if (ErrorMessages.ContainsKey(err.code))
					errorMessages.Add(ErrorMessages[err.code]);
				else
					errorMessages.Add(err.text);
			}
			return errorMessages;
		}

		public static List<string> GetTransactionResponseMessages(List<transactionResponseError> errors)
		{
			var errorMessages = new List<string>();

			foreach (var err in errors)
			{
				if (ErrorMessages.ContainsKey(err.errorCode))
					errorMessages.Add(ErrorMessages[err.errorCode]);
				else
					errorMessages.Add(err.errorText);
			}
			return errorMessages;
		}

		public static string GetConnectionResponseMessage(SocketException error)
		{
			if (ConnectionErrorMessages.ContainsKey(error.ErrorCode))
				return ConnectionErrorMessages[error.ErrorCode];
			else
				return error.Message;
		}
	}
}
