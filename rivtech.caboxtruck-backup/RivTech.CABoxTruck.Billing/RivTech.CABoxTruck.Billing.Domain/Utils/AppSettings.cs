﻿using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.Utils
{
    public class AppSettings
    {
        public AuthorizeNetSettings AuthorizeNet { get; set; }
        public class AuthorizeNetSettings
        {
            public string ApiLoginID { get; set; }
            public string ApiTransactionKey { get; set; }
            public string BaseUrl { get; set; }
            public string XMLbasedUrl { get; set; }
            public string ValidationMode { get; set; }
        }

        public LockboxSettings Lockbox { get; set; }

        public class LockboxSettings
        {
            public string DLFileDirectory { get; set; }
            public FtpSettingLB FTP { get; set; }
            public class FtpSettingLB
            {
                public string BaseUrl { get; set; }
                public string Directory { get; set; }
                public int Port { get; set; }
                public bool IsSsh { get; set; }
                public string Username { get; set; }
                public string Password { get; set; }
                public string FilePath { get; set; }
                public string FileName { get; set; }
            }
        }

        public DirectBillSettings DirectBill { get; set; }

        public class DirectBillSettings
        {
            public string DLFileDirectory { get; set; }
            public FtpSettingDB FTP { get; set; }
            public class FtpSettingDB
            {
                public string BaseUrl { get; set; }
                public string Directory { get; set; }
                public int Port { get; set; } 
                public bool IsSsh { get; set; }
                public string Username { get; set; }
                public string Password { get; set; }
                public string FilePath { get; set; }
                public string FileName { get; set; }
                public string FileNameCSIC { get; set; }
                public string FileNameCNIC { get; set; }
            }
        }

        public RefundRequestSettings RefundRequest { get; set; }

        public class RefundRequestSettings
        {
            public string FilePath { get; set; }
            public string EncryptedFileName { get; set; }
            public string AzureStorageDirectory { get; set; }
            public FtpSettings FTP { get; set; }
            public class FtpSettings
            {
                public string Host { get; set; }
                public string FilePath { get; set; }
                public string FileName { get; set; }
                public string Username { get; set; }
                public string Password { get; set; }
            }
        }

        public GenericServiceSettings GenericService { get; set; }
        public class GenericServiceSettings
        {
            public string Url { get; set; }
        }

        public AzureStorageSettings AzureStorages { get; set; }
        public class AzureStorageSettings
        {
            public StorageDetails CentauriNew { get; set; }
            public StorageDetails Centauri { get; set; }
            public StorageDetails CommAuto { get; set; }

            public StorageDetails this[int programId]
            {
                get
                {
                    var properties = GetType().GetProperties();
                    return properties
                        .Select(property => property.GetValue(this, null) as StorageDetails)
                        .FirstOrDefault(prop => prop != null && prop.ProgramId == programId);
                }
            }

            public class StorageDetails
            {
                public int ProgramId { get; set; }
                public string ConnectionString { get; set; }
                public string Directory { get; set; }
            }
        }
    }
}
