﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Utils
{
    public class Service
    {
        public static IHttpContextAccessor HttpContextAccessor;
        public static T GetService<T>()
        {
            return (T)HttpContextAccessor?.HttpContext?.RequestServices?.GetService(typeof(T));
        }

        private static AppSettings _appSettings;
        public static AppSettings AppSettings
        {
            get
            {
                var appSettings = GetService<IOptionsSnapshot<AppSettings>>()?.Value;

                if (appSettings != null)
                    _appSettings = appSettings;

                return _appSettings;
            }
            set => _appSettings = value;
        }
    }
}
