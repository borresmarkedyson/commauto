﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RivTech.CABoxTruck.Billing.Domain.Common
{
    public abstract class Enumeration : BaseEntity<string>
    {
        public virtual string Description { get; set; }
        public virtual bool IsActive { get; set; }

        protected Enumeration()
        {
            IsActive = true;
        }

        protected Enumeration(string id, string description, string code = "")
        {
            Id = id;
            Description = description;
            IsActive = true;
        }

        public override string ToString()
        {
            return Description;
        }

        private static readonly Dictionary<string, IEnumerable<object>> ListDictionary = new Dictionary<string, IEnumerable<object>>();

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;
            if (otherValue == null)
            {
                return false;
            }
            var typeMatch = GetType() == obj.GetType();
            var valueMatch = Id.Equals(otherValue.Id);
            return typeMatch && valueMatch;
        }

        public static T GetEnumerationById<T>(string id) where T : Enumeration, new()
        {
            return id == null ? null : GetAll<T>().SingleOrDefault(x => x.Id.Equals(id));
        }

        public static List<T> GetAll<T>() where T : Enumeration, new()
        {
            var type = typeof(T);
            var typeName = type.Name;

            if (ListDictionary.ContainsKey(typeName))
            {
                return (ListDictionary.SingleOrDefault(x => x.Key.Equals(typeName)).Value as List<T>);
            }

            var fields = type.GetTypeInfo().GetMembers(BindingFlags.Public |
                                                       BindingFlags.Static |
                                                       BindingFlags.DeclaredOnly);
            var list = new List<T>();
            foreach (var info in fields)
            {
                var instance = new T();
                var locatedValue = GetValue(info, instance) as T;
                if (locatedValue != null)
                {
                    list.Add(locatedValue);
                }
            }

            ListDictionary.Add(typeName, list);

            return list;
        }

        public static List<string> GetAllId<T>() where T : Enumeration, new()
        {
            var types = GetAll<T>();
            var typeIds = new List<string>();

            foreach (var type in types)
            {
                typeIds.Add(type.Id);
            }
            return typeIds;
        }

        private static object GetValue(MemberInfo memberInfo, object forObject)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)memberInfo).GetValue(forObject);
                case MemberTypes.Property:
                    return ((PropertyInfo)memberInfo).GetValue(forObject);
                default:
                    return null;
            }
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
