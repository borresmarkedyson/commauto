﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions
{
    public class PaymentThirdPartyException : Exception
    {
        public PaymentThirdPartyException() { }
        public PaymentThirdPartyException(string message) : base(message) { }
    }
}
