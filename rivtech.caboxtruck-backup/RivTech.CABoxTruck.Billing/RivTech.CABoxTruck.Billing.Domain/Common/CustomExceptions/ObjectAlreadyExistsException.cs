﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions
{
    public class ObjectAlreadyExistsException : Exception
    {
        public ObjectAlreadyExistsException() : base(null)
        {
        }
    }
}
