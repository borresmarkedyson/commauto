﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions
{
    public class ParameterException : Exception
    {
        public ParameterException(string msg = "Invalid parameter.") : base(msg)
        {
        }
    }
}
