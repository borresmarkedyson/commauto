﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions
{
    public class ModelNotFoundException : Exception
    {
        public ModelNotFoundException(string msg = "Not found.") : base(msg)
        {
        }
    }
}
