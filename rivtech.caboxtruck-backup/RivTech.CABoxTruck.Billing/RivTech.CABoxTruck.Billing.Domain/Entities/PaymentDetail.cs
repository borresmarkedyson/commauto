﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class PaymentDetail : BaseEntity<Guid>
    {
        public Guid PaymentId { get; private set; }
        public decimal Amount { get; private set; }
        public string AmountTypeId { get; private set; }
        public string AmountSubTypeId { get; private set; }

        public AmountType AmountType { get; private set; }
        public AmountSubType AmountSubType { get; private set; }
        public Payment Payment { get; private set; }

        public PaymentDetail(decimal amount, AmountType amountType)
        {
            if (amount == 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_ZERO);
            }

            if (!amountType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }

            Amount = amount;
            AmountTypeId = amountType.Id;
        }

        public PaymentDetail(decimal amount, AmountType amountType, AmountSubType amountSubType)
        {
            if (amount == 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_ZERO);
            }

            if (!amountType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }

            Amount = amount;
            AmountTypeId = amountType.Id;
            AmountSubTypeId = amountSubType != null ? amountSubType.Id : null;
        }

        public PaymentDetail(decimal amount, AmountType amountType, Guid paymentId)
        {
            if (amount == 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_ZERO);
            }

            if (!amountType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }

            Amount = amount;
            AmountTypeId = amountType.Id;
            PaymentId = paymentId;
        }

        public PaymentDetail(decimal amount, AmountType amountType, AmountSubType amountSubType, Guid paymentId)
        {
            if (amount == 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_ZERO);
            }

            if (!amountType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }

            Amount = amount;
            AmountTypeId = amountType.Id;
            AmountSubTypeId = amountSubType != null ? amountSubType.Id : null;
            PaymentId = paymentId;
        }

        public void SetPaymentDetailAmount(decimal amount)
        {
            if (amount < 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_LESS_THAN_ZERO);
            }

            Amount = amount;
        }

        private PaymentDetail() { }
    }
}
