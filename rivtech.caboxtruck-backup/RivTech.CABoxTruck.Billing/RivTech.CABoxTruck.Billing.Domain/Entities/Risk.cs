﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Risk : BaseEntity<Guid>
    {
        public string PaymentPlanId { get; private set; }
        public PaymentPlan PaymentPlan { get; private set; }
        public string ApplicationId { get; private set; }
        public RivtechApplication Application { get; private set; }
        public List<Installment> InstallmentSchedule { get; set; }
        public DateTime EffectiveDate { get; private set; }
        public NoticeHistory NoticeHistory { get; private set; }
        public DateTime LatestInvoiceDueDate { get; private set; }
        public string PolicyNumber { get; private set; }
        public string PolicyStatusId { get; private set; }
        public PolicyStatus PolicyStatus { get; private set; }
        public bool IsPendingCancellationForNonPayment { get; private set; }
        public bool CanAutoReinstate { get; private set; } = true;
        public DateTime? CancellationEffectiveDate { get; private set; }
        public decimal? AlpCommission { get; private set; }
        public decimal? PdpCommission { get; private set; }
        public string StateCode { get; private set; }


        public void SetPaymentPlanId(string paymentPlanId)
        {
            if (!PaymentPlan.IsValid(paymentPlanId))
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_PLAN);
            }
            PaymentPlanId = paymentPlanId;
        }

        public void SetApplicationId(string applicationId)
        {
            if (!RivtechApplication.IsValid(applicationId))
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
            ApplicationId = applicationId;
        }

        public void SetEffectiveDate(DateTime effectiveDate)
        {
            EffectiveDate = effectiveDate;
        }

        public void InitializeNoticeHistory()
        {
            NoticeHistory = new NoticeHistory();
        }

        public void SetLatestInvoiceDueDate(DateTime dueDate)
        {
            LatestInvoiceDueDate = dueDate;
        }

        public void SetPolicyStatus(PolicyStatus policyStatus)
        {
            if (policyStatus == null || !policyStatus.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_POLICY_STATUS);
            }
            PolicyStatusId = policyStatus.Id;
        }

        public void SetIsPendingCancellationForNonPayment(bool isPendingCancellationForNonPayment)
        {
            IsPendingCancellationForNonPayment = isPendingCancellationForNonPayment;
        }

        public void SetCanAutoReinstate(bool canAutoReinstate)
        {
            CanAutoReinstate = canAutoReinstate;
        }
        public void SetStateCode(string stateCode)
        {
            StateCode = stateCode;
        }

        public void SetCancellationEffectiveDate(DateTime? date)
        {
            CancellationEffectiveDate = date;
        }

        public void SetPolicyNumber(string policyNumber)
        {
            PolicyNumber = policyNumber;
        }

        public void SetAlpCommission(decimal alpCommission)
        {
            AlpCommission = alpCommission;
        }
        public void SetPdpCommission(decimal pdpCommission)
        {
            PdpCommission = pdpCommission;
        }
    }
}
