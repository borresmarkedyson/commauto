﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Payment : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public decimal Amount { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public string InstrumentId { get; private set; }
        public string Reference { get; private set; }
        public string Comment { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }
        public Guid? VoidOfPaymentId { get; private set; }
        public string VoidedById { get; private set; }
        public DateTime? VoidDate { get; private set; }
        public DateTime? ClearDate { get; private set; }
        public DateTime? EscheatDate { get; private set; }

        public InstrumentType InstrumentType { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public Payment VoidOfPayment { get; private set; }
        public ApplicationUser VoidedBy { get; private set; }
        public List<PaymentDetail> PaymentDetails { get; private set; }
        public string PaymentTypeId { get; private set; }
        public PaymentType PaymentType { get; private set; }
        public Guid? PayeeInfoId { get; private set; }
        public PayeeInfo PayeeInfo { get; private set; }


        private Payment() { }

        public Payment(Guid riskId, decimal amount, DateTime effectiveDate, string reference, InstrumentType instrument,
            List<PaymentDetail> paymentDetail, string comment, ApplicationUser user, string paymentTypeId = null)
        {
            if (!instrument.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }

            if (paymentDetail == null || !paymentDetail.Any())
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENTDETAIL);
            }

            RiskId = riskId;
            Amount = amount;
            EffectiveDate = effectiveDate;
            InstrumentId = instrument.Id;
            Reference = reference;
            PaymentDetails = paymentDetail;
            Comment = comment;

            CreatedDate = user.UserDate;
            CreatedById = user.Id;
            PaymentTypeId = instrument.Id == InstrumentType.Transferred.Id ? PaymentType.Transfer.Id
                : string.IsNullOrWhiteSpace(paymentTypeId) ? PaymentType.Regular.Id
                : paymentTypeId;
        }

        public static Payment CreateVoidPayment(ApplicationUser user, Payment paymentToVoid, PaymentType paymentType, string comments)
        {
            if (paymentType == null || !paymentType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_TYPE);
            }

            paymentToVoid.VoidDate = user.UserDate;
            paymentToVoid.VoidedById = user.Id;

            Payment voidPayment = new Payment
            {
                RiskId = paymentToVoid.RiskId,
                EffectiveDate = paymentToVoid.EffectiveDate,
                InstrumentId = paymentToVoid.InstrumentId,
                Reference = paymentToVoid.Reference,
                PaymentDetails = GenerateNegativePaymentDetails(paymentToVoid.PaymentDetails),
                Comment = comments,
                CreatedDate = paymentToVoid.CreatedDate,
                Amount = paymentToVoid.Amount * -1,
                VoidedById = user.Id,
                VoidDate = user.UserDate,
                VoidOfPayment = paymentToVoid,
                PaymentTypeId = paymentType.Id
            };
            return voidPayment;
        }

        public static Payment CreateNegativePayment(Payment paymentToNegate, PaymentType paymentType, string comments)
        {
            if (paymentType == null || !paymentType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_TYPE);
            }

            Payment negativePayment = new Payment
            {
                RiskId = paymentToNegate.RiskId,
                EffectiveDate = paymentToNegate.EffectiveDate,
                InstrumentId = paymentToNegate.InstrumentId,
                Reference = paymentToNegate.Reference,
                PaymentDetails = GenerateNegativePaymentDetails(paymentToNegate.PaymentDetails),
                Comment = comments,
                CreatedDate = paymentToNegate.CreatedDate,
                Amount = paymentToNegate.Amount * -1,
                PaymentTypeId = paymentType.Id
            };

            return negativePayment;
        }

        public static List<PaymentDetail> GenerateNegativePaymentDetails(List<PaymentDetail> paymentDetails)
        {
            if (paymentDetails == null)
            {
                return null;
            }

            List<PaymentDetail> negativePaymentDetails = new List<PaymentDetail>();
            foreach (var paymentDetail in paymentDetails)
            {
                if (paymentDetail != null)
                {
                    if (paymentDetail.Amount != 0)
                    {
                        if (!string.IsNullOrWhiteSpace(paymentDetail.AmountSubTypeId))
                        {
                            negativePaymentDetails.Add(new PaymentDetail(-paymentDetail.Amount, Enumeration.GetEnumerationById<AmountType>(paymentDetail.AmountTypeId), Enumeration.GetEnumerationById<AmountSubType>(paymentDetail.AmountSubTypeId)));
                        }
                        else
                        {
                            negativePaymentDetails.Add(new PaymentDetail(-paymentDetail.Amount, Enumeration.GetEnumerationById<AmountType>(paymentDetail.AmountTypeId)));
                        }
                    }
                }
            }
            return negativePaymentDetails;
        }

        public static Payment CreateTransferPayment(ApplicationUser user, Payment paymentToVoid, Guid destinationRiskId, string comment)
        {
            var paymentDetailTransfer = new List<PaymentDetail>();
            foreach (var paymentDetail in paymentToVoid.PaymentDetails)
            {
                if (!string.IsNullOrWhiteSpace(paymentDetail.AmountSubTypeId))
                {
                    paymentDetailTransfer.Add(new PaymentDetail(paymentDetail.Amount, paymentDetail.AmountType, Enumeration.GetEnumerationById<AmountSubType>(paymentDetail.AmountSubTypeId)));
                }
                else
                {
                    paymentDetailTransfer.Add(new PaymentDetail(paymentDetail.Amount, paymentDetail.AmountType));
                }
            }

            Payment transferPayment = new Payment
            {
                RiskId = destinationRiskId,
                EffectiveDate = paymentToVoid.EffectiveDate,
                InstrumentId = paymentToVoid.InstrumentId,
                Reference = paymentToVoid.Reference,
                PaymentDetails = paymentDetailTransfer,
                Comment = comment,
                CreatedDate = user.UserDate,
                Amount = paymentToVoid.Amount,
                PaymentTypeId = PaymentType.Transfer.Id,
                CreatedById = user.Id
            };
            return transferPayment;
        }

        public void SetPayeeInfo(PayeeInfo payeeInfo)
        {
            PayeeInfo = payeeInfo;
        }

        public void SetPaymentType(PaymentType paymentType)
        {
            if (paymentType == null || !paymentType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_TYPE);
            }

            PaymentTypeId = paymentType.Id;
        }

        public void SetClearDate(DateTime? clearDate)
        {
            ClearDate = clearDate;
        }

        public void SetEscheatDate(DateTime? escheatDate, string comments)
        {
            EscheatDate = escheatDate;
            Comment = comments;
        }

        public void RemoveVoidStatus()
        {
            VoidDate = null;
            VoidedById = null;
        }
    }
}
