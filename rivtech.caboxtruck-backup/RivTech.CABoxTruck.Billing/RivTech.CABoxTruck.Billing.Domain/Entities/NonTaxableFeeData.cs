﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class NonTaxableFeeData:  BaseEntity<Guid>
    {
        public string StateCode { get; private set; }
        public string FeeKindId { get; private set; }
        public DateTime EffectiveDate { get; private set; }
    }
}
