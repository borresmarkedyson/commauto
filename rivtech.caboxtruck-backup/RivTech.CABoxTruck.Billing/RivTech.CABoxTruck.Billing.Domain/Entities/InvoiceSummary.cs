﻿using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class InvoiceSummary
    {
        public decimal PremiumAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public List<InvoiceDetail> Details { get; set; }
    }
}
