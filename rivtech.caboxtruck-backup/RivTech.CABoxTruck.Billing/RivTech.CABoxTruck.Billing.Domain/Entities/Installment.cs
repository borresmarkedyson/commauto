﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Installment : BaseEntity<Guid>
    {
        public Risk Risk { get; private set; }
        public Guid RiskId { get; private set; }
        public DateTime InvoiceOnDate { get; private set; }
        public DateTime DueDate { get; private set; }
        public decimal PercentOfOutstanding { get; private set; }
        public bool IsInvoiced { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public string CreatedById { get; private set; }
        public DateTime? VoidDate { get; private set; }
        public ApplicationUser VoidedBy { get; private set; }
        public string VoidedById { get; private set; }
        public InstallmentType InstallmentType { get; private set; }
        public string InstallmentTypeId { get; private set; }
        public Guid? InvoiceId { get; private set; }
        public Invoice Invoice { get; private set; }
        public bool IsSkipped { get; private set; }

        private Installment() { }

        public Installment(Guid riskId, DateTime invoiceOnDate, DateTime dueDate, decimal? percentOutstanding, InstallmentType installmentType, ApplicationUser user)
        {
            if (installmentType == null || !installmentType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_INSTALLMENT_TYPE);
            }

            RiskId = riskId;
            InvoiceOnDate = invoiceOnDate;
            DueDate = dueDate;

            if (percentOutstanding != null && percentOutstanding.HasValue)
            {
                PercentOfOutstanding = percentOutstanding.Value;
            }

            CreatedDate = user.UserDate;
            InstallmentTypeId = installmentType.Id;
            CreatedById = user.Id;
        }

        public static Installment AddInstallment(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
            InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        {
            return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        }

        //public static Installment FullPay(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        //public static Installment TwoPay(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        //public static Installment FourPay(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        //public static Installment EightPay(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        //public static Installment TenPay(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        //public static Installment TenPayRenewal(Guid riskId, DateTime invoiceOnDate, DateTime dueDate,
        //    InstallmentType installmentType, string appId, ApplicationUser user, decimal? percentOutstanding)
        //{
        //    return new Installment(riskId, invoiceOnDate, dueDate, percentOutstanding, installmentType, user);
        //}

        public void SetAsVoid(ApplicationUser user)
        {
            VoidDate = user.UserDate;
            VoidedById = user.Id;
        }

        public void SetAsInvoiced(Guid invoiceId)
        {
            InvoiceId = invoiceId;
            IsInvoiced = true;
        }

        public void SetAsSkipped()
        {
            IsSkipped = true;
        }

        public void SetInvoiceOnDate(DateTime invoiceOnDate)
        {
            InvoiceOnDate = invoiceOnDate;
        }
        public void SetDueDate(DateTime dueDate)
        {
            DueDate = dueDate;
        }
    }
}
