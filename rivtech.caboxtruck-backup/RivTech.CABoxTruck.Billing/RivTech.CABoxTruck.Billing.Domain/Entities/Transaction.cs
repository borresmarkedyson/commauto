﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Transaction : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public string TransactionTypeId { get; private set; }
        public TransactionType TransactionType { get; private set; }
        public List<TransactionDetail> TransactionDetails { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public Guid? InvoiceId { get; private set; }
        public Invoice Invoice { get; private set; }

        public Transaction(Guid riskId, TransactionType transactionType, List<TransactionDetail> transactionDetails, DateTime effectiveDate, ApplicationUser user, Guid? invoiceId)
        {
            if (transactionType == null || !transactionType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_TRANSACTION_TYPE);
            }
            else if (effectiveDate == default(DateTime))
            {
                throw new ParameterException(ExceptionMessages.EFFECTIVEDATE_INVALID);
            }

            RiskId = riskId;
            TransactionTypeId = transactionType.Id;
            TransactionDetails = transactionDetails;
            EffectiveDate = effectiveDate;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
            InvoiceId = invoiceId;
        }

        private Transaction() { }

        public void SetInvoiceId(Guid invoiceId)
        {
            InvoiceId = invoiceId;
        }
    }
}
