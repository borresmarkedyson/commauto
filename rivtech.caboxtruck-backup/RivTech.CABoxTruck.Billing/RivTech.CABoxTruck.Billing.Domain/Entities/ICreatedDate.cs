﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public interface ICreatedDate
    {
        DateTime CreatedDate { get; set; }
    }
}
