﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class FeeDetails : BaseEntity<int>, IIsActive
    {
        public FeeDetails()
        {
            IsActive = true;
        }

        public string StateCode { get; set; }
        public string FeeKindId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public bool IsActive { get; set; }
    }
}
