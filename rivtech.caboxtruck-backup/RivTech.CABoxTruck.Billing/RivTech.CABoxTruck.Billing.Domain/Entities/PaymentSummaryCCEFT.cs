﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class PaymentSummaryCCEFT
    {
        public Guid Id { get; set; }
        public Guid? SubmissionId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string InstrumentId { get; set; }
        public string Instrument { get; set; }
        public string TransactionDetail { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid CreatedById { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? VoidDate { get; set; }
        public string VoidedBy { get; set; }
        public decimal? Premium { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Fee { get; set; }
        public string Payer { get; set; }
        public string PaymentType { get; set; }
        public Guid? VoidOfPayment { get; set; }
        public string Email { get; set; }
        public string PolicyNumber { get; set; }
        public int PolicyDetailId { get; set; }
        public string PostedBy { get; set; }
        public string InsuredName { get; set; }
        public string AccessCode { get; set; }
        // CreditCard
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? CreditCardTypeId { get; set; }
        public string CreditCardNumber { get; set; }
        public int? ExpirationMonth { get; set; } = DateTime.Now.Month;
        public int? ExpirationYear { get; set; } = DateTime.Now.Year;
        public string Cvv { get; set; }

        // EFT
        public int? AccountType { get; set; } = 0;
        public int? AccountUsageType { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }

    }

}
