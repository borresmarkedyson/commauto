﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedStatusHistory : BaseEntity<Guid>
    {
        public Guid PaymentId { get; private set; }
        public string StatusId { get; private set; }
        public DateTime ChangedDate { get; private set; }
        public string Comment { get; private set; }
        public string ChangedById { get; private set; }        

        public SuspendedStatus Status { get; private set; }
        public SuspendedPayment Payment { get; private set; }
        public ApplicationUser ChangedBy { get; private set; }

        private SuspendedStatusHistory() { }
        public SuspendedStatusHistory(SuspendedPayment suspendedPayment, ApplicationUser user)
        {
            PaymentId = suspendedPayment.Id;
            Comment = suspendedPayment.Comment;
            StatusId = suspendedPayment.StatusId;
            ChangedDate = user.UserDate;
            ChangedById = user.Id;
        }
    }
}

