﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class AuthNetProfile : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public string CustomerProfileId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }

        public Risk Risk { get; private set; }
        public List<AuthNetAccount> AuthNetAccount { get; private set; }

        private AuthNetProfile() { }
        public AuthNetProfile(Guid riskId, string customerProfileId, ApplicationUser user, List<AuthNetAccount> authNetAccount)
        {
            if (riskId == null)
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(riskId)}");
            }
            if (string.IsNullOrWhiteSpace(customerProfileId))
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(customerProfileId)}");
            }
            if (authNetAccount == null || !authNetAccount.Any())
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(authNetAccount)}");
            }

            RiskId = riskId;
            CustomerProfileId = customerProfileId;
            AuthNetAccount = authNetAccount;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
        }
    }
}
