﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class PaymentAccount : BaseEntity<Guid>
    {
        public Guid PaymentProfileId { get; private set; }
        public string InstrumentTypeId { get; private set; }
        public string Description { get; private set; }
        public bool IsDefault { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public string CreatedById { get; private set; }
        public Guid? PayerId { get; private set; }
        public bool InsuredAgree { get; private set; }
        public bool UwAgree { get; private set; }


        public InstrumentType InstrumentType { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public Customer Payer { get; private set; }

        private PaymentAccount() { }
        public PaymentAccount(Guid paymentProfileId, string description, bool isDefault, bool insuredAgree, bool uwAgree,
            ApplicationUser user, InstrumentType instrument, Customer payer)
        {
            if (paymentProfileId == null)
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(paymentProfileId)}");
            }
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(description)}");
            }
            if (!instrument.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_INSTRUMENT_TYPE);
            }

            PaymentProfileId = paymentProfileId;
            InstrumentTypeId = instrument.Id;
            Description = description;
            IsDefault = isDefault;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
            UpdatedDate = user.UserDate;
            Payer = payer;
            InsuredAgree = insuredAgree;
            UwAgree = uwAgree;
        }

        public void SetUpdatedDate(DateTime date)
        {
            UpdatedDate = date;
        }
    }
}
