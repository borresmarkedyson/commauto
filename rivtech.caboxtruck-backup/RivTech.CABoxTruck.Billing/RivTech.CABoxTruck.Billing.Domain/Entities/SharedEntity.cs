﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SharedEntity : BaseEntity<string>
    {
        public static readonly SharedEntity CentauriInvoiceNumber = new SharedEntity("CentauriInvoiceNumber", null);
        public static readonly SharedEntity BoxtruckInvoiceNumber = new SharedEntity("BoxtruckInvoiceNumber", null);
        public static readonly SharedEntity CentauriFileSequenceNumber = new SharedEntity("CentauriFileSequenceNumber", null);
        public static readonly SharedEntity CentauriLastFileSequenceNumberGeneration = new SharedEntity("CentauriLastFileSequenceNumberGeneration", null);
        public static readonly SharedEntity CentauriCheckNumber = new SharedEntity("CentauriCheckNumber", null);

        public string Value { get; set; }

        public SharedEntity(string id, string value)
        {
            Id = id;
            Value = value;
        }

        private SharedEntity() { }

    }
}
