﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedCheck : BaseEntity<Guid>
    {
        public Guid SuspendedPaymentId { get; private set; }
        public string Check { get; private set; }
        public decimal? CheckAmount { get; private set; }
        public string CheckRoutingNum { get; private set; }
        public string CheckAccountNum { get; private set; }
        public string CheckNum { get; private set; }

        public SuspendedPayment SuspendedPayment { get; private set; }
        public SuspendedCheck(string check, decimal? checkAmount, string checkRoutingNum, string checkAccountNum, string checkNum)
        {
            Check = check;
            CheckAmount = checkAmount;
            CheckRoutingNum = checkRoutingNum;
            CheckAccountNum = checkAccountNum;
            CheckNum = checkNum;
        }
    }
}
