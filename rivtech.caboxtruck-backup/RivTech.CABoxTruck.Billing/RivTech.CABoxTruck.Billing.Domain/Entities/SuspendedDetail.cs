﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedDetail : BaseEntity<Guid>
    {
        public Guid SuspendedPaymentId { get; private set; }
        public string TransactionNum { get; private set; }
        public string LockboxNum { get; private set; }
        public string Batch { get; private set; }
        public string BatchItem { get; private set; }
        public string Number { get; private set; }
        public string EnvelopeNum { get; private set; }
        public string Envelope { get; private set; }
        public string InvoicePage { get; private set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public DateTime? SettlementDate { get; set; }
        public string GroupId { get; set; }
        public string GroupDescription { get; set; }


        public SuspendedPayment SuspendedPayment { get; private set; }
        public SuspendedDetail(string transactionNum, string lockboxNum, string batch, string batchItem,
            string number, string envelopeNum, string envelope, string invoicePage)
        {
            TransactionNum = transactionNum;
            LockboxNum = lockboxNum;
            Batch = batch;
            BatchItem = batchItem;
            Number = number;
            EnvelopeNum = envelopeNum;
            Envelope = envelope;
            InvoicePage = invoicePage;
        }
    }
}
