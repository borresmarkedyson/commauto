﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedPayment : BaseEntity<Guid>
    {
        public DateTime ReceiptDate { get; private set; }
        public string Time { get; private set; }
        public string PolicyNumber { get; private set; }
        public string PolicyId { get; private set; }
        public decimal Amount { get; private set; }
        public string SourceId { get; private set; }
        public string ReasonId { get; private set; }
        public string StatusId { get; private set; }
        public string Comment { get; private set; }
        public bool CheckRequested { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }
        public Guid? PaymentId { get; private set; }
        public string DirectBillCarrierId { get; private set; }

        public SuspendedDetail SuspendedDetail { get; private set; }
        public SuspendedCheck SuspendedCheck { get; private set; }
        public SuspendedImage SuspendedImage { get; private set; }
        public SuspendedSource Source { get; private set; }
        public SuspendedReason Reason { get; private set; }
        public SuspendedStatus Status { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public Payment Payment { get; private set; }
        public SuspendedPayer SuspendedPayer { get; private set; }
        public SuspendedDirectBillCarrier DirectBillCarrier { get; private set; }

        private SuspendedPayment() { }
        public SuspendedPayment(DateTime receiptDate, string time, string policyNumber,
             string policyId, decimal amount, string comment, ApplicationUser user, 
             SuspendedSource source, SuspendedReason reason, SuspendedStatus status,
             SuspendedDetail suspendedDetail, SuspendedCheck suspendedCheck, SuspendedImage suspendedImage,
             SuspendedPayer suspendedPayer, SuspendedDirectBillCarrier directBillCarrier)
        {
            if (amount <= 0)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO);
            }
            if (!source.IsValid())
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_SOURCE_INVALID);
            }
            if (reason != null && !reason.IsValid())
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_REASON_INVALID);
            }
            if (!status.IsValid())
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_STATUS_INVALID);
            }
            if (directBillCarrier != null && !directBillCarrier.IsValid())
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_CARRIER_INVALID);
            }

            ReceiptDate = receiptDate;
            Time = time;
            PolicyNumber = policyNumber;
            PolicyId = policyId;
            Amount = amount;
            Comment = comment;
            SourceId = source.Id;
            ReasonId = reason == null ? null : reason.Id;
            StatusId = status.Id;
            SuspendedDetail = suspendedDetail;
            SuspendedCheck = suspendedCheck;
            SuspendedImage = suspendedImage;
            CheckRequested = false;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
            SuspendedPayer = suspendedPayer;
            DirectBillCarrierId = directBillCarrier == null ? null : directBillCarrier.Id;
        }
    }
}
