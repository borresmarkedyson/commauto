﻿using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class BillingSummaryDetails
    {
        public string Description { get; set; }
        public decimal Written { get; set; }
        public decimal Billed { get; set; }
        public decimal Paid { get; set; }
        public decimal Balance { get; set; }
    }
}
