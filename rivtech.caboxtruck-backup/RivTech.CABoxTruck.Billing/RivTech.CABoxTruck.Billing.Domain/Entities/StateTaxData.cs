﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class StateTaxData
    {
        public string StateCode { get; set; }
        public string TaxType { get; set; }
        public string TaxSubtype { get; set; }
        public decimal Rate { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
