﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class PaymentProfile : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Email { get; private set; }
        public bool IsRecurringPayment { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }
        
        public Risk Risk { get; private set; }
        public List<PaymentAccount> PaymentAccounts { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }

        private PaymentProfile() { }
        public PaymentProfile(Guid riskId, string firstName, string lastName, string phoneNumber, string email, bool isRecurringPayment, ApplicationUser user, PaymentAccount paymentAccount)
        {
            if (riskId == null)
            {
                throw new ParameterException(ExceptionMessages.AMOUNT_ZERO);
            }
            if (string.IsNullOrWhiteSpace(firstName) || !Regex.IsMatch(firstName, RegexPatterns.NAME))
            {
                throw new ParameterException(ExceptionMessages.FIRSTNAME_INVALID);
            }
            if (string.IsNullOrWhiteSpace(lastName) || !Regex.IsMatch(lastName, RegexPatterns.NAME))
            {
                throw new ParameterException(ExceptionMessages.LASTNAME_INVALID);
            }
            if (string.IsNullOrWhiteSpace(email) || !Regex.IsMatch(email, RegexPatterns.EMAIL))
            {
                throw new ParameterException(ExceptionMessages.EMAIL_INVALID);
            }

            IsRecurringPayment = isRecurringPayment;
            RiskId = riskId;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Email = email;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;

            PaymentAccounts = new List<PaymentAccount> { paymentAccount };
        }
    }
}
