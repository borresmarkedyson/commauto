﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class InvoiceDetail : BaseEntity<Guid>
    {
        public Guid InvoiceId { get; private set; }
        public Invoice Invoice { get; private set; }
        public string AmountSubTypeId { get; private set; }
        public AmountSubType AmountSubType { get; private set; }
        public decimal InvoicedAmount { get; private set; }

        public InvoiceDetail(decimal invoicedAmount, AmountSubType amountSubType)
        {
            if (amountSubType == null || !amountSubType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);
            }

            InvoicedAmount = invoicedAmount;
            AmountSubTypeId = amountSubType.Id;
        }

        public void AddAmount(decimal amount)
        {
            InvoicedAmount += amount;
        }

        public void SetAmount(decimal amount)
        {
            InvoicedAmount = amount;
        }

        private InvoiceDetail() { }
    }
}