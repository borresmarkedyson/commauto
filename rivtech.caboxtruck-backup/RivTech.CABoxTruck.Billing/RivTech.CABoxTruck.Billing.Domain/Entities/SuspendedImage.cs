﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedImage : BaseEntity<Guid>
    {
        public Guid SuspendedPaymentId { get; private set; }
        public string CheckImg { get; private set; }
        public string EnvelopeImg { get; private set; }
        public string InvoiceImg { get; private set; }

        public SuspendedPayment SuspendedPayment { get; private set; }
        public SuspendedImage(string checkImg, string envelopeImg, string invoiceImg)
        {
            CheckImg = checkImg;
            EnvelopeImg = envelopeImg;
            InvoiceImg = invoiceImg;
        }
    }
}
