﻿namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class PremiumFeeAndTax
    {
        public decimal Premium { get; set; }
        public decimal PremiumFee { get; set; }
        public decimal TransactionFee { get; set; }
        public decimal Fees { get; set; }
        public decimal Tax { get; set; }
        public decimal TransactionTax { get; set; }
        public decimal TotalAmount { get { return Premium + Fees + Tax; } }

        public PremiumFeeAndTax(decimal premium, decimal fees, decimal tax)
        {
            Premium = premium;
            Fees = fees;
            Tax = tax;
        }

        public PremiumFeeAndTax(decimal premium, decimal premiumFee, decimal transactionFee, decimal tax, decimal transactionTax)
        {
            Premium = premium;
            PremiumFee = premiumFee;
            TransactionFee = transactionFee;
            TransactionTax = transactionTax;
            Tax = tax;
            Fees = premiumFee + transactionFee;
        }
    }
}
