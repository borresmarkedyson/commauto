﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Customer : BaseEntity<Guid>
    {
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public string NameSuffix { get; private set; }
        public string FullName { get; private set; }
        public string Email { get; private set; }
        public string StreetAddress1 { get; private set; }
        public string StreetAddress2 { get; private set; }
        public string StreetAddress { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }

        private Customer() { }

        public void SetEmail(string email)
        {
            Email = email;
        }
    }
}

