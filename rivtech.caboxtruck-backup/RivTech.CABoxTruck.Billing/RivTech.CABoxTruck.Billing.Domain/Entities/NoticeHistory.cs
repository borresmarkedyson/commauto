﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class NoticeHistory : BaseEntity<Guid>
    {
        public DateTime? FlatCancelNopcDate { get; set; }
        public DateTime? NopcNoticeDate { get; set; }
        public DateTime? CancellationEffectiveDate { get; set; }
        public DateTime? CancellationProcessDate { get; set; }
        public DateTime? BalanceDueNoticeDate { get; set; }
        public Guid RiskId { get; set; }
        public Risk Risk { get; set; }
    }
}
