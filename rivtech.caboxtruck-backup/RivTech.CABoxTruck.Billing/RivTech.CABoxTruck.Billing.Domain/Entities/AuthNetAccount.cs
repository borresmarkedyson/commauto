﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class AuthNetAccount : BaseEntity<Guid>
    {
        public Guid PaymentAccountId { get; private set; }
        public Guid AuthNetProfileId { get; private set; }
        public string CustomerPaymentId { get; private set; }

        public PaymentAccount PaymentAccount { get; private set; }

        private AuthNetAccount() { }
        public AuthNetAccount(Guid paymentAccountId, string customerPaymentId)
        {
            if (paymentAccountId == null)
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(paymentAccountId)}");
            }
            if (string.IsNullOrWhiteSpace(customerPaymentId))
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(customerPaymentId)}");
            }

            PaymentAccountId = paymentAccountId;
            CustomerPaymentId = customerPaymentId;
        }

        public AuthNetAccount(Guid paymentAccountId, Guid authNetProfileId, string customerPaymentId)
        {
            if (paymentAccountId == null)
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(paymentAccountId)}");
            }
            if (authNetProfileId == null)
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(authNetProfileId)}");
            }
            if (string.IsNullOrWhiteSpace(customerPaymentId))
            {
                throw new ParameterException($"{ExceptionMessages.INVALID} {nameof(customerPaymentId)}");
            }

            PaymentAccountId = paymentAccountId;
            AuthNetProfileId = authNetProfileId;
            CustomerPaymentId = customerPaymentId;
        }
    }
}
