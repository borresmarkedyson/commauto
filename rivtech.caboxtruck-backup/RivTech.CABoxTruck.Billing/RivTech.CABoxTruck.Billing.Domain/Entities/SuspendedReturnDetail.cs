﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedReturnDetail : BaseEntity<Guid>
    {
        public Guid SuspendedPaymentId { get; private set; }
        public Guid PayeeId { get; private set; }
        public string CheckNumber { get; private set; }
        public DateTime? CheckIssueDate { get; private set; }
        public string CheckCopyLink { get; private set; }
        public DateTime? CheckClearDate { get; private set; }
        public DateTime? CheckEscheatDate { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }

        public SuspendedPayee Payee { get; private set; }
        public SuspendedPayment SuspendedPayment { get; private set; }

        private SuspendedReturnDetail() { }
        public SuspendedReturnDetail(Guid suspendedPaymentId, SuspendedPayee payee, ApplicationUser user)
        {
            if (payee == null)
            {
                throw new ParameterException(ExceptionMessages.SUSPENDED_PAYEE_REQUIRED);
            }

            SuspendedPaymentId = suspendedPaymentId;
            Payee = payee;
            CreatedById = user.Id;
            CreatedDate = user.UserDate;
        }
    }
}
