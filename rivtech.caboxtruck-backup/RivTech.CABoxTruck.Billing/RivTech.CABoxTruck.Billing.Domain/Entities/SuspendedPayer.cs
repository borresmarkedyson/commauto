﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedPayer : BaseEntity<Guid>
    {
        public Guid SuspendedPaymentId { get; private set; }
        public string FirstName { get; private set; }
        public string MiddleInitial { get; private set; }
        public string LastName { get; private set; }
        public string Address1 { get; private set; }
        public string Address2 { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }
        
        public SuspendedPayment SuspendedPayment { get; private set; }

        private SuspendedPayer() { }
    }
}
