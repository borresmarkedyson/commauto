﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class FeeKindData
    {
        public string Id { get; }
        public string Description { get; }
        public bool IsActive { get; } = true;
    }
}
