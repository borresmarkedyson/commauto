﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities.Logging
{
    public class AppLog : BaseEntity<long>, ICreatedDate
    {
        public string UserName { get; set; }
        public string Message { get; set; }
        public string StatusCode { get; set; }
        public string JsonMessage { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
