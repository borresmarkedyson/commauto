﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities.Logging
{
    public class AuditLog : BaseEntity<long>, ICreatedDate
    {
        public string UserName { get; set; }
        public string Description { get; set; }
        public int KeyID { get; set; }
        public string AuditType { get; set; }
        public string Action { get; set; }
        public string Method { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
