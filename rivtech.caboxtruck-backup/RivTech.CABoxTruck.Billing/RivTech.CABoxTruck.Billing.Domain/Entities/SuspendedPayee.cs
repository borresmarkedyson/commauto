﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Text.RegularExpressions;
namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class SuspendedPayee : BaseEntity<Guid>
    {
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }

        private SuspendedPayee() { }

        public SuspendedPayee(string name, string address, string city, string state, string zip)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ParameterException(ExceptionMessages.NAME_REQUIRED);
            }

            Name = name;
            Address = address;
            City = city;
            State = state;
            Zip = zip;
        }
    }
}
