﻿using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class BillingSummary
    {
        private readonly List<Invoice> _previousNonVoidInvoices;
        private List<Installment> _installments;
        private List<Transaction> _transactions;
        private List<Payment> _payments;
        private DateTime _currentDate;
        private StateTaxData _stateTaxData { get; set; }
        private decimal _taxRate { get; set; }

        public Risk Risk { get; set; }
        public List<InstallmentAndInvoice> InstallmentAndInvoices { get { return ComputeInstallmentAndInvoices(); } }
        public decimal TotalPremium { get { return ComputePremiumsFeesAndTaxesFromTransactions().TotalAmount; } }
        public decimal PremiumAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Billed { get { return ComputeTotalInvoiced(); } }
        public decimal Paid { get { return ComputeTotalPayment(); } }
        public decimal Balance { get { return Billed - Paid; } }
        public string PaymentPlan { get { return Risk?.PaymentPlan != null ? Risk.PaymentPlan.Description : null; } }
        public decimal PastDueAmount { get { return ComputePastDueAmount(); } }
        public DateTime? PastDueDate { get { return GetNextDueDate(); } }
        public decimal PayoffAmount { get { return ComputeTotalTransactionBeforeCancellation() - Paid; } }
        public DateTime? EquityDate { get { return ComputeEquityDate(); } }
        public DateTime? ExpirationDate { get { return Risk?.EffectiveDate != null ? Risk?.EffectiveDate.AddYears(1) : null; } }
        public DateTime? EffectiveDate { get { return Risk?.EffectiveDate != null ? Risk?.EffectiveDate : null; } }
        public decimal MinimumToBindRenewal { get; set; }
        public decimal PayoffRenewal { get; set; }
        public PremiumFeeAndTax RemainingBalance { get { return GetRemainingBalance(); } }
        public PremiumFeeAndTax UnpaidTransactions { get { return GetUpaidTransactions(); } }
        public decimal PastDueAmountBeforeCancellation { get { return GetPastDueInvoicesExcludingCancellationAndReinstatement() - Paid; } }
        public decimal AmountNeededToReinstate { get { return Paid > 0 ? PastDueAmountBeforeCancellation : GetDepositAmount(); } }
        public bool IsDepositFullyPaid { get { return Paid >= GetDepositAmount(); } }
        public List<BillingSummaryDetails> BillingSummaryDetails { get { return GetBillingSummaryDetails(); } }
        public decimal? TotalCommissionWritten { get; set; }
        public decimal? TotalCommissionBilled { get; set; }
        public decimal? TotalCommissionPaid { get; set; }
        public decimal? TotalCommissionBalance { get; set; }
        public decimal? ALCommission { get { return Risk?.AlpCommission != null ? Risk?.AlpCommission.Value : 0m; } }
        public decimal? PDCommission { get { return Risk?.PdpCommission != null ? Risk?.PdpCommission.Value : 0m; } }

        public BillingSummary(Risk risk)
        {
            Risk = risk;
        }

        public BillingSummary(Risk risk, List<Transaction> transactions, List<Installment> installments,
            List<Invoice> invoices, List<Payment> payments, DateTime currentDate)
        {
            Risk = risk;
            _transactions = transactions;
            _installments = installments;
            _previousNonVoidInvoices = invoices != null ? invoices.Where(x => x.VoidDate == null).ToList() : null;
            _payments = payments;
            _currentDate = currentDate.Date;
        }

        public BillingSummary(Risk risk, List<Transaction> transactions, List<Installment> installments,
            List<Invoice> invoices, List<Payment> payments, DateTime currentDate, decimal taxRate)
        {
            Risk = risk;
            _transactions = transactions;
            _installments = installments;
            _previousNonVoidInvoices = invoices != null ? invoices.Where(x => x.VoidDate == null).ToList() : null;
            _payments = payments;
            _currentDate = currentDate.Date;
            _taxRate = taxRate;
        }

        public void SetInstallments(List<Installment> installments)
        {
            _installments = installments;
        }

        public void SetTransactions(List<Transaction> transactions)
        {
            _transactions = transactions;
        }

        public void SetPayments(List<Payment> payments)
        {
            _payments = payments;
        }

        private List<InstallmentAndInvoice> ComputeInstallmentAndInvoices()
        {
            List<InstallmentAndInvoice> installmentAndInvoices = new List<InstallmentAndInvoice>();
            bool isPreviousInstallmentInvoiced = false;
            DateTime previousInvoiceDate = _installments[0].InvoiceOnDate;
            decimal previousTotalDue = 0;

            foreach (var installment in _installments.Where(x => string.Equals(x.InstallmentTypeId, InstallmentType.Deposit.Id, StringComparison.OrdinalIgnoreCase)).ToList())
            {
                PremiumFeeAndTax billed = ComputePremiumsFeesAndTaxesFromInvoice(installment.Invoice);

                if (installment.IsSkipped || (installment.VoidDate != null && !installment.IsInvoiced))
                {
                    continue;
                }


                if (installment != null && installment.IsInvoiced && installment.Invoice != null)
                {
                    var installmentAndInvoice = new InstallmentAndInvoice
                    {
                        InvoiceId = installment.InvoiceId,
                        InstallmentId = installment.Id,
                        Status = installment.VoidDate == null ? "Billed" : "Void",
                        InstallmentType = installment.InstallmentType.Description,
                        Premium = billed.Premium,
                        Fee = billed.Fees,
                        Tax = billed.Tax,
                        Balance = installment.Invoice.PreviousBalance,
                        BillDate = installment.Invoice.InvoiceDate,
                        DueDate = installment.Invoice.DueDate,
                        InvoiceNumber = installment.Invoice.InvoiceNumber,
                        TotalBilled = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals(),
                        TotalDue = billed.Premium + billed.Fees + billed.Tax + installment.Invoice.PreviousBalance
                    };

                    if (installment.InstallmentTypeId == InstallmentType.Installment.Id && installment.VoidDate == null && installmentAndInvoice.TotalDue <= 0)
                    {
                        installmentAndInvoice.Status = "Skipped";
                    }

                    installmentAndInvoices.Add(installmentAndInvoice);
                    isPreviousInstallmentInvoiced = true;
                }
            }

            foreach (var installment in _installments.Where(x => string.Equals(x.InstallmentTypeId, InstallmentType.Installment.Id, StringComparison.OrdinalIgnoreCase)).ToList())
            {
                PremiumFeeAndTax billed = ComputePremiumsFeesAndTaxesFromInvoice(installment.Invoice);

                if (installment.IsSkipped || (installment.VoidDate != null && !installment.IsInvoiced))
                {
                    continue;
                }
                if (installment != null && installment.IsInvoiced && installment.Invoice != null)
                {
                    var installmentAndInvoice = new InstallmentAndInvoice
                    {
                        InvoiceId = installment.InvoiceId,
                        InstallmentId = installment.Id,
                        Status = installment.VoidDate == null ? "Billed" : "Void",
                        InstallmentType = installment.InstallmentType.Description,
                        Premium = billed.Premium,
                        Fee = billed.Fees,
                        Tax = billed.Tax,
                        Balance = installment.Invoice.PreviousBalance,
                        BillDate = installment.InvoiceOnDate,
                        DueDate = installment.DueDate,
                        InvoiceNumber = installment.Invoice.InvoiceNumber,
                        TotalBilled = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals(),
                        TotalDue = billed.Premium + billed.Fees + billed.Tax + installment.Invoice.PreviousBalance
                    };

                    if (installment.InstallmentTypeId == InstallmentType.Installment.Id && installment.VoidDate == null && installmentAndInvoice.TotalDue <= 0)
                    {
                        installmentAndInvoice.Status = "Skipped";
                    }

                    installmentAndInvoices.Add(installmentAndInvoice);
                    isPreviousInstallmentInvoiced = true;
                }
            }

            foreach (var installment in _installments.Where(x => !string.Equals(x.InstallmentTypeId, InstallmentType.Deposit.Id, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.InvoiceOnDate).ToList())
            {
                if (installment.IsSkipped || (installment.VoidDate != null && !installment.IsInvoiced))
                {
                    continue;
                }

                if (installment != null && !installment.IsInvoiced && Risk.PolicyStatusId != PolicyStatus.Cancelled.Id)
                {
                    decimal initialTotalPremium = ComputeInitialTotalPremiumsFromTransactions();
                    PremiumFeeAndTax premiumFeeAndTaxFromTransactions = ComputeActivePremiumsFeesAndTaxesFromTransactions(previousInvoiceDate, installment.InvoiceOnDate, isPreviousInstallmentInvoiced);
                    (PremiumFeeAndTax, decimal) premiumFeeAndTaxFromPreviousInvoices = ComputePremiumsFeesAndTaxesFromPreviousInvoices();

                    int countInstallmentsNotInvoiced = CountInstallmentsNotInvoiced();

                    decimal premium = ((premiumFeeAndTaxFromTransactions.Premium - premiumFeeAndTaxFromPreviousInvoices.Item1.Premium) / countInstallmentsNotInvoiced);
                    decimal fee = isPreviousInstallmentInvoiced ? premiumFeeAndTaxFromTransactions.Fees - premiumFeeAndTaxFromPreviousInvoices.Item1.Fees : 0;
                    decimal tax = isPreviousInstallmentInvoiced ? premiumFeeAndTaxFromTransactions.Tax - premiumFeeAndTaxFromPreviousInvoices.Item1.Tax : 0;
                    decimal balance = 0;

                    if (isPreviousInstallmentInvoiced)
                    {
                        balance = Balance;
                    }
                    else if (previousTotalDue < 0)
                    {
                        balance = previousTotalDue;
                    }
                    else
                    {
                        balance = 0;
                    }

                    decimal totalDue = premium + fee + tax + balance;
                    InstallmentFee installmentFee = new InstallmentFee(initialTotalPremium, premiumFeeAndTaxFromPreviousInvoices.Item1.Premium, countInstallmentsNotInvoiced, _taxRate, premiumFeeAndTaxFromPreviousInvoices.Item2);
                    if (totalDue > 0.10m)
                    {
                        fee += installmentFee.Fee;
                        tax += installmentFee.Tax;
                    }

                    installmentAndInvoices.Add(new InstallmentAndInvoice
                    {
                        InstallmentId = installment.Id,
                        Status = "Future",
                        InstallmentType = installment.InstallmentType.Description,
                        Premium = premium,
                        Fee = fee,
                        Tax = tax,
                        Balance = balance,
                        BillDate = installment.InvoiceOnDate,
                        DueDate = installment.DueDate,
                        InvoiceNumber = null,
                        FutureInstallmentFee = totalDue > 0 ? installmentFee.Fee : 0,
                        TotalBilled = premium.RoundTo2Decimals() + fee.RoundTo2Decimals() + tax.RoundTo2Decimals(),
                        TotalDue = premium + fee + tax + balance.RoundTo2Decimals()
                    });

                    previousTotalDue = premium + fee + tax + balance.RoundTo2Decimals();

                    isPreviousInstallmentInvoiced = false;
                    previousInvoiceDate = installment.InvoiceOnDate;
                }
            }

            foreach (var installment in _installments.Where(x => string.Equals(x.InstallmentTypeId, InstallmentType.Endorsement.Id, StringComparison.OrdinalIgnoreCase)).ToList())
            {
                PremiumFeeAndTax billed = ComputePremiumsFeesAndTaxesFromInvoice(installment.Invoice);

                if (installment.IsSkipped || (installment.VoidDate != null && !installment.IsInvoiced))
                {
                    continue;
                }
                if (installment != null && installment.IsInvoiced && installment.Invoice != null)
                {
                    var installmentAndInvoice = new InstallmentAndInvoice
                    {
                        InvoiceId = installment.InvoiceId,
                        InstallmentId = installment.Id,
                        Status = installment.VoidDate == null ? "Billed" : "Void",
                        InstallmentType = installment.InstallmentType.Description,
                        Premium = billed.Premium,
                        Fee = billed.Fees,
                        Tax = billed.Tax,
                        Balance = installment.Invoice.PreviousBalance,
                        BillDate = installment.Invoice.InvoiceDate,
                        DueDate = installment.Invoice.DueDate,
                        InvoiceNumber = installment.Invoice.InvoiceNumber,
                        TotalBilled = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals(),
                        TotalDue = billed.Premium + billed.Fees + billed.Tax + installment.Invoice.PreviousBalance
                    };

                    if (installment.InstallmentTypeId == InstallmentType.Installment.Id && installment.VoidDate == null && installmentAndInvoice.TotalDue <= 0)
                    {
                        installmentAndInvoice.Status = "Skipped";
                    }

                    installmentAndInvoices.Add(installmentAndInvoice);
                    isPreviousInstallmentInvoiced = false;
                }
            }


            foreach (var invoice in _previousNonVoidInvoices)
            {
                bool isInvoiceTiedToInstallment = false;
                Guid installmentId = Guid.Empty;

                foreach (var installment in _installments)
                {
                    if (installment.InvoiceId == invoice.Id)
                    {
                        installmentId = installment.Id;
                        isInvoiceTiedToInstallment = true;
                        break;
                    }
                }

                if (!isInvoiceTiedToInstallment)
                {
                    PremiumFeeAndTax billed = ComputePremiumsFeesAndTaxesFromInvoice(invoice);
                    installmentAndInvoices.Add(new InstallmentAndInvoice
                    {
                        InvoiceId = invoice.Id,
                        InstallmentId = installmentId,
                        Status = invoice.VoidDate == null ? "Billed" : "Void",
                        InstallmentType = "Cancellation",
                        Premium = billed.Premium,
                        Fee = billed.Fees,
                        Tax = billed.Tax,
                        Balance = invoice.PreviousBalance,
                        BillDate = invoice.InvoiceDate,
                        DueDate = invoice.DueDate,
                        InvoiceNumber = invoice.InvoiceNumber,
                        IsFlatCancelInvoice = invoice.IsFlatCancelInvoice,
                        TotalBilled = billed.Premium.RoundTo2Decimals() + billed.Fees.RoundTo2Decimals() + billed.Tax.RoundTo2Decimals(),
                        TotalDue = billed.Premium + billed.Fees + billed.Tax + invoice.PreviousBalance
                    });
                }
            }

            installmentAndInvoices = installmentAndInvoices.OrderBy(x => x.InvoiceNumber == null)
                .ThenBy(x => x.InvoiceNumber).ThenBy(x => x.BillDate).ToList();

            return installmentAndInvoices;
        }

        private PremiumFeeAndTax ComputePremiumsFeesAndTaxesFromInvoice(Invoice invoice)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            if (invoice?.InvoiceDetails != null)
            {
                foreach (var invoiceDetail in invoice.InvoiceDetails)
                {
                    if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                    {
                        totalPremium += invoiceDetail.InvoicedAmount;
                    }
                    else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                        || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                    {
                        totalFees += invoiceDetail.InvoicedAmount;
                    }
                    else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                        || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                    {
                        tax += invoiceDetail.InvoicedAmount;
                    }
                }
            }
            return new PremiumFeeAndTax(totalPremium, totalFees, tax);
        }

        private PremiumFeeAndTax ComputePremiumsFeesAndTaxesFromTransactions()
        {
            PremiumAmount = 0;
            FeeAmount = 0;
            TaxAmount = 0;
            if (_transactions != null)
            {
                foreach (var transaction in _transactions)
                {
                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                            {
                                PremiumAmount += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                            {
                                FeeAmount += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                            {
                                TaxAmount += transactionDetail.Amount;
                            }
                        }
                    }
                }
            }
            return new PremiumFeeAndTax(PremiumAmount, FeeAmount, TaxAmount);
        }

        private PremiumFeeAndTax ComputeActivePremiumsFeesAndTaxesFromTransactions(DateTime previousInvoiceDate, DateTime currentInvoiceDate, bool isPreviousInstallmentInvoiced)
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            if (_transactions != null)
            {
                foreach (var transaction in _transactions)
                {
                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                            {
                                totalPremium += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id)
                            {
                                totalFees += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                                && isPreviousInstallmentInvoiced
                                && previousInvoiceDate.Date >= transaction.EffectiveDate.Date
                                && transactionDetail.InvoiceDate == null)
                            {
                                totalFees += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                                && currentInvoiceDate.Date >= transaction.EffectiveDate.Date
                                && previousInvoiceDate.Date < transaction.EffectiveDate.Date
                                && transactionDetail.InvoiceDate == null)
                            {
                                totalFees += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                                && transactionDetail.InvoiceDate != null)
                            {
                                totalFees += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                            {
                                tax += transactionDetail.Amount;
                            }
                        }
                    }
                }
            }
            return new PremiumFeeAndTax(totalPremium, totalFees, tax);
        }

        private decimal ComputeInitialTotalPremiumsFromTransactions()
        {
            decimal totalPremium = 0;
            if (_transactions != null)
            {
                foreach (var transaction in _transactions.Where(t => string.Equals(t.TransactionTypeId, TransactionType.New.Id, StringComparison.OrdinalIgnoreCase)))
                {
                    if (transaction.TransactionTypeId == TransactionType.Cancellation.Id ||
                        transaction.TransactionTypeId == TransactionType.Reinstatement.Id)
                    {
                        continue;
                    }

                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                            {
                                totalPremium += transactionDetail.Amount;
                            }
                        }
                    }
                }
            }
            return totalPremium;
        }

        private (PremiumFeeAndTax, decimal) ComputePremiumsFeesAndTaxesFromPreviousInvoices()
        {
            decimal totalPremium = 0;
            decimal totalFees = 0;
            decimal tax = 0;
            decimal totalInstallmentFee = 0;

            if (_previousNonVoidInvoices != null)
            {
                foreach (var invoice in _previousNonVoidInvoices)
                {
                    if (invoice?.InvoiceDetails != null)
                    {
                        foreach (var invoiceDetail in invoice.InvoiceDetails)
                        {
                            if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id)
                            {
                                totalPremium += invoiceDetail.InvoicedAmount;
                            }
                            else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                                || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                            {
                                totalFees += invoiceDetail.InvoicedAmount;

                                if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id
                                       || invoiceDetail?.AmountSubType?.Id == AmountSubType.InstallmentFee.Id)
                                {
                                    totalInstallmentFee += invoiceDetail.InvoicedAmount;
                                }
                            }
                            else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                            {
                                tax += invoiceDetail.InvoicedAmount;
                            }
                        }
                    }
                }
            }
            return (new PremiumFeeAndTax(totalPremium, totalFees, tax), totalInstallmentFee);
        }

        private int CountInstallmentsNotInvoiced()
        {
            return _installments == null ? 0 : _installments.Count(x => !x.IsSkipped && x.VoidDate == null && !x.IsInvoiced);
        }

        private decimal ComputeTotalPayment()
        {
            return _payments == null ? 0 : _payments.Sum(x => x.Amount);
        }

        private decimal ComputeTotalInvoiced()
        {
            return _previousNonVoidInvoices == null ? 0 : _previousNonVoidInvoices.Sum(x => x.CurrentAmountInvoiced);
        }

        private decimal ComputePastDueInvoice()
        {
            return _previousNonVoidInvoices == null ? 0 : _previousNonVoidInvoices
                .Where(x => x.DueDate.Date < _currentDate)
                .Sum(x => x.CurrentAmountInvoiced);
        }

        private DateTime? GetNextDueDate()
        {
            if (_previousNonVoidInvoices == null)
            {
                return null;
            }
            else
            {
                DateTime? nextDueDate = null;

                var latestInvoiceBeforeToday = _previousNonVoidInvoices.Where(x => x.DueDate.Date < _currentDate).OrderBy(x => x.DueDate).LastOrDefault();
                if (latestInvoiceBeforeToday != null)
                {
                    nextDueDate = latestInvoiceBeforeToday.DueDate.Date;
                }

                return nextDueDate;
            }
        }

        private DateTime? ComputeEquityDate()
        {
            if (Risk == null)
            {
                return null;
            }

            decimal premiumPerDay = ComputePremiumsFeesAndTaxesFromTransactions().Premium /
                (decimal)((Risk.EffectiveDate.Date.AddYears(1) - Risk.EffectiveDate.Date).TotalDays);

            if (premiumPerDay == 0)
            {
                return null;
            }

            decimal paidPremiums = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Premium.Id || y.AmountTypeId == AmountType.Commission.Id).Sum(z => z.Amount));

            long numberOfPaidDays = (long)(paidPremiums / premiumPerDay);

            try
            {
                DateTime equityDate = Risk.EffectiveDate.AddDays(numberOfPaidDays);
                DateTime expirationDate = Risk.EffectiveDate.AddYears(1);

                if (equityDate < Risk.EffectiveDate)
                {
                    return Risk.EffectiveDate;
                }
                else if (equityDate < expirationDate)
                {
                    return equityDate;
                }
                else
                {
                    return expirationDate;
                }
            }
            catch
            {
                return null;
            }
        }

        private PremiumFeeAndTax GetRemainingBalance()
        {
            var previousNonVoidInvoicesBeforeCancellation = _previousNonVoidInvoices.Where(x => _installments.Exists(y => y.InvoiceId == x.Id));

            decimal invoicedPremiums = previousNonVoidInvoicesBeforeCancellation.Sum(x => x.InvoiceDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Premium.Id).Sum(z => z.InvoicedAmount));

            decimal invoicedPremiumFees = previousNonVoidInvoicesBeforeCancellation.Sum(x => x.InvoiceDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.InvoicedAmount));

            decimal invoicedTransactionFees = previousNonVoidInvoicesBeforeCancellation.Sum(x => x.InvoiceDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.InvoicedAmount));

            decimal invoicedTaxes = previousNonVoidInvoicesBeforeCancellation.Sum(x => x.InvoiceDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Tax.Id).Sum(z => z.InvoicedAmount));

            decimal invoicedTransactionTaxes = previousNonVoidInvoicesBeforeCancellation.Sum(x => x.InvoiceDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.InvoicedAmount));


            var transactionsBeforeCancellation = _transactions.Where(x => x.TransactionTypeId != TransactionType.Cancellation.Id
                && x.TransactionTypeId != TransactionType.Reinstatement.Id);

            decimal premiums = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Premium.Id).Sum(z => z.Amount));

            decimal premiumFees = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.Amount));

            decimal transactionFees = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.Amount));

            decimal taxes = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Tax.Id).Sum(z => z.Amount));

            decimal transactionTaxes = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.Amount));

            decimal paidPremiums = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Premium.Id).Sum(z => z.Amount));

            decimal paidPremiumFees = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.Amount));

            decimal paidTransactionFees = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.Amount));

            decimal paidTaxes = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Tax.Id).Sum(z => z.Amount));

            decimal paidTransactionTaxes = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.Amount));


            decimal totalTransactionAmount = premiums + premiumFees + transactionFees + taxes + transactionTaxes;

            decimal totalInvoiceAmount = invoicedPremiums + invoicedPremiumFees + invoicedTransactionFees + invoicedTaxes + invoicedTransactionTaxes;

            decimal totalPaidAmount = paidPremiums + paidPremiumFees + paidTransactionFees + paidTaxes + paidTransactionTaxes;

            if ((totalInvoiceAmount - totalPaidAmount) <= 0 && (totalTransactionAmount - totalPaidAmount) > 0)
            {
                return new PremiumFeeAndTax(premiums - paidPremiums, premiumFees - paidPremiumFees,
                    transactionFees - paidTransactionFees, taxes - paidTaxes, transactionTaxes - paidTransactionTaxes);
            }
            else
            {
                return new PremiumFeeAndTax(invoicedPremiums - paidPremiums, invoicedPremiumFees - paidPremiumFees,
                    invoicedTransactionFees - paidTransactionFees, invoicedTaxes - paidTaxes, invoicedTransactionTaxes - paidTransactionTaxes);
            }
        }

        private PremiumFeeAndTax GetUpaidTransactions()
        {
            var transactionsBeforeCancellation = _transactions.Where(x => x.TransactionTypeId != TransactionType.Cancellation.Id
                && x.TransactionTypeId != TransactionType.Reinstatement.Id);

            decimal premiums = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Premium.Id).Sum(z => z.Amount));

            decimal premiumFees = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.Amount));

            decimal transactionFees = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.Amount));

            decimal taxes = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.Tax.Id).Sum(z => z.Amount));

            decimal transactionTaxes = transactionsBeforeCancellation.Sum(x => x.TransactionDetails
                .Where(y => y.AmountSubType.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.Amount));


            decimal paidPremiums = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Premium.Id).Sum(z => z.Amount));

            paidPremiums += _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Commission.Id).Sum(z => z.Amount));

            decimal paidPremiumFees = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.PremiumFee.Id).Sum(z => z.Amount));

            decimal paidTransactionFees = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.TransactionFee.Id).Sum(z => z.Amount));

            decimal paidTaxes = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.Tax.Id).Sum(z => z.Amount));

            decimal paidTransactionTaxes = _payments.Sum(x => x.PaymentDetails
                .Where(y => y.AmountTypeId == AmountType.TransactionTax.Id).Sum(z => z.Amount));


            return new PremiumFeeAndTax(premiums - paidPremiums, premiumFees - paidPremiumFees,
                transactionFees - paidTransactionFees, taxes - paidTaxes, transactionTaxes - paidTransactionTaxes);
        }

        private decimal ComputePastDueAmount()
        {
            decimal pastDueAmount = ComputePastDueInvoice() - Paid;
            if (pastDueAmount < 0)
            {
                return 0;
            }
            else
            {
                return pastDueAmount;
            }
        }

        private decimal ComputeTotalTransactionBeforeCancellation()
        {
            decimal totalTransaction = 0;

            if (_transactions != null)
            {
                var transactionsBeforeCancellation = _transactions;

                foreach (var transaction in transactionsBeforeCancellation)
                {
                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            totalTransaction += transactionDetail.Amount;
                        }
                    }
                }
            }
            return totalTransaction;
        }

        private decimal GetPastDueInvoicesExcludingCancellationAndReinstatement()
        {
            decimal pastDueInvoiceAmount = 0;
            foreach (var installment in _installments)
            {
                var invoice = _previousNonVoidInvoices.FirstOrDefault(x => x.Id == installment.InvoiceId);
                if (invoice != null && invoice.DueDate.Date < _currentDate)
                {
                    pastDueInvoiceAmount += invoice.CurrentAmountInvoiced;
                }
            }
            return pastDueInvoiceAmount;
        }

        private decimal GetDepositAmount()
        {
            PremiumFeeAndTax transactionAmounts = ComputePremiumsFeesAndTaxesFromTransactions();

            decimal premiumAmount = transactionAmounts.Premium * (_installments != null && _installments.Count > 0 ? _installments
                                                                    .Where(w => w.InstallmentTypeId.Equals("D", StringComparison.OrdinalIgnoreCase)).Select(i => i.PercentOfOutstanding).FirstOrDefault() : 0.0m);
            decimal feeAmount = transactionAmounts.Fees;
            decimal taxAmount = transactionAmounts.Tax;

            return premiumAmount + feeAmount + taxAmount;
        }

        private List<BillingSummaryDetails> GetBillingSummaryDetails()
        {
            List<BillingSummaryDetails> billingSummaryDetails = new List<BillingSummaryDetails>();

            billingSummaryDetails.Add(new BillingSummaryDetails { Description = AmountSubType.AutoLiabilityPremium.Description, Written = 0.0m, Billed = 0.0m, Paid = 0.0m, Balance = 0.0m });
            billingSummaryDetails.Add(new BillingSummaryDetails { Description = AmountSubType.PhysicalDamagePremium.Description, Written = 0.0m, Billed = 0.0m, Paid = 0.0m, Balance = 0.0m });
            billingSummaryDetails.Add(new BillingSummaryDetails { Description = AmountType.PremiumFee.Description, Written = 0.0m, Billed = 0.0m, Paid = 0.0m, Balance = 0.0m });
            billingSummaryDetails.Add(new BillingSummaryDetails { Description = AmountType.Tax.Description, Written = 0.0m, Billed = 0.0m, Paid = 0.0m, Balance = 0.0m });
            billingSummaryDetails.Add(new BillingSummaryDetails { Description = AmountSubType.PolicyOverpayment.Description, Written = 0.0m, Billed = 0.0m, Paid = 0.0m, Balance = 0.0m });

            billingSummaryDetails = ComputeTransactionBillingSummaryDetails(billingSummaryDetails);

            billingSummaryDetails = ComputeInvoiceBillingSummaryDetails(billingSummaryDetails);

            billingSummaryDetails = ComputePaymentBillingSummaryDetails(billingSummaryDetails);

            billingSummaryDetails = ComputeBalanceBillingSummaryDetails(billingSummaryDetails);

            ComputeCommissionBillingSummaryDetails(billingSummaryDetails);

            return billingSummaryDetails;
        }

        private void ComputeCommissionBillingSummaryDetails(List<BillingSummaryDetails> billingSummaryDetails)
        {

            var totalAlpPremiumWritten = billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written;
            var totalPdpPremiumWritten =
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written;

            var totalAlpPremiumBilled = billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                     .Billed;
            var totalPdpPremiumBilled = billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed;

            var totalAlpPremiumPaid = billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                     .Paid;
            var totalPdpPremiumPaid =
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid;

            var totalAlpPremiumBalance = billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                     .Balance;
            var totalPdpPremiumBalance =
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Balance;

            this.TotalCommissionWritten = ((totalAlpPremiumWritten * ALCommission) + (totalPdpPremiumWritten * PDCommission)) * -1;
            this.TotalCommissionBilled = ((totalAlpPremiumBilled * ALCommission) + (totalPdpPremiumBilled * PDCommission)) * -1;
            this.TotalCommissionPaid = ((totalAlpPremiumPaid * ALCommission) + (totalPdpPremiumPaid * PDCommission)) * -1;
            this.TotalCommissionBalance = ((totalAlpPremiumBalance * ALCommission) + (totalPdpPremiumBalance * PDCommission)) * -1;
        }

        private List<BillingSummaryDetails> ComputeTransactionBillingSummaryDetails(List<BillingSummaryDetails> billingSummaryDetails)
        {
            if (_transactions != null)
            {
                foreach (var transaction in _transactions)
                {
                    if (transaction?.TransactionDetails != null)
                    {
                        foreach (var transactionDetail in transaction.TransactionDetails)
                        {
                            if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id 
                                && (transactionDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.AdditionalInsured.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.WaiverOfSubrogation.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PrimaryNoncontributory.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.CargoPremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumShortRate.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumShortRate.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumShortRate.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumShortRate.Id))
                            {
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id
                               && (transactionDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.TIEndst.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.HPDEndst.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremium.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumAdjustment.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumShortRate.Id
                                    || transactionDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumShortRate.Id))
                            {
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                            {
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.PremiumFee.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written += transactionDetail.Amount;
                            }
                            else if (transactionDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || transactionDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                            {
                                billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.Tax.Description, StringComparison.OrdinalIgnoreCase))
                                    .Written += transactionDetail.Amount;
                            }
                        }
                    }
                }
            }
            return billingSummaryDetails;
        }

        private List<BillingSummaryDetails> ComputeInvoiceBillingSummaryDetails(List<BillingSummaryDetails> billingSummaryDetails)
        {
            foreach (Invoice invoice in _previousNonVoidInvoices)
            {
                if (invoice?.InvoiceDetails != null)
                {
                    foreach (var invoiceDetail in invoice.InvoiceDetails)
                    {
                        if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id
                                && (invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.AdditionalInsured.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.WaiverOfSubrogation.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PrimaryNoncontributory.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumShortRate.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumShortRate.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumShortRate.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumShortRate.Id))
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed += invoiceDetail.InvoicedAmount;
                        }
                        else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id
                           && (invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.TIEndst.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.HPDEndst.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremium.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumAdjustment.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumShortRate.Id
                                    || invoiceDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumShortRate.Id))
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed += invoiceDetail.InvoicedAmount;
                        }
                        else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.PremiumFee.Id
                            || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionFee.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.PremiumFee.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed += invoiceDetail.InvoicedAmount;
                        }
                        else if (invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.Tax.Id
                                || invoiceDetail?.AmountSubType?.AmountTypeId == AmountType.TransactionTax.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.Tax.Description, StringComparison.OrdinalIgnoreCase))
                                    .Billed += invoiceDetail.InvoicedAmount;
                        }
                    }
                }
            }
            return billingSummaryDetails;
        }

        private List<BillingSummaryDetails> ComputePaymentBillingSummaryDetails(List<BillingSummaryDetails> billingSummaryDetails)
        {
            foreach (Payment payment in _payments)
            {
                if (payment?.PaymentDetails != null)
                {
                    foreach (var paymentDetail in payment.PaymentDetails)
                    {
                        if (paymentDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id
                                && (paymentDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.AdditionalInsured.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.WaiverOfSubrogation.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PrimaryNoncontributory.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.CargoPremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.AutoLiabilityPremiumShortRate.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.CargoPremiumShortRate.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.ALManuscriptPremiumShortRate.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.GeneralLiabilityPremiumShortRate.Id))
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountSubType?.AmountTypeId == AmountType.Premium.Id
                           && (paymentDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.TIEndst.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.HPDEndst.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremium.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumAdjustment.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PhysicalDamagePremiumShortRate.Id
                                    || paymentDetail?.AmountSubType?.Id == AmountSubType.PDManuscriptPremiumShortRate.Id))
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountType?.Id == AmountType.PremiumFee.Id
                            || paymentDetail?.AmountType?.Id == AmountType.TransactionFee.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.PremiumFee.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountType?.Id == AmountType.Tax.Id
                                || paymentDetail?.AmountType?.Id == AmountType.TransactionTax.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountType.Tax.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountType?.Id == AmountType.Commission.Id
                                && paymentDetail?.AmountSubTypeId == AmountSubType.ALCommission.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.AutoLiabilityPremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountType?.Id == AmountType.Commission.Id
                                && paymentDetail?.AmountSubTypeId == AmountSubType.PDCommission.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PhysicalDamagePremium.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += paymentDetail.Amount;
                        }
                        else if (paymentDetail?.AmountType?.Id == AmountType.Overpayment.Id
                                && paymentDetail?.AmountSubTypeId == AmountSubType.PolicyOverpayment.Id)
                        {
                            billingSummaryDetails.FirstOrDefault(b => string.Equals(b.Description, AmountSubType.PolicyOverpayment.Description, StringComparison.OrdinalIgnoreCase))
                                    .Paid += Math.Truncate(100 * paymentDetail.Amount) / 100;
                        }
                    }
                }
            }

            return billingSummaryDetails;
        }

        private List<BillingSummaryDetails> ComputeBalanceBillingSummaryDetails(List<BillingSummaryDetails> billingSummaryDetails)
        {
            foreach (BillingSummaryDetails billingSummaryDetail in billingSummaryDetails)
            {
                if (!string.Equals(billingSummaryDetail.Description, AmountSubType.PolicyOverpayment.Description))
                {
                    billingSummaryDetail.Balance = billingSummaryDetail.Billed - billingSummaryDetail.Paid;
                }
                else
                {
                    billingSummaryDetail.Balance = billingSummaryDetail.Paid * -1;
                }
            }
            return billingSummaryDetails;
        }
    }
}
