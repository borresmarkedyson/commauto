﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{

    public class TransactionDetail : BaseEntity<Guid>
    {
        public decimal Amount { get; private set; }
        public string AmountSubTypeId { get; private set; }
        public AmountSubType AmountSubType { get; private set; }
        public string AmountTypeId { get; private set; }
        public Guid? VoidedTransactionDetailId { get; private set; }
        public TransactionDetail VoidedTransactionDetail { get; private set; }
        public DateTime? VoidDate { get; private set; }
        public string VoidedById { get; private set; }
        public ApplicationUser VoidedBy { get; private set; }
        public Guid TransactionId { get; private set; }
        public Transaction Transaction { get; private set; }
        public DateTime? InvoiceDate { get; private set; }
        public string Description { get; private set; }

        public TransactionDetail(decimal amount, string description, AmountSubType amountSubType)
        {
            if (amountSubType == null || !amountSubType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);
            }

            Amount = amount;
            AmountSubTypeId = amountSubType.Id;
            Description = description;
            AmountTypeId = amountSubType.AmountTypeId;
        }

        private TransactionDetail() { }

        public TransactionDetail(TransactionDetail voidedTransactionDetail, ApplicationUser user)
        {
            if (voidedTransactionDetail == null)
            {
                throw new ParameterException(ExceptionMessages.TRANSACTION_NOT_FOUND);
            }

            VoidedTransactionDetail = voidedTransactionDetail;
            Amount = voidedTransactionDetail.Amount * -1;
            AmountSubTypeId = voidedTransactionDetail.AmountSubTypeId;
            voidedTransactionDetail.SetAsVoid(user);
        }

        public void SetAsVoid(ApplicationUser user)
        {
            VoidDate = user.UserDate;
            VoidedById = user.Id;
        }

        public void SetInvoiceDate(DateTime invoiceDate)
        {
            InvoiceDate = invoiceDate;
        }
        public void SetDescription(string description)
        {
            Description = description;
        }

        public void RemoveVoidStatus()
        {
            VoidDate = null;
            VoidedById = null;
        }
    }
}
