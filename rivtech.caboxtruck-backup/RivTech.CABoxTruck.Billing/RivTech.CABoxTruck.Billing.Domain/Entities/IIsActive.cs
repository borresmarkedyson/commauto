﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public interface IIsActive
    {
        bool IsActive { get; set; }
    }
}
