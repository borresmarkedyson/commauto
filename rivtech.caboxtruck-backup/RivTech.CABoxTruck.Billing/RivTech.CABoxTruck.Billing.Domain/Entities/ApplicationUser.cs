﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class ApplicationUser : BaseEntity<string>
    {
        public string Username { get; set; }
        public DateTime UserDate { get; set; }

        public ApplicationUser(string id, string name)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ParameterException(ExceptionMessages.INVALID_USER_ID);
            }

            Id = id;
            Username = name;
        }

        public ApplicationUser() { }
    }
}