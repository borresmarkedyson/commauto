﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class RefundRequest : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public decimal Amount { get; private set; }
        public DateTime? CheckDate { get; private set; }
        public string CheckNumber { get; private set; }
        public Guid? PaymentId { get; private set; }
        public string RefundToTypeId { get; private set; }
        public Guid RefundPayeeId { get; private set; }
        public string RefundReason { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }

        public Risk Risk { get; private set; }
        public Payment Payment { get; private set; }
        public RefundToType RefundToType { get; private set; }
        public RefundPayee RefundPayee { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }

        private RefundRequest() { }

        public RefundRequest(Guid riskId, decimal amount, RefundToType refundToType, RefundPayee refundPayee, ApplicationUser user)
        {
            if (riskId == null) throw new ParameterException(ExceptionMessages.RISK_ID_REQUIRED);
            if (amount <= 0 ) throw new ParameterException(ExceptionMessages.AMOUNT_LESSTHANOREQUALZERO);
            if (!refundToType.IsValid()) throw new ParameterException(ExceptionMessages.REFUNDTOTYPE_INVALID);

            RiskId = riskId;
            Amount = amount;
            RefundToTypeId = refundToType.Id;
            RefundPayee = refundPayee;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
        }

        public void SetCheckDetails(string checkNumber, DateTime checkDate, Guid paymentId)
        {
            CheckNumber = checkNumber;
            CheckDate = checkDate;
            PaymentId = paymentId;
        }
    }
}
