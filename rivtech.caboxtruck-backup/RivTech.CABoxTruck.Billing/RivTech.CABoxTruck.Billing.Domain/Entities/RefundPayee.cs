﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Text.RegularExpressions;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class RefundPayee : BaseEntity<Guid>
    {
        public string Name { get; private set; }
        public string Street1 { get; private set; }
        public string Street2 { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }

        private RefundPayee() { }

        public RefundPayee(string name, string street1, string street2, string city, string state, string zip)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ParameterException(ExceptionMessages.NAME_REQUIRED);
            if (string.IsNullOrWhiteSpace(street1))
                throw new ParameterException(ExceptionMessages.ADDRESS_REQUIRED);
            if (string.IsNullOrWhiteSpace(city))
                throw new ParameterException(ExceptionMessages.CITY_REQUIRED);
            if (string.IsNullOrWhiteSpace(state))
                throw new ParameterException(ExceptionMessages.STATE_REQUIRED);
            if (string.IsNullOrWhiteSpace(zip))
                throw new ParameterException(ExceptionMessages.ZIP_REQUIRED);

            if (name.Length > UtilConstant.NAME_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.NAME_TOOLONG);
            if (street1.Length > UtilConstant.STREET1_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.ADDRESS_REQUIRED);
            if (!string.IsNullOrEmpty(street2) && street2.Length > UtilConstant.STREET2_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.ADDRESS_REQUIRED);
            if (city.Length > UtilConstant.CITY_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.CITY_TOOLONG);
            if (state.Length > UtilConstant.STATE_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.STATE_TOOLONG);
            if (zip.Length > UtilConstant.ZIP_MAXLENGTH)
                throw new ParameterException(ExceptionMessages.ZIP_TOOLONG);

            if (!Regex.IsMatch(name, RegexPatterns.NAME))
                throw new ParameterException(ExceptionMessages.NAME_INVALID);
            //if (!Regex.IsMatch(street1, RegexPatterns.ALPHANUMERIC_SPACE))
            //    throw new ParameterException(ExceptionMessages.STREET1_INVALID);
            //if (!string.IsNullOrEmpty(street2) && !Regex.IsMatch(street2, RegexPatterns.ALPHANUMERIC_SPACE))
            //    throw new ParameterException(ExceptionMessages.STREET2_INVALID);
            if (!Regex.IsMatch(city, RegexPatterns.ALPHANUMERIC_SPACE))
                throw new ParameterException(ExceptionMessages.CITY_INVALID);
            if (!Regex.IsMatch(state, RegexPatterns.ALPHANUMERIC_SPACE))
                throw new ParameterException(ExceptionMessages.STATE_INVALID);
            if (!Regex.IsMatch(zip, RegexPatterns.ZIP))
                throw new ParameterException(ExceptionMessages.ZIP_INVALID);

            Name = name;
            Street1 = street1;
            Street2 = street2;
            City = city;
            State = state;
            Zip = zip;
        }
    }
}

