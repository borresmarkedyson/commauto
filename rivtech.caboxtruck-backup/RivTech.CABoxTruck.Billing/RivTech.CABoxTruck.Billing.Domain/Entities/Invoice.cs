﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Entities
{
    public class Invoice : BaseEntity<Guid>
    {
        public Guid RiskId { get; private set; }
        public string InvoiceNumber { get; private set; }
        public DateTime InvoiceDate { get; private set; }
        public DateTime DueDate { get; private set; }
        public decimal PreviousBalance { get; private set; }
        public decimal CurrentAmountInvoiced { get; private set; }
        public decimal TotalAmountDue { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public string CreatedById { get; private set; }
        public ApplicationUser CreatedBy { get; private set; }
        public DateTime? VoidDate { get; private set; }
        public string VoidedById { get; private set; }
        public ApplicationUser VoidedBy { get; private set; }
        public List<InvoiceDetail> InvoiceDetails { get; private set; }
        public bool IsFlatCancelInvoice { get; private set; }

        public Invoice(Guid riskId, string invoiceNumber, DateTime invoiceDate, DateTime dueDate, decimal previousBalance,
            decimal currentAmountInvoiced, List<InvoiceDetail> invoiceDetails, ApplicationUser user)
        {
            RiskId = riskId;
            InvoiceNumber = invoiceNumber;
            InvoiceDate = invoiceDate;
            DueDate = dueDate;
            PreviousBalance = previousBalance;
            CurrentAmountInvoiced = currentAmountInvoiced;
            TotalAmountDue = PreviousBalance + CurrentAmountInvoiced;
            InvoiceDetails = invoiceDetails;
            CreatedDate = user.UserDate;
            CreatedById = user.Id;
        }

        private Invoice() { }

        public void SetAsVoid(ApplicationUser user)
        {
            VoidDate = user.UserDate;
            VoidedById = user.Id;
        }

        public void SetIsFlatCancelInvoice(bool isFlatCancelInvoice)
        {
            IsFlatCancelInvoice = isFlatCancelInvoice;
        }

        public void ResetVoidStatus()
        {
            VoidDate = null;
            VoidedById = null;
        }
    }
}
