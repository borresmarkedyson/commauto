﻿using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.KeylessEntities
{
    public class RefundRequestRisk
    {
        public Guid RiskId { get; private set; }
        public string RefundToId { get; private set; }

        public RefundToType RefundToType { get; private set; }
    }
}
