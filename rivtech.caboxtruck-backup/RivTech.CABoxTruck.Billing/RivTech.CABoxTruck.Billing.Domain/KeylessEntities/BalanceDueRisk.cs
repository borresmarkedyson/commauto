﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.KeylessEntities
{
    public class BalanceDueRisk
    {
        public Guid RiskId { get; set; }
        public DateTime NoticeDate { get; set; }
        public decimal DepositAmountBilled { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal BalanceDue { get; set; }
        public decimal PayoffAmount { get; set; }
        public bool IsPaymentTransferred { get; set; }
    }
}
