﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Factory.InstallmentSchedules
{
    public class TwoPayInstallmentScheduleFactory : InstallmentScheduleFactory
    {
        private readonly Guid _riskId;
        private readonly DateTime _startDate;
        private readonly ApplicationUser _user;
        private readonly string _appId;
        private readonly bool _isPayPlanChange;
        private readonly decimal? _percentOfOutstanding;
        private int _payCount;

        public TwoPayInstallmentScheduleFactory(Guid riskId, DateTime startDate, string appId, ApplicationUser user, bool isPayPlanChange, decimal? percentOfOutstanding, int payCount)
        {
            _riskId = riskId;
            _startDate = startDate;
            _user = user;
            _appId = appId;
            _isPayPlanChange = isPayPlanChange;
            _percentOfOutstanding = percentOfOutstanding;
            _payCount = payCount;
        }

        public override IList<Installment> GetInstallmentSchedule()
        {
            InstallmentType deposit = InstallmentType.Deposit;
            InstallmentType installment = InstallmentType.Installment;
            InstallmentType billPlanChange = InstallmentType.BillPlanChange;
            var dueDate = _startDate;
            var installementSchedule = new List<Installment>();

            if (_isPayPlanChange)
            {
                installementSchedule.Add(Installment.AddInstallment(_riskId, _startDate, dueDate, billPlanChange, _appId, _user, _percentOfOutstanding));
            }
            else
            {
                installementSchedule.Add(Installment.AddInstallment(_riskId, _startDate, dueDate, deposit, _appId, _user, _percentOfOutstanding));
            }

            for (int i = 1; i <= _payCount; i++)
            {
                installementSchedule.Add(Installment.AddInstallment(_riskId, dueDate.AddDays(180 * i).AddDays(-15), dueDate.AddDays(180 * i), installment, _appId, _user, _percentOfOutstanding));
            }

            return installementSchedule;
        }
    }
}
