﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Factory.InstallmentSchedules
{
    public abstract class InstallmentScheduleFactory
    {
        public abstract IList<Installment> GetInstallmentSchedule();
    }
}
