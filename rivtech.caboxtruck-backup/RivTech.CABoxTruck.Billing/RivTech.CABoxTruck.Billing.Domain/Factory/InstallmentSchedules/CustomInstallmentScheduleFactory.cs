﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.Factory.InstallmentSchedules
{
    public class CustomInstallmentScheduleFactory
    {
        private readonly Guid _riskId;
        private readonly DateTime _invoiceDate;
        private readonly DateTime _dueDate;
        private readonly ApplicationUser _user;
        private readonly string _appId;
        private readonly decimal? _percentOfOutstanding;
        private InstallmentType _installmentType;

        public CustomInstallmentScheduleFactory(Guid riskId, DateTime invoiceDate, DateTime dueDate, ApplicationUser user, decimal? percentOfOutstanding, InstallmentType installmentType)
        {
            _riskId = riskId;
            _invoiceDate = invoiceDate;
            _user = user;
            _percentOfOutstanding = percentOfOutstanding;
            _installmentType = installmentType;
            _dueDate = dueDate;
        }

        public Installment GetInstallment()
        {
            return Installment.AddInstallment(_riskId, _invoiceDate, _dueDate, _installmentType, null, _user, _percentOfOutstanding);
        }
    }
}
