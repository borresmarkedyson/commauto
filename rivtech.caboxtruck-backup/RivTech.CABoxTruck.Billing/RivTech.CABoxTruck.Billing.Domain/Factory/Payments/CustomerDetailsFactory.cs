﻿using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Domain.Factory.Payments
{
    public class CustomerDetailsFactory
    {
        public CustomerDetails Create(PaymentSummaryCCEFT ps)
        {
            if (ps.InstrumentId.Equals(InstrumentType.CreditCard.Id))
            {
                return new CustomerDetails(ps.FirstName ?? "", ps.LastName ?? "", ps.Email,
                   new Address(ps.Address, ps.City, ps.State, ps.ZipCode));
            }
            else if (ps.InstrumentId.Equals(InstrumentType.EFT.Id))
            {
                return new CustomerDetails(ps.Payer, ps.Email,
                   new Address(ps.Address, ps.City, ps.State, ps.ZipCode));
            }
            else
            {
                throw new InvalidOperationException("Invalid instrument type");
            }
        }
    }
}
