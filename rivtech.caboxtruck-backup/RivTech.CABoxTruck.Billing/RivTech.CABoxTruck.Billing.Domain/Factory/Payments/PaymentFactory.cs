﻿using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RivTech.CABoxTruck.Billing.Domain.Entities;

namespace RivTech.CABoxTruck.Billing.Domain.Factory.Payments
{
    public class PaymentFactory
    {
        public PaymentCCEFT Create(PaymentSummaryCCEFT ps)
        {
            switch (ps.InstrumentId)
            {
                case "CC":
                    return new CreditCardPayment(
                        (CardType)ps.CreditCardTypeId,
                        ps.CreditCardNumber,
                        ps.Cvv,
                        ps.ExpirationYear ?? 0,
                        ps.ExpirationMonth ?? 0,
                        ps.Email);

                case "EFT":
                    return new AchPayment(
                        ps.RoutingNumber,
                        ps.AccountNumber,
                        (AchType)ps.AccountType,
                        (AchUsageType)ps.AccountUsageType, 
                        ps.Email);
                default:
                    throw new InvalidOperationException("Invalid instrument type");
            }
        }
    }
}
