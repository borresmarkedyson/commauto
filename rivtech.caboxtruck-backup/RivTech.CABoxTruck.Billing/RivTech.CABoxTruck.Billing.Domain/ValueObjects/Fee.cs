﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using System;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class Fee : ValueObject<Fee>
    {
        public decimal Amount { get; private set; }
        public string Comments { get; private set; }
        public string Description { get; private set; }
        public DateTime AddDate { get; private set; }
        public ApplicationUser AddedBy { get; private set; }
        public DateTime? VoidDate { get; private set; }
        public ApplicationUser VoidedBy { get; private set; }
        public string VoidReason { get; private set; }


        public Fee(decimal amount, string description, ApplicationUser user)
        {
            Amount = amount;
            Description = description;
            AddDate = user.UserDate;
            AddedBy = user;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Amount;
            yield return Description;
        }

        public void SetAsVoid(ApplicationUser user, string voidReason)
        {
            VoidedBy = user;
            VoidReason = voidReason;
            VoidDate = user.UserDate;
        }
    }
}
