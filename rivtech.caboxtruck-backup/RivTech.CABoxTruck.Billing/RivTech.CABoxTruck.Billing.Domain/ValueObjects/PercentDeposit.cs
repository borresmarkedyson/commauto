﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class PercentDeposit : Enumeration
    {
        public static readonly PercentDeposit CentauriMortgagee = new PercentDeposit("CENT-Mortgagee", "Centauri Mortgagee", 1, PaymentPlan.PremiumFinanced, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriFullPay = new PercentDeposit("CENT-Full-Pay", "Centauri Full Pay", 1, PaymentPlan.FullPay, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriTwoPay = new PercentDeposit("CENT-Two-Pay", "Centauri Two Pay", 0.6m, PaymentPlan.TwoPay, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriFourPay = new PercentDeposit("CENT-Four-Pay", "Centauri Four Pay", 0.4m, PaymentPlan.FourPay, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriEightPay = new PercentDeposit("CENT-Eight-Pay", "Centauri Eight Pay", 0.3m, PaymentPlan.EightPay, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriTenPay = new PercentDeposit("CENT-Ten-Pay", "Centauri Ten Pay", 0.25m, PaymentPlan.TenPay, RivtechApplication.Centauri);
        public static readonly PercentDeposit CentauriTenPayRenewal = new PercentDeposit("CENT-Ten-Pay-Renewal", "Centauri Ten Pay Renewal", 0.25m, PaymentPlan.TenPayRenewal, RivtechApplication.Centauri);

        public static readonly PercentDeposit BoxtruckMortgagee = new PercentDeposit("CABT-Mortgagee", "Boxtruck Mortgagee", 1, PaymentPlan.PremiumFinanced, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckFullPay = new PercentDeposit("CABT-Full-Pay", "Boxtruck Full Pay", 1, PaymentPlan.FullPay, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckTwoPay = new PercentDeposit("CABT-Two-Pay", "Boxtruck Two Pay", 0.6m, PaymentPlan.TwoPay, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckFourPay = new PercentDeposit("CABT-Four-Pay", "Boxtruck Four Pay", 0.4m, PaymentPlan.FourPay, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckEightPay = new PercentDeposit("CABT-Eight-Pay", "Boxtruck Eight Pay", 0.3m, PaymentPlan.EightPay, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckTenPay = new PercentDeposit("CABT-Ten-Pay", "Boxtruck Ten Pay", 0.25m, PaymentPlan.TenPay, RivtechApplication.Boxtruck);
        public static readonly PercentDeposit BoxtruckTenPayRenewal = new PercentDeposit("CABT-Ten-Pay-Renewal", "Boxtruck Ten Pay Renewal", 0.25m, PaymentPlan.TenPayRenewal, RivtechApplication.Boxtruck);


        public decimal Value { get; private set; }
        public string ApplicationId { get; private set; }
        public RivtechApplication Application { get; private set; }
        public string PaymentPlanId { get; private set; }
        public PaymentPlan PaymentPlan { get; private set; }


        public PercentDeposit(string id, string name, decimal value, PaymentPlan paymentPlan, RivtechApplication application)
            : base(id, name)
        {
            if (paymentPlan == null || !paymentPlan.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_PLAN);
            }
            else if (application == null || !application.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
            else if (value < 0)
            {
                throw new ParameterException(ExceptionMessages.INVALID_DEPOSIT_PERCENTAGE);
            }

            Value = value;
            PaymentPlanId = paymentPlan.Id;
            ApplicationId = application.Id;
        }

        public PercentDeposit() { }

        public bool IsValid()
        {
            return GetAllId<PercentDeposit>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<PercentDeposit>().Contains(id);
        }

        public static PercentDeposit GetByPaymentPlanAndAppId(string paymentPlanId, string appId)
        {
            if (!PaymentPlan.IsValid(paymentPlanId))
            {
                throw new ParameterException(ExceptionMessages.INVALID_PAYMENT_PLAN);
            }
            else if (!RivtechApplication.IsValid(appId))
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
            return GetAll<PercentDeposit>().FirstOrDefault(x => x.PaymentPlanId == paymentPlanId && x.ApplicationId == appId);
        }
    }
}
