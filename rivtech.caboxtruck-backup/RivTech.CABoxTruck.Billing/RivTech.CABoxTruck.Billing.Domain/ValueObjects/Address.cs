﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class Address
    {
        public Address(
            string street,
            string city,
            string state,
            string zipcode
        ) {
            Street = street;
            City = city;
            Zipcode = zipcode;
            State = state;
        }

        public string Street { get;  }
        public string City { get; }
        public string Zipcode { get; }
        public string State { get; }
    }
}
