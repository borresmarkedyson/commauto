﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class RivtechApplication : Enumeration
    {
        public static readonly RivtechApplication Centauri = new RivtechApplication("CENT", "Centauri");
        public static readonly RivtechApplication Boxtruck = new RivtechApplication("CABT", "Boxtruck");

        public RivtechApplication(string id, string name)
            : base(id, name)
        {
        }

        public RivtechApplication() { }

        public bool IsValid()
        {
            return GetAllId<RivtechApplication>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<RivtechApplication>().Contains(id);
        }

    }
}
