﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class InstallmentType : Enumeration
    {
        public static readonly InstallmentType Deposit = new InstallmentType("D", "Deposit");
        public static readonly InstallmentType Installment = new InstallmentType("I", "Installment");
        public static readonly InstallmentType BillPlanChange = new InstallmentType("B", "Bill Plan Change");
        public static readonly InstallmentType Endorsement = new InstallmentType("E", "Endorsement");

        public InstallmentType(string id, string name)
            : base(id, name)
        {
        }

        public InstallmentType() { }

        public bool IsValid()
        {
            return GetAllId<InstallmentType>().Contains(Id);
        }

    }
}