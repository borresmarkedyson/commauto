﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class ConditionalFee : Enumeration
    {
        public static readonly ConditionalFee CentauriInstallmentFee = new ConditionalFee("CENT-IF", "Centauri Installment Fee", AmountSubType.InstallmentFee, 3m, false, RivtechApplication.Centauri, DateTime.Now);
        public static readonly ConditionalFee CommAutoInstallmentFee = new ConditionalFee("CA-IF", "CommAuto Installment Fee", AmountSubType.InstallmentFee, 5m, true, RivtechApplication.Boxtruck, DateTime.Now);

        public string RivtechApplicationId { get; private set; }
        public RivtechApplication RivtechApplication { get; private set; }
        public string AmountSubTypeId { get; private set; }
        public AmountSubType AmountSubType { get; private set; }
        public bool IsRate { get; private set; }
        public decimal Value { get; private set; }
        public DateTime EffectiveDate { get; private set; }
        public DateTime? VoidDate { get; private set; }


        public ConditionalFee(string id, string name, AmountSubType amountSubType, decimal value, bool isRate, RivtechApplication rivtechApplication, DateTime effectiveDate)
            : base(id, name)
        {
            if (amountSubType == null || !amountSubType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_SUBTYPE);
            }
            AmountSubTypeId = amountSubType.Id;
            Value = value;
            IsRate = isRate;
            EffectiveDate = effectiveDate;

            if (rivtechApplication == null || !rivtechApplication.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }

            RivtechApplicationId = rivtechApplication.Id;
        }

        public ConditionalFee() { }

        public bool IsValid()
        {
            if (Id == CentauriInstallmentFee.Id || Id == CommAutoInstallmentFee.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetAsVoid()
        {
            VoidDate = DateTime.Now;
        }
    }
}
