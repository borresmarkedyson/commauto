﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class PolicyStatus : Enumeration
    {
        public static readonly PolicyStatus InForce = new PolicyStatus("I", "In-Force");
        public static readonly PolicyStatus Cancelled = new PolicyStatus("C", "Cancelled");

        public PolicyStatus(string id, string name)
            : base(id, name)
        {
        }

        public PolicyStatus() { }

        public bool IsValid()
        {
            return GetAllId<PolicyStatus>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<PolicyStatus>().Contains(id);
        }

    }
}
