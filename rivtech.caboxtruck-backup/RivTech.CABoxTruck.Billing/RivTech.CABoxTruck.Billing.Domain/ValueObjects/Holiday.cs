﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class Holiday : Enumeration
    {
        public static readonly Holiday Christmas = new Holiday("Christmas", "Christmas Day", new DateTime(2020, 12, 25));
        public static readonly Holiday ChristmasEve = new Holiday("ChristmasEve", "Christmas Eve", new DateTime(2020, 12, 24));
       
        public DateTime Date { get; private set; }
        public string Comment { get; private set; }

        public Holiday(string id, string description, DateTime date, string comment = null)
            : base(id, description)
        {
            Date = date;
            Comment = comment;
        }
        public Holiday() { }

        public static bool IsValid(string id)
        {
            return GetAllId<Holiday>().Contains(id);
        }

        public static Holiday GetByDate(DateTime date)
        {
            return GetAll<Holiday>().FirstOrDefault(x => x.Date == date);
        }
    }
}

