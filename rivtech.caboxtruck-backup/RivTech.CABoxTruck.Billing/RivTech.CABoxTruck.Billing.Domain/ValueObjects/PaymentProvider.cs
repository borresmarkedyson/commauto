﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class PaymentProvider : Enumeration
    {
        public static readonly PaymentProvider AuthorizeNet = new PaymentProvider("AUTHNET", "Authorize.Net", RivtechApplication.Centauri);
        
        public string ApplicationId { get; private set; }
        public RivtechApplication Application { get; private set; }

        public PaymentProvider(string id, string name, RivtechApplication application)
            : base(id, name)
        {
            if (application == null || !application.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }

            ApplicationId = application.Id;
        }

        public PaymentProvider() { }

        public bool IsValid()
        {
            return GetAllId<PaymentProvider>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<PaymentProvider>().Contains(id);
        }

        public static PaymentProvider GetByAppId(string appId)
        {
            if (!RivtechApplication.IsValid(appId))
            {
                throw new ParameterException(ExceptionMessages.INVALID_RIVTECH_APPLICATION);
            }
            return GetAll<PaymentProvider>().FirstOrDefault(x => x.ApplicationId == appId);
        }
    }
}

