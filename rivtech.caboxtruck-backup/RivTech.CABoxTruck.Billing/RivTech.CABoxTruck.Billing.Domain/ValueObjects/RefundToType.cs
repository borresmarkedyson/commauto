﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class RefundToType : Enumeration
    {
        public static readonly RefundToType Insured = new RefundToType("I", "Insured");
        public static readonly RefundToType PremiumFinanced = new RefundToType("M", "PremiumFinanced");

        public RefundToType(string id, string name)
            : base(id, name)
        {
        }

        public RefundToType() { }

        public bool IsValid()
        {
            return GetAllId<RefundToType>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<RefundToType>().Contains(id);
        }
    }
}
