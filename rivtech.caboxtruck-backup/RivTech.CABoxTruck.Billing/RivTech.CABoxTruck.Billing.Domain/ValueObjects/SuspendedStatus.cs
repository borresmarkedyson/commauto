﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class SuspendedStatus : Enumeration
    {
        public static readonly SuspendedStatus Pending = new SuspendedStatus("PEND", "Pending");
        public static readonly SuspendedStatus Posted = new SuspendedStatus("POST", "Posted");
        public static readonly SuspendedStatus Returned = new SuspendedStatus("R", "Returned");
        public static readonly SuspendedStatus Voided = new SuspendedStatus("V", "Voided");

        public SuspendedStatus(string id, string name)
            : base(id, name)
        {
        }

        public SuspendedStatus() { }

        public bool IsValid()
        {
            return GetAllId<SuspendedStatus>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<SuspendedStatus>().Contains(id);
        }
    }
}


