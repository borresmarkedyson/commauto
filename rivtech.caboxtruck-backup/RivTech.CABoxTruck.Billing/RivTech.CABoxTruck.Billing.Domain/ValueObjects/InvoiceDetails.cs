﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class InvoiceDetails
    {
        public InvoiceDetails(decimal amountDue, string refNum)
        {
            AmountDue = amountDue;
            DueDate = DateTime.Now.Date.AddDays(1).AddMinutes(-1);
            ReferenceNumber = refNum;
        }
        public decimal AmountDue { get; }
        public DateTime DueDate { get; }
        public string ReferenceNumber { get; }
    }
}