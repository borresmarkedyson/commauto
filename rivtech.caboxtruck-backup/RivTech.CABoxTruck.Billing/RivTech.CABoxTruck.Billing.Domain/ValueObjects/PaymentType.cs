﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class PaymentType : Enumeration
    {
        public static readonly PaymentType Regular = new PaymentType("R", "Regular");
        public static readonly PaymentType Void = new PaymentType("V", "Void");
        public static readonly PaymentType NSF = new PaymentType("N", "NSF");
        public static readonly PaymentType StopPay = new PaymentType("S", "Stop Pay");
        public static readonly PaymentType Transfer = new PaymentType("T", "Transferred");
        public static readonly PaymentType Refund = new PaymentType("RF", "Refund");
        public static readonly PaymentType SuspendedPayment = new PaymentType("SPND", "Suspended Payment");

        public PaymentType(string id, string name)
            : base(id, name)
        {
        }

        public PaymentType() { }

        public bool IsValid()
        {
            return GetAllId<PaymentType>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<PaymentType>().Contains(id);
        }
    }
}
