﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class InstallmentFee : Enumeration
    {
        private const decimal INSTALLMENT_CHARGE = 0.03m;

        public InstallmentFee(decimal totalPremium, decimal depositPremium, decimal installmentCount, decimal taxRate, decimal previousInvoicedInstallmentFee)
        {
            TotalPremiumDue = totalPremium;
            RemainingPremium = totalPremium - depositPremium;
            TaxRate = taxRate;
            InstallmentCount = installmentCount;
            PreviousInvoicedInstallmentFee = previousInvoicedInstallmentFee;
        }

        public decimal TaxRate { get; }
        public decimal InstallmentCount { get; }
        public decimal Fee => ((TotalInstallmentFee - PreviousInvoicedInstallmentFee) / InstallmentCount);
        public decimal RemainingPremium { get; private set; }
        public decimal Premium => (RemainingPremium / InstallmentCount);
        public decimal Tax => ((TaxRate / 100) * Fee);
        public decimal PerInstallmentDue => Premium + Fee + Tax;
        public decimal TotalFee => Fee * InstallmentCount;
        public decimal TotalDue => PerInstallmentDue * InstallmentCount;
        public decimal TotalPremiumDue { get; }
        public decimal PreviousInvoicedInstallmentFee { get; }
        public decimal TotalInstallmentFee => TotalPremiumDue switch // => (Premium * INSTALLMENT_CHARGE); // No value yet.
        {
            decimal totalPremiumDue when totalPremiumDue < 0 => 0,
            decimal totalPremiumDue when totalPremiumDue >= 0 && totalPremiumDue < 5000 => 77,
            decimal totalPremiumDue when totalPremiumDue >= 5000 && totalPremiumDue < 10000 => 232,
            decimal totalPremiumDue when totalPremiumDue >= 10000 && totalPremiumDue < 20000 => 465,
            decimal totalPremiumDue when totalPremiumDue >= 20000 && totalPremiumDue < 35000 => 852,
            decimal totalPremiumDue when totalPremiumDue >= 35000 && totalPremiumDue < 50000 => 1317,
            decimal totalPremiumDue when totalPremiumDue >= 50000 && totalPremiumDue < 100000 => 2325,
            decimal totalPremiumDue when totalPremiumDue >= 100000 && totalPremiumDue < 200000 => 4650,
            decimal totalPremiumDue when totalPremiumDue >= 200000 && totalPremiumDue < 300000 => 7750,
            decimal totalPremiumDue when totalPremiumDue >= 300000 && totalPremiumDue < 400000 => 10850,
            decimal totalPremiumDue when totalPremiumDue >= 400000 && totalPremiumDue < 500000 => 13950,
            decimal totalPremiumDue when totalPremiumDue >= 500000 && totalPremiumDue < 750000 => 19375,
            decimal totalPremiumDue when totalPremiumDue >= 750000 && totalPremiumDue < 1000000 => 27125,
            decimal totalPremiumDue when totalPremiumDue >= 1000000 => 30000,
            _ => 0
        };
    }
}
