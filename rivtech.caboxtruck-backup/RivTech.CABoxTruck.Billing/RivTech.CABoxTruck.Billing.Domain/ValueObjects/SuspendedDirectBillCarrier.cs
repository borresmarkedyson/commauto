﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class SuspendedDirectBillCarrier : Enumeration
    {
        public static readonly SuspendedDirectBillCarrier CentauriSpecialtyInsuranceCompany = new SuspendedDirectBillCarrier("CSIC", "Centauri Specialty Insurance Company");
        public static readonly SuspendedDirectBillCarrier CentauriNationalInsuranceCompany = new SuspendedDirectBillCarrier("CNIC", "Centauri National Insurance Company");
        
        public SuspendedDirectBillCarrier(string id, string name)
            : base(id, name)
        {
        }

        public SuspendedDirectBillCarrier() { }

        public bool IsValid()
        {
            return GetAllId<SuspendedDirectBillCarrier>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<SuspendedDirectBillCarrier>().Contains(id);
        }
    }
}



