﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class InstrumentType : Enumeration
    {
        public static readonly InstrumentType Cash = new InstrumentType("C", "Cash");
        public static readonly InstrumentType CreditCard = new InstrumentType("CC", "Credit Card");
        public static readonly InstrumentType EFT = new InstrumentType("EFT", "EFT");
        public static readonly InstrumentType Check = new InstrumentType("CHQ", "Check");
        public static readonly InstrumentType Adjustment = new InstrumentType("ADJ", "Correction");
        public static readonly InstrumentType WriteOff = new InstrumentType("WO", "Write-Off");
        public static readonly InstrumentType Refund = new InstrumentType("R", "Refund");
        public static readonly InstrumentType LockBox = new InstrumentType("LB", "Lock Box");
        public static readonly InstrumentType RecurringECheck = new InstrumentType("RCHQ", "Recurring ECheck");
        public static readonly InstrumentType RecurringCreditCard = new InstrumentType("RCC", "Recurring Credit Card");
        public static readonly InstrumentType Transferred = new InstrumentType("T", "Transferred");
        public static readonly InstrumentType Internal = new InstrumentType("IN", "Internal");
        public static readonly InstrumentType DirectBill = new InstrumentType("DB", "Direct Bill");
        public static readonly InstrumentType RebalanceOverpayment = new InstrumentType("RO", "Rebalance Overpayment");

        public InstrumentType(string id, string name)
            : base(id, name)
        {
        }

        public InstrumentType() { }

        public bool IsValid()
        {
            return GetAllId<InstrumentType>().Contains(Id);
        }
    }
}
