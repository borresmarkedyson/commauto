﻿namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class BankAccount
    {
        public string BankName { get; }
        public string Number { get; }

    }
}