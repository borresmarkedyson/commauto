﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public enum AchType
    {
        Unknown = 0,
        Checking = 1,
        Savings = 2
    }
}

