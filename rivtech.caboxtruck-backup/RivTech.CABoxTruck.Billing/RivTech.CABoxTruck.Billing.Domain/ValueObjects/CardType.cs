﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{

    public enum CardType
    {
        Unknown = 0,
        Visa = 1,
        MasterCard = 2,
        Discover = 3,
        AmericanExpress = 4
    }
}
