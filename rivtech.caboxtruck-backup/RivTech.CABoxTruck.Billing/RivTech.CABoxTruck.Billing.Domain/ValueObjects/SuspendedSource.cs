﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class SuspendedSource : Enumeration
    {
        public static readonly SuspendedSource Lockbox = new SuspendedSource("LB", "Lockbox");
        public static readonly SuspendedSource Internal = new SuspendedSource("IN", "INTERNAL");
        public static readonly SuspendedSource DirectBill = new SuspendedSource("DB", "Direct Bill");

        public SuspendedSource(string id, string name)
            : base(id, name)
        {
        }

        public SuspendedSource() { }

        public bool IsValid()
        {
            return GetAllId<SuspendedSource>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<SuspendedSource>().Contains(id);
        }
    }
}
