﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class PaymentPlan : Enumeration
    {
        public static readonly PaymentPlan FullPay = new PaymentPlan("Full", "Full Pay");
        public static readonly PaymentPlan PremiumFinanced = new PaymentPlan("PremFin", "Premium Financed");
        public static readonly PaymentPlan TenPayRenewal = new PaymentPlan("Renewal", "Ten Pay Renewal");
        public static readonly PaymentPlan TwoPay = new PaymentPlan("Two", "Two Pay");
        public static readonly PaymentPlan ThreePay = new PaymentPlan("Three", "Three Pay");
        public static readonly PaymentPlan FourPay = new PaymentPlan("Four", "Four Pay");
        public static readonly PaymentPlan FivePay = new PaymentPlan("Five", "Five Pay");
        public static readonly PaymentPlan SixPay = new PaymentPlan("Six", "Six Pay");
        public static readonly PaymentPlan SevenPay = new PaymentPlan("Seven", "Seven Pay");
        public static readonly PaymentPlan EightPay = new PaymentPlan("Eight", "Eight Pay");
        public static readonly PaymentPlan NinePay = new PaymentPlan("Nine", "Nine Pay");
        public static readonly PaymentPlan TenPay = new PaymentPlan("Ten", "Ten Pay");
        public static readonly PaymentPlan ElevenPay = new PaymentPlan("Eleven", "Eleven Pay");

        public PaymentPlan(string id, string name)
            : base(id, name)
        {
        }

        public PaymentPlan() { }

        public bool IsValid()
        {
            return GetAllId<PaymentPlan>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<PaymentPlan>().Contains(id);
        }

        public static List<string> GetPaymentPlanIds()
        {
            return new List<string> {
                PremiumFinanced.Id,
                FullPay.Id,
                TwoPay.Id,
                ThreePay.Id,
                FourPay.Id,
                FivePay.Id,
                SixPay.Id,
                SevenPay.Id,
                EightPay.Id,
                NinePay.Id,
                TenPay.Id,
                ElevenPay.Id
            };
        }

        public static Dictionary<string, int> PayCounts = new Dictionary<string, int>()
        {
            { "Full", 1 },
            { "PremFin", 1},
            { "Renewal", 10},
            { "Two", 2},
            { "Three", 3},
            { "Four", 4},
            { "Five", 5},
            { "Six", 6},
            { "Seven", 7},
            { "Eight", 8},
            { "Nine", 9},
            { "Ten", 10},
            { "Eleven", 11}
        };
    }
}
