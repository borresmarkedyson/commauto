﻿using System;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class CustomerDetails
    {
        public CustomerDetails(
            string firstName, 
            string lastName, 
            string email, 
            Address address
        ) {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
            Payer = $"{FirstName} {LastName}";
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Address = address;
        }
        public CustomerDetails(
            string payer,
            string email, 
            Address address
        ) {
            Payer = payer ?? throw new ArgumentNullException(nameof(payer));
            Email = email ?? throw new ArgumentNullException(nameof(email));
            Address = address;
        }

        public string FirstName { get; } = string.Empty;
        public string LastName { get; } = string.Empty;
        public Address Address { get; }
        public string Email { get; }
        public string Payer { get; private set; }
    }
}