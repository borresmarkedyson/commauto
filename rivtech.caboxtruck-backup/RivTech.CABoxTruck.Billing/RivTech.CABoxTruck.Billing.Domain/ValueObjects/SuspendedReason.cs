﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class SuspendedReason : Enumeration
    {
        public static readonly SuspendedReason ReinstatementCriteriaNotMet = new SuspendedReason("RCNM", "Reinstatement Criteria Not Met");
        public static readonly SuspendedReason NoMatchingPolicy = new SuspendedReason("NMP", "No Matching Policy");
        public static readonly SuspendedReason PC = new SuspendedReason("PC", "Policy Cancel");

        public SuspendedReason(string id, string name)
            : base(id, name)
        {
        }

        public SuspendedReason() { }

        public bool IsValid()
        {
            return GetAllId<SuspendedReason>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<SuspendedReason>().Contains(id);
        }
    }
}

