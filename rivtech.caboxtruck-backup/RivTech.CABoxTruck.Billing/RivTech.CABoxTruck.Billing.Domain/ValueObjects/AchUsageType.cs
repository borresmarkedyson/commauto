﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public enum AchUsageType
    {
        Unknown = 0,
        Business = 1,
        Personal = 2
    } 
}
