﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class AmountType : Enumeration
    {
        public static readonly AmountType Premium = new AmountType("P", "Premium");
        public static readonly AmountType PremiumFee = new AmountType("F", "Fee");
        public static readonly AmountType Tax = new AmountType("T", "Tax");
        public static readonly AmountType TransactionFee = new AmountType("TF", "Transaction Fee");
        public static readonly AmountType TransactionTax = new AmountType("TT", "Transaction Tax");
        public static readonly AmountType Commission = new AmountType("C", "Commission");
        public static readonly AmountType Overpayment = new AmountType("O", "Overpayment");

        public AmountType(string id, string name)
            : base(id, name)
        {
        }

        public AmountType() { }

        public bool IsValid()
        {
            return GetAllId<AmountType>().Contains(Id);
        }
    }
}
