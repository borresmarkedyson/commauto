﻿namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class SubCategory
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}