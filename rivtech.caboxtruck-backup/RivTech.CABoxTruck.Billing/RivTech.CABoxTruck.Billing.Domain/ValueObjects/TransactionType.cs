﻿using RivTech.CABoxTruck.Billing.Domain.Common;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class TransactionType : Enumeration
    {
        public static readonly TransactionType New = new TransactionType("N", "New");
        public static readonly TransactionType Cancellation = new TransactionType("C", "Cancellation");
        public static readonly TransactionType Endorsement = new TransactionType("E", "Endorsement");
        public static readonly TransactionType Fee = new TransactionType("F", "Fee");
        public static readonly TransactionType Reinstatement = new TransactionType("R", "Reinstatement");
        public static readonly TransactionType Void = new TransactionType("V", "Void");
        public static readonly TransactionType BillPlanChange = new TransactionType("B", "Bill Plan Change");

        public TransactionType(string id, string name)
            : base(id, name)
        {
        }

        public TransactionType() { }

        public bool IsValid()
        {
            return GetAllId<TransactionType>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<TransactionType>().Contains(id);
        }
    }
}
