﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.Domain.ValueObjects
{
    public class AmountSubType : Enumeration
    {
        public static readonly AmountSubType Premium = new AmountSubType("PREM", "Premium", AmountType.Premium);
        public static readonly AmountSubType InspectionFee = new AmountSubType("INSF", "Inspection Fee", AmountType.TransactionFee);
        public static readonly AmountSubType PolicyFee = new AmountSubType("PF", "Policy Fee", AmountType.PremiumFee);
        public static readonly AmountSubType Tax = new AmountSubType("TAX", "Tax", AmountType.Tax);
        public static readonly AmountSubType MgaFee = new AmountSubType("MGAFee", "MGA Fee", AmountType.PremiumFee);
        public static readonly AmountSubType EmpatfsFee = new AmountSubType("EMPATFS", "Emergency Management Preparedness and Assisstance Trust Fund Surcharge", AmountType.PremiumFee);
        public static readonly AmountSubType NonHurricanePremium = new AmountSubType("NHR", "Non-Hurricane Premium", AmountType.Premium);
        public static readonly AmountSubType HurricanePremium = new AmountSubType("HUR", "Hurricane Premium", AmountType.Premium);
        public static readonly AmountSubType PremiumAdjustment = new AmountSubType("PremiumAdj", "Premium Adjustment", AmountType.Premium);
        public static readonly AmountSubType ConsentToRate = new AmountSubType("CTR", "Consent To Rate Amount", AmountType.Premium);
        public static readonly AmountSubType FullPayDiscount = new AmountSubType("FPD", "Full-Pay Discount", AmountType.Premium);
        public static readonly AmountSubType NSFFee = new AmountSubType("NSFF", "NSF Fee", AmountType.TransactionFee);
        public static readonly AmountSubType Additive = new AmountSubType("AD", "Additive", AmountType.Premium);
        public static readonly AmountSubType TotalPremiumWithAdjustment = new AmountSubType("TotalPremiumWithAdj", "Total Premium With Adjustment", AmountType.Premium);

        //Premiums
        public static readonly AmountSubType AutoLiabilityPremium = new AmountSubType("ALP", "Auto Liability Premium", AmountType.Premium);
        public static readonly AmountSubType AdditionalInsured = new AmountSubType("AIP", "Additional Insured", AmountType.Premium);
        public static readonly AmountSubType WaiverOfSubrogation = new AmountSubType("WS", "Waiver Of Subrogation", AmountType.Premium);
        public static readonly AmountSubType PrimaryNoncontributory = new AmountSubType("PNC", "Primary And Noncontributory", AmountType.Premium);
        public static readonly AmountSubType CargoPremium = new AmountSubType("CP", "Cargo Premium", AmountType.Premium);
        public static readonly AmountSubType ALManuscriptPremium = new AmountSubType("ALMP", "AL Manuscript Premium", AmountType.Premium);
        public static readonly AmountSubType GeneralLiabilityPremium = new AmountSubType("GLP", "General Liability Premium", AmountType.Premium);

        public static readonly AmountSubType PhysicalDamagePremium = new AmountSubType("PDP", "Physical Damage Premium", AmountType.Premium);
        public static readonly AmountSubType TIEndst = new AmountSubType("TIE", "Trailer Interchange Endst", AmountType.Premium);
        public static readonly AmountSubType HPDEndst = new AmountSubType("HPDE", "Hired Physical Damage Endst", AmountType.Premium);
        public static readonly AmountSubType PDManuscriptPremium = new AmountSubType("PDM", "PD Manuscript Premium", AmountType.Premium);

        //Submission/Policy Fees
        //public static readonly AmountSubType ServiceFee = new AmountSubType("SEF", "Service Fee", AmountType.PremiumFee);
        public static readonly AmountSubType ALSFee = new AmountSubType("ALSF", "Service Fee - AL", AmountType.PremiumFee);
        public static readonly AmountSubType PDSFee = new AmountSubType("PDSF", "Service Fee - PD", AmountType.PremiumFee);

        
        //public static readonly AmountSubType MTSFFee = new AmountSubType("MTSF", "Mid Term Service Fee", AmountType.PremiumFee);
        public static readonly AmountSubType ReinstatementFee = new AmountSubType("REINF", "Reinstatement Fee", AmountType.PremiumFee);

        //Transaction Fees
        public static readonly AmountSubType InstallmentFee = new AmountSubType("IF", "Installment Fee", AmountType.TransactionFee);
        //public static readonly AmountSubType NSFFFee = new AmountSubType("NSFF", "Non Sufficient Funds Fee", AmountType.TransactionFee);

        //Transaction Tax
        public static readonly AmountSubType InstallmentTax = new AmountSubType("IFT", "Installment Fee Tax", AmountType.TransactionTax);

        //Taxes
        public static readonly AmountSubType SurplusLinesTax = new AmountSubType("SLT", "Surplus Lines Tax", AmountType.Tax);
        public static readonly AmountSubType StampingFee = new AmountSubType("SF", "Stamping Fee", AmountType.Tax);

        //Commission
        public static readonly AmountSubType ALCommission = new AmountSubType("ALC", "Auto Liability Commission", AmountType.Commission);
        public static readonly AmountSubType PDCommission = new AmountSubType("PDC", "Physical Damage Commission", AmountType.Commission);

        //Minimum Premium Adjustment
        public static readonly AmountSubType AutoLiabilityPremiumAdjustment = new AmountSubType("ALPMPA", "Auto Liability Premium Adjustment", AmountType.Premium);
        public static readonly AmountSubType CargoPremiumAdjustment = new AmountSubType("CPMPA", "Cargo Premium Adjustment", AmountType.Premium);
        public static readonly AmountSubType ALManuscriptPremiumAdjustment = new AmountSubType("ALMPMPA", "AL Manuscript Premium Adjustment", AmountType.Premium);
        public static readonly AmountSubType GeneralLiabilityPremiumAdjustment = new AmountSubType("GLPMPA", "General Liability Premium Adjustment", AmountType.Premium);

        public static readonly AmountSubType PhysicalDamagePremiumAdjustment = new AmountSubType("PDPMPA", "Physical Damage Premium Adjustment", AmountType.Premium);
        public static readonly AmountSubType PDManuscriptPremiumAdjustment = new AmountSubType("PDMMPA", "PD Manuscript Premium Adjustment", AmountType.Premium);

        //Short Rate Premium
        public static readonly AmountSubType AutoLiabilityPremiumShortRate = new AmountSubType("ALPSRP", "Auto Liability Premium Short Rate", AmountType.Premium);
        public static readonly AmountSubType CargoPremiumShortRate = new AmountSubType("CPSRP", "Cargo PremiumSRP Short Rate", AmountType.Premium);
        public static readonly AmountSubType ALManuscriptPremiumShortRate = new AmountSubType("ALMPSRP", "AL Manuscript Premium Short Rate", AmountType.Premium);
        public static readonly AmountSubType GeneralLiabilityPremiumShortRate = new AmountSubType("GLPSRP", "General Liability Premium Short Rate", AmountType.Premium);

        public static readonly AmountSubType PhysicalDamagePremiumShortRate = new AmountSubType("PDPSRP", "Physical Damage Premium Short Rate", AmountType.Premium);
        public static readonly AmountSubType PDManuscriptPremiumShortRate = new AmountSubType("PDMSRP", "PD Manuscript Premium Short Rate", AmountType.Premium);

        //Policy Overpayment
        public static readonly AmountSubType PolicyOverpayment = new AmountSubType("PO", "Policy Overpayment", AmountType.Overpayment);

        public string AmountTypeId { get; private set; }
        public AmountType AmountType { get; private set; }


        public AmountSubType(string id, string name, AmountType amountType)
            : base(id, name)
        {
            if (amountType == null || !amountType.IsValid())
            {
                throw new ParameterException(ExceptionMessages.INVALID_AMOUNT_TYPE);
            }
            AmountTypeId = amountType.Id;
        }

        public AmountSubType() { }

        public bool IsValid()
        {
            return GetAllId<AmountSubType>().Contains(Id);
        }

        public static bool IsValid(string id)
        {
            return GetAllId<AmountSubType>().Contains(id);
        }

        public static Dictionary<string, int> ALPremiumGroup = new Dictionary<string, int>()
        {
            { "ALP", 1 },
            { "AIP", 2 },
            { "WS", 3 },
            { "PNC", 4 },
            { "CP", 5 },
            { "GLP", 6 },
            { "ALMP", 7 },
            { "ALPMPA", 8 },
            { "CPMPA", 9 },
            { "GLPMPA", 10 },
            { "ALMPMPA", 11 },
            { "ALPSRP", 12 },
            { "CPSRP", 13 },
            { "GLPSRP", 14 },
            { "ALMPSRP", 15 }
        };

        public static Dictionary<string, int> PDPremiumGroup = new Dictionary<string, int>()
        {
            { "PDP", 1 },
            { "TIE", 2 },
            { "HPDE", 3 },
            { "PDM", 4 },
            { "PDPMPA", 5 },
            { "PDMMPA", 6 },
            { "PDPSRP", 7 },
            { "PDMSRP", 8 }
        };

        public static Dictionary<string, int> NonBindTransactionData = new Dictionary<string, int>()
        {
            { "PDP", 1 },
            { "TIE", 2 },
            { "HPDE", 3 },
            { "PDM", 4 }
        };
    }
}

