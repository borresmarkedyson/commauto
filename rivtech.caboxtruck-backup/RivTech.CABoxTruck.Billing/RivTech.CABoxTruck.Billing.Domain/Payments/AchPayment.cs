﻿using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class AchPayment : PaymentCCEFT
    {

        public AchPayment(
            string routingNumber,
            string accountNumber,
            AchType type,
            AchUsageType usageType,
            string email = null
        ) : base() {
            AccountNumber = accountNumber;
            Type = type;
            UsageType = usageType;
            RoutingNumber = routingNumber;
            Email = email;
        }

        public string RoutingNumber { get; }
        public string AccountNumber { get; }
        public AchType Type { get; }
        public AchUsageType UsageType { get; }
        public override string CheckNumber => AccountNumber; 
    }
}
