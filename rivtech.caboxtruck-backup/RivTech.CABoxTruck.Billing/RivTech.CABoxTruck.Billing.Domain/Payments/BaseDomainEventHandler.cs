﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public abstract class BaseDomainEventHandler<TEvent> where TEvent : BaseDomainEvent
    {
        public virtual void Handle(TEvent domainEvent) { }
        public virtual async Task HandleAsync(TEvent domainEvent)
        {
            await Task.Run(() => Handle(domainEvent)); 
        }
    }
}
