﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class PaymentResult
    {
        public PaymentResult(PaymentCCEFT payment)
        {
            DateTime = DateTime.Now;
            Payment = payment;
        }
        public DateTime DateTime { get; }
        public string Message { get; set; }
        public string[] Errors { get; set; } = new string[] { };
        public bool Success { get; set; }
        public string ReferenceNumber { get; set; }
        public PaymentCCEFT Payment { get; set; }
    }
}
