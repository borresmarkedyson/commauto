﻿using RivTech.CABoxTruck.Billing.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class PaymentInstrumentType : Enumeration
    {
        public static readonly PaymentInstrumentType Cash = new PaymentInstrumentType("151", "Cash");
        public static readonly PaymentInstrumentType CreditCard = new PaymentInstrumentType("152", "Credit Card");
        public static readonly PaymentInstrumentType EFT = new PaymentInstrumentType("153", "Electronic Fund Transfer");
        
        private PaymentInstrumentType()
        {
        }

        private PaymentInstrumentType(string value, string displayName) : base(value, displayName)
        {
        }

        public static PaymentInstrumentType FromPayment(PaymentCCEFT payment)
        {
            if (payment is AchPayment) return EFT;
            if (payment is CreditCardPayment) return CreditCard;
            throw new ArgumentException("Invalid payment type");
        }
    }

}
