﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{

    /// <summary>
    /// Aggregate Root of Aggregate 'Payment'.
    /// </summary>
    public abstract class PaymentCCEFT
    {
        protected PaymentCCEFT()
        {
            DueDate = DateTime.Now;
        }

        public DateTime DueDate { get; }
        public abstract string CheckNumber { get; }
        public string Email { get; set; }
    }
}
