﻿namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class PreBindPaymentDetails
    {
        public string InsuredName { get; set; }
        public string AccessCode { get; set; }
        public string PayeeName { get; set; }
        public string PayeeEmail { get; set; }
        public string PaymentMethod { get; set; }
        public decimal PaymentAmount { get; set; }
        public string ConfirmationCode { get; set; }
    }
}
