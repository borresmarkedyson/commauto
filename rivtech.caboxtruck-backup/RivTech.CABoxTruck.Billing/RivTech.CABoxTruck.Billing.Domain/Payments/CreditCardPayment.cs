﻿using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class CreditCardPayment : PaymentCCEFT
    {

        public CreditCardPayment(
            CardType cardType, 
            string number,
            string cvv,
            int expirationYear,
            int expirationMonth,
            string email = null
        ) : base() {
            CardType = cardType;
            Number = number;
            Cvv = cvv;
            ExpirationDate = new DateTime(expirationYear, expirationMonth, DateTime.DaysInMonth(expirationYear, expirationMonth));
            Email = email;
        }

        public CardType CardType { get; }
        public string Number { get; }
        public string Cvv { get; }
        public DateTime ExpirationDate { get; }

        public override string CheckNumber => Number;
    }
}
