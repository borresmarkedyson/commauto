﻿namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class PaymentDetails
    {
        public string PolicyNumber { get; set; }
        public string ZipCode { get; set; }
        public string InsuredName { get; set; }
        public string CurrentAmountDue { get; set; }
        public string PayoffAmount { get; set; }
        public int PolicyDetailID { get; set; }
    }
}