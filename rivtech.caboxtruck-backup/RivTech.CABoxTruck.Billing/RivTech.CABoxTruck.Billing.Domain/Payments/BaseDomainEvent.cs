﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public abstract class BaseDomainEvent
    {
        public DateTime DateTime { get; } = DateTime.Now;
    }
}
