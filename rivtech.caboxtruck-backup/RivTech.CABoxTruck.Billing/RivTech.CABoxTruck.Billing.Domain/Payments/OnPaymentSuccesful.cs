﻿using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class OnPaymentSuccesful : BaseDomainEvent
    {
        public OnPaymentSuccesful(
            int policyDetailID,
            CustomerDetails customer, 
            InvoiceDetails invoice, 
            PaymentCCEFT payment,
            string referenceNumber,
            string postedBy
        ) {
            PolicyDetailID = policyDetailID;
            Customer = customer;
            Invoice = invoice;
            Payment = payment;
            ReferenceNumber = referenceNumber;
            PostedBy = postedBy;
        }

        public int PolicyDetailID { get; }
        public CustomerDetails Customer { get; }
        public InvoiceDetails Invoice { get; }
        public PaymentCCEFT Payment { get; }
        public string ReferenceNumber { get; }
        public string PostedBy { get; }
    }
}
