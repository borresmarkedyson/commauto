﻿namespace RivTech.CABoxTruck.Billing.Domain.Payments
{
    public class OnPrebindPaymentSuccessful : BaseDomainEvent
    {
        public OnPrebindPaymentSuccessful(
            PreBindPaymentDetails preBindPaymentDetails,
            PaymentResult paymentResult
        ) {
            PreBindPaymentDetails = preBindPaymentDetails;
            PaymentResult = paymentResult;
        }
        public PreBindPaymentDetails PreBindPaymentDetails { get; }
        public PaymentResult PaymentResult { get; }
    }
}
