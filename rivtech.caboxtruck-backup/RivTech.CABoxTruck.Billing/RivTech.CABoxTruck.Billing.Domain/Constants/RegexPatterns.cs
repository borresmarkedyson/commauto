﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Constants
{
    public static class RegexPatterns
    {
        public const string ALPHANUMERIC = @"^[A-Za-z0-9\-]+$";
        public const string ALPHANUMERIC_SPACE = @"^[A-Za-z0-9\-\s]+$";
        public const string NUMERIC = @"^\d+$";
        public const string EMAIL = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
        public const string NAME = @"^[A-Za-z0-9'-.\s]+$";
        public const string PHONENUMBER = @"^\d{10}$";
        public const string POLICYNUMBER = @"^[A-Za-z0-9-_]+$";
        public const string ZIP = @"^\d{5}$";
    }
}
