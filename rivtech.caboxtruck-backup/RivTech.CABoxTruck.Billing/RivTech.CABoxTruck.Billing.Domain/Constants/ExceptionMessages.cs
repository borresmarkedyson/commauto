﻿namespace RivTech.CABoxTruck.Billing.Domain.Constants
{
    public static class ExceptionMessages
    {
        public const string SYSTEMEXCEPTION = "System related error encountered. Please contact your system Administrator.";
        public const string NOIMPLEMENTEDEXCEPTION = "No Implementation code error encountered. Please contact your system Administrator.";
        public const string AUTHENTICATIONEXCEPTION = "Authorization required";
        public const string UNAUTHORIZEDEXCEPTION = "You are not allowed to access the file.";
        public const string ARGUMENTEXCEPTION = "Passed arguments does not meet the required parameter specification";
        public const string DBUPDATEEXCEPTION = "Failed to update record.";
        public const string DBEXCEPTION = "Database related error encountered.";
        public const string NULLREFERENCEEXCEPTION = "A code problem error encountered. Please contact your system Administrator.";
        public const string INDEXOUTOFRANGEEXCEPTION = "An code problem error encountered. Please contact your system Administrator.";
        public const string IOEXCEPTION = "A Problem encountered on processing the file. Please contact your system Administrator.";
        public const string WEBEXCEPTION = "An Error occured on your http calls. Please contact your system Administrator.";
        public const string SQLEXCEPTION = "A Database error encountered. Please contact your system Administrator.";
        public const string STACKOVERFLOWEXCEPTION = "A recursive code error encountered. Please contact your system Administrator.";
        public const string OUTOFMEMORYEXCEPTION = "Out of Memory error encountered. Please contact your system Administrator.";
        public const string INVALIDCASTEXCEPTION = "A code error encountered on casting. Please contact your system Administrator.";
        public const string INVALIDOPERATIONEXCEPTION = "A Library/Plugin error encountered. Please contact your system Administrator.";
        public const string OBJECTDISPOSEDEXCEPTION = "An error occued on a disposed object. Please contact your system Administrator.";
        public const string EXCEPTION = "Something went wrong. Please contact your system Administrator.";

        public const string REGEX_ALPHANUMERIC_MSG = "Only use letters, numbers and dashes.";
        public const string VALUE_EMPTY_MSG = "Value cannot be empty.";
        public const string ERROR_MSG = "An error has occured. Error: ";
        public const string EMAIL_INVALID = "Email used is invalid.";
        public const string DOMAIN_CODE_INVALID = "Domain used is invalid, only use letters, numbers and dashes in between with a length of 3 to 50 characters.";
        public const string NAME_INVALID = "Name is invalid. Only use letters, spaces and apostrophes.";
        public const string RECORD_NOT_FOUND = "Record not found.";
        public const string USER_IS_NULL_MSG = "User cannot be null.";
        public const string USER_LOCKED_MSG = "User is currently locked. Please unlock the user";
        public const string LOGIN_IS_NULL_MSG = "User login cannot be null.";

        public const string USER_ID_CODE = "USR";

        public const string SAVE_UPDATE_FAILED_MSG = "Process has failed, please check your data content or try again later.";
        public const string SAVE_UPDATE_SUCCESS_MSG = "Successfully saved data.";

        public const string USER_ID_REQUIRED_MSG = "User id is required.";
        public const string USER_NOT_EXISTS_MSG = "User does not exist.";
        public const string USER_INVALID_LOGIN_MSG = "Username or password is incorrect.";
        public const string USER_PASSWORD_REQUIRED_MSG = "Password is required.";
        public const string USER_PASSWORD_MISMATCH_MSG = "Password and confirm password do not match.";
        public const string USER_USERNAME_EXISTS_MSG = "Username is already in use.";
        public const string USER_EMAIL_EXISTS_MSG = "Email is already in use.";
        public const string USER_CHANGE_USERNAME_MSG = "Username cannot be changed.";
        public const string USER_USERPASSLENGTH_MSG = "Passwords must be at least 8 characters.";
        public const string USER_HASH_LENGTH_MSG = "Invalid length of password hash (64 bytes expected).";

        public const string USERROLE_NO_ROLE = "No role is associated to the user.";

        public const string USERDOMAIN_NO_USER = "Selected domain is not accociated to the user.";

        public const string DOMAIN_ID_REQUIRED_MSG = "Domain id is required.";
        public const string DOMAIN_NAME_REQUIRED_MSG = "Organization name is required.";
        public const string DOMAIN_CODE_REQUIRED_MSG = "Domain code is required.";
        public const string DOMAIN_CODE_LENGTH_MSG = "Domain code must be at least 3 characters.";
        public const string DOMAIN_CODE_EXISTS_MSG = "Domain code is already in use.";
        public const string DOMAIN_NOT_EXISTS_MSG = "Domain name does not exist.";

        public const string GROUP_ID_REQUIRED_MSG = "Group id is required.";
        public const string GROUP_NAME_ALREADY_EXISTS = "Group name already exists.";

        public const string NOT_AUTHORIZED = "You are not authorized.";

        public const string OBJECT_APREADY_EXISTS = "An object with the same id already exists";
        public const string INVALID_TRANSACTION_TYPE = "Invalid transaction type";
        public const string INVALID_AMOUNT_SUBTYPE = "Invalid amount sub type";
        public const string INVALID_AMOUNT_TYPE = "Invalid amount type";
        public const string EFFECTIVE_DATE_CANNOT_BE_PAST_DATE = "Effective date cannot be a past date";
        public const string INVALID_TRANSACTION_ID = "Invalid transaction ID";
        public const string TRANSACTION_NOT_FOUND = "Transaction does not exist";
        public const string INVALID_RISK_ID = "Invalid risk ID";
        public const string INVALID_INSTALLMENT_TYPE = "Invalid installment type";
        public const string INVALID_PAYMENT_PLAN = "Invalid payment plan";
        public const string INSTALLMENT_WITH_INVOICE_DATE_NOT_FOUND = "Cannot find an installment with the given invoice date";
        public const string NO_TRANSACTIONS_FOR_RISK = "Cannot find any transaction for this risk";
        public const string INVALID_RIVTECH_APPLICATION = "Invalid Rivtech Application";
        public const string INSTALLMENT_ALREADY_INVOICED = "Installment has already been invoiced";
        public const string INVALID_USER_ID = "Invalid user id";
        public const string INVALID_PAYMENTDETAIL = "Invalid payment detail";
        public const string INVALID_INVOICE_NUMBER = "Invalid invoice number";
        public const string INVOICE_ALREADY_VOID = "This invoice has already been voided previously";
        public const string INVALID_INSTRUMENT_TYPE = "Invalid instrument type";
        public const string INVALID_DEPOSIT_PERCENTAGE = "Invalid deposit percentage";
        public const string TOTAL_AMOUNT_CHARGED_MISMATCH = "Total amount charged does not match the sum of transactions";
        public const string INVALID_PAYMENT_PROVIDER = "Invalid payment provider";

        public const string AMOUNT_ZERO = "Amount cannot be zero";
        public const string AMOUNT_LESSTHANOREQUALZERO = "Amount cannot be less than or equal to zero";
        public const string AMOUNT_LESS_THAN_ZERO = "Amount cannot be less than zero";
        public const string INVALID_REQUEST = "Invalid request";
        public const string BILLING_ADDRESS_REQUIRED = "Billing address is required";
        public const string BILLING_DETAILS_REQUIRED = "Billing details are required";
        public const string TRANSACTION_ID_REQUIRED = "Transaction id is required";

        public const string NO_PAYMENT_PROVIDER = "No payment provider.";
        public const string INVALID_PAYMENT_TYPE = "Invalid payment type";
        public const string INVALID_REVERSAL_TYPE = "Invalid reversal type";

        public const string INVALID = "Invalid";
        public const string PHONE_INVALID = "Phone number invalid";
        public const string PAYMENTACCOUNT_CANNOT_DELETE = "Cannot delete default payment account";
        public const string TRANSACTION_DETAILS_REQUIRED = "Transaction details are required";
        public const string PAYMENT_ID_REQUIRED = "Payment id is required";
        public const string RISK_ID_REQUIRED = "Risk id is required";
        public const string SOURCE_RISK_ID_REQUIRED = "Source risk id is required";
        public const string DESTINATION_RISK_ID_REQUIRED = "Destination risk id is required";
        public const string DESTINATION_POLICY_NUMBER_REQUIRED = "Destination policy number is required";
        public const string DESTINATION_RISK_ID_MUST_NOT_EQUAL_SOURCE = "Destination risk must not be equal to the source risk";
        public const string FEE_ID_REQUIRED = "Fee id is required";
        public const string PAY_LATER_NOT_ALLOWED_FOR_EIGHT_PAY = "Pay later is not allowed for eight-pay payment plan";
        public const string PAYMENT_GATEWAY_ERROR = "An error occured while connecting with the payment gateway";

        public const string TOTAL_AMOUNT_LESSTHANOREQUALZERO = "Total amount cannot be less than or equal to zero";
        public const string TOTAL_AMOUNT_LESS_THAN_ZERO = "Total amount cannot be less than zero";
        public const string PREMIUM_AMOUNT_LESS_THAN_ZERO = "Premium amount cannot be less than zero";
        public const string FEE_AMOUNT_LESS_THAN_ZERO = "Fee amount cannot be less than zero";
        public const string TAX_AMOUNT_LESS_THAN_ZERO = "Tax amount cannot be less than zero";
        public const string TOTAL_AMOUNT_ZERO = "Total amount cannot be zero";
        public const string RISK_ALREADY_BINDED = "Risk has already been binded previously";
        public const string EFFECTIVE_DATE_REQUIRED = "Effective date is required";
        public const string COMMENT_TOO_LONG = "Comment must not exceed 1000 characters";
        public const string INVALID_AMOUNT = "Invalid amount";
        public const string INVALID_TOTAL_AMOUNT = "Invalid total amount";
        public const string INVALID_PREMIUM_AMOUNT = "Invalid premium amount";
        public const string INVALID_FEE_AMOUNT = "Invalid fee amount";
        public const string INVALID_TAX_AMOUNT = "Invalid tax amount";
        public const string TOTAL_AMOUNT_MUST_EQUAL_SUM = "Total amount must be equal to the sum of premium, tax, and fee";
        public const string INVALID_TRANSACTION_AMOUNT = "Invalid transaction amount";
        public const string PAYMENT_PLANS_THE_SAME = "New payment plan cannot be equal to the current payment plan";
        public const string PAYMENT_DETAILS_REQUIRED = "Payment details are required";
        public const string CHANGE_PAYMENT_PLAN_AMOUNT_MISMATCH = "Payment applied does not match the amount to invoice";
        public const string NAME_TOO_LONG = "Name is too long";
        public const string ADDRESS_TOO_LONG = "Address is too long";
        public const string CITY_TOO_LONG = "City is too long";
        public const string STATE_TOO_LONG = "State is too long";
        public const string ZIP_CODE_TOO_LONG = "Zip Code is too long";
        public const string INSUFFICIENT_AMOUNT_TO_CHANGE_PAYMENT_PLAN = "Amount paid is not enough to change payment plan";

        public const string ADDRESS_REQUIRED = "Address is required";
        public const string AMOUNT_REQUIRED = "Amount is required";
        public const string BINDREQUEST_REQUIRED = "Bind request is required";
        public const string CANCELLATIONREQUEST_REQUIRED = "Cancellation request is required";
        public const string CARDNUMBER_REQUIRED = "Card number is required";
        public const string CITY_REQUIRED = "City is required";
        public const string COMMENT_REQUIRED = "Comment is required";
        public const string CVV_REQUIRED = "CVV is required";
        public const string DATE_REQUIRED = "Date is required";
        public const string EMAIL_REQUIRED = "Email is required";
        public const string EXPIRATIONDATE_REQUIRED = "Expiration date is required";
        public const string FIRSTNAME_REQUIRED = "First Name is required";
        public const string INVOICEDUEDATE_REQUIRED = "Invoice Due Date is required";
        public const string ISRECURRINGPAYMENT_REQUIRED = "Is Recurring Payment value is required";
        public const string LASTNAME_REQUIRED = "Last Name is required";
        public const string NAME_REQUIRED = "Name is required";
        public const string PAYMENTACCOUNT_REQUIRED = "Payment account is required";
        public const string PAYMENTACCOUNTDEFAULT_REQUIRED = "Default payment account is required";
        public const string PAYMENTSUMMARY_REQUIRED = "Payment summary is required";
        public const string POLICYNUMBER_REQUIRED = "Policy number is required";
        public const string RECEIPTDATE_REQUIRED = "Receipt date is required";
        public const string REFUNDPAYEE_REQUIRED = "Refund payee is required";
        public const string REFUNDPAYEEID_REQUIRED = "Refund payee id is required";
        public const string REFUNDREQUESTID_REQUIRED = "Refund request id is required";
        public const string REFUNDTOTYPE_REQUIRED = "Refund to type is required";
        public const string RIVTECHAPPLICATIONID_REQUIRED = "Rivtech Application Id is required";
        public const string SUSPENDED_ID_REQUIRED = "Suspended payment id is required";
        public const string SUSPENDED_PAYEE_REQUIRED = "Suspended payee is required";
        public const string SUSPENDED_PAYER_REQUIRED = "Suspended payer is required";
        public const string SUSPENDED_REASON_REQUIRED = "Suspended reason is required";
        public const string STATE_REQUIRED = "State is required";
        public const string STREET1_REQUIRED = "Street1 is required";
        public const string ZIP_REQUIRED = "Zip is required";

        public const string ADDRESS_TOOLONG = "Address cannot exceed 60 characters";
        public const string BATCHITEM_TOOLONG = "Batch item too long";
        public const string CARDNUMBER_TOOLONG = "Card number must be 13-16 digits";
        public const string CHECK_TOOLONG = "Check too long";
        public const string CHECKACCOUNTNUM_TOOLONG = "Check account num too long";
        public const string CHECKIMG_TOOLONG = "Check image too long";
        public const string CHECKNUM_TOOLONG = "Check number too long";
        public const string CHECKNUMBER_TOOLONG = "Check number cannot exceed 30 characters";
        public const string CHECKROUTINGNUM_TOOLONG = "Check routing number too long";
        public const string CITY_TOOLONG = "City cannot exceed 40 characters";
        public const string CLIENTID_TOOLONG = "Client id too long";
        public const string CLIENTNAME_TOOLONG = "Client name too long";
        public const string CVV_TOOLONG = "CVV must be 3-4 digits";
        public const string ENVELOPE_TOOLONG = "Envelope too long";
        public const string ENVELOPEIMG_TOOLONG = "Envelope image too long";
        public const string ENVELOPENUM_TOOLONG = "Envelope number too long";
        public const string EMAIL_TOOLONG = "Email cannot exceed 100 characters";
        public const string FIRSTNAME_TOOLONG = "First Name cannot exceed 50 characters";
        public const string GROUPDESCRIPTION_TOOLONG = "Group description too long";
        public const string GROUPID_TOOLONG = "Group id too long";
        public const string INVOICEIMG_TOOLONG = "Invoice image too long";
        public const string INVOICEPAGE_TOOLONG = "Invoice page too long";
        public const string LASTNAME_TOOLONG = "Last Name cannot exceed 50 characters";
        public const string LOCKBOXNUM_TOOLONG = "Lockbox number too long";
        public const string MIDDLEINITIAL_TOOLONG = "Middle initial cannot exceed 1 character";
        public const string NUMBER_TOOLONG = "Number too long";
        public const string NAME_TOOLONG = "Name cannot exceed 100 characters";
        public const string POLICYNUMBER_TOOLONG = "Policy number cannot exceed 30 characters";
        public const string STATE_TOOLONG = "State cannot exceed 40 characters";
        public const string STREET1_TOOLONG = "Street1 cannot exceed 30 characters";
        public const string STREET2_TOOLONG = "Street2 cannot exceed 30 characters";
        public const string TRANSACTIONNUM_TOOLONG = "Transaction number too long";
        public const string ZIP_TOOLONG = "Zip cannot exceed 5 characters";

        public const string ADDRESS_INVALID = "Address cannot contain special characters";
        public const string CARDNUMBER_INVALID = "Invalid credit card. Only use Visa or Mastercard";
        public const string CHECKAMOUNT_INVALID = "Check amount invalid";
        public const string CITY_INVALID = "City cannot contain special characters";
        public const string EFFECTIVEDATE_INVALID = "Invalid effective date";
        public const string EXPIRATIONDATE_INVALID = "Invalid expiration date format";
        public const string FIRSTNAME_INVALID = "Invalid first name. Only use letters, spaces and apostrophes";
        public const string ISRECURRINGPAYMENT_INVALID = "Invalid 'is recurring payment' value";
        public const string LASTNAME_INVALID = "Invalid last name. Only use letters, spaces and apostrophess";
        public const string NAMECOMMA_INVALID = "Name is invalid. Only use letters, spaces, apostrophes, and comma.";
        public const string PAYMENT_IAGREE_INVALID = "Insured must agree to authorize payment.";
        public const string PAYMENTACCOUNTID_INVALID = "Invalid payment account id";
        public const string POLICYNUMBER_INVALID = "Invalid policynumber. Only use letters, numbers and dashes";
        public const string RECEIPTDATE_INVALID = "Invalid receipt date";
        public const string REFUNDREQUESTID_INVALID = "Invalid refund request id";
        public const string REFUNDTOTYPE_INVALID = "Invalid refund to type";
        public const string STATE_INVALID = "State cannot contain special characters";
        public const string STREET1_INVALID = "Street1 cannot contain special characters";
        public const string STREET2_INVALID = "Street2 cannot contain special characters";
        public const string SUSPENDED_CARRIER_INVALID = "Invalid suspended direct bill carrier";
        public const string SUSPENDED_ID_INVALID = "Invalid suspended payment id";
        public const string SUSPENDED_PAYER_INVALID = "Invalid suspended payer";
        public const string SUSPENDED_REASON_INVALID = "Invalid suspended reason";
        public const string SUSPENDED_SOURCE_INVALID = "Invalid suspended source";
        public const string SUSPENDED_STATUS_INVALID = "Invalid suspended status";
        public const string ZIP_INVALID = "Invalid zip. Only use numbers";

        public const string INVALID_DATE = "Invalid date";
        public const string INVALID_EFFECTIVE_DATE = "Invalid effective date";

        public const string PAYMENTPROFILE_ALREADYEXIST = "Payment profile for risk already exist";
        public const string PAYMENTPROFILE_DOESNTEXIST = "Payment profile for risk doesn't exist";

        public const string ENROLL_AUTOPAYMENT_INSTRUMENTCONFLICT = "Only recurring credit card and recurring echeck instruments can be enrolled to auto payment";

        public const string TOTAL_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION = "Total paid cannot exceed total transaction";
        public const string TOTAL_PREMIUM_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION = "Total paid premium cannot exceed total transaction";
        public const string TOTAL_FEE_TAX_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION = "Total paid fee and tax cannot exceed total transaction";
        public const string TOTAL_FEE_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION = "Total paid fee cannot exceed total transaction";
        public const string TOTAL_TAX_PAID_CANNOT_EXCEED_TOTAL_TRANSACTION = "Total paid tax cannot exceed total transaction";

        public const string NEGATIVE_TOTAL_PAYMENT_NOT_ALLOWED = "Adjustments and reversals must not result to negative total payment";
        public const string PAYMENT_REVERSAL_ALREADY_APPLIED = "Payment reversal has already been previously applied for this payment";

        public const string INVALID_POLICY_STATUS = "Invalid policy status";

        public const string AMOUNT_CANCELLEDRISKPAYMENT_LESSTHANTOTALDUE = "Payment amount should be greater than or equal to total due amount";
        public const string AMOUNT_CANCELLEDRISKPAYMENT_GREATERTHANBALANCE = "Payment amount should be less than or equal to balance amount";
        public const string RECURRINGPAYMENT_PAYMENTPLANCONFLICT = "Cannot enroll to recurring payment under selected payment plan";
        public const string POST_SUSPENDEDPAYMENT_STATUSCONFLICT = "Only suspended payments with 'pending' status can be posted";
        public const string REFUNDREQUEST_PENDINGREFUND_EXIST = "A pending refund request already exist for risk";
        public const string RETURN_SUSPENDEDPAYMENT_UPDATEDETAIL_STATUSCONFLICT = "Only suspended payments with 'return' status can be updated";
        public const string RETURN_SUSPENDEDPAYMENT_STATUSCONFLICT = "Only suspended payments with 'pending' status can be returned";
        public const string REVERSE_SUSPENDEDPAYMENT_STATUSCONFLICT = "Only suspended payments with 'return' status can be reverse";

        public const string PAYMENT_MUST_BE_POLICY_PAYOFF = "Payment amount should be equal to the policy payoff amount";
        public const string MINIMUM_PAYMENT_MUST_BE_PAST_DUE = "Payment amount should at least be equal to the past due amount";
        public const string MINIMUM_PAYMENT_MUST_BE_DEPOSIT = "Payment amount should at least be equal to the deposit amount";
        public const string UPDATE_SUSPENDEDPAYMENT_STATUSCONFLICT = "Only suspended payments with 'pending' status can be updated";
        public const string UPDATE_PAYMENTACCOUNT_DEFAULTACCOUNT = "Cannot update a default payment account";
        public const string VOID_SUSPENDEDPAYMENT_STATUSCONFLICT = "Only suspended payments with 'pending' status can be voided";

        public const string ENCRYPTION_PUBLIC_KEY_REQUIRED = "Encryption public key is required";
        public const string POLICY_ALREADY_CANCELLED = "This policy has already been cancelled previously";
        public const string CHANGE_PAYPLAN_TO_PREMIUMFINANCED_NOT_ALLOWED = "Changing payment plan to premium financed is not allowed";

        public const string CANCELLATION_EFFECTIVE_DATE_REQUIRED = "Cancellation effective date is required";
        public const string INVALID_INVOICE_ID = "Invalid invoice ID";
        public const string INVALID_PAYMENT_ID = "Invalid payment ID";
    }
}
