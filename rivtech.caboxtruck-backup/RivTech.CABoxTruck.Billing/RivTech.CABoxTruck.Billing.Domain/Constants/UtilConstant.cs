﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RivTech.CABoxTruck.Billing.Domain.Constants
{
    public class UtilConstant
    {
        public const int ADDRESS_MAXLENGTH = 60;
        public const int CITY_MAXLENGTH = 40;
        public const int EMAIL_MAXLENGTH = 100;
        public const int ENUMID_MAXLENGTH = 10;
        public const int ENUMDESCRIPTION_MAXLENGTH = 50;
        public const int STATE_MAXLENGTH = 40;
        public const int STRING_MAXLENGTH = 1000;
        public const int STREET1_MAXLENGTH = 30;
        public const int STREET2_MAXLENGTH = 30;
        public const int NAME_MAXLENGTH = 100;
        public const int POLICYNUMBER_LENGTH = 23;
        public const int ZIP_MAXLENGTH = 5;
        public const int CHECKNUMBER_MAXLENGTH = 30;

        public const decimal REFUNDREQUEST_OVERPAYMENT_MAXAMOUNT = 2500m;
        public const decimal REFUNDREQUEST_OVERPAYMENT_DEFAULTAMOUNT = 0m;

        public const int AUTOPAY_DAYSBEFOREDUEDATE = 3;
        public const string LOCKBOX_FILENAME_REGEXPATTERN = @"lbx.\d{6}.out.[date].\d{6}.csv";

        public static readonly string HTTP_CLIENT_COMM_AUTO_BILLING = "HTTP Client Comm Auto Billing";
        public static readonly string HTTP_CLIENT_PNC = "HTTP Client PNC";
    }
}
