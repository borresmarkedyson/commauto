﻿using Microsoft.AspNetCore.Builder;
using RivTech.CABoxTruck.Billing.API.Middlewares;

namespace RivTech.CABoxTruck.Billing.API.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseErrorLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorLoggingMiddleware>();
        }
    }
}
