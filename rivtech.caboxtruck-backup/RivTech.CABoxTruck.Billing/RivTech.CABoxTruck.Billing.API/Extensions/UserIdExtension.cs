﻿using RivTech.CABoxTruck.Billing.Application.DTO;
using System.Linq;
using System.Security.Claims;

namespace RivTech.CABoxTruck.Billing.API.Extensions
{
    public static class UserIdExtension
    {
        public static ApplicationUserDto GetUser(this ClaimsPrincipal user)
        {
            if (!user.Claims.Any(i => i.Type == ClaimTypes.NameIdentifier))
            {
                return new ApplicationUserDto
                {
                    Id = null,
                    UserName = null
                };
            }

            string userId = user.Claims.First(i => i.Type == ClaimTypes.NameIdentifier).Value;
            string username = null;
            if (user.Claims.Any(i => i.Type == ClaimTypes.Name))
            {
                username = user.Claims.First(i => i.Type == ClaimTypes.Name).Value;
            }

            return new ApplicationUserDto
            {
                Id = userId,
                UserName = username
            };
        }
    }
}
