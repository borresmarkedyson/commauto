using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RivTech.CABoxTruck.Billing.API.Extensions;
using System.IO;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API
{
    public class Program
    {
        private static IConfiguration _configuration;
        public static IConfiguration Configuration
        {
            get => _configuration;
            set
            {
                // assures Configuration will be set only the first time
                if (_configuration == null)
                    _configuration = value;
            }
        }

        public static async Task Main(string[] args)
        {
            var host = await CreateHostBuilder(args).Build().MigrateDatabase();
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    var currentDirectory = Directory.GetCurrentDirectory();

                    config
                        .SetBasePath(currentDirectory)
                        .AddJsonFile("appsettings.json", false, true)
                        .AddEnvironmentVariables();

                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true);

                    Configuration = config.Build();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
