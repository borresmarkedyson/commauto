﻿using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RivTech.CABoxTruck.Billing.Application.DTO.Logging;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Application.Interfaces;
using RivTech.CABoxTruck.Billing.Domain.Common.CustomExceptions;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Middlewares
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILog4NetService _logger;
        private readonly IHostEnvironment _env;       

        public IConfiguration Configuration { get; }

        public ErrorLoggingMiddleware(RequestDelegate next, ILog4NetService logger, IConfiguration configuration, IHostEnvironment env)
        {
            _logger = logger;
            _next = next;
            _env = env;
            Configuration = configuration;
        }

        private const string BILLINGDETAIL_HEADER = "\"billingDetail\":{";
        private const string CREDITCARD_HEADER = "\"creditCard\":{";
        private const string BANKACCOUNT_HEADER = "\"bankAccount\":{"; 
        private const string BILLINGDETAIL_END = "},";
        private const string PLACEHOLDER = " [redacted]";

        public static readonly IList<string> BILLINGDETAIL_HEADERS = new ReadOnlyCollection<string>
            (new List<String> { CREDITCARD_HEADER, BANKACCOUNT_HEADER });

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                if (ex is AggregateException)
                {
                    await HandleAggregateExceptionAsync(httpContext, (AggregateException)ex);
                }
                else
                {
                    await HandleExceptionAsync(httpContext, ex);
                }
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var innerMostException = GetInnerMostException(exception);
            string innerMostExceptionMessage = innerMostException != null ? innerMostException.Message : string.Empty;
            //TODO: do not show stack trace in prod. save in db instead
            var response = new ExceptionResponse((int)HttpStatusCode.InternalServerError, innerMostExceptionMessage, exception.StackTrace.ToString());

            await LogErrorToDatabase(context, exception);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await context.Response.WriteAsync(response.ToString());
        }

        private async Task HandleAggregateExceptionAsync(HttpContext context, AggregateException aggregateException)
        {
            await LogErrorToDatabase(context, aggregateException);

            var response = new ExceptionResponse((int)HttpStatusCode.InternalServerError, aggregateException.Message, aggregateException.StackTrace.ToString());

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await context.Response.WriteAsync(response.ToString());
        }

        private async Task LogErrorToDatabase(HttpContext context, Exception exception)
        {
            //Get Error Code
            int errorCode = 0;
            if (!(exception is Win32Exception w32ex))
            {
                w32ex = exception.InnerException as Win32Exception;
            }

            if (w32ex != null)
            {
                errorCode = w32ex.ErrorCode;
            }

            ErrorLogDto errLog = new ErrorLogDto
            {
                UserId = 0,
                ErrorMessage = exception.Message,
                ErrorCode = errorCode != 0 ? errorCode.ToString() : null,
                JsonMessage = exception.ToString(),
                Action = context.Request.Path.Value,
                Method = context.Request.Method,
                Body = await GetRequestBody(context),
                CreatedDate = DateTime.Now
            };

            await _logger.Error(errLog);
        }

        public async Task<string> GetRequestBody(HttpContext context)
        {
            context.Request.Body.Position = 0;

            using var streamReader = new StreamReader(context.Request.Body);
            string bodyContent = await streamReader.ReadToEndAsync();

            context.Request.Body.Position = 0;

            return RemoveBillingDetail(bodyContent);
        }

        private string RemoveBillingDetail(string body)
        {
            //check if request has billing details
            if (body.IndexOf(BILLINGDETAIL_HEADER, StringComparison.OrdinalIgnoreCase) < 0) return body;

            int startIndex, endIndex;
            string newBody;
            foreach (var header in BILLINGDETAIL_HEADERS)
            {
                //find start and end of billing detail header
                startIndex = body.IndexOf(header, StringComparison.OrdinalIgnoreCase);
                if (startIndex < 0) continue;
                endIndex = body.IndexOf(BILLINGDETAIL_END, startIndex, StringComparison.OrdinalIgnoreCase);

                //remove billing details
                newBody = body.Remove(startIndex + header.Length, endIndex - startIndex - header.Length);
                //insert a placeholder on removed billing details
                body = newBody.Insert(startIndex + header.Length, PLACEHOLDER);
            }
            return body;
        }

        private Exception GetInnerMostException(Exception ex)
        {
            Exception currentEx = ex;
            while (currentEx.InnerException != null)
            {
                currentEx = currentEx.InnerException;
            }

            return currentEx;
        }

        private ExceptionResponse ErrorMessageHandler(Exception exception, string innermostExceptionMessage)
        {
            ExceptionResponse response = new ExceptionResponse((int)HttpStatusCode.InternalServerError);

            var errorName = exception.GetType().Name.ToString();
            response.Details = innermostExceptionMessage;

            switch (errorName)
            {
                case nameof(NotImplementedException):
                    response.StatusCode = (int)HttpStatusCode.NotImplemented;
                    response.Message = ExceptionMessages.NOIMPLEMENTEDEXCEPTION;
                    break;
                case nameof(NotSupportedException):
                    response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    response.Message = ExceptionMessages.NOIMPLEMENTEDEXCEPTION;
                    break;
                case nameof(ArgumentException):
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = ExceptionMessages.ARGUMENTEXCEPTION;
                    break;
                case nameof(NullReferenceException):
                    response.Message = ExceptionMessages.NULLREFERENCEEXCEPTION;
                    break;
                case nameof(AuthenticationException):
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    response.Message = ExceptionMessages.AUTHENTICATIONEXCEPTION;
                    break;
                case nameof(UnauthorizedAccessException):
                    response.StatusCode = (int)HttpStatusCode.Forbidden;
                    response.Message = ExceptionMessages.UNAUTHORIZEDEXCEPTION;
                    break;
                case nameof(DbUpdateConcurrencyException):
                case nameof(DbUpdateException):
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    response.Message = ExceptionMessages.DBUPDATEEXCEPTION;
                    break;
                case nameof(DbException):
                    response.Message = ExceptionMessages.DBEXCEPTION;
                    break;
                case nameof(IndexOutOfRangeException):
                    response.Message = ExceptionMessages.INDEXOUTOFRANGEEXCEPTION;
                    break;
                case nameof(IOException):
                    response.Message = ExceptionMessages.IOEXCEPTION;
                    break;
                case nameof(WebException):
                    response.Message = ExceptionMessages.WEBEXCEPTION;
                    break;
                case nameof(SqlException):
                    response.Message = ExceptionMessages.SQLEXCEPTION;
                    break;
                case nameof(StackOverflowException):
                    response.StatusCode = (int)HttpStatusCode.RequestEntityTooLarge;
                    response.Message = ExceptionMessages.STACKOVERFLOWEXCEPTION;
                    break;
                case nameof(OutOfMemoryException):
                    response.StatusCode = (int)HttpStatusCode.UnsupportedMediaType;
                    response.Message = ExceptionMessages.OUTOFMEMORYEXCEPTION;
                    break;
                case nameof(InvalidCastException):
                    response.Message = ExceptionMessages.INVALIDCASTEXCEPTION;
                    break;
                case nameof(InvalidOperationException):
                    response.Message = ExceptionMessages.INVALIDOPERATIONEXCEPTION;
                    break;
                case nameof(ObjectDisposedException):
                    response.Message = ExceptionMessages.OBJECTDISPOSEDEXCEPTION;
                    break;
                case nameof(ModelNotFoundException):
                    response.Message = exception.Message;
                    break;
                case nameof(ObjectAlreadyExistsException):
                    response.Message = ExceptionMessages.OBJECT_APREADY_EXISTS;
                    break;
                case nameof(PaymentParameterException):
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = exception.Message;
                    break;
                case nameof(ParameterException):
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = exception.Message;
                    break;
                case nameof(AuthNetException):
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    response.Message = exception.Message;
                    break;
                default:
                    response.Message = ExceptionMessages.EXCEPTION;
                    break;
            }

            return response;
        }
    }
}
