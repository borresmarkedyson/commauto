﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentAccountController : ControllerBase
    {
        private readonly IPaymentAccountService _paymentAccountService;
        private readonly IUserDateService _userDateService;

        public PaymentAccountController(
             IPaymentAccountService paymentAccountService,
             IUserDateService userDateService
        )
        {
            _paymentAccountService = paymentAccountService;
            _userDateService = userDateService;
        }

        [HttpPost("CreateAccount")]
        [Produces(typeof(PaymentAccountDto))]
        public async Task<IActionResult> CreateAccount([FromBody] PaymentAccountRequestDto accountRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentAccountService.CreateAccount(accountRequest));
        }

        [HttpPut("DefaultAccount")]
        [Produces(typeof(PaymentAccountDto))]
        public async Task<IActionResult> UpdateDefaultAccount([FromBody] UpdateDefaultAccountRequestDto accountRequest)
        {
            return Ok(await _paymentAccountService.UpdateDefaultAccount(accountRequest));
        }

        [HttpPut("")]
        [Produces(typeof(PaymentAccountDto))]
        public async Task<IActionResult> UpdateAccount([FromBody] UpdatePaymentAccountDto updateRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentAccountService.UpdateAccount(updateRequest));
        }

        [HttpDelete("DeleteAccount")]
        [Produces(typeof(PaymentAccountDto))]
        public async Task<IActionResult> DeleteAccount([FromBody] PaymentAccountRequestDto accountRequest)
        {
            return Ok(await _paymentAccountService.DeleteAccount(accountRequest));
        }

        [HttpGet("GetAccount/{riskId}")]
        [Produces(typeof(IEnumerable<PaymentAccountDto>))]
        public async Task<IActionResult> GetAccount([FromRoute] Guid riskId)
        {
            return Ok(await _paymentAccountService.GetAccounts(riskId));
        }
    }
}