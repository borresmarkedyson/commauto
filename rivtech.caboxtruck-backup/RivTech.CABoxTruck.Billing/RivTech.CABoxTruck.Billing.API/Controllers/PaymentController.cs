using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using RivTech.CABoxTruck.Billing.Domain.Entities;
using RivTech.CABoxTruck.Billing.Domain.Factory.Payments;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly IPaymentViewService _paymentViewService;
        private readonly IReversePaymentService _reversePaymentService;
        private readonly ITransferPaymentService _transferPaymentService;
        private readonly IPaymentValidationService _paymentValidationService;
        private readonly ICancelledRiskPaymentService _cancelledRiskPaymentService;
        private readonly IUserDateService _userDateService;
        private readonly IMapper _mapper;

        public PaymentController(
            IPaymentService paymentService,
            IPaymentViewService paymentViewService,
            IReversePaymentService reversePaymentService,
            ITransferPaymentService transferPaymentService,
            IPaymentValidationService paymentValidationService,
            ICancelledRiskPaymentService cancelledRiskPaymentService,
            IMapper mapper,
            IUserDateService userDateService
        )
        {
            _paymentService = paymentService;
            _paymentViewService = paymentViewService;
            _reversePaymentService = reversePaymentService;
            _transferPaymentService = transferPaymentService;
            _paymentValidationService = paymentValidationService;
            _cancelledRiskPaymentService = cancelledRiskPaymentService;
            _userDateService = userDateService;
            _mapper = mapper;
        }

        [HttpPost("PostBasic")]
        [Produces(typeof(PostPaymentViewDto))]
        [AllowAnonymous]
        public async Task<IActionResult> PostBasicPayment([FromBody] PaymentRequestDto paymentRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentService.PostPayment(paymentRequest));
        }

        [HttpGet("GetAll/{riskId}")]
        [Produces(typeof(IEnumerable<PaymentDto>))]
        public async Task<IActionResult> GetAll([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentService.GetAllByRiskId(riskId));
        }

        [HttpGet("GetAllView/{riskId}")]
        [Produces(typeof(IEnumerable<PaymentViewDto>))]
        public async Task<IActionResult> GetAllView([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentViewService.GetPaymentView(riskId));
        }

        [HttpPost("reverse")]
        [Produces(typeof(FeeInvoiceDto))]
        public async Task<IActionResult> ReversePayment([FromBody] ReversePaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _reversePaymentService.ReversePayment(request));
        }

        [HttpPost("authorize/{riskId}")]
        [Produces(typeof(string))]
        public async Task<IActionResult> AuthorizePayment([FromRoute] Guid riskId, [FromBody] AuthorizePaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(_paymentService.AuthorizePayment(riskId, request));
        }

        [HttpGet("details/{riskId}/{paymentId}")]
        [Produces(typeof(PaymentDetailsViewDto))]
        public async Task<IActionResult> GetPaymentDetails([FromRoute] Guid riskId, [FromRoute] Guid paymentId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentViewService.GetPaymentDetailsView(riskId, paymentId));
        }

        [HttpPost("transfer")]
        [Produces(typeof(IEnumerable<TransferPaymentDto>))]
        public async Task<IActionResult> TransferPayment([FromBody] TransferPaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transferPaymentService.TransferPayment(request));
        }

        [HttpGet("allowed-range")]
        [Produces(typeof(AllowedPaymentRangeDto))]
        public async Task<IActionResult> GetAllowedPaymentRange([FromQuery] Guid riskId, [FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentValidationService.GetAllowedPaymentRange(riskId, appId));
        }

        [HttpPost("PayToReinstate")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> PayToReinstate([FromBody] PaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _cancelledRiskPaymentService.PayToReinstate(request));
        }

        [HttpPost("PayBalance")]
        [Produces(typeof(PostPaymentViewDto))]
        public async Task<IActionResult> PayBalance([FromBody] PaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _cancelledRiskPaymentService.PayBalance(request));
        }

        [HttpPost("pay-post")]
        public async Task<IActionResult> PayWithPosting(PaymentSummaryCCEFTDto ps)
        {
            try
            {
                if (!ps.RiskId.HasValue)
                    throw new ArgumentException("RiskId is required.");

                var paymentSummaryCCEFT = _mapper.Map<PaymentSummaryCCEFT>(ps);

                var customer = new CustomerDetailsFactory().Create(paymentSummaryCCEFT);
                var invoice = new InvoiceDetails(paymentSummaryCCEFT.Amount, paymentSummaryCCEFT.PolicyNumber);
                var payment = new PaymentFactory().Create(paymentSummaryCCEFT);

                //CustomerDetails customer = new CustomerDetails(
                //"Jon", "Snow", "jon.snow@got.com",
                //new Address("Riverside Street", "Winterfell", "CA", "123456789"));
                //InvoiceDetails invoice = new InvoiceDetails(10, "1234567");
                //PaymentCCEFT payment = new CreditCardPayment(CardType.Visa, "4001143077274115", "123", 2022, 06);

                //CustomerDetails customer = new CustomerDetails(
                //"Jon",
                //"Snow",
                //"jon.snow@got.com",
                //new Address("Riverside Street", "Winterfell", "CA", "123456789"));
                //InvoiceDetails invoice = new InvoiceDetails(10, "1234567");
                //PaymentCCEFT payment = new AchPayment("051000017", "091000019", AchType.Savings,
                //    AchUsageType.Personal);

                ps.PostedBy = "susercommautobilling";
                var result = await _paymentService.PayWithPostingAsync(
                    customer,
                    invoice,
                    payment,
                    ps.PostedBy);

                if (result.Success)
                    return Ok(result);

                if (result.Errors.Length > 0)
                    return BadRequest(result.Errors);

                throw new Exception(result.Message);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message + " " + e.StackTrace);
            }
        }
    }
}