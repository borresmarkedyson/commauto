﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolicyCancellationController : ControllerBase
    {
        private readonly IPolicyCancellationService _policyCancellationService;
        private readonly IUserDateService _userDateService;

        public PolicyCancellationController(
            IPolicyCancellationService policyCancellationService,
            IUserDateService userDateService
        )
        {
            _policyCancellationService = policyCancellationService;
            _userDateService = userDateService;
        }

        [HttpPost]
        [Produces(typeof(PolicyCancellationInvoiceDto))]
        public async Task<IActionResult> ProrateAndCancelPolicy([FromBody] PolicyCancellationRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _policyCancellationService.ProrateAndCancelPolicy(request));
        }

        [HttpPost("pending")]
        [Produces(typeof(NopcRiskDto))]
        public async Task<IActionResult> SetPendingCancellation([FromBody] PendingCancellationRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _policyCancellationService.SetPendingCancellation(request));
        }
    }
}
