﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using System;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RefundRequestController : ControllerBase
    {
        private readonly IRefundRequestService _refundRequestService;
        private readonly IUserDateService _userDateService;

        public RefundRequestController(
             IRefundRequestService refundRequestService,
             IUserDateService userDateService
        )
        {
            _refundRequestService = refundRequestService;
            _userDateService = userDateService;
        }

        [HttpGet("{applicationId}")]
        [Produces(typeof(IEnumerable<RefundRequestRiskViewDto>))]
        public async Task<IActionResult> GetRefundRequest([FromRoute] string applicationId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(_refundRequestService.GetRiskListWithRefundRequest(applicationId));
        }

        [HttpPost()]
        [Produces(typeof(IEnumerable<RefundRequestViewDto>))]
        public async Task<IActionResult> AutoCheckRefundRequest([FromBody] AutoCheckRefundRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _refundRequestService.AutoCheckRefundRequest(request));
        }

        [HttpPost("refundrequest")]
        [Produces(typeof(IEnumerable<RefundRequestViewDto>))]
        public async Task<IActionResult> CreateRefundRequest([FromBody] CreateRefundRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _refundRequestService.CreateRefundRequest(request));
        }

        [HttpPut()]
        [Produces(typeof(RefundReqDto))]
        public async Task<IActionResult> UpdateRefundRequest([FromBody] UpdateRefundRequestDto request)
        {
            return Ok(await _refundRequestService.UpdateRefundRequest(request));
        }

        [HttpDelete("{refundRequestId}")]
        [Produces(typeof(RefundReqDto))]
        public async Task<IActionResult> DeleteRefundRequest([FromRoute] Guid refundRequestId)
        {
            return Ok(await _refundRequestService.DeleteRefundRequest(refundRequestId));
        }
    }
}