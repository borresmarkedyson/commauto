﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuspendedPaymentController : ControllerBase
    {
        private readonly ISuspendedPaymentService _suspendedPaymentService;
        private readonly ISuspendedReturnDetailService _suspendedReturnDetailService;
        private readonly ISuspendedPaymentViewService _suspendedPaymentViewService;
        private readonly ISuspendedLockboxService _suspendedLockboxService;
        private readonly ISuspendedDirectBillService _suspendedDirectBillService;
        private readonly IUserDateService _userDateService;

        public SuspendedPaymentController(
            ISuspendedPaymentService suspendedPaymentService,
            ISuspendedReturnDetailService suspendedReturnDetailService,
            ISuspendedPaymentViewService suspendedPaymentViewService,
            ISuspendedLockboxService suspendedLockboxService,
            IUserDateService userDateService,
            ISuspendedDirectBillService suspendedDirectBillService
        )
        {
            _suspendedPaymentService = suspendedPaymentService;
            _suspendedReturnDetailService = suspendedReturnDetailService;
            _suspendedPaymentViewService = suspendedPaymentViewService;
            _suspendedLockboxService = suspendedLockboxService;
            _userDateService = userDateService;
            _suspendedDirectBillService = suspendedDirectBillService;
        }

        [HttpPost("CreateSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> CreateSuspendedPayment([FromBody] CreateSuspendedPaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedPaymentService.CreateSuspendedPayment(request));
        }

        [HttpPut()]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> UpdateSuspendedPayment([FromBody] UpdateSuspendedPaymentRequestDto request)
        {
            return Ok(await _suspendedPaymentService.UpdateSuspendedPayment(request));
        }

        [HttpPost("PostSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> PostSuspendedPayment([FromBody] PostSuspendedPaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedPaymentService.PostSuspendedPayment(request));
        }

        [HttpPost("VoidSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> VoidSuspendedPayment([FromBody] VoidSuspendedPaymentRequestDto suspendedPaymentRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedPaymentService.VoidSuspendedPayment(suspendedPaymentRequest));
        }

        [HttpPost("ReturnSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> ReturnSuspendedPayment([FromBody] ReturnSuspendedPaymentRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedPaymentService.ReturnSuspendedPayment(request));
        }

        [HttpPost("ReverseSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> ReverseSuspendedPayment([FromBody] ReverseSuspendedPaymentRequestDto suspendedPaymentRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedPaymentService.ReverseSuspendedPayment(suspendedPaymentRequest));
        }

        [HttpGet("GetAllView/{statusId}")]
        [Produces(typeof(IEnumerable<SuspendedPaymentViewDto>))]
        public async Task<IActionResult> GetAllView([FromRoute] string statusId)
        {
            return Ok(await _suspendedPaymentViewService.GetAllByStatus(statusId));
        }

        [HttpGet("GetReturnedView")]
        [Produces(typeof(IEnumerable<ReturnSuspendedViewDto>))]
        public async Task<IActionResult> GetReturnedView()
        {
            return Ok(await _suspendedPaymentViewService.GetReturnedSuspendedPayment());
        }

        [HttpGet("GetDetailView/{suspendedPaymentId}")]
        [Produces(typeof(SuspendedPaymentDetailViewDto))]
        public async Task<IActionResult> GetDetailView([FromRoute] Guid suspendedPaymentId)
        {
            return Ok(await _suspendedPaymentViewService.GetDetailView(suspendedPaymentId));
        }

        [HttpPut("UpdateReturnDetail")]
        [Produces(typeof(SuspendedPaymentDto))]
        public async Task<IActionResult> UpdateReturnDetail([FromBody] UpdateReturnSuspendedPaymentRequestDto returnDetailRequest)
        {
            return Ok(await _suspendedReturnDetailService.UpdateReturnDetail(returnDetailRequest));
        }

        [HttpPost("PostLockboxSuspendedPayment")]
        [Produces(typeof(SuspendedPaymentJobViewDto))]
        public async Task<IActionResult> ImportLockboxFile([FromBody] PostLockboxSuspendedRequest request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedLockboxService.ImportLockboxFile(request));
        }

        [HttpPost("DirectBill")]
        [Produces(typeof(SuspendedPaymentJobViewDto))]
        public async Task<IActionResult> ImportDirectBill([FromBody] DirectBillRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _suspendedDirectBillService.ImportDirectBillFile(request));
        }
    }
}