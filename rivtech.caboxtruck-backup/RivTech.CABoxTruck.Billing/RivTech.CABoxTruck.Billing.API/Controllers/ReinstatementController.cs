﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReinstatementController : ControllerBase
    {
        private readonly IReinstatementService _reinstatementService;
        private readonly IUserDateService _userDateService;

        public ReinstatementController(
             IReinstatementService reinstatementService,
             IUserDateService userDateService
        )
        {
            _reinstatementService = reinstatementService;
            _userDateService = userDateService;
        }

        [HttpPost("")]
        [Produces(typeof(IEnumerable<ReinstatementRuleView>))]
        public async Task<IActionResult> GetRisksForReinstatement([FromBody] GetRisksForReinstatementRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _reinstatementService.GetRisksForReinstatement(request));
        }

        [HttpGet("{riskId}")]
        [Produces(typeof(ReinstatementRuleView))]
        public async Task<IActionResult> GetRiskForReinstatement([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _reinstatementService.GetRiskForReinstatement(riskId));
        }

        [HttpPost("reinstate")]
        [Produces(typeof(RiskDto))]
        public async Task<IActionResult> Reinstate([FromBody] ReinstateRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _reinstatementService.Reinstate(request));
        }

        [HttpPost("tag-as-manual")]
        [Produces(typeof(RisksTaggedForManualReinstatementDto))]
        public async Task<IActionResult> TagForManualReinstatement([FromBody] TagForManualReinstatementRequestDto request)
        {
            return Ok(await _reinstatementService.TagForManualReinstatement(request));
        }
    }
}