﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentSubscriptionController : ControllerBase
    {
        private readonly IPaymentSubscriptionService _paymentSubscriptionService;
        private readonly IUserDateService _userDateService;

        public PaymentSubscriptionController(
             IPaymentSubscriptionService paymentSubscriptionService,
             IUserDateService userDateService
        )
        {
            _paymentSubscriptionService = paymentSubscriptionService;
            _userDateService = userDateService;
        }

        [HttpPost("")]
        [Produces(typeof(RecurringPaymentJobViewDto))]
        public async Task<IActionResult> PostPaymentAllRisk([FromBody] PostPaymentAllEnrolledAccountRequest request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentSubscriptionService.PostPaymentAllRisk(request));
        }

        [HttpPost("{riskId}")]
        [Produces(typeof(PaymentDto))]
        public async Task<IActionResult> PostPaymentSingleRisk([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentSubscriptionService.PostPaymentSingleRisk(riskId));
        }
    }
}