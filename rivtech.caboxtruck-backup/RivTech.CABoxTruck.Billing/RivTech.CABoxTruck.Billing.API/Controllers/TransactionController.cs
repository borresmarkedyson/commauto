﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;
        private readonly ITransactionFeeService _transactionFeeService;
        private readonly IUserDateService _userDateService;

        public TransactionController(
            ITransactionService transactionService,
            ITransactionFeeService transactionFeeService,
            IUserDateService userDateService)
        {
            _transactionService = transactionService;
            _transactionFeeService = transactionFeeService;
            _userDateService = userDateService;
        }

        [HttpGet("{riskId}")]
        [Produces(typeof(IEnumerable<TransactionDto>))]
        public async Task<IActionResult> GetAll([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionService.GetAllAsync(riskId));
        }

        [HttpPost("{riskId}")]
        [Produces(typeof(string))]
        public async Task<IActionResult> Post([FromRoute] Guid riskId, [FromBody] TransactionDto transaction)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionService.InsertTransactionAsync(riskId, transaction));
        }

        [HttpPost("fee/void")]
        [Produces(typeof(FeeInvoiceDto))]
        public async Task<IActionResult> VoidFee([FromBody] VoidFeeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.VoidTransactionFee(request));
        }

        [HttpPost("fee")]
        [Produces(typeof(FeeInvoiceDto))]
        public async Task<IActionResult> AddTransactionFee([FromBody] AddTransactionFeeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.AddTransactionFee(request, false));
        }

        [HttpGet("fee/{riskId}")]
        [Produces(typeof(IEnumerable<TransactionFeeDto>))]
        public async Task<IActionResult> GetTransactionFees([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.GetTransactionFees(riskId));
        }

        [HttpGet("tax/{riskId}")]
        [Produces(typeof(IEnumerable<TransactionTaxDto>))]
        public async Task<IActionResult> GetTransactionTaxes([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.GetTransactionTaxes(riskId));
        }

        [HttpGet("summary")]
        [Produces(typeof(PremiumFeesAndTaxDto))]
        public async Task<IActionResult> GetTransactionSummary([FromQuery] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionService.GetTransactionSummary(riskId));
        }

        [HttpPost("fee/revert-added-fee")]
        [Produces(typeof(IEnumerable<TransactionFeeDto>))]
        public async Task<IActionResult> RevertAddedFee([FromBody] RevertFeeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.RevertAddedFee(request));
        }

        [HttpPost("fee/revert-voided-fee")]
        [Produces(typeof(IEnumerable<TransactionFeeDto>))]
        public async Task<IActionResult> RevertVoidedFee([FromBody] RevertFeeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _transactionFeeService.RevertVoidedFee(request));
        }
    }
}
