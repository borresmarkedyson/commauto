﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoticeController : ControllerBase
    {
        private readonly IFlatCancellationService _flatCancellationService;
        private readonly IBalanceDueService _balanceDueService;
        private readonly INopcService _nopcService;
        private readonly IFinalCancellationService _finalCancellationService;
        private readonly IUserDateService _userDateService;

        public NoticeController(
            IFlatCancellationService flatCancellationService,
            IBalanceDueService balanceDueService,
            INopcService nopcService,
            IFinalCancellationService finalCancellationService,
            IUserDateService userDateService
        )
        {
            _flatCancellationService = flatCancellationService;
            _balanceDueService = balanceDueService;
            _nopcService = nopcService;
            _finalCancellationService = finalCancellationService;
            _userDateService = userDateService;
        }


        [HttpGet("flat-cancel-nopc")]
        [Produces(typeof(List<FlatCancelNopcRiskDto>))]
        public async Task<IActionResult> GetFlatCancelNopcRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _flatCancellationService.GetFlatCancelNopcRisks(appId));
        }

        [HttpPost("flat-cancel-nopc")]
        [Produces(typeof(FlatCancelNopcRiskDto))]
        public async Task<IActionResult> SetFlatCancelNopc([FromBody] NoticeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _flatCancellationService.SetFlatCancelNopc(request));
        }

        [HttpGet("balance-due")]
        [Produces(typeof(List<BalanceDueRiskDto>))]
        public async Task<IActionResult> GetBalanceDueRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _balanceDueService.GetBalanceDueRisks(appId));
        }

        [HttpPost("balance-due")]
        [Produces(typeof(BalanceDueRiskDto))]
        public async Task<IActionResult> SetBalanceDue([FromBody] NoticeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _balanceDueService.SetBalanceDue(request));
        }

        [HttpGet("nopc")]
        [Produces(typeof(List<NopcRiskDto>))]
        public async Task<IActionResult> GetNopcRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _nopcService.GetNopcRisks(appId));
        }

        [HttpPost("nopc")]
        [Produces(typeof(NopcRiskDto))]
        public async Task<IActionResult> SetNopc([FromBody] NoticeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _nopcService.SetNopc(request));
        }

        [HttpGet("nopc/rescind")]
        [Produces(typeof(List<NopcRiskDto>))]
        public async Task<IActionResult> GetNopcRisksToRescind([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _nopcService.GetNopcRisksToRescind(appId));
        }

        [HttpPost("nopc/rescind")]
        [Produces(typeof(NopcRiskDto))]
        public async Task<IActionResult> RescindNopcRisk([FromBody] NoticeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _nopcService.RescindNopcRisk(request));
        }

        [HttpGet("final-cancel")]
        [Produces(typeof(List<FinalCancellationRiskDto>))]
        public async Task<IActionResult> GetFinalCancellationRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _finalCancellationService.GetFinalCancellationRisks(appId));
        }

        [HttpPost("final-cancel")]
        [Produces(typeof(FinalCancellationRiskDto))]
        public async Task<IActionResult> SetFinalCancellation([FromBody] NoticeRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _finalCancellationService.SetFinalCancellation(request));
        }
    }
}
