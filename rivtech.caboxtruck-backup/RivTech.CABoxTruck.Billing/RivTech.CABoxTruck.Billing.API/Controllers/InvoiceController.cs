﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;
        private readonly IUserDateService _userDateService;

        public InvoiceController(
            IInvoiceService invoiceService,
            IUserDateService userDateService)
        {
            _invoiceService = invoiceService;
            _userDateService = userDateService;
        }

        [HttpGet("{riskId}")]
        [Produces(typeof(IEnumerable<InvoiceDto>))]
        public async Task<IActionResult> GetAll([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _invoiceService.GetAllAsync(riskId));
        }

        [HttpPost("{riskId}")]
        [Produces(typeof(InvoiceDto))]
        public async Task<IActionResult> Post([FromRoute] Guid riskId, [FromBody] GenerateInvoiceRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _invoiceService.GenerateInvoiceAsync(riskId, request, false, 0));
        }

        [HttpPost]
        [Produces(typeof(List<InvoiceNotifView>))]
        [AllowAnonymous]
        public async Task<IActionResult> GenerateCurrentDayInstallmentInvoices([FromBody] GenerateInvoiceRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _invoiceService.GenerateCurrentDayInvoices(request));
        }

        [HttpPut("void")]
        [Produces(typeof(string))]
        public async Task<IActionResult> VoidInvoice([FromBody] VoidInvoiceRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _invoiceService.VoidInvoice(request.InvoiceNumber, request.AppId, true));
        }
    }
}
