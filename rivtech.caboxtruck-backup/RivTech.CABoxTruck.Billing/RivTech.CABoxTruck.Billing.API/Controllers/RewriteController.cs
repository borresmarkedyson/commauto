﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RewriteController : ControllerBase
    {
        private readonly IRewriteService _rewriteService;
        private readonly IUserDateService _userDateService;

        public RewriteController(
             IRewriteService rewriteService,
             IUserDateService userDateService
        )
        {
            _rewriteService = rewriteService;
            _userDateService = userDateService;
        }

        [HttpPost()]
        [Produces(typeof(RewriteViewDto))]
        public async Task<IActionResult> Rewrite([FromBody] RewriteRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _rewriteService.Rewrite(request));
        }
    }
}