﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RefundController : ControllerBase
    {
        private readonly IRefundService _refundService;
        private readonly IUserDateService _userDateService;

        public RefundController(
            IRefundService refundService,
            IUserDateService userDateService
        )
        {
            _refundService = refundService;
            _userDateService = userDateService;
        }

        [HttpPost]
        [Produces(typeof(PaymentDto))]
        public async Task<IActionResult> PostRefund([FromBody] RefundRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _refundService.PostRefund(request));
        }

        [HttpPost("set-clear-date")]
        [Produces(typeof(PaymentDto))]
        public async Task<IActionResult> SetClearDate([FromBody] SetClearDateRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _refundService.SetClearDate(request));
        }

        [HttpPost("set-escheat-date")]
        [Produces(typeof(PaymentDto))]
        public async Task<IActionResult> SetEscheatDate([FromBody] SetEscheatDateRequestDto request)
        {
            return Ok(await _refundService.SetEscheatDate(request));
        }
    }
}
