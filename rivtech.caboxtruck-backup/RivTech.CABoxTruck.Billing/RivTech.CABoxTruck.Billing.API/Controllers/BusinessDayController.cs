﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessDayController : ControllerBase
    {
        private readonly IBusinessDayService _businessDayService;
        private readonly IUserDateService _userDateService;

        public BusinessDayController(
            IBusinessDayService businessDayService,
            IUserDateService userDateService
        )
        {
            _businessDayService = businessDayService;
            _userDateService = userDateService;
        }

        [HttpGet]
        [Produces(typeof(bool))]
        public async Task<IActionResult> CheckIfBusinessDay([FromQuery] DateTime date)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _businessDayService.CheckIfBusinessDay(date));
        }
    }
}
