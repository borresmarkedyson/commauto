﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class EndorsementController : ControllerBase
    {
        private readonly IEndorsementService _endorsementService;
        private readonly IUserDateService _userDateService;

        public EndorsementController(
            IEndorsementService endorsementService,
            IUserDateService userDateService
        )
        {
            _endorsementService = endorsementService;
            _userDateService = userDateService;
        }

        [HttpPost]
        [Produces(typeof(InvoiceDto))]
        public async Task<IActionResult> AddEndorsement([FromRoute] Guid riskId, [FromBody] EndorsementRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _endorsementService.AddEndorsement(request));
        }
    }

}
