﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentDueController : ControllerBase
    {
        private readonly IPaymentDueService _paymentDueService;
        private readonly IUserDateService _userDateService;

        public PaymentDueController(
            IPaymentDueService paymentDueService,
            IUserDateService userDateService
        )
        {
            _paymentDueService = paymentDueService;
            _userDateService = userDateService;
        }

        [HttpGet]
        [Produces(typeof(PaymentDueRisksDto))]
        public async Task<IActionResult> GetPaymentDueRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentDueService.GetPaymentDueRisks(appId));
        }

        [HttpGet("past-due")]
        [Produces(typeof(PaymentDueRisksDto))]
        public async Task<IActionResult> GetPaymentPastDueRisks([FromQuery] string appId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentDueService.GetPaymentPastDueRisks(appId));
        }
    }
}
