﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiskController : ControllerBase
    {
        private readonly IRiskService _riskService;

        public RiskController(
            IRiskService riskService
        )
        {
            _riskService = riskService;
        }

        [HttpPut()]
        [Produces(typeof(RiskDto))]
        public async Task<IActionResult> UpdateRisk([FromBody] UpdateRiskRequestDto request)
        {
            return Ok(await _riskService.UpdateRisk(request));
        }

        [HttpPost("get-risks-for-notice-cancellation")]
        [Produces(typeof(List<Guid>))]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllRiskForNoticeCancellation()
        {
            var jobDate = GetRequestDate(Request);
            return Ok(await _riskService.GetAllRiskForNoticeCancellation(jobDate));
        }

        private DateTime GetRequestDate(HttpRequest request)
        {
            var jobDate = DateTime.Now;
            if (request.Headers.TryGetValue("userdate", out var headerValues))
            {
                jobDate = DateTime.Parse(headerValues.FirstOrDefault());
            }

            return jobDate;
        }
    }
}