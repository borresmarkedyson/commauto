﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.DTO.Views;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BillingController : ControllerBase
    {
        private readonly IBindService _bindService;
        private readonly IBillingSummaryService _billingSummaryService;
        private readonly IChangePaymentPlanService _changePaymentPlanService;
        private readonly IUserDateService _userDateService;
        private readonly ITaxService _taxService;
        private readonly IInstallmentScheduleService _installmentScheduleService;

        public BillingController(
            IBindService bindService,
            IBillingSummaryService billingSummaryService,
            IChangePaymentPlanService changePaymentPlanService,
            IUserDateService userDateService,
            ITaxService taxService,
            IInstallmentScheduleService installmentScheduleService
        )
        {
            _bindService = bindService;
            _billingSummaryService = billingSummaryService;
            _changePaymentPlanService = changePaymentPlanService;
            _userDateService = userDateService;
            _taxService = taxService;
            _installmentScheduleService = installmentScheduleService;
        }

        [HttpPost("bind/{riskId}")]
        [Produces(typeof(BindViewDto))]
        public async Task<IActionResult> Bind([FromRoute] Guid riskId, [FromBody] BindRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _bindService.Bind(riskId, request));
        }

        [HttpGet("installment-and-invoice/{riskId}")]
        [Produces(typeof(List<InstallmentAndInvoiceDto>))]
        public async Task<IActionResult> GetInstallmentsAndInvoices([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _billingSummaryService.GetInstallmentsAndInvoices(riskId));
        }

        [HttpGet("summary/{riskId}")]
        [Produces(typeof(BillingSummaryDto))]
        [AllowAnonymous]
        public async Task<IActionResult> GetBillingSummary([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _billingSummaryService.GetBillingSummary(riskId));
        }

        [HttpGet("change-payment-plan/{riskId}")]
        [Produces(typeof(List<ChangePaymentPlanRequiredAmountDto>))]
        public async Task<IActionResult> GetChangePaymentPlanRequiredPayment([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _changePaymentPlanService.ComputePaymentsToChangePaymentPlan(riskId));
        }

        [HttpPost("change-payment-plan")]
        [Produces(typeof(ChangePayPlanInvoiceDto))]
        public async Task<IActionResult> ChangePaymentPlan([FromBody] ChangePaymentPlanRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _changePaymentPlanService.ChangePaymentPlan(request));
        }

        [HttpPost("calculate-risk-tax/{riskId}")]
        [Produces(typeof(ComputeRiskTaxResponseDto))]
        public async Task<IActionResult> CalculateRiskTaxAsync([FromRoute] Guid riskId, [FromBody] ComputeRiskTaxRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _taxService.PostCalculateRiskTaxAsync(request.EffectiveDate, request.StateCode, request.TransactionDetails, request.InstallmentCount));
        }

        [HttpPost("installment-and-invoice-before-bind")]
        [Produces(typeof(List<InstallmentAndInvoiceDto>))]
        public async Task<IActionResult> GetInstallmentsAndInvoicesBeforeBind([FromBody] InstallmentInvoiceBeforeBindRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _installmentScheduleService.GenerateInstallmentScheduleBeforeBind(request.TransactionDetails, request.EffectiveDate, request.StateCode,
                            request.PercentOfOutstanding, request.PaymentPlan, request.AppId));
        }
    }
}