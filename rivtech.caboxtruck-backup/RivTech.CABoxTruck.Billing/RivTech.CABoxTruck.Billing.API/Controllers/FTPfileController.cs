﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Collections.Generic;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FTPfileController : ControllerBase
    {
        private readonly IFtpService _ftpService;

        public FTPfileController(
            IFtpService ftpService
        )
        {
            _ftpService = ftpService;
        }

        [HttpGet("ServerFileName/{ftpSettingId}")]
        [Produces(typeof(IEnumerable<string>))]
        public IActionResult GetServerFileNames([FromRoute] string ftpSettingId)
        {
            return Ok(_ftpService.GetFileNamesInServer(ftpSettingId));
        }

        [HttpPost("DownloadFile")]
        [Produces(typeof(string))]
        public IActionResult DownloadFile([FromBody] FTPDownloadRequest request)
        {
            return Ok(_ftpService.DownloadFile(request));
        }
    }
}