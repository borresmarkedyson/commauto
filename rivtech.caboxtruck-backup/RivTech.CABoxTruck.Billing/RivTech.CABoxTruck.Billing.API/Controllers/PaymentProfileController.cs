﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentProfileController : ControllerBase
    {
        private readonly IPaymentProfileService _paymentProfileService;
        private readonly IUserDateService _userDateService;

        public PaymentProfileController(
            IPaymentProfileService paymentProfileService,
            IUserDateService userDateService
        )
        {
            _paymentProfileService = paymentProfileService;
            _userDateService = userDateService;
        }

        [HttpPost("CreateProfile")]
        [Produces(typeof(PaymentProfileDto))]
        public async Task<IActionResult> CreateProfile([FromBody] PaymentProfileRequestDto profileRequest)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _paymentProfileService.CreateProfileOnEnroll(profileRequest));
        }

        [HttpPut("UpdateProfile")]
        [Produces(typeof(PaymentProfileDto))]
        public async Task<IActionResult> UpdateProfile([FromBody] PaymentProfileRequestDto profileRequest)
        {
            return Ok(await _paymentProfileService.UpdateProfile(profileRequest));
        }

        [HttpDelete("DeleteProfile/{riskId}")]
        [Produces(typeof(PaymentProfileDto))]
        public async Task<IActionResult> DeleteProfile([FromRoute] Guid riskId)
        {
            return Ok(await _paymentProfileService.DeleteProfile(riskId));
        }

        [HttpGet("GetProfile/{riskId}")]
        [Produces(typeof(PaymentProfileDto))]
        public async Task<IActionResult> GetProfile([FromRoute] Guid riskId)
        {
            return Ok(await _paymentProfileService.GetProfile(riskId));
        }
    }
}