﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NsfController : ControllerBase
    {
        private readonly INsfService _nsfService;
        private readonly IUserDateService _userDateService;

        public NsfController(
            INsfService nsfService,
            IUserDateService userDateService
        )
        {
            _nsfService = nsfService;
            _userDateService = userDateService;
        }

        [HttpPost("count/{riskId}")]
        [Produces(typeof(NsfCountDto))]
        public async Task<IActionResult> GetNsfCount([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _nsfService.GetNsfCount(riskId));
        }
    }
}
