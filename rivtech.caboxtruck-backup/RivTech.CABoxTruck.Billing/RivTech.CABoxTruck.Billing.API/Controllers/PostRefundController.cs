﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostRefundController : ControllerBase
    {
        private readonly IRefundRequestTransmittalService _refundRequestTransmittalService;
        private readonly IUserDateService _userDateService;

        public PostRefundController(
            IRefundRequestTransmittalService refundRequestTransmittalService,
            IUserDateService userDateService
        )
        {
            _refundRequestTransmittalService = refundRequestTransmittalService;
            _userDateService = userDateService;
        }

        [HttpPost]
        [Produces(typeof(PostRefundResponseDto))]
        public async Task<IActionResult> PostRefundRequests([FromBody] PostRefundRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _refundRequestTransmittalService.SendRefundRequestViaFtp(request));
        }
    }
}
