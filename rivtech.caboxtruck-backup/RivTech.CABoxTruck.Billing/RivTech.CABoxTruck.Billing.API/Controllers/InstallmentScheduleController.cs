﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application.DTO;
using RivTech.CABoxTruck.Billing.Application.DTO.Requests;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RivTech.CABoxTruck.Billing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InstallmentScheduleController : ControllerBase
    {
        private readonly IInstallmentScheduleService _installmentScheduleService;
        private readonly IUserDateService _userDateService;

        public InstallmentScheduleController(
            IInstallmentScheduleService installmentScheduleService,
            IUserDateService userDateService)
        {
            _installmentScheduleService = installmentScheduleService;
            _userDateService = userDateService;
        }

        [HttpGet("{riskId}")]
        [Produces(typeof(IEnumerable<InstallmentDto>))]
        public async Task<IActionResult> GetAll([FromRoute] Guid riskId)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _installmentScheduleService.GetAllAsync(riskId));
        }

        [HttpPost("{riskId}")]
        [Produces(typeof(string))]
        public async Task<IActionResult> Post([FromRoute] Guid riskId, [FromBody] GenerateInstallmentScheduleRequestDto installmentSchedule)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _installmentScheduleService.SaveInstallmentScheduleAsync(riskId, installmentSchedule.PolicyNumber, installmentSchedule.StartingDueDate,
                installmentSchedule.PaymentPlan, installmentSchedule.AppId, false, null));
        }

        [HttpPost("add-edit/{riskId}")]
        [Produces(typeof(InstallmentDto))]
        public async Task<IActionResult> AddEditInstallmentSchedule([FromRoute] Guid riskId, [FromBody] AddEditInstallmentScheduleRequestDto request)
        {
            await _userDateService.SetUser(User.GetUser(), Request);
            return Ok(await _installmentScheduleService.AddEditFutureInstallmentScheduleAsync(riskId, request.InstallmentId, request.InvoiceDate, request.DueDate));
        }
    }
}
