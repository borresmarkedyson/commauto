using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using RivTech.CABoxTruck.Billing.API.Extensions;
using RivTech.CABoxTruck.Billing.Application;
using RivTech.CABoxTruck.Billing.Application.DomainEvents;
using RivTech.CABoxTruck.Billing.Application.DomainEvents.Handlers;
using RivTech.CABoxTruck.Billing.Application.ErrorHandling;
using RivTech.CABoxTruck.Billing.Application.Interfaces.Infrastructures;
using RivTech.CABoxTruck.Billing.Domain.Constants;
using RivTech.CABoxTruck.Billing.Domain.Payments;
using RivTech.CABoxTruck.Billing.Domain.Utils;
using RivTech.CABoxTruck.Billing.Infrastructure;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments;
using RivTech.CABoxTruck.Billing.Infrastructure.Payments.Pnc;
using RivTech.CABoxTruck.Billing.Persistence;
using RivTech.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RivTech.CABoxTruck.Billing.API
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private const string POLICY = "CABoxTruckPolicy";

        public IConfiguration Configuration { get; }
        readonly DateTime versionDate = DateTime.Now;

        public void ConfigureServices(IServiceCollection services)
        {
            var appSettings = Program.Configuration.Get<AppSettings>();

            services.AddCors(o => o.AddPolicy("Development", builder =>
            {
                builder.WithOrigins(Configuration.GetSection("AllowedOrigins").Get<string[]>())
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddApplication();
            services.AddPersistenceLayer(Configuration);
            services.AddInfrastructure(Configuration);

            services.AddControllers(options =>
            {
                options.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build()));
            });

            services.AddMemoryCache();

            services.AddSwaggerGen(cc =>
            {
                cc.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Billing",
                    Version = $"{versionDate:MMddyyyy}" + "-1",
                    Description = "An API to perform Pre-Defined data operations."
                });


                cc.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });


                cc.AddSecurityRequirement(new OpenApiSecurityRequirement()
                  {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                          {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                          },
                          Scheme = "oauth2",
                          Name = "Bearer",
                          In = ParameterLocation.Header,

                        },
                        new List<string>()
                      }
                    });
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpClient(ApiConstants.PNC_HTTP_CLIENT, c =>
            {
                c.BaseAddress = new Uri(Configuration.GetPncSettings("BaseUrl"));
                var sp = services.BuildServiceProvider();
                var tokenProvider = sp.GetService<ITokenProvider>();
                c.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenProvider.GetToken());
            });

            services.AddHttpClient(UtilConstant.HTTP_CLIENT_COMM_AUTO_BILLING, c => {
                c.BaseAddress = new Uri(Configuration["CommAutoBillingUrl"]);
            });

            InjectEventHandlers(services);

            services.AddScoped(x => new TokenProviderSettings
            {
                ApiKey = Configuration.GetPncSettings("ApiKey"),
                BaseUrl = Configuration.GetPncSettings("BaseUrl"),
                PrivateKey = Configuration.GetPncSettings("PrivateKey"),
                Secret = Configuration.GetPncSettings("Secret"),
                Role = Configuration.GetPncSettings("Role"),
                HashAlgorithm = Configuration.GetPncSettings("HashAlgorithm"),
                PassPhrase = Configuration.GetPncSettings("PassPhrase")
            });

            services.AddScoped<ITokenProvider, PncCachedTokenProvider>();
            services.AddScoped<IPaymentProcessor, PncPaymentProcessor>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(POLICY, policy =>
                {
                    policy.RequireAuthenticatedUser();
                });
            });
            JWTSecurity.RegisterJWTAuthentication(services);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var errors = actionContext.ModelState
                    .Where(e => e.Value.Errors.Count > 0)
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToArray();

                    var errorResponse = new ValidationErrorResponse { Errors = errors };
                    return new BadRequestObjectResult(errorResponse);
                };
            });

            services.Configure<AppSettings>(Program.Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var appSettings = Program.Configuration.Get<AppSettings>();
            Service.AppSettings = appSettings;

            app.Use(next => context =>
            {
                context.Request.EnableBuffering();
                return next(context);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseErrorLogging();
            app.UseStatusCodePagesWithReExecute("/errors/{0}");

            app.UseCors("Development");
            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireAuthorization();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Values Api V1");
            });
        }
        private void InjectEventHandlers(IServiceCollection services)
        {
            services.AddScoped<IDomainEventDispatcher>(x => new DomainEventDispatcher(services.BuildServiceProvider()));
            services.AddScoped<BaseDomainEventHandler<OnPaymentSuccesful>, OnPaymentSuccessfulEventHandler>();
        }
    }
}
